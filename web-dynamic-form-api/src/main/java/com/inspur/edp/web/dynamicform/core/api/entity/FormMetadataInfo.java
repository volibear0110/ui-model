/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;

import com.inspur.edp.web.common.utility.StringUtility;

/**
 * 关联的表单元数据信息
 *
 * @author guozhiqi
 */
public class FormMetadataInfo {

    /**
     * 待创建表单的code参数
     */
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    /**
     * 筛选条件字段信息
     */
    private FormFilterInfo formFilterInfo;

    /**
     *
     */
    private FormTableInfo formTableInfo;


    /**
     * eapi 元数据id
     */
    private String eapiMetadataId;

    /**
     * eapi 暴露的uri地址
     */
    private String eapiSourceUri;

    /**
     * eapi元数据code参数值
     */
    private String eapiMetadataCode;

    /**
     * eapi 元数据name参数值
     */
    private String eapiMetadataName;

    /**
     * vo元数据id
     */
    private String voMetadataId;

    /**
     * vo元数据code参数值
     */
    private String voMetadataCode;
    /**
     * vo元数据name参数值
     */
    private String voMetadataName;

    public String getEapiMetadataId() {
        return eapiMetadataId;
    }

    public void setEapiMetadataId(String eapiMetadataId) {
        this.eapiMetadataId = eapiMetadataId;
    }

    public String getEapiSourceUri() {
        return eapiSourceUri;
    }

    public void setEapiSourceUri(String eapiSourceUri) {
        this.eapiSourceUri = eapiSourceUri;
    }

    public String getEapiMetadataCode() {
        return eapiMetadataCode;
    }

    public void setEapiMetadataCode(String eapiMetadataCode) {
        this.eapiMetadataCode = eapiMetadataCode;
    }

    public String getEapiMetadataName() {
        return eapiMetadataName;
    }

    public void setEapiMetadataName(String eapiMetadataName) {
        this.eapiMetadataName = eapiMetadataName;
    }

    public String getVoMetadataId() {
        return voMetadataId;
    }

    public void setVoMetadataId(String voMetadataId) {
        this.voMetadataId = voMetadataId;
    }

    public String getVoMetadataCode() {
        return voMetadataCode;
    }

    public void setVoMetadataCode(String voMetadataCode) {
        this.voMetadataCode = voMetadataCode;
    }

    public String getVoMetadataName() {
        return voMetadataName;
    }

    public void setVoMetadataName(String voMetadataName) {
        this.voMetadataName = voMetadataName;
    }

    /**
     * 待创建表单的name参数值
     */
    private String name;

    public String getName() {
        if (StringUtility.isNullOrEmpty(this.name)) {
            return this.getCode();
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 关联的表单模板信息
     */
    private FormMetadataTemplateInfo templateInfo;

    public FormMetadataTemplateInfo getTemplateInfo() {
        return templateInfo;
    }

    public void setTemplateInfo(FormMetadataTemplateInfo templateInfo) {
        this.templateInfo = templateInfo;
    }


    public FormFilterInfo getFormFilterInfo() {
        return formFilterInfo;
    }

    public void setFormFilterInfo(FormFilterInfo formFilterInfo) {
        this.formFilterInfo = formFilterInfo;
    }

    public FormTableInfo getFormTableInfo() {
        return formTableInfo;
    }

    public void setFormTableInfo(FormTableInfo formTableInfo) {
        this.formTableInfo = formTableInfo;
    }
}
