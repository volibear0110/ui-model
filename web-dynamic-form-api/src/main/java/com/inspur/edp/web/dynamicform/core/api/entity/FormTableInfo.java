/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单关联的table 信息
 *
 * @author guozhiqi
 */
public class FormTableInfo {
    /**
     * table关联的code
     */
    private String code;

    /**
     * table 关联的name
     */
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * table 关联的字段信息
     */
    private List<FormFieldInfo> fieldInfoList;

    /**
     * 子表信息
     */
    private List<FormTableInfo> childTableInfoList;

    public List<FormFieldInfo> getFieldInfoList() {
        if (this.fieldInfoList == null) {
            this.fieldInfoList = new ArrayList<>();
        }
        return fieldInfoList;
    }

    public void setFieldInfoList(List<FormFieldInfo> fieldInfoList) {
        this.fieldInfoList = fieldInfoList;
    }

    public List<FormTableInfo> getChildTableInfoList() {
        if (this.childTableInfoList == null) {
            this.childTableInfoList = new ArrayList<>();
        }
        return childTableInfoList;
    }

    public void setChildTableInfoList(List<FormTableInfo> childTableInfoList) {
        this.childTableInfoList = childTableInfoList;
    }

    /**
     * 依据code 查找对应主表及其子表中的tableInfo
     *
     * @param code
     * @return
     */
    public FormTableInfo getByCode(String code) {
        if (code.equals(this.code)) {
            return this;
        }
        if (!this.getChildTableInfoList().isEmpty()) {
            for (FormTableInfo tableInfoItem : this.getChildTableInfoList()) {
                FormTableInfo tempTableInfo = tableInfoItem.getByCode(code);
                if (tempTableInfo != null) {
                    return tempTableInfo;
                }
            }
        }
        return null;
    }

}
