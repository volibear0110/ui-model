/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;

import lombok.Data;

import java.util.Objects;

/**
 * 表单关联的模板信息
 *
 * @author guozhiqi
 */
@Data
public class FormMetadataTemplateInfo {
    /**
     * 关联的表单模板id  此用来使用固定的模板参数
     */
    private String templateId;
    /**
     * 关联的表单模板code  此参数主要用于标识
     */
    private String templateCode;

    /**
     * 关联的表单模板name  此参数主要用于标识
     */
    private String templateName;

    /**
     * 是否使用列表模板
     *
     * @return
     */
    public boolean isListTemplate() {
        return Objects.equals(this.templateId, "list-template");
    }

    /**
     * 是否卡片模板
     * @return
     */
    public boolean isNewCardTemplate() {
        return Objects.equals(this.templateId, "card-template");
    }
}
