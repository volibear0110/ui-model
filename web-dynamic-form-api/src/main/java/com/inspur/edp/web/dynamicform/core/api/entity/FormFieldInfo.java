/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;

import lombok.Data;

/**
 * 字段信息
 */
@Data
public class FormFieldInfo {

    /**
     * 可以设置字段id 如果存在的话
     */
    private String id;

    /**
     * 字段name属性值
     */
    private String name;

    /**
     * 字段code属性值
     */
    private String code;

    /**
     * 是否显示
     */
    private boolean show;

    /**
     * 是否只读
     */
    private boolean readOnly;

    /**
     * 是否必填
     */
    private boolean required;
}
