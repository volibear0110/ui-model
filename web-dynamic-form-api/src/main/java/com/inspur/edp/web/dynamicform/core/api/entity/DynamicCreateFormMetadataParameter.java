/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 动态创建表单元数据参数
 *
 * @author guozhiqi
 */
@Data
public class DynamicCreateFormMetadataParameter {
    /**
     * 工程路径  应该为相对路径，通过补充工作空间路径来构造称为绝对路径
     */
    private String projectPath;
    /**
     * 工程对应code参数值
     */
    private String projectCode;

    /**
     * 工程所属namespace
     */
    private  String projectNameSpace;

    /**
     * 业务对象id
     */
    private  String bizobjectID;





    /**
     * 关联的表单信息列表  主要用于标识待创建的表单及其关联的模板信息
     */
    private List<FormMetadataInfo> formMetadataInfoList = new ArrayList<>();


}
