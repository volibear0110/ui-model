/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.api.entity;

import org.junit.Assert;
import org.junit.Test;

public class FormTableInfoTest {

    @Test
    public void testFindTableInfo() {
        String code = "main";
        String secondCode = "second";

        FormTableInfo formTableInfo = new FormTableInfo();
        formTableInfo.setCode(code);

        FormTableInfo secondInfo = new FormTableInfo();
        secondInfo.setCode(secondCode);

        FormTableInfo thirdInfo = new FormTableInfo();
        thirdInfo.setCode("third");

        secondInfo.getChildTableInfoList().add(thirdInfo);

        formTableInfo.getChildTableInfoList().add(secondInfo);

        FormTableInfo findMainTableInfo = formTableInfo.getByCode(code);
        Assert.assertNotEquals(findMainTableInfo, null);

        FormTableInfo secondTableInfo = formTableInfo.getByCode(secondCode);
        Assert.assertNotEquals(secondTableInfo, null);



    }
}