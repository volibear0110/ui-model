package com.inspur.edp.web.frontendproject.zerocode;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ZeroCodeServiceImplTest {

    @Test
    void resolveMetadataAndGenerateSource() {
        ZeroCodeParameter zeroCodeParameter=new ZeroCodeParameter();
        zeroCodeParameter.setProjectName("bo-guozhiqi");
        zeroCodeParameter.setAbsoluteBasePath("c:/projects/guozhiqi");
        zeroCodeParameter.setRelyNodeModulesPath("c:/projects/node_modules");
        zeroCodeParameter.setServiceUnitPath("scm/sd");


        List<ZeroCodeFormParameter>formParameters=new ArrayList<>();
        ZeroCodeFormParameter formParameter=new ZeroCodeFormParameter();
        formParameter.setCode("formcode");
        formParameter.setName("formname");

        ZeroCodeServiceImpl serviceImpl=new ZeroCodeServiceImpl();
        serviceImpl.resolveMetadataAndGenerateSource(zeroCodeParameter);

    }
}
