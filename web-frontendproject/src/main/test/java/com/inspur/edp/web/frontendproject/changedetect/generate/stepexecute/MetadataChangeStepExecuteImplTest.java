package com.inspur.edp.web.frontendproject.changedetect.generate.stepexecute;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * @Title: MetadataChangeStepExecuteImplTest
 * @Description: com.inspur.edp.web.frontendproject.changedetect.generate.stepexecute
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/1 8:47
 */
public class MetadataChangeStepExecuteImplTest {
    @Test
    public void testRegular() {
        String filePath = "fff.frm.en-vht.Lres";

        Pattern p = Pattern.compile(".frm.[a-zA-Z-_]+.lres", Pattern.COMMENTS | Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(filePath);

        Assert.assertEquals(m.find(), true);


        // assertTrue(hasFlag);
    }
}
