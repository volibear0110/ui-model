/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.debuguri;

import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.web.appconfig.api.entity.GspAppConfig;
import com.inspur.edp.web.appconfig.core.service.GspAppConfigService;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.metadata.GspProjectUtility;
import com.inspur.edp.web.common.metadata.MetadataProjectUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;
import com.inspur.edp.web.pageflow.metadata.service.RouteMetadataService;

/**
 * @Title: AbstractFormMetadataDebugUri
 * @Description: com.inspur.edp.web.frontendproject.debuguri 合并移动和PC的公共代码部分
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/4 17:04
 */
abstract class AbstractFormMetadataDebugUri implements FormMetadataDebugUri {
    private final TerminalType terminalType;

    public AbstractFormMetadataDebugUri(TerminalType terminalType) {
        this.terminalType = terminalType;
    }

    /**
     * 提取debuguri地址
     *
     * @param formMetadataRelativePath
     * @param formMetadataId
     * @return
     */
    public String getFormMetadataDebugUri(String formMetadataRelativePath, String formMetadataId) {
        // 1. 获取表单元数据对应route Uri
        String routeUri = this.generateRouteUri(formMetadataRelativePath, formMetadataId);

        // 2. 获取部署路径
        return this.generateDebugUri(formMetadataRelativePath, routeUri);
    }

    /**
     * 构造路由跳转Uri地址
     *
     * @return
     */
    protected abstract String generateRouteUri(String formMetadataRelativePath, String formMetadataId);

    /**
     * 判断是否为PC表单
     *
     * @return
     */
    protected boolean isPc() {
        return TerminalType.isPC(this.terminalType);
    }

    /**
     * 构造debugUri地址
     *
     * @param formMetadataRelativePath
     * @param routeUri
     * @return
     */
    protected String generateDebugUri(String formMetadataRelativePath, String routeUri) {
        // 2. 获取部署路径
        MetadataProject metadataProject = MetadataUtility.getInstance().getMetadataProject(formMetadataRelativePath).orElseThrow(() -> new WebCustomException(String.format("依据工程路径%s获取工程信息为空", formMetadataRelativePath)));
        GspProject gspProject = GspProjectUtility.getProjectInformation(metadataProject.getProjectPath());
        String deploymentPath = "/" + gspProject.getSuDeploymentPath().toLowerCase();

        String projectName = GspProjectUtility.getProjectName(gspProject);

        return deploymentPath + "/" + this.terminalType.getPublishPathName() + "/" + projectName + "/" + "index.html" + "#" + routeUri;
    }

    /**
     * 获取工程目录下得页面流元数据信息
     *
     * @param projectPath 工程路径
     * @return
     */
    protected PageFlowMetadataEntity getRouteMetadataContent(String projectPath) {
        PageFlowMetadataEntity pageFlowMetadataEntity = null;

        // 1.获取路由元数据中内容
        GspAppConfig appConfig = GspAppConfigService.getCurrent().getOrCreateAppConfig(projectPath);
        // 非前端工程
        if (appConfig == null) {
            return null;
        }
        // 2.获取路由元数据中内容
        if (isPc()) {
            pageFlowMetadataEntity = RouteMetadataService.getRouteMetadataContentWithMetadataIdAndProjectPath(appConfig.getPageFlowMetadataID(), appConfig.getPageFlowMetadataFileName(), projectPath);
        } else {
            pageFlowMetadataEntity = RouteMetadataService.getRouteMetadataContentWithMetadataIdAndProjectPath(appConfig.getMobilePageFlowMetadataID(), appConfig.getMobilePageFlowMetadataFileName(), projectPath);
        }

        return pageFlowMetadataEntity;
    }

    protected String getFormMetadataRouteUriWithDevPath(String projectPath, String formMetadataId) {
        String routeUri = "";

        if (StringUtility.isNullOrEmpty(projectPath) || StringUtility.isNullOrEmpty(formMetadataId)) {
            return routeUri;
        }

        PageFlowMetadataEntity pageFlowMetadataEntity = getRouteMetadataContent(projectPath);
        if (pageFlowMetadataEntity == null) {
            return routeUri;
        }

        routeUri = PageFlowMetadataEntity.GetPageRouteUri(pageFlowMetadataEntity, formMetadataId);

        return routeUri;
    }

    protected String getFormMetadataRouteUriWithoutDevPath(String projectPath, String formMetadataId) {
        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);
        return getFormMetadataRouteUriWithDevPath(projectPath, formMetadataId);
    }

    protected String getFormMetadataRouteUri(String formMetadataRelativePath, String formMetadataId) {
        MetadataProject metadataProject = MetadataProjectUtility.getMetadataProject(formMetadataRelativePath).orElseThrow(() ->
                new WebCustomException(String.format("根据工程路径%s获取对应工程信息为空", formMetadataRelativePath))
        );

        return getFormMetadataRouteUriWithoutDevPath(metadataProject.getProjectPath(), formMetadataId);
    }
}
