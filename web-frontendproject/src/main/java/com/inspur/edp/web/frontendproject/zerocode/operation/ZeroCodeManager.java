/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;
import com.inspur.edp.web.frontendproject.zerocode.operation.deploy.SourceCodeDeployManager;
import com.inspur.edp.web.frontendproject.zerocode.operation.sourcegenerate.SourceCodeManager;
import com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.WebDevJsonManager;
import com.inspur.edp.web.jitengine.JITEngineManager;

public class ZeroCodeManager {


    private ZeroCodeManager() {

    }

    public static ZeroCodeManager getInstance() {
        return new ZeroCodeManager();
    }

    /**
     * 保存零代码参数元数据到webdev文件目录
     *
     * @param zeroCodeParameter
     */
    public void saveZeroCodeParameterMetadataIntoJson(ZeroCodeParameter zeroCodeParameter) {
        // 保存webdevjson文件
        WebDevJsonManager.saveWebDevJson(zeroCodeParameter);
    }

    public void build(ZeroCodeParameter zeroCodeParameter) {
        TerminalType.rescure((t) -> {
            if (zeroCodeParameter.hasFormParameter(t)) {
                JITEngineManager.buildFrontendProject(zeroCodeParameter.getAbsoluteBasePath(), ExecuteEnvironment.Runtime, t);
            }
        });
    }

    public void generateSource(ZeroCodeParameter zeroCodeParameter) {
        SourceCodeManager.generateSourceCode(zeroCodeParameter);
    }

    public void deploy(ZeroCodeParameter zeroCodeParameter) {
        SourceCodeDeployManager.deploy(zeroCodeParameter);
    }
}
