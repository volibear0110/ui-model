/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadataanalysis.StateMachineAnalysis;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormRefMetadataParameter;

public class StateMachineMetadataOperation {
    /**
     * 保存
     *
     * @param formParameter
     */
    public void save(FormDOM json, ZeroCodeFormParameter formParameter, String wevDevPath,
                     String formMetadataFileName) {
        StateMachineAnalysis stateMachineAnalysis=new StateMachineAnalysis(ExecuteEnvironment.Runtime,false);
        stateMachineAnalysis.resolveStateMachine(json, formMetadataFileName, wevDevPath, null, (resolveStateMachineParameter) -> {
            ZeroCodeFormRefMetadataParameter formRefMetadataParameter = formParameter.getSpecialFormRefMetaData(resolveStateMachineParameter.getMetadataManagerParameter().getId(), MetadataTypeEnum.StateMachine);
            return formRefMetadataParameter.getMetadata();
        });
    }


}
