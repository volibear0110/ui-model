/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect;

/**
 * @Title: ChangeDetectExecuteResult
 * @Description: 变更检测执行结果
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 11:03
 */
public class ChangeDetectExecuteResult {
    /**
     * 定义当前的执行类型
     * 为generate 或  compile
     */
    private ChangeDetectExecuteType currentExecuteType = ChangeDetectExecuteType.Generate;

    /**
     * 是否所有的变更检测项均已通过
     */
    private boolean allPass = false;

    /**
     * 未变更检测通过项  具体的未通过原因
     */
    private String unPassReason;

    public ChangeDetectExecuteType getCurrentExecuteType() {
        return this.currentExecuteType;
    }

    public boolean isAllPass() {
        return this.allPass;
    }

    public String getUnPassReason() {
        return this.unPassReason;
    }

    private ChangeDetectExecuteResult() {
    }

    /**
     * 获取对应的检测通过的结果定义
     *
     * @return
     */
    public static ChangeDetectExecuteResult getPassResult(ChangeDetectExecuteType changeDetectExecuteType) {
        ChangeDetectExecuteResult executeResult = new ChangeDetectExecuteResult();
        executeResult.allPass = true;
        executeResult.currentExecuteType = changeDetectExecuteType;
        executeResult.unPassReason = null;
        return executeResult;
    }

    /**
     * 获取对应的检测不通过结果定义
     *
     * @param changeDetectExecuteType
     * @param unPassReason
     * @return
     */
    public static ChangeDetectExecuteResult getUnPassResult(ChangeDetectExecuteType changeDetectExecuteType, String unPassReason) {
        ChangeDetectExecuteResult executeResult = new ChangeDetectExecuteResult();
        executeResult.allPass = false;
        executeResult.currentExecuteType = changeDetectExecuteType;
        executeResult.unPassReason = unPassReason;
        return executeResult;
    }

}
