/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.generate;

import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteResult;
import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteService;
import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteType;
import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteResult;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteService;

import java.util.List;

/**
 * @Title: GenerateChangeDetectExecuteService
 * @Description: 变更检测  生成动作实现
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 20:12
 */
public class GenerateChangeDetectExecuteServiceImpl implements ChangeDetectExecuteService {

    // 当前执行动作的检测类型为生成动作
    private final ChangeDetectExecuteType currentChangeDetectType = ChangeDetectExecuteType.Generate;

    /**
     * 变更检测
     * 获取生成需要执行的变更检测步骤，按照顺序依次执行
     * 如果上一个步骤的执行结果为变更检测未通过，那么后续的变更检测都不再执行
     *
     * @param detectContext 变更检测上下文参数
     * @return
     */
    @Override
    public ChangeDetectExecuteResult execute(ChangeDetectContext detectContext) {
        ChangeDetectStepExecuteResult stepExecuteResult = ChangeDetectStepExecuteResult.getPassResult(this.currentChangeDetectType);
        // 获取排序后的生成变更检测步骤列表
        List<GenerateChangeDetectStepType> orderedChangeDetectTypeList = GenerateChangeDetectStepType.getOrderedList();

        for (GenerateChangeDetectStepType t : orderedChangeDetectTypeList) {
            ChangeDetectStepExecuteService executeService = t.getChangeDetectStep();
            stepExecuteResult = executeService.execute(detectContext);
            if (!stepExecuteResult.isPass()) {
                break;
            }
        }

        if (stepExecuteResult.isPass()) {
            return ChangeDetectExecuteResult.getPassResult(this.currentChangeDetectType);
        } else {
            return ChangeDetectExecuteResult.getUnPassResult(this.currentChangeDetectType, stepExecuteResult.getReason());
        }
    }

    /**
     * 更新对应的变更记录
     *
     * @param detectContext
     */
    @Override
    public void updateChangeset(ChangeDetectContext detectContext) {
        List<GenerateChangeDetectStepType> orderedChangeDetectTypeList = GenerateChangeDetectStepType.getOrderedList();

        for (GenerateChangeDetectStepType t : orderedChangeDetectTypeList) {
            ChangeDetectStepExecuteService executeService = t.getChangeDetectStep();
            executeService.updateChangeset(detectContext);
        }
    }
}
