/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormFormatParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.pageflow.metadata.entity.AdaptedPage;

public class MetaDataOperationManager {
    public static void save(ZeroCodeFormParameter formParameter, String webDevPath, AdaptedPage adaptedPage) {

        // 保存表单元数据
        FormMetadataContentOperation formMetadataContentOperation = new FormMetadataContentOperation();
        formMetadataContentOperation.saveFormMetadataInJson(webDevPath, formParameter,adaptedPage);

    }

    public static void saveFormFormat(ZeroCodeFormParameter formParameter, ZeroCodeFormFormatParameter formFormatParameter,
                                      String webDevPath, AdaptedPage adaptedPage) {

        // 保存表单元数据
        FormMetadataContentOperation formMetadataContentOperation = new FormMetadataContentOperation();
        formMetadataContentOperation.saveFormFormat(webDevPath, formParameter, formFormatParameter, adaptedPage);

    }
}
