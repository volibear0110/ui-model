package com.inspur.edp.web.frontendproject.constant;

public class I18nExceptionConstant {
    public final static String WEB_FRONT_PROJECT_ERROR_0001 = "WEB_FRONT_PROJECT_ERROR_0001";
    public final static String WEB_FRONT_PROJECT_ERROR_0002 = "WEB_FRONT_PROJECT_ERROR_0002";
//    public final static String WEB_FRONT_PROJECT_ERROR_0003 = "WEB_FRONT_PROJECT_ERROR_0003";
    public final static String WEB_FRONT_PROJECT_ERROR_0004 = "WEB_FRONT_PROJECT_ERROR_0004";
    public final static String WEB_FRONT_PROJECT_ERROR_0005 = "WEB_FRONT_PROJECT_ERROR_0005";
    public final static String WEB_FRONT_PROJECT_ERROR_0006 = "WEB_FRONT_PROJECT_ERROR_0006";
    public final static String WEB_FRONT_PROJECT_ERROR_0007 = "WEB_FRONT_PROJECT_ERROR_0007";
    public final static String WEB_FRONT_PROJECT_ERROR_0008 = "WEB_FRONT_PROJECT_ERROR_0008";
    public final static String WEB_FRONT_PROJECT_ERROR_0009 = "WEB_FRONT_PROJECT_ERROR_0009";
    public final static String WEB_FRONT_PROJECT_ERROR_0010 = "WEB_FRONT_PROJECT_ERROR_0010";
    public final static String WEB_FRONT_PROJECT_ERROR_0011 = "WEB_FRONT_PROJECT_ERROR_0011";
    public final static String WEB_FRONT_PROJECT_ERROR_0012 = "WEB_FRONT_PROJECT_ERROR_0012";
    public final static String WEB_FRONT_PROJECT_ERROR_0013 = "WEB_FRONT_PROJECT_ERROR_0013";
    public final static String WEB_FRONT_PROJECT_ERROR_0014 = "WEB_FRONT_PROJECT_ERROR_0014";
}
