/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.customservice;

import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.GspProjectUtility;

/**
 * 自定义代码构件
 *
 * @author guozhiqi
 */
public class SourceServicePathGenerator {

    /**
     * 获取自定义代码路径
     *
     * @param projectPath
     * @param terminalType
     * @return
     */
    public static String getSourceServiceProductPath(String projectPath, TerminalType terminalType, boolean isSingleDynamicForm) {
        String serviceRelativeProductPath = terminalType.getServiceRelativePath(isSingleDynamicForm);
        return FileUtility.combine(projectPath, serviceRelativeProductPath);
    }

    /**
     * 获取命令控件ts的部署位置
     *
     * @param projectPath
     * @param terminalType
     * @param isSingleDynamicForm
     * @return
     */
    public static String getTargetServiceProductPath(String projectPath, TerminalType terminalType, boolean isSingleDynamicForm) {
        String targetServiceProductPath = "";

        if (terminalType == TerminalType.PC) {
            if (isSingleDynamicForm) {
                // 定义解析表单service的部署路径 主要目的是部署解析表单依赖js
                targetServiceProductPath = projectPath + "/src/" + FrontendProjectConstant.PROJECT_GENERATE_PATH_FOR_Dynamic;
            } else {
                targetServiceProductPath = projectPath + "/src/app/projects/" + GspProjectUtility.getProjectName(projectPath) + "/src/app";
            }
        } else if (terminalType == TerminalType.MOBILE) {
            targetServiceProductPath = projectPath + "/src/mobileapp/src/apps";
        } else {
            throw new WebCustomException("暂不支持的终端类型：" + terminalType + "。请联系管理员处理。");
        }

        return targetServiceProductPath;
    }
}
