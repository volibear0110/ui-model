/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.debuguri;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContentService;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * description:移动表单调试Url地址
 *
 * @author Noah Guo
 * @date 2021/03/17
 */
  class FormMetadataDebugUriWithMobile extends AbstractFormMetadataDebugUri {
    public FormMetadataDebugUriWithMobile() {
        super(TerminalType.MOBILE);
    }

    @Override
    protected String generateRouteUri(String formMetadataRelativePath, String formMetadataId) {
        String formRouteUri = getFormMetadataRouteUri(formMetadataRelativePath, formMetadataId);

        // 获取表单实体，进而获取表单代码
        String defaultPageUri = "";

        // 进行元数据的调用借口获取方式调整
        MetadataGetterParameter metadataGetterParameter = MetadataGetterParameter.getNewInstance(formMetadataId, null, MetadataTypeEnum.Frm);
        metadataGetterParameter.setTargetMetadataNotFoundMessage("表单调试，获取表单元数据为空，对应元数据id:" + formMetadataId);

        GspMetadata metadata = MetadataUtility.getInstance().getMetadataWithDesign(metadataGetterParameter);
        FormMetadataContent formMetadataContent = (FormMetadataContent) metadata.getContent();
        FormDOM formDom = FormMetadataContentService.getInstance().getFormContent(formMetadataContent);
        HashMap<String, Object> component = null;
        ArrayList<HashMap<String, Object>> components = formDom.getModule().getComponents();
        if (components != null && !components.isEmpty()) {
            // 使用第一组件作为默认组件
            component = components.get(0);
        }

        if (component == null) {
            defaultPageUri = "";
        } else {
            if (component.containsKey("route")) {
                HashMap<String, Object> routeObject = convertToDictionaryObject(component.get("route"));
                if (routeObject.containsKey("uri")) {
                    defaultPageUri = routeObject.get("uri").toString();
                }
            }

            if (StringUtility.isNullOrEmpty(defaultPageUri)) {
                defaultPageUri = component.get("id").toString();
            }
        }
        return "/" + formRouteUri + "/" + defaultPageUri;
    }


    private HashMap<String, Object> convertToDictionaryObject(Object targetObject) {
        String json = SerializeUtility.getInstance().serialize(targetObject, false);
        return SerializeUtility.getInstance().deserialize(json, HashMap.class);
    }
}
