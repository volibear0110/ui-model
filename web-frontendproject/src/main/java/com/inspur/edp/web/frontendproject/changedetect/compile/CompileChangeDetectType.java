/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.compile;

import com.inspur.edp.web.frontendproject.changedetect.compile.stepexecute.FileChangeStepExecuteImpl;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Title: CompileChangeDetectType
 * @Description: 编译依赖的变更检测类型
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 10:57
 */
public enum CompileChangeDetectType {
    FileChange(1, "文件变更检测") {
        @Override
        public ChangeDetectStepExecuteService getChangeDetectStep() {
            return new FileChangeStepExecuteImpl();
        }
    };

    /**
     * 执行序号
     */
    private int order = 1;
    /**
     * 描述信息
     */
    private final String description;

    CompileChangeDetectType(int order, String description) {
        this.order = order;
        this.description = description;
    }

    /**
     * 获取编译对应的步骤执行实例
     *
     * @return
     */
    public abstract ChangeDetectStepExecuteService getChangeDetectStep();

    /**
     * 获取编译变更检测步骤按顺序执行列表
     *
     * @return
     */
    public static List<CompileChangeDetectType> getOrderedList() {
        return Arrays.stream(values()).sorted(Comparator.comparing(t -> t.order)).collect(Collectors.toList());
    }
}
