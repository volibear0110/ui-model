/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.constant.I18nExceptionConstant;

public class ZeroCodeParameterValidator {
    /**
     * 编译入参参数验证
     *
     * @param zeroCodeParameter
     */
    public static void validate(ZeroCodeParameter zeroCodeParameter) {
        if (zeroCodeParameter == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0009);
        }

        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getAbsoluteBasePath())) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0010);
        }

        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getProjectName())) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0011);
        }
        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getRelyNodeModulesPath())) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0012);
        }

        if (zeroCodeParameter.getFormParameters() == null || zeroCodeParameter.getFormParameters().size() == 0) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0013);
        }
        if(StringUtility.isNullOrEmpty(zeroCodeParameter.getServiceUnitPath())){
            throw new WebCustomException(I18nExceptionConstant.WEB_FRONT_PROJECT_ERROR_0014);
        }
    }

}
