/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;

/**
 * @author guozhiqi
 */
public class FrontendProjectUtility {
    private FrontendProjectUtility() {

    }

    /**
     * 获取工程类型：前端工程和非前端工程
     * 判断是否是前端工程
     */
    public static String getProjectType(String projectPath) {
        if (StringUtility.isNullOrEmpty(projectPath)) {
            return FrontendProjectConstant.NON_FRONTEND_PROJECT_TYPE;
        }

        // TODO: 更新算法，提供基于文件后缀的查找方法
        // 历史原因，只能根据当前是否包含表单来区分前端工程和非前端工程
        List<GspMetadata> formMetataList = getAllTypeFormMetadata(projectPath);
        if (ListUtility.isNotEmpty(formMetataList)) {
            return FrontendProjectConstant.FRONTEND_PROJECT_TYPE;
        } else {
            return FrontendProjectConstant.NON_FRONTEND_PROJECT_TYPE;
        }
    }

    /**
     * 获取工程包含的表单个数
     */
    private static List<GspMetadata> getAllTypeFormMetadata(String projectPath) {
        MetadataService metadataProjectService = SpringBeanUtils.getBean(MetadataService.class);
        // .frm 和 .mfrm 后缀
        return metadataProjectService.getMetadataList(projectPath, TerminalType.getAllTypeSuffixList());
    }

    /**
     * 获取工程包含的表单个数
     */
    public static List<GspMetadata> getFormMetadataList(String projectPath, TerminalType terminalType) {
        return MetadataUtility.getInstance().getMetadataListInProjectWithDesign(projectPath, terminalType.getFormMetadataSuffix());
    }

    /**
     * 获取工程下前端代码编译脚本对应目录
     *
     * @param projectPath  工程路径
     * @param projectName  工程名称
     * @param terminalType 表单类型
     * @return
     */
    public static String getProjectSourceCodeBuildPath(String projectPath, String projectName, TerminalType terminalType, boolean isJieXiForm) {
        if (isJieXiForm) {
            return FileUtility.combine(projectPath, terminalType.getDeployableProjectRelativePath(true));
        }
        return FileUtility.combine(projectPath, terminalType.getDeployableProjectRelativePath(false), projectName);
    }

    /**
     * 获取含开发根路径的工程路径
     */
    public static String getProjectPathWithDevRootPath(String projectPath) {
        String projectPathWithDevRootPath = projectPath;
        String devRootPath = MetadataUtility.getInstance().getDevRootPath();
        if (!checkIfStartsWithDevRoot(projectPath, devRootPath)) {
            projectPathWithDevRootPath = java.nio.file.Paths.get(devRootPath).resolve(projectPath).toString();
        }
        return projectPathWithDevRootPath;
    }

    private static boolean checkIfStartsWithDevRoot(String projectPath, String devRootPath) {
        return StringUtility.startWith(FileUtility.getPlatformIndependentPath(projectPath), FileUtility.getPlatformIndependentPath(devRootPath));
    }
}
