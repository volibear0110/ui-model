/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.formdynamic;

import com.inspur.edp.web.frontendproject.FrontendProjectService;
import com.inspur.edp.web.frontendproject.entity.ChosenFormList;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;

/**
 * 解析型表单 元数据解析
 *
 * @author guozhiqi
 */
public class FormDynamicMetadataResolver {
    /**
     * 元数据解析
     *
     * @param dynamicParameter
     */
    public static void resolve(FormDynamicParameter dynamicParameter, ChosenFormList buildFormList) {

        // 执行工程参数得提取
        FrontendProjectService.getInstance().resolveFormMetadatas(dynamicParameter.getProjectPath(), buildFormList);
    }
}
