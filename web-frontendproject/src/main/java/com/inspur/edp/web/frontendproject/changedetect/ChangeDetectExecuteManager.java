/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect;

import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;

/**
 * @Title: ChangeDetectExecuteManager
 * @Description: 变更检测执行服务manager  对外暴露统一的使用方式
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/22 15:05
 */
public class ChangeDetectExecuteManager {
    /**
     * 执行对应的变更检测动作 对应的执行服务从枚举中获取
     *
     * @param executeType
     * @param detectContext
     * @return
     */
    public static ChangeDetectExecuteResult execute(ChangeDetectExecuteType executeType, ChangeDetectContext detectContext) {
        detectContext.setExecuteType(executeType);
        return executeType.getExecuteService().execute(detectContext);
    }

    /**
     * 变更检测后回写
     * @param executeType
     * @param detectContext
     */
    public static void updateChangeset(ChangeDetectExecuteType executeType, ChangeDetectContext detectContext) {
        detectContext.setExecuteType(executeType);
        executeType.getExecuteService().updateChangeset(detectContext);
    }
}
