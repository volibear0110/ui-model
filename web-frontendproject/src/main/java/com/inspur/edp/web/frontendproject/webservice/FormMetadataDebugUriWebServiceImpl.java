/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.webservice;

import com.inspur.edp.web.frontendproject.FrontendProjectService;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/15
 */
public class FormMetadataDebugUriWebServiceImpl implements FormMetadataDebugUriWebService {
    /**
     * 获取PC表单元数据的部署uri
     */
    @Override
    public String getFormMetadataDebugUri(String formMetadataRelativePath, String formMetadataId) {
        return FrontendProjectService.getFormMetadataDebugUri(formMetadataRelativePath, formMetadataId, "pc");
    }

    /**
     * 获取表单元数据的部署uri
     *
     * @param formMetadataRelativePath
     * @param formMetadataId
     * @param formType
     * @return
     */
    @Override
    public String getFormMetadataDebugUriByFormType(String formMetadataRelativePath,
                                                    String formMetadataId,
                                                    String formType) {
        return FrontendProjectService.getFormMetadataDebugUri(formMetadataRelativePath, formMetadataId, formType);
    }
}
