/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;

/***
 * 零代码 入参 参数初始化
 */
public class ZeroCodeParameterInitializer {
    public static void initialize(ZeroCodeParameter zeroCodeParameter) {
        if (zeroCodeParameter == null) {
            return;
        }
        String currentServerPath = FileUtility.getCurrentWorkPath();
        // 为node_modules 设置默认值
        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getRelyNodeModulesPath())) {
            String devRootPath = FileUtility.combine(currentServerPath, ZeroCodeConstants.ZeroCodeRelativePath);
            String refNodeModulesPath = FileUtility.getPlatformIndependentPath(FileUtility.combine(devRootPath, "node_modules"));
            zeroCodeParameter.setRelyNodeModulesPath(refNodeModulesPath);
        }


        // 设置安装盘server路径
        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getServerPath())) {
            zeroCodeParameter.setServerPath(currentServerPath);
        }

        // 如果su路径不为空
        if (!StringUtility.isNullOrEmpty(zeroCodeParameter.getServiceUnitPath())) {
            // 设置源su路径
            zeroCodeParameter.setOriginalServiceUnitPath(zeroCodeParameter.getServiceUnitPath());
            String serviceUnitPath = zeroCodeParameter.getServiceUnitPath();
            if (!serviceUnitPath.startsWith("apps")) {
                // 在serviceUnit 路径前面追加apps
                serviceUnitPath = "apps" + FileUtility.DIRECTORY_SEPARATOR_CHAR + serviceUnitPath;
                zeroCodeParameter.setServiceUnitPath(serviceUnitPath.toLowerCase());
            }
        } else {
            zeroCodeParameter.setServiceUnitPath("apps");
        }

        if (StringUtility.isNullOrEmpty(zeroCodeParameter.getAbsoluteBasePath())) {
            // 设置
            String absolutePath = FileUtility.combineOptional(currentServerPath, ZeroCodeConstants.ZeroCodeRelativePath, ZeroCodeConstants.ZeroCodePath, zeroCodeParameter.getOriginalServiceUnitPath(), zeroCodeParameter.getProjectName().toLowerCase());
            zeroCodeParameter.setAbsoluteBasePath(absolutePath);
        }


    }
}
