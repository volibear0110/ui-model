/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.deploy;

import com.inspur.edp.lcm.metadata.api.entity.ExtractContext;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.GspProjectUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectExtractor;

/**
 * 部署交付物到publish目录
 *
 * @author noah
 * 2023/7/8 15:04
 */
public class ProjectDeployerToPublish {
    /**
     * 部署前端交付物至publish目录
     *
     * @param projectPath
     */
    public static void deploy(String projectPath) {
        try {
            GspProject projectInfo = GspProjectUtility.getProjectInformation(projectPath);
            String projectName = projectInfo.getMetadataProjectName();
            // 移除工程名后的路径
            projectPath = projectPath.substring(0, projectPath.toLowerCase().lastIndexOf(projectName.toLowerCase()) + projectName.length());

            ExtractContext extractContext = new ExtractContext();
            extractContext.setProjectPath(projectPath);
            extractContext.setDeployPath(projectInfo.getSuDeploymentPath());
            FrontendProjectExtractor projectExtractor = new FrontendProjectExtractor();
            projectExtractor.extract(extractContext);
        } catch (Exception ex) {
            WebLogger.Instance.error("部署前端脚本至publish目录失败" + ex, ProjectDeployerToPublish.class.getName());
        }
    }
}
