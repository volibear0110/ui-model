/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.i18n.resource.api.II18nResourceDTManager;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.I18nResource;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadatamanager.FormMetadataManager;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormFormatParameter;
import com.inspur.edp.web.jitengine.i18nresource.GenerateResourceManager;
import com.inspur.edp.web.pageflow.metadata.entity.AdaptedPage;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 表单元数据保存
 *
 * @author noah
 */
class FormMetadataContentOperation {

    /**
     * 表单源苏剧保存
     *
     * @param webDevPath
     * @param formParameter
     */
    public void saveFormMetadataInJson(String webDevPath, ZeroCodeFormParameter formParameter, AdaptedPage adaptedPage) {
        GspMetadata formMd = formParameter.getMetadata();
        String strFormDom = ((FormMetadataContent) formMd.getContent()).getContents().toString();
        String formMetadataFileName = formParameter.getMetadata().getHeader().getFileName();
        // 保存表单元数据到webdev
        saveFormMetadataInJson(webDevPath, formMetadataFileName, strFormDom);

        FormDOM formDOM = SerializeUtility.getInstance().deserialize(strFormDom, FormDOM.class);

        // 构造页面流需要数据
        adaptedPage.setId(formParameter.getMetadata().getHeader().getId());
        adaptedPage.setCode(formDOM.getModule().getCode());
        adaptedPage.setName(formDOM.getModule().getName());
        adaptedPage.setFileName(formParameter.getMetadata().getHeader().getFileName());
        adaptedPage.setRouteUri(formDOM.getModule().getCode());


        // command命令元数据保存
        CommandMetadataOperation commandMetadataOperation = new CommandMetadataOperation();
        commandMetadataOperation.save(formDOM, formParameter, webDevPath, formMetadataFileName);

        //eapi 元数据保存
        EapiMetadataOperation eapiMetadataOperation = new EapiMetadataOperation();
        eapiMetadataOperation.save(formDOM, formMetadataFileName, webDevPath, formParameter);

        // 状态机元数据保存
        StateMachineMetadataOperation stateMachineMetadataOperation = new StateMachineMetadataOperation();
        stateMachineMetadataOperation.save(formDOM, formParameter, webDevPath, formMetadataFileName);

        // 资源元数据保存
        FormResourceOperation formResourceOperation = new FormResourceOperation();
        formResourceOperation.save(formMd, webDevPath);

        // 表达式构造
        FormExpressionOperation formExpressionOperation = new FormExpressionOperation();
        TerminalType terminalType = formParameter.isMobile() ? TerminalType.MOBILE : TerminalType.PC;
        formExpressionOperation.save(formDOM, formParameter, webDevPath, terminalType);
    }

    public void saveFormFormat(String webDevPath, ZeroCodeFormParameter formParameter, ZeroCodeFormFormatParameter formFormatParameter, AdaptedPage adaptedPage) {
        GspMetadata formMd = formParameter.getMetadata();
        String configId = formFormatParameter.getFormFormatConfigId();
        String formatPath = FileUtility.combine("form-format", configId);
        String jsonFilePath = FileUtility.combine(webDevPath, formatPath);

        String strFormDom = ((FormMetadataContent) formFormatParameter.getMetadataContent()).getContents().toString();
        String formMetadataFileName = formParameter.getMetadata().getHeader().getFileName();
        // 保存表单元数据到webdev/form-format/{configId}
        saveFormMetadataInJson(jsonFilePath, formMetadataFileName, strFormDom);

        FormDOM formDOM = SerializeUtility.getInstance().deserialize(strFormDom, FormDOM.class);

        // 构造页面流需要数据
        adaptedPage.setId(configId);
        String formCode = formDOM.getModule().getCode();
        String pageCode = formCode + "_" + configId.substring(0, 8);
        String pageName = formDOM.getModule().getName() + "_" + configId.substring(0, 8);
        adaptedPage.setCode(pageCode);
        adaptedPage.setName(pageName);
        adaptedPage.setRouteUri(pageCode);
        String fullName = FileUtility.combine(formatPath, formParameter.getMetadata().getHeader().getFileName());
        adaptedPage.setFileName(fullName);

        // command命令元数据保存
        CommandMetadataOperation commandMetadataOperation = new CommandMetadataOperation();
        commandMetadataOperation.save(formDOM, formParameter, jsonFilePath, formMetadataFileName);

        //eapi 元数据保存
        EapiMetadataOperation eapiMetadataOperation = new EapiMetadataOperation();
        eapiMetadataOperation.save(formDOM, formMetadataFileName, jsonFilePath, formParameter);

        // 状态机元数据保存
        StateMachineMetadataOperation stateMachineMetadataOperation = new StateMachineMetadataOperation();
        stateMachineMetadataOperation.save(formDOM, formParameter, jsonFilePath, formMetadataFileName);

        // 资源元数据保存
        FormResourceOperation formResourceOperation = new FormResourceOperation();
        formResourceOperation.save(formMd, jsonFilePath);

        // 表达式构造
        FormExpressionOperation formExpressionOperation = new FormExpressionOperation();
        TerminalType terminalType = formParameter.isMobile() ? TerminalType.MOBILE : TerminalType.PC;
        formExpressionOperation.save(formDOM, formParameter, jsonFilePath, terminalType);
    }

    /**
     * 表单元数据保存操作
     *
     * @param webDevPath
     * @param formMetaDataFileName
     * @param content
     */
    private void saveFormMetadataInJson(String webDevPath, String formMetaDataFileName, String content) {
        FormMetadataManager formMetadataManager = new FormMetadataManager(ExecuteEnvironment.Design, null, false);
        formMetadataManager.saveMetadataFile(webDevPath, formMetaDataFileName.toLowerCase() + ".json", content);
    }
}
