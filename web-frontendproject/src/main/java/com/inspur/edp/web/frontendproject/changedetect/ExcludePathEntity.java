/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * @Title: ExcludePathEntity
 * @Description: com.inspur.edp.web.frontendproject.compileactionextend
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/20 17:06
 */
@Data
public class ExcludePathEntity {
    private String parentDir;
    private File parentDirFile;
    private String name;

    public ExcludePathEntity(String parentDir, String name) {
        this.parentDir = parentDir;
        this.name = name;
        if (!StringUtils.isEmpty(this.parentDir)) {
            this.parentDirFile = new File(this.parentDir);
        }
    }
}
