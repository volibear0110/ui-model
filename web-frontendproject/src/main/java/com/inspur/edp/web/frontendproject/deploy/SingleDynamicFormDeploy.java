/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.deploy;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;

/**
 * @Title: SingleDynamicFormDeploy
 * @Description: com.inspur.edp.web.frontendproject.deploy 单个解析表单得部署过程
 *
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/12 16:39
 */
public class SingleDynamicFormDeploy {
    public  void deploy(String targetDeployBasePath, String projectName, String projectPath,FormDynamicParameter dynamicParameter){

        String strFormMetadataFilePath = FileUtility.combineOptional(projectPath, "metadata");
        String targetFormMetadataPath = FileUtility.combineOptional(targetDeployBasePath, "web", projectName.toLowerCase(), dynamicParameter.getFormCode().toLowerCase());

        if (FileUtility.exists(strFormMetadataFilePath)) {
            // 首先进行文件目录删除
            //FileUtility.deleteFolder(targetFormMetadataPath);
            // 进行文件拷贝
            FileUtility.copyFolder(strFormMetadataFilePath, targetFormMetadataPath);
        }

        //复制
        String strFormServicePath = FileUtility.combineOptional(projectPath, "src", FrontendProjectConstant.PROJECT_GENERATE_PATH_FOR_Dynamic, "services", dynamicParameter.getFormCode().toLowerCase());
        String customServicePath = FileUtility.combine(strFormServicePath, "services", JITEngineConstants.DistRollupPathName);
        if (FileUtility.exists(customServicePath)) {
            String targetFormMetadataServicePath = targetFormMetadataPath;
            // 删除目标文件目录
            //FileUtility.deleteFolder(targetFormMetadataServicePath);
            FileUtility.copyFolder(customServicePath, targetFormMetadataServicePath);
        }
    }
}
