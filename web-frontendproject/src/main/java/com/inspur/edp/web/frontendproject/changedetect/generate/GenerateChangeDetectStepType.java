/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.generate;

import com.inspur.edp.web.frontendproject.changedetect.generate.stepexecute.FileChangeStepExecuteImpl;
import com.inspur.edp.web.frontendproject.changedetect.generate.stepexecute.MetadataChangeStepExecuteImpl;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Title: GenerateChangeDetectType
 * @Description: 生成动作对应的变更检测步骤类型
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 10:55
 */
public enum GenerateChangeDetectStepType {
    /**
     * 文件变更检测步骤
     */
    FileChange(1,"文件变更检测") {
        @Override
        public ChangeDetectStepExecuteService getChangeDetectStep() {
            return new FileChangeStepExecuteImpl();
        }
    },
    /**
     * 元数据变更检测步骤
     */
    MetadataChange(2,"元数据变更检测") {
        @Override
        public ChangeDetectStepExecuteService getChangeDetectStep() {
            return new MetadataChangeStepExecuteImpl();
        }
    };

    private final int order;

    private final String description;

    GenerateChangeDetectStepType(int order, String description) {
        this.order = order;
        this.description = description;
    }

    public abstract ChangeDetectStepExecuteService getChangeDetectStep();

    /**
     * 获取编译变更检测步骤按顺序执行列表
     *
     * @return
     */
    public static List<GenerateChangeDetectStepType> getOrderedList() {
        return Arrays.stream(values()).sorted(Comparator.comparing(t -> t.order)).collect(Collectors.toList());
    }
}
