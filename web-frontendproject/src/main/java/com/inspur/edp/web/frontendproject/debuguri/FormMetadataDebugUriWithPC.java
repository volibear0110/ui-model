/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.debuguri;

import com.inspur.edp.web.common.entity.TerminalType;

/**
 * description: PC 表单调试Url地址
 *
 * @author Noah Guo
 * @date 2021/03/17
 */
class FormMetadataDebugUriWithPC extends AbstractFormMetadataDebugUri {
    public FormMetadataDebugUriWithPC() {
        super(TerminalType.PC);
    }

    @Override
    protected String generateRouteUri(String formMetadataRelativePath, String formMetadataId) {
        return getFormMetadataRouteUri(formMetadataRelativePath, formMetadataId);
    }
}
