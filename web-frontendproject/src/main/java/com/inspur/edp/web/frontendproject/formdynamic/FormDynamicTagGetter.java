/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.formdynamic;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * @Title: FormDynamicTagGeberator
 * @Description: com.inspur.edp.web.frontendproject.formdynamic
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/16 11:13
 */
public class FormDynamicTagGetter {
    /**
     * 从表单元数据中获取是否解析表单得标识
     *
     * @param gspMetadata 表单元数据
     * @return
     */
    public static boolean getDynamicFormTagWithGspMetadata(GspMetadata gspMetadata) {
        if (gspMetadata == null || gspMetadata.getContent() == null) {
            return false;
        }
        String formDom = ((FormMetadataContent) gspMetadata.getContent()).getContents().toString();
        // 反序列化表单元数据
        FormDOM json = SerializeUtility.getInstance().deserialize(formDom, FormDOM.class);
        return json.getOptions().isEnableFormJieXi();
    }
}
