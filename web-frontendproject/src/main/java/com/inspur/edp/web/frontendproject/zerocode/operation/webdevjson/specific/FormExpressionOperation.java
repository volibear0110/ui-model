/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.jitengine.expressions.ExpressionFormGenerator;
import com.inspur.edp.web.jitengine.expressions.ExpressionManifest;
import com.inspur.edp.web.jitengine.expressions.ExpressionManifestManager;
import com.inspur.edp.web.jitengine.expressions.ModuleFormExpressions;
import com.inspur.edp.web.jitengine.expressions.utility.ExpressionUtility;

public class FormExpressionOperation {
    public void save(FormDOM json, ZeroCodeFormParameter formParameter, String webDevPath, TerminalType terminalType) {
        // 定义表单关联的表达式
        ExpressionManifest expressionManifest = new ExpressionManifest();
        // 设置表达式 manifest.json 文件的code及其对应的文件名
        expressionManifest.setFormModuleCode(json.getModule().getCode());
        expressionManifest.setManifestJsonPath(ExpressionUtility.getExpressionManifestJsonPath(json.getModule().getCode()));

        ///获取表单对应的表达式
        ModuleFormExpressions moduleFormExpressions = ExpressionFormGenerator.generate(json, null, null);
        // 仅仅在包含表达式时才进行添加
        if (moduleFormExpressions != null && ListUtility.isNotEmpty(moduleFormExpressions.getExpressions())) {
            expressionManifest.getExpressions().add(moduleFormExpressions);
        }
        // 写入表单表达式json文件 仅在PC下构造表达式
        if (TerminalType.isPC(terminalType)) {
            ExpressionManifestManager.writeExpressionJson(expressionManifest, webDevPath, false);
        }
    }
}
