/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.step;

import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 表单变更检测
 * @author  noah
 */
public abstract class AbstractChangeDetectStepExecuteService implements ChangeDetectStepExecuteService {

    protected String webDevPathName = "webdev";

    protected String webSrcPathName = "src";

    protected String webSrcAppPathName = "app";

    protected String webSrcAppProjectsPathName = "projects";

    protected String angularJsonName = "angular.json";

    protected String packageJsonName = "package.json";

    protected String tsConfigJsonName = "tsconfig.json";

    protected String appConfigJsonName = "app.config.json";

    /**
     * 定义变更检测步骤执行的提示信息前缀
     *
     * @return 自定义的提示信息前缀
     */
    protected abstract String getPrefixTip();

    /**
     * 获取对应的元数据projectService
     *
     * @return
     */
    protected MetadataProjectService getMetadataProjectService() {
        return SpringBeanUtils.getBean(MetadataProjectService.class);
    }

    /**
     * 获取src/app 目录 路径
     *
     * @param context
     * @return
     */
    protected String getSrcAppPath(ChangeDetectContext context) {
        return FileUtility.combine(context.getProjectPath(), this.webSrcPathName, context.getTerminalType().getAppPathName());
    }


    /**
     * 获取src 目录 路径
     *
     * @param context
     * @return
     */
    protected String getSrcPath(ChangeDetectContext context) {
        return FileUtility.combine(context.getProjectPath(), this.webSrcPathName);
    }

    /**
     * 获取src/webdev 目录 路径
     *
     * @param context
     * @return
     */
    protected String getSrcWebDevPath(ChangeDetectContext context) {
        return FileUtility.combine(context.getProjectPath(), this.webSrcPathName, context.getTerminalType().getWebDevPathName());
    }

    /**
     * @param reason
     * @return
     */
    protected String generateReasonWithPrefix(String reason) {
        return this.getPrefixTip() + reason;
    }
}
