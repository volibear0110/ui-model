/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.debuguri.FormMetadataDebugUriService;
import com.inspur.edp.web.frontendproject.entity.ChosenFormList;
import com.inspur.edp.web.frontendproject.resolver.FormMetadataResolver;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class FrontendProjectService {

    private FrontendProjectService() {

    }

    public static FrontendProjectService getInstance() {
        return new FrontendProjectService();
    }

    /**
     * 解析表单元数据
     * 生成对应的json文件
     *
     * @param buildFormList 参与编译的表单code列表
     */
    public final void resolveFormMetadatas(String projectPath, ChosenFormList buildFormList) {
        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        FormMetadataResolver.resolveFormMetadatas(projectPath, buildFormList);
    }


    /**
     * 获取表单调试 url地址
     * @param formMetadataRelativePath
     * @param formMetadataId
     * @param formType
     * @return
     */
    public static String getFormMetadataDebugUri(String formMetadataRelativePath, String formMetadataId, String formType) {
        return FormMetadataDebugUriService.getFormMetadataDebugUri(formMetadataRelativePath, formMetadataId, formType);
    }
}
