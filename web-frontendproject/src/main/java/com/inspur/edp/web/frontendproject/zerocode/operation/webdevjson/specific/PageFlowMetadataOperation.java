/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.pageflow.metadata.entity.AdaptedPageFlowMetadataEntity;

public class PageFlowMetadataOperation {
    /**
     * 保存页面流元数据 json
     * @param webDevPath
     * @param pageFlowMetadataEntity
     * @param formMetadataName
     */
    public void save(String webDevPath, AdaptedPageFlowMetadataEntity pageFlowMetadataEntity, String formMetadataName) {
        String pageFlowContent = SerializeUtility.getInstance().serialize(pageFlowMetadataEntity);
        FileUtility.writeFile(webDevPath, formMetadataName.toLowerCase() + JITEngineConstants.ProjectRouteFileExtension, pageFlowContent);
    }
}
