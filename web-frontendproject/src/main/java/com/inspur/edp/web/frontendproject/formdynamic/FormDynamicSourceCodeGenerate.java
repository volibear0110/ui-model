/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.formdynamic;

import com.inspur.edp.web.frontendproject.entity.ChosenFormList;
import com.inspur.edp.web.frontendproject.entity.FrontendProjectGenerateParameter;
import com.inspur.edp.web.frontendproject.generate.FrontendProjectGenerate;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;

/**
 * 解析表单代码生成
 * @author guozhiqi
 */
public class FormDynamicSourceCodeGenerate {
    /**
     * 执行代码生成
     * @param jieXiParameter
     */
    public  static  void generate(FormDynamicParameter jieXiParameter, ChosenFormList buildFormList){
        FrontendProjectGenerateParameter generateParameter = new FrontendProjectGenerateParameter();
        // 如果是单个表单 那么强制执行解析  如果是工程级别 那么按照表单具体的参数配置进行
        generateParameter.setForceUseJieXi(jieXiParameter.isUseSingleForm());
        generateParameter.setProjectPath(jieXiParameter.getProjectPath());
        generateParameter.setBuildFormList(buildFormList);

        FrontendProjectGenerate.generateFrontendProject(generateParameter);
    }
}
