/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.generate;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.environment.EnvironmentException;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectUtility;
import com.inspur.edp.web.frontendproject.customservice.SourceServicePathGenerator;
import com.inspur.edp.web.frontendproject.entity.FrontendProjectGenerateParameter;
import com.inspur.edp.web.frontendproject.metadata.FormMetadataManager;
import com.inspur.edp.web.jitengine.JITEngineManager;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 前端工程代码生成
 *
 * @author guozhiqi
 */
public class FrontendProjectGenerate {
    public static void generateFrontendProject(String projectPath) {
        FrontendProjectGenerateParameter generateParameter = new FrontendProjectGenerateParameter();
        generateParameter.setProjectPath(projectPath);

        generateFrontendProject(generateParameter);
    }

    public static void generateFrontendProject(FrontendProjectGenerateParameter generateParameter) {
        String projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(generateParameter.getProjectPath());

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }
        // 调整工程路径参数
        generateParameter.setProjectPath(projectPath);

        try {

            TerminalType.rescure(terminalType -> {
                if (FormMetadataManager.checkFormMetadataExists(projectPath, terminalType, generateParameter.getBuildFormList(), generateParameter.isForceUseJieXi(), true)) {
                    generateFrontendProject(generateParameter, terminalType);
                }
            });

        } catch (EnvironmentException ex) {
            throw new WebCustomException(ex.getMessage(), ex);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Generating a Frontend Project. Current Project Path is: %1$s。%2$s", projectPath, exception.getMessage()), exception);
        }
    }


    public static void generateFrontendProject(String projectPath, TerminalType terminalType) {
        FrontendProjectGenerateParameter generateParameter = new FrontendProjectGenerateParameter();
        generateParameter.setProjectPath(projectPath);
        generateFrontendProject(generateParameter, terminalType);
    }

    /**
     * 基于解析后的表单代码生成前端工程
     */
    private static void generateFrontendProject(FrontendProjectGenerateParameter generateParameter, TerminalType terminalType) {
        boolean isJieXiForm = generateParameter != null && generateParameter.isForceUseJieXi();
        String devRootPath = MetadataUtility.getInstance().getDevRootPath();
        if (!FileUtility.isAbsolute(devRootPath)) {
            devRootPath = FileUtility.getAbsolutePathHead(devRootPath) + devRootPath;
        }
        JITEngineManager.generateFrontendProject(generateParameter, devRootPath, terminalType);

        String targetServiceProductPath = SourceServicePathGenerator.getTargetServiceProductPath(generateParameter.getProjectPath(), terminalType, isJieXiForm);

        String sourceServiceProductPath = SourceServicePathGenerator.getSourceServiceProductPath(generateParameter.getProjectPath(), terminalType, generateParameter.isForceUseJieXi());
        boolean isPathExists = FileUtility.exists(sourceServiceProductPath);
        if (isPathExists) {
            FileUtility.copyFolder(sourceServiceProductPath, targetServiceProductPath);
        }
    }
}
