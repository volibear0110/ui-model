/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.deploy;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerPathGenerator;

/**
 * 移动表单 脚本部署
 * @author  noah
 */
 class MobileDeployOperation extends AbstractDeployOperation implements IDeployOperation {
    public MobileDeployOperation() {
        super(TerminalType.MOBILE);
    }

    @Override
    public void deploy(ZeroCodeParameter zeroCodeParameter) {
        if (zeroCodeParameter.hasFormParameter(TerminalType.MOBILE)) {
            // 编译后的目标目录
            String distRollupPath = getAppDistRollupPath(zeroCodeParameter.getAbsoluteBasePath());

            String deployPath = FileUtility.combineOptional(zeroCodeParameter.getServerPath(), "web", zeroCodeParameter.getServiceUnitPath().toLowerCase(), "mob");
            FileUtility.copyFolder(distRollupPath, deployPath);

            if (FileUtility.exists(deployPath)) {
                String strDeployPathWithProject = FileUtility.combine(deployPath, zeroCodeParameter.getProjectName().toLowerCase());
                String strLocalServerPath = LocalServerPathGenerator.getNewInstance(false).getLocalServerWebPath();

                zeroCodeParameter.getFormParameters().stream().filter(ZeroCodeFormParameter::isMobile).forEach((zeroCodeFormParameter) -> {
                    String metaDataId = zeroCodeParameter.getProjectName().toLowerCase();
                    if (zeroCodeFormParameter != null) {
                        metaDataId = zeroCodeFormParameter.getMetadata().getHeader().getId();
                    }


                    PublishScriptRequest publishScriptRequest = new PublishScriptRequest();
                    publishScriptRequest.setAbsoluteBaseDirectory(strDeployPathWithProject);
                    // 为了和设计时传参保持一致  将参数调整为小写形式
                    publishScriptRequest.setProjectName(zeroCodeParameter.getProjectName().toLowerCase());


                    String strRelativePath = FileUtility.getRelativePath(strLocalServerPath, strDeployPathWithProject, true);
                    publishScriptRequest.setProjectRelativePath(strRelativePath);
                    publishScriptRequest.setMetaDataId(metaDataId);
                    publishScriptRequest.setFormCode(zeroCodeFormParameter.getCode());
                    publishScriptRequest.setZeroCodeMobileForm(true);

                    this.getScriptCacheServiceInstance().publishScriptWithDirectory(publishScriptRequest);
                });

            }
        }
    }
}
