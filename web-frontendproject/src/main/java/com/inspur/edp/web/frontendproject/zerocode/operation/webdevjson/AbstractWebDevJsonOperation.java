/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;
import com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific.MetaDataOperationManager;
import com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific.PageFlowMetadataOperation;
import com.inspur.edp.web.pageflow.metadata.entity.AdaptedPage;
import com.inspur.edp.web.pageflow.metadata.entity.AdaptedPageFlowMetadataEntity;
import com.inspur.edp.web.pageflow.metadata.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * webdev json 文件生成 基类
 *
 * @author guozhiqi
 */
  abstract class AbstractWebDevJsonOperation implements IWevDevJsonOperation {

    private final TerminalType terminalType;

    public AbstractWebDevJsonOperation(TerminalType terminalType) {
        this.terminalType = terminalType;
    }

    protected TerminalType getTerminalType() {
        return this.terminalType;
    }

    /**
     * 获取对应的 webdev 路径
     *
     * @return
     */
    private String getWebdevPath(ZeroCodeParameter zeroCodeParameter) {
        return WebDevJsonPathGenerator.generate(zeroCodeParameter, this.terminalType);
    }

    /**
     * 保存json文件到指定目录
     *
     * @param formParameterList
     * @param zeroCodeParameter
     */
    protected void saveJson(List<ZeroCodeFormParameter> formParameterList, ZeroCodeParameter zeroCodeParameter) {
        if (ListUtility.isEmpty(formParameterList)) {
            WebLogger.Instance.info("零代码 webdev json 文件生成，" + this.terminalType.getMetadataType() + "类型表单为空");
            return;
        }

        Project pageFlowProject = new Project();
        pageFlowProject.setName(zeroCodeParameter.getProjectName());

        AdaptedPageFlowMetadataEntity pageFlowMetadataEntity = new AdaptedPageFlowMetadataEntity();
        pageFlowMetadataEntity.setProject(pageFlowProject);
        pageFlowMetadataEntity.setPages(new ArrayList<>());

        String webDevPath = this.getWebdevPath(zeroCodeParameter);

        formParameterList.forEach(t -> {
            List<AdaptedPage> pageList = pageFlowMetadataEntity.getPages();

            AdaptedPage adaptedPage = new AdaptedPage();
            MetaDataOperationManager.save(t, webDevPath, adaptedPage);

            pageList.add(adaptedPage);

            t.getFormFormatList().forEach(formFormatParam -> {
                AdaptedPage adaptedFormatPage = new AdaptedPage();
                MetaDataOperationManager.saveFormFormat(t, formFormatParam, webDevPath, adaptedFormatPage);
                pageList.add(adaptedFormatPage);
            });
        });

        // 保存页面流元数据
        PageFlowMetadataOperation pageFlowMetadataOperation = new PageFlowMetadataOperation();
        pageFlowMetadataOperation.save(webDevPath, pageFlowMetadataEntity, zeroCodeParameter.getProjectName());
    }
}
