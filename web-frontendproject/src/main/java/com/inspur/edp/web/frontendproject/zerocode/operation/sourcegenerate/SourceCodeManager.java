/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.sourcegenerate;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;

public class SourceCodeManager {
    public static void generateSourceCode(ZeroCodeParameter zeroCodeParameter) {
        TerminalType.rescure(t -> {
            if (zeroCodeParameter.hasFormParameter(t)) {
                ISourceCodeOperation sourceCodeOperation = getSourceCodeOperation(t);
                sourceCodeOperation.generate(zeroCodeParameter);
            }
        });
    }

    /**
     * 依据 terminalType 获取对应的操作实例
     *
     * @param terminalType
     * @return
     */
    private static ISourceCodeOperation getSourceCodeOperation(TerminalType terminalType) {
        if (TerminalType.isPC(terminalType)) {
            return new SourceCodePCOperation();
        }
        return new SourceCodeMobileOperation();
    }

}
