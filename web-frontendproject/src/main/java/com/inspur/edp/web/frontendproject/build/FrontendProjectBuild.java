/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.build;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.entity.ResultCode;
import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectCompiler;
import com.inspur.edp.web.frontendproject.FrontendProjectUtility;
import com.inspur.edp.web.frontendproject.NpmPackageCheckUpdate;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @author guozhiqi
 */
public class FrontendProjectBuild {
    public static ResultMessage<String> buildFrontendProject(String projectPath, boolean isJieXiForm) {
        // 编译前操作  判定是否需要执行更新操作
        NpmPackageResponse npmPackageResponse = NpmPackageCheckUpdate.check();
        if (!npmPackageResponse.isSuccess()) {
            return ResultCode.custom(2000, npmPackageResponse.getErrorMessage());
        }

        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        try {
            FrontendProjectCompiler frontendProjectCompiler = new FrontendProjectCompiler();
            frontendProjectCompiler.buildFrontendProject(projectPath, isJieXiForm);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Building a Frontend. Current Project Path is: %1$s", projectPath), exception);
        }
        return ResultCode.success();
    }

    public static void buildFrontendProjectForBabel(String projectPath) {
        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        //获取webdev路径 包含根路径
        try {
            FrontendProjectCompiler frontendProjectCompiler = new FrontendProjectCompiler();
            frontendProjectCompiler.buildFrontendProjectForBabel(projectPath);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Building a Frontend. Current Project Path is: %1$s", projectPath), exception);
        }
    }
}
