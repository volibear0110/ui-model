/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.context;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteType;
import lombok.Data;

/**
 * @Title: ChangeDetectContext
 * @Description: 变更检测上下文参数
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 16:30
 */
@Data
public class ChangeDetectContext {
    /**
     * 变更检测的工程路径 参数
     */
    private String projectPath;

    /**
     * 变更检测关联的表单类型
     */
    private TerminalType terminalType = TerminalType.PC;

    /**
     * 设置变更检测类型  默认为生成动作
     */
    private ChangeDetectExecuteType executeType = ChangeDetectExecuteType.Generate;
}
