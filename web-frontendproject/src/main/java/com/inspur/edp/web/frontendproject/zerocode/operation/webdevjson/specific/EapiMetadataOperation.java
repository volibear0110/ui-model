/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson.specific;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadataanalysis.EapiAnalysis;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormRefMetadataParameter;

import java.util.Optional;

class EapiMetadataOperation {
    public void save(FormDOM json, String formMetadataName, String webDevPath, ZeroCodeFormParameter formParameter) {
        EapiAnalysis eapiAnalysis = new EapiAnalysis(ExecuteEnvironment.Runtime, false);
        eapiAnalysis.resolveEapi(json, formMetadataName, webDevPath, null, null, (resolveEapiParameter) -> {
            ZeroCodeFormRefMetadataParameter formRefMetadataParameter = formParameter.getSpecialFormRefMetaData(resolveEapiParameter.getMetadataManagerParameter().getId(),  MetadataTypeEnum.Eapi);
            return Optional.ofNullable(formRefMetadataParameter.getMetadata());
        });
    }
}
