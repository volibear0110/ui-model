/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeFormParameter;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;

import java.util.List;
import java.util.stream.Collectors;

class WebDevPCJsonOperation extends AbstractWebDevJsonOperation implements IWevDevJsonOperation {

    WebDevPCJsonOperation() {
        super(TerminalType.PC);
    }

    @Override
    public void save(ZeroCodeParameter zeroCodeParameter) {
        List<ZeroCodeFormParameter> zeroCodeFormParameterList = zeroCodeParameter.getFormParameters().stream().filter(t -> !t.isMobile()).collect(Collectors.toList());
        // 保存PC 表单元数据
        this.saveJson(zeroCodeFormParameterList, zeroCodeParameter);
    }
}
