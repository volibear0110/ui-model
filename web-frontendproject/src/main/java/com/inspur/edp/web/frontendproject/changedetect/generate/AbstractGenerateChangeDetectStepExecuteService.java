/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.generate;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;
import com.inspur.edp.web.frontendproject.changedetect.step.AbstractChangeDetectStepExecuteService;

/**
 * @Title: AbstractGenerateChangeDetectStepExecuteService
 * @Description: 生成步骤对应的步骤执行
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 19:15
 */
public abstract class AbstractGenerateChangeDetectStepExecuteService extends AbstractChangeDetectStepExecuteService {

    @Override
    protected String getPrefixTip() {
        return "Web依赖变更检测，生成：";
    }

    protected String getAppConfigJsonPath(ChangeDetectContext context) {
        return FileUtility.combine(context.getProjectPath(), this.appConfigJsonName);
    }
}
