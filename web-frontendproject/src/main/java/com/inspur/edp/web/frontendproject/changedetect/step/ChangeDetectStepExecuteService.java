/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.step;

import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;

/**
 * @Title: ChangeDetectStepExecutor
 * @Description: 变更检测步骤执行接口
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 11:40
 */
public interface ChangeDetectStepExecuteService {
    /**
     * 步骤执行器参数
     * @param context 变更检测上下文参数
     * @return
     */
    ChangeDetectStepExecuteResult execute(ChangeDetectContext context);

    /**
     * 针对元数据的变更回写动作
     * @param context
     * @return
     */
    ChangeDetectStepExecuteResult updateChangeset(ChangeDetectContext context);

}
