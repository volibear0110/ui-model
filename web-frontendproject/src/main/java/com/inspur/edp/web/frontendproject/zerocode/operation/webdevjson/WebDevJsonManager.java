/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.webdevjson;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;

/**
 * 零代码 webdev 目录下 json 文件构造
 *
 * @author noah
 */
public class WebDevJsonManager {
    public static void saveWebDevJson(ZeroCodeParameter zeroCodeParameter) {

        TerminalType.rescure((t) -> {
            if (zeroCodeParameter.hasFormParameter(t)) {
                IWevDevJsonOperation webDevJsonOperationInstance = getWebDevJsonOperationInstance(t);
                webDevJsonOperationInstance.save(zeroCodeParameter);
            }
        });
    }

    public static String getWebDevPath(ZeroCodeParameter zeroCodeParameter, TerminalType terminalType) {
        return WebDevJsonPathGenerator.generate(zeroCodeParameter, terminalType);
    }


    private static IWevDevJsonOperation getWebDevJsonOperationInstance(TerminalType terminalType) {
        if (terminalType == TerminalType.MOBILE) {
            return new WebDevMobileJsonOperation();
        }
        return new WebDevPCJsonOperation();
    }
}
