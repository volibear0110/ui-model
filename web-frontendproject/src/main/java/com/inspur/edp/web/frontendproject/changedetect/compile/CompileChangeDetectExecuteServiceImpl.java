/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.compile;

import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteResult;
import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteService;
import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteType;
import com.inspur.edp.web.frontendproject.changedetect.context.ChangeDetectContext;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteResult;
import com.inspur.edp.web.frontendproject.changedetect.step.ChangeDetectStepExecuteService;

import java.util.List;

/**
 * @Title: CompileChangeDetectExecuteService
 * @Description: 变更检测  编译动作实现
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 20:11
 */
public class CompileChangeDetectExecuteServiceImpl implements ChangeDetectExecuteService {
    /**
     * 变更检测
     *
     * @param detectContext 变更检测上下文参数
     * @return
     */
    @Override
    public ChangeDetectExecuteResult execute(ChangeDetectContext detectContext) {
        List<CompileChangeDetectType> orderedChangeDetectTypeList = CompileChangeDetectType.getOrderedList();

        ChangeDetectStepExecuteResult stepExecuteResult = ChangeDetectStepExecuteResult.getPassResult(ChangeDetectExecuteType.Compile);
        for (CompileChangeDetectType t : orderedChangeDetectTypeList) {
            ChangeDetectStepExecuteService executeService = t.getChangeDetectStep();
            stepExecuteResult = executeService.execute(detectContext);
            if (!stepExecuteResult.isPass()) {
                break;
            }
        }

        if (stepExecuteResult.isPass()) {
            return ChangeDetectExecuteResult.getPassResult(ChangeDetectExecuteType.Compile);
        } else {
            return ChangeDetectExecuteResult.getUnPassResult(ChangeDetectExecuteType.Compile, stepExecuteResult.getReason());
        }

    }

    /**
     * 更新对应的变更记录
     *
     * @param detectContext
     */
    @Override
    public void updateChangeset(ChangeDetectContext detectContext) {
        List<CompileChangeDetectType> orderedChangeDetectTypeList = CompileChangeDetectType.getOrderedList();

        for (CompileChangeDetectType t : orderedChangeDetectTypeList) {
            ChangeDetectStepExecuteService executeService = t.getChangeDetectStep();
            executeService.updateChangeset(detectContext);
        }
    }
}
