/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.deploy;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerPathGenerator;

/**
 * PC 表单 脚本部署
 *
 * @author noah
 */
 class PCDeployOperation extends AbstractDeployOperation implements IDeployOperation {
    public PCDeployOperation() {
        super(TerminalType.PC);
    }

    @Override
    public void deploy(ZeroCodeParameter zeroCodeParameter) {
        if (zeroCodeParameter.hasFormParameter(TerminalType.PC)) {
            // 编译后的目标目录
            String distRollupPath = getAppDistRollupPath(zeroCodeParameter.getAbsoluteBasePath());

            String deployPath = FileUtility.combineOptional(zeroCodeParameter.getServerPath(), "web", zeroCodeParameter.getServiceUnitPath().toLowerCase(), "web");
            FileUtility.copyFolder(distRollupPath, deployPath);
            if (FileUtility.exists(deployPath)) {

                //version.json
                publishWithSingleFile(deployPath, zeroCodeParameter, "version.json");
                //index.html
                publishWithSingleFile(deployPath, zeroCodeParameter, "index.html");
                //polyfills.js
                publishWithSingleFile(deployPath, zeroCodeParameter, "polyfills.js");
                //main.js
                publishWithSingleFile(deployPath, zeroCodeParameter, "main.js");

                String strLocalServerPath = LocalServerPathGenerator.getNewInstance(false).getLocalServerWebPath();
                String strDeployPathWithProject = FileUtility.combine(deployPath, zeroCodeParameter.getProjectName().toLowerCase());
                zeroCodeParameter.getFormParameters().stream().filter(t -> !t.isMobile()).forEach(t -> {

                    PublishScriptRequest publishScriptRequest = new PublishScriptRequest();
                    publishScriptRequest.setAbsoluteBaseDirectory(strDeployPathWithProject);
                    // 为了和设计时传参保持一致  将参数调整为小写形式
                    publishScriptRequest.setProjectName(zeroCodeParameter.getProjectName().toLowerCase());
                    publishScriptRequest.setFormCode(t.getCode());

                    String strRelativePath = FileUtility.getRelativePath(strLocalServerPath, strDeployPathWithProject, true);
                    publishScriptRequest.setProjectRelativePath(strRelativePath);

                    // 由于零代码不存在对应的元数据 因此设置元数据为工程名称 作为一个临时参数值
                    publishScriptRequest.setMetaDataId(t.getMetadata().getHeader().getId());
                    this.getScriptCacheServiceInstance().publishScriptWithSingleFileName(publishScriptRequest, t.getCode().toLowerCase(), null);
                });
            }
        }
    }
}
