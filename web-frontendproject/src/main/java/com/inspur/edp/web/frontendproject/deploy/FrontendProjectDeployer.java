/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.deploy;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectUtility;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 前端工程部署
 *
 * @author guozhiqi
 */
public class FrontendProjectDeployer {
    public static void deployFrontendProject(String projectPath) {
        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        try {
            com.inspur.edp.web.frontendproject.FrontendProjectDeployer frontendProjectDeployer = new com.inspur.edp.web.frontendproject.FrontendProjectDeployer();
            frontendProjectDeployer.deploy(projectPath);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Deploying a Frontend. Current Project Path is: %1$s", projectPath), exception);
        }
    }

    /**
     * 解析表单的部署
     *
     * @param dynamicParameter
     */
    public static void deployFrontendProjectForJieXi(FormDynamicParameter dynamicParameter) {
        String projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(dynamicParameter.getProjectPath());

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        try {
            com.inspur.edp.web.frontendproject.FrontendProjectDeployer frontendProjectDeployer = new com.inspur.edp.web.frontendproject.FrontendProjectDeployer();
            frontendProjectDeployer.deployForJieXi(projectPath, dynamicParameter);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Deploying a Frontend. Current Project Path is: %1$s", projectPath), exception);
        }
    }

    /**
     * babel 表单的部署
     *
     * @param projectPath
     */
    public static void deployFrontendProjectForBabel(String projectPath) {
        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        try {
            com.inspur.edp.web.frontendproject.FrontendProjectDeployer frontendProjectDeployer = new com.inspur.edp.web.frontendproject.FrontendProjectDeployer();
            frontendProjectDeployer.deployForBabel(projectPath);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Deploying a Frontend. Current Project Path is: %1$s", projectPath), exception);
        }
    }
}
