/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.formdynamic;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;

/**
 * 表单解析参数校验
 *
 * @author guozhiqi
 */
public class FormDynamicParameterValidator {
    public static void validate(FormDynamicParameter dynamicParameter) {

        if (StringUtility.isNullOrEmpty(dynamicParameter.getProjectPath())) {
            throw new WebCustomException("表单解析工程路径不能为空，请设置！");
        }

        if (dynamicParameter.isUseSingleForm() && StringUtility.isNullOrEmpty(dynamicParameter.getFormCode())) {
            throw new WebCustomException("已启用单个表单解析，请设置表单编号");
        }

        // 如果不是单表单解析 且设置了formCode
        if (!dynamicParameter.isUseSingleForm() && !StringUtility.isNullOrEmpty(dynamicParameter.getFormCode())) {
            throw new WebCustomException("使用工程级别表单解析，请勿设置表单编号");
        }
    }
}
