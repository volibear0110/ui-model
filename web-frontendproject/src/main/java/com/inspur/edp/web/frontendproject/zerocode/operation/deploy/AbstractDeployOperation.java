/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode.operation.deploy;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.frontendproject.zerocode.ZeroCodeParameter;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.service.ScriptCacheService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerPathGenerator;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

abstract class AbstractDeployOperation implements IDeployOperation {
    private final TerminalType terminalType;
    private final ScriptCacheService scriptCacheServiceInstance;

    AbstractDeployOperation(TerminalType terminalType) {
        this.terminalType = terminalType;
        this.scriptCacheServiceInstance = SpringBeanUtils.getBean(ScriptCacheService.class);
    }

    protected ScriptCacheService getScriptCacheServiceInstance() {
        return this.scriptCacheServiceInstance;
    }

    /**
     * 获取生成文件代码的路径
     *
     * @param absolutePath
     * @return
     */
    protected String getAppDistRollupPath(String absolutePath) {
        if (TerminalType.isPC(terminalType)) {
            return FileUtility.getPlatformIndependentPath(FileUtility.combineOptional(absolutePath, "src", "app", JITEngineConstants.DistRollupPathName));
        }
        return FileUtility.getPlatformIndependentPath(FileUtility.combineOptional(absolutePath, "src", "mobileapp", JITEngineConstants.DistRollupPathName));
    }

    protected void publishWithSingleFile(String deployPath, ZeroCodeParameter zeroCodeParameter, String fileName) {
        String strDeployPathWithProject = FileUtility.combine(deployPath, zeroCodeParameter.getProjectName().toLowerCase());
        PublishScriptRequest publishScriptRequest = new PublishScriptRequest();
        publishScriptRequest.setAbsoluteBaseDirectory(strDeployPathWithProject);
        // 为了和设计时传参保持一致  将参数调整为小写形式
        publishScriptRequest.setProjectName(zeroCodeParameter.getProjectName().toLowerCase());

        String strLocalServerPath = LocalServerPathGenerator.getNewInstance(false).getLocalServerWebPath();
        String strRelativePath = FileUtility.getRelativePath(strLocalServerPath, strDeployPathWithProject, true);
        publishScriptRequest.setProjectRelativePath(strRelativePath);
        // 无需更新元数据版本
        //publishScriptRequest.setUpdateMetadataVersion(false);
        // 由于零代码不存在对应的元数据 因此设置元数据为工程名称 作为一个临时参数值
        publishScriptRequest.setMetaDataId(zeroCodeParameter.getProjectName().toLowerCase());
        this.scriptCacheServiceInstance.publishScriptWithSingleFileName(publishScriptRequest, "", fileName);
    }
}
