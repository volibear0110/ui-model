/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect.step;

import com.inspur.edp.web.frontendproject.changedetect.ChangeDetectExecuteType;

/**
 * @Title: ChangeDetectStepExecuteResult
 * @Description: 变更检测具体步骤执行结果
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 11:41
 */
public class ChangeDetectStepExecuteResult {
    /**
     * 是否执行验证通过
     */
    private boolean pass;
    /**
     * 如果未验证通过 具体的未通过原因
     */
    private String reason;

    /**
     * 当前执行变更检测类型
     */
    private ChangeDetectExecuteType currentExecuteType = ChangeDetectExecuteType.Generate;

    public boolean isPass() {
        return pass;
    }

    public void setPass(boolean pass) {
        this.pass = pass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ChangeDetectExecuteType getCurrentExecuteType() {
        return currentExecuteType;
    }

    public void setCurrentExecuteType(ChangeDetectExecuteType currentExecuteType) {
        this.currentExecuteType = currentExecuteType;
    }

    /**
     * 私有 无参构造方法  避免直接构造
     */
    private ChangeDetectStepExecuteResult() {
    }

    private ChangeDetectStepExecuteResult(ChangeDetectExecuteType currentExecuteType, boolean isPass, String unPassReason) {
        this.currentExecuteType = currentExecuteType;
        this.pass = isPass;
        this.reason = unPassReason;
    }

    /**
     * @param currentExecuteType
     * @return
     */
    public static ChangeDetectStepExecuteResult getPassResult(ChangeDetectExecuteType currentExecuteType) {
        return new ChangeDetectStepExecuteResult(currentExecuteType, true, null);
    }

    public static ChangeDetectStepExecuteResult getUnPassResult(ChangeDetectExecuteType currentExecuteType, String reason) {
        return new ChangeDetectStepExecuteResult(currentExecuteType, false, reason);
    }

    /**
     * 设置未通过原因 并同步设置状态为未通过
     * @param reason
     */
    public void setUnPassReason(String reason) {
        this.reason = reason;
        this.pass = false;
    }

}
