/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.changedetect;

import com.inspur.edp.web.frontendproject.changedetect.compile.CompileChangeDetectExecuteServiceImpl;
import com.inspur.edp.web.frontendproject.changedetect.generate.GenerateChangeDetectExecuteServiceImpl;

/**
 * @Title: ChangeDetectExecuteType
 * @Description: 变更检测类型定义  用于区分是生成还是编译动作
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/6/21 11:00
 */
public enum ChangeDetectExecuteType {
    /**
     * 变更检测执行类型 生成动作
     */
    Generate("generate", "生成检测") {
        /**
         * 获取对应的执行服务实例
         *
         * @return
         */
        @Override
        public ChangeDetectExecuteService getExecuteService() {
            return new GenerateChangeDetectExecuteServiceImpl();
        }
    },

    /**
     * 变更检测执行类型  编译动作
     */
    Compile("compile", "编译检测") {
        /**
         * 获取对应的执行服务实例
         *
         * @return
         */
        @Override
        public ChangeDetectExecuteService getExecuteService() {
            return new CompileChangeDetectExecuteServiceImpl();
        }
    };

    private final String value;
    private final String description;

    ChangeDetectExecuteType(String value, String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * 获取对应的执行服务实例
     *
     * @return
     */
    public abstract ChangeDetectExecuteService getExecuteService();

    /**
     * 获取对应变更检测类型定义的实际参数值
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * 获取对应变更检测类型定义的描述信息
     *
     * @return
     */
    public String getDescription() {
        return description;
    }
}
