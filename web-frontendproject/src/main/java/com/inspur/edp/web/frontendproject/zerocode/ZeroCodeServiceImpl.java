/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.web.frontendproject.zerocode.operation.ZeroCodeManager;

/**
 * 零代码生成、编译
 * @author noah
 */
public class ZeroCodeServiceImpl implements ZeroCodeService {


    /**
     * 解析表单元数据  生成对应的webdev  json文件   生成ts代码依赖
     *
     * @param zeroCodeParameter
     */
    @Override
    public void resolveMetadataAndGenerateSource(ZeroCodeParameter zeroCodeParameter) {
        prepareOperate(zeroCodeParameter);

        ZeroCodeManager.getInstance().saveZeroCodeParameterMetadataIntoJson(zeroCodeParameter);
    }

    /**
     * 编译生成后的代码
     *
     * @param zeroCodeParameter
     */
    @Override
    public void build(ZeroCodeParameter zeroCodeParameter) {
        prepareOperate(zeroCodeParameter);

        ZeroCodeManager.getInstance().build(zeroCodeParameter);
    }

    /**
     * 部署编译后的交付物
     *
     * @param zeroCodeParameter
     */
    @Override
    public void deploy(ZeroCodeParameter zeroCodeParameter) {
        prepareOperate(zeroCodeParameter);

        ZeroCodeManager.getInstance().deploy(zeroCodeParameter);
    }

    /**
     * 解析、编译、部署
     *
     * @param zeroCodeParameter
     */
    @Override
    public void resolveBuildAndDeploy(ZeroCodeParameter zeroCodeParameter) {
        prepareOperate(zeroCodeParameter);

        ZeroCodeManager.getInstance().saveZeroCodeParameterMetadataIntoJson(zeroCodeParameter);
        // 生成源代码
        ZeroCodeManager.getInstance().generateSource(zeroCodeParameter);
        // 编译源代码
        ZeroCodeManager.getInstance().build(zeroCodeParameter);
        // 部署源代码
        ZeroCodeManager.getInstance().deploy(zeroCodeParameter);
    }

    /**
     * 执行前操作
     * @param zeroCodeParameter
     */
    private  void prepareOperate(ZeroCodeParameter zeroCodeParameter){
        ZeroCodeParameterInitializer.initialize(zeroCodeParameter);
        // 入参验证
        ZeroCodeParameterValidator.validate(zeroCodeParameter);

    }
}
