/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.debuguri;

/**
 * description: 表单调试地址
 *
 * @author Noah Guo
 * @date 2021/03/17
 */
public class FormMetadataDebugUriService {
    /**
     * 获取表单调试地址
     *
     * @param formMetadataRelativePath 表单所在工程路径
     * @param formMetadataId           表单元数据id
     * @param formType                 表单类型
     * @return
     */
    public static String getFormMetadataDebugUri(String formMetadataRelativePath, String formMetadataId, String formType) {
        FormMetadataDebugUri formMetadataDebugUri = getFormDebugUri(formType);
        return formMetadataDebugUri.getFormMetadataDebugUri(formMetadataRelativePath, formMetadataId);
    }

    /**
     * 根据表单类型获取对应的执行实例
     *
     * @param formType
     * @return
     */
    private static FormMetadataDebugUri getFormDebugUri(String formType) {
        FormMetadataDebugUri debugUri;
        switch (formType) {
            case "mobile":
                debugUri = new FormMetadataDebugUriWithMobile();
                break;
            case "pc":
            default:
                debugUri = new FormMetadataDebugUriWithPC();
                break;
        }
        return debugUri;
    }

}
