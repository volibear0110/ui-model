/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.deploy;

import com.inspur.edp.lcm.metadata.api.entity.ExtractContext;
import com.inspur.edp.lcm.metadata.api.service.PackageGenerateService;
import com.inspur.edp.lcm.metadata.api.service.ProjectExtendService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;


/**
 * 前端元数据部署
 *
 * @author noah
 */
public class FrontMetadataDeployer {
    /**
     * 元数据部署
     * @param projectPath
     */
    public static void deploy(String projectPath) {

        PackageGenerateService packageGenerateService = SpringBeanUtils.getBean(PackageGenerateService.class);
        packageGenerateService.generatePackage(projectPath);

        ProjectExtendService projectExtendService = SpringBeanUtils.getBean(ProjectExtendService.class);
        ExtractContext extractContext = new ExtractContext();
        extractContext.setProjectPath(projectPath);

        projectExtendService.extract(projectPath);

        projectExtendService.migration(projectPath);
    }
}
