package com.inspur.edp.web.frontendproject.constant;

public class I18nMsgConstant {
    public final static String WEB_FRONT_PROJECT_MSG_0001 = "WEB_FRONT_PROJECT_MSG_0001";
    public final static String WEB_FRONT_PROJECT_MSG_0002 = "WEB_FRONT_PROJECT_MSG_0002";
    public final static String WEB_FRONT_PROJECT_MSG_0003 = "WEB_FRONT_PROJECT_MSG_0003";
}
