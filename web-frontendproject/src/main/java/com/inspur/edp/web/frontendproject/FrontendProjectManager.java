/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject;

import com.inspur.edp.web.common.entity.ResultCode;
import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.frontendproject.build.FrontendProjectBuild;
import com.inspur.edp.web.frontendproject.deploy.FrontendProjectDeployer;
import com.inspur.edp.web.frontendproject.entity.ChosenFormList;
import com.inspur.edp.web.frontendproject.formdynamic.FormDynamicMetadataResolver;
import com.inspur.edp.web.frontendproject.formdynamic.FormDynamicParameterValidator;
import com.inspur.edp.web.frontendproject.formdynamic.FormDynamicSourceCodeGenerate;
import com.inspur.edp.web.frontendproject.generate.FrontendProjectGenerate;
import com.inspur.edp.web.frontendproject.generate.FrontendProjectGenerateForBabel;
import com.inspur.edp.web.frontendproject.webservice.FormDynamicParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;

/**
 * 前端工程
 * @author  noah
 */
public class FrontendProjectManager {
    private FrontendProjectManager() {

    }

    /**
     * 该工程的调用频率极低 无需单例来占用启动及内存空间，及时释放才是王道
     * @return
     */
    public static FrontendProjectManager getInstance() {
        return new FrontendProjectManager();
    }


    /**
     * 解析表单元数据
     *
     * @param projectPath
     */
    public final void resolveFormMetadatas(String projectPath) {
        FrontendProjectService.getInstance().resolveFormMetadatas(projectPath, ChosenFormList.getNewInstance());
    }

    public final void ResolveAndGenerateFrontendProject(String projectPath) {
        FrontendProjectService.getInstance().resolveFormMetadatas(projectPath, ChosenFormList.getNewInstance());
        FrontendProjectGenerate.generateFrontendProject(projectPath);
    }

    /**
     * 解析、生成、编译前端工程
     *
     * @param projectPath
     * @return
     */
    public final ResultMessage<String> resolveAndGenerateAndBuildFrontendProject(String projectPath) {

        // 编译前操作  判定是否需要执行更新操作
        NpmPackageResponse npmPackageResponse = NpmPackageCheckUpdate.check();
        if (!npmPackageResponse.isSuccess()) {
            return ResultCode.custom(2000, npmPackageResponse.getErrorMessage());
        }

        // 解析元数据
        FrontendProjectService.getInstance().resolveFormMetadatas(projectPath, ChosenFormList.getNewInstance());
        // 生成对应代码
        FrontendProjectGenerate.generateFrontendProject(projectPath);
        // 执行代码编译
        FrontendProjectBuild.buildFrontendProject(projectPath, false);

        return ResultCode.success();
    }


    /**
     * @param projectPath
     * @param buildFormList 参与编译的表单列表
     */
    public final void babelBuildAndDeploy(String projectPath, ChosenFormList buildFormList) {
        FrontendProjectService service = FrontendProjectService.getInstance();
        service.resolveFormMetadatas(projectPath, buildFormList);
        FrontendProjectGenerateForBabel.generate(projectPath);
        FrontendProjectBuild.buildFrontendProjectForBabel(projectPath);
        FrontendProjectDeployer.deployFrontendProjectForBabel(projectPath);
    }

    /**
     * 生成解析表单的依赖项
     *
     * @return
     */
    public final ResultMessage<String> resolveAndGenerateFrontendProjectWithDynamic(FormDynamicParameter dynamicParameter) {
        // 生成前校验  判定是否需要执行更新操作
        // 此参数要求isSingleForm参数必须为true
        FormDynamicParameterValidator.validate(dynamicParameter);

        //由于解析型表单传递的元数据路径包含forms  因此此处重新进行获取真实的元数据路径
        String projectPath = MetadataUtility.getInstance().getMetadataProjectPath(dynamicParameter.getProjectPath());
        dynamicParameter.setProjectPath(projectPath);

        ChosenFormList buildFormList = ChosenFormList.getNewInstance();
        // 依据是工程还是单个表单进行解析表单的生成
        // 如果是单个表单解析 那么输出至独立的位置
        // 如果是工程级别的构造，如果所有表单均是解析类型
        // 如果是工程级别的构造，如果部分表单是解析类型
        if (dynamicParameter.isUseSingleForm()) {
            // 如果单表单
            buildFormList.add(dynamicParameter.getFormCode(), true);
        }

        // 执行元数据解析
        FormDynamicMetadataResolver.resolve(dynamicParameter, buildFormList);


        // 执行代码编译
        if (!dynamicParameter.isUseSingleForm()) {
            // 执行代码生成
            FormDynamicSourceCodeGenerate.generate(dynamicParameter, buildFormList);
            FrontendProjectBuild.buildFrontendProject(dynamicParameter.getProjectPath(), dynamicParameter.isUseSingleForm());
        }

        // 执行代码部署
        // 单个表单的解析     整体工程的解析
        FrontendProjectDeployer.deployFrontendProjectForJieXi(dynamicParameter);
        return ResultCode.success();
    }

}
