/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.projectinfo;

import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.frontendproject.FrontendProjectUtility;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 工程信息
 * @author noah
 *
 * 2023/7/8 14:53
 */
public class ProjectInfoManager {
    /**
     * 当前工程是否为前端工程
     *
     * @param projectPath
     * @return
     */
    public final boolean isFrontendProject(String projectPath) {
        String projectType = getProjectType(projectPath);
        return projectType.equals(FrontendProjectConstant.FRONTEND_PROJECT_TYPE);
    }

    private String getProjectType(String projectPath) {
        if (StringUtility.isNullOrEmpty(projectPath)) {
            throw new WebCustomException("Current Project Path is Null or Empty", ExceptionLevel.Warning);
        }

        projectPath = FrontendProjectUtility.getProjectPathWithDevRootPath(projectPath);

        try {
            // 提供基于全路径的接口
            return FrontendProjectUtility.getProjectType(projectPath);
        } catch (RuntimeException exception) {
            throw new WebCustomException(String.format("Error When Getting Project Type. Current Project Path is: %1$s", projectPath), exception);
        }
    }
}
