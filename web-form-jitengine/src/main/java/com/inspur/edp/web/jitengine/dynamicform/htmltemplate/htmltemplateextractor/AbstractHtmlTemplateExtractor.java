/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public abstract class AbstractHtmlTemplateExtractor implements IHtmlTemplateExtractor {

    private HashMap<String, Object> currentComponent;

    private final List<HtmlTemplateEntity> htmlTemplateEntityList = new ArrayList<>();


    public HashMap<String, Object> getCurrentComponent() {
        return currentComponent;
    }

    @Override
    public final List<HtmlTemplateEntity> extract(HashMap<String, Object> component) {
        this.currentComponent = component;
        // 鉴于该实例会被缓存  因此需要在使用前将数据置空
        this.htmlTemplateEntityList.clear();
        this.extractHtmlTemplate(htmlTemplateEntityList);

        return this.htmlTemplateEntityList;
    }

    /**
     * 添加html模板到集合中
     *
     * @param htmlTemplateEntity
     */
    protected void addIntoCollection(HtmlTemplateEntity htmlTemplateEntity) {
        if (htmlTemplateEntity == null) {
            return;
        }
        this.htmlTemplateEntityList.add(htmlTemplateEntity);
    }

    /**
     * 获取对应组件的id
     *
     * @return
     */
    protected final String extractComponentId() {
        if (this.currentComponent == null || this.currentComponent.isEmpty()) {
            return "";
        }
        return ComponentUtility.getInstance().getId(this.currentComponent);
    }

    /**
     * 获取对应组件的类型
     *
     * @return
     */
    protected final String extractComponentType() {
        if (this.currentComponent == null || this.currentComponent.isEmpty()) {
            return "";
        }
        return ComponentUtility.getInstance().getType(this.currentComponent);
    }

    /**
     * 组合class名称
     *
     * @param suffix
     * @return
     */
    protected final String generateClassName(String suffix) {
        return this.extractComponentType() + StringUtility.replaceLineToUnderLine(this.extractComponentId()) + StringUtility.convertNullToEmptyString(suffix);
    }

    /**
     * 提取对应的html模板
     * 子类覆盖该行为  进行html模板的提取
     *
     * @param htmlTemplateEntityList
     * @return
     */
    public abstract void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList);
}
