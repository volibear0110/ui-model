/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
@Data
public class ModuleFormExpressionItem {
    @JsonProperty("sourceType")
    private String sourceType = "Field";

    @JsonProperty("expression")
    private List<ModuleExpressionStatement> expression;

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public List<ModuleExpressionStatement> getExpression() {
        if(this.expression==null){
            this.expression=new ArrayList<>();
        }
        return expression;
    }

    public void setExpression(List<ModuleExpressionStatement> expressions) {
        this.expression = expressions;
    }
}
