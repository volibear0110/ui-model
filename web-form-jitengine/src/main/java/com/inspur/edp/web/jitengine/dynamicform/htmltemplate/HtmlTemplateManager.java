/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadataanalysis.TsBuildConfigJsonGenerator;
import com.inspur.edp.web.jitengine.dynamicform.AngularJsonFileGenerator;
import com.inspur.edp.web.jitengine.dynamicform.AppComponentTsFileContentGenerator;
import com.inspur.edp.web.jitengine.dynamicform.AppModuleTsFileContentGenerator;
import com.inspur.edp.web.jitengine.dynamicform.TsConfigAppJsonFileGenerator;

import java.util.List;

public class HtmlTemplateManager {
    public static List<HtmlTemplateEntity> generate(String htmlTemplateBasePath, FormDOM formDOM) {

        List<HtmlTemplateEntity> htmlTemplateEntityList = HtmlTemplateGenerator.init().generate(formDOM);


        String tsConfigJsonContent = TsBuildConfigJsonGenerator.getTsConfigJsonContent();
        String angularJsonContent = AngularJsonFileGenerator.getAngularJsonContent();
        String tsConfigAppJsonContent = TsConfigAppJsonFileGenerator.getTsConfigAppJsonContent();
        String appComponentTsFileContent = new AppComponentTsFileContentGenerator().generateAppComponentContent(htmlTemplateEntityList);
        String appModuleTsFileContent = new AppModuleTsFileContentGenerator().generate(htmlTemplateEntityList);

        // 写入angular.json
        FileUtility.writeFile(htmlTemplateBasePath, "angular.json", angularJsonContent);

        // 写入tsconfig.json
        FileUtility.writeFile(htmlTemplateBasePath, "tsconfig.json", tsConfigJsonContent);

        FileUtility.writeFile(htmlTemplateBasePath, "package.json", "{\n" +
                "  \"name\": \"@farris/jit-engine\",\n" +
                "  \"version\": \"0.0.27\",\n" +
                "  \"description\": \"npm auto install\",\n" +
                "  \"keywords\": [],\n" +
                "  \"author\": \"Noah\",\n" +
                "  \"license\": \"ISC\",\n" +
                "  \"scripts\": {\n" +
                "    \"build\":\"ng build\"\n" +
                "  }\n" +
                "}\n");

        String webPageTemplatesPath = FileUtility.combine(htmlTemplateBasePath, "projects", "web-page-templates");

        String tsConfigAppJsonFilePath = FileUtility.combine(webPageTemplatesPath, "tsconfig.app.json");
        FileUtility.writeFile(tsConfigAppJsonFilePath, tsConfigAppJsonContent);

        // 写入一个空的polyfills文件
        String projectsSrcFilePath = FileUtility.combine(webPageTemplatesPath, "src");
        FileUtility.writeFile(projectsSrcFilePath, "polyfills.ts", "");

        String projectsAppFilePath = FileUtility.combine(webPageTemplatesPath, "src", "app");
        String appComponentFilePath = FileUtility.combine(projectsAppFilePath, "app.component.ts");
        FileUtility.writeFile(appComponentFilePath, appComponentTsFileContent);

        String appModuleFilePath = FileUtility.combine(projectsAppFilePath, "app.module.ts");
        FileUtility.writeFile(appModuleFilePath, appModuleTsFileContent);

        return htmlTemplateEntityList;
    }
}
