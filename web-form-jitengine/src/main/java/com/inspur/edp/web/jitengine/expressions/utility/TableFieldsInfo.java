/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions.utility;

import com.inspur.edp.web.jitengine.expressions.ModuleSchemaEntities;

import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
public class TableFieldsInfo {
    private List<HashMap<String, Object>> fields;

    private ModuleSchemaEntities relatedEntity;

    private  String bindTo;

    public List<HashMap<String, Object>> getFields() {
        return fields;
    }

    public void setFields(List<HashMap<String, Object>> fields) {
        this.fields = fields;
    }

    public ModuleSchemaEntities getRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(ModuleSchemaEntities relatedEntity) {
        this.relatedEntity = relatedEntity;
    }

    public String getBindTo() {
        return bindTo;
    }

    public void setBindTo(String bindTo) {
        this.bindTo = bindTo;
    }
}
