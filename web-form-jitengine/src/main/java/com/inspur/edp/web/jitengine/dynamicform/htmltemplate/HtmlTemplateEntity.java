/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate;

import lombok.Data;

/**
 * html 解析模板提取
 *
 * @author guozhiqi
 */
@Data
public class HtmlTemplateEntity {
    /**
     * html 模板所属控件id
     */
    private String id;

    /**
     * html模板所属控件类型
     */
    private String controlType;

    /**
     * 生成的class 内容
     */
    private String generatedClassContent;

    /**
     * 生成对应组件的class名称
     */
    private String generatedClassName;

    /**
     * 选择器名称
     */
    private String selectorName;

    /**
     * 对应组件名称 默认为name+id
     */
    private String componentName;
}
