/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.jitengine.expressions.utility.ExpressionUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
public class ExpressionFormGenerator {
    public static ModuleFormExpressions generate(FormDOM formDom, String containerId, HashMap<String, Object> externalComponentItem) {
        ModuleFormExpressions moduleFormExpressions = new ModuleFormExpressions();
        moduleFormExpressions.setFormModuleCode(formDom.getModule().getCode());

        if (externalComponentItem != null) {
            String externalComponentCode = externalComponentItem.get("code") == null ? "" : externalComponentItem.get("code").toString();
            String nameSpace = ExpressionUtility.getNameSpace(externalComponentItem.get("id").toString(), externalComponentCode.toLowerCase());
            // 转换为小写形式
            moduleFormExpressions.setNameSpace(nameSpace);
        } else {
            moduleFormExpressions.setNameSpace("");
        }


        // 获取组合表单设定的containerId
        //var externalComponentUniqueID = parentExternalComponent.containerId ? parentExternalComponent.containerId : parentExternalComponent.id;
        // 临时设定uniqueId为空
        moduleFormExpressions.setFormExpressionJsonPath(ExpressionUtility.getExpressionFormJsonPath(containerId, formDom.getModule().getCode()));

        // 获取表单中定义的表达式
        List<HashMap<String, Object>> expressionsJson = formDom.getModule().getExpressions();

        // 如果表达式为空 或表达式未定义 那么不再进行后续处理
        if (expressionsJson == null || expressionsJson.isEmpty()) {
            return moduleFormExpressions;
        }
        String strExpressionJson = SerializeUtility.getInstance().serialize(expressionsJson, false);
        List<ModuleFormExpressionFieldItem> expressionFieldItemList = new ArrayList<>();
        expressionFieldItemList = SerializeUtility.getInstance().deserialize(strExpressionJson, new com.fasterxml.jackson.core.type.TypeReference<List<ModuleFormExpressionFieldItem>>() {
        });

        ArrayList<HashMap<String, Object>> entities = (ArrayList<HashMap<String, Object>>) formDom.getModule().getSchemas().get(0).get("entities");
        String mainEntityName = (String) entities.get(0).get("code");

        for (ModuleFormExpressionFieldItem item : expressionFieldItemList) {
            List<ModuleExpressionStatement> fieldExpressionList = item.getExpression();

            if (fieldExpressionList.size() == 0) {
                continue;
            }
            ModuleFormExpressionFieldItem moduleFormExpressionFieldItem = new ModuleFormExpressionFieldItem();
            moduleFormExpressionFieldItem.setFieldId(item.getFieldId());
            moduleFormExpressionFieldItem.setSourceType("Field");
            if ("Button".equals(item.getSourceType())) {
                moduleFormExpressionFieldItem.setFieldCode(item.getFieldId());
                moduleFormExpressionFieldItem.setFieldLabel(item.getFieldId());
                moduleFormExpressionFieldItem.setSourceType(item.getSourceType());
                if (fieldExpressionList != null && fieldExpressionList.size() > 0) {
                    for (ModuleExpressionStatement fieldExpressionItem : fieldExpressionList) {
                        String fieldExpressionType = fieldExpressionItem.getType();
                        String expressionValue = fieldExpressionItem.getValue();
                        ModuleExpressionStatement moduleExpressionStatement = new ModuleExpressionStatement();
                        moduleExpressionStatement.setValue(expressionValue);
                        moduleExpressionStatement.setType(fieldExpressionType);
                        moduleExpressionStatement.setMessage(fieldExpressionItem.getMessage());
                        moduleExpressionStatement.setMessageType(fieldExpressionItem.getMessageType());
                        moduleExpressionStatement.setId(fieldExpressionItem.getId());
                        moduleFormExpressionFieldItem.getExpression().add(moduleExpressionStatement);
                    }
                    // 追加
                    moduleFormExpressions.getExpressions().add(moduleFormExpressionFieldItem);
                }
                continue;
            }
            FieldInfo fieldInfo = ExpressionUtility.getFieldInfoById(item.getFieldId(), formDom);
            if (fieldInfo != null && fieldInfo.getSchemaFields() != null) {
                HashMap<String, Object> fieldInfoSchemaFields = (HashMap<String, Object>) fieldInfo.getSchemaFields();
                if (!fieldInfoSchemaFields.isEmpty()) {
                    String bindTo = fieldInfo.getBindTo();
                    moduleFormExpressionFieldItem.setFieldCode(fieldInfoSchemaFields.get("code").toString());
                    moduleFormExpressionFieldItem.setFieldLabel(fieldInfo.getRefElementLabelPath());
                    moduleFormExpressionFieldItem.setBindTo(bindTo);

                    if (fieldExpressionList != null && fieldExpressionList.size() > 0) {
                        for (ModuleExpressionStatement fieldExpressionItem : fieldExpressionList) {
                            String fieldExpressionType = fieldExpressionItem.getType();
                            String expressionValue = fieldExpressionItem.getValue();

                            ModuleExpressionStatement moduleExpressionStatement = new ModuleExpressionStatement();

                            moduleExpressionStatement.setValue(expressionValue);
                            moduleExpressionStatement.setType(fieldExpressionType);

                            moduleExpressionStatement.setMessage(fieldExpressionItem.getMessage());
                            moduleExpressionStatement.setMessageType(fieldExpressionItem.getMessageType());
                            moduleExpressionStatement.setId(fieldExpressionItem.getId());


                            moduleFormExpressionFieldItem.getExpression().add(moduleExpressionStatement);
                        }
                        // 追加
                        moduleFormExpressions.getExpressions().add(moduleFormExpressionFieldItem);
                    }
                }
            }
        }

        return moduleFormExpressions;
    }
}
