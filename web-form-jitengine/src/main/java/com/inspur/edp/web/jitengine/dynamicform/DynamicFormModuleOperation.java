/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * @Title: DynamicFormModuleOperation
 * @Description: com.inspur.edp.web.frontendproject.formdynamic 解析表单针对表单dom得操作
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/12 17:36
 */
public class DynamicFormModuleOperation {
    /**
     * 解析表单保存metadata.js文件
     *
     * @param formDOM
     * @param targetBasePath
     * @param formCode
     */
    public static void saveForMetadataJs(FormDOM formDOM, String targetBasePath, String formCode) {
        String metadataJsFileName = DynamicFormMetaFileNameGenerator.generateMetaFileName(formCode);
        String targetMetadataJsFilePath = FileUtility.combine(targetBasePath, metadataJsFileName);
        FileUtility.writeFile(targetMetadataJsFilePath, SerializeUtility.getInstance().serialize(formDOM));
    }
}
