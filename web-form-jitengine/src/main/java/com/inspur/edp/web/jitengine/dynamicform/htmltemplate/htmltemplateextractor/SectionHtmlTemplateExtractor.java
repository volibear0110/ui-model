/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

/**
 * section 模板提取
 *
 * @author guozhiqi
 */
public class SectionHtmlTemplateExtractor extends AbstractHtmlTemplateExtractor {

    /**
     * 提取对应的模板内容
     *
     * @param htmlTemplateEntityList
     */
    @Override
    public void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList) {

        String sectionId = ComponentUtility.getInstance().getId(this.getCurrentComponent());


        String titleTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "titleTemplate");
        if (!StringUtility.isNullOrEmpty(titleTemplateContent)) {
            String titleTemplateClassName = this.generateClassName("TitleTemplate");
            HtmlTemplateEntity titleTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            titleTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            titleTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            titleTemplateHtmlTemplateEntity.setGeneratedClassContent(titleTemplateContent);
            titleTemplateHtmlTemplateEntity.setGeneratedClassName(titleTemplateClassName);
            titleTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"TitleTemplate");
            titleTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"TitleTemplate");
            this.addIntoCollection(titleTemplateHtmlTemplateEntity);
        }


        String headerTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "headerTemplate");
        if (!StringUtility.isNullOrEmpty(headerTemplateContent)) {
            String headerTemplateClassName = this.generateClassName("HeaderTemplate");
            HtmlTemplateEntity headerTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            headerTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            headerTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            headerTemplateHtmlTemplateEntity.setGeneratedClassName(headerTemplateClassName);
            headerTemplateHtmlTemplateEntity.setGeneratedClassContent(headerTemplateContent);
            headerTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"HeaderTemplate");
            headerTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"HeaderTemplate");
            this.addIntoCollection(headerTemplateHtmlTemplateEntity);
        }


        String contentTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "contentTemplate");
        if (!StringUtility.isNullOrEmpty(contentTemplateContent)) {
            String contentTemplateClassName = this.generateClassName("ContentTemplate");
            HtmlTemplateEntity contentTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            contentTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            contentTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            contentTemplateHtmlTemplateEntity.setGeneratedClassName(contentTemplateClassName);
            contentTemplateHtmlTemplateEntity.setGeneratedClassContent(contentTemplateContent);
            contentTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"ContentTemplate");
            contentTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"ContentTemplate");
            this.addIntoCollection(contentTemplateHtmlTemplateEntity);
        }

        String toolbarTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "toolbarTemplate");
        if (!StringUtility.isNullOrEmpty(toolbarTemplateContent)) {
            String toolbarTemplateClassName = this.generateClassName("ToolbarTemplate");
            HtmlTemplateEntity toolbarTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            toolbarTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            toolbarTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            toolbarTemplateHtmlTemplateEntity.setGeneratedClassName(toolbarTemplateClassName);
            toolbarTemplateHtmlTemplateEntity.setGeneratedClassContent(toolbarTemplateContent);
            toolbarTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"ToolbarTemplate");
            toolbarTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"ToolbarTemplate");
            this.addIntoCollection(toolbarTemplateHtmlTemplateEntity);
        }


        String extendedAreaTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "extendedAreaTemplate");
        if (!StringUtility.isNullOrEmpty(extendedAreaTemplateContent)) {
            String extendedAreaTemplateClassName = this.generateClassName("ExtendedAreaTemplate");
            HtmlTemplateEntity extendedAreaTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            extendedAreaTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            extendedAreaTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            extendedAreaTemplateHtmlTemplateEntity.setGeneratedClassContent(extendedAreaTemplateClassName);
            extendedAreaTemplateHtmlTemplateEntity.setGeneratedClassContent(extendedAreaTemplateContent);
            extendedAreaTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"ExtendedAreaTemplate");
            extendedAreaTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"ExtendedAreaTemplate");
            this.addIntoCollection(extendedAreaTemplateHtmlTemplateEntity);
        }
    }
}
