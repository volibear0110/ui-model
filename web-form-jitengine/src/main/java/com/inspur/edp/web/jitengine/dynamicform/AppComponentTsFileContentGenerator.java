/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

public class AppComponentTsFileContentGenerator {
    private final String headerContent = "import { AfterViewInit, Component, ElementRef,Injector } from '@angular/core';\n" +
            "import { Pipe, PipeTransform } from '@angular/core';\n" +
            "\n" +
            "@Pipe({ name: 'lang' })\n" +
            "export class LangPipe implements PipeTransform {\n" +
            "    constructor(private injector: Injector) { }\n" +
            "    transform(key: string, langCode: string, defaultValue?: string) {\n" +
            "        return defaultValue;\n" +
            "    }\n" +
            "}";

    private String getComponent(String selectorName, String templateContent, String className, String componentName) {
        return "/**\n" +
                " * " + componentName + "\n" +
                " */\n" +
                "@Component({\n" +
                "    selector: '" + StringUtility.convertNullToEmptyString(selectorName) + "',\n" +
                "    template: ` \n" +
               StringUtility.convertNullToEmptyString(templateContent)
                + "`\n" +
                "})\n" +
                "export class " + className + " {\n" +
                "    viewModel: any;\n" +
                "    lang: string;\n" +
                "    constructor() { }\n" +
                "}";
    }

    /**
     * 生成对应的app.component.ts 文件内容
     *
     * @param htmlTemplateEntityList
     * @return
     */
    public String generateAppComponentContent(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        if (htmlTemplateEntityList == null || htmlTemplateEntityList.isEmpty()) {
            return headerContent;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(headerContent);
        htmlTemplateEntityList.forEach(t -> {

            String customComponentContent = getComponent(t.getSelectorName(), t.getGeneratedClassContent(), t.getGeneratedClassName(), t.getComponentName());
            sb.append("\n" + customComponentContent + "\n");
        });
        return sb.toString();
    }

}
