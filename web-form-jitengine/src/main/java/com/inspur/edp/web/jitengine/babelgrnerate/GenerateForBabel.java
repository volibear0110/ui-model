/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.babelgrnerate;

import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.io.NodejsFunctionUtility;
import com.inspur.edp.web.common.metadata.GspProjectUtility;
import com.inspur.edp.web.common.utility.CommandLineUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitengine.NpmInstallBeforeGenerate;
import com.inspur.edp.web.jitengine.ProjectCompileContext;

public class GenerateForBabel {
    public static void generateFrontendProjectForBabel(String currentProjectPath, String refNodeModulesBasePath) {
        if (StringUtility.isNullOrEmpty(currentProjectPath)) {
            return;
        }

        String currentProjectName = GspProjectUtility.getProjectName(currentProjectPath);

        if (!FileUtility.isAbsolute(currentProjectPath)) {
            currentProjectPath = FileUtility.getAbsolutePathHead(currentProjectPath) + currentProjectPath;
        }

        GspProject gspProject = GspProjectUtility.getProjectInformation(currentProjectPath);
        String serviceUnitPath = gspProject.getSuDeploymentPath();

        String formPublishAbsolutePath = currentProjectPath + "/src" + "/" + FrontendProjectConstant.PROJECT_GENERATE_PATH_FOR_BABEL;

        ProjectCompileContext projectCompileContext = new ProjectCompileContext(currentProjectName, java.nio.file.Paths.get(refNodeModulesBasePath).resolve("node_modules").toString(), "pc", "angular",
                currentProjectPath + "/src" + "/webdev", formPublishAbsolutePath,
                currentProjectPath + "/eapi", serviceUnitPath, ExecuteEnvironment.Design);

        generateProjectForBabel(projectCompileContext);

    }

    private static void generateProjectForBabel(ProjectCompileContext projectCompileContext) {
        // 编译前执行install  install的是全局离线包
        NpmInstallBeforeGenerate.beforeCompileExecuteWithNpmInstall(projectCompileContext, false, true);

        // 获取jit命令执行参数
        String jitCommand = NodejsFunctionUtility.getJitCommandInServer(projectCompileContext.executeEnvironment.equals(ExecuteEnvironment.UpgradeTool));
        String args = jitCommand;// "jit";
        args += String.format(" --projectName=%1$sforbabel", projectCompileContext.getProjectName());
        args += String.format(" --projectRoute=%1$s", projectCompileContext.getProjectRoute());
        args += String.format(" --linkedNodeModules=%1$s", projectCompileContext.getNodeModulesAbsolutePath());
        args += String.format(" --formType=%1$s", projectCompileContext.getFormType());
        args += String.format(" --frameworkType=%1$s", projectCompileContext.getFrameworkType());
        // 表单生成的json文件路径
        args += String.format(" --mp=%1$s", projectCompileContext.getFormJsonAbsolutePath());
        // 工程发布路径
        args += String.format(" --pp=%1$s", projectCompileContext.getFormPublishAbsolutePath());
        // eapi路径
        args += String.format(" --eapiPath=%1$s", projectCompileContext.getEapiAbsolutePath());
        args += String.format(" --serviceUnitPath=%1$s", projectCompileContext.getServiceUnitPath());
        args += " --isRuntime=true";
        args += " --isBabelCompile=true";
        //args += $" --isGenerateViewModel=false";

        String[] command = {args};
        CommandLineUtility.runCommand(command);
    }


}
