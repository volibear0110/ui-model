/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.jitengine.JITEngineManager;

public class AngularJsonFileGenerator {
    private static final String angularJsonContent = "{\n" +
            "  \"$schema\": \"./node_modules/@angular/cli/lib/config/schema.json\",\n" +
            "  \"version\": 1,\n" +
            "  \"newProjectRoot\": \"projects\",\n" +
            "  \"projects\": {\n" +
            "    \"web-page-templates\": {\n" +
            "      \"root\": \"projects/web-page-templates/\",\n" +
            "      \"sourceRoot\": \"projects/web-page-templates/src\",\n" +
            "      \"projectType\": \"application\",\n" +
            "      \"prefix\": \"app\",\n" +
            "      \"schematics\": {},\n" +
            "      \"architect\": {\n" +
            "        \"build\": {\n" +
            "          \"builder\": \"@farris/farris-rollup:build\",\n" +
            "          \"options\": {\n" +
            "            \"format\": \"system\",\n" +
            "            \"outputPath\": \"dist-rollup/web-page-templates\",\n" +
            "            \"ngcOut\": \"out-tsc/app/projects/web-page-templates\",\n" +
            "            \"tsConfig\": \"projects/web-page-templates/tsconfig.app.json\",\n" +
            "            \"excludeNgFactory\": true,\n" +
            "            \"entry\": {\n" +
            "              \"webapp-templates.module\": {\n" +
            "                \"entry\": \"src/app/app.module.js\"\n" +
            "              },\n" +
            "              \"webapp-templates.module.ngfactory\": {\n" +
            "                \"entry\": \"src/app/app.module.ngfactory.js\"\n" +
            "              }\n" +
            "            },\n" +
            "            \"uglify\": false,\n" +
            "            \"hash\": false,\n" +
            "            \"libs\": {\n" +
            "              \"entry\": \".js\"\n" +
            "            },\n" +
            "            \"libsPath\": \"\",\n" +
            "            \"serve\": false,\n" +
            "            \"visualize\":false\n" +
            "          },\n" +
            "          \"configurations\": {\n" +
            "            \"production\": {\n" +
            "              \"uglify\": true,\n" +
            "              \"visualize\": true\n" +
            "            }\n" +
            "          }\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  \"defaultProject\": \"web-page-templates\"\n" +
            "}";

    public static String getAngularJsonContent() {
        String devRootPath = MetadataUtility.getInstance().getDevRootPath();
        if (!FileUtility.isAbsolute(devRootPath)) {
            devRootPath = FileUtility.getAbsolutePathHead(devRootPath) + devRootPath;
        }
        String node_modulesPath =FileUtility.getPlatformIndependentPath(JITEngineManager.getRefNodeModulesPath(devRootPath, TerminalType.PC));
        String libsPath = node_modulesPath + "/@farris/farris-rollup/src/build/libsWithNgFactory.json";
        String angularJsonContent = "{\n" +
                "  \"$schema\": \"./node_modules/@angular/cli/lib/config/schema.json\",\n" +
                "  \"version\": 1,\n" +
                "  \"newProjectRoot\": \"projects\",\n" +
                "  \"projects\": {\n" +
                "    \"web-page-templates\": {\n" +
                "      \"root\": \"projects/web-page-templates/\",\n" +
                "      \"sourceRoot\": \"projects/web-page-templates/src\",\n" +
                "      \"projectType\": \"application\",\n" +
                "      \"prefix\": \"app\",\n" +
                "      \"schematics\": {},\n" +
                "      \"architect\": {\n" +
                "        \"build\": {\n" +
                "          \"builder\": \"@farris/farris-rollup:build\",\n" +
                "          \"options\": {\n" +
                "            \"format\": \"system\",\n" +
                "            \"outputPath\": \"dist-rollup/web-page-templates\",\n" +
                "            \"ngcOut\": \"out-tsc/app/projects/web-page-templates\",\n" +
                "            \"tsConfig\": \"projects/web-page-templates/tsconfig.app.json\",\n" +
                "            \"excludeNgFactory\": true,\n" +
                "            \"entry\": {\n" +
                "              \"webapp-templates.module\": {\n" +
                "                \"entry\": \"src/app/app.module.js\"\n" +
                "              },\n" +
                "              \"webapp-templates.module.ngfactory\": {\n" +
                "                \"entry\": \"src/app/app.module.ngfactory.js\"\n" +
                "              }\n" +
                "            },\n" +
                "            \"uglify\": false,\n" +
                "            \"hash\": false,\n" +
                "            \"libs\": {\n" +
                "              \"entry\": \".js\"\n" +
                "            },\n" +
                "            \"linkedNodeModules\": \"" + node_modulesPath + "\",\n" +
                "            \"libsPath\": \"" + libsPath + "\",\n" +
                "            \"serve\": false,\n" +
                "            \"visualize\":false\n" +
                "          },\n" +
                "          \"configurations\": {\n" +
                "            \"production\": {\n" +
                "              \"uglify\": true,\n" +
                "              \"visualize\": true\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"defaultProject\": \"web-page-templates\"\n" +
                "}";
        return angularJsonContent;
    }
}
