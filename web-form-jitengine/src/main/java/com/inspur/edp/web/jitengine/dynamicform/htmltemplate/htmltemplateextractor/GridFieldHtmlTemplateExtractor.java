/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

public class GridFieldHtmlTemplateExtractor  extends AbstractHtmlTemplateExtractor{
    @Override
    public void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        String colTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "colTemplate");
        if (!StringUtility.isNullOrEmpty(colTemplateContent)) {
            String colTemplateClassName = this.generateClassName("ColTemplate");
            HtmlTemplateEntity colTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            colTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            colTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            colTemplateHtmlTemplateEntity.setGeneratedClassContent(colTemplateContent);
            colTemplateHtmlTemplateEntity.setGeneratedClassName(colTemplateClassName);
            colTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"ColTemplate");
            colTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"ColTemplate");
            this.addIntoCollection(colTemplateHtmlTemplateEntity);
        }

        String captionTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "captionTemplate");
        if (!StringUtility.isNullOrEmpty(captionTemplateContent)) {
            String captionTemplateClassName = this.generateClassName("CaptionTemplate");
            HtmlTemplateEntity captionTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            captionTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            captionTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            captionTemplateHtmlTemplateEntity.setGeneratedClassContent(captionTemplateContent);
            captionTemplateHtmlTemplateEntity.setGeneratedClassName(captionTemplateClassName);
            captionTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"CaptionTemplate");
            captionTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"CaptionTemplate");
            this.addIntoCollection(captionTemplateHtmlTemplateEntity);
        }

        String captionTipContentTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "captionTipContent");
        if (!StringUtility.isNullOrEmpty(captionTipContentTemplateContent)) {
            String captionTipContentClassName = this.generateClassName("CaptionTipContent");
            HtmlTemplateEntity captionTipContentHtmlTemplateEntity = new HtmlTemplateEntity();
            captionTipContentHtmlTemplateEntity.setId(this.extractComponentId());
            captionTipContentHtmlTemplateEntity.setControlType(this.extractComponentType());
            captionTipContentHtmlTemplateEntity.setGeneratedClassContent(captionTipContentTemplateContent);
            captionTipContentHtmlTemplateEntity.setGeneratedClassName(captionTipContentClassName);
            captionTipContentHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"CaptionTipContent");
            captionTipContentHtmlTemplateEntity.setComponentName(this.extractComponentId()+"CaptionTipContent");
            this.addIntoCollection(captionTipContentHtmlTemplateEntity);
        }



    }
}
