/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

public class DataGridHtmlTemplateExtractor extends AbstractHtmlTemplateExtractor {
    @Override
    public void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        String emptyTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "emptyTemplate");
        if (!StringUtility.isNullOrEmpty(emptyTemplateContent)) {
            String emptyTemplateClassName = this.generateClassName("EmptyTemplate");
            HtmlTemplateEntity emptyTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            emptyTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            emptyTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            emptyTemplateHtmlTemplateEntity.setGeneratedClassContent(emptyTemplateContent);
            emptyTemplateHtmlTemplateEntity.setGeneratedClassName(emptyTemplateClassName);
            emptyTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId() + "EmptyTemplate");
            emptyTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId() + "EmptyTemplate");
            this.addIntoCollection(emptyTemplateHtmlTemplateEntity);
        }


        String footerTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "footerTemplate");
        if (!StringUtility.isNullOrEmpty(footerTemplateContent)) {
            String footerTemplateClassName = this.generateClassName("FooterTemplate");
            HtmlTemplateEntity footerTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            footerTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            footerTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            footerTemplateHtmlTemplateEntity.setGeneratedClassContent(footerTemplateContent);
            footerTemplateHtmlTemplateEntity.setGeneratedClassName(footerTemplateClassName);
            footerTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId() + "FooterTemplate");
            footerTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId() + "FooterTemplate");
            this.addIntoCollection(footerTemplateHtmlTemplateEntity);
        }

        if (htmlTemplateEntityList.contains("enableCommandColumn")) {
            Object enableCommandColumn = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "enableCommandColumn");
            if (enableCommandColumn instanceof Boolean) {
                Boolean boolEnableCommandColumn = (Boolean) enableCommandColumn;
                // 如果使用默认的操作列
                if (boolEnableCommandColumn) {
                    String commandColumnTemplateClassName = this.generateClassName("CommandColumn");
                    HtmlTemplateEntity commandColumnHtmlTemplateEntity = new HtmlTemplateEntity();
                    commandColumnHtmlTemplateEntity.setId(this.extractComponentId());
                    commandColumnHtmlTemplateEntity.setControlType(this.extractComponentType());
                    String onEditClicked = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "onEditClicked");
                    String onDeleteClicked = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "onDeleteClicked");
                    String editButtonLangId = StringUtility.replaceDotToUnderLine("DataGrid/" + this.extractComponentId() + "/OperateEditButton");
                    String deleteButtonLangId =StringUtility.replaceDotToUnderLine( "DataGrid/" + this.extractComponentId() + "/OperateDeleteButton");

                    String editClick = StringUtility.isNotNullOrEmpty(onEditClicked) ? "(click)='" + onEditClicked + "'" : "";
                    String deleteClick = StringUtility.isNotNullOrEmpty(onDeleteClicked) ? "(click)='" + onDeleteClicked + "'" : "";


                    commandColumnHtmlTemplateEntity.setGeneratedClassContent("    <div class=\"f-btn-group \">\n" +
                            "        <div class=\"btn-group-sm btn-group f-btn-group-links\">\n" +
                            "       <button class=\"btn-link btn ng-star-inserted\" "+editClick+">{{'${I18nResourceManager.generateI18nResourceId(editButtonLangId, i18nResourcePrefix)}'|lang:lang:'编辑'}}</button>\n" +
                            "       <button  class=\"btn-link btn ng-star-inserted\" ${deleteClick}>{{'${I18nResourceManager.generateI18nResourceId(deleteButtonLangId, i18nResourcePrefix)}'|lang:lang:'删除'}}</button>\n" +
                            "       </div>\n" +
                            "       </div>");
                    commandColumnHtmlTemplateEntity.setGeneratedClassName(commandColumnTemplateClassName);
                    commandColumnHtmlTemplateEntity.setSelectorName(this.extractComponentId() + "CommandColumn");
                    commandColumnHtmlTemplateEntity.setComponentName(this.extractComponentId() + "CommandColumn");
                    this.addIntoCollection(commandColumnHtmlTemplateEntity);
                }
            }
        }


    }
}
