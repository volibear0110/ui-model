/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.core.npminstall.NpmInstallManager;

/**
 * 生成前执行npm更新
 *
 * @author guozhiqi
 */
public class NpmInstallBeforeGenerate {
    public static void beforeCompileExecuteWithNpmInstall(ProjectCompileContext projectCompileContext, boolean isRuntime, boolean isGlobal) {
        beforeCompileExecuteWithNpmInstall(projectCompileContext.executeEnvironment, isRuntime, isGlobal);
    }

    public static void beforeCompileExecuteWithNpmInstall(ExecuteEnvironment executeEnvironment, boolean isRuntime, boolean isGlobal) {
        NpmPackageResponse packageResponse = null;
        if (isGlobal) {
            packageResponse = NpmInstallManager.npmInstallWithDefault(executeEnvironment.equals(ExecuteEnvironment.UpgradeTool), false, true);
        } else {
            packageResponse = NpmInstallManager.npmInstallWithDefault(executeEnvironment.equals(ExecuteEnvironment.UpgradeTool), isRuntime, false);
        }

        if (packageResponse != null && !packageResponse.isSuccess()) {
            throw new WebCustomException(packageResponse.getErrorMessage());
        }
    }
}
