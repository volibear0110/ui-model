/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.i18nresource;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.ArrayList;

/**
 * 生成的资源项集合
 *
 * @author guozhiqi
 */
public final class GeneratedI18nResourceList {
    /**
     * 生成的资源项列表集合
     */
    private ArrayList<GeneratedI8nResource> resourceList = null;

    public GeneratedI18nResourceList() {
        this.resourceList = new ArrayList<>();
    }

    public ArrayList<GeneratedI8nResource> getResourceList() {
        return resourceList;
    }

    /**
     * 增加资源项
     *
     * @param key
     * @param value
     */
    public void addIfNotExistsWithKey(String key, String value) {
        if (StringUtility.isNullOrEmpty(key)) {
            return;
        }
        // 如果已经存在 那么不再添加 以第一个为准
        if (this.resourceList.stream().anyMatch(t -> key.equals(t.getKey()))) {
            return;
        }

        GeneratedI8nResource generatedI8nResourceItem = new GeneratedI8nResource();
        generatedI8nResourceItem.setKey(key);
        generatedI8nResourceItem.setValue(value);
        this.resourceList.add(generatedI8nResourceItem);
    }
}
