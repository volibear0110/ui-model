/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import java.util.ArrayList;
import java.util.List;

/**
 * 关联的表达式清单
 * 包含组合表单
 *
 * @author guozhiqi
 */
public class ExpressionManifest {

    /**
     * 清单文件对应的文件路径
     */
    public String manifestJsonPath;


    /**
     * 清单文件关联的主表单code
     */
    public String formModuleCode;


    /**
     * 清单文件中包含的表单表达式集合
     */
    public List<ModuleFormExpressions> expressions;

    public String getManifestJsonPath() {
        return manifestJsonPath;
    }

    public void setManifestJsonPath(String manifestJsonPath) {
        this.manifestJsonPath = manifestJsonPath;
    }

    public String getFormModuleCode() {
        return formModuleCode;
    }

    public void setFormModuleCode(String formModuleCode) {
        this.formModuleCode = formModuleCode;
    }

    public List<ModuleFormExpressions> getExpressions() {
        if (this.expressions == null) {
            this.expressions = new ArrayList<>();
        }
        return expressions;
    }

    public void setExpressions(List<ModuleFormExpressions> expressions) {
        this.expressions = expressions;
    }
}
