/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions.parser;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
public class ExpressionParserManager {
    public static String Parse(String source, String entityName) {
        DefaultFunctionExpressionParser defaultFunctionExpressionParser = new DefaultFunctionExpressionParser();
        String defaultFunctionExpressionParseResult = defaultFunctionExpressionParser.Parse(source);

        EntityExpressionParser entityExpressionParser = new EntityExpressionParser();
        String entityExpressionParseResult = entityExpressionParser.Parse(defaultFunctionExpressionParseResult, entityName);

        FieldsExpressionParser fieldsExpressionParser = new FieldsExpressionParser();
        String fieldExpressionParseResult = fieldsExpressionParser.Parse(entityExpressionParseResult, entityName);
        if (!StringUtility.isNullOrEmpty(fieldExpressionParseResult)) {

            // 判断是否包含return
            if (!checkHasReturn(fieldExpressionParseResult)) {
                fieldExpressionParseResult = "(ctx)=>{return " + fieldExpressionParseResult + "}";
            } else {
                fieldExpressionParseResult = "(ctx)=>{" + fieldExpressionParseResult + "}";
            }

        }
        return fieldExpressionParseResult;
    }

    /**
     * 判断是否包含return
     *
     * @param source
     * @return
     */
    public static boolean checkHasReturn(String source) {
        String strPattern = "([\\s\\r\\n{;}]+return)|(^return)\\s+";
        Pattern pattern = Pattern.compile(strPattern,Pattern.COMMENTS);
        Matcher matcher = pattern.matcher(source);
        return matcher.find();
    }

}
