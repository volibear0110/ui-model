/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/02/25
 */
public class ExpressionManifestManager {

    /**
     * 表单表达式json文件写入
     *
     * @param expressionManifest
     * @param basePath
     * @param isSingleDynamicForm
     */
    public static void writeExpressionJson(ExpressionManifest expressionManifest, String basePath, boolean isSingleDynamicForm) {

        String expressionManifestJsonPath = Paths.get(basePath, "expressions", expressionManifest.getFormModuleCode().toLowerCase(), "expressions").toString();
        if (isSingleDynamicForm) {
            expressionManifestJsonPath = Paths.get(basePath, "expressions").toString();
        }
        java.io.File directoryInfo = new java.io.File(expressionManifestJsonPath);
        if (directoryInfo.exists()) {
            directoryInfo.delete();
        }

        ExpressionManifestJsonEntity expressionManifestJsonEntity = new ExpressionManifestJsonEntity();

        if (expressionManifest != null && expressionManifest.getExpressions().size() > 0) {

            // 所有关联表单的表达式
            List<ExpressionFormJsonEntity> expressionFormJsonEntities = new ArrayList<>();

            expressionManifest.getExpressions().forEach((item) ->
            {
                ExpressionManifestJsonItemEntity expressionManifestJsonItemEntity = new ExpressionManifestJsonItemEntity();
                expressionManifestJsonItemEntity.setNs(item.getNameSpace());
                expressionManifestJsonItemEntity.setPath("./" + item.getFormExpressionJsonPath());
                expressionManifestJsonEntity.getExpressions().add(expressionManifestJsonItemEntity);

                ExpressionFormJsonEntity expressionFormJsonEntity = new ExpressionFormJsonEntity();
                expressionFormJsonEntity.setPath(item.getFormExpressionJsonPath());
                item.getExpressions().forEach((expressionItem) -> {
                    ExpressionFormFieldJsonEntity expressionFormFieldJsonEntity = new ExpressionFormFieldJsonEntity();
                    expressionFormFieldJsonEntity.setNs(item.getNameSpace());

                    if (!StringUtility.isNullOrEmpty(expressionItem.getBindTo()) && !expressionItem.getBindTo().equals("/")) {
                        expressionFormFieldJsonEntity.setPath(expressionItem.getBindTo(), expressionItem.getFieldLabel());
                    } else {
                        expressionFormFieldJsonEntity.setPath(expressionItem.getFieldLabel());
                    }

                    expressionFormFieldJsonEntity.setType(expressionItem.getSourceType());

                    expressionItem.getExpression().forEach((expressionStatementItem) -> {

                        ExpressionFormFieldExpression expressionFormFieldExpression = new ExpressionFormFieldExpression();
                        expressionFormFieldExpression.setType(expressionStatementItem.getType());

                        expressionFormFieldExpression.setMessage(expressionStatementItem.getMessage());
                        expressionFormFieldExpression.setMessageType(expressionStatementItem.getMessageType());
                        expressionFormFieldExpression.setId(expressionStatementItem.getId());
                        //
                        if (expressionStatementItem.getValue() != null || expressionStatementItem.getValue().length() > 0) {
                            DesignExpressionValue designExpressionValue = SerializeUtility.getInstance().deserialize(expressionStatementItem.getValue(), DesignExpressionValue.class);
                            if (designExpressionValue != null) {
                                expressionFormFieldExpression.setValue(designExpressionValue.getExpr());
                            } else {
                                expressionFormFieldExpression.setValue("");
                            }
                        } else {
                            expressionFormFieldExpression.setValue("");
                        }
                        // 构造依赖
                        //expressionFormFieldExpression.Deps
                        expressionFormFieldJsonEntity.getExpressions().add(expressionFormFieldExpression);
                    });


                    expressionFormJsonEntity.getFormFieldJsonEntities().add(expressionFormFieldJsonEntity);
                });

                expressionFormJsonEntities.add(expressionFormJsonEntity);
            });

            // 写入具体的表单文件
            String finalExpressionManifestJsonPath = expressionManifestJsonPath;
            expressionFormJsonEntities.forEach((expressionFormJsonEntityItem) -> {
                String strFormExpression = SerializeUtility.getInstance().serialize(expressionFormJsonEntityItem.getFormFieldJsonEntities(), false);
                FileUtility.writeFile(finalExpressionManifestJsonPath, expressionFormJsonEntityItem.getPath(), strFormExpression);
            });
        }
        String strExpressionManifestJsonEntity = SerializeUtility.getInstance().serialize(expressionManifestJsonEntity, false);
        // 写入表单 表达式清单文件
        FileUtility.writeFile(expressionManifestJsonPath, expressionManifest.getManifestJsonPath(), strExpressionManifestJsonEntity);

    }

}
