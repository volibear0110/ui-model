/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions.parser;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
/// <summary>
///  表达式包含字段参数匹配
/// </summary>
public class FieldsExpressionParser {
    public String Parse(String source, String entityName) {
        // 替换条件、case、普通实体及属性
        String parsedResult = source;
        Matcher matchedFieldsExpressions = MatchEntityFieldExpression(source, entityName);

        while (matchedFieldsExpressions.find()) {
            String matchedFieldsExpressionItemValue = matchedFieldsExpressions.group();

            Matcher matchedFieldRegex = MatchFieldExpression(entityName, matchedFieldsExpressionItemValue);
            if (matchedFieldRegex.find()) {
                String matchFieldRegexValue = matchedFieldRegex.group();
                // ENTITY~/a
                String result = matchFieldRegexValue.replace(entityName, "ENTITY~").replace(".", "/");

                // 进行括号参数替换
                result=EntityExpressionParser.replaceKuoHao(result);
                result = "ctx.getField('{" + result + "}')";



                String b = matchedFieldsExpressionItemValue.
                        replace("\"", "").replace("'", "").replace(matchFieldRegexValue, result);

                // 截取首次出现的地方
                int firstIndex = parsedResult.indexOf(matchedFieldsExpressionItemValue);

                if (firstIndex >= 0) {
                    StringBuilder sb = new StringBuilder(parsedResult);
                    parsedResult = sb.replace(firstIndex, firstIndex + matchedFieldsExpressionItemValue.length(), b).toString();
                }
            }
        }

        return parsedResult;
    }

    private static Matcher MatchFieldExpression(String entityName, String matchedFieldsExpressionItemValue) {
        Pattern fieldRegex = Pattern.compile(entityName + "[\\.\\[\\]a-zA-Z0-9_]+", Pattern.COMMENTS);
        return fieldRegex.matcher(matchedFieldsExpressionItemValue);
    }

    /// <summary>
    /// 表达式中包含字段参数匹配
    /// </summary>
    /// <param name="source"></param>
    /// <param name="entityName"></param>
    /// <returns></returns>
    private static Matcher MatchEntityFieldExpression(String source, String entityName) {
        String strPattern = "\\(?\\s*\\\"?\\s*" + entityName + "[\\.\\[\\]a-zA-Z0-9_]+\\s*\\\"?\\s*\\)?";
        Pattern regex = Pattern.compile(strPattern, Pattern.COMMENTS);
        return regex.matcher(source);
    }
}
