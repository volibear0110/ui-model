/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
@Data
public class ExpressionFormFieldJsonEntity {

    @JsonProperty("path")
    private String path;

    @JsonProperty("ns")
    private String ns;

    @JsonProperty("type")
    private String type;

    @JsonProperty("expressions")
    private List<ExpressionFormFieldExpression> expressions;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPath(String bindTo, String path) {
        this.path = "/" + bindTo + "/" + path;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ExpressionFormFieldExpression> getExpressions() {
        if(this.expressions==null){
            this.expressions=new ArrayList<>();
        }
        return expressions;
    }

    public void setExpressions(List<ExpressionFormFieldExpression> expressions) {
        this.expressions = expressions;
    }

}
