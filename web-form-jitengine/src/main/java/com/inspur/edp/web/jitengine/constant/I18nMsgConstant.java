package com.inspur.edp.web.jitengine.constant;

public class I18nMsgConstant {
    public final static String WEB_JIT_ENGINE_MSG_0001 = "WEB_JIT_ENGINE_MSG_0001";
    public final static String WEB_JIT_ENGINE_MSG_0002 = "WEB_JIT_ENGINE_MSG_0002";
    public final static String WEB_JIT_ENGINE_MSG_0003 = "WEB_JIT_ENGINE_MSG_0003";
    public final static String WEB_JIT_ENGINE_MSG_0004 = "WEB_JIT_ENGINE_MSG_0004";

}
