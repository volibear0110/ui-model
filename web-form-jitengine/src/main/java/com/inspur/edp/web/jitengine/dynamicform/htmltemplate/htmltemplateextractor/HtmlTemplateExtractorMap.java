/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.formmetadata.i18n.constant.ComponentType;

import java.util.HashMap;
import java.util.Map;

class HtmlTemplateExtractorMap {
    private static final Map<String, IHtmlTemplateExtractor> dicHtmlTemplateExtractor = new HashMap<>();

    static {
        dicHtmlTemplateExtractor.put(ComponentType.SECTION, new SectionHtmlTemplateExtractor());
        dicHtmlTemplateExtractor.put(ComponentType.ListView, new ListViewHtmlTemplateExtractor());
        dicHtmlTemplateExtractor.put(ComponentType.HTML_TEMPLATE, new HtmlTemplateHtmlTemplateExtractor());
        dicHtmlTemplateExtractor.put(ComponentType.GRID_FIELD, new GridFieldHtmlTemplateExtractor());
        dicHtmlTemplateExtractor.put(ComponentType.DATA_GRID, new DataGridHtmlTemplateExtractor());



    }


    /**
     * 获取对应的html 模板提取器
     *
     * @param componentType
     * @return
     */
    public static IHtmlTemplateExtractor getHtmlTemplateExtractor(String componentType) {
        if (!dicHtmlTemplateExtractor.containsKey(componentType)) {
            return null;
//            throw new WebCustomException("未定义的Html模板提取类型");
        }
        return dicHtmlTemplateExtractor.get(componentType);
    }


}
