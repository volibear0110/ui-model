/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions.utility;

import com.inspur.edp.web.jitengine.expressions.ModuleSchemaEntities;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
class FieldByIDAndVMIDResult {
    private Object schemaField;

    private boolean isRefElement;

    private String refElementLabelPath;

    private ModuleSchemaEntities relatedEntity;

    public Object getSchemaField() {
        return schemaField;
    }

    public void setSchemaField(Object schemaField) {
        this.schemaField = schemaField;
    }

    public boolean isRefElement() {
        return isRefElement;
    }

    public void setRefElement(boolean refElement) {
        isRefElement = refElement;
    }

    public String getRefElementLabelPath() {
        return refElementLabelPath;
    }

    public void setRefElementLabelPath(String refElementLabelPath) {
        this.refElementLabelPath = refElementLabelPath;
    }

    public ModuleSchemaEntities getRelatedEntity() {
        return relatedEntity;
    }

    public void setRelatedEntity(ModuleSchemaEntities relatedEntity) {
        this.relatedEntity = relatedEntity;
    }
}

