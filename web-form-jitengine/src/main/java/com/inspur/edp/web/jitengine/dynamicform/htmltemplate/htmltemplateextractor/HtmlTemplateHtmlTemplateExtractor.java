/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

public class HtmlTemplateHtmlTemplateExtractor  extends AbstractHtmlTemplateExtractor{
    @Override
    public void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        String htmlTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "html");
        if (!StringUtility.isNullOrEmpty(htmlTemplateContent)) {
            String htmlTemplateClassName = this.generateClassName("Html");
            HtmlTemplateEntity htmlTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            htmlTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            htmlTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            htmlTemplateHtmlTemplateEntity.setGeneratedClassContent(htmlTemplateContent);
            htmlTemplateHtmlTemplateEntity.setGeneratedClassName(htmlTemplateClassName);
            htmlTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"Html");
            htmlTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"Html");
            this.addIntoCollection(htmlTemplateHtmlTemplateEntity);
        }
    }
}
