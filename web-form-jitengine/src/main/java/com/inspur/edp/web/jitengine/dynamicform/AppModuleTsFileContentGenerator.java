/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform;

import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.ArrayList;
import java.util.List;

public class AppModuleTsFileContentGenerator {
    public String generate(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        StringBuilder sb = new StringBuilder();
        sb.append("import { CommonModule } from '@angular/common';\n");
        sb.append("import { NgModule } from '@angular/core';\n");
        sb.append("import { LangPipe } from './app.component';\n");
        for (HtmlTemplateEntity item : htmlTemplateEntityList) {
            sb.append("import { " + item.getGeneratedClassName() + "} from './app.component';" + "\n");
        }


        sb.append("@NgModule({" + "\n");
        sb.append("declarations:[" + "\n");
        for (HtmlTemplateEntity htmlTemplateEntity : htmlTemplateEntityList) {
            sb.append(htmlTemplateEntity.getGeneratedClassName());
            sb.append(",");
        }
        sb.append("LangPipe]," + "\n");

        sb.append("imports:[ CommonModule ]," + "\n");

        sb.append("bootstrap:[" + "\n");
        for (int i = 0; i < htmlTemplateEntityList.size(); i++) {
            sb.append(htmlTemplateEntityList.get(i).getGeneratedClassName());
            if (i < htmlTemplateEntityList.size() - 1) {
                sb.append(",");
            }
        }
        sb.append("\n" + "]" + "\n");
        sb.append("})" + "\n");
        sb.append("export class AppModule { }" + "\n");
        return sb.toString();
    }

    private static class NgModuleClass {
        public List<String> declarations;
        public List<String> imports;
        public List<String> bootstrap;

        public NgModuleClass() {
            this.declarations = new ArrayList<>();
            this.declarations.add("LangPipe");

            this.imports = new ArrayList<>();
            this.imports.add("CommonModule");

            this.bootstrap = new ArrayList<>();
        }
    }
}
