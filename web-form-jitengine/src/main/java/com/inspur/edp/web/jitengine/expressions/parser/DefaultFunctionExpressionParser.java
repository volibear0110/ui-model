/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions.parser;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
/// <summary>
///  表达式 defaultFunction 解析
///  将defaultFunction替换成为ctx
/// </summary>
public class DefaultFunctionExpressionParser {
    private final Pattern regex;

    public DefaultFunctionExpressionParser() {
        regex = Pattern.compile("DefaultFunction.[^\n(]+",Pattern.COMMENTS);
    }

    /// <summary>
    /// defaultfunction 的正则匹配 及参数替换操作
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public String Parse(String source) {
        String parsedResult = source;
        Matcher defaultFunctionMatches = regex.matcher(source);

        while (defaultFunctionMatches.find()) {
            String matchedValue = defaultFunctionMatches.group();
            String[] chainedMethods = matchedValue.split("\\.");
            if (chainedMethods.length > 1) {
                String matchedMethod = chainedMethods[1];
                // 如果为固定方法名 进行替换
                if ("GetContextParameter".equals(matchedMethod)) {
                    matchedMethod = "getState";
                }
                String lowerMethod = FirstToLower(matchedMethod);
                String target = "ctx." + lowerMethod;
                parsedResult = parsedResult.replace(matchedValue, target);
            }
        }
        return parsedResult;
    }

    /// <summary>
    ///  首字母小写
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public String FirstToLower(String source) {
        if (StringUtility.isNullOrEmpty(source)) {
            return "";
        }
        return source.substring(0, 1).toLowerCase() + source.substring(1);
    }


}
