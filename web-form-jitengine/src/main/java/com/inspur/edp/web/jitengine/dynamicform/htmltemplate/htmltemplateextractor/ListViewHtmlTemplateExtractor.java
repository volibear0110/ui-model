/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.dynamicform.htmltemplate.htmltemplateextractor;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.jitengine.dynamicform.htmltemplate.HtmlTemplateEntity;

import java.util.List;

public class ListViewHtmlTemplateExtractor extends AbstractHtmlTemplateExtractor  {
    @Override
    public void extractHtmlTemplate(List<HtmlTemplateEntity> htmlTemplateEntityList) {
        String headerTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "headerTemplate");
        if (!StringUtility.isNullOrEmpty(headerTemplateContent)) {
            String headerTemplateClassName = this.generateClassName("HeaderTemplate");
            HtmlTemplateEntity titleTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            titleTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            titleTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            titleTemplateHtmlTemplateEntity.setGeneratedClassContent(headerTemplateContent);
            titleTemplateHtmlTemplateEntity.setGeneratedClassName(headerTemplateClassName);
            titleTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"HeaderTemplate");
            titleTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"HeaderTemplate");
            this.addIntoCollection(titleTemplateHtmlTemplateEntity);
        }

//        String contentTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "contentTemplate");
//        if (!StringUtility.isNullOrEmpty(contentTemplateContent)) {
//            String contentTemplateClassName = this.generateClassName("ContentTemplate");
//            HtmlTemplateEntity contentTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
//            contentTemplateHtmlTemplateEntity.setId(this.extractComponentId());
//            contentTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
//            contentTemplateHtmlTemplateEntity.setGeneratedClassContent(contentTemplateContent);
//            contentTemplateHtmlTemplateEntity.setGeneratedClassName(contentTemplateClassName);
//            contentTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"ContentTemplate");
//            contentTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"ContentTemplate");
//            this.addIntoCollection(contentTemplateHtmlTemplateEntity);
//        }

        String footerTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "footerTemplate");
        if (!StringUtility.isNullOrEmpty(footerTemplateContent)) {
            String footerTemplateClassName = this.generateClassName("FooterTemplate");
            HtmlTemplateEntity footerTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            footerTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            footerTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            footerTemplateHtmlTemplateEntity.setGeneratedClassContent(footerTemplateContent);
            footerTemplateHtmlTemplateEntity.setGeneratedClassName(footerTemplateClassName);
            footerTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"FooterTemplate");
            footerTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"FooterTemplate");
            this.addIntoCollection(footerTemplateHtmlTemplateEntity);
        }

        String emptyTemplateContent = ComponentUtility.getInstance().getValue(this.getCurrentComponent(), "emptyTemplate");
        if (!StringUtility.isNullOrEmpty(emptyTemplateContent)) {
            String emptyTemplateClassName = this.generateClassName("EmptyTemplate");
            HtmlTemplateEntity emptyTemplateHtmlTemplateEntity = new HtmlTemplateEntity();
            emptyTemplateHtmlTemplateEntity.setId(this.extractComponentId());
            emptyTemplateHtmlTemplateEntity.setControlType(this.extractComponentType());
            emptyTemplateHtmlTemplateEntity.setGeneratedClassContent(emptyTemplateContent);
            emptyTemplateHtmlTemplateEntity.setGeneratedClassName(emptyTemplateClassName);
            emptyTemplateHtmlTemplateEntity.setSelectorName(this.extractComponentId()+"EmptyTemplate");
            emptyTemplateHtmlTemplateEntity.setComponentName(this.extractComponentId()+"EmptyTemplate");
            this.addIntoCollection(emptyTemplateHtmlTemplateEntity);
        }
    }
}
