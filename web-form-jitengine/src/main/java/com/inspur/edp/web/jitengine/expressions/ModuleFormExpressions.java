/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/07
 */
public class ModuleFormExpressions {
    private String formModuleCode;
    private String nameSpace;
    private String formExpressionJsonPath;
    private List<ModuleFormExpressionFieldItem> expressions;

    public String getFormModuleCode() {
        return formModuleCode;
    }

    public void setFormModuleCode(String formModuleCode) {
        this.formModuleCode = formModuleCode;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getFormExpressionJsonPath() {
        return formExpressionJsonPath;
    }

    public void setFormExpressionJsonPath(String formExpressionJsonPath) {
        this.formExpressionJsonPath = formExpressionJsonPath;
    }

    public List<ModuleFormExpressionFieldItem> getExpressions() {
        if (this.expressions == null) {
            this.expressions = new ArrayList<>();
        }
        return expressions;
    }

    public void setExpressions(List<ModuleFormExpressionFieldItem> expressions) {
        this.expressions = expressions;
    }
}
