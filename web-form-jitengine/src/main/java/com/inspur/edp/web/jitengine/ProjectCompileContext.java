/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 *
 * @author guozhiqi
 */
public class ProjectCompileContext {

    private boolean generateViewModel;

    public boolean isGenerateViewModel() {
        return generateViewModel;
    }

    public void setGenerateViewModel(boolean generateViewModel) {
        this.generateViewModel = generateViewModel;
    }

    private String projectName;

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    private String projectRoute;

    public String getProjectRoute() {
        return this.projectRoute;
    }

    public void setProjectRoute(String projectRoute) {
        this.projectRoute = projectRoute;
    }

    private String nodeModulesAbsolutePath;

    private boolean isJieXiForm = false;

    public boolean isJieXiForm() {
        return isJieXiForm;
    }

    public void setJieXiForm(boolean jieXiForm) {
        isJieXiForm = jieXiForm;
    }

    public String getNodeModulesAbsolutePath() {
        return this.nodeModulesAbsolutePath;
    }

    public void setNodeModulesAbsolutePath(String nodeModulesAbsolutePath) {
        this.nodeModulesAbsolutePath = nodeModulesAbsolutePath;
    }

    private boolean isMobileApprove = false;

    public boolean getIsMobileApprove() {
        return isMobileApprove;
    }

    public void setIsMobileApprove(boolean mobileApprove) {
        isMobileApprove = mobileApprove;
    }


    private String mobileApproveFormCode;


    public String getMobileApproveFormCode() {
        return mobileApproveFormCode;
    }

    public void setMobileApproveFormCode(String mobileApproveFormCode) {
        this.mobileApproveFormCode = mobileApproveFormCode;
    }

    /**
     * 表单生成的json文件路径
     */
    private String formJsonAbsolutePath;

    public String getFormJsonAbsolutePath() {
        return this.formJsonAbsolutePath;
    }

    public void setFormJsonAbsolutePath(String formJsonAbsolutePath) {
        this.formJsonAbsolutePath = formJsonAbsolutePath;
    }

    /**
     * 工程发布路径
     */
    private String formPublishAbsolutePath;

    public String getFormPublishAbsolutePath() {
        return this.formPublishAbsolutePath;
    }

    public void setFormPublishAbsolutePath(String formPublishAbsolutePath) {
        this.formPublishAbsolutePath = formPublishAbsolutePath;
    }

    private String eapiAbsolutePath;

    public String getEapiAbsolutePath() {
        return this.eapiAbsolutePath;
    }

    public void setEapiAbsolutePath(String eapiAbsolutePath) {
        this.eapiAbsolutePath = eapiAbsolutePath;
    }

    public String getAngularVersion() {
        return "ng7";
    }

    public String getUserInterfaceLibrary() {
        return "kendo";
    }

    private FormDOM json = null;

    public FormDOM getFormDom() {
        return this.json;
    }

    public void setFormDom(FormDOM formDom) {
        this.json = formDom;
    }

    public ExecuteEnvironment executeEnvironment = ExecuteEnvironment.Design;


    /**
     * 微服务单元路径
     */
    private String serviceUnitPath;

    public String getServiceUnitPath() {
        return this.serviceUnitPath;
    }

    public void setServiceUnitPath(String serviceUnitPath) {
        this.serviceUnitPath = serviceUnitPath;
    }

    /**
     * 运行时定制维度信息
     */
    public String extraFormPath = "";

    private boolean deleteSourceCodeBeforeGenerate = false;

    /**
     * 设置生成前删除源文件目录
     *
     * @return
     */
    public boolean isDeleteSourceCodeBeforeGenerate() {
        return deleteSourceCodeBeforeGenerate;
    }

    /**
     * 设置生成前删除源文件目录
     *
     * @param deleteSourceCodeBeforeGenerate
     */
    public void setDeleteSourceCodeBeforeGenerate(boolean deleteSourceCodeBeforeGenerate) {
        this.deleteSourceCodeBeforeGenerate = deleteSourceCodeBeforeGenerate;
    }

    private String formType = "pc";
    private String frameworkType = "angular";
    private String frameworkVersion = "7.0.0";

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFrameworkType() {
        return frameworkType;
    }

    public void setFrameworkType(String frameworkType) {
        this.frameworkType = frameworkType;
    }

    public String getFrameworkVersion() {
        return frameworkVersion;
    }

    public void setFrameworkVersion(String frameworkVersion) {
        this.frameworkVersion = frameworkVersion;
    }

    public ProjectCompileContext(String projectName, String refNodeModulesPath,
                                 String formType, String frameworkType,
                                 String formJsonAbsolutePath,
                                 String formPublishAbsolutePath, String eapiAbsolutePath, String serviceUnitPath, ExecuteEnvironment executeEnvironment) {
        this.projectName = projectName;
        this.projectRoute = projectName + JITEngineConstants.ProjectRouteFileExtension;
        this.nodeModulesAbsolutePath = refNodeModulesPath;
        this.formType = formType;
        this.frameworkType = frameworkType;

        this.formJsonAbsolutePath = formJsonAbsolutePath;
        this.formPublishAbsolutePath = formPublishAbsolutePath;
        this.eapiAbsolutePath = eapiAbsolutePath;
        this.serviceUnitPath = serviceUnitPath;
        this.executeEnvironment = executeEnvironment;
    }
}
