/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.metadataanalysis;

import com.inspur.edp.web.common.io.FileUtility;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import static org.junit.Assert.*;

public class CommandServiceAnalysisTest {

    @Test
    public void test() {
        Path path = Paths.get("dd\\dsds\\dsdsd\\dsdsdddddd");
        File file = new File("dd\\dsdsd\\dsdsdsds\\gfdgdfgdfgdfgdfg");
        assertEquals("gfdgdfgdfgdfgdfg", file.getName());

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("path", "nohhhh\\fdfd\\fffff");

        String extensionResult = hashMap.get("path").substring(hashMap.get("path").lastIndexOf("/") + 1);
        assertEquals(extensionResult, "nohhhh\\fdfd\\fffff");

        String fileNameResult = FileUtility.getFileName("nohhhh\\fdfd\\fffff");
        assertEquals(fileNameResult, "fffff");


        File extensionFile = new File("fff/fff.txt");

    }

    @Test
    public void testFileExtension() {
        String result = "dd/ddd/dddddd.txt";
        String strExtension = FileUtility.getFileExtension(result);
        System.out.println(strExtension);
    }
}