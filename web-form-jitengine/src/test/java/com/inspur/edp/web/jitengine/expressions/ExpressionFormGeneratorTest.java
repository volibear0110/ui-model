/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.expressions;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ExpressionFormGeneratorTest {


    @Test
    void testGenerate() {
        String fileContent = FileUtility.readAsString("D:\\InspurCode\\N转J之后代码\\tag2103\\web\\web-form-jitengine\\src\\test\\java\\com\\inspur\\edp\\web\\jitengine\\expressions\\expensemrgtxflcjt.frm.json");
        FormDOM formDOM = SerializeUtility.getInstance().deserialize(fileContent, FormDOM.class);
        ModuleFormExpressions expressions = ExpressionFormGenerator.generate(formDOM, "", null);

        ExpressionManifest expressionManifest = new ExpressionManifest();


        List<ModuleFormExpressions> eeeeeee = new ArrayList<>();
        eeeeeee.add(expressions);

        expressionManifest.setExpressions(eeeeeee);
        expressionManifest.setFormModuleCode("ddddddd");
        expressionManifest.setManifestJsonPath("c:/dddd.json");
        ExpressionManifestManager.writeExpressionJson(expressionManifest, "c:/dddd", false);

    }


}
