/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.webservice;

import org.junit.jupiter.api.Test;

class NpmPacakgeWebServiceImplTest {

    @Test
    void fromCharCode() {
//        NpmPacakgeWebServiceImpl impl=new NpmPacakgeWebServiceImpl();
//        String result= impl.fromCharCode(3,"guozhiqi");
//        System.out.println(result);
//
//
//       String des= this.fromCharCode(3,result);
//       System.out.println(des);
    }

    public String fromCharCode(int offset, String source) {
        char[] arrCharSource = source.toCharArray();
        StringBuilder builder = new StringBuilder(arrCharSource.length);

        for (char codePoint : arrCharSource) {

            builder.append((char) (codePoint - offset));
        }
        return builder.toString();
    }
}
