/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.appconfig.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.web.common.serialize.SerializeUtility;

/**
 * 前端工程应用配置实体
 * @author guozhiqi
 */
public class GspAppConfig {
    /**
     * 避免直接实例化 采用getNewInstance的形式进行实例化
     */
    private  GspAppConfig(){}

    /**
     * PC页面流元数据ID
     */
    @JsonProperty("pageFlowMetadataID")
    private String pageFlowMetadataID;

    /**
     * PC页面流元数据文件名
     */
    @JsonProperty("pageFlowMetadataFileName")
    private String pageFlowMetadataFileName;
    /**
     * PC页面流元数据ID
     * 全路径，包含projects信息
     */
    @JsonProperty("pageFlowMetadataPath")
    private String pageFlowMetadataPath;

    /**
     * 移动页面流元数据ID
     */
    @JsonProperty("mobilePageFlowMetadataID")
    private String mobilePageFlowMetadataID;

    /**
     * 移动页面流元数据文件名
     */
    @JsonProperty("mobilePageFlowMetadataFileName")
    private String mobilePageFlowMetadataFileName;
    /**
     * 移动页面流元数据ID
     * 全路径，包含projects信息
     */
    @JsonProperty("mobilePageFlowMetadataPath")
    private String mobilePageFlowMetadataPath;

    public String getPageFlowMetadataID() {
        return pageFlowMetadataID;
    }

    public void setPageFlowMetadataID(String pageFlowMetadataID) {
        this.pageFlowMetadataID = pageFlowMetadataID;
    }

    public String getPageFlowMetadataFileName() {
        return pageFlowMetadataFileName;
    }

    public void setPageFlowMetadataFileName(String pageFlowMetadataFileName) {
        this.pageFlowMetadataFileName = pageFlowMetadataFileName;
    }

    public String getPageFlowMetadataPath() {
        return pageFlowMetadataPath;
    }

    public void setPageFlowMetadataPath(String pageFlowMetadataPath) {
        this.pageFlowMetadataPath = pageFlowMetadataPath;
    }

    public String getMobilePageFlowMetadataID() {
        return mobilePageFlowMetadataID;
    }

    public void setMobilePageFlowMetadataID(String mobilePageFlowMetadataID) {
        this.mobilePageFlowMetadataID = mobilePageFlowMetadataID;
    }

    public String getMobilePageFlowMetadataFileName() {
        return mobilePageFlowMetadataFileName;
    }

    public void setMobilePageFlowMetadataFileName(String mobilePageFlowMetadataFileName) {
        this.mobilePageFlowMetadataFileName = mobilePageFlowMetadataFileName;
    }

    public String getMobilePageFlowMetadataPath() {
        return mobilePageFlowMetadataPath;
    }

    public void setMobilePageFlowMetadataPath(String mobilePageFlowMetadataPath) {
        this.mobilePageFlowMetadataPath = mobilePageFlowMetadataPath;
    }

    /**
     * 将当前实体进行JSON序列化
     */
    public final String toJson() {
        return SerializeUtility.getInstance().serialize(this, false);
    }

    /**
     * 获取对应实例
     *
     * @return
     */
    public static GspAppConfig getNewInstance() {
        return new GspAppConfig();
    }
}
