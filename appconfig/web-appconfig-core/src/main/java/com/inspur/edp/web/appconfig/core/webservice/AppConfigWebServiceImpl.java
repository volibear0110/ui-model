/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.appconfig.core.webservice;

import com.inspur.edp.web.appconfig.api.entity.GspAppConfig;
import com.inspur.edp.web.appconfig.api.webservice.AppConfigWebService;
import com.inspur.edp.web.appconfig.core.service.GspAppConfigService;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/14
 */
public class AppConfigWebServiceImpl implements AppConfigWebService {
    /**
     * 获取应用配置文件内容
     *
     * @param projectPath 工程路径
     * @return <see cref="GspProject"/>
     */
    @Override
    public GspAppConfig getGspAppConfigContent(String projectPath) {

        return GspAppConfigService.getCurrent().getGspAppConfigInfo(projectPath);
    }

    /**
     * 保存应用配置文件内容
     */
    @Override
    public void saveGspAppConfigContent(GspAppConfig appConfigEntity, String projectPath) {
        GspAppConfigService.getCurrent().saveAppConfigFile(projectPath, appConfigEntity);
    }
}
