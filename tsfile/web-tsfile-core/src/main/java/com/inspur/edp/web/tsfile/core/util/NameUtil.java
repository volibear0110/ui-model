/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.tsfile.core.util;

public class NameUtil {
    public static String getPascal(String name) {
        if (name.contains("-")) {
            name = name.replace('-', '_');
        }
        String[] parts = name.split("[_]", -1);
        StringBuilder sb = new StringBuilder();
        for (String part : parts) {
            sb.append(getCapitalized(part));
        }

        return sb.toString();
    }

    /**
     * 首字母大写
     *
     * @param name
     * @return
     */
    public static String getCapitalized(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
