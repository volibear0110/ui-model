/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.tsfile.core.service;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.tsfile.api.service.TsFileService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author guozhiqi
 */
public class TsFileManagerService {
    /**
     * 注入TsFileService
     */
    @Resource
    private TsFileService tsFileService;

    public final void deleteTsFile(String fullPath) {
        tsFileService.deleteTsFile(fullPath);
    }

    public final List<String> getTsFileList(String path) {
        return tsFileService.getTsFileList(path);
    }

    public final boolean isTsFileExist(String path, String fileName) {
        String filePathAndName = FileUtility.combine(path, fileName);
        return tsFileService.isTsFileExist(filePathAndName);
    }

    public final boolean isTsFileExist(String fullPath) {
        return tsFileService.isTsFileExist(fullPath);
    }

    public final String loadTsFile(String fullPath) {
        return tsFileService.loadTsFileContent(fullPath);
    }

    public final String loadTsFileByWebCmp(String formRelativePath, String webCmpId) {
        return tsFileService.loadTsFileContentByWebCmp(formRelativePath, webCmpId);
    }

    public final void renameTsFile(String fullPath, String newName) {
        tsFileService.renameTsFile(fullPath, newName);
    }

    public final void saveTsFile(String fullPath, String content) {
        tsFileService.saveTsFile(fullPath, content);
    }

    public final void createTypescriptFile(String fullPath) {
        String normalized = tsFileService.getTsClassNameByPath(fullPath);

        String contentSB = "import { Injectable } from '@angular/core';" + "\r\n" +
                "\r\n@Injectable()" + "\r\n" +
                "export class " + normalized + " {" + "\r\n" +
                "  constructor() {}" + "\r\n" +
                "}" + "\r\n";
        tsFileService.saveTsFile(fullPath, contentSB);
    }

    public final void createMobileTsFile(String fullPath) {
        String normalized = tsFileService.getTsClassNameByPath(fullPath);

        String contentSB = "import { BaseDataService } from '@farris/mobile-command-services';\r\n" +
                "import { ViewModelContext} from '@farris/mobile-devkit';\r\n" +

                "\r\nexport class " + normalized + " extends BaseDataService {\r\n" +
                "  constructor(viewModelContext: ViewModelContext) {\r\n" +
                "    super(viewModelContext)\r\n" +
                "  }\r\n" +
                "}\r\n";
        tsFileService.saveTsFile(fullPath, contentSB);
    }
}
