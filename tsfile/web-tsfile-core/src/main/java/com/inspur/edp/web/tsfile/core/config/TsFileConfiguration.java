/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.tsfile.core.config;

import com.inspur.edp.web.tsfile.api.service.TsFileService;
import com.inspur.edp.web.tsfile.core.TsFileWebServiceImpl;
import com.inspur.edp.web.tsfile.core.service.TsFileManagerService;
import com.inspur.edp.web.tsfile.core.service.TsFileServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * description: 定义ts文件定义的自定义restful url地址
 *
 * @author Noah Guo
 * @date 2021/01/19
 */
@Configuration("com.inspur.edp.web.tsfile.core.config.TsFileConfiguration")
public class TsFileConfiguration {
    @Bean
    public TsFileService tsFileService() {
        return new TsFileServiceImpl();
    }

    @Bean
    public TsFileManagerService tsFileManagerService() {
        return new TsFileManagerService();
    }

    @Bean()
    public RESTEndpoint tsFileWebapiEndPoint(TsFileManagerService tsFileManagerService) {
        return new RESTEndpoint("/dev/main/v1.0/tsfile", new TsFileWebServiceImpl(tsFileManagerService));
    }
}
