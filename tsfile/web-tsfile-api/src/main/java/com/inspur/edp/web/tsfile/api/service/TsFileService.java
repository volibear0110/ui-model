/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.tsfile.api.service;

import java.util.ArrayList;
import java.util.List;

/**
 * ts文件对外暴露service
 *
 * @author noah
 */
public interface TsFileService {

    /**
     * 保存TS文件，包括新增和修改
     *
     * @param fullPath 文件相对于开发根目录的路径，包含文件名和扩展名
     * @param content  文件内容
     */
    void saveTsFile(String fullPath, String content);

    /**
     * 获取TS文件内容
     *
     * @param fullPath 文件相对于开发根目录的路径，包含文件名和扩展名
     */
    String loadTsFileContent(String fullPath);

    /**
     * 根据web构件获取对应TS文件内容
     *
     * @param formRelativePath 所属表单的相对路径
     * @param webCmpId
     * @return
     */
    String loadTsFileContentByWebCmp(String formRelativePath, String webCmpId);

    /**
     * 依据ts文件路径构造对应的ts 中class名称
     *
     * @param tsFilePath
     * @return
     */
    String getTsClassNameByPath(String tsFilePath);

    /**
     * 删除指定的TS文件
     *
     * @param fullPath 文件相对于开发根目录的路径，包含文件名和扩展名
     */
    void deleteTsFile(String fullPath);

    /**
     * 重命名指定的文件
     *
     * @param fullPath 文件相对于开发根目录的路径，包含文件名和扩展名
     * @param newName  新文件名
     */
    void renameTsFile(String fullPath, String newName);

    /**
     * 获取指定目录下的所有TS文件名
     *
     * @param path 文件相对于开发根目录的路径，不含文件名
     */
    List<String> getTsFileList(String path);


    /**
     * 判断指定文件是否存在
     *
     * @param fullPath 文件相对于开发根目录的路径，包含文件名和扩展名
     * @return
     */
    boolean isTsFileExist(String fullPath);
}
