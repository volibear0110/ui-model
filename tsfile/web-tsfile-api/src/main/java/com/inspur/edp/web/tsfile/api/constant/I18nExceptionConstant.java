package com.inspur.edp.web.tsfile.api.constant;

public class I18nExceptionConstant {
    public final static String WEB_TS_FILE_ERROR_0001 = "WEB_TS_FILE_ERROR_0001";
    public final static String WEB_TS_FILE_ERROR_0002 = "WEB_TS_FILE_ERROR_0002";
}
