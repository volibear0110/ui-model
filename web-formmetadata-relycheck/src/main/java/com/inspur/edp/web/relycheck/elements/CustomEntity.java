/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.relycheck.elements;

import com.inspur.edp.web.relycheck.elements.type.CustomEntityTypeBase;

/**
 * 实体元素
 * @author  noah
 */
public class CustomEntity {
    /**
     * 实体标识
     */
    private String Id;

    public final String getId() {
        return Id;
    }

    public final void setId(String value) {
        Id = value;
    }

    /**
     * 实体编号
     */
    private String Code;

    public final String getCode() {
        return Code;
    }

    public final void setCode(String value) {
        Code = value;
    }

    /**
     * 实体名称
     */
    private String Name;

    public final String getName() {
        return Name;
    }

    public final void setName(String value) {
        Name = value;
    }

    /**
     * 实体标签，即别名
     */
    private String Label;

    public final String getLabel() {
        return Label;
    }

    public final void setLabel(String value) {
        Label = value;
    }

    /**
     * 实体类型描述
     */
    private CustomEntityTypeBase Type;

    public final CustomEntityTypeBase getType() {
        return Type;
    }

    public final void setType(CustomEntityTypeBase value) {
        Type = value;
    }
}