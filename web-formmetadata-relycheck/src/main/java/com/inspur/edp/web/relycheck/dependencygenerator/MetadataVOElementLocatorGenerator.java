package com.inspur.edp.web.relycheck.dependencygenerator;

import com.inspur.edp.lcm.metadata.spi.entity.MetadataElementLocator;
import com.inspur.edp.web.relycheck.constant.RelyCheckType;

import java.util.function.Supplier;

/**
 * metadataElementLocator 实例构造
 *
 * @author noah
 */
class MetadataVOElementLocatorGenerator {


    /**
     * 构造 metadataElementLocator 实例
     * 主要参数是构造对应的 id 及 id
     *
     * @param id
     * @return
     */
    static MetadataElementLocator generate(RelyCheckType relyCheckType, String id) {
        MetadataElementLocator metadataElementLocator = new MetadataElementLocator();
        metadataElementLocator.setId(id);
        metadataElementLocator.setPath(relyCheckType.getReferenceType());
        return metadataElementLocator;
    }

    /**
     * 构造 metadataElementLocator 实例
     * 主要参数是构造对应的 id 及 path
     *
     * @param idSupplier
     * @param pathSupplier
     * @return
     */
    static MetadataElementLocator generate(Supplier<String> idSupplier, Supplier<String> pathSupplier) {
        MetadataElementLocator metadataElementLocator = new MetadataElementLocator();
        if (idSupplier != null) {
            metadataElementLocator.setId(idSupplier.get());
        }
        if (pathSupplier != null) {
            metadataElementLocator.setPath(pathSupplier.get());
        }

        return metadataElementLocator;
    }
}
