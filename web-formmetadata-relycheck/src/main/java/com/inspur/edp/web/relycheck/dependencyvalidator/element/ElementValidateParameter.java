package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.relycheck.dependencygenerator.entity.SourceElementField;
import lombok.Data;

/**
 * element 验证参数
 *
 * @author noah
 * 2023/8/10 10:48
 */
@Data
public class ElementValidateParameter {
    /**
     * 校验源字段
     */
    private SourceElementField sourceElementField;
    /**
     * 校验viewmodel 字段
     */
    private GspViewModelElement gspViewModelElement;
    /**
     * 校验  表单元数据
     */
    private GspMetadata formMetadata;
    /**
     * 校验 viewmodel 元数据
     */
    private GspMetadata viewModelMetadata;

    /**
     * vo 元数据 name
     */
    private String voMetadataName;

    /**
     * 表单元数据 name
     */
    private String formMetadataName;

    /**
     * vo 关联实体 code
     */
    private String entityCode;
}
