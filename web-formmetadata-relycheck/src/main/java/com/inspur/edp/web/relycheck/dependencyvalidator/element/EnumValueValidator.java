package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.relycheck.constant.ValidateConstants;
import com.inspur.edp.web.relycheck.elements.type.CustomEnumItem;
import com.inspur.edp.web.relycheck.utility.StringConcatUtility;
import com.inspur.edp.web.relycheck.utility.ValidateResultUtility;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 枚举元素验证
 *
 * @author noah
 * 2023/8/10 16:56
 */
public class EnumValueValidator extends BaseElementValidator {
    @Override
    public ValidateResult validate(ElementValidateParameter parameter) {
        ValidateResult warningValidateResult = ValidateResultUtility.success();

        GspEnumValueCollection voEnumValueCollection = parameter.getGspViewModelElement().getContainEnumValues();
        List<CustomEnumItem> fieldCustomEnumItemList = this.getSourceField(parameter).getType().getEnumValues();

        // 必须是枚举类型比较对应的枚举值
        if (this.getSourceField(parameter).getType().get$type().equals("EnumType") && parameter.getGspViewModelElement().getObjectType().name().equals("Enum")) {
//                if (voEnumValueCollection.size() != fieldCustomEnumItemList.size()) {
//                    return this.failureWithError(String.format("视图对象%1$s中子表%2$s更改了%3$s的枚举项。为使表单生效" + ValidateConstants.Comma + "请在表单%4$s的实体树中执行更新schema操作",
//                            StringConcatUtility.concatWithNamePrefix(voMetadataName, true),
//                            StringConcatUtility.concatWithCodePrefix(entityCode, true),
//                            StringConcatUtility.concatWithCodePrefix(field.getSourceField().getCode(), true),
//                            StringConcatUtility.concatWithNamePrefix(formMetadataName, true)
//                    ));
//                }

            List<CustomEnumItem> customEnumItemList = voEnumValueCollection.stream().map(t -> {
                CustomEnumItem customEnumItem = new CustomEnumItem();
                customEnumItem.setName(t.getName());
                customEnumItem.setValue(t.getValue());
                return customEnumItem;
            }).collect(Collectors.toList());
            String serializedVoEnum = SerializeUtility.getInstance().serialize(customEnumItemList);
            String serializedFieldEnum = SerializeUtility.getInstance().serialize(fieldCustomEnumItemList);
            if (!serializedVoEnum.equals(serializedFieldEnum)) {
                return ValidateResultUtility.failureWithError(String.format("视图对象%1$s中子表%2$s更改了%3$s的枚举项，其中视图对象中枚举项为:%5$s，表单字段枚举项为:%6$s。为使表单生效" + ValidateConstants.Comma + "请在表单%4$s的实体树中执行更新schema操作",
                        StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                        StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true),
                        serializedVoEnum, serializedFieldEnum
                ));
            }
        }
        return warningValidateResult;
    }
}
