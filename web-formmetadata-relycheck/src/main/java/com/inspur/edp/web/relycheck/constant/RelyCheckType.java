package com.inspur.edp.web.relycheck.constant;

/**
 * 一致性检查类型定义
 *
 * @author noah
 */
public enum RelyCheckType {
    /**
     * schema entity 实体类型
     */
    SchemaEntity {
        @Override
        public String getType() {
            return "SchemaEntity";
        }

        @Override
        public String getReferenceType() {
            return "VoEntity";
        }
//
//        @Override
//        public String getValidateRefType() {
//            return "Schema-VoEntity";
//        }
    },
    /**
     * schema field 字段类型
     */
    SchemaField {
        @Override
        public String getType() {
            return "SchemaField";
        }

        @Override
        public String getReferenceType() {
            return "VoElement";
        }

//        @Override
//        public String getValidateRefType() {
//            return "Schema-VoElement";
//        }
    };

    /**
     * 获取具体的类型参数字符串表示
     *
     * @return
     */
    public abstract String getType();

    /**
     * 获取对应的referenceType参数值
     *
     * @return
     */
    public abstract String getReferenceType();
}
