package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.web.relycheck.constant.ValidateConstants;
import com.inspur.edp.web.relycheck.utility.StringConcatUtility;
import com.inspur.edp.web.relycheck.utility.ValidateResultUtility;

/**
 * 必填校验
 *
 * @author noah
 * 2023/8/10 14:00
 */
public class RequireValidator extends BaseElementValidator {
    @Override
    public ValidateResult validate(ElementValidateParameter parameter) {
        ValidateResult warningValidateResult = this.success();
        boolean voElementRequire = parameter.getGspViewModelElement().getIsRequire();
        boolean fieldRequire = this.getSourceField(parameter).getRequire();
        if (!voElementRequire && fieldRequire) {
            warningValidateResult = ValidateResultUtility.failureWithWarning(String.format("视图对象%1$s中子表%2$s的字段%3$s由必填调整为非必填" + ValidateConstants.Comma + "而表单对应字段为必填。若要使表单生效" + ValidateConstants.Comma + "请在表单%4$s的实体树中执行更新schema操作",
                    StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                    StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
            ));
        }
        if (voElementRequire && !fieldRequire) {
            return ValidateResultUtility.failureWithError(String.format("视图对象%1$s中子表%2$s的字段%3$s启用必填属性" + ValidateConstants.Comma + "而表单对应字段为非必填。若要使表单控件生效" + ValidateConstants.Comma + "请在表单%4$s的实体树中执行更新schema操作",
                    StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                    StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
            ));
        }
        return warningValidateResult;
    }
}
