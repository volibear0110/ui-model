package com.inspur.edp.web.relycheck.elements;



/**
 * 增加此属性的目的 是因为反序列化需要到具体的子类，但鉴于反序列化到子类需要调整较多源代码
 * 因此将子类的属性复制到该类中，用于进行反序列化
 * @author noah
 * 2023/8/3 14:35
 */
public class CustomField extends BaseField {

    private String defaultValue;

    public final String getDefaultValue() {
        return defaultValue;
    }

    public final void setDefaultValue(String value) {
        defaultValue = value;
    }


    private boolean require;

    public final boolean getRequire() {
        return require;
    }

    public final void setRequire(boolean value) {
        require = value;
    }


    private boolean readonly;

    public final boolean getReadonly() {
        return readonly;
    }

    public final void setReadonly(boolean value) {
        readonly = value;
    }


    private CustomFieldEditor editor ;

    public final CustomFieldEditor getEditor() {
        return editor;
    }

    public final void setEditor(CustomFieldEditor value) {
        editor = value;
    }


    private boolean multiLanguage = false;

    public final boolean getMultiLanguage() {
        return multiLanguage;
    }

    public final void setMultiLanguage(boolean value) {
        multiLanguage = value;
    }
}
