package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.web.relycheck.dependencygenerator.entity.SourceElementField;
import com.inspur.edp.web.relycheck.elements.CustomField;
import com.inspur.edp.web.relycheck.utility.ValidateResultUtility;

import java.util.Optional;


/**
 * element 依赖检查
 *
 * @author noah
 * 2023/8/10 10:45
 */
public abstract class BaseElementValidator {
    public abstract ValidateResult validate(ElementValidateParameter parameter);

    protected String getVoMetadataName(ElementValidateParameter parameter) {
        return Optional.ofNullable(parameter.getVoMetadataName()).orElseGet(() -> parameter.getViewModelMetadata().getHeader().getName());
    }

    protected String getEntityCode(ElementValidateParameter parameter) {
        return Optional.ofNullable(parameter.getEntityCode()).orElseGet(() -> parameter.getSourceElementField().getSourceEntity().getCode());
    }

    protected String getFormMetadataName(ElementValidateParameter parameter) {
        return Optional.ofNullable(parameter.getFormMetadataName()).orElseGet(() -> parameter.getFormMetadata().getHeader().getName());
    }

    protected String getSourceFieldCode(ElementValidateParameter parameter) {
        return parameter.getSourceElementField().getSourceField().getCode();
    }

    protected CustomField getSourceField(ElementValidateParameter parameter) {
        return parameter.getSourceElementField().getSourceField();
    }

    /**
     * 构造快捷访问
     *
     * @return
     */
    protected ValidateResult success() {
        return ValidateResultUtility.success();
    }

    protected ValidateResult failureWithWarning(String message) {
        return ValidateResultUtility.failureWithWarning(message);
    }

    protected ValidateResult failureWithError(String message){
        return ValidateResultUtility.failureWithError(message);
    }
}
