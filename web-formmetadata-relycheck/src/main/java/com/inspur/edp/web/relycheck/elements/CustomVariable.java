/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.relycheck.elements;

/**
 * 变量定义
 * @author  noah
 */
public class CustomVariable {
    private String Id;

    public final String getId() {
        return Id;
    }

    public final void setId(String value) {
        Id = value;
    }

    private String Code;

    public final String getCode() {
        return Code;
    }

    public final void setCode(String value) {
        Code = value;
    }

    private String Name;

    public final String getName() {
        return Name;
    }

    public final void setName(String value) {
        Name = value;
    }

    private String Type;

    public final String getType() {
        return Type;
    }

    public final void setType(String value) {
        Type = value;
    }
}