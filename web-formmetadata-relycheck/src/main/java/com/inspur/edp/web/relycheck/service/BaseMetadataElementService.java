package com.inspur.edp.web.relycheck.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataDependencyDetail;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElementDependencyDetail;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContentService;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.relycheck.dependencygenerator.SchemaEntityDependencyGenerator;
import com.inspur.edp.web.relycheck.elements.CustomSchema;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 元数据检测 service  base
 */
public abstract class BaseMetadataElementService implements ApplicationContextAware {
    protected ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * 获取表单元数据对应的依赖项
     *
     * @param gspMetadata
     * @return
     */
    protected List<MetadataDependencyDetail> getMetadataDependencyDetails(GspMetadata gspMetadata) {
        if (gspMetadata == null) {
            throw new WebCustomException("元数据一致性检查，getMetadataDependencyDetails 传递的gspMetadata参数为空");
        }
        if (gspMetadata.getContent() instanceof FormMetadataContent) {
            List<MetadataDependencyDetail> dependencyDetailList = new ArrayList<>();

            // 如果是表单元数据 那么提取其中的schema节点
            FormMetadataContent formMetadataContent = (FormMetadataContent) gspMetadata.getContent();
            FormDOM formDOM = FormMetadataContentService.getInstance().getFormContent(formMetadataContent);
            List<HashMap<String, Object>> schemsList = formDOM.getModule().getSchemas();
            schemsList.forEach(schemaItem -> {
                String voMetadataId = (String) schemaItem.get("id");
                if (StringUtility.isNotNullOrEmpty(voMetadataId)) {
                    SchemaEntityDependencyGenerator schemaEntityDependencyGenerator = this.applicationContext.getBean(SchemaEntityDependencyGenerator.class);

                    MetadataDependencyDetail schemaDependencyDetail = new MetadataDependencyDetail();
                    schemaDependencyDetail.setMetadataId(voMetadataId);

                    String serializedSchema = SerializeUtility.getInstance().serialize(schemaItem);
                    CustomSchema deserializedCustomSchema = SerializeUtility.getInstance().deserialize(serializedSchema, CustomSchema.class);
                    //  当前共检查两类
                    List<MetadataElementDependencyDetail> schemaEntityDependencyDetailList = schemaEntityDependencyGenerator.generateDependencyDetailWithSchemaEntityList(deserializedCustomSchema.getEntities(), formDOM, gspMetadata);
                    schemaDependencyDetail.setElementReferences(schemaEntityDependencyDetailList);

                    dependencyDetailList.add(schemaDependencyDetail);
                } else {
                    WebLogger.Instance.info(String.format("表单元数据依赖 VO metadataId 为空,对应表单元数据 id:%s,对应 namespace:%s", gspMetadata.getHeader().getId(), gspMetadata.getHeader().getNameSpace()));
                }
            });
            return dependencyDetailList;
        }
        return new ArrayList<>(0);
    }
}
