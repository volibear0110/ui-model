package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.web.relycheck.constant.ValidateConstants;
import com.inspur.edp.web.relycheck.utility.StringConcatUtility;
import com.inspur.edp.web.relycheck.utility.ValidateResultUtility;

/**
 * 是否多语验证
 * @author noah
 * 2023/8/10 16:35
 */
public class MultiLanguageValidator extends BaseElementValidator {
    @Override
    public ValidateResult validate(ElementValidateParameter parameter) {
        ValidateResult warningValidateResult = ValidateResultUtility.success();

        boolean voElementIsMultiLanguage = parameter.getGspViewModelElement().getIsMultiLanguage();
        boolean fieldIsMultiLanguage = this.getSourceField(parameter).getMultiLanguage();
        if (voElementIsMultiLanguage && !fieldIsMultiLanguage) {
            return ValidateResultUtility.failureWithError(String.format("视图对象%1$s中子表%2$s的字段%3$s启用多语属性" + ValidateConstants.Comma + "而表单对应字段不是多语属性。若要使表单控件变更为多语控件" + ValidateConstants.Comma + "请在表单%4$s的实体树中执行更新schema操作",
                    StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                    StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
            ));
        }
        if (!voElementIsMultiLanguage && fieldIsMultiLanguage) {
            return ValidateResultUtility.failureWithError(String.format("视图对象%1$s中子表%2$s的字段%3$s关闭多语属性" + ValidateConstants.Comma + "而表单对应字段仍是多语。若要同步变更表单控件" + ValidateConstants.Comma + "请在表单%4$s实体树中执行更新schema操作",
                    StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                    StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                    StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
            ));
        }
        return warningValidateResult;
    }
}
