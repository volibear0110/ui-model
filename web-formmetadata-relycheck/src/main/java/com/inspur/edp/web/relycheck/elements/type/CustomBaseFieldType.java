package com.inspur.edp.web.relycheck.elements.type;

import com.inspur.edp.web.relycheck.elements.CustomField;
import com.inspur.edp.web.relycheck.elements.CustomEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author noah
 * 2023/8/4 16:56
 */
public class CustomBaseFieldType extends BaseFieldType {


    private int Length;

    public final int getLength() {
        return Length;
    }

    public final void setLength(int value) {
        Length = value;
    }

    private int Precision;

    public final int getPrecision() {
        return Precision;
    }

    public final void setPrecision(int value) {
        Precision = value;
    }

    private String Primary;

    public final String getPrimary() {
        return Primary;
    }

    public final void setPrimary(String value) {
        Primary = value;
    }

    private List<CustomField> Fields = new ArrayList<>();

    public final List<CustomField> getFields() {
        return Fields;
    }

    public final void setFields(List<CustomField> value) {
        Fields = value;
    }

    private List<CustomEntity> Entities = new ArrayList<>();

    public final List<CustomEntity> getEntities() {
        return Entities;
    }

    public final void setEntities(List<CustomEntity> value) {
        Entities = value;
    }

    private ArrayList<CustomEnumItem> EnumValues = new ArrayList<>();

    public final ArrayList<CustomEnumItem> getEnumValues() {
        return EnumValues;
    }

    public final void setEnumValues(ArrayList<CustomEnumItem> value) {
        EnumValues = value;
    }


}
