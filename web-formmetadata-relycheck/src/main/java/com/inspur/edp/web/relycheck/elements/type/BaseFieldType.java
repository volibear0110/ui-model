/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.relycheck.elements.type;

/**
 * 字段类型
 * @author  noah
 */
public class BaseFieldType {
    private String $type;

    public String get$type() {
        return $type;
    }

    public void set$type(String $type) {
        this.$type = $type;
    }


    public BaseFieldType() {

    }
    public BaseFieldType(String _$type) {
        this.$type = _$type;
    }

    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    private String DisplayName;

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String value) {
        DisplayName = value;
    }
}
