package com.inspur.edp.web.relycheck.utility;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.relycheck.constant.ValidateConstants;

/**
 * @author noah
 * 2023/8/7 17:30
 */
public class StringConcatUtility {

    /**
     * 合并前缀 后缀及其对应的是否包含边界
     * @param prefix
     * @param suffix
     * @param includeBorder
     * @return
     */
    public static String concat(String prefix, String suffix, boolean includeBorder) {
        String result = !StringUtility.isNullOrEmpty(prefix) ? (prefix + ":" + suffix) : suffix;
        if (includeBorder) {
            return "【" + result + "】";
        }
        return result;
    }

    /**
     * 快捷方式 合并 名称前缀
     * @param suffix
     * @param includeBorder
     * @return
     */
    public static String concatWithNamePrefix(String suffix, boolean includeBorder) {
        return concat(ValidateConstants.Name, suffix, includeBorder);
    }

    /**
     * 快捷方式 合并编号前缀
     * @param suffix
     * @param includeBorder
     * @return
     */
    public static String concatWithCodePrefix(String suffix, boolean includeBorder) {
        return concat(ValidateConstants.Code, suffix, includeBorder);
    }
}
