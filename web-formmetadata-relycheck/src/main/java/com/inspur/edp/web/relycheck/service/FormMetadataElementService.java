package com.inspur.edp.web.relycheck.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataElementService;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataDependencyDetail;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElement;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElementLocator;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 表单元数据依赖检查
 * 注册为bean，通过外部统一调用
 *
 * @author noah
 */
public class FormMetadataElementService extends BaseMetadataElementService implements MetadataElementService {


    @Override
    public String getMetadataType() {
        return "Form";
    }


    @Override
    public Map<MetadataElementLocator, MetadataElement> getMetadataElement(GspMetadata gspMetadata, Set<MetadataElementLocator> set) {
        return null;
    }

    @Override
    public List<MetadataDependencyDetail> getMetadataDependencyDetails(GspMetadata gspMetadata) {
        return super.getMetadataDependencyDetails(gspMetadata);
    }

}
