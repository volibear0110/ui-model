package com.inspur.edp.web.relycheck.constant;

import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.designschema.elements.type.BigNumericType;
import com.inspur.edp.web.designschema.elements.type.NumericType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author noah
 * 2023/8/7 16:39
 */
public class NeedCheckPrecisionType {
    private List<String> needCheckPrecisionList = new ArrayList<>();

    public NeedCheckPrecisionType() {
        this.needCheckPrecisionList.add(new BigNumericType().get$type());
        this.needCheckPrecisionList.add(new NumericType().get$type());
    }

    public static NeedCheckPrecisionType getInstance() {
        return new NeedCheckPrecisionType();
    }

    public boolean contains(String $type) {
        return ListUtility.contains(this.needCheckPrecisionList, $type);
    }
}
