package com.inspur.edp.web.relycheck.config;

import com.inspur.edp.web.relycheck.dependencygenerator.SchemaEntityDependencyGenerator;
import com.inspur.edp.web.relycheck.dependencygenerator.SchemaFieldDependencyGenerator;
import com.inspur.edp.web.relycheck.dependencyvalidator.VoElementValidator;
import com.inspur.edp.web.relycheck.dependencyvalidator.VoEntityValidator;
import com.inspur.edp.web.relycheck.dependencyvalidator.element.*;
import com.inspur.edp.web.relycheck.service.FormMetadataElementService;
import com.inspur.edp.web.relycheck.service.MobileFormMetadataElementService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * relyCheck 的配置类
 *
 * @author noah
 * 2023/7/27 15:44
 */
@Configuration(proxyBeanMethods = false)
public class RelyCheckConfig {
    /**
     * PC表单的element service
     *
     * @return
     */
    @Bean
    public FormMetadataElementService formMetadataElementService() {
        return new FormMetadataElementService();
    }

    /**
     * mobile 表单的element service
     *
     * @return
     */
    @Bean
    public MobileFormMetadataElementService mobileFormMetadataElementService() {
        return new MobileFormMetadataElementService();
    }

    /**
     * vo entity 校验
     *
     * @return
     */
    @Bean
    public VoEntityValidator voEntityValidator() {
        return new VoEntityValidator();
    }

    /**
     * vo element 校验
     *
     * @return
     */
    @Bean
    public VoElementValidator voElementValidator() {
        return new VoElementValidator();
    }

    @Bean
    public RequireValidator requireValidator() {
        return new RequireValidator();
    }

    @Bean
    public MultiLanguageValidator multiLanguageValidator(){
        return  new MultiLanguageValidator();
    }

    @Bean
    public LengthValidator lengthValidator(){
        return new LengthValidator();
    }

    @Bean
    public PrecisionValidator precisionValidator(){
        return new PrecisionValidator();
    }

    @Bean
    public DataTypeValidator dataTypeValidator(){
        return  new DataTypeValidator();
    }

    @Bean
    public  EnumValueValidator enumValueValidator(){
        return new EnumValueValidator();
    }

    /**
     * entity 校验依赖项构造
     *
     * @return
     */
    @Bean
    @Scope(value = "prototype")
    public SchemaEntityDependencyGenerator schemaEntityDependencyGenerator() {
        return new SchemaEntityDependencyGenerator();
    }

    /**
     * field 校验依赖项构造
     *
     * @return
     */
    @Bean
    @Scope(value = "prototype")
    public SchemaFieldDependencyGenerator schemaFieldDependencyGenerator() {
        return new SchemaFieldDependencyGenerator();
    }
}
