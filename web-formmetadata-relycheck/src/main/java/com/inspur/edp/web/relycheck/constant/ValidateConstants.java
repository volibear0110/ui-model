package com.inspur.edp.web.relycheck.constant;

/**
 * @author noah
 * 2023/8/7 17:28
 */
public class ValidateConstants {

    public static final String Name = "名称";

    public static final String Code = "编号";

    /**
     * 校验常量  逗号
     */
    public static final String Comma = ",";

    /**
     * 校验常量 冒号
     */
    public static final String Colon = ":";
}
