/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.relycheck.elements;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单Schema元素
 *
 * @author guozhiqi
 */
@Data
public class CustomSchema {
    private String Id;

    public final String getId() {
        return Id;
    }

    public final void setId(String value) {
        Id = value;
    }

    private String Code;

    public final String getCode() {
        return Code;
    }

    public final void setCode(String value) {
        Code = value;
    }

    private String Name;

    public final String getName() {
        return Name;
    }

    public final void setName(String value) {
        Name = value;
    }

    private String SourceUri;

    public final String getSourceUri() {
        return SourceUri;
    }

    public final void setSourceUri(String value) {
        SourceUri = value;
    }

    private String SourceType;

    public final String getSourceType() {
        return SourceType;
    }

    public final void setSourceType(String value) {
        SourceType = value;
    }

    private String eapiId;
    private String eapiCode;
    private String eapiName;
    private String eapiNameSpace;
    private String voPath;
    private String voNameSpace;

    public String getEapiId() {
        return eapiId;
    }

    public void setEapiId(String eapiId) {
        this.eapiId = eapiId;
    }

    public String getEapiCode() {
        return eapiCode;
    }

    public void setEapiCode(String eapiCode) {
        this.eapiCode = eapiCode;
    }

    public String getEapiName() {
        return eapiName;
    }

    public void setEapiName(String eapiName) {
        this.eapiName = eapiName;
    }

    public String getEapiNameSpace() {
        return eapiNameSpace;
    }

    public void setEapiNameSpace(String eapiNameSpace) {
        this.eapiNameSpace = eapiNameSpace;
    }

    public String getVoPath() {
        return voPath;
    }

    public void setVoPath(String voPath) {
        this.voPath = voPath;
    }

    public String getVoNameSpace() {
        return voNameSpace;
    }

    public void setVoNameSpace(String voNameSpace) {
        this.voNameSpace = voNameSpace;
    }

    private List<CustomEntity> Entities;

    public final List<CustomEntity> getEntities() {
        if (this.Entities == null) {
            this.Entities = new ArrayList<>();
        }
        return Entities;
    }

    public final void setEntities(List<CustomEntity> value) {
        Entities = value;
    }

    private List<CustomField> Variables;

    public final List<CustomField> getVariables() {
        if (this.Variables == null) {
            this.Variables = new ArrayList<>();
        }
        return Variables;
    }

    public final void setVariables(List<CustomField> value) {
        Variables = value;
    }

    @JsonProperty("extendProperties")
    private Map<String, Object> ExtendProperties;

    public Map<String, Object> getExtendProperties() {
        if (this.ExtendProperties == null) {
            this.ExtendProperties = new HashMap<>();
        }
        return ExtendProperties;
    }

    public void setExtendProperties(Map<String, Object> extendProperties) {
        ExtendProperties = extendProperties;
    }
}
