package com.inspur.edp.web.relycheck.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cdp.common.utils.json.JsonUtil;
import com.inspur.edp.web.relycheck.elements.editor.DataSource;
import com.inspur.edp.web.relycheck.elements.editor.FieldEditor;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author noah
 * 2023/8/3 14:37
 */
public class CustomFieldEditor extends FieldEditor {
    protected CustomFieldEditor(String _$type) {
        super(_$type);
    }

    public CustomFieldEditor() {
        super("");
    }

    private String format;

    public final String getFormat() {
        return format;
    }

    public final void setFormat(String value) {
        format = value;
    }

    private String helpId;

    public final String getHelpId() {
        return helpId;
    }

    public final void setHelpId(String value) {
        helpId = value;
    }

    private String uri;

    public final String getUri() {
        return uri;
    }

    public final void setUri(String value) {
        uri = value;
    }

    private String textField;

    public final String getTextField() {
        return textField;
    }

    public final void setTextField(String value) {
        textField = value;
    }

    private String valueField;

    public final String getValueField() {
        return valueField;
    }

    public final void setValueField(String value) {
        valueField = value;
    }

    private String displayType;

    public final String getDisplayType() {
        return displayType;
    }

    public final void setDisplayType(String value) {
        displayType = value;
    }

    private com.inspur.edp.web.relycheck.elements.editor.DataSource DataSource;

    public final DataSource getDataSource() {
        return DataSource;
    }

    public final void setDataSource(DataSource value) {
        DataSource = value;
    }

    private HashMap<String, String> map;


    @JsonProperty(value = "mapFields")
    public final HashMap<String, String> getMap() {
        return map;
    }

    public final void setMap(HashMap<String, String> value) {
        map = value;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private JsonNode options;

    public final JsonNode getOptions() {
        return options;
    }

    public final void setOptions(JsonNode options) {
        this.options = options;
    }
}


