/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.relycheck.elements;

import com.inspur.edp.web.relycheck.elements.type.CustomBaseFieldType;

/**
 * 字段元素
 *
 * @author noah
 */
public class BaseField {

    public BaseField() {
    }

    public BaseField(String _$type) {
        this.$type = _$type;
    }

    private String Id;

    public final String getId() {
        return Id;
    }

    public final void setId(String value) {
        Id = value;
    }


    private String OriginalId;

    public final String getOriginalId() {
        return OriginalId;
    }

    public final void setOriginalId(String value) {
        OriginalId = value;
    }


    private String Code;

    public final String getCode() {
        return Code;
    }

    public final void setCode(String value) {
        Code = value;
    }


    private String Name;

    public final String getName() {
        return Name;
    }

    public final void setName(String value) {
        Name = value;
    }


    private String Label;

    public final String getLabel() {
        return Label;
    }

    public final void setLabel(String value) {
        Label = value;
    }


    private String BindingField;

    public final String getBindingField() {
        return BindingField;
    }

    public final void setBindingField(String value) {
        BindingField = value;
    }


    private CustomBaseFieldType Type;

    public final CustomBaseFieldType getType() {
        return Type;
    }

    public final void setType(CustomBaseFieldType value) {
        Type = value;
    }


    private String Path;

    public final String getPath() {
        return Path;
    }

    public final void setPath(String value) {
        Path = value;
    }

    private String BindingPath;

    public final String getBindingPath() {
        return BindingPath;
    }

    public final void setBindingPath(String value) {
        BindingPath = value;
    }

    private String $type;

    public String get$type() {
        return $type;
    }

    public void set$type(String $type) {
        this.$type = $type;
    }

}
