package com.inspur.edp.web.relycheck.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataElementService;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataDependencyDetail;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElement;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElementLocator;

import java.util.*;

/**
 * 移动表单元数据依赖检查
 * 注册为bean 统一外部统一进行调用
 *
 * @author noah
 */
public class MobileFormMetadataElementService extends BaseMetadataElementService implements MetadataElementService {

    @Override
    public String getMetadataType() {
        return "MobileForm";
    }


    @Override
    public Map<MetadataElementLocator, MetadataElement> getMetadataElement(GspMetadata gspMetadata, Set<MetadataElementLocator> set) {
        return null;
    }

    @Override
    public List<MetadataDependencyDetail> getMetadataDependencyDetails(GspMetadata gspMetadata) {
        return super.getMetadataDependencyDetails(gspMetadata);
    }

}
