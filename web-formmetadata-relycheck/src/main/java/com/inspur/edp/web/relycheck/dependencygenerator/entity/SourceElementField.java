package com.inspur.edp.web.relycheck.dependencygenerator.entity;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.relycheck.elements.CustomField;
import com.inspur.edp.web.relycheck.elements.CustomEntity;

/**
 * 用于进行字段验证时 ，字段验证需要的参数值
 *
 * @author noah
 * 2023/7/25 11:34
 */
public class SourceElementField extends BaseSourceElement {
    private CustomEntity sourceCustomEntity;
    private CustomField sourceField;
    private FormDOM formDOM;

    public CustomEntity getSourceEntity() {
        return sourceCustomEntity;
    }

    public CustomField getSourceField() {
        return sourceField;
    }

    public FormDOM getFormDOM() {
        return formDOM;
    }

    private SourceElementField() {
    }

    public static SourceElementField init(CustomEntity sourceCustomEntity, CustomField sourceField, FormDOM formDOM, GspMetadata gspMetadata) {
        SourceElementField sourceElementField = new SourceElementField();
        sourceElementField.sourceCustomEntity = sourceCustomEntity;
        sourceElementField.sourceField = sourceField;
        sourceElementField.formDOM = formDOM;
        sourceElementField.initWithGspMetadata(gspMetadata);
        return sourceElementField;
    }
}
