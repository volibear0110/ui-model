package com.inspur.edp.web.relycheck.constant;

import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.designschema.elements.type.BigNumericType;
import com.inspur.edp.web.designschema.elements.type.NumericType;
import com.inspur.edp.web.designschema.elements.type.StringType;
import com.inspur.edp.web.designschema.elements.type.TextType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 允许检测字段长度的类型
 *
 * @author noah
 * 2023/8/7 16:29
 */
public class NeedCheckLengthType {
    private List<String> needCheckLengthList = new ArrayList<>();

    public NeedCheckLengthType() {
        this.needCheckLengthList.add(new BigNumericType().get$type());
        this.needCheckLengthList.add(new NumericType().get$type());
        this.needCheckLengthList.add(new StringType().get$type());
        this.needCheckLengthList.add(new TextType().get$type());
    }

    public static NeedCheckLengthType getInstance() {
        return new NeedCheckLengthType();
    }

    public boolean contains(String $type) {
        return ListUtility.contains(this.needCheckLengthList, $type);
    }

    /**
     * 获取所有可以检测length的 $type 参数
     *
     * @return
     */
    public List<String> getNeedCheckLengthList() {
        return this.needCheckLengthList;
    }
}
