package com.inspur.edp.web.relycheck.dependencygenerator.entity;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.relycheck.elements.CustomEntity;


/**
 * 用于记录需要针对entity进行验证时，传递的参数值
 *
 * @author noah
 * 2023/7/25 11:34
 */
public class SourceElementEntity extends BaseSourceElement {
    private CustomEntity sourceCustomEntity;
    private FormDOM formDOM;

    public CustomEntity getSourceEntity() {
        return sourceCustomEntity;
    }

    public FormDOM getFormDOM() {
        return formDOM;
    }

    private SourceElementEntity() {
    }

    /**
     * sourceEntity的参数构造
     * @param sourceCustomEntity
     * @param formDOM
     * @param gspMetadata
     * @return
     */
    public static SourceElementEntity init(CustomEntity sourceCustomEntity, FormDOM formDOM, GspMetadata gspMetadata) {
        SourceElementEntity sourceElementEntity = new SourceElementEntity();
        sourceElementEntity.sourceCustomEntity = sourceCustomEntity;
        sourceElementEntity.formDOM = formDOM;
        sourceElementEntity.initWithGspMetadata(gspMetadata);
        return sourceElementEntity;
    }

}
