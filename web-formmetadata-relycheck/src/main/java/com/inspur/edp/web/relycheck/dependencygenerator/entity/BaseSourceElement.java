package com.inspur.edp.web.relycheck.dependencygenerator.entity;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

/**
 * entity 和 field的传递参数
 * 传递对应的元数据相关信息
 *
 * @author noah
 * 2023/7/25 11:40
 */
public class BaseSourceElement {
    /**
     * 元数据id参数
     */
    private String metadataId;
    /**
     * 元数据code参数
     */
    private String metadataCode;
    /**
     * 元数据name参数
     */
    private String metadataName;
    /**
     * 元数据 namespace参数
     */
    private String metadataNameSpace;

    public String getMetadataId() {
        return metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    public String getMetadataCode() {
        return metadataCode;
    }

    public void setMetadataCode(String metadataCode) {
        this.metadataCode = metadataCode;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public String getMetadataNameSpace() {
        return metadataNameSpace;
    }

    public void setMetadataNameSpace(String metadataNameSpace) {
        this.metadataNameSpace = metadataNameSpace;
    }

    /**
     * 依据元数据进行参数构造
     *
     * @param gspMetadata
     */
    protected void initWithGspMetadata(GspMetadata gspMetadata) {
        if (gspMetadata == null) {
            return;
        }
        this.setMetadataId(gspMetadata.getHeader().getId());
        this.setMetadataCode(gspMetadata.getHeader().getCode());
        this.setMetadataName(gspMetadata.getHeader().getName());
        this.setMetadataNameSpace(gspMetadata.getHeader().getNameSpace());
    }
}
