package com.inspur.edp.web.relycheck.dependencygenerator;

import com.inspur.edp.lcm.metadata.spi.entity.MetadataElement;
import com.inspur.edp.web.relycheck.constant.RelyCheckType;
import com.inspur.edp.web.relycheck.dependencygenerator.entity.BaseSourceElement;

/**
 * 元数据 element 实体构造
 *
 * @author noah
 */
class MetadataElementGenerator {
    /**
     * 构造 metadataElement 实例
     *
     * @param type
     * @param content
     * @return
     */
    static MetadataElement generate(String type, BaseSourceElement content) {
        MetadataElement metadataElement = new MetadataElement();
        metadataElement.setType(type);
        metadataElement.setContent(content);
        return metadataElement;
    }

    /**
     * 构造 metadataElement 实例
     *
     * @param relyCheckType
     * @param content
     * @return
     */
    static MetadataElement generate(RelyCheckType relyCheckType, BaseSourceElement content) {
        return generate(relyCheckType.getType(), content);
    }
}
