package com.inspur.edp.web.relycheck.utility;

import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.lcm.metadata.spi.entity.ValidationLevelEnum;

/**
 * 校验返回值信息构造
 *
 * @author noah
 * 2023/8/10 16:13
 */
public class ValidateResultUtility {
    /**
     * 定义成功的返回结果
     *
     * @return
     */
    public static ValidateResult success() {
        ValidateResult validateResult = new ValidateResult();
        validateResult.setValid(true);
        return validateResult;
    }

    /**
     * 定义失败的返回结果
     * 默认级别为error
     *
     * @param message
     * @return
     */
    public static ValidateResult failureWithError(String message) {
        return failure(message, ValidationLevelEnum.ERROR);
    }

    /**
     * 校验提示信息
     * 校验级别为warning
     *
     * @param message
     * @return
     */
    public static ValidateResult failureWithWarning(String message) {
        return failure(message, ValidationLevelEnum.WARNING);
    }


    /**
     * 传递校验级别
     *
     * @param message
     * @param validationLevel
     * @return
     */
    public static ValidateResult failure(String message, ValidationLevelEnum validationLevel) {
        ValidateResult validateResult = new ValidateResult();
        validateResult.setValid(false);
        validateResult.setMessage(message);
        validateResult.setLevel(validationLevel);
        return validateResult;
    }

    /**
     * 判断校验结果是否应该继续进行验证
     * @param validateResult
     * @return
     */
    public static boolean canContinute(ValidateResult validateResult) {
        if (validateResult == null) {
            return true;
        }
        if (!validateResult.isValid() && validateResult.getLevel() == ValidationLevelEnum.ERROR) {
            return false;
        }
        return true;
    }
}
