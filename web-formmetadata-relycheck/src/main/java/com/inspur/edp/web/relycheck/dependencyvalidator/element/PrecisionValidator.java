package com.inspur.edp.web.relycheck.dependencyvalidator.element;

import com.inspur.edp.lcm.metadata.spi.entity.ValidateResult;
import com.inspur.edp.web.relycheck.constant.NeedCheckPrecisionType;
import com.inspur.edp.web.relycheck.constant.ValidateConstants;
import com.inspur.edp.web.relycheck.utility.StringConcatUtility;
import com.inspur.edp.web.relycheck.utility.ValidateResultUtility;

/**
 * @author noah
 * 2023/8/10 14:01
 */
public class PrecisionValidator extends BaseElementValidator {
    @Override
    public ValidateResult validate(ElementValidateParameter parameter) {
        ValidateResult warningValidateResult = ValidateResultUtility.success();
        if (NeedCheckPrecisionType.getInstance().contains(this.getSourceField(parameter).getType().get$type())) {
            int voElementPrecision = parameter.getGspViewModelElement().getPrecision();
            int fieldPrecision = this.getSourceField(parameter).getType().getPrecision();
            if (voElementPrecision > fieldPrecision) {
                warningValidateResult = ValidateResultUtility.failureWithWarning(String.format("视图对象%1$s中子表%2$s的字段%3$s精度增加为" + ValidateConstants.Colon + "%4$s" + ValidateConstants.Comma + "而表单对应字段精度为" + ValidateConstants.Colon + "%5$s。为同步表单控件精度属性" + ValidateConstants.Comma + "请在表单%6$s的实体树中执行更新schema操作",
                        StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                        StringConcatUtility.concat("", String.valueOf(voElementPrecision), true),
                        StringConcatUtility.concat("", String.valueOf(fieldPrecision), true),
                        StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
                ));
            }
            if (voElementPrecision < fieldPrecision) {
                return ValidateResultUtility.failureWithError(String.format("视图对象%1$s中子表%2$s的字段%3$s精度减小为" + ValidateConstants.Colon + "%4$s" + ValidateConstants.Comma + "而表单对应字段精度为" + ValidateConstants.Colon + "%5$s。为同步表单控件精度属性" + ValidateConstants.Comma + "请在表单%6$s的实体树种执行更新schema操作",
                        StringConcatUtility.concatWithNamePrefix(this.getVoMetadataName(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getEntityCode(parameter), true),
                        StringConcatUtility.concatWithCodePrefix(this.getSourceFieldCode(parameter), true),
                        StringConcatUtility.concat("", String.valueOf(voElementPrecision), true),
                        StringConcatUtility.concat("", String.valueOf(fieldPrecision), true),
                        StringConcatUtility.concatWithNamePrefix(this.getFormMetadataName(parameter), true)
                ));
            }
        }
        return warningValidateResult;
    }
}
