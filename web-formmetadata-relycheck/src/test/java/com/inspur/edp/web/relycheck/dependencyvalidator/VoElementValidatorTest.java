package com.inspur.edp.web.relycheck.dependencyvalidator;

import com.inspur.edp.web.relycheck.config.RelyCheckConfig;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author noah
 * 2023/7/31 16:50
 */
@SpringBootTest
public class VoElementValidatorTest implements ApplicationContextAware {

    private ApplicationContext context;

    @Autowired
    private VoElementValidator voElementValidator;

    @Test
    public void test() {
        System.out.println(this.voElementValidator);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}