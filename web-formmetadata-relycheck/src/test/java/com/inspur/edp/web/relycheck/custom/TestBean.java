package com.inspur.edp.web.relycheck.custom;

import lombok.Data;

/**
 * @author noah
 * 2023/8/7 09:03
 */
@Data
public class TestBean {
    private String name;

}
