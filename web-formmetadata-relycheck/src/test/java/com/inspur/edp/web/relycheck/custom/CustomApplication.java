package com.inspur.edp.web.relycheck.custom;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author noah
 * 2023/8/7 09:04
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class CustomApplication {
    @Test
    public void test() {
        ConfigurableApplicationContext context = SpringApplication.run(CustomApplication.class);
        TestBean testBean = context.getBean(MyConfig.class).testBean();
        TestBean testBean1 = context.getBean(TestBean.class);
        System.out.println(testBean);
        System.out.println(testBean1);

        context.close();
    }
}
