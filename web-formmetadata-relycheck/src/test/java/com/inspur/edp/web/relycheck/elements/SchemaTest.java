package com.inspur.edp.web.relycheck.elements;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import org.junit.Test;

/**
 * @author noah
 * 2023/8/4 16:23
 */
public class SchemaTest {
    @Test
    public void test() {
        CustomSchema customSchema = new CustomSchema();
        customSchema.setCode("fff");
        String jsonResult = SerializeUtility.getInstance().serialize(customSchema);
        CustomSchema ss = SerializeUtility.getInstance().deserialize(jsonResult, CustomSchema.class);

        System.out.println(ss.getCode().equals("fff"));
    }

}