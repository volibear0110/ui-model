package com.inspur.edp.web.relycheck.custom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author noah
 * 2023/8/7 09:03
 */
@Configuration
public class MyConfig {
    @Bean
    public TestBean testBean() {
        return new TestBean();
    }
}
