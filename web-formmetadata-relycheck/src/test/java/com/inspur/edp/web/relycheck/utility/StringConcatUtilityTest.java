package com.inspur.edp.web.relycheck.utility;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.Test;

public class StringConcatUtilityTest {

    @Test
    public void testConcat() {
        String prefix = "Hello";
        String suffix = "World";
        boolean includeBorder = true;

        String expected = "【Hello:World】";
        String actual = StringConcatUtility.concat(prefix, suffix, includeBorder);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConcatWithNamePrefix() {
        String suffix = "John";
        boolean includeBorder = false;

        String expected = "名称:John";
        String actual = StringConcatUtility.concatWithNamePrefix(suffix, includeBorder);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConcatWithCodePrefix() {
        String suffix = "123";
        boolean includeBorder = true;

        String expected = "【编号:123】";
        String actual = StringConcatUtility.concatWithCodePrefix(suffix, includeBorder);

        Assert.assertEquals(expected, actual);
    }
}