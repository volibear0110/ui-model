/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageConstants;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;

/**
 * package.json 文件复制
 *
 * @author noah
 */
class PackageJsonCopyManager {
    /**
     * package.json 文件从server复制到目标node_modules 目录下
     *
     * @param npmInstallParameter npm install执行参数
     * @param currentWorkPath     当前工作server目录
     * @param node_ModulesPath    node_modules 路径
     * @return
     */
    public static NpmPackageResponse copy(NpmInstallParameter npmInstallParameter, String currentWorkPath, String node_ModulesPath) {
        //从server拷贝package.json文件
        // 获取package.json 在server 目录下的存放位置
        String packageJsonPathInServer = PackageJsonPathGenerator.generate(currentWorkPath, npmInstallParameter.isMobile());
        boolean packageJsonExists = FileUtility.exists(packageJsonPathInServer);
        // node_modules 目录下的package.json 文件是否存在
        String packageJsonPathInNodeModules = FileUtility.combine(node_ModulesPath, NpmPackageConstants.PackageJsonName);
        boolean packageJsonInNodeModulesExists = FileUtility.exists(packageJsonPathInNodeModules);
        if (!packageJsonExists && packageJsonInNodeModulesExists) {
            // 如果server目录下放置的package.json 不存在 但node_modules 目录下的package.json 文件存在
            WebLogger.Instance.info("the package.json file in the server is not exists,server path is :" + packageJsonPathInServer + " , but the node_modules path has package.json . so use the node_modules package.json ", PackageJsonCopyManager.class.getName());
        }

        if (!packageJsonExists && !packageJsonInNodeModulesExists) {
            // 如果均不存在 那么无法执行install 操作
            return NpmPackageResponse.createError("the package.json file is not exists in the server, the path is " + packageJsonPathInServer);
        }

        if (packageJsonExists) {
            // 复制server目录下的package.json文件至node_modules 下 保证package.json文件得以更新
            // 首先删除对应的目标文件
            FileUtility.deleteFile(packageJsonPathInNodeModules);

            FileUtility.copyFile(packageJsonPathInServer, packageJsonPathInNodeModules, true);
        }

        return NpmPackageResponse.create();
    }
}
