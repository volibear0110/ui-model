/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmsetting;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.RandomUtility;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmRepository;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;

import java.util.Random;

public class NpmSettingEncryptService {
    private static final NpmSettingEncryptService encryService = new NpmSettingEncryptService();

    public static NpmSettingEncryptService getInstance() {
        return encryService;
    }

    public NpmSettings deEncrypt(NpmSettings npmSettings) {
        NpmSettings deepCloneNpmSetting = SerializeUtility.getInstance().deserialize(SerializeUtility.getInstance().serialize(npmSettings), NpmSettings.class);
        if (deepCloneNpmSetting != null && deepCloneNpmSetting.getSettingConfig() != null && deepCloneNpmSetting.getSettingConfig().getRepositories() != null) {
            NpmRepository[] repositories = deepCloneNpmSetting.getSettingConfig().getRepositories();
            int offset = 3;
            for (int i = 0; i < deepCloneNpmSetting.getSettingConfig().getRepositories().length; i++) {
                NpmRepository repositoryItem = repositories[i];
                // 修正对应参数值 避免由于安全问题  暴露明文
                repositoryItem.setUserName(this.fromCharCode(offset, repositoryItem.getUserName(), false));
                repositoryItem.setPassword(this.fromCharCode(offset, repositoryItem.getPassword(), false));
                repositoryItem.setUrl(this.fromCharCode(offset, repositoryItem.getUrl(), false));
            }
        }
        return deepCloneNpmSetting;
    }

    public NpmSettings encrypt(NpmSettings npmSettings) {
        NpmSettings deepCloneNpmSetting = SerializeUtility.getInstance().deserialize(SerializeUtility.getInstance().serialize(npmSettings), NpmSettings.class);
        if (deepCloneNpmSetting != null && deepCloneNpmSetting.getSettingConfig() != null && deepCloneNpmSetting.getSettingConfig().getRepositories() != null) {
            NpmRepository[] repositories = deepCloneNpmSetting.getSettingConfig().getRepositories();
            for (int i = 0; i < deepCloneNpmSetting.getSettingConfig().getRepositories().length; i++) {
                NpmRepository repositoryItem = repositories[i];
                // 修正对应参数值 避免由于安全问题  暴露明文
                repositoryItem.setPassword(RandomUtility.newGuid().replaceAll("-", ""));
            }
        }
        return deepCloneNpmSetting;
    }


    /**
     * 加密加3   解密减3
     *
     * @param offset
     * @param source
     * @param isEncrypt
     * @return
     */
    private String fromCharCode(int offset, String source, boolean isEncrypt) {
        if (source == null || source.length() == 0) {
            return "";
        }
        char[] arrCharSource = source.toCharArray();
        StringBuilder builder = new StringBuilder(arrCharSource.length);

        for (char codePoint : arrCharSource) {
            if (isEncrypt) {
                builder.append((char) (codePoint + offset));
            } else {
                builder.append((char) (codePoint - offset));
            }

        }
        return builder.toString();
    }
}
