/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.npmpackage.api.entity.*;

/**
 * node_modules 路径构造
 *
 * @author guozhiqi
 */
public class NodeModulesPathGenerator {
    /**
     * 根据入参获取node_modules 待安装路径
     * 针对设计时：如果传递projectPath，那么以projectPath为基础获取对应工作区；如果projectPath为空，那么使用当前启用工作区
     * 针对运行时定制：获取当前server及对应的node_modules
     * 针对元数据部署工具：获取当前server及对应node_modules
     * 针对移动：适应上述情况
     *
     * @param npmInstallParameter
     * @return
     */
    public static String generate(NpmInstallParameter npmInstallParameter, String currentWorkPath) {

        String parentNodeModulesPath = generatePackageJsonPath(npmInstallParameter, currentWorkPath);
        String node_modulesPath = FileUtility.combine(parentNodeModulesPath, NpmPackageConstants.Node_ModulesName);
        return node_modulesPath;
    }

    /**
     * 获取对应package.json 对应目录
     * @param npmInstallParameter
     * @param currentWorkPath
     * @return
     */
    public static String generatePackageJsonPath(NpmInstallParameter npmInstallParameter, String currentWorkPath) {
        ExecuteEnvironment executeEnvironment = npmInstallParameter.getExecuteEnvironment();
        return generatePackageJsonPath(executeEnvironment, currentWorkPath);
    }

    public static String generatePackageJsonPath(ExecuteEnvironment executeEnvironment, String currentWorkPath) {
        String parentNode_modulesPath = "";

        // 如果是运行时定制
        if (executeEnvironment.equals(ExecuteEnvironment.Runtime)) {

            String relativePath = FileUtility.combine("web", "runtime", "projects");
            // 获取到运行时定制下node_modules path
            parentNode_modulesPath = FileUtility.combine(currentWorkPath, relativePath);
        }

        // 设计时
        if (executeEnvironment.equals(ExecuteEnvironment.Design)) {
            // 获取当前开发时工作空间绝对路径
            String currentDevRootPath = MetadataUtility.getInstance().getDevRootAbsolutePath();
            parentNode_modulesPath = currentDevRootPath;

        }

        // 如果是部署工具
        if (executeEnvironment.equals(ExecuteEnvironment.UpgradeTool)) {
            String relativePath = FileUtility.combine("web", "runtime", "projects");
            // 获取到元数据部署工具下node_modules path
            parentNode_modulesPath = FileUtility.combine(currentWorkPath, relativePath);
        }
        return parentNode_modulesPath;
    }

}
