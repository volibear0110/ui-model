/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmsetting;

/**
 * npm 参数配置常量
 * @author  noah
 */
public class NpmSettingConstant {
    /**
     * npm 参数配置文件名称
     */
    public static final String SettingFileName="setting.json";

    /**
     * 相对于nodejs路径的目录名称
     */
    public static final String RelativePathWithNodeJs="npmsettings";
}
