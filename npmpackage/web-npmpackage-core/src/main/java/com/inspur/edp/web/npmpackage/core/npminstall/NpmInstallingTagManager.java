/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageConstants;

/**
 * npm安装中标识管理
 *
 * @author noah
 */
class NpmInstallingTagManager {
    /**
     * 写入npm 安装参数到标识文件
     *
     * @param nodeModulePath
     */
    public static void createTag(String nodeModulePath) {
        String npmInstallingTagPath = getNpmInstallingTagPath(nodeModulePath);
        if (!FileUtility.exists(npmInstallingTagPath)) {
            FileUtility.createDirectory(nodeModulePath);
        }
        String content = SerializeUtility.getInstance().serialize(NpmInstallingParameter.createNew());
        FileUtility.writeFile(npmInstallingTagPath, content);
    }

    /**
     * 判断是否当前处于installing状态
     * @param nodeModulesPath
     * @return
     */
    public static boolean isInstalling(String nodeModulesPath) {
        String npmInstallingTagPath = getNpmInstallingTagPath(nodeModulesPath);
        return  FileUtility.exists(npmInstallingTagPath);
    }


    /**
     * 构造npm installing标识路径参数
     * * @param nodeModulesPath
     * @return
     */
    public static String getNpmInstallingTagPath(String nodeModulesPath) {
        return FileUtility.combine(nodeModulesPath, NpmPackageConstants.NpmInstallingTag);
    }
}
