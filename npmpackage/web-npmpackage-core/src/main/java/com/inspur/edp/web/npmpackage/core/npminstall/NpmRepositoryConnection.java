/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import java.net.InetAddress;

/**
 * 执行对npm仓库的连接请求
 *
 * @author noah
 */
public class NpmRepositoryConnection {
    /**
     * 进行连接测试
     * @param ipAddress
     * @return
     */
    public static boolean connectTest(String ipAddress) {
        try {
            int timeOut = 3000;  //超时应该在3钞以上
            return InetAddress.getByName(ipAddress).isReachable(timeOut);
        } catch (Exception e) {
            return  false;
        }
    }
}
