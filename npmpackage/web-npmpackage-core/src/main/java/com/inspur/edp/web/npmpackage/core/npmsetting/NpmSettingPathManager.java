/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmsetting;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageConstants;

/**
 * npm 配置文件路径manager
 *
 * @author noah
 */
class NpmSettingPathManager {
    /**
     * 根据nodejs目录获取npm配置文件路径
     *
     * @param nodeJsPath nodejs 文件路径
     * @return
     */
    public static String getNpmSettingFilePath(String nodeJsPath) {
        String npmSettingPath = getNpmSettingPath(nodeJsPath);
        return FileUtility.combine(npmSettingPath, NpmPackageConstants.NpmSettingFileName);
    }

    /**
     * 获取npm配置文件所在目录
     * * @param nodeJsPath
     *
     * @return
     */
    public static String getNpmSettingPath(String nodeJsPath) {
        return FileUtility.combine(nodeJsPath, NpmPackageConstants.NpmSettingPath);
    }

    /**
     * npm参数配置文件是否存在
     *
     * @param npmSettingPath npm参数配置文件路径
     * @return
     */
    public static boolean exists(String npmSettingPath) {
        return FileUtility.exists(npmSettingPath);
    }
}
