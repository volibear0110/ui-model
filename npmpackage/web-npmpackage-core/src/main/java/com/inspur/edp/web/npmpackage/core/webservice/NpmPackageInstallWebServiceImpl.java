/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.webservice;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.CommonUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.npmpackage.api.constant.I18nExceptionConstant;
import com.inspur.edp.web.npmpackage.api.constant.I18nMsgConstant;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponseLevel;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;
import com.inspur.edp.web.npmpackage.api.service.NpmPackageInstallService;
import com.inspur.edp.web.npmpackage.api.webservice.NpmPackageInstallWebService;
import com.inspur.edp.web.npmpackage.core.npmsetting.NpmSettingConvertor;
import com.inspur.edp.web.npmpackage.core.npmsetting.NpmSettingEncryptService;
import com.inspur.edp.web.npmpackage.core.npmsetting.NpmSettingManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.Arrays;

/**
 * npm 包安装webservice
 *
 * @author guozhiqi
 */
public class NpmPackageInstallWebServiceImpl implements NpmPackageInstallWebService {

    @Override
    public NpmPackageResponse install(NpmSettings npmSettings) {
        if (npmSettings == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_NPM_PACKAGE_ERROR_0002);
        }
        npmSettings = NpmSettingEncryptService.getInstance().deEncrypt(npmSettings);

        // 执行参数转换
        try {
            NpmInstallParameter npmInstallParameter = NpmSettingConvertor.convertFromNpmSetting(npmSettings);

            // 执行npm 安装
            NpmPackageInstallService npmPackageInstallService = SpringBeanUtils.getBean(NpmPackageInstallService.class);
            NpmPackageResponse npmPackageResponse = npmPackageInstallService.install(npmInstallParameter);

            try {
                // 保存最后的更新时间
                npmSettings.setLastUpdated(CommonUtility.getCurrentDateString());
                NpmSettingManager.saveNpmSetting(npmSettings);
            } catch (Exception ignored) {
                WebLogger.Instance.info("npm install failed " + ignored.getMessage() + Arrays.toString(ignored.getStackTrace()));
            }

            return npmPackageResponse;
        } catch (Exception e) {
            NpmPackageResponse npmPackageResponse = new NpmPackageResponse();
            npmPackageResponse.setResponseLevel(NpmPackageResponseLevel.Error);
            npmPackageResponse.setSuccess(false);
            npmPackageResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0020));
            return npmPackageResponse;
        }
    }

    /**
     * npm install命令执行 使用默认的npm配置信息
     *
     * @return 安装结果反馈
     */
    @Override
    public NpmPackageResponse install() {
        // 获取默认的npm 仓库配置
        NpmSettings npmSettings = NpmSettingManager.getNpmSetting(false);
        // 使用默认配置进行在线install
        return install(npmSettings);
    }

    /**
     * npm install命令执行
     *
     * @param installParameter install参数
     * @return 安装结果反馈
     */
    @Override
    public NpmPackageResponse installCheck(NpmSettings installParameter) {
        installParameter = NpmSettingEncryptService.getInstance().deEncrypt(installParameter);

        // 获取默认的npm 仓库配置
        NpmSettings npmSettings = NpmSettingManager.getNpmSetting(false);
        NpmInstallParameter npmInstallParameter = NpmSettingConvertor.convertFromNpmSetting(npmSettings);
        // 使用默认配置进行在线install
        NpmPackageInstallService npmPackageInstallService = SpringBeanUtils.getBean(NpmPackageInstallService.class);
        return npmPackageInstallService.installCheck(npmInstallParameter);
    }
}
