/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall.global;


import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallPackageParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageConstants;

import java.util.List;

/**
 * npm 安装命令参数构造
 *
 * @author noah
 */
class GlobalNpmInstallCommandParameterGenerator {
    /**
     * npm 安装命令参数构造
     *
     * @param npmInstallParameter
     * @return
     */
    public static String generate(NpmInstallParameter npmInstallParameter) {
        String args = "";

        if (npmInstallParameter.isUseProduction()) {
            args += " --production=true ";
        }
        args += " -g ";
        args += " --package-lock=false ";
        args += " --audit=false ";
        args += " --registry=" + npmInstallParameter.getRegistry();

        // 为了避免某些依赖项无法安装 因此增加强制标识
        args += " --force";

        return args;
    }


    /**
     * 构造npm安装包列表
     *
     * @param npmInstallPackageParameterList
     * @return
     */
    private static String getNpmInstalledPackageList(List<NpmInstallPackageParameter> npmInstallPackageParameterList) {
        StringBuilder sb = new StringBuilder();
        npmInstallPackageParameterList.forEach(packageParameter -> {
            String packageArgs = getNpmInstalledPackage(packageParameter);
            if (!StringUtility.isNullOrEmpty(packageArgs)) {
                sb.append(packageArgs);
            }
        });
        return sb.toString();
    }

    /**
     * 构造npm 安装包参数及其对应的版本标识
     *
     * @param npmInstallPackageParameter npm 安装包参数
     * @return
     */
    private static String getNpmInstalledPackage(NpmInstallPackageParameter npmInstallPackageParameter) {
        String args = "";
        // 获取对应的npm包名
        String npmInstallPackageName = npmInstallPackageParameter.getPackageName();
        if (StringUtility.isNullOrEmpty(npmInstallPackageName)) {
            return args;
        }
        // 获取对应的npm包对应的版本
        String npmInstallPackageVersion = StringUtility.isNullOrEmpty(npmInstallPackageParameter.getVersion()) ? npmInstallPackageParameter.getVersion() : NpmPackageConstants.LatestVersion;
        args += " " + npmInstallPackageName + "@" + npmInstallPackageVersion;
        return args;
    }

}
