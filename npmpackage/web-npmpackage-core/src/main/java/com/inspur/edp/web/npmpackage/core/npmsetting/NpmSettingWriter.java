/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmsetting;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;

import java.util.Arrays;

/**
 * npm 配置文件写入
 *
 * @author noah
 */
class NpmSettingWriter {
    /**
     * 写入文件内容到npm配置文件
     *
     * @param nodeJsPath nodejs文件路径
     * @param content    npm配置文件内容
     */
    public static void write(String nodeJsPath, String content) {
        String npmSettingFilePath = NpmSettingPathManager.getNpmSettingFilePath(nodeJsPath);
        if (!NpmSettingPathManager.exists(npmSettingFilePath)) {
            // 构造对应的目录
            String npmSettingPath = NpmSettingPathManager.getNpmSettingPath(nodeJsPath);
            FileUtility.createDirectory(npmSettingPath);
        }
        try {
            FileUtility.writeFile(npmSettingFilePath, content);
        } catch (Exception ex) {
            WebLogger.Instance.info("write to npmsetting file failed,please check! the path is " + npmSettingFilePath + " , and the reason is " + Arrays.toString(ex.getStackTrace()), NpmSettingWriter.class.getName());
        }
    }
}
