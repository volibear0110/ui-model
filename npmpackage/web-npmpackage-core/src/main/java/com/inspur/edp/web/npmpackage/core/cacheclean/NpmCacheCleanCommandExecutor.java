/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.cacheclean;

import com.inspur.edp.web.common.entity.NodeJsCommandEnum;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.io.NodejsFunctionUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.CommandLineUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.npmpackage.api.constant.I18nMsgConstant;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;


/**
 * @author guozhiqi
 */
public class NpmCacheCleanCommandExecutor {
    /**
     * 命令执行
     *
     * @param npmInstallParameter
     * @param npmName
     */
    public static NpmPackageResponse execute(NpmInstallParameter npmInstallParameter, String npmName, String version) {

        // 构造install 命令参数 执行npm包安装操作

        WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0022), NpmCacheCleanCommandExecutor.class.getName());

        boolean isUpgradeTool = npmInstallParameter.getExecuteEnvironment() == ExecuteEnvironment.UpgradeTool;
        String currentWorkPath = FileUtility.getCurrentWorkPath(isUpgradeTool);

        String npmCommandExecuteWithPath = NodejsFunctionUtility.getNodeJsCommandInServerWithOS(NodeJsCommandEnum.Npm, currentWorkPath);
        String npmInstallCommandExecuteWithPath = npmCommandExecuteWithPath + " cache clean --force";

        String[] command = new String[2];
        // 盘符独立打开
        command[0] = npmInstallCommandExecuteWithPath;
        command[1] = "exit";
        // 执行npm 包安装
        return runCommand(command, true);
    }

    private static NpmPackageResponse runCommand(String[] commands, boolean isWait) {
        String command = String.join(" && ", commands);
        return runCommand(command, isWait);
    }

    /**
     * 执行命令
     *
     * @param command 待执行命令
     * @param isWait  是否等待返回结果
     */
    private static NpmPackageResponse runCommand(String command, boolean isWait) {
        StringBuffer errorSB = new StringBuffer();
        String errorMessage = CommandLineUtility.executeCommand(command, errorSB);
        if (!StringUtility.isNullOrEmpty(errorMessage)) {
            WebLogger.Instance.error(errorMessage, NpmCacheCleanCommandExecutor.class.getName());
        }

        return NpmPackageResponse.create();
    }

}
