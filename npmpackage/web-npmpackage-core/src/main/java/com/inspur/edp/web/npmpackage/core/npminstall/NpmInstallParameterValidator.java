/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.npmpackage.api.constant.I18nMsgConstant;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponseLevel;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmUpdatePolicy;

/**
 * npm安装参数验证
 *
 * @author noah
 */
class NpmInstallParameterValidator {
    /**
     * 参数验证
     *
     * @param npmInstallParameter
     * @return
     */
    public static NpmPackageResponse validate(NpmInstallParameter npmInstallParameter) {
        ExecuteEnvironment executeEnvironment = npmInstallParameter.getExecuteEnvironment();
        if (executeEnvironment.equals(ExecuteEnvironment.None)) {
            // 表示是设计时
            return NpmPackageResponse.createError(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0001));
        }

        if (StringUtility.isNullOrEmpty(npmInstallParameter.getUserName()) && !StringUtility.isNullOrEmpty(npmInstallParameter.getPassword())) {
            return NpmPackageResponse.createError(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0002));
        }
        if (StringUtility.isNullOrEmpty(npmInstallParameter.getPassword()) && !StringUtility.isNullOrEmpty(npmInstallParameter.getUserName())) {
            return NpmPackageResponse.createError(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0003));
        }

        if (!npmInstallParameter.isUpdateAll() && npmInstallParameter.getNpmInstallPackageParameterList().size() == 0) {
            return NpmPackageResponse.createError(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0004));
        }
        // 如果更新策略为never
        if (npmInstallParameter.getUpdatePolicy().equals(NpmUpdatePolicy.never)) {
            NpmPackageResponse npmPackageResponse = new NpmPackageResponse();
            npmPackageResponse.setResponseLevel(NpmPackageResponseLevel.Info);
            npmPackageResponse.setSuccess(true);
            npmPackageResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0005));
            return npmPackageResponse;
        }

        // 如果为离线模式 那么无需执行任何安装
        if (npmInstallParameter.isOfflineMode()) {
            NpmPackageResponse npmPackageResponse = new NpmPackageResponse();
            npmPackageResponse.setResponseLevel(NpmPackageResponseLevel.Info);
            npmPackageResponse.setSuccess(true);
            npmPackageResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_NPM_PACKAGE_MSG_0006));
            return npmPackageResponse;
        }
        return NpmPackageResponse.create();
    }
}
