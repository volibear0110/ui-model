/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.io.NodejsFunctionUtility;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageConstants;

/**
 * package.json 服务端路径获取
 *
 * @author noah
 */
public class PackageJsonPathGenerator {
    /**
     * 构造package.json 路径
     *
     * @param isUpgradeTool 是否运行在元数据部署工具
     * @param isMobile      是否移动工程
     * @return
     */
    public static String generate(boolean isUpgradeTool, boolean isMobile) {
        String serverPath = FileUtility.getCurrentWorkPath(isUpgradeTool);
        return generate(serverPath, isMobile);
    }

    /**
     * 构造package。json文件路径
     *
     * @param serverPath 服务端server路径
     * @param isMobile   是否移动工程
     * @return
     */
    public static String generate(String serverPath, boolean isMobile) {
        String nodejsPath = NodejsFunctionUtility.getNodeJsPathInServer();
        String packageJsonPath = FileUtility.combine(nodejsPath, "packagejson");
        return FileUtility.combine(packageJsonPath, NpmPackageConstants.PackageJsonName);
    }

    public static String generate() {
        String nodejsPath = NodejsFunctionUtility.getNodeJsPathInServer();
        String packageJsonPath = FileUtility.combine(nodejsPath, "packagejson");
        return FileUtility.combine(packageJsonPath, NpmPackageConstants.PackageJsonName);
    }
}
