/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmsetting;

import com.inspur.edp.web.npmpackage.api.entity.settings.NpmRepository;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettingConfig;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettingMode;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * npm 参数配置初始化
 *
 * @author noah
 */
class NpmSettingInitialization {

    /**
     * 构造默认的npm配置参数
     * @return
     */
    public static NpmSettings initialize() {
        NpmSettings npmSettings = new NpmSettings();
        // 默认为离线模式 为停用 如果要启用必须手工开启
        npmSettings.setOffline(true);
        npmSettings.setSettingMode(NpmSettingMode.prod);

        NpmSettingConfig settingConfig = new NpmSettingConfig();


        // 构造三个默认的仓库
        NpmRepository[] repositories = new NpmRepository[1];
        String encryptedText = "UGtqdzhsaTl5RE9jSFF2Qg==";
        NpmRepository prodRepository = new NpmRepository();
        prodRepository.setId("prod");
        prodRepository.setName("npm-gsp");
        prodRepository.setUrl("https://repos.iec.io/repository/npm-gsp/");
        prodRepository.setUserName("igix-npm-common");
        prodRepository.setPassword(new String(Base64.getDecoder().decode(encryptedText), StandardCharsets.UTF_8));
        prodRepository.setNeedLogin(true);
        prodRepository.setCanDelete(false);

        repositories[0] = prodRepository;

        settingConfig.setRepositories(repositories);

        npmSettings.setSettingConfig(settingConfig);

        return npmSettings;
    }
}
