/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npminstall.global;

import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.OperatingSystemUtility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author guozhiqi
 */
public class GlobalNpmDeployPathGenerator {
    public static String getGlobalNpmPath() {
        if (!OperatingSystemUtility.isWindows()) {
            return "";
        }
        Process p;
        //test.bat中的命令是ipconfig/all
        String cmd = "cmd.exe" + " /C" + " " + "npm config get prefix ";
//        String cmd="jarsigner -verify -verbose -certs C:\\Users\\Administrator\\Desktop\\PandaClient.apk";
        String resultstr = null;
        try {
            //执行命令
            p = Runtime.getRuntime().exec(cmd);
            //取得命令结果的输出流
            InputStream fis = p.getInputStream();
            //用一个读输出流类去读
            //用缓冲器读行
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, "GB2312"));
            String line = null;
            //直到读完为止

            while ((line = br.readLine()) != null) {
                resultstr = line;
            }
        } catch (IOException e) {
            WebLogger.Instance.error(e);
        }
        return resultstr;
    }
}
