/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.webservice;

import com.inspur.edp.web.common.entity.CommandExecutedResult;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.npmpackage.api.entity.CheckNpmPackageJsonParam;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;
import com.inspur.edp.web.npmpackage.api.service.NpmPackageService;
import com.inspur.edp.web.npmpackage.api.webservice.NpmPackageWebService;
import com.inspur.edp.web.npmpackage.core.npminstall.NpmInstallManager;
import com.inspur.edp.web.npmpackage.core.npmsetting.NpmSettingEncryptService;
import com.inspur.edp.web.npmpackage.core.npmsetting.NpmSettingManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * npm 包webservice
 *
 * @author noah
 */
public class NpmPackageWebServiceImpl implements NpmPackageWebService {
    @Override
    public NpmSettings getNpmSetting() {
        NpmSettings npmSettings = NpmSettingManager.getNpmSetting(false);

        npmSettings = NpmSettingEncryptService.getInstance().encrypt(npmSettings);

        return npmSettings;
    }


    /**
     * 保存npm配置信息
     *
     * @param npmSettings npm配置信息
     * @return
     */
    @Override
    public NpmPackageResponse saveNpmSetting(NpmSettings npmSettings) {
        npmSettings = NpmSettingEncryptService.getInstance().deEncrypt(npmSettings);

        return NpmSettingManager.saveNpmSetting(npmSettings);
    }

    @Override
    public NpmPackageResponse checkUpdate() {
        NpmPackageService npmPackageService = SpringBeanUtils.getBean(NpmPackageService.class);
        return npmPackageService.checkUpdate();
    }

    @Override
    public CheckNpmPackageJsonParam checkNodeModules(String executeEnvironment) {
        return NpmInstallManager.checkNpmPackageJson(ExecuteEnvironment.valueOf(executeEnvironment));
    }
}
