/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.core.npmlogout;

import com.inspur.edp.web.common.entity.NodeJsCommandEnum;
import com.inspur.edp.web.common.io.NodejsFunctionUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.CommandLineUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.npmpackage.api.entity.NpmInstallParameter;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import org.apache.commons.lang3.SystemUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * npm 登出命令执行
 *
 * @author guozhiqi
 */
public class NpmLogoutCommandExecutor {
    /**
     * npm登出命令执行
     *
     * @param npmInstallParameter
     * @param currentServerPath
     */
    public static NpmPackageResponse execute(NpmInstallParameter npmInstallParameter, String currentServerPath) {
        // 构造install 命令参数 执行npm包安装操作
        String npmCommandExecuteWithPath = NodejsFunctionUtility.getNodeJsCommandInServerWithOS(NodeJsCommandEnum.Npm, currentServerPath);
        String args = npmCommandExecuteWithPath;

        args += " logout ";

        String npmLoginCommand = NpmLogoutCommandParameterGenerator.generate(npmInstallParameter);

        if (!StringUtility.isNullOrEmpty(npmLoginCommand)) {
            args += npmLoginCommand;
        }

        String[] command = {args};
        // 执行npm 包安装
        return runCommand(command, true);

    }

    public static NpmPackageResponse runCommand(String[] commands, boolean isWait) {
        String command = String.join(" && ", commands);
        return runCommand(command, isWait);
    }

    /**
     * 执行命令
     *
     * @param command 待执行命令
     * @param isWait  是否等待返回结果
     */
    public static NpmPackageResponse runCommand(String command, boolean isWait) {
        String errorMessage = "";
        BufferedReader bufferedReader = null;
        InputStream errinputStream = null;
        StringBuilder errorSB = new StringBuilder();
        try {
            WebLogger.Instance.info(command, NpmLogoutCommandExecutor.class.getName());
            Process process = CommandLineUtility.getProcessWithOs(command);

            InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
            bufferedReader = new BufferedReader(inputStreamReader);

            errinputStream = process.getErrorStream();
            InputStreamReader errInputStreamReader = new InputStreamReader(errinputStream);

            StringBuilder sb = new StringBuilder();

            int len = -1;
            char[] c = new char[1024];
            //读取进程输入流中的内容
            while ((len = inputStreamReader.read(c)) != -1) {
                String s = new String(c, 0, len);
                WebLogger.Instance.info(s, NpmLogoutCommandExecutor.class.getName());
                //判断是否存在错误描述
                if (CommandLineUtility.checkHasError(s)) {
                    errorMessage = s;
                    break;
                }
            }

            // 获取error输出流

            while ((len = errInputStreamReader.read(c)) != -1) {
                String s = new String(c, 0, len);
                errorSB.append(s);
                WebLogger.Instance.info(s, NpmLogoutCommandExecutor.class.getName());
            }


        } catch (Exception e) {
            WebLogger.Instance.error(e);
            errorMessage = e.getMessage() + Arrays.toString(e.getStackTrace());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    WebLogger.Instance.error(e);
                    errorMessage = e.getMessage() + Arrays.toString(e.getStackTrace());
                }
            }
        }
        // 如果出现编译错误 那么通过异常的方式将错误信息进行抛出
        if (!StringUtility.isNullOrEmpty(errorMessage)) {
            return NpmPackageResponse.createError("npm logout failed,the message is " + errorMessage);
        }
        // 如果error信息不为空
        if (!StringUtility.isNullOrEmpty(errorSB.toString()) && errorSB.toString().contains("npm ERR!") && !errorSB.toString().contains("not logged in ")) {
            return NpmPackageResponse.createError(errorSB.toString());
        }
        return NpmPackageResponse.create();
    }

}
