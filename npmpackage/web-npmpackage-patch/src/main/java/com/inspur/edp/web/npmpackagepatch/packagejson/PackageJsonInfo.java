/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackagepatch.packagejson;

import com.inspur.edp.web.npmpackagepatch.packagejson.PackageJsonDependencyInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 补丁文件更新
 *
 * @author noah
 */
public class PackageJsonInfo {
    private String name;
    private String version;
    private String description;

    private String[] keywords;
    private String author = "Noah";
    private String license;
    private List<PackageJsonDependencyInfo> dependencies;
    private List<PackageJsonDependencyInfo> devDependencies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public List<PackageJsonDependencyInfo> getDependencies() {
        if (dependencies == null) {
            dependencies = new ArrayList<>();
        }
        return dependencies;
    }

    public void setDependencies(List<PackageJsonDependencyInfo> dependencies) {
        this.dependencies = dependencies;
    }

    public List<PackageJsonDependencyInfo> getDevDependencies() {
        if (devDependencies == null) {
            devDependencies = new ArrayList<>();
        }
        return devDependencies;
    }

    public void setDevDependencies(List<PackageJsonDependencyInfo> devDependencies) {
        this.devDependencies = devDependencies;
    }

    /**
     * 执行version自增加
     */
    public void versionPlus() {
        if (this.version == null) {
            this.version = "0.0.1";
        }
        int lastQuoteIndex = this.version.lastIndexOf(".");
        String lastVersion = this.version.substring(lastQuoteIndex + 1);
        long lastVersionLang = Long.parseLong(lastVersion) + 1;
        this.version = this.version.substring(0, lastQuoteIndex+1) + lastVersionLang;
    }

}
