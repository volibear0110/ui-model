/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackagepatch;

import java.io.File;
import java.util.List;

/**
 * 递归读取目录下的所有文件列表
 *
 * @author noah
 */
public class RescureReadFiles {
    /**
     * 获取文件目录下所有文件列表
     * @param path
     * @param fileList
     */
    public static void getFiles(String path, List<File> fileList) {
        File file = new File(path);
        File[] array = file.listFiles();
        for (File value : array) {
            if (value.isFile()) {
                fileList.add(value);
            } else if (value.isDirectory()) {
                getFiles(value.getPath(), fileList);
            }
        }
    }
}
