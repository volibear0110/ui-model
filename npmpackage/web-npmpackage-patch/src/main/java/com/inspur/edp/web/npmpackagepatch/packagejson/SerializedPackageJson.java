/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackagepatch.packagejson;

import java.util.HashMap;

/**
 * 可序列化成固定格式的实体结构
 */
public class SerializedPackageJson {
    private String name;
    private String version;
    private String description;

    private String[] keywords;
    private String author = "Noah";
    private String license;
    private HashMap<String, String> dependencies;
    private HashMap<String, String> devDependencies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getKeywords() {
        if (keywords == null) {
            keywords = new String[0];
        }
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public HashMap<String, String> getDependencies() {
        return dependencies;
    }

    public void setDependencies(HashMap<String, String> dependencies) {
        this.dependencies = dependencies;
    }

    public HashMap<String, String> getDevDependencies() {
        return devDependencies;
    }

    public void setDevDependencies(HashMap<String, String> devDependencies) {
        this.devDependencies = devDependencies;
    }
}
