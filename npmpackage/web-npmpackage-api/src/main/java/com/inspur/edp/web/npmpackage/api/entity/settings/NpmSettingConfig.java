/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity.settings;

/**
 * npm 参数配置
 *
 * @author noah
 */
public class NpmSettingConfig {

    /**
     * npm 仓库配置
     */
    private NpmRepository[] repositories = {};

    /**
     * 是否启用移动编译
     */
    private boolean enableMobileBuild=true;

    public NpmRepository[] getRepositories() {
        return repositories;
    }

    public void setRepositories(NpmRepository[] repositories) {
        this.repositories = repositories;
    }

    public boolean isEnableMobileBuild() {
        return enableMobileBuild;
    }

    public void setEnableMobileBuild(boolean enableMobileBuild) {
        this.enableMobileBuild = enableMobileBuild;
    }
}
