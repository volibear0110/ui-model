package com.inspur.edp.web.npmpackage.api.entity;

import com.inspur.edp.web.common.entity.CommandExecutedResult;
import lombok.Data;

@Data
public class CheckNpmPackageJsonParam {
    private String message;
    // 0: 成功,
    // 1: 失败，弹出提示
    // -1:失败，不做处理
    private int successCode;
}
