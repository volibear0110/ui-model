/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity;

/**
 * npm 包常量定义
 *
 * @author noah
 */
public class NpmPackageConstants {
    /**
     * node_modules 常量
     */
    public static final String Node_ModulesName = "node_modules";

    /**
     * package.json 文件名称
     */
    public static final String PackageJsonName = "package.json";

    /**
     * package-lock.json 文件
     */
    public  static  final  String PackageLockJsonName="package-lock.json";

    /**
     * 最新版本标识
     */
    public static final String LatestVersion = "latest";

    /**
     * npm 参数配置文件名称
     */
    public static final String NpmSettingFileName = "setting.json";

    /**
     * npm 参数配置文件相对于nodejs路径的路径名
     */
    public static final String NpmSettingPath = "npmsettings";

    /**
     * 标识npm正在安装过程中的标识
     */
    public static final String NpmInstallingTag = "npminstalling.log";
}
