/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity.settings;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.inspur.edp.web.common.utility.StringUtility;

/**
 * npm 仓库配置
 *
 * @author noah
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NpmRepository {
    /**
     * id 参数
     */
    private String id;

    /**
     * 仓库名称
     */
    private String name;

    /**
     * 仓库地址
     */
    private String url;
    /**
     * 配置登录用户名
     */
    private String userName;
    /**
     * 配置登录密码
     */
    private String password;
    /**
     * 更新策略  默认为手工更新
     */
    private String updatePolicy = NpmUpdatePolicy.beforeBuild;

    /**
     * 最后更新时间 标识node_modules 的最后更新时间戳
     */
    private String lastUpdated;

    /**
     * 是否需要登录
     */
    private boolean needLogin = false;

    /**
     * 是否允许删除  默认值为true  即允许删除
     */
    private boolean canDelete = true;

    private Boolean passwordChanged;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        if (StringUtility.isNullOrEmpty(userName)) {
            userName = "";
        }
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        if (StringUtility.isNullOrEmpty(password)) {
            password = "";
        }
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUpdatePolicy() {
        return updatePolicy;
    }

    public void setUpdatePolicy(String updatePolicy) {
        this.updatePolicy = updatePolicy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isNeedLogin() {
        return needLogin;
    }

    public void setNeedLogin(boolean needLogin) {
        this.needLogin = needLogin;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }
}
