/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmUpdatePolicy;

import java.util.ArrayList;
import java.util.List;

/**
 * npm 包参数
 *
 * @author noah
 */
public class NpmInstallParameter {

    /**
     * 执行npm install时的代理地址
     * 默认为官方源
     */
    private String registry = "https://registry.npmjs.org/";

    /**
     * npm 安装登录用户名 无默认值
     * 针对需要登录才可以install的源，那么必须首先进行登录
     */
    private String userName;

    /**
     * npm 安装密码  无默认值
     * 针对需要登录才可以install的源，那么必须首先进行登录
     */
    private String password;

    /**
     * 登录数据源登录需要输入的email
     */
    private String email;
    /**
     * 使用生产模式进行install
     * 使用该模式 不会安装devdependies的配置脚本
     */
    private boolean useProduction = false;
    /**
     * 是否使用离线模式
     * 默认情况下使用离线模式 即使用已经解压后的node_modules 离线包
     */
    private boolean offlineMode = true;

    /**
     * 是否整体更新
     */
    private boolean updateAll = true;

    /**
     * 是否强制更新
     */
    private boolean force = false;

    /**
     * 是否是移动表单工程
     */
    private boolean isMobile = false;

    /**
     * 更新策略  默认为手工更新
     */
    private String updatePolicy = NpmUpdatePolicy.manual;

    /**
     * 运行位置
     * 默认为设计时
     */
    private ExecuteEnvironment executeEnvironment = ExecuteEnvironment.None;

    private List<NpmInstallPackageParameter> npmInstallPackageParameterList;

    public String getRegistry() {
        // 确保获取到的地址 末尾不包含反斜线
        if (!StringUtility.isNullOrEmpty(this.registry) &&
                this.registry.lastIndexOf("/") == (this.registry.length() - 1)) {
            this.registry = this.registry.substring(0, this.registry.length() - 1);
        }
        return registry;
    }

    public void setRegistry(String registry) {
        this.registry = registry;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isUseProduction() {
        return useProduction;
    }

    public void setUseProduction(boolean useProduction) {
        this.useProduction = useProduction;
    }

    public boolean isOfflineMode() {
        return offlineMode;
    }

    public void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
    }

    public boolean isUpdateAll() {
        return updateAll;
    }

    public void setUpdateAll(boolean updateAll) {
        this.updateAll = updateAll;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public List<NpmInstallPackageParameter> getNpmInstallPackageParameterList() {
        if (this.npmInstallPackageParameterList == null) {
            this.npmInstallPackageParameterList = new ArrayList<>();
        }
        return npmInstallPackageParameterList;
    }

    public void setNpmInstallPackageParameterList(List<NpmInstallPackageParameter> npmInstallPackageParameterList) {
        this.npmInstallPackageParameterList = npmInstallPackageParameterList;
    }

    public ExecuteEnvironment getExecuteEnvironment() {
        return executeEnvironment;
    }

    public void setExecuteEnvironment(ExecuteEnvironment executeEnvironment) {
        this.executeEnvironment = executeEnvironment;
    }

    public boolean isMobile() {
        return isMobile;
    }

    public void setMobile(boolean mobile) {
        isMobile = mobile;
    }

    public String getUpdatePolicy() {
        return updatePolicy;
    }

    public void setUpdatePolicy(String updatePolicy) {
        this.updatePolicy = updatePolicy;
    }

    public String getEmail() {
        if (StringUtility.isNullOrEmpty(this.email)) {
            this.email = "guozhiqi21@163.com";
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
