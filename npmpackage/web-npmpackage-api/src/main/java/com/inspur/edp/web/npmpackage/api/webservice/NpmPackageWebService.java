/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.webservice;

import com.inspur.edp.web.common.entity.CommandExecutedResult;
import com.inspur.edp.web.npmpackage.api.entity.CheckNpmPackageJsonParam;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * npm 包webservice
 *
 * @author noah
 */
@Path("/")
public interface NpmPackageWebService {

    /**
     * 获取npm 配置参数
     * @return
     */
    @GET
    @Path("/npmsetting")
    NpmSettings getNpmSetting();

    /**
     * 保存npm配置信息
     * @param npmSettings npm配置信息
     * @return
     */
    @POST
    @Path("/savenpmsetting")
    NpmPackageResponse saveNpmSetting(NpmSettings npmSettings);

    @GET
    @Path("/npmcheckupdate")
    NpmPackageResponse checkUpdate();

    @GET
    @Path("/nodemodulescheck/{executeEnvironment}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    CheckNpmPackageJsonParam checkNodeModules(@PathParam("executeEnvironment") String executeEnvironment);
}
