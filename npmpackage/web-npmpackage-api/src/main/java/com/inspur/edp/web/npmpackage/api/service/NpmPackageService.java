/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.service;

import com.inspur.edp.web.npmpackage.api.entity.NpmPackageCheckUpdateOptions;
import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;

/**
 * npm 包service
 *
 * @author noah
 * 2023/7/8 15:11
 */
public interface NpmPackageService {
    /**
     * 检查是否发生更新
     *
     * @param checkUpdateOptions
     * @return
     */
    NpmPackageResponse checkUpdate(NpmPackageCheckUpdateOptions checkUpdateOptions);

    /**
     * 检查是否发生更新
     * @return
     */
    NpmPackageResponse checkUpdate();
}
