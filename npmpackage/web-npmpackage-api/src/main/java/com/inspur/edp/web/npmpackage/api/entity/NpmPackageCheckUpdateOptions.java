/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;

/**
 * npm 包checkupdate 选项
 *
 * @author noah
 */
public class NpmPackageCheckUpdateOptions {
    /**
     * 是否在升级工具中执行
     */
    private boolean isUpgradeTool = false;
    /**
     * 执行环境
     */
    private ExecuteEnvironment executeEnvironment = ExecuteEnvironment.Design;

    /**
     * 是否移动表单
     */
    private boolean isMobile = false;

    public boolean isUpgradeTool() {
        return isUpgradeTool;
    }

    public void setUpgradeTool(boolean upgradeTool) {
        isUpgradeTool = upgradeTool;
    }

    public ExecuteEnvironment getExecuteEnvironment() {
        return executeEnvironment;
    }

    public void setExecuteEnvironment(ExecuteEnvironment executeEnvironment) {
        this.executeEnvironment = executeEnvironment;
    }

    public boolean isMobile() {
        return isMobile;
    }

    public void setMobile(boolean mobile) {
        isMobile = mobile;
    }
}
