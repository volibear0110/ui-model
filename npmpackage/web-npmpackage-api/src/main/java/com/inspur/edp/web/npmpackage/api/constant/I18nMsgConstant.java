package com.inspur.edp.web.npmpackage.api.constant;

public class I18nMsgConstant {
    public final static String WEB_NPM_PACKAGE_MSG_0001 = "WEB_NPM_PACKAGE_MSG_0001";
    public final static String WEB_NPM_PACKAGE_MSG_0002 = "WEB_NPM_PACKAGE_MSG_0002";
    public final static String WEB_NPM_PACKAGE_MSG_0003 = "WEB_NPM_PACKAGE_MSG_0003";
    public final static String WEB_NPM_PACKAGE_MSG_0004 = "WEB_NPM_PACKAGE_MSG_0004";
    public final static String WEB_NPM_PACKAGE_MSG_0005 = "WEB_NPM_PACKAGE_MSG_0005";
    public final static String WEB_NPM_PACKAGE_MSG_0006 = "WEB_NPM_PACKAGE_MSG_0006";
    public final static String WEB_NPM_PACKAGE_MSG_0007 = "WEB_NPM_PACKAGE_MSG_0007";
    public final static String WEB_NPM_PACKAGE_MSG_0008 = "WEB_NPM_PACKAGE_MSG_0008";
    public final static String WEB_NPM_PACKAGE_MSG_0009 = "WEB_NPM_PACKAGE_MSG_0009";
    public final static String WEB_NPM_PACKAGE_MSG_0010 = "WEB_NPM_PACKAGE_MSG_0010";
    public final static String WEB_NPM_PACKAGE_MSG_0011 = "WEB_NPM_PACKAGE_MSG_0011";
    public final static String WEB_NPM_PACKAGE_MSG_0012 = "WEB_NPM_PACKAGE_MSG_0012";
    public final static String WEB_NPM_PACKAGE_MSG_0013 = "WEB_NPM_PACKAGE_MSG_0013";
    public final static String WEB_NPM_PACKAGE_MSG_0014 = "WEB_NPM_PACKAGE_MSG_0014";
    public final static String WEB_NPM_PACKAGE_MSG_0015 = "WEB_NPM_PACKAGE_MSG_0015";
    public final static String WEB_NPM_PACKAGE_MSG_0016 = "WEB_NPM_PACKAGE_MSG_0016";
    public final static String WEB_NPM_PACKAGE_MSG_0017 = "WEB_NPM_PACKAGE_MSG_0017";
    public final static String WEB_NPM_PACKAGE_MSG_0018 = "WEB_NPM_PACKAGE_MSG_0018";
    public final static String WEB_NPM_PACKAGE_MSG_0019 = "WEB_NPM_PACKAGE_MSG_0019";
    public final static String WEB_NPM_PACKAGE_MSG_0020 = "WEB_NPM_PACKAGE_MSG_0020";
    public final static String WEB_NPM_PACKAGE_MSG_0021 = "WEB_NPM_PACKAGE_MSG_0021";
    public final static String WEB_NPM_PACKAGE_MSG_0022 = "WEB_NPM_PACKAGE_MSG_0022";
    public final static String WEB_NPM_PACKAGE_MSG_0023 = "WEB_NPM_PACKAGE_MSG_0023";
    public final static String WEB_NPM_PACKAGE_MSG_0024 = "WEB_NPM_PACKAGE_MSG_0024";
    public final static String WEB_NPM_PACKAGE_MSG_0025 = "WEB_NPM_PACKAGE_MSG_0025";
    public final static String WEB_NPM_PACKAGE_MSG_0026 = "WEB_NPM_PACKAGE_MSG_0026";
    public final static String WEB_NPM_PACKAGE_MSG_0027 = "WEB_NPM_PACKAGE_MSG_0027";
    public final static String WEB_NPM_PACKAGE_MSG_0028 = "WEB_NPM_PACKAGE_MSG_0028";
    public final static String WEB_NPM_PACKAGE_MSG_0029 = "WEB_NPM_PACKAGE_MSG_0029";
    public final static String WEB_NPM_PACKAGE_MSG_0030 = "WEB_NPM_PACKAGE_MSG_0030";
    public final static String WEB_NPM_PACKAGE_MSG_0031 = "WEB_NPM_PACKAGE_MSG_0031";
    public final static String WEB_NPM_PACKAGE_MSG_0032 = "WEB_NPM_PACKAGE_MSG_0032";
    public final static String WEB_NPM_PACKAGE_MSG_0033 = "WEB_NPM_PACKAGE_MSG_0033";
    public final static String WEB_NPM_PACKAGE_MSG_0034 = "WEB_NPM_PACKAGE_MSG_0034";
    public final static String WEB_NPM_PACKAGE_MSG_0035 = "WEB_NPM_PACKAGE_MSG_0035";
    public final static String WEB_NPM_PACKAGE_MSG_0036 = "WEB_NPM_PACKAGE_MSG_0036";
    public final static String WEB_NPM_PACKAGE_MSG_0037 = "WEB_NPM_PACKAGE_MSG_0037";
    public final static String WEB_NPM_PACKAGE_MSG_0038 = "WEB_NPM_PACKAGE_MSG_0038";
    public final static String WEB_NPM_PACKAGE_MSG_0039 = "WEB_NPM_PACKAGE_MSG_0039";
    public final static String WEB_NPM_PACKAGE_MSG_0040 = "WEB_NPM_PACKAGE_MSG_0040";
    public final static String WEB_NPM_PACKAGE_MSG_0041 = "WEB_NPM_PACKAGE_MSG_0041";
    public final static String WEB_NPM_PACKAGE_MSG_0042 = "WEB_NPM_PACKAGE_MSG_0042";
    public final static String WEB_NPM_PACKAGE_MSG_0043 = "WEB_NPM_PACKAGE_MSG_0043";
    public final static String WEB_NPM_PACKAGE_MSG_0044 = "WEB_NPM_PACKAGE_MSG_0044";

}
