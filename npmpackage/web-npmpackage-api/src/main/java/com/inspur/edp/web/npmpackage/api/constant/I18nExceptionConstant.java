package com.inspur.edp.web.npmpackage.api.constant;

public class I18nExceptionConstant {
    public final static String WEB_NPM_PACKAGE_ERROR_0001 = "WEB_NPM_PACKAGE_ERROR_0001";
    public final static String WEB_NPM_PACKAGE_ERROR_0002 = "WEB_NPM_PACKAGE_ERROR_0002";
    public final static String WEB_NPM_PACKAGE_ERROR_0003 = "WEB_NPM_PACKAGE_ERROR_0003";
    public final static String WEB_NPM_PACKAGE_ERROR_0004 = "WEB_NPM_PACKAGE_ERROR_0004";
    public final static String WEB_NPM_PACKAGE_ERROR_0005 = "WEB_NPM_PACKAGE_ERROR_0005";
    public final static String WEB_NPM_PACKAGE_ERROR_0006 = "WEB_NPM_PACKAGE_ERROR_0006";
    public final static String WEB_NPM_PACKAGE_ERROR_0007 = "WEB_NPM_PACKAGE_ERROR_0007";
    public final static String WEB_NPM_PACKAGE_ERROR_0008 = "WEB_NPM_PACKAGE_ERROR_0008";
    public final static String WEB_NPM_PACKAGE_ERROR_0009 = "WEB_NPM_PACKAGE_ERROR_0009";
    public final static String WEB_NPM_PACKAGE_ERROR_0010 = "WEB_NPM_PACKAGE_ERROR_0010";
    public final static String WEB_NPM_PACKAGE_ERROR_0011 = "WEB_NPM_PACKAGE_ERROR_0011";
    public final static String WEB_NPM_PACKAGE_ERROR_0012 = "WEB_NPM_PACKAGE_ERROR_0012";
    public final static String WEB_NPM_PACKAGE_ERROR_0013 = "WEB_NPM_PACKAGE_ERROR_0013";
    public final static String WEB_NPM_PACKAGE_ERROR_0014 = "WEB_NPM_PACKAGE_ERROR_0014";
    public final static String WEB_NPM_PACKAGE_ERROR_0015 = "WEB_NPM_PACKAGE_ERROR_0015";
}
