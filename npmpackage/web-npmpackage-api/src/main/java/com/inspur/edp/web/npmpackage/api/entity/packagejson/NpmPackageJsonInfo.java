/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity.packagejson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author guozhiqi
 */
public class NpmPackageJsonInfo {
    private String name;
    private String version;
    private String description;
    private String author = "Noah";
    private String license;
    private List<NpmPackageJsonDependencyInfo> dependencies;
    private List<NpmPackageJsonDependencyInfo> devDependencies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public List<NpmPackageJsonDependencyInfo> getDependencies() {
        if (dependencies == null) {
            this.dependencies = new ArrayList<>();
        }
        return dependencies;
    }

    public void setDependencies(List<NpmPackageJsonDependencyInfo> dependencies) {
        this.dependencies = dependencies;
    }

    public List<NpmPackageJsonDependencyInfo> getDevDependencies() {
        if (this.devDependencies == null) {
            this.devDependencies = new ArrayList<>();
        }
        return devDependencies;
    }

    public void setDevDependencies(List<NpmPackageJsonDependencyInfo> devDependencies) {
        this.devDependencies = devDependencies;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NpmPackageJsonInfo that = (NpmPackageJsonInfo) o;
        if (this.getDependencies().size() != that.getDependencies().size() || this.getDevDependencies().size() != that.getDevDependencies().size()) {
            return false;
        }

        final boolean[] exists = {true};
        this.getDependencies().forEach(t -> {
            if (exists[0]) {
                String dependencyKey = t.getKey();
                String dependencyValue = t.getValue();
                exists[0] = that.getDependencies().stream().anyMatch(d -> d.getKey().equals(dependencyKey) && d.getValue().equals(dependencyValue));
            }
        });
        if (!exists[0]) {
            return false;
        }
        this.getDevDependencies().forEach(t -> {
            if (exists[0]) {
                String dependencyKey = t.getKey();
                String dependencyValue = t.getValue();
                exists[0] = that.getDevDependencies().stream().anyMatch(d -> d.getKey().equals(dependencyKey) && d.getValue().equals(dependencyValue));
            }
        });
        if (!exists[0]) {
            return false;
        }


        return this.version.equals(that.version);
    }

    public boolean equalsInDependencies(String dependencyName, String version) {
        return this.getDependencies().stream().anyMatch(d -> d.getKey().equals(dependencyName) && d.getValue().equals(version));
    }

    public boolean equalsInDevDependencies(String dependencyName, String version) {
        return this.getDevDependencies().stream().anyMatch(d -> d.getKey().equals(dependencyName) && d.getValue().equals(version));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, version, description, author, license, dependencies, devDependencies);
    }
}
