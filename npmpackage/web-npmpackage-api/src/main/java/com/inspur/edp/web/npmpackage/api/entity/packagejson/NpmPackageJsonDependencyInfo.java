/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity.packagejson;

import com.inspur.edp.web.common.utility.StringUtility;

public class NpmPackageJsonDependencyInfo {
    private String key;
    private String value;

    public NpmPackageJsonDependencyInfo(String key, String value) {
        this.key = !StringUtility.isNullOrEmpty(key) ? key.trim() : "";
        this.value = !StringUtility.isNullOrEmpty(value) ? value.trim() : "";
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
