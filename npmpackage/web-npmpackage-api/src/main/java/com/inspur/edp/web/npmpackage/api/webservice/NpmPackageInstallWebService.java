/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.webservice;

import com.inspur.edp.web.npmpackage.api.entity.NpmPackageResponse;
import com.inspur.edp.web.npmpackage.api.entity.settings.NpmSettings;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * npm 包安装webservice
 *
 * @author noah
 */
@Path("/")
public interface NpmPackageInstallWebService {

    /**
     * npm install命令执行
     * @param installParameter install参数
     * @return 安装结果反馈
     */
    @POST
    @Path("/npminstall")
    NpmPackageResponse install(NpmSettings installParameter);

    /**
     * npm install命令执行 使用默认的npm配置信息
     * @return 安装结果反馈
     */
    @GET
    @Path("/npminstall")
    NpmPackageResponse install();

    /**
     * npm install命令执行
     * @param installParameter install参数
     * @return 安装结果反馈
     */
    @POST
    @Path("/npminstallcheck")
    NpmPackageResponse installCheck(NpmSettings installParameter);


}
