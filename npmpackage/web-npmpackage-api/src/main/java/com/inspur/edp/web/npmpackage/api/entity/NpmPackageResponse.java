/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.npmpackage.api.entity;

/**
 * npm包安装响应
 *
 * @author noah
 */
public class NpmPackageResponse {
    /**
     * 是否执行成功标识
     */
    private boolean success = true;
    /**
     * 执行失败 错误信息提示
     */
    private String errorMessage;

    private NpmPackageResponseLevel responseLevel = NpmPackageResponseLevel.Info;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public NpmPackageResponseLevel getResponseLevel() {
        return responseLevel;
    }

    public Object getExtraData() {
        return extraData;
    }

    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }

    /**
     * 额外的数据传递
     */
    public Object extraData;

    public void setResponseLevel(NpmPackageResponseLevel responseLevel) {
        this.responseLevel = responseLevel;
    }

    public static NpmPackageResponse create() {
        return new NpmPackageResponse();
    }
    public static NpmPackageResponse createError(String errorMessage) {
        NpmPackageResponse npmPackageResponse=new NpmPackageResponse();
        npmPackageResponse.setErrorMessage(errorMessage);
        npmPackageResponse.setSuccess(false);
        npmPackageResponse.setResponseLevel(NpmPackageResponseLevel.Error);
        return npmPackageResponse;
    }

}
