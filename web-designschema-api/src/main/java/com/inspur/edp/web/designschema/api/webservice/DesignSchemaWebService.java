/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.api.webservice;

import com.alibaba.fastjson.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;


/**
 * schema 同步webservice
 * @author  noah
 */
@Path("/")
public interface DesignSchemaWebService
{

	/**
	 * 创建designschema
	 * @param content
	 * @return
	 */
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON,MediaType.TEXT_PLAIN})
    String CreateDesignSchema(JSONObject content);

	@POST
	@Path("/createByScene")
    String CreateDesignSchemaByScene(JSONObject content);
}
