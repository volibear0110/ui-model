@echo off

for /f "tokens=*" %%i in ('CALL .\xpath0.1.bat pom.xml "/project/properties/custom.version"') do set version=%%i
ECHO version=%version%

DEL /S/Q .\out

MKDIR .\out\server\platform\dev\main\libs
MKDIR .\out\server\platform\common\libs
MKDIR .\out\server\platform\runtime\bcc\libs


COPY .\web-appconfig-core\target\web-appconfig-core-%version%.jar .\out\server\platform\common\libs\web-appconfig-core.jar
COPY .\web-appconfig-manager\target\web-appconfig-manager-%version%.jar .\out\server\platform\dev\main\libs\web-appconfig-manager.jar
COPY .\web-appconfig-webapi\target\web-appconfig-webapi-%version%.jar .\out\server\platform\dev\main\libs\web-appconfig-webapi.jar
COPY .\web-common\target\web-common-%version%.jar .\out\server\platform\common\libs\web-common.jar

COPY .\runtime-api\target\web-jitengine-runtimebuild-api-%version%.jar .\out\server\platform\common\libs\web-jitengine-runtimebuild-api.jar
COPY .\runtime-core\target\web-jitengine-runtimebuild-core-%version%.jar .\out\server\platform\common\libs\web-jitengine-runtimebuild-core.jar

COPY .\runtime-scriptcache\target\web-jitengine-runtimebuild-scriptcache-%version%.jar .\out\server\platform\common\libs\web-jitengine-runtimebuild-scriptcache.jar
COPY .\runtime-scriptcache-api\target\web-jitengine-runtimebuild-scriptcache-api-%version%.jar .\out\server\platform\common\libs\web-jitengine-runtimebuild-scriptcache-api.jar

COPY .\web-appconfig-core\target\web-appconfig-core-%version%.jar .\out\server\platform\common\libs\web-appconfig-core.jar
COPY .\web-appconfig-api\target\web-appconfig-api-%version%.jar .\out\server\platform\common\libs\web-appconfig-api.jar

COPY .\web-common\target\web-jitengine-common-%version%.jar .\out\server\platform\common\libs\web-jitengine-common.jar


COPY .\web-form-jitengine\target\web-jitengine-%version%.jar .\out\server\platform\common\libs\web-jitengine.jar

COPY .\web-form-metadata\target\web-jitengine-formmetadata-%version%.jar .\out\server\platform\common\libs\web-jitengine-formmetadata.jar
COPY .\web-form-metadata-api\target\web-jitengine-formmetadata-api-%version%.jar .\out\server\platform\common\libs\web-jitengine-formmetadata-api.jar

COPY .\web-frontendproject\target\web-jitengine-frontendproject-%version%.jar .\out\server\platform\common\libs\web-jitengine-frontendproject.jar
COPY .\web-frontendproject-api\target\web-jitengine-frontendproject-api-%version%.jar .\out\server\platform\common\libs\web-jitengine-frontendproject-api.jar

COPY .\web-designschema\target\web-designschema-%version%.jar .\out\server\platform\common\libs\web-designschema.jar
COPY .\web-designschema-api\target\web-designschema-api-%version%.jar .\out\server\platform\common\libs\web-designschema-api.jar

COPY .\web-dynamic-form-api\target\web-dynamicform-api-%version%.jar .\out\server\platform\common\libs\web-dynamicform-api.jar
COPY .\web-dynamic-form-core\target\web-dynamicform-core-%version%.jar .\out\server\platform\common\libs\web-dynamicform-core.jar

COPY .\web-pageflow-metadata\target\web-pageflow-metadata-%version%.jar .\out\server\platform\common\libs\web-pageflow-metadata.jar
COPY .\web-statemachine\target\web-statemachine-metadata-%version%.jar .\out\server\platform\common\libs\web-statemachine-metadata.jar

COPY .\web-sourcecode-metadata\target\web-sourcecode-metadata-%version%.jar .\out\server\platform\common\libs\web-sourcecode-metadata.jar

COPY .\web-tsfile-api\target\web-tsfile-api-%version%.jar .\out\server\platform\common\libs\web-tsfile-api.jar
COPY .\web-tsfile-core\target\web-tsfile-core-%version%.jar .\out\server\platform\common\libs\web-tsfile-core.jar

COPY .\jitengine-web-api\target\web-jitengine-web-api-%version%.jar .\out\server\platform\common\libs\web-jitengine-web-api.jar
COPY .\jitengine-web-core\target\web-jitengine-web-core-%version%.jar .\out\server\platform\common\libs\web-jitengine-web-core.jar

COPY .\web-approval-format-api\target\web-approval-format-api-%version%.jar .\out\server\platform\runtime\bcc\libs\web-approval-format-api.jar
COPY .\web-approval-format-core\target\web-approval-format-core-%version%.jar .\out\server\platform\runtime\bcc\libs\web-approval-format-core.jar
COPY .\web-approval-format-rpc\target\web-approval-format-rpc-%version%.jar .\out\server\platform\common\libs\web-approval-format-rpc.jar

COPY .\web-npmpackage-api\target\web-npmpackage-api-%version%.jar .\out\server\platform\common\libs\web-npmpackage-api.jar
COPY .\web-npmpackage-core\target\web-npmpackage-core-%version%.jar .\out\server\platform\common\libs\web-npmpackage-core.jar

COPY .\web-npmpackage-api\target\web-npmpackage-api-%version%.jar .\out\server\platform\common\libs\web-npmpackage-api.jar
COPY .\web-npmpackage-core\target\web-npmpackage-core-%version%.jar .\out\server\platform\common\libs\web-npmpackage-core.jar

COPY .\web-formconfig-api\target\web-formconfig-api-%version%.jar .\out\server\platform\common\libs\web-formconfig-api.jar
COPY .\web-formconfig-core\target\web-formconfig-core-%version%.jar .\out\server\platform\common\libs\web-formconfig-core.jar

COPY .\web-formmetadata-relycheck\target\web-formmetadata-relycheck-%version%.jar .\out\server\platform\common\libs\web-formmetadata-relycheck.jar
::pause
::pause


