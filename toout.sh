version=$(sed -n 's/.*<custom.version>\([^<]*\)<\/custom.version>.*/\1/p' pom.xml)
echo $version
rm -rf  out

mkdir -p out/server/platform/dev/main/libs
mkdir -p out/server/platform/common/libs
mkdir -p out/server/platform/runtime/bcc/libs
cp appconfig/web-appconfig-core/target/web-appconfig-core-$version.jar ./out/server/platform/common/libs/web-appconfig-core.jar
cp appconfig/web-appconfig-api/target/web-appconfig-api-$version.jar ./out/server/platform/common/libs/web-appconfig-api.jar

cp approval-format/web-approval-format-api/target/web-approval-format-api-$version.jar ./out/server/platform/runtime/bcc/libs/web-approval-format-api.jar
cp approval-format/web-approval-format-core/target/web-approval-format-core-$version.jar ./out/server/platform/runtime/bcc/libs/web-approval-format-core.jar
cp approval-format/web-approval-format-rpc/target/web-approval-format-rpc-$version.jar ./out/server/platform/common/libs/web-approval-format-rpc.jar

cp form-process/web-form-process/target/web-form-process-$version.jar ./out/server/platform/dev/main/libs/web-form-process.jar

cp jitengine-web-api/target/web-jitengine-web-api-$version.jar ./out/server/platform/common/libs/web-jitengine-web-api.jar
cp jitengine-web-core/target/web-jitengine-web-core-$version.jar ./out/server/platform/common/libs/web-jitengine-web-core.jar

cp metadata/web-pageflow-metadata/target/web-pageflow-metadata-$version.jar ./out/server/platform/common/libs/web-pageflow-metadata.jar
cp metadata/web-sourcecode-metadata/target/web-sourcecode-metadata-$version.jar ./out/server/platform/common/libs/web-sourcecode-metadata.jar
cp metadata/web-statemachine/target/web-statemachine-metadata-$version.jar ./out/server/platform/common/libs/web-statemachine-metadata.jar

cp npmpackage/web-npmpackage-api/target/web-npmpackage-api-$version.jar ./out/server/platform/common/libs/web-npmpackage-api.jar
cp npmpackage/web-npmpackage-core/target/web-npmpackage-core-$version.jar ./out/server/platform/common/libs/web-npmpackage-core.jar

cp runtime/runtime-api/target/web-jitengine-runtimebuild-api-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-api.jar
cp runtime/runtime-core/target/web-jitengine-runtimebuild-core-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-core.jar

cp scriptcache/runtime-scriptcache/target/web-jitengine-runtimebuild-scriptcache-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-scriptcache.jar
cp scriptcache/runtime-scriptcache-api/target/web-jitengine-runtimebuild-scriptcache-api-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-scriptcache-api.jar

cp tsfile/web-tsfile-api/target/web-tsfile-api-$version.jar ./out/server/platform/common/libs/web-tsfile-api.jar
cp tsfile/web-tsfile-core/target/web-tsfile-core-$version.jar ./out/server/platform/common/libs/web-tsfile-core.jar

cp web-common/target/web-jitengine-common-$version.jar ./out/server/platform/common/libs/web-jitengine-common.jar

cp web-designschema/target/web-designschema-$version.jar ./out/server/platform/common/libs/web-designschema.jar
cp web-designschema-api/target/web-designschema-api-$version.jar ./out/server/platform/common/libs/web-designschema-api.jar

#cp web-dynamic-form-api/target/web-dynamicform-api-$version.jar ./out/server/platform/common/libs/web-dynamicform-api.jar
#cp web-dynamic-form-core/target/web-dynamicform-core-$version.jar ./out/server/platform/common/libs/web-dynamicform-core.jar

cp web-form-jitengine/target/web-jitengine-$version.jar ./out/server/platform/common/libs/web-jitengine.jar

cp web-form-metadata/target/web-jitengine-formmetadata-$version.jar ./out/server/platform/common/libs/web-jitengine-formmetadata.jar
cp web-form-metadata-api/target/web-jitengine-formmetadata-api-$version.jar ./out/server/platform/common/libs/web-jitengine-formmetadata-api.jar

#cp web-formconfig-api/target/web-formconfig-api-$version.jar ./out/server/platform/common/libs/web-formconfig-api.jar
#cp web-formconfig-core/target/web-formconfig-core-$version.jar ./out/server/platform/common/libs/web-formconfig-core.jar

#cp web-formmetadata-relycheck/target/web-formmetadata-relycheck-$version.jar ./out/server/platform/dev/main/libs/web-formmetadata-relycheck.jar

cp web-frontendproject/target/web-jitengine-frontendproject-$version.jar ./out/server/platform/common/libs/web-jitengine-frontendproject.jar
cp web-frontendproject-api/target/web-jitengine-frontendproject-api-$version.jar ./out/server/platform/common/libs/web-jitengine-frontendproject-api.jar

#cp web-ide-api/target/web-ide-api-$version.jar ./out/server/platform/common/libs/web-ide-api.jar
cp web-ide-webapi/target/ide-config-webapi-$version.jar ./out/server/platform/dev/main/libs/ide-config-webapi.jar





