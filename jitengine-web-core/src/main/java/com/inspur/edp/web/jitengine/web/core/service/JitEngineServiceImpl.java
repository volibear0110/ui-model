/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.web.core.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitengine.web.api.service.JitEngineService;

public class JitEngineServiceImpl implements JitEngineService {

    @Override
    public void deleteMobileApproveDevSource(String formId) throws Exception {
        if (StringUtility.isNullOrEmpty(formId)) {
            throw new WebCustomException("delete mobileapprove dev source failed,the formId is null");
        }

        GspMetadata gspMetadata = MobileApprovePreviewImpl.getGspMetadataWithFormId(formId);
        String formCode = gspMetadata.getHeader().getCode();
        String mobileApproveAppPath = MobileApprovePreviewImpl.generateMobileApproveDevBasePath(formCode, false);
        try {
            FileUtility.deleteFolder(mobileApproveAppPath);
        } catch (Exception ex) {
            WebLogger.Instance.error(ex);
        }
    }
}
