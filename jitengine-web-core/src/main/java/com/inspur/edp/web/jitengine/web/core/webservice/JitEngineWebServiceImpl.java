/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.web.core.webservice;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.jitengine.JITEngineManager;
import com.inspur.edp.web.jitengine.ProjectCompileContext;
import com.inspur.edp.web.jitengine.web.api.service.MobileApproveReviewParameter;
import com.inspur.edp.web.jitengine.web.api.service.MobileApproveReviewResponse;
import com.inspur.edp.web.jitengine.web.api.webservice.JitEngineWebService;
import com.inspur.edp.web.jitengine.web.core.service.MobileApprovePreviewImpl;
import com.inspur.edp.web.jitengine.web.core.service.MobileApproveDocker;

import javax.annotation.Resource;
import java.io.IOException;

public class JitEngineWebServiceImpl implements JitEngineWebService {

    @Resource
    private MobileApproveDocker mobileApproveDocker;

    @Override
    public Object createNewTemplate(Object data) {
        return null;
    }

    @Override
    public Object getMetaDataById(String id) {
        return null;
    }

    /**
     * 移动审批预览
     *
     * @param data
     * @return
     * @throws Exception
     */
    @Override
    public Object preview(MobileApproveReviewParameter data) throws Exception {
        // 将参数转换成预览实体类型
        if (data == null) {
            return null;
        }

        // 根据维度信息读取表单元数据信息
        GspMetadata gspMetadata = MobileApprovePreviewImpl.getGspMetaDataWithPreviewParameter(data);
        //FormMetaDataContent
        String serializedMetaDataContent = SerializeUtility.getInstance().serialize(gspMetadata.getContent());
        FormMetadataContent formMetadataContent = SerializeUtility.getInstance().deserialize(serializedMetaDataContent, FormMetadataContent.class);

        String metaDataContent = formMetadataContent.getContents().toString();
        // 保存表单json文件
        // 根据id读取对应的表单json文件
        // 获取su的路径参数
        String formCode = gspMetadata.getHeader().getCode();
        String webDevPath = MobileApprovePreviewImpl.generateWebDevPath(formCode, false);
        // 全部转换成小写
        String publishPath = MobileApprovePreviewImpl.generatePublishPath(formCode, gspMetadata, false);
        // 保存表单元数据至json文件
        MobileApprovePreviewImpl.saveMetaDataContent(webDevPath, formCode + ".frm.json", metaDataContent);

        String serviceUnitPath = MobileApprovePreviewImpl.getServiceUnit(gspMetadata);

        // 构造webdev文件路径
        // 设置为移动审批
        ProjectCompileContext compileContext = new ProjectCompileContext(
                formCode,
                "",
                "pc", "angular",
                webDevPath,
                publishPath,
                "",
                serviceUnitPath, ExecuteEnvironment.Runtime
        );
        // 设置移动审批表单code
        compileContext.setMobileApproveFormCode(formCode);
        compileContext.setIsMobileApprove(true);

        JITEngineManager.compileProject(compileContext);

        // 调用jit进行脚本文件生成
        // js存入数据库
        try {
            this.mobileApproveDocker.afterPreview(formCode, publishPath, data.getFormId());
        } catch (Exception e) {
            throw new WebCustomException(e.getMessage());
        }

        // 返回文件路径
        String publishFormUrl = MobileApprovePreviewImpl.generatePublishFormPathUrl(formCode, gspMetadata);
        MobileApproveReviewResponse reviewResponse = new MobileApproveReviewResponse();
        // 返回路径转换成小写
        String reviewPath = publishFormUrl + "/" + (formCode + ".js").toLowerCase();
        reviewResponse.setPath(reviewPath);
        return reviewResponse;
    }
}
