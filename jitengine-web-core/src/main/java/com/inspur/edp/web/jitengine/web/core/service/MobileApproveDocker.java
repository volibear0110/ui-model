/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.web.core.service;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.service.ScriptCacheService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerPathGenerator;
import com.inspur.edp.web.jitruntimebuild.scriptcache.service.ScriptCacheServiceImpl;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

/**
 * 移动审批
 * 将移动审批脚本保存至对应的数据库表，作为负载均衡使用
 *
 * @author noah
 */
public class MobileApproveDocker {

    @Resource
    private ScriptCacheService scriptCacheService;

    public void afterPreview(String formCode, String publishPath, String metadataId) {
        // 去部署路径取js内容，存入数据库
        // projectName="mobileapproval"
        try {
            PublishScriptRequest request = new PublishScriptRequest();
            request.setFormCode(formCode);
            request.setProjectName("bo-mobileapprovalpresetproject");
            request.setMetaDataId(metadataId);
            // 部署路径
            request.setAbsoluteBaseDirectory(publishPath);
            // 设置脚本文件相对于运行环境的相对路径
            String strLocalServerPath = LocalServerPathGenerator.getNewInstance(false).getLocalServerWebPath();
            String strRelativePath = FileUtility.getRelativePath(strLocalServerPath, publishPath, true);
            request.setProjectRelativePath(strRelativePath);

            request.setScriptName(formCode + ".js");

            this.scriptCacheService.publishScriptWithDirectory(request);
            WebLogger.Instance.info("execute mobile approval script cache completely!!!", this.getClass().getName());
        } catch (Exception e) {
            WebLogger.Instance.error(e);
            throw new WebCustomException(e.getMessage());
        }
    }


}
