/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.web.core.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.jitengine.web.api.constant.I18nExceptionConstant;
import com.inspur.edp.web.jitengine.web.api.service.MobileApproveReviewParameter;
import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
import io.iec.edp.caf.businessobject.api.service.DevBasicInfoService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/05/17
 */
public class MobileApprovePreviewImpl {
    private static final DevBasicInfoService devBasicInfoService = SpringBeanUtils.getBean(DevBasicInfoService.class);

    /**
     * 根据预览参数获取元数据信息
     *
     * @param reviewParameter
     * @return
     */
    public static GspMetadata getGspMetaDataWithPreviewParameter(MobileApproveReviewParameter reviewParameter) throws Exception {
        return getGspMetadataWithFormId(reviewParameter.getFormId());
    }

    /**
     * 根据formId获取元数据
     *
     * @param formId
     * @return
     * @throws Exception
     */

    public static GspMetadata getGspMetadataWithFormId(String formId) {
        GspMetadata gspMetadata = MetadataUtility.getInstance().getMetadataInRuntime(formId);

        if (gspMetadata == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_JIT_ENGINE_API_ERROR_0001);
        }
        return gspMetadata;
    }


    /**
     * 保存表单元数据
     *
     * @param basePath
     * @param fileName
     */
    public static void saveMetaDataContent(String basePath, String fileName, String content) {

        FileUtility.writeFile(basePath, fileName, content);
    }

    public static String generateWebDevPath(String formCode, boolean isUpgradeTool) {
        String projectRuntimeAppBasePath = generateMobileApproveDevBasePath(formCode, isUpgradeTool);
        String projectRuntimeAppWebdevPath = projectRuntimeAppBasePath + FileUtility.DIRECTORY_SEPARATOR_CHAR + "webdev";
        return projectRuntimeAppWebdevPath;
    }

    /**
     * 获取移动审批对应表单路径
     *
     * @param formCode
     * @return
     */
    public static String generateMobileApproveDevBasePath(String formCode, boolean isUpgradeTool) {
        String currentWorkSpace = FileUtility.getCurrentWorkPath(isUpgradeTool);
        return FileUtility.combine(currentWorkSpace, "web/runtime/projects/mobileapproval", formCode);
    }


    public static String generatePublishPath(String formCode, GspMetadata gspMetadata, boolean isUpgradeTool) {
        String serviceUnitPath = getServiceUnit(gspMetadata);
        String currentWorkSpace = FileUtility.getCurrentWorkPath(isUpgradeTool);

        String projectRuntimeAppPath = currentWorkSpace + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                ("web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                        "apps" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                        serviceUnitPath + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                        "mobileapproval" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                        formCode + FileUtility.DIRECTORY_SEPARATOR_CHAR).toLowerCase();
        return projectRuntimeAppPath;
    }

    public static String generatePublishFormPathUrl(String formCode, GspMetadata gspMetadata) {
        String serviceUnitPath = getServiceUnit(gspMetadata);

        String projectRuntimeAppPath = "/apps/" + serviceUnitPath + "/mobileapproval/" + formCode;
        return projectRuntimeAppPath.toLowerCase();
    }

    /**
     * 获取元数据所在su
     *
     * @param gspMetadata
     * @return
     */
    public static String getServiceUnit(GspMetadata gspMetadata) {
        DevBasicBoInfo devBasicBoInfo = devBasicInfoService.getDevBasicBoInfo(gspMetadata.getHeader().getBizobjectID());
        return (devBasicBoInfo.getAppCode() + "/" + devBasicBoInfo.getSuCode()).toLowerCase();
    }
}
