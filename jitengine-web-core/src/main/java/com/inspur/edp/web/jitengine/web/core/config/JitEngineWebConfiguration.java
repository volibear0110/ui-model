/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitengine.web.core.config;

import com.inspur.edp.web.jitengine.web.core.service.MobileApproveDocker;
import io.iec.edp.caf.rest.RESTEndpoint;
import com.inspur.edp.web.jitengine.web.api.service.JitEngineService;
import com.inspur.edp.web.jitengine.web.api.webservice.JitEngineWebService;
import com.inspur.edp.web.jitengine.web.core.service.JitEngineServiceImpl;
import com.inspur.edp.web.jitengine.web.core.webservice.JitEngineWebServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JitEngineWebConfiguration {

    @Bean
    public JitEngineService jitEngineService() {
        return new JitEngineServiceImpl();
    }

    @Bean
    public JitEngineWebService jitEngineWebService(JitEngineService jitEngineService) {
        return new JitEngineWebServiceImpl();
    }

    @Bean
    @Scope(value = "prototype")
    public MobileApproveDocker mobileApproveDocker() {
        return new MobileApproveDocker();
    }

    @Bean
    public RESTEndpoint JitEngineWebServiceEndpoint(JitEngineWebService jitEngineWebService) {
        return new RESTEndpoint("/runtime/bcc/v1.0", jitEngineWebService);
    }
}
