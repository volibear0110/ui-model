package com.inspur.edp.web.form.process.entity;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.Arrays;

public enum FarrisContainerType {
    Form, TabPage, FieldSet, DataGrid, Component, Section, ContentContainer, ComponentRef, Tab, ToolBar, Splitter, SplitterPane, FileUploadPreview, ApprovalLogs, ApprovalComments, DiscussionEditor, ResponseLayout, ResponseLayoutItem;

    public static boolean isForm(String type) {
        return FarrisContainerType.Form.name().equals(type);
    }

    public static boolean isTabPage(String type) {
        return FarrisContainerType.TabPage.name().equals(type);
    }

    public static boolean isFieldSet(String type) {
        return FarrisContainerType.FieldSet.name().equals(type);
    }

    public static boolean isDataGrid(String type) {
        return FarrisContainerType.DataGrid.name().equals(type);
    }

    public static boolean isSection(String type) {
        return FarrisContainerType.Section.name().equals(type);
    }

    public static boolean isValidType(String type) {
        if (StringUtility.isNullOrEmpty(type)) {
            return false;
        } else {
            return Arrays.stream(FarrisContainerType.values()).anyMatch(enumType -> enumType.name().equals(type));
        }
    }
}
