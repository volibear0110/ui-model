package com.inspur.edp.web.form.process.entity;


import lombok.Data;

@Data
public class ProcessParam {
    //表单id，必须
    private String formId;

    //表单路径，必须
    private String formPath;

    //流程分类id，必须
    private String processCategory;

    //业务种类id,通过接口获取
    private String bizCategory;

    //菜单id，必须
    private String formMenu;
    //菜单图片路径，可选，暂未使用
    private String iconUrl;
    //描述,可选，暂未使用
    private String description;
    //是否启用
    private Boolean enable;
}
