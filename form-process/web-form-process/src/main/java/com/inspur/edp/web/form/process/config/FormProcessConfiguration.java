package com.inspur.edp.web.form.process.config;

import com.inspur.edp.web.form.process.service.FormProcessWebService;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.ed.web.form.process.config.FormProcessConfiguration")
public class FormProcessConfiguration {
    @Bean
    public RESTEndpoint formProcessWebapiEndPoint() {
        return new RESTEndpoint("/dev/main/v1.0/form/form-process", new Object[]{new FormProcessWebService()});
    }
}

