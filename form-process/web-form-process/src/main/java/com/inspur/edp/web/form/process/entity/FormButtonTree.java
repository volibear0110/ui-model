package com.inspur.edp.web.form.process.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.wf.bizprocess.entity.FormButton;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FormButtonTree {
    private FormButton data;
    private boolean expanded = true;
    private List<FormButtonTree> children = new ArrayList<>();

    public FormButton getData() {
        return data;
    }

    public void setData(FormButton data) {
        this.data = data;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public List<FormButtonTree> getChildren() {
        return children;
    }

    public void setChildren(List<FormButtonTree> children) {
        this.children = children;
    }
    @JsonIgnore
    public List<FormButton> getChildButtons(){
        return this.children.stream().map(treeNode -> {
            String parentName = this.data.getButtonName();
            String name = treeNode.getData().getButtonName();
            treeNode.getData().setButtonName(name + "(" + parentName + ")");
            return treeNode.getData();
        }).collect(Collectors.toList());
    }
}

