package com.inspur.edp.web.form.process.utils;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.common.ViewModelActionUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.form.process.entity.ProcessParam;

public class VOUtils {

    /**
     * 添加草稿相关构件到vo元数据。
     * <p>注意这里会立即保存vo元数据。</p>
     *
     * @param app   流程信息。
     * @param beMetadata be元数据。
     * @param voMd vo元数据。
     */
    public static void addDraftCmp(ProcessParam app, GspMetadata beMetadata,GspMetadata voMd) {
        // 给vo添加存储参数的变量
        String processCategory = app.getProcessCategory();
        String bizCategory = app.getBizCategory();
        ViewModelActionUtil.addVoActions((GspViewModel) voMd.getContent(), beMetadata.getHeader().getId(), processCategory, bizCategory, null);
        MetadataUtility.getInstance().saveMetadataWithDesign(voMd);
    }

}
