 package com.inspur.edp.web.form.process.service;
 import com.inspur.edp.web.form.process.entity.ProcessParam;

 import javax.ws.rs.*;

 @Path("/")
 @Consumes({"application/json"})
 @Produces({"application/json"})
 public class FormProcessWebService {
   @Path("/form-format")
   @PUT
   public void publishFormFormat(@QueryParam("id") String formId, @QueryParam("path") String path) {
     FormProcessManager.getInstance().publishFormFormat(formId, path);
   }

     @Path("/mobile-register-proccess")
     @POST
     public void mobileRegisterToProccessCenter(ProcessParam param) {
         FormProcessManager.getInstance().mobileRegisterToProccessCenter(param);
     }
 }

