package com.inspur.edp.web.form.process.constant;

public class I18nExceptionConstant {
    public final static String WEB_FORM_PROCESS_ERROR_0001 = "WEB_FORM_PROCESS_ERROR_0001";
    public final static String WEB_FORM_PROCESS_ERROR_0002 = "WEB_FORM_PROCESS_ERROR_0002";
    public final static String WEB_FORM_PROCESS_ERROR_0003 = "WEB_FORM_PROCESS_ERROR_0003";
    public final static String WEB_FORM_PROCESS_ERROR_0004 = "WEB_FORM_PROCESS_ERROR_0004";
}
