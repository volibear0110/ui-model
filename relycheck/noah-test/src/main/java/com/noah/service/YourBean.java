package com.noah.service;

import javax.annotation.PostConstruct;

/**
 * @author noah
 * 2023/8/7 13:41
 */
public class YourBean {

    private final MyBean myBean;

    public YourBean(MyBean myBean) {
        this.myBean = myBean;
    }

    @PostConstruct
    public void init() {
        System.out.println("your bean init");
    }
}
