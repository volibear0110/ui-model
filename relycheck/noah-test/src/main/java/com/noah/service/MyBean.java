package com.noah.service;

import javax.annotation.PostConstruct;

/**
 * @author noah
 * 2023/8/7 13:41
 */
public class MyBean {
    @PostConstruct
    public void init() {
        System.out.println("my bean init");
    }
}
