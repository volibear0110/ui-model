package com.noah;

import com.noah.config.NoahConfiguration;
import com.noah.service.MyBean;
import com.noah.service.YourBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * @author noah
 * 2023/8/7 10:40
 */
@SpringBootApplication
public class NoahApplication {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(NoahConfiguration.class);
        context.refresh();

        System.out.println(context.getBean(YourBean.class));

        System.out.println(context.getBean(MyBean.class));
        System.out.println(context.getBean(MyBean.class));

        System.out.println(context.getBean(NoahConfiguration.class));
        System.out.println(context.getBean(NoahConfiguration.class).myBean());
        Arrays.stream(context.getBeanDefinitionNames()).forEach(t-> System.out.println(t));

        context.close();

    }
}
