package com.noah.config;

import com.noah.service.MyBean;
import com.noah.service.YourBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author noah
 * 2023/8/7 10:41
 */
@Configuration(proxyBeanMethods = true)
public class NoahConfiguration {

    @Bean(name = "noahMyBean")
    public MyBean myBean() {
        return new MyBean();
    }

    @Bean(name = "noahYourBean")
    public YourBean yourBean(MyBean myBean) {
        // 直接调用  不会执行 bean 的生命周期
        return new YourBean(myBean);
    }

}
