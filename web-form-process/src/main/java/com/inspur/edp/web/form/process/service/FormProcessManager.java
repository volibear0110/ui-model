package com.inspur.edp.web.form.process.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.metadata.businesstype.api.MdBizTypeMappingService;
import com.inspur.edp.wf.bizprocess.entity.FormFormat;
import com.inspur.edp.wf.bizprocess.entity.UrlParameter;
import com.inspur.edp.wf.bizprocess.service.FormFormatRpcService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.client.RpcClassHolder;
import java.util.Arrays;
import java.util.List;

public class FormProcessManager {
    private static class LazyHolder {
        private static final FormProcessManager INSTANCE = new FormProcessManager();
    }

    private FormProcessManager() {}

    public static final FormProcessManager getInstance() {
        return LazyHolder.INSTANCE;
    }

    public void publishFormFormat(String formId, String formPath) {
        if (formPath == null || formPath.equals(""))
            throw new RuntimeException(");
                    String unifiedPath = formPath.replace('\\', '/');
        if (unifiedPath.startsWith("/"))
            unifiedPath = unifiedPath.substring(1);
        MetadataService metadataService = (MetadataService)SpringBeanUtils.getBean(MetadataService.class);
        List<GspMetadata> mdList = metadataService.getMetadataList(unifiedPath);
        GspMetadata md = mdList.stream().filter(item -> item.getHeader().getId().equals(formId)).findFirst().orElse(null);
        if (md == null)
            throw new RuntimeException("" + formId + "+ unifiedPath + ");
        MetadataHeader header = md.getHeader();
        GspProject project = metadataService.getGspProjectInfo(unifiedPath);
        String deploymentPath = project.getSuDeploymentPath();
        String projectName = project.getMetadataProjectName().toLowerCase();
        String url = "/" + deploymentPath + "/web/" + projectName + "/index.html#/" + header.getCode();
        FormFormat ff = new FormFormat();
        ff.setId(formId);
        ff.setCode(header.getCode());
        ff.setName(header.getName());
        ff.setUrlType("url");
        ff.setFormUrl(url);
        UrlParameter actionParam = new UrlParameter();
        actionParam.setCode("action");
        actionParam.setName(");
                actionParam.setValue("LoadAndView1");
        UrlParameter idParam = new UrlParameter();
        idParam.setCode("id");
        idParam.setName(");
                idParam.setValue("{\"expr\":\"DefaultFunction.GetContextParameter(\\\"dataId\\\")\",\"sexpr\":\"\"}\n");
        List<UrlParameter> list = Arrays.asList(new UrlParameter[] { actionParam, idParam });
        ff.setUrlParameters(list);
        ff.setFormatType("wf");
        String boId = project.getBizobjectID();
        ff.setBizCategory(getBizTypeId(boId));
        ff.setTerminal("PC");
        RpcClassHolder rpcHelper = (RpcClassHolder)SpringBeanUtils.getBean(RpcClassHolder.class);
        FormFormatRpcService service = (FormFormatRpcService)rpcHelper.getRpcClass(FormFormatRpcService.class);
        service.addFormFormat(ff);
    }

    private String getBizTypeId(String bizObjectId) {
        MdBizTypeMappingService service = (MdBizTypeMappingService)SpringBeanUtils.getBean(MdBizTypeMappingService.class);
        List<String> bizTypeIds = service.getBizTypeIdsByBoId(bizObjectId);
        return (bizTypeIds == null || bizTypeIds.size() == 0) ? "" : bizTypeIds.get(0);
    }
}
