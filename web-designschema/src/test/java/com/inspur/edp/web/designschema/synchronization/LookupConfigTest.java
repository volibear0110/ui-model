/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.designschema.synchronization.helplinkconfig.HelpLinkConfig;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LookupConfigTest {

    @Test
    public void testHelpLinkConfig() {
        LookupConfig lookupConfig = new LookupConfig();
        // 验证存在 默认的参数配置
        Assert.assertNotEquals(lookupConfig.getLinkConfig(), null);
        // 默认的参数值为false
        assertFalse(lookupConfig.getLinkConfig().isEnable());

    }

    @Test
    public void testLinkConfigSerializer() {
        String json = "{\n" +
                "        \"enable\": true,\n" +
                "        \"config\": [\n" +
                "            {\n" +
                "                \"linkedFieldsMapping\": {\n" +
                "                    \"sourceField\": \"name\",\n" +
                "                    \"sourceObjCode\": \"sczhuzi\",\n" +
                "                    \"targetField\": \"name\",\n" +
                "                    \"targetObjCode\": \"featurenoahbe\"\n" +
                "                },\n" +
                "                \"linkedType\": \"Normal\",\n" +
                "                \"sourceFieldInChild\": false,\n" +
                "                \"enableFilterWhenConditionEmpty\": true,\n" +
                "                \"targetFieldName\": \"name\",\n" +
                "                \"sourceFieldName\": \"name\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"LinkedFieldsMapping\": {\n" +
                "                    \"SourceField\": \"code\",\n" +
                "                    \"SourceObjCode\": \"sczhuzi\",\n" +
                "                    \"TargetField\": \"code\",\n" +
                "                    \"TargetObjCode\": \"featurenoahbe\"\n" +
                "                },\n" +
                "                \"LinkedType\": \"Normal\",\n" +
                "                \"SourceFieldInChild\": false,\n" +
                "                \"EnableFilterWhenConditionEmpty\": false,\n" +
                "                \"TargetFieldName\": \"code\",\n" +
                "                \"SourceFieldName\": \"code\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }";

        HelpLinkConfig linkConfig = SerializeUtility.getInstance().deserialize(json, HelpLinkConfig.class);

        assertTrue(linkConfig.isEnable());
        assertEquals(linkConfig.getConfig().size(), 2);


    }

}