package com.inspur.edp.web.designschema.elements;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author noah
 * 2023/7/31 19:35
 */
public class FieldTest {

    @Test
    public void testFieldSerializer() {
        SimpleField simpleField = new SimpleField();
        simpleField.setDefaultValue("simplefield");

        String serializedField = SerializeUtility.getInstance().serialize(simpleField);

        Field field = SerializeUtility.getInstance().deserialize(serializedField, Field.class);
        if (field instanceof SimpleField) {
            //
            System.out.println("simple field");
        }
    }
}