/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.type;

import com.inspur.edp.web.designschema.elements.Entity;
import com.inspur.edp.web.designschema.elements.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * 实体类型
 * @author  noah
 */
public class EntityType extends FieldType {

    public EntityType() {
        super("EntityType");
    }

    private String Primary;

    public final String getPrimary() {
        return Primary;
    }

    public final void setPrimary(String value) {
        Primary = value;
    }

    private List<Field> Fields = new ArrayList<>();

    public final List<Field> getFields() {
        return Fields;
    }

    public final void setFields(List<Field> value) {
        Fields = value;
    }

    private List<Entity> Entities = new ArrayList<>();

    public final List<Entity> getEntities() {
        return Entities;
    }

    public final void setEntities(List<Entity> value) {
        Entities = value;
    }
}
