/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization.helplinkconfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 帮助关联配置
 *
 * @author guozhiqi
 */
public class HelpLinkConfig implements Serializable {
    private static final long serialVersionUID = 1928282L;

    public HelpLinkConfig() {
        this.enable = false;
    }

    /**
     * 是否启用帮助联动
     */
    private boolean enable = false;

    /**
     * 帮助联动映射项  和设计器的具体属性名保持一致
     */
    private List<HelpLinkConfigItem> config;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public List<HelpLinkConfigItem> getConfig() {
        if (this.config == null) {
            this.config = new ArrayList<>();
        }
        return config;
    }

    public void setConfig(List<HelpLinkConfigItem> configItemList) {
        this.config = configItemList;
    }
}
