/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.type;

/**
 * @author noah
 */
public class BooleanType extends FieldType {
    public  BooleanType(){
        super("BooleanType");
    }
    private String Name = "Boolean";

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public void setName(String value) {
        Name = value;
    }

    private String DisplayName = "布尔";

    @Override
    public String getDisplayName() {
        return DisplayName;
    }

    @Override
    public void setDisplayName(String value) {
        DisplayName = value;
    }
}
