/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.generator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;

/**
 * 变量类型构造器
 * @author  noah
 */
public class VariableTypeBuilder {
    public VariableTypeBuilder() {
    }

    public final String build(IGspCommonField variableField) {
        return generateFieldType(variableField);
    }

    public final String generateFieldType(IGspCommonField variableField) {
        return generateSimpleFieldType(variableField);
    }

    private String generateSimpleFieldType(IGspCommonField variableField) {
        switch (variableField.getMDataType()) {
            case Boolean:
                return "Boolean";
            case Date:
            case DateTime:
                return "Date";
            case Decimal:
            case Integer:
                return "Decimal";
            case Text:
            case String:
            default:
                return "String";
        }
    }
}