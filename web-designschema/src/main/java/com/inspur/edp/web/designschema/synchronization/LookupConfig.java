/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization;

import com.inspur.edp.web.designschema.synchronization.helplinkconfig.HelpLinkConfig;

public class LookupConfig {
    private String LookupId;

    public final String getLookupId() {
        return LookupId;
    }

    public final void setLookupId(String value) {
        LookupId = value;
    }

    private String Condition;

    public final String getCondition() {
        return Condition;
    }

    public final void setCondition(String value) {
        Condition = value;
    }

    private String Uri;

    public final String getUri() {
        return Uri;
    }

    public final void setUri(String value) {
        Uri = value;
    }

    /**
     * 联动映射参数 如果此参数未配置 那么默认不启用该参数
     */
    private HelpLinkConfig linkConfig;

    /**
     * 获取对应的联动映射信息
     *
     * @return
     */
    public HelpLinkConfig getLinkConfig() {
        if (this.linkConfig == null) {
            this.linkConfig = new HelpLinkConfig();
        }
        return linkConfig;
    }

    public void setLinkConfig(HelpLinkConfig helpLinkConfig) {
        this.linkConfig = helpLinkConfig;
    }
}