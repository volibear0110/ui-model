/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.generator;

import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.web.designschema.elements.Field;

import java.util.ArrayList;

/**
 * 变量构造器
 * @author  noah
 */
public class VariableBuilder extends FieldBuilder {
    public VariableBuilder() {
    }

    public final ArrayList<Field> build(CommonVariableEntity variableEntity) {
        FieldBuilder fieldBuilder = new FieldBuilder();

        ArrayList<Field> variableList = new ArrayList<>();
        variableEntity.getContainElements().forEach(variableField ->
        {
            TypeBuildingContext variableContext = TypeBuildingContext.Create(variableField,null);
            Field field = fieldBuilder.build(variableContext, null, null);
            variableList.add(field);
        });
        return variableList;
    }
}
