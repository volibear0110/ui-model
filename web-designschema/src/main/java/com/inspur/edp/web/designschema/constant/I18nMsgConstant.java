package com.inspur.edp.web.designschema.constant;

public class I18nMsgConstant {
    public final static String WEB_DESIGN_SCHEMA_MSG_0001 = "WEB_DESIGN_SCHEMA_MSG_0001";
    public final static String WEB_DESIGN_SCHEMA_MSG_0002 = "WEB_DESIGN_SCHEMA_MSG_0002";
    public final static String WEB_DESIGN_SCHEMA_MSG_0003 = "WEB_DESIGN_SCHEMA_MSG_0003";
    public final static String WEB_DESIGN_SCHEMA_MSG_0004 = "WEB_DESIGN_SCHEMA_MSG_0004";
    public final static String WEB_DESIGN_SCHEMA_MSG_0005 = "WEB_DESIGN_SCHEMA_MSG_0005";

}
