/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization;

import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;

/**
 * 元数据事件监听
 *
 * @author noah
 */
public class DesignSchemaChangeListener implements MetadataEventListener {

    @Override
    public void fireMetadataSavingEvent(MetadataEventArgs e) {
        if (e != null && e.getMetadata() != null) {
            FormMetadataUpdate.update(e.getMetadata());
        }
    }

    @Override
    public void fireMetadataSavedEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {

    }
}
