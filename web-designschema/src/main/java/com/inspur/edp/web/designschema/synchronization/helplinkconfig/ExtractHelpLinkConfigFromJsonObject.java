/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization.helplinkconfig;

import com.alibaba.fastjson.JSONObject;

/**
 * 从JsonObject中提取对应的帮助联动配置参数
 *
 * @author guozhiqi
 */
public class ExtractHelpLinkConfigFromJsonObject {
    private static final String linkConfigAttributeName = "linkConfig";

    public static HelpLinkConfig extract(JSONObject jsonObject) {

        if (jsonObject == null || !jsonObject.containsKey(linkConfigAttributeName)) {
            return null;
        }
        HelpLinkConfig helpLinkConfig = jsonObject.getObject(linkConfigAttributeName, HelpLinkConfig.class);

        return helpLinkConfig;
    }
}
