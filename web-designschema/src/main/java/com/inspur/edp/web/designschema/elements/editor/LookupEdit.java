/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.editor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cdp.common.utils.json.JsonUtil;

import java.io.IOException;
import java.util.HashMap;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LookupEdit extends FieldEditor {
    private String HelpId;

    public final String getHelpId() {
        return HelpId;
    }

    public final void setHelpId(String value) {
        HelpId = value;
    }

    private String Uri;

    public final String getUri() {
        return Uri;
    }

    public final void setUri(String value) {
        Uri = value;
    }

    private String TextField;

    public final String getTextField() {
        return TextField;
    }

    public final void setTextField(String value) {
        TextField = value;
    }

    private String ValueField;

    public final String getValueField() {
        return ValueField;
    }

    public final void setValueField(String value) {
        ValueField = value;
    }

    private String DisplayType;

    public final String getDisplayType() {
        return DisplayType;
    }

    public final void setDisplayType(String value) {
        DisplayType = value;
    }

    private DataSource DataSource;

    public final DataSource getDataSource() {
        return DataSource;
    }

    public final void setDataSource(DataSource value) {
        DataSource = value;
    }

    private HashMap<String, String> Map;

    @JsonSerialize(using = MapFieldsSerializer.class)
    @JsonProperty(value = "mapFields")
    public final HashMap<String, String> getMap() {
        return Map;
    }

    public final void setMap(HashMap<String, String> value) {
        Map = value;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private JsonNode options;

    public final JsonNode getOptions() {
        return options;
    }

    public final void setOptions(JsonNode options) {
        this.options = options;
    }

    public LookupEdit() {
        super("LookupEdit");
    }
}

class MapFieldsSerializer extends JsonSerializer<HashMap<String, String>> {

    @Override
    public void serialize(HashMap<String, String> map, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String mapString = JsonUtil.toJson(map);
        jsonGenerator.writeString(mapString);
    }
}
