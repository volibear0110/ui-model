/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.generator;

import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.designschema.constant.I18nExceptionConstant;
import com.inspur.edp.web.designschema.elements.Entity;
import com.inspur.edp.web.designschema.elements.Field;
import com.inspur.edp.web.designschema.elements.type.EntityType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 实体builder
 * @author  noah
 */
public class EntityBuilder {

    public final Entity Build(IGspCommonObject commonObject) {
        return Build(commonObject, null);
    }
    public final Entity Build(IGspCommonObject commonObject, String scene) {
        return Build(commonObject, scene, false);
    }

    public final Entity Build(IGspCommonObject commonObject, String scene, boolean isRuntime) {
        Entity tempVar = new Entity();
        tempVar.setId(commonObject.getID());
        tempVar.setCode(commonObject.getCode());
        tempVar.setName(commonObject.getName());
        tempVar.setLabel(StringUtility.toCamelCase(commonObject.getCode()) + "s");
        tempVar.setType(this.GenerateEntityType(commonObject, scene, isRuntime));
        return tempVar;
    }


    private EntityType GenerateEntityType(IGspCommonObject viewObject, boolean isRuntime) {
        return GenerateEntityType(viewObject, null, isRuntime);
    }

    private EntityType GenerateEntityType(IGspCommonObject viewObject, String scene, boolean isRuntime) {
        this.VerifyViewObject(viewObject);

        FieldBuilder fieldBuilder = new FieldBuilder();
        EntityBuilder entityBuilder = new EntityBuilder();

        List<Field> fields = viewObject.getContainElements()
                .stream()
                .map((e) -> {
                    if (e instanceof GspViewModelElement) {
                        GspViewModelElement gspViewModelElement = (GspViewModelElement) e;
                        Field f = fieldBuilder.build(gspViewModelElement, null, scene, isRuntime);
                        return f;
                    } else if (e instanceof SimpleDataTypeDef) {
                        SimpleDataTypeDef simpleDataTypeDef = (SimpleDataTypeDef) e;
                        return fieldBuilder.build(simpleDataTypeDef, null, scene);
                    } else {
                        return fieldBuilder.build(e, null, scene);
                    }

                }).collect(Collectors.toList());

        //N转J N版代码 LINQ
        List<Entity> entities = viewObject.getContainChildObjects()
                .stream()
                .map(childObject -> {
                    Entity entity = entityBuilder.Build(childObject, scene, isRuntime);
                    return entity;
                }).collect(Collectors.toList());

        EntityType tempVar = new EntityType();
        tempVar.setName(viewObject.getCode());
        tempVar.setDisplayName(viewObject.getName());
        tempVar.setPrimary(StringUtility.toCamelCase(viewObject.getIDElement().getLabelID()));
        tempVar.setFields(fields);
        tempVar.setEntities(entities);
        return tempVar;
    }

    private void VerifyViewObject(IGspCommonObject viewObject) {
        if (viewObject.getIDElement() == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_DESIGN_SCHEMA_ERROR_0001, new String[]{ viewObject.getBelongModelID(), viewObject.getName()});
        }
    }
}
