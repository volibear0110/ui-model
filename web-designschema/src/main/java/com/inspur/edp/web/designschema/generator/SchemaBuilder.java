/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.generator;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.web.designschema.elements.Schema;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * @author guozhiqi
 */
public class SchemaBuilder {
    public final Schema build(GspViewModel model) {
        return this.buildWithScene(model, "");
    }

    public final Schema buildWithScene(GspViewModel model, String scene) {
        return buildWithScene(model, scene, false);
    }

    public final Schema buildWithScene(GspViewModel model, String scene, boolean isRuntime) {
        EntityBuilder entityBuilder = new EntityBuilder();
        VariableBuilder variableBuilder = new VariableBuilder();
        Schema tempVar = new Schema();
        tempVar.setId(model.getID());
        tempVar.setCode(model.getCode());
        tempVar.setName(model.getName());
        tempVar.setSourceType("vo");
        tempVar.setEntities(new ArrayList<>(Collections.singletonList(entityBuilder.Build(model.getMainObject(), scene, isRuntime))));
        tempVar.setVariables(variableBuilder.build(model.getVariables()));
        tempVar.ExtendProperties = new HashMap<>();
        tempVar.ExtendProperties.put("enableStdTimeFormat", model.getEnableStdTimeFormat());
        return tempVar;
    }
}
