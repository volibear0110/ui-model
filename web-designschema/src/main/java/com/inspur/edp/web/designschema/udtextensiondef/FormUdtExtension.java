/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.udtextensiondef;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.udt.designtime.api.extension.BaseUdtExtension;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;

/**
 * @author liyz
 * @date 2021/7/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FormUdtExtension extends BaseUdtExtension {

    private String controlBindingFieldId;
    private String controlType;
    private LookupConfig lookupConfig;

    @Data
    public static class LookupConfig {
        private String helpId;
        private String idField;
        private String valueField;
        private String textField;
        private String displayType;
        private String displayName;
        private HashMap<String, String> mapFields;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private JsonNode options;
    }
}
