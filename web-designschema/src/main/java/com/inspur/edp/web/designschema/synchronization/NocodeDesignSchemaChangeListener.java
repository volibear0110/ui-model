/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.NoCodeService;
import com.inspur.edp.lcm.metadata.spi.event.NoCodeServiceEventListener;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeAfterDeleteArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeAfterSaveArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeBeforeDeleteArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeBeforeSaveArgs;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * @author liyz
 * @date 2021-9-5
 */
public class NocodeDesignSchemaChangeListener extends BaseDesignSchemaChangeHandler implements NoCodeServiceEventListener {
    @Override
    public void fireMetadataBeforeSaveEvent(NoCodeBeforeSaveArgs e) {
        FormMetadataContent content = (FormMetadataContent) ((e.getMetadata().getContent() instanceof FormMetadataContent) ? e.getMetadata().getContent() : null);
        if (content == null) {
            return;
        }
        this.handleFormSaving(content);
    }

    @Override
    public void fireMetadataAfterSaveEvent(NoCodeAfterSaveArgs noCodeAfterSaveArgs) {

    }

    @Override
    public void fireMetadataBeforeDeleteEvent(NoCodeBeforeDeleteArgs noCodeBeforeDeleteArgs) {

    }

    @Override
    public void fireMetadataAfterDeleteEvent(NoCodeAfterDeleteArgs noCodeAfterDeleteArgs) {

    }

    @Override
    protected GspMetadata getMetadata(String id) {
        CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);
        return customizationService.getMetadata(id);
    }

    @Override
    protected void saveMetadata(GspMetadata metadata, Object... args) {
        // 执行元数据保存前 action
        this.beforeSaveMetadataAction(metadata);

        NoCodeService nocodeService = SpringBeanUtils.getBean(NoCodeService.class);
        nocodeService.saveMetadata(metadata);
    }
}
