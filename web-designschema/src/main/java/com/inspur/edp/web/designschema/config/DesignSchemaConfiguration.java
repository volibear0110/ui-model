/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.config;

import com.inspur.edp.web.designschema.webservice.DesignSchemaWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/16
 */
@Configuration("com.inspur.edp.web.designschema.config.DesignSchemaConfiguration")
public class DesignSchemaConfiguration {
    @Bean()
    public RESTEndpoint designSchemaWebServiceEndpoint(){
        return new RESTEndpoint("/dev/main/v1.0/designschema", new DesignSchemaWebServiceImpl());
    }
}


