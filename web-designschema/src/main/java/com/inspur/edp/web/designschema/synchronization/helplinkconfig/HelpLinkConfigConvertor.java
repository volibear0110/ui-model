/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization.helplinkconfig;

import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfig;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfigCollection;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedFieldsMapping;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedType;

import java.util.List;
import java.util.stream.Collectors;

public class HelpLinkConfigConvertor {
    public static LinkedConfigCollection convert(HelpLinkConfig linkConfig) {
        LinkedConfigCollection linkedConfigCollection = new LinkedConfigCollection();
        // 如果未启用 那么返回空集合
        if (linkConfig == null || !linkConfig.isEnable()) {
            return linkedConfigCollection;
        }

        List<LinkedConfig> linkedConfigList = linkConfig.getConfig().stream().map(LinkedConfigItemConvertor::convert).collect(Collectors.toList());
        linkedConfigCollection.addAll(linkedConfigList);
        return linkedConfigCollection;
    }

    /**
     * 帮助配置项转换
     */
    private static class LinkedConfigItemConvertor {
        public static LinkedConfig convert(HelpLinkConfigItem helpLinkConfigItem) {
            LinkedConfig linkedConfig = new LinkedConfig();
            linkedConfig.setLinkedType(LinkedType.valueOf(helpLinkConfigItem.getLinkedType()));
            linkedConfig.setEnableFilterWhenConditionEmpty(helpLinkConfigItem.isEnableFilterWhenConditionEmpty());
            linkedConfig.setSourceFieldInChild(helpLinkConfigItem.isSourceFieldInChild());
            linkedConfig.setLinkedFieldsMapping(LinkedMappingConvertor.convert(helpLinkConfigItem.getLinkedFieldsMapping()));
            return linkedConfig;
        }
    }

    public static class LinkedMappingConvertor {
        public static LinkedFieldsMapping convert(HelpLinkedFieldsMapping helpLinkedFieldsMapping) {
            LinkedFieldsMapping linkedFieldsMapping = new LinkedFieldsMapping();
            linkedFieldsMapping.setSourceField(helpLinkedFieldsMapping.getSourceField());
            linkedFieldsMapping.setSourceObjCode(helpLinkedFieldsMapping.getSourceObjCode());
            linkedFieldsMapping.setTargetField(helpLinkedFieldsMapping.getTargetField());
            linkedFieldsMapping.setTargetObjCode(helpLinkedFieldsMapping.getTargetObjCode());
            return linkedFieldsMapping;
        }
    }
}
