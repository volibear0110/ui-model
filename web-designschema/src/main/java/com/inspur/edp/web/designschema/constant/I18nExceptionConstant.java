package com.inspur.edp.web.designschema.constant;

public class I18nExceptionConstant {
    public final static String WEB_DESIGN_SCHEMA_ERROR_0001 = "WEB_DESIGN_SCHEMA_ERROR_0001";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0002 = "WEB_DESIGN_SCHEMA_ERROR_0002";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0003 = "WEB_DESIGN_SCHEMA_ERROR_0003";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0004 = "WEB_DESIGN_SCHEMA_ERROR_0004";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0005 = "WEB_DESIGN_SCHEMA_ERROR_0005";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0006 = "WEB_DESIGN_SCHEMA_ERROR_0006";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0007 = "WEB_DESIGN_SCHEMA_ERROR_0007";
    public final static String WEB_DESIGN_SCHEMA_ERROR_0008 = "WEB_DESIGN_SCHEMA_ERROR_0008";
}
