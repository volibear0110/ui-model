/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.utils;

import com.fasterxml.jackson.databind.node.TextNode;

/**
 * @author noah
 */
public final class StringUtils {


    /**
     * 根据具体类型获取对应的参数值
     * @param source
     * @return
     */
    public static String getSpecialValue(Object source) {
        if(source==null){
            return "";
        }
        if (source instanceof String) {
            return (String) source;
        }
        if (source instanceof TextNode) {
            return ((TextNode) source).asText();
        }

        return source.toString();
    }
}
