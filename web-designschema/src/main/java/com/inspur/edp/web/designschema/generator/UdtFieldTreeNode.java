/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.generator;

import com.inspur.edp.web.designschema.elements.Field;
import lombok.Getter;

import java.util.HashMap;

/**
 * @author liyz
 * @date 2021/8/7
 */
public class UdtFieldTreeNode {
    @Getter
    private final Field field;

    @Getter
    private UdtFieldTreeNode parent;

    private final HashMap<String, UdtFieldTreeNode> children = new HashMap<>();

    public UdtFieldTreeNode(Field field) {
        this.field = field;
    }

    public UdtFieldTreeNode(Field field, UdtFieldTreeNode parent) {
        this.field = field;
        this.setParent(parent);
    }

    public void setParent(UdtFieldTreeNode parent) {
        if (this.parent == parent) {
            return;
        }
        if (this.parent != null) {
            // todo: remove from parent's children node
        }
        this.parent = parent;
        if (parent != null) {
            parent.addChild(this);
        }
    }

    public UdtFieldTreeNode getChild(String id) {
        return this.children.get(id);
    }

    private void addChild(UdtFieldTreeNode child) {
        String id = child.getField().getOriginalId();
        children.put(id, child);
    }
}
