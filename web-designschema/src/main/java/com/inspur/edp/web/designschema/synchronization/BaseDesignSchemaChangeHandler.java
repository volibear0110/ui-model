/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.designschema.constant.I18nExceptionConstant;
import com.inspur.edp.web.designschema.elements.Schema;
import com.inspur.edp.web.designschema.synchronization.helplinkconfig.VoMetadataHelpLinkConfigModifierBeforeSaving;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

import java.util.List;

/**
 * @author liyz
 * @date 2021-9-5
 */
public abstract class BaseDesignSchemaChangeHandler {
    public void handleFormSaving(FormMetadataContent content) {
        if (content == null) {
            return;
        }
        String formContent = SerializeUtility.getInstance().serialize(content.getContents());
        JsonNode formObject = null;

        String formString = (formContent instanceof String) ? formContent : null;
        formObject = SerializeUtility.getInstance().toJsonNode(formString);

        JsonNode schemas = null;
        JsonNode jsonNodeModules = formObject.get("module");
        if (jsonNodeModules != null) {
            schemas = jsonNodeModules.get("schemas");
        }

        if (schemas != null) {
            String schemasString = schemas.toString();

            List<Schema> schemaList = SerializeUtility.getInstance().deserialize(schemasString, new TypeReference<List<Schema>>() {
            });
            Synchronizer synchronizer = new Synchronizer();
            for (Schema schema : schemaList) {
                if ("vo".equals(schema.getSourceType())) {
                    GspMetadata metadata = this.getMetadata(schema.getId());
                    GspViewModel viewObject = (GspViewModel) ((metadata.getContent() instanceof GspViewModel) ? metadata.getContent() : null);
                    metadata.setContent(synchronizer.synchronize(formObject, schema, viewObject));
                    this.saveMetadata(metadata);
                }
            }
        } else {
            throw new WebCustomException(I18nExceptionConstant.WEB_DESIGN_SCHEMA_ERROR_0008);
        }
    }

    /**
     * 元数据保存前执行action
     * @param gspMetadata
     */
    protected void beforeSaveMetadataAction(GspMetadata gspMetadata) {
        // 此处执行的是VO 帮助联动配置信息调整
        VoMetadataHelpLinkConfigModifierBeforeSaving.modify(gspMetadata);
    }

    /**
     * 获取元数据
     *
     * @param id 元数据id
     * @return 元数据
     */
    protected abstract GspMetadata getMetadata(String id);

    /**
     * 保存元数据的抽象方法
     *
     * @param metadata 要保存的元数据
     * @param args     额外的参数
     */
    protected abstract void saveMetadata(GspMetadata metadata, Object... args);
}
