/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.type;

/**
 * 大数类型
 * @author noah
 */
public class BigNumericType extends FieldType {

    public  BigNumericType(){
        super("BigNumericType");
    }

    private String Name = "BigNumber";

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public void setName(String value) {
        Name = value;
    }

    private String DisplayName = "大数字";

    @Override
    public String getDisplayName() {
        return DisplayName;
    }

    @Override
    public void setDisplayName(String value) {
        DisplayName = value;
    }

    private int Length;

    public final int getLength() {
        return Length;
    }

    public final void setLength(int value) {
        Length = value;
    }

    private int Precision;

    public final int getPrecision() {
        return Precision;
    }

    public final void setPrecision(int value) {
        Precision = value;
    }
}
