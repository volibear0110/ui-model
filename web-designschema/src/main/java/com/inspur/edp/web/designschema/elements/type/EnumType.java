/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.type;

import java.util.ArrayList;

/**
 * 枚举类型
 * @author  noah
 */
public class EnumType extends FieldType {

    public EnumType() {
        super("EnumType");
    }

    private String Name = "Enum";

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public void setName(String value) {
        Name = value;
    }

    private String DisplayName = "枚举";

    @Override
    public String getDisplayName() {
        return DisplayName;
    }

    @Override
    public void setDisplayName(String value) {
        DisplayName = value;
    }

    private FieldType ValueType;

    public final FieldType getValueType() {
        return ValueType;
    }

    public final void setValueType(FieldType value) {
        ValueType = value;
    }

    //public Dictionary<object, string> EnumValues { get; set; } = new Dictionary<object, string>();

    private ArrayList<EnumItem> EnumValues = new ArrayList<>();

    public final ArrayList<EnumItem> getEnumValues() {
        return EnumValues;
    }

    public final void setEnumValues(ArrayList<EnumItem> value) {
        EnumValues = value;
    }

}
