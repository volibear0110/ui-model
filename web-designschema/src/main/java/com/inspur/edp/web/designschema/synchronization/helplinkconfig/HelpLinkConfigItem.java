/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization.helplinkconfig;

import lombok.Data;

import java.io.Serializable;

/**
 * 帮助联动具体的配置项
 *
 * @author guozhiqi
 */
@Data
public class HelpLinkConfigItem implements Serializable {
    private static final long serialVersionUID = 51455444L;
    private HelpLinkedFieldsMapping linkedFieldsMapping;

    private String linkedType;
    private boolean sourceFieldInChild;

    private boolean enableFilterWhenConditionEmpty;
    private String targetFieldName;
    private String sourceFieldName;


}
