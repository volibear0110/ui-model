/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements.editor;

/**
 * datasource 数据源实体配置
 * @author  noah
 */
public class DataSource {
    private String Uri;

    public final String getUri() {
        return Uri;
    }

    public final void setUri(String value) {
        Uri = value;
    }

    private String DisplayName;

    public final String getDisplayName() {
        return DisplayName;
    }

    public final void setDisplayName(String value) {
        DisplayName = value;
    }

    private String IdField;

    public final String getIdField() {
        return IdField;
    }

    public final void setIdField(String value) {
        IdField = value;
    }

    private String Type;

    public final String getType() {
        return Type;
    }

    public final void setType(String value) {
        Type = value;
    }
}