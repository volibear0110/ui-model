/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.synchronization.helplinkconfig;

import com.inspur.edp.formserver.viewmodel.linkedconfig.service.LinkedConfigService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * VO 元数据保存前 帮助映射或帮助联动映射信息调整
 * 此调整由VO 服务进行
 *
 * @author guozhiqi
 */
public class VoMetadataHelpLinkConfigModifierBeforeSaving {
    /**
     * 仅处理VO元数据
     *
     * @param gspMetadata
     */
    public static void modify(GspMetadata gspMetadata) {
        if (gspMetadata == null || SpringBeanUtils.getBean(LinkedConfigService.class) == null) {
            return;
        }
        if (!MetadataTypeEnum.ViewModel.isCurrentMetadataType(gspMetadata.getHeader().getType())) {
            return;
        }
        LinkedConfigService linkedConfigService = SpringBeanUtils.getBean(LinkedConfigService.class);
        linkedConfigService.handleLinkedConfig(gspMetadata);
    }
}
