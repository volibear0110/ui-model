/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.udtextensiondef;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.udt.designtime.api.extension.BaseUdtExtensionDeserializer;
import com.inspur.edp.web.common.logger.WebLogger;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author liyz
 * @date 2021/7/27
 */
public class FormUdtExtensionDeserializer extends BaseUdtExtensionDeserializer<FormUdtExtension> {
    @Override
    public FormUdtExtension deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        FormUdtExtension formUdtExtension = new FormUdtExtension();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case "controlBindingFieldId":
                    formUdtExtension.setControlBindingFieldId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "controlType":
                    formUdtExtension.setControlType(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "lookupConfig":
                    readLookupConfig(formUdtExtension, jsonParser, deserializationContext);
                    break;
                default:
                    SerializerUtils.readNullObject(jsonParser);
                    break;
            }
        }

        SerializerUtils.readEndObject(jsonParser);
        return formUdtExtension;
    }

    private void readLookupConfig(FormUdtExtension extension, JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);

        FormUdtExtension.LookupConfig lookupConfig = new FormUdtExtension.LookupConfig();
        extension.setLookupConfig(lookupConfig);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case "helpId":
                    lookupConfig.setHelpId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "idField":
                    lookupConfig.setIdField(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "valueField":
                    lookupConfig.setValueField(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "textField":
                    lookupConfig.setTextField(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "displayType":
                    lookupConfig.setDisplayType(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "displayName":
                    lookupConfig.setDisplayName(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case "mapFields":
                    readMapFields(lookupConfig, jsonParser, deserializationContext);
                    break;
                case "options":
                    // 大坑，readPropertyValue_Object没有让游标往下走，当前token为空。
                    // lookupConfig.setOptions(SerializerUtils.readPropertyValue_Object(JsonNode.class, jsonParser));
                    try {
                        lookupConfig.setOptions((new ObjectMapper()).readValue(jsonParser, JsonNode.class));
                        jsonParser.nextToken(); // 游标往下走
                    } catch (IOException e) {
                        WebLogger.Instance.error(e);
                    }
                    // readOptions(lookupConfig, jsonParser, deserializationContext);
                    break;
                default:
                    SerializerUtils.readNullObject(jsonParser);
                    break;
            }
        }

        SerializerUtils.readEndObject(jsonParser);
    }

    private void readMapFields(FormUdtExtension.LookupConfig lookupConfig, JsonParser jsonParser, DeserializationContext deserializationContext) {
        HashMap<String, String> mapFields = new HashMap<>();
        lookupConfig.setMapFields(mapFields);

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String mapKey = SerializerUtils.readPropertyName(jsonParser);
            String mapValue = SerializerUtils.readPropertyValue_String(jsonParser);
            mapFields.put(mapKey, mapValue);
        }

        SerializerUtils.readEndObject(jsonParser);
    }
}
