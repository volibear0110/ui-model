/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.designschema.elements;

import com.inspur.edp.web.designschema.elements.editor.DefaultEditor;
import com.inspur.edp.web.designschema.elements.editor.FieldEditor;

/**
 * 简单类型字段，如：字符串、数字、日期、布尔类型等。
 */
public class SimpleField extends Field {

    public  SimpleField(){
        super("SimpleField");
    }

    private String DefaultValue;

    public final String getDefaultValue() {
        return DefaultValue;
    }

    public final void setDefaultValue(String value) {
        DefaultValue = value;
    }


    private boolean Require;

    public final boolean getRequire() {
        return Require;
    }

    public final void setRequire(boolean value) {
        Require = value;
    }


    private boolean Readonly;

    public final boolean getReadonly() {
        return Readonly;
    }

    public final void setReadonly(boolean value) {
        Readonly = value;
    }


    private FieldEditor Editor = new DefaultEditor();

    public final FieldEditor getEditor() {
        return Editor;
    }

    public final void setEditor(FieldEditor value) {
        Editor = value;
    }


    private boolean MultiLanguage = false;

    public final boolean getMultiLanguage() {
        return MultiLanguage;
    }

    public final void setMultiLanguage(boolean value) {
        MultiLanguage = value;
    }
}
