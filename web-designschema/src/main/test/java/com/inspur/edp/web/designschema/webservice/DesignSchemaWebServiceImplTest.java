package com.inspur.edp.web.designschema.webservice;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.designschema.elements.Schema;
import com.inspur.edp.web.designschema.generator.SchemaBuilder;
import com.inspur.edp.web.formmetadata.metadata.module.Module;
import org.junit.jupiter.api.Test;

class DesignSchemaWebServiceImplTest {

    @Test
    void createDesignSchema() {
        String fileContent = FileUtility.readAsString("D:\\InspurCode\\N转J之后代码\\web\\web-designschema\\src\\main\\test\\java\\com\\inspur\\edp\\web\\designschema\\webservice\\mmm.json");

       Module mm= SerializeUtility.getInstance().deserialize(fileContent, Module.class);
       String sss=SerializeUtility.getInstance().serialize(mm.getSchemas().get(0));
        GspViewModel vm = SerializeUtility.getInstance().deserialize(sss, GspViewModel.class);

        SchemaBuilder schemaBuilder = new SchemaBuilder();

        Schema schema = schemaBuilder.buildWithScene(vm, "");
    }
}