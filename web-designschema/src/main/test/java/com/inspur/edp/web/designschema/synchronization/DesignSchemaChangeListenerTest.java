package java.com.inspur.edp.web.designschema.synchronization;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.designschema.synchronization.DesignSchemaChangeListener;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import org.junit.jupiter.api.Test;

class DesignSchemaChangeListenerTest {

    @Test
    void fireMetadataSavingEvent() {
        String json = FileUtility.readAsString("D:\\InspurCode\\N转J之后代码\\tag2103\\web\\web-designschema\\src\\main\\test\\java\\com\\inspur\\edp\\web\\designschema\\synchronization\\help.json");
        DesignSchemaChangeListener changeListener = new DesignSchemaChangeListener();
        MetadataEventArgs eventArgs = new MetadataEventArgs();
        GspMetadata metadata = new GspMetadata();
        FormMetadataContent metadataContent = new FormMetadataContent();
        metadataContent.setContents(SerializeUtility.getInstance().readTree(json));

        metadata.setContent(metadataContent);
        eventArgs.setMetadata(metadata);

        changeListener.fireMetadataSavingEvent(eventArgs);
    }
}
