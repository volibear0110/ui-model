/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.config;

import com.inspur.edp.web.approvalformat.api.rpcservice.IApprovalFormatRpcService;
import com.inspur.edp.web.approvalformat.api.service.*;
import com.inspur.edp.web.approvalformat.api.webservice.ApprovalFormMetadataWebService;
import com.inspur.edp.web.approvalformat.api.webservice.ApprovalFormatWebService;
import com.inspur.edp.web.approvalformat.api.webservice.BusinessEntityWebService;
import com.inspur.edp.web.approvalformat.api.webservice.DimensionWebService;
import com.inspur.edp.web.approvalformat.core.domain.manager.ApprovalFormatManager;
import com.inspur.edp.web.approvalformat.core.domain.repository.ApprovalFormatRepository;
import com.inspur.edp.web.approvalformat.core.rpcservice.ApprovalFormatRpcServiceImpl;
import com.inspur.edp.web.approvalformat.core.service.*;
import com.inspur.edp.web.approvalformat.core.webservice.ApprovalFormMetadataWebServiceImpl;
import com.inspur.edp.web.approvalformat.core.webservice.ApprovalFormatWebServiceImpl;
import com.inspur.edp.web.approvalformat.core.webservice.BusinessEntityWebServiceImpl;
import com.inspur.edp.web.approvalformat.core.webservice.DimensionWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * 审批格式配置
 * @author Xu‘fa Wang
 * @date 2020/5/14 16:18
 */
@Configuration(proxyBeanMethods = false)
@EnableJpaRepositories("com.inspur.edp.web.approvalformat.core.domain.repository")
@EntityScan({"com.inspur.edp.web.approvalformat.core.domain.entity"})
public class ApprovalFormatConfiguration {
    /**
     * TODO: 迁移到constant中
     */
    private final String KEY_APPLICATION_NAME = "runtime";
    private final String SERVICE_UNIT_NAME = "bcc";
    private final String BASE_PATH_NAME = "approval";
    private final String VERSION = "v1.0";
    private final String SEPARATOR = "/";

    /**
     * 审批格式 Web Service 基础 Uri
     * uri实例如下：
     * "/runtime/bcc/v1.0"
     */
    private final String APPROVAL_FORMAT_WEB_SERVICE_BASE_URI = SEPARATOR
            + KEY_APPLICATION_NAME + SEPARATOR
            + SERVICE_UNIT_NAME + SEPARATOR
            + VERSION + SEPARATOR
            + BASE_PATH_NAME;

    @Bean
    public ApprovalFormatManager approvalFormatManager(ApprovalFormatRepository repository) {
        return new ApprovalFormatManager(repository);
    }

    @Bean
    public IRuntimeMetadataService runtimeMetadataService() {
        return new RuntimeMetadataServiceImpl();
    }

    @Bean
    public DimensionService dimensionService() {
        return new DimensionServiceImpl();
    }

    @Bean
    public DimensionWebService dimensionWebService(DimensionService dimensionService) {
        return new DimensionWebServiceImpl(dimensionService);
    }

    @Bean
    public BusinessEntityService businessEntityService() {
        return new BusinessEntityServiceImpl();
    }

    @Bean
    public BusinessEntityWebService businessEntityWebService(BusinessEntityService businessEntityService) {
        return new BusinessEntityWebServiceImpl(businessEntityService);
    }

    @Bean
    public ApprovalFormMetadataService approvalFormMetadataService() {
        return new ApprovalFormMetadataServiceImpl();
    }

    @Bean
    public ApprovalFormMetadataWebService approvalFormMetadataWebService(ApprovalFormMetadataService approvalFormMetadataService) {
        return new ApprovalFormMetadataWebServiceImpl(approvalFormMetadataService);
    }

    @Bean
    public ApprovalFormatService approvalFormatService(DimensionService dimensionService, ApprovalFormatManager approvalFormatManager) {
        return new ApprovalFormatServiceImpl(dimensionService, approvalFormatManager);
    }

    @Bean
    public ApprovalFormatWebService approvalFormatWebService(ApprovalFormatService approvalFormatService) {
        return new ApprovalFormatWebServiceImpl(approvalFormatService);
    }

    /**
     * 构造RESTEndpoint实例，用来组装WebService，以发布 Web API
     * @param dimensionWebService 维度服务
     * @param approvalFormatWebService 审批格式服务
     * @return RESTEndpoint实例
     */
    @Bean
    public RESTEndpoint dimensionWebServiceEndPoint(ApprovalFormatWebService approvalFormatWebService,
                                                    DimensionWebService dimensionWebService,
                                                    BusinessEntityWebService businessEntityWebService,
                                                    ApprovalFormMetadataWebService approvalFormMetadataWebService) {
        return new RESTEndpoint(APPROVAL_FORMAT_WEB_SERVICE_BASE_URI,
                approvalFormatWebService,
                dimensionWebService,
                businessEntityWebService,
                approvalFormMetadataWebService);
    }
    /**
     * RPC服务的bean
     * @return IApprovalFormatRpcService 实例化对象
     */
    @Bean
    public IApprovalFormatRpcService approvalFormatRpcService(ApprovalFormatService approvalFormatService) {
        return new ApprovalFormatRpcServiceImpl(approvalFormatService);
    }
}