/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.util;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.licservice.api.manager.StandardControlFactory;


public class ApprovalFormatPermission {

    private final StandardControlFactory standardControlFactory;
    private final String moduleName = "CTMD";
    private final String moduleDisplayName = "动态建模";

    public ApprovalFormatPermission() {
        this.standardControlFactory = SpringBeanUtils.getBean(StandardControlFactory.class);
    }

    /**
     * 动态建模模块权限控制
     */
    public void checkModulePermission() {
        standardControlFactory.licStandardControl(this.moduleName);
    }
}
