/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.domain.repository;

import com.inspur.edp.web.approvalformat.core.domain.entity.ApprovalFormatDO;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 审批格式repo
 * @author Xu‘fa Wang
 * @date 2020/5/16 20:23
 */
public interface ApprovalFormatRepository extends DataRepository<ApprovalFormatDO, String> {
    /**
     * 基于beId获取所有的审批格式记录
     * @param beId 业务实体id
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE beId = ?1 AND (dim1 = '' OR dim1 is null) AND (dim2 = '' OR dim2 is null)")
    List<ApprovalFormatDO> findAllByBeIdAndDim1IsNullAndDim2IsNull(String beId);

    /**
     * 基于beId获取所有的审批格式记录
     * @param beId 业务实体id
     * @param dim1 审批格式第一维度
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE beId = ?1 AND dim1 = ?2 AND (dim2 = '' OR dim2 is null)")
    List<ApprovalFormatDO> findAllByBeIdAndDim1AndDim2IsNull(String beId, String dim1);

    /**
     * 基于beId获取所有的审批格式记录
     * @param beId 业务实体id
     * @param dim1 审批格式第一维度
     * @param dim2 审批格式第二维度
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE beId = ?1 AND dim1 = ?2 AND dim2 = ?3")
    List<ApprovalFormatDO> findAllByBeIdAndDim1AndDim2(String beId, String dim1, String dim2);

    /**
     * 基于billCategoryId获取所有的审批格式记录
     * @param billCategoryId 单据种类id
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE billCategoryId = ?1")
    List<ApprovalFormatDO> findAllByBillCategoryId(String billCategoryId);

    /**
     * 基于billCategoryId获取所有的审批格式记录
     * @param billCategoryId 单据种类id
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE billCategoryId = ?1 ORDER BY dim1 ASC, dim2 ASC")
    List<ApprovalFormatDO> findAllByBillCategoryIdAndOrderByDimension(String billCategoryId);

    /**
     * 基于billCategoryId获取所有的审批格式记录
     * @param billCategoryId 单据种类id
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE billCategoryId = ?1 AND (dim1 = '' OR dim1 is null) AND (dim2 = '' OR dim2 is null)")
    List<ApprovalFormatDO> findAllByBillCategoryIdAndDim1IsNullAndDim2IsNull(String billCategoryId);

    /**
     * 基于BillCategoryId获取所有的审批格式记录
     * @param billCategoryId 单据种类id
     * @param dim1 审批格式第一维度
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE billCategoryId = ?1 AND dim1 = ?2 AND (dim2 = '' OR dim2 is null)")
    List<ApprovalFormatDO> findAllByBillCategoryIdAndDim1AndDim2IsNull(String billCategoryId, String dim1);

    /**
     * 基于billCategoryId获取所有的审批格式记录
     * @param billCategoryId 单据种类id
     * @param dim1 审批格式第一维度
     * @param dim2 审批格式第二维度
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE billCategoryId = ?1 AND dim1 = ?2 AND dim2 = ?3")
    List<ApprovalFormatDO> findAllByBillCategoryIdAndDim1AndDim2(String billCategoryId, String dim1, String dim2);

    /**
     * 基于formId获取所有的审批格式记录
     * @param formId 审批单据id
     * @return 审批格式DO集合
     */
    @Query(nativeQuery = true, value = "SELECT * FROM gspapprovalformat WHERE formId = ?1")
    List<ApprovalFormatDO> findAllByFormId(String formId);


    Boolean existsAllByBillCategoryIdAndDim1AndDim2(String a,String b,String c);
}
