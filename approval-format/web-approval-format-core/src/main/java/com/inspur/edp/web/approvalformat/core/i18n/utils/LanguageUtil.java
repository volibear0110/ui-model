package com.inspur.edp.web.approvalformat.core.i18n.utils;

import io.iec.edp.caf.boot.context.CAFContext;

public class LanguageUtil {
    /**
     * 获取当前用户的系统语言
     *
     * @return zh-CHS,en,zh-CHT
     */
    public String getLanguage() {
        String language = getLanguageFromExternalAPI();
        return language;
    }

    private String getLanguageFromExternalAPI() {
        return CAFContext.current.getLanguage();
    }

}