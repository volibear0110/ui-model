/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.util;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/05/17
 */
public class ApproveFormatPreviewUtil {
    private static final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);


    /**
     * 根据formId获取元数据
     *
     * @param formId
     * @return
     * @throws Exception
     */

    public static GspMetadata getGspMetadataWithFormId(String formId) throws Exception {
        GspMetadata gspMetadata = customizationService.getMetadata(formId);
        if (gspMetadata == null) {
            throw new WebCustomException(ApprovalFormatTranslateUtil.getMessage("getMetadataErrorByFormID"));
        }
        return gspMetadata;
    }


    /**
     * 获取移动审批对应表单路径
     *
     * @param formCode
     * @return
     */
    public static String generateMobileApproveDevBasePath(String formCode) {
        String currentWorkSpace = FileUtility.getCurrentWorkPath();
        String projectRuntimeAppPath = currentWorkSpace + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                "web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                "runtime" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                "projects" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                "mobileapproval" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                formCode;
        return projectRuntimeAppPath;
    }

}
