/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.webservice;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.approvalformat.api.service.BusinessEntityService;
import com.inspur.edp.web.approvalformat.api.webservice.BusinessEntityWebService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 业务实体服务实现类
 * @author Xu‘fa Wang
 * @date 2020/5/16 19:12
 */
public class BusinessEntityWebServiceImpl implements BusinessEntityWebService {
    private BusinessEntityService businessEntityService = SpringBeanUtils.getBean(BusinessEntityService.class);

    public BusinessEntityWebServiceImpl(BusinessEntityService businessEntityService) {
        if(businessEntityService != null) {
            this.businessEntityService = businessEntityService;
        }
    }

    @Override
    public GspBusinessEntity getBusinessEntity(String bizEntityId) {
        return this.businessEntityService.getBusinessEntity(bizEntityId);
    }

    @Override
    public GspMetadata getBusinessEntityMetadata(String bizEntityId) {
        return this.businessEntityService.getBusinessEntityMetadata(bizEntityId);
    }
}
