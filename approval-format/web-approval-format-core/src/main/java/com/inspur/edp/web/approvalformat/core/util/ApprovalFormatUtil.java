/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.util;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormat;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormatQueryResponseBody;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

/**
 * 审批格式通用类
 * @author Xu‘fa Wang
 * @date 2020/5/19 21:13
 */
public class ApprovalFormatUtil {
    /**
     * 比较两个ApprovalFormat实例是否相等
     * @param source 待比较ApprovalFormat实例
     * @param destination 比较ApprovalFormat实例
     * @return true: 两个值相同; false：两个值不同
     */
    public static boolean isEqual(ApprovalFormat source, ApprovalFormat destination) {
        return source.getId().equals(destination.getId())
                && source.getFirstDimension().equals(destination.getFirstDimension())
                && source.getSecondDimension().equals(destination.getSecondDimension())
                && source.getBillCategoryId().equals(destination.getBillCategoryId())
                && source.getBizEntityId().equals(destination.getBizEntityId())
                && source.getViewObjectId().equals(destination.getViewObjectId())
                && source.getApprovalFormId().equals(destination.getApprovalFormId())
                && source.getApprovalFormPublishUri().equals(destination.getApprovalFormPublishUri());
    }


    /**
     * 转换帮助元数据内容
     * @param approvalFormat 审批格式实例
     */
    public static ApprovalFormatQueryResponseBody convertToApprovalFormatQueryResponseBody(ApprovalFormat approvalFormat) {
        if(approvalFormat == null) {
            return null;
        }

        ApprovalFormatQueryResponseBody approvalFormatQueryResponseBody = new ApprovalFormatQueryResponseBody();

        approvalFormatQueryResponseBody.setId(approvalFormat.getId());
        approvalFormatQueryResponseBody.setCode(approvalFormat.getCode());
        approvalFormatQueryResponseBody.setName(approvalFormat.getName());
        approvalFormatQueryResponseBody.setAttributeName(approvalFormat.getAttributeName());
        approvalFormatQueryResponseBody.setFirstDimension(approvalFormat.getFirstDimension());
        // 维度名称默认等于维度编号
        approvalFormatQueryResponseBody.setFirstDimensionName(approvalFormat.getFirstDimension());
        approvalFormatQueryResponseBody.setSecondDimension(approvalFormat.getSecondDimension());
        approvalFormatQueryResponseBody.setSecondDimensionName(approvalFormat.getSecondDimension());
        approvalFormatQueryResponseBody.setBillCategoryId(approvalFormat.getBillCategoryId());
        approvalFormatQueryResponseBody.setBizEntityId(approvalFormat.getBizEntityId());
        approvalFormatQueryResponseBody.setViewObjectId(approvalFormat.getViewObjectId());
        approvalFormatQueryResponseBody.setApprovalFormId(approvalFormat.getApprovalFormId());
        approvalFormatQueryResponseBody.setApprovalFormPublishUri(approvalFormat.getApprovalFormPublishUri());

        return approvalFormatQueryResponseBody;
    }

    public static ObjectNode getVoContent(GspMetadata formMetaData) {
 		FormMetadataContent formMetaDataContent = (FormMetadataContent)formMetaData.getContent();
     ObjectNode voContent = (ObjectNode)formMetaDataContent.getContents().get("module").get("schemas").get(0);
     return voContent;
    }
}
