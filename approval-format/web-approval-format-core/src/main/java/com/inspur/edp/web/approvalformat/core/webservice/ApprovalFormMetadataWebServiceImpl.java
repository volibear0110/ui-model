/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalViewObjectUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.service.ApprovalFormMetadataService;
import com.inspur.edp.web.approvalformat.api.webservice.ApprovalFormMetadataWebService;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 审批单据元数据Web服务实现类
 * @author Xu‘fa Wang
 * @date 2020/5/17 18:05
 */
public class ApprovalFormMetadataWebServiceImpl implements ApprovalFormMetadataWebService {
    private ApprovalFormMetadataService approvalFormMetadataService = SpringBeanUtils.getBean(ApprovalFormMetadataService.class);

    public ApprovalFormMetadataWebServiceImpl(ApprovalFormMetadataService approvalFormMetadataService) {
        if(approvalFormMetadataService != null) {
            this.approvalFormMetadataService = approvalFormMetadataService;
        }
    }

    @Override
    public void updateApprovalFormMetadata(ApprovalFormUpdateRequestBody approvalFormUpdateRequestBody) {
        this.approvalFormMetadataService.updateApprovalFormMetadata(approvalFormUpdateRequestBody);
    }

    @Override
    public void updateApprovalViewModelMetadata(ApprovalViewObjectUpdateRequestBody approvalViewObjectUpdateRequestBody) {
        this.approvalFormMetadataService.updateApprovalViewModelMetadata(approvalViewObjectUpdateRequestBody);
    }

    @Override
    public FormMetadataContent getApprovalFormMetadataContent(String approvalFormMetadataId) {
        return this.approvalFormMetadataService.getApprovalFormMetadataContent(approvalFormMetadataId);
    }

    @Override
    public String createSchemaFromVo(@RequestBody JsonNode voContent) {
        return this.approvalFormMetadataService.createSchemaFromVo(voContent);
    }
}
