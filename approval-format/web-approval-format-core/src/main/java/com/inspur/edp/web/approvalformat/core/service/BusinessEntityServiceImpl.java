/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.service;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.approvalformat.api.service.BusinessEntityService;
import com.inspur.edp.web.approvalformat.core.util.ApprovalFormatTranslateUtil;
import com.inspur.edp.web.common.customexception.WebCustomException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 业务实体服务实现类
 *
 * @author Xu‘fa Wang
 * @date 2020/5/16 19:15
 */
public class BusinessEntityServiceImpl implements BusinessEntityService {
    private final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);

    @Override
    public GspBusinessEntity getBusinessEntity(String bizEntityId) {
        GspMetadata bizEntityMetadata = this.customizationService.getMetadata(bizEntityId);
        if (bizEntityMetadata == null) {
            Object[] params = {bizEntityId};
            throw new WebCustomException(ApprovalFormatTranslateUtil.getMessage("parameterConversionFailed", params));
        }

        return (GspBusinessEntity) bizEntityMetadata.getContent();
    }

    @Override
    public GspMetadata getBusinessEntityMetadata(String bizEntityId) {
        GspMetadata bizEntityMetadata = this.customizationService.getMetadata(bizEntityId);
        if (bizEntityMetadata == null) {
            Object[] params = {bizEntityId};
            throw new WebCustomException(ApprovalFormatTranslateUtil.getMessage("parameterConversionFailed", params));
        }

        return bizEntityMetadata;
    }
}
