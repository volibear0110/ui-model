/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.service;

import com.inspur.edp.bcc.billcategory.api.BillCategoryService;
import com.inspur.edp.bcc.billcategory.entity.dimension.BillCategoryDimension;
import com.inspur.edp.web.approvalformat.api.service.DimensionService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 维度服务
 * @author Xu‘fa Wang
 * @date 2020/5/11 16:44
 */
public class DimensionServiceImpl implements DimensionService {
    @Autowired
    private BillCategoryService billCategoryService;

    public DimensionServiceImpl() {
        if(this.billCategoryService == null) {
            billCategoryService = SpringBeanUtils.getBean(BillCategoryService.class);
        }
    }

    public DimensionServiceImpl(BillCategoryService billCategoryService) {
        if(billCategoryService == null) {
            this.billCategoryService = SpringBeanUtils.getBean(BillCategoryService.class);
        } else {
            this.billCategoryService = billCategoryService;
        }
    }

    @Override
    public BillCategoryDimension getDimension(String billCategoryId, String billCategoryExtendCode) {
        return this.billCategoryService.getDimension(billCategoryId, billCategoryExtendCode);
    }

    @Override
    public List<BillCategoryDimension> getDimensionCollection(String billCategoryId) {
        return this.billCategoryService.getDimensions(billCategoryId);
    }
}
