/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.web.approvalformat.api.entity.*;
import com.inspur.edp.web.approvalformat.api.service.ApprovalFormatService;
import com.inspur.edp.web.approvalformat.api.webservice.ApprovalFormatWebService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 审批格式Web服务实现
 * @author Xu‘fa Wang
 * @date 2020/5/14 9:25
 */
public class ApprovalFormatWebServiceImpl implements ApprovalFormatWebService {
    private final ApprovalFormatService approvalFormatService;

    public ApprovalFormatWebServiceImpl(ApprovalFormatService approvalFormatService) {
        this.approvalFormatService = approvalFormatService;
    }

    @Override
    public ApprovalFormatForestByDimension getApprovalFormatByDimension(String billCategoryId, String billCategoryExtendCode) {
        return this.approvalFormatService.getApprovalFormatByDimension(billCategoryId, billCategoryExtendCode);
    }

    @Override
    public Map<String, Object> getEntityDataByBizEntityId(String dataId, String bizEntityId) {
        return this.approvalFormatService.getEntityDataByBizEntityId(dataId, bizEntityId);
    }

    @Override
    public IEntityData getEntityDataByBizEntityConfigId(String dataId, String bizEntityConfigId) {
        return this.approvalFormatService.getEntityDataByBizEntityConfigId(dataId, bizEntityConfigId);
    }

    @Override
    public List<IEntityData> queryEntityData(String bizEntityId) {
        return this.approvalFormatService.queryEntityData(bizEntityId);
    }

    @Override
    public List<IEntityData> queryEntityDataWithCondition(JsonNode condition) {
        return this.approvalFormatService.queryEntityDataWithCondition(condition);
    }

    @Override
    @Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ApprovalFormatCreateResponseBody createApprovalFormat(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody) {
        return this.approvalFormatService.createApprovalFormat(approvalFormatCreateRequestBody);
    }


    @Override
    public void deleteApprovalFormat(String approvalFormatId) {
        this.approvalFormatService.deleteApprovalFormat(approvalFormatId);
    }

    @Override
    public void deleteApprovalFormatAndRelation(String billCategoryId, String metadataId, String firstDimension, String secondDimension) {
        this.approvalFormatService.deleteApprovalFormatAndRelation(billCategoryId, metadataId, firstDimension, secondDimension);
    }

    @Override
    public void deleteApprovalFormatAndRelationById(String approvalFormatId) {
        this.approvalFormatService.deleteApprovalFormatAndRelationById(approvalFormatId);
    }

    @Override
    public ApprovalFormat updateApprovalFormat(ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody) {
        return this.approvalFormatService.updateApprovalFormat(approvalFormatUpdateRequestBody);
    }

    @Override
    public ApprovalFormat updateApprovalFormatFormUrl(ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody) {
        return this.approvalFormatService.updateApprovalFormatFormUrl(approvalFormatUpdateRequestBody);
    }

    @Override
    public ApprovalFormat updateApprovalFormatDimension(ApprovalFormatDimensionUpdateRequestBody approvalFormatDimensionUpdateRequestBody) {
        return this.approvalFormatService.updateApprovalFormatDimension(approvalFormatDimensionUpdateRequestBody);
    }

    @Override
    public ApprovalFormat getApprovalFormatEntityById(String approvalFormatId) {
        return this.approvalFormatService.getApprovalFormatEntityById(approvalFormatId);
    }

    @Override
    public List<ApprovalFormat> getApprovalFormatCollection(String billCategoryId) {
        return this.approvalFormatService.getApprovalFormatCollection(billCategoryId);
    }

    @Override
    public List<ApprovalFormat> getApprovalFormatCollectionOrderByDimension(String billCategoryId) {
        return this.approvalFormatService.getApprovalFormatCollectionOrderByDimension(billCategoryId);
    }

    @Override
    public List<ApprovalFormatQueryResponseBody> getApprovalFormatCollectionOrderByDimension(String billCategoryId, String billCategoryExtendCode) {
        return this.approvalFormatService.getApprovalFormatCollectionOrderByDimension(billCategoryId, billCategoryExtendCode);
    }

    @Override
    public void replicateApprovalFormat(String approvalFormatId) {
        this.approvalFormatService.replicateApprovalFormat(approvalFormatId);
    }

    @Override
    public ApprovalFormat getApprovalFormatByBizEntityIdWithDimension(String businessEntityId, String firstDimension, String secondDimension) {
        return this.approvalFormatService.getApprovalFormatByBizEntityIdWithDimension(businessEntityId, firstDimension, secondDimension);
    }

    @Override
    public String getApprovalFormUri(String businessEntityId, String firstDimension, String secondDimension) {
        String localFirstDimension = firstDimension;
        if("null".equalsIgnoreCase(firstDimension)) {
            localFirstDimension = null;
        }
        String localSecondDimension = secondDimension;
        if("null".equalsIgnoreCase(localSecondDimension)) {
            localSecondDimension = null;
        }
        return this.approvalFormatService.getApprovalFormUri(businessEntityId, localFirstDimension, localSecondDimension);
    }

    @Override
    public String getApprovalFormUriByBillCategoryId(String billCategoryId, String firstDimension, String secondDimension) {
        String localFirstDimension = firstDimension;
        if("null".equalsIgnoreCase(firstDimension)) {
            localFirstDimension = null;
        }
        String localSecondDimension = secondDimension;
        if("null".equalsIgnoreCase(localSecondDimension)) {
            localSecondDimension = null;
        }
        return this.approvalFormatService.getApprovalFormUriByBillCategoryId(billCategoryId, localFirstDimension, localSecondDimension);
    }

    @Override
    public HelpProviderResult getHelpProviderData(String billCategoryId, String billCategoryExtendCode, String dimensionId, String billCategoryDimensionItemId) {
        return this.approvalFormatService.getHelpProviderData(billCategoryId, billCategoryExtendCode, dimensionId, billCategoryDimensionItemId);
    }

}
