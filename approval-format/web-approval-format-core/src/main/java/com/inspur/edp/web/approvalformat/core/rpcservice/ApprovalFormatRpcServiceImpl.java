/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.rpcservice;

import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.api.services.IBefSessionManager;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.approvalformat.api.rpcservice.IApprovalFormatRpcService;
import com.inspur.edp.web.approvalformat.api.service.ApprovalFormatService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;

/**
 * 审批格式RPC服务实现类
 * @author Xu‘fa Wang
 * @date 2020/5/20 12:43
 */
public class ApprovalFormatRpcServiceImpl implements IApprovalFormatRpcService  {
    private ApprovalFormatService approvalFormatService =  SpringBeanUtils.getBean(ApprovalFormatService.class);
    private final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);

    public ApprovalFormatRpcServiceImpl(ApprovalFormatService approvalFormatService) {
        if(approvalFormatService != null) {
            this.approvalFormatService = approvalFormatService;
        }
    }

    @Override
    public IEntityData getEntityDataByBizEntityId(String dataId, String bizEntityId) {
        SpringBeanUtils.getBean(IBefSessionManager.class).createSession();
        try {
            IStandardLcp lcp = null;

            GspMetadata  bizEntityMetadata = this.customizationService.getMetadata(bizEntityId);

            // 优先基于bizEntityConfigId创建lcp实例
            if(bizEntityMetadata != null) {
                GspBusinessEntity businessEntity = (GspBusinessEntity)bizEntityMetadata.getContent();
                String bizEntityConfigId = businessEntity.getGeneratedConfigID();

                lcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcp(bizEntityConfigId);
            } else {
                lcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(bizEntityId);
            }

            return lcp.retrieve(dataId).getData();
        } finally {
            SpringBeanUtils.getBean(IBefSessionManager.class).closeCurrentSession();
        }
    }

    @Override
    public List<IEntityData> queryEntityData(String bizEntityId) {
        SpringBeanUtils.getBean(IBefSessionManager.class).createSession();
        try {
            IStandardLcp lcp = null;

            GspMetadata bizEntityMetadata = this.customizationService.getMetadata(bizEntityId);
            // 优先基于bizEntityConfigId创建lcp实例
            if(bizEntityMetadata != null) {
                GspBusinessEntity businessEntity = (GspBusinessEntity)bizEntityMetadata.getContent();
                String bizEntityConfigId = businessEntity.getGeneratedConfigID();

                lcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcp(bizEntityConfigId);
            } else {
                lcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(bizEntityId);
            }
            return dividePage(lcp, 20, 1);
        } finally {
            SpringBeanUtils.getBean(IBefSessionManager.class).closeCurrentSession();
        }
    }
    private List<IEntityData> dividePage(IStandardLcp lcp, int numberInPage, int pageIndex) {
        Pagination pagination = new Pagination();
        pagination.setPageSize(numberInPage);
        pagination.setPageIndex(pageIndex);
        //pagination.setTotalCount(lcp.query().size());
        EntityFilter entityFilter = new EntityFilter();
        entityFilter.setIsUsePagination(true);
        entityFilter.setPagination(pagination);
        return lcp.query(entityFilter);

    }

    @Override
    public String getApprovalFormUriByBizEntityId(String businessEntityId, String firstDimension, String secondDimension) {
        return this.approvalFormatService.getApprovalFormUri(businessEntityId, firstDimension, secondDimension);
    }
}

