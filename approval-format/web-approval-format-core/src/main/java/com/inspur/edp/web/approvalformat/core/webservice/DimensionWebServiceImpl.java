/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.webservice;

import com.inspur.edp.bcc.billcategory.entity.dimension.BillCategoryDimension;
import com.inspur.edp.web.approvalformat.api.service.DimensionService;
import com.inspur.edp.web.approvalformat.api.webservice.DimensionWebService;

import java.util.List;

/**
 * @author Xu‘fa Wang
 * @date 2020/5/14 9:25
 */
public class DimensionWebServiceImpl implements DimensionWebService {
    private final DimensionService dimensionService;

    public DimensionWebServiceImpl(DimensionService dimensionService) {
        this.dimensionService = dimensionService;
    }

    @Override
    public BillCategoryDimension getDimension(String billCategoryId, String billCategoryExtendCode) {
        return this.dimensionService.getDimension(billCategoryId, billCategoryExtendCode);
    }

    @Override
    public List<BillCategoryDimension> getDimensionCollection(String billCategoryId) {
        return this.dimensionService.getDimensionCollection(billCategoryId);
    }
}
