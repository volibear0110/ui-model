/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.domain.converter;

import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormat;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormatCreateResponseBody;
import com.inspur.edp.web.approvalformat.core.domain.entity.ApprovalFormatDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xu‘fa Wang
 * @date 2020/5/16 20:52
 */
public class ApprovalFormatConverter {
    /**
     * VO转DO
     * @param entity
     * @return
     */
    public static ApprovalFormatDO toDo(ApprovalFormat entity) {
        ApprovalFormatDO approvalFormatDO = new ApprovalFormatDO();

        approvalFormatDO.setId(entity.getId());
        approvalFormatDO.setDim1(entity.getFirstDimension());
        approvalFormatDO.setDim2(entity.getSecondDimension());
        approvalFormatDO.setBillCategoryId(entity.getBillCategoryId());
        approvalFormatDO.setBeId(entity.getBizEntityId());
        approvalFormatDO.setVoId(entity.getViewObjectId());
        approvalFormatDO.setFormId(entity.getApprovalFormId());
        approvalFormatDO.setFormUrl(entity.getApprovalFormPublishUri());

        return approvalFormatDO;
    }

    public static ApprovalFormat toVo(ApprovalFormatDO entity) {
        ApprovalFormat approvalFormat = new ApprovalFormat();
        if(entity == null) {
            return null;
        }

        approvalFormat.setId(entity.getId());

        // 统一不同类型数据库返回 ""和null不一致的问题
        approvalFormat.setFirstDimension(entity.getDim1() == null || entity.getDim1().isEmpty() ? "" : entity.getDim1());
        approvalFormat.setSecondDimension(entity.getDim2() == null || entity.getDim2().isEmpty() ? "" : entity.getDim2());

        approvalFormat.setBillCategoryId(entity.getBillCategoryId());
        approvalFormat.setBizEntityId((entity.getBeId() == null || entity.getBeId().isEmpty()) ? "" : entity.getBeId());
        approvalFormat.setViewObjectId((entity.getVoId() == null || entity.getVoId().isEmpty()) ? "" : entity.getVoId());
        approvalFormat.setApprovalFormId(entity.getFormId());
        approvalFormat.setApprovalFormPublishUri(entity.getFormUrl());

        return approvalFormat;
    }

    public static List<ApprovalFormat> toVo(List<ApprovalFormatDO> entityList) {
        List<ApprovalFormat> voList = new ArrayList<>();
        if(entityList == null || entityList.size() == 0) {
            return voList;
        }

        for (ApprovalFormatDO entity: entityList) {
            voList.add(ApprovalFormatConverter.toVo(entity));
        }

        return voList;
    }

    public static ApprovalFormatCreateResponseBody toCreateResponseBody(ApprovalFormat approvalFormat) {
        ApprovalFormatCreateResponseBody approvalFormatCreateResponseBody = new ApprovalFormatCreateResponseBody();

        approvalFormatCreateResponseBody.setId(approvalFormat.getId());
        approvalFormatCreateResponseBody.setCode(approvalFormat.getCode());
        approvalFormatCreateResponseBody.setName(approvalFormat.getName());
        approvalFormatCreateResponseBody.setFirstDimension(approvalFormat.getFirstDimension());
        approvalFormatCreateResponseBody.setSecondDimension(approvalFormat.getSecondDimension());
        approvalFormatCreateResponseBody.setBillCategoryId(approvalFormat.getBillCategoryId());
        approvalFormatCreateResponseBody.setBizEntityId(approvalFormat.getBizEntityId());
        approvalFormatCreateResponseBody.setViewObjectId(approvalFormat.getViewObjectId());
        approvalFormatCreateResponseBody.setApprovalFormId(approvalFormat.getApprovalFormId());
        approvalFormatCreateResponseBody.setApprovalFormPublishUri(approvalFormat.getApprovalFormPublishUri());

        return approvalFormatCreateResponseBody;
    }
}
