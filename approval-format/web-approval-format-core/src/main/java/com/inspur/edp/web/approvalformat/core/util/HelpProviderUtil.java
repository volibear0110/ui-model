/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.util;

import com.inspur.edp.web.approvalformat.api.entity.HelpMetadataSimpleContent;
import com.inspur.edp.web.help.metadata.HelpMetadataContent;

/**
 * 帮助工具类
 * @author Xu‘fa Wang
 * @date 2020/5/18 11:07
 */
public class HelpProviderUtil {
    /**
     * 转换帮助元数据内容
     * @param helpMetadataContent 帮助元数据内容
     */
    public static HelpMetadataSimpleContent convertToHelpMetadataSimpleContent(HelpMetadataContent helpMetadataContent) {
        if(helpMetadataContent == null) {
            return null;
        }

        HelpMetadataSimpleContent helpMetadataSimpleContent = new HelpMetadataSimpleContent();
        helpMetadataSimpleContent.setVersion(helpMetadataContent.getVersion());
        helpMetadataSimpleContent.setId(helpMetadataContent.getId());
        helpMetadataSimpleContent.setCode(helpMetadataContent.getCode());
        helpMetadataSimpleContent.setName(helpMetadataContent.getName());
        helpMetadataSimpleContent.setTitle(helpMetadataContent.getTitle());
        helpMetadataSimpleContent.setIdField(helpMetadataContent.getIdField());
        helpMetadataSimpleContent.setIdFieldName(helpMetadataContent.getIdFieldName());
        helpMetadataSimpleContent.setTextField(helpMetadataContent.getTextField());
        helpMetadataSimpleContent.setTextFieldName(helpMetadataContent.getTextFieldName());
        helpMetadataSimpleContent.setValueField(helpMetadataContent.getValueField());
        helpMetadataSimpleContent.setValueFieldName(helpMetadataContent.getValueFieldName());
        helpMetadataSimpleContent.setDisplayType(helpMetadataContent.getDisplayType());
        helpMetadataSimpleContent.setDescription(helpMetadataContent.getDescription());
        return helpMetadataSimpleContent;
    }
}
