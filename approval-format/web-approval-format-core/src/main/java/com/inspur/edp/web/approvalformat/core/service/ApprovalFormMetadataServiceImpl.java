/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.core.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormatCreateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalViewObjectUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.schema.FormSchema;
import com.inspur.edp.web.approvalformat.api.service.ApprovalFormMetadataService;
import com.inspur.edp.web.approvalformat.core.util.ApprovalFormSchemaUtil;
import com.inspur.edp.web.approvalformat.core.util.ApprovalFormUtil;
import com.inspur.edp.web.approvalformat.core.util.ApprovalFormatPermission;
import com.inspur.edp.web.approvalformat.core.util.ApprovalFormatTranslateUtil;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 审批单据元数据服务实现类
 *
 * @author Xu‘fa Wang
 * @date 2020/5/17 18:10
 */
public class ApprovalFormMetadataServiceImpl implements ApprovalFormMetadataService {
    private final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);

    @Override
    public void updateApprovalFormMetadata(ApprovalFormUpdateRequestBody approvalFormUpdateRequestBody) {
        new ApprovalFormatPermission().checkModulePermission();
        String approvalFormMetadataId = approvalFormUpdateRequestBody.getFormMetadataId();
        AbstractMetadataContent approvalFormMetadataContent = approvalFormUpdateRequestBody.getFormMetadataContent();

        GspMetadata approvalFormMetadata = this.customizationService.getMetadata(approvalFormMetadataId);
        if (approvalFormMetadata == null) {
            return;
        }
        approvalFormMetadata.setContent(approvalFormMetadataContent);

        this.customizationService.save(approvalFormMetadata);
    }

    @Override
    public void updateApprovalViewModelMetadata(ApprovalViewObjectUpdateRequestBody approvalViewObjectUpdateRequestBody) {
//        String viewModelId = approvalViewObjectUpdateRequestBody.getViewModelId();
        GspViewModel viewModelContent = approvalViewObjectUpdateRequestBody.getViewModelContent();

        GspMetadata viewModelMetadata = this.customizationService.getMetadata(viewModelContent.getID());
        if (viewModelMetadata == null) {
            return;
        }
        viewModelMetadata.setContent(viewModelContent);

        this.customizationService.save(viewModelMetadata);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void createApprovalForm(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody) {
        if (StringUtility.isNullOrEmpty(approvalFormatCreateRequestBody.getBasicFormId())) {
            ApprovalFormUtil.createRootApprovalForm(approvalFormatCreateRequestBody, "Form");
        } else {
            //永远不会进else 20210825
            ApprovalFormUtil.createChildApprovalForm(approvalFormatCreateRequestBody, "Form");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void createApprovalFormViewObject(ApprovalFormatCreateRequestBody request) {
        if (StringUtility.isNullOrEmpty(request.getBasicVoId())) {
            ApprovalFormUtil.createRootApprovalForm(request, "GSPViewModel");
        } else {
            ApprovalFormUtil.createChildApprovalForm(request, "GSPViewModel");
        }
    }

    @Override
    public FormMetadataContent getApprovalFormMetadataContent(String approvalFormMetadataId) {
        new ApprovalFormatPermission().checkModulePermission();
        GspMetadata approvalFormMetadata = this.customizationService.getMetadata(approvalFormMetadataId);
        if (approvalFormMetadata == null) {
            return null;
        }

        return (FormMetadataContent) approvalFormMetadata.getContent();
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public String createSchemaFromVo(JsonNode voContent) {
        try {
            ObjectMapper mapper = Utils.getMapper();
            String vmString = mapper.writeValueAsString(voContent);
            GspViewModel vm = mapper.readValue(vmString, GspViewModel.class);
            FormSchema schema = ApprovalFormSchemaUtil.constructSchema(vm);
            return new ObjectMapper().writeValueAsString(schema);
        } catch (JsonProcessingException e) {
            Object[] params = {voContent.get("ID")};
            throw new WebCustomException(ApprovalFormatTranslateUtil.getMessage("schemaSerializationFailed", params));
        }
    }
}
