/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.rpc.webservice.impl;

import com.fasterxml.jackson.databind.JsonNode;

import com.inspur.edp.web.approvalformat.rpc.service.deployapprovalformat.DeployApprovalFormat;
import com.inspur.edp.web.approvalformat.rpc.webservice.RtcValidationApproveWebService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.Map;

public class RtcValidationApproveWebServiceImpl implements RtcValidationApproveWebService {
    private final DeployApprovalFormat deployApprovalFormat = SpringBeanUtils.getBean(DeployApprovalFormat.class);

    @Override
    public Map<String, Object> beforeNavigateApprove(JsonNode queryStringMap) {
        return deployApprovalFormat.deployApprovalFormat(queryStringMap);
    }


}
