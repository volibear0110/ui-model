package com.inspur.edp.web.approvalformat.rpc.util;

import com.inspur.edp.web.approvalformat.rpc.i18n.Translator;

import java.text.MessageFormat;

public class ApprovalFormatTranslateUtil {

    /**
     * 根据资源编号获取国际化之后的资源
     *
     * @param resourceID 资源编号
     * @return 翻译之后的国际化资源
     */
    public static String getMessage(String resourceID) {
        return Translator.getInstance().translate(resourceID);
    }

    /**
     * 根据资源编号和格式化参数获取国际化之后的资源
     *
     * @param resourceID 资源编号
     * @param params     格式化参数
     * @return 翻译之后的国际化资源
     */
    public static String getMessage(String resourceID, Object[] params) {
        return MessageFormat.format(Translator.getInstance().translate(resourceID), params);
    }
}
