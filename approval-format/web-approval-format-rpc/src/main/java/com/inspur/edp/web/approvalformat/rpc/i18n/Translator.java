package com.inspur.edp.web.approvalformat.rpc.i18n;

import com.inspur.edp.web.approvalformat.rpc.i18n.utils.LanguageUtil;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

public class Translator {

    ResourceLocalizer resource = SpringBeanUtils.getBean(ResourceLocalizer.class);

    private static Translator instance;

    private String currentLanguage = "";

    public static Translator getInstance() {
        String currentLanguage = new LanguageUtil().getLanguage();
        if (instance == null || !currentLanguage.equals(instance.currentLanguage)) {
            instance = new Translator();
        }
        return instance;
    }

    private Translator() {
        currentLanguage = new LanguageUtil().getLanguage();
    }

    public String translate(String key) {
        String str = resource.getString(key, "MobileApproveFormatRpcException.properties", "pfcommon", currentLanguage);
        return str;
    }
}