/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.rpc.service.deployapprovalformat;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.web.approvalformat.rpc.util.ApprovalFormatTranslateUtil;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.ScriptCacheResponse;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerVersionManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.HashMap;
import java.util.Map;

public class DeployApprovalFormatImpl implements DeployApprovalFormat {
    @Override
    public Map<String, Object> deployApprovalFormat(JsonNode queryStringMap) {
        // 两个参数 projectName和相对地址
        String projectName = queryStringMap.get("projectName").asText();
        String projectrelativepath = queryStringMap.get("projectrelativepath").asText();

        ScriptCacheResponse scriptCacheResponse = ScriptCacheResponse.getInstance();
        Map<String, Object> result = new HashMap<>();

        LocalServerVersionManager localServerVersionManager = SpringBeanUtils.getBean(LocalServerVersionManager.class);
        scriptCacheResponse = localServerVersionManager.checkVersionWithProjectNameAndRelativePath(projectName, projectrelativepath);

        if (scriptCacheResponse != null) {
            if (scriptCacheResponse.isSuccess()) {
                // 处理成功
                result.put("flag", "true");
                return result;
            } else {
                throw new WebCustomException(scriptCacheResponse.getErrorMessage());
            }
        } else {
            throw new WebCustomException(ApprovalFormatTranslateUtil.getMessage("parameterConversionFailed"));
        }
    }
}
