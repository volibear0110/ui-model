/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 审批格式更新请求体
 * @author Xu‘fa Wang
 * @date 2020/5/18 20:53
 */
@Data
public class ApprovalFormatUpdateRequestBody {
    /**
     * 审批单据id
     */
    @NotNull
    private String approvalFormatId;

    /**
     * 审批格式
     */
    @NotNull
    private ApprovalFormat approvalFormatInstance;
}
