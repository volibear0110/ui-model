/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

import lombok.Data;

/**
 * 审批格式创建返回实体类
 * @author Xu‘fa Wang
 * @date 2020/5/22 15:07
 */
@Data
public class ApprovalFormatCreateResponseBody {
    /**
     * id
     */
    private String id;

    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 模板属性名
     */
    private String attributeName;

    /**
     * 第一维度
     */
    private String firstDimension;

    /**
     * 第二维度
     */
    private String secondDimension;

    /**
     * 单据种类id
     */
    private String billCategoryId;

    /**
     * 审批格式关联业务实体id
     */
    private String bizEntityId;

    /**
     * 审批格式关联viewObject id
     */
    private String viewObjectId;

    /**
     * 审批格式关联审批单据id
     */
    private String approvalFormId;
    /**
     * 审批单据的发布地址
     */
    private String approvalFormPublishUri;
}
