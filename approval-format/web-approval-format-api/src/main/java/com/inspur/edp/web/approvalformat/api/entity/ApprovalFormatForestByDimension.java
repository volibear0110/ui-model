/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

import lombok.Data;
import com.inspur.edp.bcc.billcategory.entity.dimension.BillCategoryDimension;

import java.util.List;

/**
 * 基于维度的审批格式树集合
 * @author Xu‘fa Wang
 * @date 2020/5/16 9:28
 */
@Data
public class ApprovalFormatForestByDimension {
    /**
     * 树形审批格式集合--审批格式森林
     */
    private List<ApprovalFormatTreeNode> approvalFormatForest;

    /**
     * 单据种类维度
     */
    private BillCategoryDimension billCategoryDimension;

    public ApprovalFormatForestByDimension() {

    }

    public ApprovalFormatForestByDimension(List<ApprovalFormatTreeNode> approvalFormatForest, BillCategoryDimension billCategoryDimension) {
        this.approvalFormatForest = approvalFormatForest;
        this.billCategoryDimension = billCategoryDimension;
    }
}
