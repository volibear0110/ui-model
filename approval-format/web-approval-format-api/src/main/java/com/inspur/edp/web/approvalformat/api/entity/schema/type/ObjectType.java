/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity.schema.type;

import com.inspur.edp.web.approvalformat.api.entity.schema.Field;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Xu‘fa Wang
 * @date 2020/6/8 16:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ObjectType extends FieldType {
    private List<Field> fields;

    public ObjectType(String name, String displayName, List<Field> fields) {
        super(ObjectType.class.getSimpleName(), name, displayName);
        this.fields = fields;
    }

    public ObjectType(String $type, String name, String displayName, List<Field> fields){
        super($type, name, displayName);
        this.fields = fields;
    }
}