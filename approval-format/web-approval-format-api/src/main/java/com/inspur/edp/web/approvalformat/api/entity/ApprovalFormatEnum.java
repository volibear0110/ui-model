/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

/**
 * 审批格式枚举
 * @author Xu‘fa Wang
 * @date 2020/5/16 14:37
 */
public class ApprovalFormatEnum {
    public enum TEMPLATE_ATTRIBUTES {
        SYS, NEW
    }

    public enum LEVEL {
        ONE, TWO, THREE
    }

    public enum FIELD_TYPES{
        SIMPLE("SimpleField"), COMPLEX("ComplexField");
        private final String text;
        FIELD_TYPES(String text){
            this.text = text;
        }
        public String text(){
            return this.text;
        }
    }

    public enum SIMPLE_FIELD_TYPES{
        BOOLEAN("BooleanType"),
        DATETYPE("DateType"),
        DATE("Date"),
        DATETIME("DateTime"),
        ENUM("EnumType"),
        NUMBER("NumericType"),
        INTEGER("Integer"),
        DECIMAL("Decimal"),
        STRING("StringType"),
        TEXT("TextType");

        private final String text;
        SIMPLE_FIELD_TYPES(String text){
            this.text = text;
        }
        public String text(){
            return this.text;
        }
    }

    public enum COMPLEX_FIELD_TYPES{
        ENTITY("EntityType"), OBJECT("ObjectType"), HIERARCHY("HierarchyType");
        private final String text;
        COMPLEX_FIELD_TYPES(String text){
            this.text = text;
        }
        public String text(){
            return this.text;
        }
    }
}
