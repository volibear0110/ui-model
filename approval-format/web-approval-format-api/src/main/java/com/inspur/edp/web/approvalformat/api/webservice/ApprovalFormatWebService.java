/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.web.approvalformat.api.entity.*;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

/**
 * 审批格式 web 服务
 * @author Xu‘fa Wang
 * @date 2020/5/15 10:06
 */
@Path("/format")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ApprovalFormatWebService {
    /**
     * 获取含维度信息的审批单据树集合
     * @param billCategoryId 单据种类Id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @return ApprovalFormatForestByDimension  基于维度的审批格式树集合
     */
    @GET
    @Path("/dimension/{billCategoryId}/{billCategoryExtendCode}")
    @Deprecated
    ApprovalFormatForestByDimension getApprovalFormatByDimension(@PathParam("billCategoryId") String billCategoryId, @PathParam("billCategoryExtendCode") String billCategoryExtendCode);

    /**
     * 基于业务实体id获取实体数据
     * @param dataId 数据id
     * @param bizEntityId 业务实体id
     * @return 实体数据
     */
    @GET
    @Path("/entity/data/{dataId}/{bizEntityId}")
    Map<String, Object> getEntityDataByBizEntityId(@PathParam("dataId") String dataId,
                                                   @PathParam("bizEntityId") String bizEntityId);

    /**
     * 基于业务实体id获取实体数据
     * @param dataId 数据id
     * @param bizEntityConfigId 业务实体配置id
     * @return 实体数据
     */
    @GET
    @Path("/entity/data/configid/{dataId}/{bizEntityConfigId}")
    @Deprecated
    IEntityData getEntityDataByBizEntityConfigId(@PathParam("dataId") String dataId,
                                                 @PathParam("bizEntityConfigId") String bizEntityConfigId);

    /**
     * 基于业务实体id（含维度）获取数据
     * @param bizEntityId 业务实体id
     * @return 实体数据集合
     */
    @GET
    @Path("/entity/data/query/{bizEntityId}")
    List<IEntityData> queryEntityData(@PathParam("bizEntityId") String bizEntityId);

    /**
     * 基于业务实体id（含维度）和条件获取数据
     * @param condition
     * @return
     */
    @POST
    @Path("/entity/data/query")
    List<IEntityData> queryEntityDataWithCondition(@RequestBody JsonNode condition);
    /**
     * 创建审批格式实例
     * @param approvalFormatCreateRequestBody 审批格式创建请求体
     * @return ApprovalFormatCreateResponseBody 审批格式创建返回体实例
     */
    @POST
    @Path("/")
    ApprovalFormatCreateResponseBody createApprovalFormat(@RequestBody ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody);

    /**
     * 删除审批格式实例（仅删除审批格式）
     * @param approvalFormatId 审批格式id
     */
    @DELETE
    @Path("/{id}")
    @Deprecated
    void deleteApprovalFormat(@PathParam("id") String approvalFormatId);

    /**
     * 删除审批格式实例
     * @param billCategoryId 审批格式关联单据种类id
     * @param metadataId 审批格式关联元数据id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     */
    @DELETE
    @Path("/{billCategoryId}/{metadataId}/{firstDimension}/{secondDimension}")
    @Deprecated
    void deleteApprovalFormatAndRelation(@PathParam("billCategoryId") String billCategoryId,
                                         @PathParam("metadataId") String metadataId,
                                         @PathParam("firstDimension") String firstDimension,
                                         @PathParam("secondDimension") String secondDimension);

    /**
     * 删除审批格式实例及关联数据
     * @param id 审批格式id
     */
    @DELETE
    @Path("/relation/{id}")
    void deleteApprovalFormatAndRelationById(@PathParam("id") String id);

    /**
     * 更新审批格式的formUrl
     * @param approvalFormatUpdateRequestBody 审批格式更新请求体
     * @return 更新后ApprovalFormat实例
     */
    @PUT
    @Path("/formUrl")
    ApprovalFormat updateApprovalFormatFormUrl(@RequestBody ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody);

    /**
     * 更新审批格式实例
     * @param approvalFormatUpdateRequestBody 审批格式更新请求体
     * @return 更新后ApprovalFormat实例
     */
    @PUT
    @Path("/")
    ApprovalFormat updateApprovalFormat(@RequestBody ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody);

    /**
     * 更新审批格式维度
     * @param approvalFormatDimensionUpdateRequestBody 审批格式更新请求体
     * @return 更新后ApprovalFormat实例
     */
    @PUT
    @Path("/dimension")
    ApprovalFormat updateApprovalFormatDimension(@RequestBody ApprovalFormatDimensionUpdateRequestBody approvalFormatDimensionUpdateRequestBody);

    /**
     * 基于ID查询审批格式
     * @param approvalFormatId 审批格式id
     * @return 审批格式实例
     */
    @GET
    @Path("/{id}")
    ApprovalFormat getApprovalFormatEntityById(@PathParam("id") String approvalFormatId);

    /**
     * 基于单据种类id获取审批格式
     * @param billCategoryId 单据种类Id
     * @return 审批格式实例集合
     */
    @GET
    @Path("/list/{billCategoryId}")
    List<ApprovalFormat> getApprovalFormatCollection(@PathParam("billCategoryId") String billCategoryId);

    /**
     * 基于单据种类id获取已排序（基于维度）审批格式集合
     * @param billCategoryId 单据种类Id
     * @return 审批格式实例集合
     */
    @GET
    @Path("/order/list/{billCategoryId}")
    @Deprecated
    List<ApprovalFormat> getApprovalFormatCollectionOrderByDimension(@PathParam("billCategoryId") String billCategoryId);

    /**
     * 基于单据种类id获取已排序（基于维度）审批格式集合
     * @param billCategoryId 单据种类Id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @return 审批格式实例集合
     */
    @GET
    @Path("/order/list/{billCategoryId}/{billCategoryExtendCode}")
    List<ApprovalFormatQueryResponseBody> getApprovalFormatCollectionOrderByDimension(@PathParam("billCategoryId") String billCategoryId, @PathParam("billCategoryExtendCode") String billCategoryExtendCode);

    /**
     * 基于ID复制审批格式
     * @param approvalFormatId 审批格式id
     */
    @PUT
    @Path("/replicate/{id}")
    void replicateApprovalFormat(@PathParam("id") String approvalFormatId);

    /**
     * 基于业务实体ID和审批格式维度获取审批格式实例
     * @param businessEntityId 业务实体id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批格式实例
     */
    @GET
    @Path("/business/entity/{bizEntityId}/{firstDimension}/{secondDimension}")
    @Deprecated
    ApprovalFormat getApprovalFormatByBizEntityIdWithDimension(@PathParam("bizEntityId") String businessEntityId,
                                                               @PathParam("firstDimension") String firstDimension,
                                                               @PathParam("secondDimension") String secondDimension);

    /***
     * 获取业务实体关联审批格式中审批单据uri
     * @param businessEntityId 业务实体id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批单据uri
     */
    @GET
    @Path("/form/url/{bizEntityId}/{firstDimension}/{secondDimension}")
    String getApprovalFormUri(@PathParam("bizEntityId") String businessEntityId, @PathParam("firstDimension") String firstDimension, @PathParam("secondDimension") String secondDimension);

    /***
     * 获取单据种类关联审批格式的审批单据uri
     * @param billCategoryId 单据种类id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批单据uri
     */
    @GET
    @Path("/billcategory/form/uri/{billCategoryId}/{firstDimension}/{secondDimension}")
    String getApprovalFormUriByBillCategoryId(@PathParam("billCategoryId") String billCategoryId, @PathParam("firstDimension") String firstDimension, @PathParam("secondDimension") String secondDimension);

    /***
     * 获取帮助数据
     * @param billCategoryId 单据种类id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @param dimensionId 维度id
     * @param billCategoryDimensionItemId 单据种类维度项id
     * @return 帮助结果
     */
    @GET
    @Path("/lookup/{billCategoryId}/{billCategoryExtendCode}/{dimensionId}/{billCategoryDimensionItemId}")
    HelpProviderResult getHelpProviderData(@PathParam("billCategoryId") String billCategoryId,
                                           @PathParam("billCategoryExtendCode") String billCategoryExtendCode,
                                           @PathParam("dimensionId") String dimensionId,
                                           @PathParam("billCategoryDimensionItemId") String billCategoryDimensionItemId);

}
