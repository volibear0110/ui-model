/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.web.approvalformat.api.entity.*;

import java.util.List;
import java.util.Map;

/**
 * 审批格式服务
 * @author Xu‘fa Wang
 * @date 2020/5/14 19:53
 */
public interface ApprovalFormatService {
    /**
     * 获取含维度信息的审批单据树集合
     * @param billCategoryId 单据种类id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @return ApprovalFormatForestByDimension  基于维度的审批格式树集合
     */
    ApprovalFormatForestByDimension getApprovalFormatByDimension(String billCategoryId, String billCategoryExtendCode);

    /**
     * 基于业务实体id获取实体数据
     * @param dataId 数据id
     * @param bizEntityId 业务实体id
     * @return 实体数据
     */
    Map<String, Object> getEntityDataByBizEntityId(String dataId, String bizEntityId);

    /**
     * 基于业务实体配置id获取实体数据
     * @param dataId 数据id
     * @param bizEntityConfigId 业务实体配置id
     * @return 实体数据
     */
    @Deprecated
    IEntityData getEntityDataByBizEntityConfigId(String dataId, String bizEntityConfigId);

    /**
     * 基于业务实体id（含维度）获取数据
     * @param bizEntityId 业务实体id
     * @return 实体数据集合
     */
    List<IEntityData> queryEntityData(String bizEntityId);


    List<IEntityData> queryEntityDataWithCondition(JsonNode queryStringMap);

    /**
     * 创建审批格式实例
     * @param approvalFormatCreateRequestBody 审批格式创建请求体
     * @return ApprovalFormatCreateResponseBody 审批格式创建返回体实例
     */
    ApprovalFormatCreateResponseBody createApprovalFormat(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody);

    /**
     * 创建审批格式
     * @param approvalFormatCreateRequestBody 审批格式创建请求体
     * @return ApprovalFormat 审批格式实例
     */
    ApprovalFormat createApprovalFormatRelation(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody);

    /**
     * 删除审批格式实例(仅删除审批格式)
     * @param approvalFormatId 审批格式id
     */
    void deleteApprovalFormat(String approvalFormatId);

    /**
     * 删除审批格式实例
     * @param billCategoryId 审批格式关联单据种类id
     * @param metadataId 审批格式关联元数据id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     */
    @Deprecated
    void deleteApprovalFormatAndRelation(String billCategoryId, String metadataId, String firstDimension, String secondDimension);

    /**
     * 删除审批格式实例及关联数据
     * @param id 审批格式id
     */
    void deleteApprovalFormatAndRelationById(String id);

    /**
     * 更新审批格式实例
     * @param approvalFormatUpdateRequestBody 审批格式更新请求体
     * @return 更新后ApprovalFormat实例
     */
    ApprovalFormat updateApprovalFormat(ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody);

    ApprovalFormat updateApprovalFormatFormUrl(ApprovalFormatUpdateRequestBody approvalFormatUpdateRequestBody);
    /**
     * 更新审批格式维度
     * @param approvalFormatDimensionUpdateRequestBody 审批格式维度更新请求体
     * @return 更新后ApprovalFormat实例
     */
    ApprovalFormat updateApprovalFormatDimension(ApprovalFormatDimensionUpdateRequestBody approvalFormatDimensionUpdateRequestBody);

    /**
     * 基于ID查询审批格式
     * @param approvalFormatId 审批格式id
     * @return 审批格式实例
     */
    ApprovalFormat getApprovalFormatEntityById(String approvalFormatId);

    /**
     * 基于单据种类id获取审批格式
     * @param billCategoryId 单据种类id
     * @return 审批格式实例集合
     */
    List<ApprovalFormat> getApprovalFormatCollection(String billCategoryId);

    /**
     * 基于单据种类id获取审批格式并按照维度排序
     * @param billCategoryId 单据种类Id
     * @return 审批格式实例集合
     */
    @Deprecated
    List<ApprovalFormat> getApprovalFormatCollectionOrderByDimension(String billCategoryId);

    /**
     * 基于单据种类id获取已排序（基于维度）审批格式集合
     * @param billCategoryId 单据种类Id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @return 审批格式实例集合
     */
    List<ApprovalFormatQueryResponseBody> getApprovalFormatCollectionOrderByDimension(String billCategoryId, String billCategoryExtendCode);

    /**
     * 基于ID复制审批格式
     * @param approvalFormatId 审批格式id
     */
    void replicateApprovalFormat(String approvalFormatId);

    /**
     * 基于业务实体ID获取审批格式实例
     * @param businessEntityId 业务实体id
     * @param firstDimension   审批格式第一维度
     * @param secondDimension  审批格式第二维度
     * @return 审批格式实例
     */
    ApprovalFormat getApprovalFormatByBizEntityIdWithDimension(String businessEntityId, String firstDimension, String secondDimension);

    /**
     * 基于单据种类ID获取审批格式实例
     * @param billCategoryId   单据种类Id
     * @param firstDimension   审批格式第一维度
     * @param secondDimension  审批格式第二维度
     * @return 审批格式实例
     */
    ApprovalFormat getApprovalFormatByBillCategoryIdWithDimension(String billCategoryId, String firstDimension, String secondDimension);

    /***
     * 获取业务实体关联审批格式中审批单据uri
     * @param businessEntityId 业务实体id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批单据uri
     */
    String getApprovalFormUri(String businessEntityId, String firstDimension, String secondDimension);

    /***
     * 获取单据种类关联审批格式的审批单据uri
     * @param billCategoryId 单据种类id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批单据uri
     */
    String getApprovalFormUriByBillCategoryId( String billCategoryId, String firstDimension, String secondDimension);

    /***
     * 获取帮助数据
     * @param billCategoryId 单据种类id
     * @param dimensionId 维度id
     * @param billCategoryExtendCode 单据种类可扩展编号
     * @param billCategoryDimensionItemId 单据种类维度项id
     * @return 帮助结果
     */
    HelpProviderResult getHelpProviderData(String billCategoryId, String billCategoryExtendCode, String dimensionId, String billCategoryDimensionItemId);


}
