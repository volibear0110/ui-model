/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormatCreateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalViewObjectUpdateRequestBody;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

/**
 * 审批表单元数据服务
 * @author Xu‘fa Wang
 * @date 2020/5/14 18:17
 */
public interface ApprovalFormMetadataService {
    /**
     * 创建审批单据
     * @param approvalFormatCreateRequestBody 审批单据创建请求体
     */
    void createApprovalForm(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody);

    /**
     * 创建审批单据视图对象
     * @param approvalFormatCreateRequestBody 审批单据创建请求体
     */
    void createApprovalFormViewObject(ApprovalFormatCreateRequestBody approvalFormatCreateRequestBody);

    /**
     * 更新审批表单元数据（仅更新元数据内容）
     * @param approvalFormUpdateRequestBody 待更新审批单据元数据id
     */
    void updateApprovalFormMetadata(ApprovalFormUpdateRequestBody approvalFormUpdateRequestBody);

    /**
     * 更新审批视图模型元数据（仅更新元数据内容）
     * @param approvalViewObjectUpdateRequestBody 审批视图模型更新请求体
     */
    void updateApprovalViewModelMetadata(ApprovalViewObjectUpdateRequestBody approvalViewObjectUpdateRequestBody);

    /**
     * 基于id获取审批表单元数据内容
     * @param approvalFormMetadataId 审批单据id
     * @return 审批单据元数据内容
     */
    FormMetadataContent getApprovalFormMetadataContent(String approvalFormMetadataId);

    /**
     * 基于voContent创建表单Schema
     * @param voContent 视图对象内容
     * @return schema json串
     */
    String createSchemaFromVo(JsonNode voContent);
}
