/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.webservice;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * 业务实体 web 服务
 * @author Xu‘fa Wang
 * @date 2020/5/14 17:40
 */
@Path("/business/entity")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BusinessEntityWebService {
    /**
     * 基于业务实体Id获取业务实体实例
     * @param bizEntityId 业务实体id
     * @return 业务实体实例
     */
    @GET
    @Path("/{bizEntityId}")
    GspBusinessEntity getBusinessEntity(@PathParam("bizEntityId") String bizEntityId);

    /**
     * 基于业务实体Id获取业务实体元数据
     * @param bizEntityId 业务实体id
     * @return 业务实体实例
     */
    @GET
    @Path("/metadata/{bizEntityId}")
    GspMetadata getBusinessEntityMetadata(@PathParam("bizEntityId") String bizEntityId);
}
