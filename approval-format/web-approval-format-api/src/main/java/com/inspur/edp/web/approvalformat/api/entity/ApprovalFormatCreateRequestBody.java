/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

import lombok.Data;

/**
 * 审批格式创建请求实体类
 * @author Xu‘fa Wang
 * @date 2020/5/19 19:38
 */
@Data
public class ApprovalFormatCreateRequestBody {
    /**
     * 单据种类id
     */
    private String billCategoryId;
    /**
     * 业务对象id
     */
    private String bizObjectId;
    /**
     * 命名空间
     */
    private String nameSpace;
    /**
     * 业务实体id
     */
    private String beId;
    /**
     * 基础视图对象id
     */
    private String basicVoId;
    /**
     * 视图id
     */
    private String voId;
    /**
     * 基础表单id
     */
    private String basicFormId;
    /**
     * 表单id
     */
    private String formId;
    /**
     * 表单编号
     */
    private String code;
    /**
     * 表单名称
     */
    private String name;
    /**
     * 第一维度
     */
    private String dim1;
    /**
     * 第二维度
     */
    private String dim2;
    /**
     * 视图对象内容
     */
    private String voContent;
    /**
     * 表单内容
     */
    private String formContent;
    /**
     * 表单url
     */
    private String formUrl;
}
