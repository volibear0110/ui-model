/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.rpcservice;

import com.inspur.edp.cef.entity.entity.IEntityData;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;
import io.iec.edp.caf.rpc.api.annotation.RpcServiceMethod;

import java.util.List;

/**
 * 审批格式rpc服务
 * @author Xu‘fa Wang
 * @date 2020/5/20 12:39
 */
@GspServiceBundle(applicationName = "runtime",serviceUnitName = "bcc", serviceName = "approval_format")
public interface IApprovalFormatRpcService {
    /**
     * 基于业务实体id获取实体数据
     * @param dataId 数据id
     * @param bizEntityId 业务实体id
     * @return 实体数据
     */
    @RpcServiceMethod(serviceId = "com.inspur.edp.web.approvalformat.api.rpcservice.IApprovalFormatRpcService.getEntityDataByBizEntityId")
    IEntityData getEntityDataByBizEntityId(@RpcParam(paramName = "dataId") String dataId,
                                           @RpcParam(paramName = "bizEntityId") String bizEntityId);

    /**
     * 基于业务实体id（含维度）获取数据
     * @param bizEntityId 业务实体id
     * @return 实体数据集合
     */
    @RpcServiceMethod(serviceId = "com.inspur.edp.web.approvalformat.api.rpcservice.IApprovalFormatRpcService.queryEntityData")
    List<IEntityData> queryEntityData(@RpcParam(paramName = "bizEntityId") String bizEntityId);

    /***
     * 获取业务实体关联审批格式中审批单据uri
     * @param businessEntityId 业务实体id
     * @param firstDimension 审批格式第一维度
     * @param secondDimension 审批格式第二维度
     * @return 审批单据uri
     */
    @RpcServiceMethod(serviceId = "com.inspur.edp.web.approvalformat.api.rpcservice.IApprovalFormatRpcService.getApprovalFormUriByBizEntityId")
    String getApprovalFormUriByBizEntityId(@RpcParam(paramName = "businessEntityId") String businessEntityId,
                                           @RpcParam(paramName = "firstDimension") String firstDimension,
                                           @RpcParam(paramName = "secondDimension") String secondDimension);
}
