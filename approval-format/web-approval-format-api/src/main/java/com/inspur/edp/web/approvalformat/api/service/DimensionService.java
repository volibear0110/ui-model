/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.service;

import com.inspur.edp.bcc.billcategory.entity.dimension.BillCategoryDimension;

import java.util.List;

/**
 * 维度服务接口
 * @author Xu‘fa Wang
 * @date 2020/5/13 19:59
 */
public interface DimensionService {
    /**
     * 基于单据种类Id和单据种类扩展码获取维度
     * @param billCategoryId 单据种类Id
     * @param billCategoryExtendCode 单据种类扩展码
     * @return 单据种类维度实例
     */
    BillCategoryDimension getDimension(String billCategoryId, String billCategoryExtendCode);

    /**
     * 基于单据种类Id获取维度集合
     * @param billCategoryId 单据种类Id
     * @return 单据种类维度实例集合
     */
    List<BillCategoryDimension> getDimensionCollection(String billCategoryId);
}
