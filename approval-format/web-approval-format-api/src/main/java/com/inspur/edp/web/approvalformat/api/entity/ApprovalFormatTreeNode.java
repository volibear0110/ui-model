/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.entity;

import lombok.Data;

import java.util.List;

/**
 * 树形结构的审批格式集合
 * @author Xu‘fa Wang
 * @date 2020/5/16 9:37
 */
@Data
public class ApprovalFormatTreeNode {
    private ApprovalFormat data;
    private Boolean expanded;
    private List<ApprovalFormatTreeNode> children;

    public ApprovalFormatTreeNode(ApprovalFormat data) {
        this.data = data;
        this.expanded = true;
    }

    public ApprovalFormatTreeNode(ApprovalFormat data, List<ApprovalFormatTreeNode> children) {
        this.data = data;
        this.children = children;
        this.expanded = true;
    }

    @Override
    public String toString() {
        return "ApprovalFormatTreeNode{" +
                "data=" + data +
                ", children=" + children +
                '}';
    }
}
