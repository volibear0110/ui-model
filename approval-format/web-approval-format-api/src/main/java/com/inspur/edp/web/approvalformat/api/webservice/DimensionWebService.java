/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.webservice;

import com.inspur.edp.bcc.billcategory.entity.dimension.BillCategoryDimension;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 维度 web 服务
 * @author Xu‘fa Wang
 * @date 2020/5/14 9:24
 */
@Path("/dimension")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface DimensionWebService {
    /**
     * 基于单据种类Id和单据种类扩展码获取维度
     * @param billCategoryId 单据种类Id
     * @param billCategoryExtendCode 单据种类扩展码
     * @return 单据种类维度实例
     */
    @GET
    @Path("/{billCategoryId}/{billCategoryExtendCode}")
    BillCategoryDimension getDimension(@PathParam("billCategoryId") String billCategoryId, @PathParam("billCategoryExtendCode")  String billCategoryExtendCode);

    /**
     * 基于单据种类Id获取维度集合
     * @param billCategoryId 单据种类Id
     * @return 单据种类维度实例集合
     */
    @GET
    @Path("/list/{billCategoryId}")
    List<BillCategoryDimension> getDimensionCollection(@PathParam("billCategoryId") String billCategoryId);
}
