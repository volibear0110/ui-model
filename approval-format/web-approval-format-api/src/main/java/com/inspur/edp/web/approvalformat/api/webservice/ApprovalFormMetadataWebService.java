/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalFormUpdateRequestBody;
import com.inspur.edp.web.approvalformat.api.entity.ApprovalViewObjectUpdateRequestBody;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;

/**
 * 审批单据元数据web服务
 * @author Xu‘fa Wang
 * @date 2020/5/17 17:54
 */
@Path("/metadata")
public interface ApprovalFormMetadataWebService {

    /**
     * 更新审批表单元数据（仅更新元数据内容）
     * @param approvalFormUpdateRequestBody 审批单据更新请求体
     */
    @PUT
    @Path("/")
    void updateApprovalFormMetadata(@RequestBody ApprovalFormUpdateRequestBody approvalFormUpdateRequestBody);

    /**
     * 更新审批视图模型元数据（仅更新元数据内容）
     * @param approvalViewObjectUpdateRequestBody 审批视图模型更新请求体
     */
    @PUT
    @Path("/viewmodel/")
    void updateApprovalViewModelMetadata(@RequestBody ApprovalViewObjectUpdateRequestBody approvalViewObjectUpdateRequestBody);

    /**
     * 基于id获取审批表单元数据内容
     * @param approvalFormMetadataId 审批单据id
     * @return 审批单据元数据内容
     */
    @GET
    @Path("/{formMetadataId}")
    FormMetadataContent getApprovalFormMetadataContent(@PathParam("formMetadataId") String approvalFormMetadataId);

    /**
     * 基于voContent创建表单Schema
     * @param voContent 视图对象内容
     * @return schema json串
     */
    @POST
    @Path("/schema")
    String createSchemaFromVo(@RequestBody JsonNode voContent);
}
