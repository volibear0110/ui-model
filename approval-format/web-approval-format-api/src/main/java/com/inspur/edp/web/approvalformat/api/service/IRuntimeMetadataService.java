/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.approvalformat.api.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

/**
 * 元数据运行时服务
 * @author Xu‘fa Wang
 * @date 2020/5/18 14:28
 */
public interface IRuntimeMetadataService {
    /**
     * 基于维度获取元数据
     * @param metadataId 元数据id
     * @param metadataCertificateId 元数据证书id
     * @param firstDimension 第一维度名称
     * @param secondDimension 第二维度名称
     * @return 获取的元数据实例
     */
    GspMetadata getMetadataByIdWithDimension(String metadataId,
                                             String metadataCertificateId,
                                             String firstDimension,
                                             String secondDimension);
}
