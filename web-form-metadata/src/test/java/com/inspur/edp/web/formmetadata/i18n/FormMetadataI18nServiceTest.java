/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class FormMetadataI18nServiceTest {

    //@Test
    void getResourceItem() {

        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String jsonContent = FileUtility.readAsString("D:\\InspurCode\\N转J之后代码\\web\\web-form-metadata\\src\\test\\java\\com\\inspur\\edp\\web\\formmetadata\\i18n\\form.json");
        FormDOM formDOM = SerializeUtility.getInstance().deserialize(jsonContent, FormDOM.class);
        ArrayList<HashMap<String, Object>> components = formDOM.getModule().getComponents();
        // 递归提取每个component中的组件及其对应的待国际化的值
        for (HashMap<String, Object> component : components) {
            // 每个component是一个树结构
            // 表单树表现出浅而宽的特性，所以优先使用深度优先算法
            if (component != null) {
                FormMetadataI18nService service = new FormMetadataI18nService();
                service.DepthFirstSearchTraverse(i18nResourceItemCollection, "", component, formDOM);
            }
        }
    }
}
