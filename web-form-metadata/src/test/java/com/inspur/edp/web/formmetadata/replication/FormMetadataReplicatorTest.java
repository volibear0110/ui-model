/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

public class FormMetadataReplicatorTest {
    private String source = "noah";


    @Test
    public void test() {
//        try {
//            String source = "";
//            this.testException();
//        } catch (Exception ex) {
//            source = "";
//            System.out.println("出现了异常 " + ex.getMessage());
//        }
//
//        System.out.println("当前的值为 " + source);

    }

    @Test
    public void testSubString() {
        String strNameSpace = "Inspur.GS.noahtest.runtimehelp.formcopy.formcopy.Front";
        String resource = "Inspur.GS.noahtest.runtimehelp.formcopy.formcopy.Front.formcopyForm.Form.root-component";
        System.out.println(resource.substring(strNameSpace.length()) + 1);

        String subStringResult = resource.substring(strNameSpace.length() + 1);
        String[] arr = subStringResult.split(java.util.regex.Pattern.quote("."), -1);
        System.out.println(arr.length);
        System.out.println(Arrays.toString(arr));

        System.out.println(StringUtils.join(arr,"."));

        String result = strNameSpace + StringUtils.join(arr, ".");
        System.out.println(result);

    }

    @Test
    public void testFileExtension() {
        String fileName = "formcopyForm.frm.en.lres";
        File file = new File(fileName);
        System.out.println(FileUtility.getFileExtension(fileName));
    }

    public void testException() {
        try {
            source = "noah try";
            int ii = 4 / 0;
        } catch (Exception ex) {
            source = "noah catch";
            throw new WebCustomException("", ex);
        } finally {
            source = "noah finally";
        }
    }
}
