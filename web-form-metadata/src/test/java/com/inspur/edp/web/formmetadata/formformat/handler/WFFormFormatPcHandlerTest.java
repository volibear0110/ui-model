package com.inspur.edp.web.formmetadata.formformat.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.formserver.viewmodel.formentity.*;
import com.inspur.edp.web.common.utility.JsonNodeUtility;
import io.iec.edp.caf.common.JSONSerializer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

public class WFFormFormatPcHandlerTest {

    private WFFormFormatPcHandler handler;
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        handler = new WFFormFormatPcHandler();
        objectMapper = new ObjectMapper(); // For parsing JSON strings into JsonNode
    }

    @Test
    public void testGetObjectData() throws Exception {
        // Sample JSON string representing the form content
//        String formContentJson = "";
//
//        // Parse the JSON string into a JsonNode
//        JsonNode formContent = objectMapper.readTree(formContentJson);
//        File jsonFile = new File("/Users/liuyuan/workspace/code/java/ui-model/web-form-metadata/src/test/java/com/inspur/edp/web/formmetadata/formformat/handler/formContent.json");
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        // 读取JSON文件并转换为JsonNode
//        JsonNode jsonNode = objectMapper.readTree(jsonFile);
//
//
//        VoFormModel voFormModel = new VoFormModel();
//
//        // Call the method under test
//        ObjectData result = handler.getObjectData(jsonNode);
//        List<ButtonGroup> buttonGroups = handler.getButtons(jsonNode);
//        List<MethodGroup> methodGroups = handler.getMethods(jsonNode);
//        voFormModel.setMethods(methodGroups);
//        voFormModel.setObjectAuthInfo(result);
//        voFormModel.setButtonGroup(buttonGroups);
//        System.out.println(JSONSerializer.serialize(voFormModel));
//
//        System.out.println("sss");
    }

    // Add more tests to cover different scenarios, such as empty JSON, missing nodes, etc.
}
