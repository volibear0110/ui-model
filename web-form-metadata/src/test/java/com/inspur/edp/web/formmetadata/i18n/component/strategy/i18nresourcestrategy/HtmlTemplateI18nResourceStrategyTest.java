/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlTemplateI18nResourceStrategyTest {

    private Pattern extractPattern = null;
    private Pattern specificPattern = null;

    public HtmlTemplateI18nResourceStrategyTest() {
        // 用于匹配{{}}结构的字符
        this.extractPattern = Pattern.compile("(?<=\\{\\{)[^}]*(?=\\}\\})");

        // 用于匹配 ${Title:"name"} 类似的结构
        this.specificPattern = Pattern.compile("\\$\\{([^\\}]+):([^\\}]+)\\}");
    }




    @Test
    public void testExtractPattern() {
        String source = "fshjkfshkjfsdhjfk{{'dsdsdsds'|lang:lang:'6666666'}}hfghfghgfhfgh";
        Matcher matcher = this.extractPattern.matcher(source);
        while (matcher.find()) {
            String i18nResourceExpress = matcher.group();
            int expressDelimiterIndex = i18nResourceExpress.indexOf("|");
            if (expressDelimiterIndex > 0) {
                String componentIdStr = i18nResourceExpress.substring(0, expressDelimiterIndex).trim();

                String[] componentIdStrArray = componentIdStr.split(java.util.regex.Pattern.quote("'"), -1);

                if (componentIdStrArray.length == 3) {
                    String componentId = componentIdStrArray[1];

                    System.out.println("componentId is: " + componentId);

                    String generatedComponentId = componentId;
                    System.out.println(generatedComponentId);
                    //I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, "", "");


                }
            }
        }
    }

    @Test
    public void extractAttributeI18nResourceItemCollection() {
    }

    @Test
    public void test() {
        String[] arr = "\"dedede\"".split(java.util.regex.Pattern.quote("\""), -1);
        System.out.println(arr.length);

        int index = "ffff|".indexOf("|");
        String suffix = "ffff|".substring(index + 1);
        System.out.println(suffix);
    }
}