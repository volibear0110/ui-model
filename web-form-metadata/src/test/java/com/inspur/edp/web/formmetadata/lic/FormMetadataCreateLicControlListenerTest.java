/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.lic;

import com.inspur.edp.web.common.entity.TerminalType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class FormMetadataCreateLicControlListenerTest {

    @Test
    public void metadataCreateControl() {
        List<ModuleWithMetadataType> MODULE_WITH_METADATA_TYPE_LIST = new ArrayList<>();
        ModuleWithMetadataType formModuleType = new ModuleWithMetadataType();
        formModuleType.setModuleType("WDP");
        formModuleType.getMetadataTypeList().add(TerminalType.PC.getMetadataType());
        MODULE_WITH_METADATA_TYPE_LIST.add(formModuleType);

        ModuleWithMetadataType mobileFormModuleType = new ModuleWithMetadataType();
        mobileFormModuleType.setModuleType("MDP");
        mobileFormModuleType.getMetadataTypeList().add(TerminalType.MOBILE.getMetadataType());
        MODULE_WITH_METADATA_TYPE_LIST.add(mobileFormModuleType);

        String metaType = "Form";
        Optional<ModuleWithMetadataType> filterModuleType = MODULE_WITH_METADATA_TYPE_LIST.stream().filter(t -> t.getMetadataTypeList().contains(metaType)).findFirst();
        assertTrue(filterModuleType.isPresent());
        assertEquals(filterModuleType.get().getModuleType(), "WDP");

        String mobileMetaType = "MobileForm";
        filterModuleType = MODULE_WITH_METADATA_TYPE_LIST.stream().filter(t -> t.getMetadataTypeList().contains(mobileMetaType)).findFirst();
        assertTrue(filterModuleType.isPresent());
        assertEquals(filterModuleType.get().getModuleType(), "MDP");
    }
}