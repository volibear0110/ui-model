/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlTemplateTest {

    @Test
    public void test() {
        String pattern = "\\$\\{([^\\}]+):([^\\}]+)\\}";
        String source = "<span class=\\\"f-title-icon f-text-orna-bill\\\">\\r\\n  <i class=\\\"f-icon f-icon-page-title-record\\\"></i>\\r\\n</span>\\r\\n<h4 class=\\\"f-title-text\\\">${Title1:'页面标题1'}</h4>${Title2:'页面标题22'}\\r\\n${Title3:'页面标题3'}<${Title4:'页面标题4'} div class=\\\"f-title-pagination\\\">\\r\\n  <span class=\\\"f-icon f-icon-arrow-w f-state-disabled\\\"></span>\\r\\n  <span class=\\\"f-icon f-icon-arrow-e\\\"></span>\\r\\n</div>";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(source);
        while (m.find()) {
            int groupCount = m.groupCount();
            System.out.println(groupCount);
            System.out.println(m.group(1));
            System.out.println(m.group(2));
        }


    }
}
