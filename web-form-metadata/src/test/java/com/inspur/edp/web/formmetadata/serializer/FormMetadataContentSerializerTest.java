/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

class FormMetadataContentSerializerTest {

    // @Test
    void serialize() {
        FormMetadataContent formMetadataContent = new FormMetadataContent();
        formMetadataContent.setCode("ggg");
        formMetadataContent.setId("fdfdfdf");


        JsonNode result = SerializeUtility.getInstance().valueToJson(formMetadataContent, PropertyNamingStrategy.UPPER_CAMEL_CASE);


    }

    public void test() throws ExecutionException, InterruptedException {
        Future<Void> future = CompletableFuture.supplyAsync(() -> {
            return null;
        });
        Future<Void> future1 = CompletableFuture.supplyAsync(() -> {
            Future<Void> future2 = CompletableFuture.supplyAsync(() -> {
                return null;
            });

            Future<Void> future3 = CompletableFuture.supplyAsync(() -> {
                return null;
            });

            try {
                future2.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new WebCustomException("", e);
            }
            try {
                future3.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new WebCustomException("", e);
            }
            return null;
        });


        future.get();
        future1.get();
    }


}
