/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.web.common.io.FileUtility;
import org.junit.Test;

import java.io.File;

public class FormMetadataCmdManagerTest {
    @Test
    public void test() {
        String source = "noahguozhiqinoah";
        String replaceResult = source.replace("noah", "郭志奇");
        System.out.println(replaceResult);

        File file = new File("dd/ffff.txt");
        System.out.println(file.getName());

        System.out.println(FileUtility.combine(new File("dd/dddsdsd/fff.txt").getParent(), file.getName()));
    }
}