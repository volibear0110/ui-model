/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadatamanager;

import lombok.Data;

import java.io.Serializable;

/**
 * 通过id获取元数据参数
 * 增加code、name、namespace的目的是为了更加友好的提示元数据找不到时的异常提示信息
 *
 * @author noah
 * 2023/7/8 19:06
 */
public class MetadataManagerParameter implements Serializable {
    private static final long serialVersionUID = -8388L;

    private MetadataManagerParameter() {
    }

    /**
     * 元数据id
     */
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private ExtendParameter extendParameter;

    public ExtendParameter getExtendParameter() {
        if (this.extendParameter == null) {
            this.extendParameter = new ExtendParameter();
        }
        return extendParameter;
    }

    public void setExtendParameter(ExtendParameter extendParameter) {
        this.extendParameter = extendParameter;
    }

    public static MetadataManagerParameter init(String id, String code, String name, String nameSpace) {
        MetadataManagerParameter metadataManagerParameter = new MetadataManagerParameter();
        metadataManagerParameter.setId(id);
        ExtendParameter extendParameter = ExtendParameter.init(code, name, nameSpace);
        metadataManagerParameter.setExtendParameter(extendParameter);
        return metadataManagerParameter;
    }

    /**
     * 扩展参数信息
     */
    @Data
    public static class ExtendParameter {
        /**
         * 元数据code
         */
        String code;
        /**
         * 元数据name
         */
        String name;
        /**
         * 元数据nameSpace
         */
        String nameSpace;

        private ExtendParameter() {
        }

        public static ExtendParameter init(String code, String name, String nameSpace) {
            ExtendParameter extendParameter = new ExtendParameter();
            extendParameter.setCode(code);
            extendParameter.setName(name);
            extendParameter.setNameSpace(nameSpace);
            return extendParameter;
        }
    }

}
