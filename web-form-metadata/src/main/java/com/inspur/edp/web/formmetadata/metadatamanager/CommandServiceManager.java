/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadatamanager;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.constant.I18nMsgConstant;
import com.inspur.edp.web.tsfile.api.service.TsFileService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.HashMap;

/**
 * 命令manager
 *
 * @author noah
 */
public class CommandServiceManager {
    private CommandServiceManager() {
    }

    /**
     * 从ts文件内容或ts所在构件元数据中读取文件内容
     *
     * @param sourcePath
     * @param serviceRef
     * @return
     */
    public static String getTypescriptFileContent(String sourcePath, String devRootPath, HashMap<String, Object> serviceRef, ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        try {
            String refCmpId = serviceRef.get("cmpId").toString();
            // 仅在设计时进行文件读取
            if (executeEnvironment.equals(ExecuteEnvironment.Design)) {
                try {
                    TsFileService fileService = SpringBeanUtils.getBean(TsFileService.class);
                    return fileService.loadTsFileContentByWebCmp(sourcePath, refCmpId);
                } catch (Exception ex) {
                    WebLogger.Instance.info("command ts file read failed,use cmp metadata! the cmpid is " + refCmpId, CommandServiceManager.class.getName());
                }
            }

            return getTsSourceFromMetaData(refCmpId, executeEnvironment, isUpdradeTool);

        } catch (Exception e) {
            WebLogger.Instance.error(e);
            return null;
        }
    }


    /**
     * 从元数据中读取ts内容  适用于跨工程引用表单的情况
     *
     * @param cmpId
     * @param executeEnvironment
     * @param isUpdradeTool
     * @return
     */
    private static String getTsSourceFromMetaData(String cmpId, ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        GspMetadata gspMetadata = MetadataUtility.getInstance().getMetadataWithEnvironment(() -> {
            MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
            getterMetadataInfo.setId(cmpId);
            getterMetadataInfo.setMetadataType(MetadataTypeEnum.TS);
            return getterMetadataInfo;
        }, null, ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0005, cmpId), executeEnvironment, isUpdradeTool);
        if (gspMetadata != null && !StringUtility.isNullOrEmpty(gspMetadata.getExtendProperty())) {
            CommandExtendProperty commandExtendProperty = SerializeUtility.getInstance().deserialize(gspMetadata.getExtendProperty(), CommandExtendProperty.class);
            if (commandExtendProperty != null) {
                return commandExtendProperty.getSourceCode();
            }
        }
        return "";
    }

    /**
     * 命令扩展
     *
     * @author noah
     */
    private static class CommandExtendProperty {
        private String sourceCode;

        public final String getSourceCode() {
            return sourceCode;
        }

        public final void setsourceCode(String value) {
            sourceCode = value;
        }
    }
}
