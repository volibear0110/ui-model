/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.module;

import com.inspur.edp.web.formmetadata.metadata.i18nsetting.I18nSetting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 模块
 */
public class Module {
    //id
    private String id;

    public final String getId() {
        return id;
    }

    public final void setId(String value) {
        id = value;
    }

    /**
     * 编码
     */
    private String code;

    public final String getCode() {
        return code;
    }

    public final void setCode(String value) {
        code = value;
    }

    /**
     * 名称
     */
    private String name;

    public final String getName() {
        return name;
    }

    public final void setName(String value) {
        name = value;
    }

    private String caption;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String creator;
    private String creationDate;
    private String updateVersion;
    private boolean showTitle;
    private String bootstrap;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdateVersion() {
        return updateVersion;
    }

    public void setUpdateVersion(String updateVersion) {
        this.updateVersion = updateVersion;
    }

    public boolean getShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public String getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(String bootstrap) {
        this.bootstrap = bootstrap;
    }

    private String templateId;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }


    private ArrayList<HashMap<String, Object>> schemas;

    public final ArrayList<HashMap<String, Object>> getSchemas() {
        return schemas;
    }

    public final void setSchemas(ArrayList<HashMap<String, Object>> value) {
        schemas = value;
    }

    private ArrayList<HashMap<String, Object>> stateMachines;

    public final ArrayList<HashMap<String, Object>> getStateMachines() {
        return stateMachines;
    }

    public final void setStateMachines(ArrayList<HashMap<String, Object>> value) {
        stateMachines = value;
    }

    private ArrayList<HashMap<String, Object>> viewmodels;

    public final ArrayList<HashMap<String, Object>> getviewmodels() {
        return viewmodels;
    }

    public final void setviewmodels(ArrayList<HashMap<String, Object>> value) {
        viewmodels = value;
    }

    private ArrayList<HashMap<String, Object>> components;

    public final ArrayList<HashMap<String, Object>> getComponents() {
        if (this.components == null) {
            this.components = new ArrayList<>();
        }
        return components;
    }

    public final void setComponents(ArrayList<HashMap<String, Object>> value) {
        components = value;
    }

    private ArrayList<HashMap<String, Object>> webcmds;

    public final ArrayList<HashMap<String, Object>> getWebcmds() {
        return webcmds;
    }

    public final void setWebcmds(ArrayList<HashMap<String, Object>> value) {
        webcmds = value;
    }


    private ArrayList<HashMap<String, Object>> states;

    public ArrayList<HashMap<String, Object>> getStates() {
        return states;
    }

    public void setStates(ArrayList<HashMap<String, Object>> states) {
        this.states = states;
    }

    private ArrayList<HashMap<String, Object>> contents;

    public ArrayList<HashMap<String, Object>> getContents() {
        return contents;
    }

    public void setContents(ArrayList<HashMap<String, Object>> contents) {
        this.contents = contents;
    }

    private List<HashMap<String, Object>> expressions;

    public List<HashMap<String, Object>> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<HashMap<String, Object>> expressions) {
        this.expressions = expressions;
    }

    private  String showType;

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    /**
     * 对应的工具栏信息
     */
    private HashMap<String, Object> toolbar;

    public final HashMap<String, Object> getToolbar() {
        return this.toolbar;
    }

    public final void setToolbar(HashMap<String, Object> toolbar) {
        this.toolbar = toolbar;
    }

    private ArrayList<HashMap<String, Object>> serviceRefs;

    public final ArrayList<HashMap<String, Object>> getServiceRefs() {
        if (this.serviceRefs == null) {
            this.serviceRefs = new ArrayList<>();
        }
        return serviceRefs;
    }

    public final void setServiceRefs(ArrayList<HashMap<String, Object>> value) {
        serviceRefs = value;
    }

    /**
     * 外部组件
     */
    private ArrayList<HashMap<String, Object>> externalComponents;

    public final ArrayList<HashMap<String, Object>> getExternalComponents() {
        return externalComponents;
    }

    public final void setExternalComponents(ArrayList<HashMap<String, Object>> value) {
        externalComponents = value;
    }

    /**
     * 国际化设置
     */
    private I18nSetting i18nSetting;

    public final I18nSetting getI18nSetting() {
        return i18nSetting;
    }

    public final void setI18nSetting(I18nSetting value) {
        i18nSetting = value;
    }


    // 自定义代码

    private List<HashMap<String, Object>> sourceCodeRef;

    public List<HashMap<String, Object>> getSourceCodeRef() {
        return sourceCodeRef;
    }

    public void setSourceCodeRef(List<HashMap<String, Object>> sourceCodeRef) {
        this.sourceCodeRef = sourceCodeRef;
    }


    private boolean isComposedFrm;

    public boolean getIsComposedFrm() {
        return isComposedFrm;
    }

    public void setIsComposedFrm(boolean isComposedFrm) {
        this.isComposedFrm = isComposedFrm;
    }

    public void setComposedFrm(boolean composedFrm) {
        isComposedFrm = composedFrm;
    }

    // 自定义代码
    private List<HashMap<String, Object>> extraImports;

    public List<HashMap<String, Object>> getExtraImports() {
        return extraImports;
    }

    public void setExtraImports(List<HashMap<String, Object>> extraImports) {
        this.extraImports = extraImports;
    }

    private boolean isMobileApprove;

    public boolean isMobileApprove() {
        return isMobileApprove;
    }

    public void setMobileApprove(boolean mobileApprove) {
        isMobileApprove = mobileApprove;
    }


    private String projectName;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    private Object declarations;

    public Object getDeclarations() {
        return declarations;
    }

    public void setDeclarations(Object declarations) {
        this.declarations = declarations;
    }

    private String metadataId;

    private String getMetadataId() {
        return this.metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    private HashMap<String, String> customClass;

    public HashMap<String, String> getCustomClass() {
        return customClass;
    }

    public void setCustomClass(HashMap<String, String> customClass) {
        this.customClass = customClass;
    }

    private List<HashMap<String, Object>> subscriptions;

    public List<HashMap<String, Object>> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<HashMap<String, Object>> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
