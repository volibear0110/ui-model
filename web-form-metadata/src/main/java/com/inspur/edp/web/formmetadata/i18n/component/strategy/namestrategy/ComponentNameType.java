/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy;

/**
 * 组件名称类型
 *
 * @author noah
 */
public class ComponentNameType {
    private ComponentNameType() {
    }

    /**
     * name
     */
    public static final String NAME = "name";
    /**
     * text
     */
    public static final String TEXT = "text";
    /**
     * title
     */
    public static final String TITLE = "title";
    /**
     * caption
     */
    public static final String CAPTION = "caption";
    /**
     * presetQuerySolutionName
     */
    public static final String PRESET_QUERY_SOLUTION_NAME = "presetQuerySolutionName";

    public static final String QUERY_NAME = "queryName";

    /**
     * 读取label属性
     */
    public static final String LABEL = "label";
}
