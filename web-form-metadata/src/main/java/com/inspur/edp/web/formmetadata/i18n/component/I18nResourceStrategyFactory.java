/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy.*;
import com.inspur.edp.web.formmetadata.i18n.constant.ComponentType;

import java.util.HashMap;

/**
 * 多语策略工厂
 */
public class I18nResourceStrategyFactory {

    private static final HashMap<String, AbstractI18nResourceStrategy> dicComponentStrategies = new HashMap<>();

    /**
     * 静态单例属性
     */
    private static volatile I18nResourceStrategyFactory _instance = null;

    private static final Object _lock = new Object();

    public static I18nResourceStrategyFactory getInstance() {
        if (_instance == null) {
            synchronized (_lock) {
                if (_instance == null) {
                    _instance = new I18nResourceStrategyFactory();
                }
            }
        }
        return _instance;
    }

    private I18nResourceStrategyFactory() {
        InitI18nResourceStrategyDictionary();
    }

    /**
     * 更新多语资源策略字典
     */
    private void InitI18nResourceStrategyDictionary() {
        // TODO: 进一步重构，将实例化策略的过程分离出去
        // 初始化意味着将很多的实例在启动时就进行了初始化  且不会释放
        dicComponentStrategies.put(ComponentType.TOOL_BAR_ITEM, new ToolBarItemI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.BUTTON, new ButtonI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.GRID_FIELD, new GridFieldI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.TREE_GRID_FIELD, new GridFieldI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.DATE_BOX, new DateBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.TEXT_BOX, new TextBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.CALENDAR, new CalendarI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.HTML_TEMPLATE, new HtmlTemplateI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.SECTION, new SectionI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.CHECK_GROUP, new CheckGroupI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.QUERY_SCHEME, new QuerySchemeI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.DATA_GRID, new DataGridI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.RADIO_GROUP, new RadioGroupI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.NUMERIC_BOX, new NumericBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.INPUT_GROUP, new InputGroupI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.MULTI_TEXT_BOX, new MultiTextBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.ENUM_FIELD, new EnumFieldI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.HELP_PROVIDER, new HelpProviderI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.TAG, new TagI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.TableTd, new TableTdI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.FileUploadPreview, new FileUploadPreviewI18nResourceStrategy());

        // 取消tags的国际化提取
        //dicComponentStrategies.Add(ComponentType.TAGS, new TagsI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.LIST_FILTER, new ListFilterI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.AppointmentCalendar, new AppointmentCalendarResourceStrategy());

        dicComponentStrategies.put(ComponentType.COMBOLIST, new ComboListI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.COMBOLOOKUP, new ComboLookupI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.LOOKUP, new LookupI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.LIST_NAV, new ListNavI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.SCROLL_SPY, new ScrollSpyI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.QUERY_FRAMEWORK, new QueryFrameworkI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.WIZARD, new WizardI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.TimePicker, new TimePickerI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.TimeSpinner, new TimeSpinnerI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.LanguageTextBox, new LanguageTextBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.DisplayField, new DefaultComponentI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.CheckBox, new DefaultComponentI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.TabToolbarItem, new DefaultComponentI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.DatePicker, new DatePickerI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.RichTextBox, new RichTextBoxI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.Image, new ImageI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.Avatar, new AvatarI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.MultiSelect, new DefaultComponentI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.NumberRange, new NumberRangeI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.NumberSpinner, new NumberSpinnerI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.SwitchField, new DefaultComponentI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.FieldSet, new FieldSetI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.Footer, new FooterI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.Header, new HeaderI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.PersonnelSelector, new PersonnelSelectorI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.OrganizationSelector, new OrganizationSelectorI18nResourceStrategy());

        dicComponentStrategies.put(ComponentType.ListView, new ListViewII18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.ExternalContainer, null);
        dicComponentStrategies.put(ComponentType.Steps, null);

        dicComponentStrategies.put(ComponentType.NavTab, new NavTabI18nResourceStrategy());
        dicComponentStrategies.put(ComponentType.ViewChange, new ViewChangeI18nResourceStrategy());
    }

    /**
     * 根据组件创建对应的多语策略
     */
    public final AbstractI18nResourceStrategy getI18nResourceStrategy(HashMap<String, Object> currentComponent, String i18nResourceBaseId) {
        // 获取组件的类型
        String componentType = ComponentUtility.getInstance().getType(currentComponent);

        if (StringUtility.isNullOrEmpty(componentType)) {
            return null;
        }


        AbstractI18nResourceStrategy componentI18nResourceStrategy = dicComponentStrategies.get(componentType);
        if (componentI18nResourceStrategy == null) {
            return new DefaultComponentI18nResourceStrategy();
        }
        return componentI18nResourceStrategy;
    }
}
