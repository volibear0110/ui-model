/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.service;


import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.api.FormMetadataCommonService;
import com.inspur.edp.web.formmetadata.api.entity.FormSuInfoEntity;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;
import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
import io.iec.edp.caf.businessobject.api.service.DevBasicInfoService;

import javax.annotation.Resource;

public class FormMetadataCommonServiceImpl implements FormMetadataCommonService {

    @Resource
    DevBasicInfoService devBasicInfoService;

    /**
     * 依据业务对象获取对应的配置信息
     *
     * @param bizObjId
     * @return
     */
    @Override
    public FormSuInfoEntity getSuInfoWithBizobjId(String bizObjId) {
        if (StringUtility.isNullOrEmpty(bizObjId)) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0016);
        }
        DevBasicBoInfo basicBoInfo = this.devBasicInfoService.getDevBasicBoInfo(bizObjId);
        if (basicBoInfo == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0017, new String[]{bizObjId});
        }
        FormSuInfoEntity formSuInfo = new FormSuInfoEntity();
        formSuInfo.setAppCode(basicBoInfo.getAppCode());
        formSuInfo.setSuCode(basicBoInfo.getSuCode());
        formSuInfo.setNameSpace(basicBoInfo.getBoNameSpace());
        return formSuInfo;
    }
}
