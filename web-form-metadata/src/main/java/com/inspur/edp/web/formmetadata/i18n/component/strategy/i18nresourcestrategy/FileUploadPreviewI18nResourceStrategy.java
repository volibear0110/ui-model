/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 附件上传、预览多语参数提取
 *
 * @author guozhiqi
 */
public class FileUploadPreviewI18nResourceStrategy extends AbstractI18nResourceStrategy {

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        I18nResourceItemCollection uploadSelectTextI18nResourceItemCollection = this.extractUploadSelectTextI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (uploadSelectTextI18nResourceItemCollection != null && uploadSelectTextI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(uploadSelectTextI18nResourceItemCollection);
        }

        I18nResourceItemCollection previewDefaultNameI18nResourceItemCollection = this.extractPreviewDefaultNameI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (previewDefaultNameI18nResourceItemCollection != null && previewDefaultNameI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(previewDefaultNameI18nResourceItemCollection);
        }

        I18nResourceItemCollection customInfo = this.extractCustomInfo(i18nResourceItemBaseId, currentComponent);
        if (!customInfo.isEmpty()) {
            i18nResourceItemCollection.addRange(customInfo);
        }

        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        ArrayList<HashMap<String, Object>> cols = ComponentUtility.getInstance().getValue(currentComponent, "previewColumns");
        if (cols != null && !cols.isEmpty()) {
            cols.forEach(field -> {
                String bindingField = ComponentUtility.getInstance().getValue(field, "bindingField");
                String value = ComponentUtility.getInstance().getValue(field, "title");
                String baseId = currentComponentType
                        + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId
                        + I18nResourceConstant.SECOND_LEVEL_DELIMITER + bindingField;
                String generatedComponentId = baseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "title";
                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, value, value);
                i18nResourceItemCollection.add(i18nResourceItem);

                // 提取枚举值资源项
                ArrayList<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumData(field);
                if (enumValueCollection.size() > 0) {
                    // 从enumData中提取多语资源项
                    String enumDataAttributeName = ComponentUtility.getInstance().GetEnumDataName(currentComponent);
                    I18nResourceItemCollection enumDataI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, baseId, enumValueCollection, enumDataAttributeName, "value", "name");
                    if (enumDataI18nResourceItemCollection.size() > 0) {
                        i18nResourceItemCollection.addRange(enumDataI18nResourceItemCollection);
                    }
                }

                // 提取布尔值资源项
                HashMap<String, Object> formatter = ComponentUtility.getInstance().getValue(field, "formatter");
                String formatterType = ComponentUtility.getInstance().getValue(formatter, "type");
                if ("boolean".equals(formatterType)) {
                    String baseKey = baseId
                            + I18nResourceConstant.SECOND_LEVEL_DELIMITER
                            + "formatter"
                            + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
                    String trueTextVal = ComponentUtility.getInstance().getValue(formatter, "trueText");
                    String trueTextKey = baseKey + "trueText";
                    I18nResourceItem trueTextI18nItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, trueTextKey, trueTextVal, trueTextVal);
                    i18nResourceItemCollection.add(trueTextI18nItem);

                    String falseTextVal = ComponentUtility.getInstance().getValue(formatter, "falseText");
                    String falseTextKey = baseKey + "falseText";
                    I18nResourceItem falseTextI18nItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, falseTextKey, falseTextVal, falseTextVal);
                    i18nResourceItemCollection.add(falseTextI18nItem);
                }
            });
        }
        return i18nResourceItemCollection;
    }

    /**
     * 提取groupText属性中多语资源项
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection extractUploadSelectTextI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String uploadSelectTextValue = ComponentUtility.getInstance().getUploadSelectText(currentComponent);
        String uploadSelectTextName = "uploadSelectText";

        String componentId = uploadSelectTextName;
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, uploadSelectTextValue, uploadSelectTextValue);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);


        return i18nResourceItemCollection;
    }


    private I18nResourceItemCollection extractPreviewDefaultNameI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String uploadSelectTextValue = ComponentUtility.getInstance().getPreviewDefaultRename(currentComponent);
        String uploadSelectTextName = "previewDefaultRename";

        String componentId = uploadSelectTextName;
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, uploadSelectTextValue, uploadSelectTextValue);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractCustomInfo(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String customInfo = ComponentUtility.getInstance().getAttachCustomInfo(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "customInfo";
        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, customInfo, customInfo);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        return i18nResourceItemCollection;
    }
}
