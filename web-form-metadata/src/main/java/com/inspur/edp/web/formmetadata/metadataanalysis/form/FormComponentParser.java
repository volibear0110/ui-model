/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadataanalysis.form;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 表单组件解析器
 * @author  noah
 */
public class FormComponentParser {
    private FormComponentParser() {

    }

    private static final FormComponentParser formComponentParser = new FormComponentParser();

    public static FormComponentParser getInstance() {
        return formComponentParser;
    }

    /**
     * 提取组件信息
     */
    public final void ExtractComponent(FormDOM json, String metadataFileName, String targetStoragePath, String webDevPath) {
        if (json == null || json.getModule() == null || json.getModule().getComponents() == null || json.getModule().getComponents().size() == 0) {
            return;
        }
        String serilizedPageCollectionStr = SerilizePageCollection(json.getModule().getComponents());
        if (StringUtility.isNullOrEmpty(serilizedPageCollectionStr)) {
            return;
        }

        String internalRoutes = "{" +
                serilizedPageCollectionStr +
                "}";

        FileUtility.writeFile(targetStoragePath, String.format("%1$s%2$s", metadataFileName.toLowerCase(), JITEngineConstants.ProjectRouteFileExtension), internalRoutes);
    }

    private String SerilizePageCollection(ArrayList<HashMap<String, Object>> componentCollection) {
        if (componentCollection == null || componentCollection.size() == 0) {
            return null;
        }

        StringBuilder pages = new StringBuilder();
        pages.append("\"pages\":");
        pages.append("[");

        int index = 0;
        for (HashMap<String, Object> component : componentCollection) {
            if (component.containsKey("route")) {
                if (index > 0) {
                    pages.append(",");
                }
                pages.append(SerializeUtility.getInstance().serialize(component.get("route"),false));
                index++;
            }
        }

        pages.append("]");
        return pages.toString();
    }

    ///// <summary>
    ///// 将Route对象序列化成json串
    ///// TODO：找到将Dictionary转换成实体类的方法并统一替换
    ///// </summary>
    ///// <param name="routeObject"></param>
    ///// <returns></returns>
    //private static string serilizeRouteObject(Dictionary<string, object> routeObject)
    //{
    //    StringBuilder routeStringBuilder = new StringBuilder();
    //    routeStringBuilder.Append("{");
    //    if (routeObject.ContainsKey("id"))
    //    {
    //        routeStringBuilder.Append("\"id\":" + "\"" + routeObject["id"] + "\"" + ",");
    //    }

    //    if (routeObject.ContainsKey("uri"))
    //    {
    //        routeStringBuilder.Append("\"uri\":" + "\"" + routeObject["uri"] + "\"" + ",");
    //    }

    //    if (routeObject.ContainsKey("name"))
    //    {
    //        routeStringBuilder.Append("\"name\":" + "\"" + routeObject["name"] + "\"");
    //    }

    //    // TODO: 列表还需单独处理
    //    //routeObject["params"]

    //    routeStringBuilder.Append("}");

    //    return routeStringBuilder.ToString();
    //}

    //private Dictionary<string, object> ConvertToDictionaryObject(object targetObject)
    //{
    //    string json = JsonConvert.SerializeObject(targetObject);
    //    Dictionary<string, object> convertedObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
    //    return convertedObject;
    //}
}
