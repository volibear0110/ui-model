/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.webservice;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.api.FormMetadataCommonService;
import com.inspur.edp.web.formmetadata.api.FormMetadataWebService;
import com.inspur.edp.web.formmetadata.api.dto.FormRelateMetadataInDesignParameterDto;
import com.inspur.edp.web.formmetadata.api.dto.FormRelateMetadataInDesignResultDto;
import com.inspur.edp.web.formmetadata.api.entity.FormSuInfoEntity;
import com.inspur.edp.web.formmetadata.api.entity.ReplicateFormRequestBody;
import com.inspur.edp.web.formmetadata.api.entity.SuInfoWithBizobjIdEntity;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;
import com.inspur.edp.web.formmetadata.service.FormMetadataService;
import com.inspur.edp.web.formmetadata.service.FormRelateMetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;

import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/15
 */
public class FormMetadataWebServiceImpl implements FormMetadataWebService {
    /**
     * 同步语言包到表单元数据
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Override
    public void synchronizeLanuagePackage(String fileName, String path) {

    }

    /**
     * 导出表单元数据中语言包
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Override
    public void exportLanguagePackage(String fileName, String path) {

    }

    /**
     * 同步本地语言包到表单元数据
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Override
    public void synchronizeLocalLanuagePackage(String fileName, String path) {

    }

    /**
     * 获取启用的语种
     */
    @Override
    public List<EcpLanguage> getEnabledLanguageList() {
        return FormMetadataService.getBeanInstance().getEnabledLanguageList();
    }

    /**
     * 获取系统内置的语种
     */
    @Override
    public List<EcpLanguage> getBuiltinLanguageList() {
        return FormMetadataService.getBeanInstance().getBuiltinLanguageList();
    }

    /**
     * 获取所有语种的列表
     */
    @Override
    public List<EcpLanguage> getAllLanguageList() {
        return FormMetadataService.getBeanInstance().getAllLanguageList();
    }

    /**
     * 复制表单及关联元数据
     */
    @Override
    public ResultMessage<String> replicateForm(ReplicateFormRequestBody replicateFormRequestBody) {
        ResultMessage<String> resultMessage = FormMetadataService.getBeanInstance().replicateForm(replicateFormRequestBody.getSourceMetadataId(), replicateFormRequestBody.getSourceMetadataRelativePath(), replicateFormRequestBody.getTargetMetadataCode(), replicateFormRequestBody.getTargetMetadataName(), replicateFormRequestBody.getTargetProjectName());
        return resultMessage;
    }

    @Override
    public FormSuInfoEntity getSuInfoWithBizobjId(SuInfoWithBizobjIdEntity bizobjIdEntity) {
        FormMetadataCommonService formMetadataCommonService = SpringBeanUtils.getBean(FormMetadataCommonService.class);
        return formMetadataCommonService.getSuInfoWithBizobjId(bizobjIdEntity.getBizObjId());
    }

    /**
     * 获取表单关联元数据信息
     * 设计时元数据文件名称及路径参数不能为空
     * 运行时元数据id不能为空
     *
     * @param parameter
     * @return
     */
    @Override
    public List<FormRelateMetadataInDesignResultDto> getFormRelateMetadata(FormRelateMetadataInDesignParameterDto parameter) {
        if (parameter == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0022);
        }

        if (parameter.getEnvironment().equals(ExecuteEnvironment.Design)) {
            if (StringUtility.isNullOrEmpty(parameter.getFormMetadataFileName()) || StringUtility.isNullOrEmpty(parameter.getFormMetadataPath())) {
                throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0023);
            }
        }

        if (parameter.getEnvironment().equals(ExecuteEnvironment.Runtime)) {
            if (StringUtility.isNullOrEmpty(parameter.getFormMetadataId())) {
                throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0024);
            }
        }

        FormRelateMetadataService formRelateMetadataService = SpringBeanUtils.getBean(FormRelateMetadataService.class);
        return formRelateMetadataService.getFormRelateMetadata(parameter);
    }
}

