/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.formmetadata.entity.FormRelateViewModelEntity;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContentService;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单元数据rtservice
 */
public class FormMetadataRTService {
    /**
     * 单个viewModel 获取关联的表单元数据
     *
     * @param viewModelMetadata
     * @return
     */
    public List<FormRelateViewModelEntity> checkFormRelateToViewModel(GspMetadata viewModelMetadata) {

        if (viewModelMetadata == null) {
            return null;
        }
        String relativePath = viewModelMetadata.getRelativePath();
        ArrayList<String> filterType = new ArrayList<>();
        filterType.add(".frm");

        List<GspMetadata> formMetadataList = MetadataUtility.getInstance().getMetadataListWithDesign(relativePath, filterType);
        if (formMetadataList == null || formMetadataList.size() == 0) {
            return null;
        }
        List<FormRelateViewModelEntity> formRelateViewModelEntityList = new ArrayList<>();
        String viewModelId = viewModelMetadata.getHeader().getId();

        formMetadataList.forEach(formMetadata -> {
            // 如果对应的表单元数据内容为空 那么根据id重新获取对应元数据
            if (formMetadata.getContent() == null) {
                MetadataGetterParameter metadataGetterParameter = MetadataGetterParameter.getNewInstance(formMetadata.getHeader().getId(), formMetadata.getRelativePath(), MetadataTypeEnum.Frm);
                metadataGetterParameter.setSourceMetadata(viewModelMetadata, MetadataTypeEnum.ViewModel);
                formMetadata = MetadataUtility.getInstance().getMetadataWithDesign(metadataGetterParameter);
            }
            FormMetadataContentService.ParameterFromMetadataResult formParameter = FormMetadataContentService.getInstance().getParameterFromFormMetadata(formMetadata);
            // 为了应对一个viewModel可能对应多个表单的情况
            if (formParameter != null && viewModelId.equals(formParameter.getVoId())) {
                FormRelateViewModelEntity formRelateViewModelEntity = new FormRelateViewModelEntity();
                formRelateViewModelEntity.setFormMetadataId(formParameter.getMetadataId());
                formRelateViewModelEntity.setFormMetadata(formMetadata);
                formRelateViewModelEntity.setFormCode(formParameter.getFormCode());
                formRelateViewModelEntity.setFormName(formParameter.getFormName());
                formRelateViewModelEntity.setViewModelMetadataId(formParameter.getMetadataId());
                formRelateViewModelEntity.setViewModelMetadata(viewModelMetadata);
                formRelateViewModelEntityList.add(formRelateViewModelEntity);
            }

        });
        return formRelateViewModelEntityList;
    }

    /**
     * viewModel元数据集合获取对应的表单元数据列表
     *
     * @param viewModelMetadataList
     * @return
     */
    public List<FormRelateViewModelEntity> checkFormRelateToViewModelList(List<GspMetadata> viewModelMetadataList) {
        List<FormRelateViewModelEntity> formRelateViewModelEntities = new ArrayList<>();
        if (viewModelMetadataList == null || viewModelMetadataList.size() == 0) {
            return formRelateViewModelEntities;
        }
        viewModelMetadataList.forEach(t -> {
            List<FormRelateViewModelEntity> formRelateViewModelEntityListFromViewModel = checkFormRelateToViewModel(t);
            if (formRelateViewModelEntityListFromViewModel != null && formRelateViewModelEntityListFromViewModel.size() > 0) {
                formRelateViewModelEntities.addAll(formRelateViewModelEntityListFromViewModel);
            }

        });
        return formRelateViewModelEntities;
    }
}
