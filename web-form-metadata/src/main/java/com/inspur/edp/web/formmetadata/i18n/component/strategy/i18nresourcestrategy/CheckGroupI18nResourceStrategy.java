/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 复选框组 国际化参数提取
 * @author  noah
 */
public class CheckGroupI18nResourceStrategy extends AbstractI18nResourceStrategy {
    @Override
    protected String getComponentName(HashMap<String, Object> currentComponent) {
        // 读取组件名称
        // 从title属性中获取组件名称
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        return componentNameStrategy.getComponentName(currentComponent);
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        // 从html属性中提取多语字段
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        I18nResourceItemCollection itemsI18nResourceItemCollection = extractComponentItemsI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (itemsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(itemsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractComponentItemsI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从items属性中提取多语资源
        ArrayList<HashMap<String, Object>> childComponents = ComponentUtility.getInstance().GetItems(currentComponent);
        I18nResourceItemCollection childComponentsI18nResourceItemCollection = exetractChildComponentsI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent, childComponents);
        if (childComponentsI18nResourceItemCollection != null && childComponentsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(childComponentsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection exetractChildComponentsI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, ArrayList<HashMap<String, Object>> childComponentList) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtils.isEmpty(currentComponentType) || StringUtils.isEmpty(currentComponentType)) {
            return null;
        }

        String strValueFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.ValueFieldKey,"");
        String strTextFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.TextFieldKey,"");

        for (HashMap<String, Object> childComponent : childComponentList) {
            // 属性为value的值
            String valueAtributeValue = ComponentUtility.getInstance().getValueWithKey(childComponent, strValueFieldKey, "value");
            String nameAtributeValue = ComponentUtility.getInstance().getValueWithKey(childComponent, strTextFieldKey, "name");
            if (!StringUtils.isEmpty(valueAtributeValue)) {
                // 使用 "/"作为分隔符
                String generatedComponentId = currentComponentType + "/" + currentComponentId + "/" + valueAtributeValue;

                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
                this.addInCollection(i18nResourceItemCollection,i18nResourceItem);
            }
        }

        return i18nResourceItemCollection;
    }
}
