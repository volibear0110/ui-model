package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.ArrayList;
import java.util.HashMap;

public class MobileNavComponentI18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    public MobileNavComponentI18nResourceStrategy() {
        super();
    }

    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> currentComponent, FormDOM formDOM) {
        this.init(baseId, currentComponent, formDOM);

        switch (this.componentType) {
            case ComponentTypes.ScrollNavbar:
                this.collectScrollNavbarResourceItems();
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集ScrollNavbar相关的资源项
     */
    private void collectScrollNavbarResourceItems() {
        final String ATTRIBUTE_NAME = "scrollItems";
        String itemPathPrefix = this.getComponentAttrPath(ATTRIBUTE_NAME);
        ArrayList<HashMap<String, Object>> scrollItems = this.componentUtility.GetCollectionAttributeByName(this.component, ATTRIBUTE_NAME);
        final String idField = "id";
        final String textField = "title";
        for (HashMap<String, Object> item : scrollItems) {
            String itemId = this.getComponentAttr(item, idField);
            String itemText = this.getComponentAttr(item, textField);
            String itemPath = itemPathPrefix + "/" + itemId;
            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemText, itemText);
            this.resourceItemCollection.add(resourceItem);
        }
    }

}
