package com.inspur.edp.web.formmetadata.formformat.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.customize.form.api.RuntimeCheckVoFormVariableService;
import com.inspur.edp.customize.service.VoRuntimeBuildFormFormatService;
import com.inspur.edp.formserver.viewmodel.formentity.*;
import com.inspur.edp.formserver.vmapi.formconfig.VoBuildFormFormatService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.web.formmetadata.formformat.FormFormatHandler;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
import io.iec.edp.caf.businessobject.api.service.DevBasicInfoService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;
import org.springframework.security.web.header.Header;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * wf表单格式化handler
 * 适配工作流的提取处理
 */
public class WFFormFormatPcHandler extends FormFormatHandler {
    @Override
    public String getFormType() {
        return "PC";
    }

    @Override
    public List<ButtonGroup> getButtons(JsonNode formContent) {
        JsonNode components = formContent.at("/module/components");
        List<ButtonGroup> buttonGroups = new ArrayList<>();
        components.forEach(component -> {
            extractButtons(null, buttonGroups, component, null);
        });
        return buttonGroups;
    }

    public void extractButtons(JsonNode parentNode, List<ButtonGroup> buttonGroups, JsonNode component, JsonNode grandParentNode) {
        if (component != null && !component.isEmpty()) {
            if (component.isArray()) {
                List<Button> btnList = new ArrayList<>();
                boolean isButtonArray = false;
                List<JsonNode> otherNodes = new ArrayList<>();
                for (int i = 0; i < component.size(); i++) {
                    JsonNode button = component.get(i);
                    JsonNode type = button.get("type");
                    if (type != null) {
                        String typeText = type.asText();
                        if ("Button".equals(typeText)
                                || "ToolBarItem".equals(typeText)
                                || "SectionToolbarItem".equals(typeText)
                                || "TabToolbarItem".equals(typeText)) {
                            Button formButton = generateButton(button);
                            isButtonArray = true;
                            if (formButton != null) {
                                btnList.add(formButton);
                            }
                        } else {
                            // 处理非button节点
                            otherNodes.add(button);
                        }
                    }
                }
                if (isButtonArray) {
                    ButtonGroup buttonGroup = new ButtonGroup();
                    String name = getFieldName(component);
                    String id = "";
                    if (parentNode.get("id") != null) {
                        id = parentNode.get("id").asText();
                    } else if (grandParentNode.get("id") != null) {
                        id = grandParentNode.get("id").asText();
                    }
                    if (StringUtils.isEmpty(name)){
                        name = getFieldName(parentNode);
                    }
                    if (StringUtils.isEmpty(name)){
                        name = getFieldName(grandParentNode);
                        if (StringUtils.isEmpty(name)) {
                            JsonNode parentType = parentNode.get("type");
                            if (parentType != null) {
                                String componentTypeText = parentType.asText();
                                if ("Sidebar".equals(componentTypeText)) {
                                    name = "侧边栏";
                                } else if ("ToolBar".equals(componentTypeText)) {
                                    name = "工具栏";
                                }
                            }
                        }
                    }
                    name += "[" + id + "]";
                    buttonGroup.setId(id);
                    buttonGroup.setName(name);
                    buttonGroup.setCode(id);
                    buttonGroup.setButtons(btnList);
                    buttonGroups.add(buttonGroup);
                    // 当前按钮组包含其他节点，遍历其他节点
                    otherNodes.forEach(node -> {
                        JsonNode contents = node.get("contents");
                        JsonNode items = node.get("items");
                        JsonNode toolbar = node.get("toolbar");
                        extractButtons(node, buttonGroups, contents, parentNode);
                        extractButtons(node, buttonGroups, items, parentNode);
                        extractButtons(node, buttonGroups, toolbar, parentNode);
                    });
                } else {
                    component.forEach(node -> {
                        JsonNode contents = node.get("contents");
                        JsonNode items = node.get("items");
                        JsonNode toolbar = node.get("toolbar");
                        extractButtons(node, buttonGroups, contents, parentNode);
                        extractButtons(node, buttonGroups, items, parentNode);
                        extractButtons(node, buttonGroups, toolbar, parentNode);
                    });
                }
            } else {
                JsonNode contents = component.get("contents");
                JsonNode items = component.get("items");
                JsonNode toolbar = component.get("toolbar");
                extractButtons(component, buttonGroups, contents, parentNode);
                extractButtons(component, buttonGroups, items, parentNode);
                extractButtons(component, buttonGroups, toolbar, parentNode);
            }
        }
    }

    private Button generateButton(JsonNode button) {
        Button formButton = null;
        List<String> configAbleAttrs = getConfigAbleAttrs(button);
        if (!configAbleAttrs.isEmpty()) {
            formButton = new Button();
            formButton.setId(button.get("id").asText());
            formButton.setCode(button.get("id").asText());
            formButton.setName(getFieldName(button));
            formButton.setConfigurableAttrs(configAbleAttrs);
            JsonNode contents = button.get("contents");
            JsonNode items = button.get("items");
            setChildButtons(formButton, contents);
            setChildButtons(formButton, items);
        }
        return formButton;
    }

    private static List<String> getConfigAbleAttrs(JsonNode node) {
        List<String> configAbleAttrs = new ArrayList<>();
        if (node != null) {
            setConfigAbleAttrs(node.get("visibleControlledByRules"), "Visible", configAbleAttrs);
            setConfigAbleAttrs(node.get("disableControlledByRules"), "Disable", configAbleAttrs);
            setConfigAbleAttrs(node.get("readonlyControlledByRules"), "Readonly", configAbleAttrs);
            setConfigAbleAttrs(node.get("requireControlledByRules"), "Required", configAbleAttrs);
        }
        return configAbleAttrs;
    }

    private void setChildButtons(Button button, JsonNode nodes) {
        if (nodes != null) {
            List<Button> buttons = new ArrayList<>();
            nodes.forEach(item -> {
                JsonNode type = item.get("type");
                if (type != null) {
                    String typeText = type.asText();
                    if ("Button".equals(typeText)
                            || "ToolBarItem".equals(typeText)
                            || "SectionToolbarItem".equals(typeText)
                            || "TabToolbarItem".equals(typeText)) {
                        Button formButton = generateButton(item);
                        if (formButton != null) {
                            buttons.add(formButton);
                        }

                    }
                }
            });
            button.setChildFields(buttons);
        }
    }

    @Override
    public ObjectData getObjectData(JsonNode formContent) {
        // 提取viewmodel中的字段
        JsonNode viewModels = formContent.at("/module/viewmodels");
        List<String> fieldIds = new ArrayList<>();
        viewModels.forEach(viewModel -> {
            JsonNode fields = viewModel.get("fields");
            if (fields != null && !fields.isEmpty()) {
                fieldIds.addAll(fields.findValuesAsText("id"));
            }
        });
        JsonNode schema = formContent.at("/module/schemas/0");
        if (schema == null || schema.isEmpty()) {
            return null;
        }

        // 获取component中字段信息
        JsonNode components = formContent.at("/module/components");
        Map<String, ChildData> fieldMap = new HashMap<>();
        extractComponents(components, fieldMap, fieldIds);

        // 构建实体树
        JsonNode mainEntity = schema.at("/entities/0");
        ObjectData root = new ObjectData();
        extractEntities(mainEntity, root, fieldMap);
        if ((root.getChildObjects() == null || root.getChildObjects().isEmpty()) &&
                (root.getElements() == null || root.getElements().isEmpty())) {
            // 如果主表没有任何字段，并且子表不可设置，则返回null
            root = null;
        }
        return root;
    }

    @Override
    public VoFormModel getVoFormModel(GspMetadata formMetadata, String formatType, String basePath) {
        VoFormModel voFormModel = new VoFormModel();
        MetadataHeader header = formMetadata.getHeader();
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();
        JsonNode content = formContents.getContents();

        voFormModel.setId(header.getId());
        voFormModel.setCode(header.getCode());
        voFormModel.setName(header.getName());
        voFormModel.setTerminal(getFormType());
        voFormModel.setFormatType(formatType);
        voFormModel.setObjectAuthInfo(getObjectData(content));
        voFormModel.setMethods(getMethods(content));
        voFormModel.setButtonGroup(getButtons(content));
        voFormModel.setProjectPath(basePath);
        generateFormFormat(voFormModel, formMetadata, basePath);
        return voFormModel;
    }

    @Override
    public void pushFormFormat(GspMetadata formMetadata, String formatType, String formPath) {
        MetadataProjectService projectService = SpringBeanUtils.getBean(MetadataProjectService.class);
        Path path = Paths.get(formPath);
        Path parentPath = path.getParent();
        String formPathExcludeFileName = parentPath != null ? parentPath.toString() : "";
        String projectPath = projectService.getProjPath(formPathExcludeFileName);
        VoBuildFormFormatService formFormatService = SpringBeanUtils.getBean(VoBuildFormFormatService.class);
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();
        JsonNode content = formContents.getContents();
        String voId = content.at("/module/schemas/0/id").textValue();
        formFormatService.buildFormFormat(getVoFormModel(formMetadata, formatType, projectPath), voId, formMetadata.getHeader().getBizobjectID());
    }

    @Override
    public void pushRtcFormFormat(GspMetadata formMetadata, String formatType, DimensionExtendEntity dimension) {
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();
        JsonNode content = formContents.getContents();
        String voId = content.at("/module/schemas/0/id").textValue();
        RuntimeCheckVoFormVariableService runtimeCheckVoFormVariableService = SpringBeanUtils.getBean(RuntimeCheckVoFormVariableService.class);
        boolean isPushRtcForm = runtimeCheckVoFormVariableService.checkVoFormVariable(voId);
        if (!isPushRtcForm) {
            return;
        }
        VoRuntimeBuildFormFormatService voRuntimeBuildFormFormatService = SpringBeanUtils.getBean(VoRuntimeBuildFormFormatService.class);
        VoFormModel voFormModel = getVoFormModel(formMetadata, formatType, null);
        String dim1 = org.apache.commons.lang3.StringUtils.isNotBlank(dimension.getFirstDimension())?dimension.getFirstDimension():"public";
        String dim2 = org.apache.commons.lang3.StringUtils.isNotBlank(dimension.getSecondDimension())?dimension.getSecondDimension():"public";

        generateParameters(voFormModel.getUrlParameters(), "dim1", "维度一", dim1);
        generateParameters(voFormModel.getUrlParameters(), "dim2", "维度二", dim2);
        generateParameters(voFormModel.getUrlParameters(), "metadataId", "元数据id", dimension.getBasicMetadataId());

        // 运行时定制没有basePath,传null
        voRuntimeBuildFormFormatService.buildFormFormat(voFormModel, dimension, voId, formMetadata.getHeader().getBizobjectID());
    }

    public List<MethodGroup> getMethods(JsonNode formContent) {
        // 提取viewmodel中的字段
        JsonNode viewModels = formContent.at("/module/viewmodels");
        JsonNode components = formContent.at("/module/components");
        String viewModelId = "";
        for (int i = 0; i < components.size(); i++) {
            JsonNode component = components.get(i);
            JsonNode type = component.get("componentType");
            if (type != null && type.asText().equals("Frame")) {
                viewModelId = component.get("viewModel").asText();
                break;
            }
        }
        List<MethodGroup> methodGroups = new ArrayList<>();
        for (int i = 0; i < viewModels.size(); i++) {
            JsonNode viewModel = viewModels.get(i);
            JsonNode idNode = viewModel.get("id");
            if (idNode != null) {
                String id = idNode.asText();
                if (id.equals(viewModelId)) {
                    JsonNode methods = viewModel.get("commands");
                    if (methods != null && !methods.isEmpty()) {
                        methods.forEach(method -> {
                            MethodGroup methodGroup = new MethodGroup();
                            methodGroup.setId(method.get("id").asText());
                            methodGroup.setMethodName(method.get("name").asText());
                            methodGroup.setMethodCode(method.get("code").asText());
                            JsonNode methodParams = method.get("params");
                            if (methodParams != null && !methodParams.isEmpty()) {
                                List<MethodParam> methodParamsList = new ArrayList<>();
                                methodParams.forEach(param -> {
                                    MethodParam methodParam = new MethodParam();
                                    methodParam.setCode(param.get("name").asText());
                                    methodParam.setName(param.get("shownName").asText());
                                    methodParam.setValue(null);
                                    methodParamsList.add(methodParam);
                                });
                                methodGroup.setMethodParam(methodParamsList);
                            }
                            methodGroups.add(methodGroup);
                        });
                    }
                }
            }
        }
        // 新增wf表单必填校验
        MethodGroup wfRequiredVerification = new MethodGroup();
        wfRequiredVerification.setId("wf-required-verification");
        wfRequiredVerification.setMethodCode("wf-required-verification");
        wfRequiredVerification.setMethodName("表单必填校验");
        methodGroups.add(wfRequiredVerification);
        return methodGroups;
    }

    private void extractComponents(JsonNode components, Map<String, ChildData> fieldMap, List<String> fieldIds) {
        for (int i = 0; i < components.size(); i++) {
            JsonNode component = components.get(i);
            JsonNode editorComponent = component.get("editor");
            if (extracted(fieldMap, fieldIds, component)) continue;
            if (extracted(fieldMap, fieldIds, editorComponent)) continue;

            if (component.get("contents") != null && !component.get("contents").isEmpty()) {
                extractComponents(component.get("contents"), fieldMap, fieldIds);
            }
            if (component.get("fields") != null && !component.get("fields").isEmpty()) {
                extractComponents(component.get("fields"), fieldMap, fieldIds);
            }
            if (component.get("rows") != null && !component.get("rows").isEmpty()) {
                extractComponents(component.get("rows"), fieldMap, fieldIds);
            }
            if (component.get("columns") != null && !component.get("columns").isEmpty()) {
                extractComponents(component.get("columns"), fieldMap, fieldIds);
            }
        }
    }

    private boolean extracted(Map<String, ChildData> fieldMap, List<String> fieldIds, JsonNode component) {
        if (component != null) {
            JsonNode binding = component.get("binding");
            if (binding != null) {
                JsonNode fieldId = binding.get("field");
                if (fieldId != null) {
                    String fieldIdText = fieldId.asText();
                    if (fieldIds.contains(fieldIdText)) {
                        ChildData fieldData = new ChildData();
                        List<String> configAbleAttrs = getConfigAbleAttrs(component);
                        List<String> editorConfigAbleAttrs = getConfigAbleAttrs(component.get("editor"));
                        // 取并集
                        Set<String> combinedSet = new HashSet<>();
                        combinedSet.addAll(configAbleAttrs);
                        combinedSet.addAll(editorConfigAbleAttrs);

                        // 将并集结果转换回List
                        List<String> combinedList = new ArrayList<>(combinedSet);
                        fieldData.setConfigurableAttrs(combinedList);
                        if (!combinedList.isEmpty()) {
                            // 获取字段名称
                            fieldData.setName(getFieldName(component));
                            fieldMap.put(fieldIdText, fieldData);
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

//    @Override
//    public List<AttachmentGroup> getAttachmentGroups(JsonNode formContent) {
//        // 获取component中字段信息
//        JsonNode components = formContent.at("/module/components");
//        List<AttachmentGroup> attachmentGroups = new ArrayList<>();
//        extractAttachment(null, components, attachmentGroups);
//        return attachmentGroups;
//    }

//    private void extractAttachment(JsonNode parentComponent, JsonNode components, List<AttachmentGroup> attachmentGroups) {
//        for (int i = 0; i < components.size(); i++) {
//            JsonNode component = components.get(i);
//            JsonNode type = component.get("type");
//            if (type != null) {
//                String typeText = type.asText();
//                if ("FileUploadPreview".equals(typeText)) {
//                    JsonNode idNode = component.get("id");
//                    String attachmentSectionNameNode = getFieldName(parentComponent);
//                    if (idNode != null) {
//                        List<ConfigableAttrs> configAbleAttrs = new ArrayList<>();
//                        setConfigAbleAttrs(component.get("uploadVisibleControlledByRules"), "uploadVisible", "上传按钮是否可见", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("uploadDisabledControlledByRules"), "uploadDisabled", "上传按钮是否禁用", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("previewVisibleControlledByRules"), "previewVisible", "预览是否可见", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("previewReadonlyControlledByRules"), "previewReadonly", "预览是否只读", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("disabledControlledByRules"), "disabled", "是否禁用", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("noDownloadControlledByRules"), "noDownload", "是否禁止下载", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("noPreviewControlledByRules"), "noPreview", "禁用预览", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("previewShowCommentsControlledByRules"), "previewShowComments", "是否显示批注", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("previewEnableEditFileControlledByRules"), "previewEnableEditFile", "是否启用在线编辑", configAbleAttrs);
//                        setConfigAbleAttrs(component.get("noDeleteControlledByRules"), "noDelete", "禁止删除", configAbleAttrs);
//                        if (!configAbleAttrs.isEmpty()) {
//                            String idText = idNode.asText();
//                            AttachmentGroup attachmentGroup = new AttachmentGroup();
//                            attachmentGroup.setId(idText);
//                            attachmentGroup.setName(attachmentSectionNameNode);
//                            attachmentGroup.setOperations(configAbleAttrs);
//                            attachmentGroups.add(attachmentGroup);
//                        }
//                    }
//                }
//
//            }
//            if (component.get("contents") != null && !component.get("contents").isEmpty()) {
//                extractAttachment(component, component.get("contents"), attachmentGroups);
//            }
//        }
//    }

    private String getFieldName(JsonNode component) {
        if (component == null) {
            return "";
        }
        JsonNode title = component.get("title");
        JsonNode caption = component.get("caption");
        JsonNode text = component.get("text");
        JsonNode mainTitle = component.get("mainTitle");
        if (title != null) {
            return title.asText();
        }
        if (caption != null) {
            return caption.asText();
        }
        if (text != null) {
            return text.asText();
        }
        if (mainTitle != null) {
            return mainTitle.asText();
        }
        return "";

    }

    private static void setConfigAbleAttrs(JsonNode attr, String code, List<String> configAbleAttrs) {
        if (attr != null) {
            boolean visibleControl = attr.asBoolean(false);
            if (visibleControl) {
                configAbleAttrs.add(code);
            }
        }
    }

    public void extractEntities(JsonNode entityNode, ObjectData entity, Map<String, ChildData> fieldMap) {
        entity.setId(entityNode.get("id").asText());
        entity.setCode(entityNode.get("label").asText());
        entity.setName(entityNode.get("name").asText());

        List<ChildData> fields = new ArrayList<>();
        JsonNode fieldNodes = entityNode.at("/type/fields");
        extractFields(fieldNodes, fields, fieldMap);
        entity.setElements(fields);

        JsonNode childEntityNodes =  entityNode.at("/type/entities");
        if (childEntityNodes != null && !childEntityNodes.isEmpty()) {
            List<ObjectData> childEntities = new ArrayList<>();
            childEntityNodes.forEach(childEntityNode -> {
                ObjectData childEntity = new ObjectData();
                extractEntities(childEntityNode, childEntity, fieldMap);
                // 如果当前子对象有字段，则添加
                if (!childEntity.getElements().isEmpty()) {
                    childEntities.add(childEntity);
                }
            });
            // 如果当前子对象有子对象，则添加
            if (!childEntities.isEmpty()) {
                entity.setChildObjects(childEntities);
            }
        }
    }

    public void extractFields(JsonNode fieldNodes, List<ChildData> fields, Map<String, ChildData> fieldMap) {
        fieldNodes.forEach(fieldNode -> {
            ChildData field = new ChildData();
            String id = fieldNode.get("id").asText();
            field.setId(fieldNode.get("id").asText());
            field.setName(fieldNode.get("name").asText());
            field.setCode(fieldNode.get("bindingPath").asText());
            if (fieldMap.containsKey(id)) {
                // 如果当前字段需要抽取，则赋值component中名称
                field.setName(fieldMap.get(id).getName());
                field.setConfigurableAttrs(fieldMap.get(id).getConfigurableAttrs());
                fields.add(field);
            }
            JsonNode childFieldNodes = fieldNode.at("/type/fields");
            if (childFieldNodes != null && !childFieldNodes.isEmpty()) {
                List<ChildData> childFields = new ArrayList<>();
                extractFields(childFieldNodes, childFields, fieldMap);
                if (!childFields.isEmpty()) {
                    field.setChildFields(childFields);
                    fields.add(field);
                }

            }
        });
    }

    private void generateFormFormat(VoFormModel voFormModel, GspMetadata md, String formPath){
        FormMetadataContent formMetadataContent = (FormMetadataContent) md.getContent();
        JsonNode content = formMetadataContent.getContents();
        String url = "";
        JsonNode codeNode = content.at("/module/code");
        String code = "";
        if (codeNode != null && !StringUtils.isEmpty(codeNode.asText())) {
            code = codeNode.asText();
        }
        if (StringUtils.isEmpty(formPath)) {
            DevBasicInfoService devBasicInfoService = SpringBeanUtils.getBean(DevBasicInfoService.class);
            DevBasicBoInfo devBasicBoInfo = devBasicInfoService.getDevBasicBoInfo(md.getHeader().getBizobjectID());
            String serviceUnitPath = devBasicBoInfo.getAppCode().toLowerCase() + "/" + devBasicBoInfo.getSuCode().toLowerCase();
            String projectName = content.at("/module/projectName").asText();
            url = "/apps/" + serviceUnitPath + "/web/" + projectName + "/index.html#/" + code;
        } else {
            MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
            GspProject project = metadataService.getGspProjectInfo(formPath);
            String deploymentPath = project.getSuDeploymentPath();
            String projectName = project.getMetadataProjectName().toLowerCase();
            url = "/" + deploymentPath + "/web/" + projectName + "/index.html#/" + code;
        }

        url += "?formRulePushedFrom=LCDP";
        JsonNode formRulePushMode = content.at("/options/formRulePushMode");
        if (formRulePushMode != null) {
            url += ("&formRulePushMode=" + formRulePushMode.textValue());
        }
        voFormModel.setUrlType("url");
        voFormModel.setUrl(url);

        List<Parameters> parameters = new ArrayList<>();
        generateParameters(parameters, "action", "动作", "LoadAndView1");
        generateParameters(parameters, "id", "内码", "{\"expr\":\"DefaultFunction.GetContextParameter(\\\"dataId\\\")\",\"sexpr\":\"\"}\n");
        voFormModel.setUrlParameters(parameters);
    }

    private void generateParameters(List<Parameters> parameters, String code, String name, String value) {
        Parameters actionParam = new Parameters();
        actionParam.setCode(code);
        actionParam.setName(name);
        actionParam.setValue(value);
        parameters.add(actionParam);
    }
}
