/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.resolver;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import lombok.Data;

/**
 * resolve参数
 *
 * @Title: ResolveFormMetadataItem
 * @Description: com.inspur.edp.web.frontendproject.resolver
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/14 16:42
 */
@Data
public class ResolveFormMetadataItem {
    /**
     * 表单关联的元数据
     */
    private GspMetadata gspMetadata;
    /**
     * 是否强制使用解析方式 请不要使用该参数直接判断表单是否解析
     * 如果要判断表单是否解析，请使用getCalculateIsDynamicForm()方法
     */
    private boolean isForceDynamicForm;

    /**
     * 表单元数据标识的解析标识
     * 此参数只标识表单元数据得解析标识，不作为表单最终是否以解析模式运行得依据，
     * 如果要判断表单是否解析，请使用getCalculateIsDynamicForm
     */
    private  boolean isFormDynamicTag;

    /**
     * 合并是否解析表单标签
     * @return
     */
    public boolean getCalculateIsDynamicForm() {
        if (this.isForceDynamicForm) {
            return true;
        }
        return this.isFormDynamicTag;
    }


}
