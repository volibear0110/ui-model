package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.HashMap;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;


/**
 * 集合控件策略提取策略
 */
public class MobileCollectionComponentI18nResourceStrategy  extends AbstractMobileI18nResourceStrategy {

    public MobileCollectionComponentI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        switch(this.componentType) {
            case ComponentTypes.LightAttachment:
                this.collectLightAttachmentResourceItems();
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集LightAttachment相关资源项
     */
    private void collectLightAttachmentResourceItems() {
        this.collectSimpleAttrResourceItem("title");
    }

}
