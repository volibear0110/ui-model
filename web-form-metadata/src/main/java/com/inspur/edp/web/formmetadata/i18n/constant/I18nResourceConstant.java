/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.constant;

public final class I18nResourceConstant {
    /**
     * 资源项第一级分隔符 逗号
     */
    public static final String FIRST_LEVEL_DELIMITER = ".";

    /**
     * 资源项第二级分隔符 反斜线
     */
    public static final String SECOND_LEVEL_DELIMITER = "/";

    /**
     * 资源项第三级分隔符 $
     */
    public static final String THIRD_LEVEL_DELIMITER = "$";

    /**
     * 中文标识
     */
    public static final String ZH_CHS = "zh-CHS";

    /**
     * 英文标识
     */
    public static final String En = "en";

    public static final String ZH_CHT = "zh-CHT";

    /**
     * 枚举对应的值field
     */
    public static final String ValueFieldKey = "valueField";

    /**
     * 枚举对应的text field
     */
    public static final String TextFieldKey = "textField";

    /**
     * 枚举对应的id field
     */
    public static final String IdFieldKey = "idField";
}
