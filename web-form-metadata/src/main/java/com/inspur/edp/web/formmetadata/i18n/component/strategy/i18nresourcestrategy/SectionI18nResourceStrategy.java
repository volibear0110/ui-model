/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;

import java.util.HashMap;
import java.util.List;

public class SectionI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        // 从html属性中提取多语字段
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        I18nResourceItemCollection htmlI18nResourceItemCollection = extractComponentHtmlI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (htmlI18nResourceItemCollection != null && htmlI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(htmlI18nResourceItemCollection);
        }

        I18nResourceItemCollection toolbarResourceItemCollection = extractToolbar(i18nResourceItemBaseId, currentComponent);
        if (toolbarResourceItemCollection != null && !toolbarResourceItemCollection.isEmpty()) {
            i18nResourceItemCollection.addRange(toolbarResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    /**
     * 提取section 工具栏
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection extractToolbar(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        if (currentComponent.containsKey("toolbar") && currentComponent.get("toolbar") != null) {
            HashMap<String, Object> toolbarComponent = (HashMap<String, Object>) currentComponent.get("toolbar");
            if (toolbarComponent != null && toolbarComponent.containsKey("contents")) {
                List<HashMap<String, Object>> contentsToolbar = (List<HashMap<String, Object>>) toolbarComponent.get("contents");
                if (contentsToolbar != null && !contentsToolbar.isEmpty()) {
                    contentsToolbar.forEach(t -> {
                        this.extractToolbarItem(t, i18nResourceItemBaseId, i18nResourceItemCollection);
                    });
                }
            }
        }
        return i18nResourceItemCollection;
    }

    /**
     * 递归获取toolbar标题 及其子
     * @param toolbarItem
     * @param i18nResourceItemBaseId
     * @param i18nResourceItemCollection
     */
    private void extractToolbarItem(HashMap<String, Object> toolbarItem, String i18nResourceItemBaseId, I18nResourceItemCollection i18nResourceItemCollection) {
        String toolbarTitle = ComponentUtility.getInstance().getTitle(toolbarItem);
        String toolbarId = ComponentUtility.getInstance().getId(toolbarItem);
        this.addInCollection(i18nResourceItemCollection, I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, toolbarId, toolbarTitle, toolbarTitle));
        // 如果包含下级菜单  那么进行递归
        List<HashMap<String, Object>> childrenItems = ComponentUtility.getInstance().GetItems(toolbarItem);
        if (childrenItems != null && childrenItems.size() > 0) {
            childrenItems.forEach(t -> this.extractToolbarItem(t, i18nResourceItemBaseId, i18nResourceItemCollection));
        }
    }

    private I18nResourceItemCollection extractComponentHtmlI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        String generatedComponentId = currentComponentType + "/" + currentComponentId + "/";
        // 从mainTitile属性中提取多语资源
        String mainTitle = ComponentUtility.getInstance().getMainTitle(currentComponent);
        String mainTitleName = ComponentUtility.getInstance().getMainTitleName();
        this.addInCollection(i18nResourceItemCollection, I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId + mainTitleName, mainTitle, mainTitle));

        // 从subTitile属性中提取多语资源
        String subTitle = ComponentUtility.getInstance().getSubTitle(currentComponent);
        String subTitleName = ComponentUtility.getInstance().getSubTitleName();
        this.addInCollection(i18nResourceItemCollection, I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId + subTitleName, subTitle, subTitle));
        return i18nResourceItemCollection;
    }

}
