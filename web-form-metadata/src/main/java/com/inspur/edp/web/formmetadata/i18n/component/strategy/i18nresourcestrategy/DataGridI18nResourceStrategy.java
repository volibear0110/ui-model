/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * datagrid 国际化参数日趋策略
 *
 * @author noah
 */
public class DataGridI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从lineNumberTitle属性中提取多语字段
        I18nResourceItemCollection LineNumberTitleI18nResourceItemCollection = ExtractComponentLineNumberTitleI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (LineNumberTitleI18nResourceItemCollection != null && LineNumberTitleI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(LineNumberTitleI18nResourceItemCollection);
        }

        // 从headerGroup属性中提取多语字段
        String headerGroupI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        I18nResourceItemCollection headerGroupI18nResourceItemCollection = ExtractHeaderGroupResourceItemCollection(i18nResourceItemBaseId, headerGroupI18nResourceItemBaseId, currentComponent);
        if (headerGroupI18nResourceItemCollection != null && headerGroupI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(headerGroupI18nResourceItemCollection);
        }

        //提取grid 的操作列
        I18nResourceItemCollection operateButtonResourceItemCollection = ExtractComponentOperateButtonI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (operateButtonResourceItemCollection != null && operateButtonResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(operateButtonResourceItemCollection);
        }

        // 提取上下问菜单中的title参数
        I18nResourceItemCollection contextMenuResourceItemCollection = extractContextMenu(i18nResourceItemBaseId, currentComponent);
        if (contextMenuResourceItemCollection != null && contextMenuResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(contextMenuResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    /**
     * 提取列表上下文参数
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection extractContextMenu(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String contextMenuI18nResourceBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "contextMenuItems" + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        ArrayList<HashMap<String, Object>> contextMenuItemList = ComponentUtility.getInstance().getDataGridContextMenu(currentComponent);
        if (contextMenuItemList != null) {
            contextMenuItemList.forEach(t -> extractContextMenuItem(i18nResourceItemBaseId, contextMenuI18nResourceBaseId, t, i18nResourceItemCollection));
        }
        return i18nResourceItemCollection;
    }

    /**
     * 提取列表上下文参数中的具体项及其children
     *
     * @param i18nResourceItemBaseId
     * @param contextMenuResourceBaseId
     * @param contextMenuItem
     * @param contextMenuResourceCollection
     */
    private void extractContextMenuItem(String i18nResourceItemBaseId, String contextMenuResourceBaseId, HashMap<String, Object> contextMenuItem, I18nResourceItemCollection contextMenuResourceCollection) {
        String contextMenuItemId = ComponentUtility.getInstance().getId(contextMenuItem);
        String title = ComponentUtility.getInstance().getTitle(contextMenuItem);
        String resourceId = contextMenuResourceBaseId + contextMenuItemId;
        I18nResourceItem contextMenuI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, resourceId, title, title);
        this.addInCollection(contextMenuResourceCollection, contextMenuI18nResourceItem);

        //获取对应的children的值
        ArrayList<HashMap<String, Object>> contextMenuItemChildren = ComponentUtility.getInstance().getDataGridContextMenuItemChildren(contextMenuItem);
        if (contextMenuItemChildren != null) {
            contextMenuItemChildren.forEach(t -> this.extractContextMenuItem(i18nResourceItemBaseId, contextMenuResourceBaseId, t, contextMenuResourceCollection));
        }
    }

    /**
     * 提取farris-grid的 操作列   编辑、删除
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection ExtractComponentOperateButtonI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType)) {
            return null;
        }

        String operateEditButtonAtributeName = "OperateEditButton";
        String operateEditButtonAtributeValue = "编辑";
        // 使用 "/"作为分隔符
        String generatedOperateEditButtonComponentId = currentComponentType + "/" + currentComponentId + "/" + operateEditButtonAtributeName;
        I18nResourceItem editI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedOperateEditButtonComponentId, operateEditButtonAtributeValue, operateEditButtonAtributeValue);
        this.addInCollection(i18nResourceItemCollection, editI18nResourceItem);


        String operateDeleteButtonAtributeName = "OperateDeleteButton";
        String operateDeleteButtonAtributeValue = "删除";
        // 使用 "/"作为分隔符
        String generatedOperateDeleteButtonComponentId = currentComponentType + "/" + currentComponentId + "/" + operateDeleteButtonAtributeName;
        I18nResourceItem deleteI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedOperateDeleteButtonComponentId, operateDeleteButtonAtributeValue, operateDeleteButtonAtributeValue);
        this.addInCollection(i18nResourceItemCollection, deleteI18nResourceItem);


        String operateColumnAtributeName = "OperateColumn";
        String operateColumnAtributeValue = "操作";
        // 使用 "/"作为分隔符
        String generatedOperateColumnComponentId = currentComponentType + "/" + currentComponentId + "/" + operateColumnAtributeName;
        I18nResourceItem operateColumnI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedOperateColumnComponentId, operateColumnAtributeValue, operateColumnAtributeValue);
        this.addInCollection(i18nResourceItemCollection, operateColumnI18nResourceItem);

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection ExtractComponentLineNumberTitleI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        String lineNumberTitleAtributeName = ComponentUtility.getInstance().GetLineNumberTitleName(currentComponent);
        String lineNumberTitleAtributeValue = ComponentUtility.getInstance().getLineNumberTitle(currentComponent);

        // 使用 "/"作为分隔符
        String generatedComponentId = currentComponentType + "/" + currentComponentId + "/" + lineNumberTitleAtributeName;
        if (!StringUtility.isNullOrEmpty(lineNumberTitleAtributeValue)) {
            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, lineNumberTitleAtributeValue, lineNumberTitleAtributeValue);
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        } else {
            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, "", "");
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection ExtractHeaderGroupResourceItemCollection(String i18nResourceItemBaseId, String headerGroupI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        headerGroupI18nResourceItemBaseId = headerGroupI18nResourceItemBaseId + ComponentUtility.getInstance().getHeaderGroupName(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;

        ArrayList<HashMap<String, Object>> headerGroupItemCollection = ComponentUtility.getInstance().getHeaderGroup(currentComponent);

        for (HashMap<String, Object> headerGroupItem : headerGroupItemCollection) {
            if (!StringUtility.isNullOrEmpty(ComponentUtility.getInstance().getFieldRef(headerGroupItem)) || ComponentUtility.getInstance().getCandidateNodeFlag(headerGroupItem)) {
                continue;
            }

            String componentId = ComponentUtility.getInstance().getId(headerGroupItem);
            String componentName = ComponentUtility.getInstance().getCaption(headerGroupItem);

            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, headerGroupI18nResourceItemBaseId + componentId, componentName, componentName);
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }

}
