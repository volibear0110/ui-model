/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/02/26
 */
public class ListViewII18nResourceStrategy extends AbstractI18nResourceStrategy {

    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        // 提取contentTemplate中的fields值
        // 从tagData中提取多语资源项
        HashMap<String, Object> listViewContentTemplate = ComponentUtility.getInstance().getContentTemplate(currentComponent);
        if (listViewContentTemplate == null || listViewContentTemplate.size() == 0) {
            return i18nResourceItemCollection;
        }

        String strListViewId = ComponentUtility.getInstance().getId(currentComponent);

        List<HashMap<String, Object>> contentTemplateFields = ComponentUtility.getInstance().GetFieldsFromContentTemplate(listViewContentTemplate);
        String listViewResourceId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                strListViewId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "contentTemplate";


        I18nResourceItemCollection resourceItemCollection = extractFieldsI18nResourceItemCollection(i18nResourceItemBaseId, listViewResourceId, contentTemplateFields);
        if (resourceItemCollection != null) {
            i18nResourceItemCollection.addRange(resourceItemCollection);
        }

        HashMap<String, Object> contentTemplateToolbar = ComponentUtility.getInstance().GetAttributeObject(listViewContentTemplate, "toolbar");
        if (contentTemplateToolbar != null) {
            List<HashMap<String, Object>> contentTemplateToolbarContents = ComponentUtility.getInstance().GetToolbarContentsFromContentTemplate(contentTemplateToolbar);
            if (contentTemplateToolbarContents != null && contentTemplateToolbarContents.size() > 0) {
                I18nResourceItemCollection toolbarContentsResourceCollection = extractToolbarContentsI18nResourceItemCollection(i18nResourceItemBaseId, listViewResourceId, contentTemplateToolbarContents);
                if (toolbarContentsResourceCollection != null) {
                    i18nResourceItemCollection.addRange(toolbarContentsResourceCollection);
                }
            }
        }

        return i18nResourceItemCollection;
    }

    /// <summary>
    /// 提取枚举属性中多语资源项
    /// </summary>
    /// <returns></returns>
    private I18nResourceItemCollection extractToolbarContentsI18nResourceItemCollection(String i18nResourceItemBaseId,
                                                                                        String listViewI18nResourceItemBaseId,
                                                                                        List<HashMap<String, Object>> componentCollection) {
        I18nResourceItemCollection contentTemplateI18nResourceItemCollection = new I18nResourceItemCollection();
        componentCollection.forEach((component) -> {
            String textAtributeValue = ComponentUtility.getInstance().getText(component);
            String idAtributeValue = ComponentUtility.getInstance().getId(component);
            String typeAttributeValue = ComponentUtility.getInstance().getType(component);

            String generatedComponentId =
                    listViewI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "toobar" + I18nResourceConstant.SECOND_LEVEL_DELIMITER + typeAttributeValue + I18nResourceConstant.SECOND_LEVEL_DELIMITER + idAtributeValue;

            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, textAtributeValue, textAtributeValue);
            this.addInCollection(contentTemplateI18nResourceItemCollection, i18nResourceItem);
        });


        return contentTemplateI18nResourceItemCollection;
    }


    /// <summary>
    /// 提取枚举属性中多语资源项
    /// </summary>
    /// <returns></returns>
    private I18nResourceItemCollection extractFieldsI18nResourceItemCollection(String i18nResourceItemBaseId,
                                                                               String listViewI18nResourceItemBaseId,
                                                                               List<HashMap<String, Object>> componentCollection) {
        I18nResourceItemCollection contentTemplateI18nResourceItemCollection = new I18nResourceItemCollection();

        componentCollection.forEach((component) -> {
            com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection i18nResourceItem = getContentTemplateFieldsI18nResourceItem(i18nResourceItemBaseId, listViewI18nResourceItemBaseId, component);
            if (i18nResourceItem != null) {
                contentTemplateI18nResourceItemCollection.addRange(i18nResourceItem);
            }
        });


        return contentTemplateI18nResourceItemCollection;
    }

    /// <summary>
    /// 提取contentTemplate中fields中的具体项
    /// </summary>
    /// <param name="enumI18nResourceItemBaseId"></param>
    /// <param name="fieldItemObject"></param>
    /// <returns></returns>
    private I18nResourceItemCollection getContentTemplateFieldsI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> fieldItemObject) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String nameAtributeValue = ComponentUtility.getInstance().getName(fieldItemObject);
        String idAtributeValue = ComponentUtility.getInstance().getId(fieldItemObject);
        String componentId = idAtributeValue;

        String generatedComponentId = enumI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                ComponentUtility.getInstance().getType(fieldItemObject) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

        I18nResourceItemCollection fieldEnumResource = getContentTemplateFieldsI18nResourceItemEnumData(i18nResourceItemBaseId, generatedComponentId, fieldItemObject);
        if (fieldEnumResource != null) {
            i18nResourceItemCollection.addRange(fieldEnumResource);
        }
        return i18nResourceItemCollection;
    }

    /// <summary>
    /// 提取contentTemplate中fields中的具体项
    /// </summary>
    /// <param name="enumI18nResourceItemBaseId"></param>
    /// <param name="fieldItemObject"></param>
    /// <returns></returns>
    private I18nResourceItemCollection getContentTemplateFieldsI18nResourceItemEnumData(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> fieldItemObject) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String strValueFieldKey = "value";
        String strTextFieldKey = "name";

        // 从enumData中提取多语资源项
        List<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumData(fieldItemObject);
        String enumDataAttributeName = ComponentUtility.getInstance().GetEnumDataName(fieldItemObject);

        I18nResourceItemCollection enumDataI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, enumI18nResourceItemBaseId, enumValueCollection, enumDataAttributeName, strValueFieldKey, strTextFieldKey);
        if (enumDataI18nResourceItemCollection != null && enumDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(enumDataI18nResourceItemCollection);
        }
        return i18nResourceItemCollection;
    }

}
