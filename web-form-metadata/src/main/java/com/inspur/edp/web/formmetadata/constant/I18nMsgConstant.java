package com.inspur.edp.web.formmetadata.constant;

public class I18nMsgConstant {
    public final static String WEB_FORM_METADATA_MSG_0001 = "WEB_FORM_METADATA_MSG_0001";
    public final static String WEB_FORM_METADATA_MSG_0002 = "WEB_FORM_METADATA_MSG_0002";
    public final static String WEB_FORM_METADATA_MSG_0003 = "WEB_FORM_METADATA_MSG_0003";
    public final static String WEB_FORM_METADATA_MSG_0004 = "WEB_FORM_METADATA_MSG_0004";
    public final static String WEB_FORM_METADATA_MSG_0005 = "WEB_FORM_METADATA_MSG_0005";
    public final static String WEB_FORM_METADATA_MSG_0006 = "WEB_FORM_METADATA_MSG_0006";
    public final static String WEB_FORM_METADATA_MSG_0007 = "WEB_FORM_METADATA_MSG_0007";
    public final static String WEB_FORM_METADATA_MSG_0008 = "WEB_FORM_METADATA_MSG_0008";
    public final static String WEB_FORM_METADATA_MSG_0009 = "WEB_FORM_METADATA_MSG_0009";
    public final static String WEB_FORM_METADATA_MSG_0010 = "WEB_FORM_METADATA_MSG_0010";
    public final static String WEB_FORM_METADATA_MSG_0011 = "WEB_FORM_METADATA_MSG_0011";

}
