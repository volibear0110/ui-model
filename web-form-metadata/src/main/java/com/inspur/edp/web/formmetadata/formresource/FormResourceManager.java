/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.formresource;

import com.inspur.edp.i18n.resource.api.II18nResourceDTManager;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.I18nResource;
import com.inspur.edp.lcm.metadata.api.exception.MetadataNotFoundException;
import com.inspur.edp.web.common.logger.WebLogger;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 表单关联资源元数据
 *
 * @author noah
 * 2023/7/8 17:19
 */
public class FormResourceManager {
    /**
     * 根据表单元数据获取对应的资源列表
     *
     * @param metadata     元数据信息*
     * @param language     语言列表
     * @param baseFormPath 传递的基础表单的路径 针对组合表单，传递的也是基础表单的路径
     */
    public List<I18nResource> getFormResourceWithMetaData(GspMetadata metadata, String language, String baseFormPath) {
        II18nResourceDTManager resourceDtManager = SpringBeanUtils.getBean(II18nResourceDTManager.class);
        // 当前语言未生效
        try {
            List<I18nResource> resourceList = resourceDtManager.getI18nResource(metadata, language, baseFormPath);
            // 按语言进行过滤
            if (resourceList != null && resourceList.size() > 0) {
                resourceList = resourceList
                        .stream()
                        .filter((t) -> language.equals(t.getLanguage()))
                        .collect(Collectors.toList());
            }
            return resourceList;
        } catch (MetadataNotFoundException ex) {
            WebLogger.Instance.warn(ex.getMessage() + Arrays.toString(ex.getStackTrace()));
        } catch (Exception ex) {
            // 排除掉找不到对应的资源文件的异常
            String fileNotFoundException = "java.io.FileNotFoundException";
            if (ex.getMessage() != null && ex.getMessage().contains(fileNotFoundException)) {
                WebLogger.Instance.warn(ex.getMessage());
            } else {
                WebLogger.Instance.error(ex);
                throw ex;
            }
        }
        return new ArrayList<>();
    }
}
