/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadataanalysis;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadatamanager.EapiMetadataManager;
import com.inspur.edp.web.formmetadata.metadatamanager.MetadataManagerParameter;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Eapi解析
 *
 * @author noah
 */

public class EapiAnalysis extends AbstractMetadataAnalysis {

    public EapiAnalysis(ExecuteEnvironment executeEnvironment, boolean isUpgradeTool) {
        super(executeEnvironment, isUpgradeTool);
    }

    public void resolveEapi(FormDOM json, String formMetadataName, String targetStoragePath,
                            String nameSpace, String relativePath,
                            Function<ResolveEapiParameter, Optional<GspMetadata>> getEapiMetadataSupplier) {
        if (json == null || json.getModule() == null || json.getModule().getCode() == null
                || StringUtility.isNullOrEmpty(targetStoragePath)
                || json.getModule().getSchemas() == null || json.getModule().getSchemas().size() == 0) {
            return;
        }
        StringBuilder eapis = new StringBuilder();
        eapis.append("[");
        int index = 0;
        // VO-EAPI(vo和eapi是一对一的关系)
        List<HashMap<String, Object>> schemas = json.getModule().getSchemas();
        EapiMetadataManager eapiMetadataManager = new EapiMetadataManager(this.executeEnvironment, this.isUpgradeTool, relativePath);
        for (HashMap<String, Object> item : schemas) {
            if (index > 0) {
                eapis.append(",");
            }
            //获取对应的EapiID  如果可以读取到
            String strEapiID = item.containsKey("eapiId") ? item.get("eapiId").toString() : "";
            // 如果获取到EapiID  抛出对应的异常，主要目的是因为eapi的结构调整
            if (!StringUtility.isNullOrEmpty(strEapiID)) {
                MetadataManagerParameter metadataManagerParameter = MetadataManagerParameter.init(strEapiID,
                        StringUtility.getOrDefaultEmpty(item.get("eapiCode")),
                        StringUtility.getOrDefaultEmpty(item.get("eapiName")),
                        StringUtility.getOrDefault(item.get("eapiNameSpace"), nameSpace));
                ResolveEapiParameter resolveEapiParameter = ResolveEapiParameter.init(metadataManagerParameter, eapiMetadataManager);
                Optional<GspMetadata> optionalEapiMetadata = getEapiMetadataSupplier.apply(resolveEapiParameter);
                if (optionalEapiMetadata.isPresent()) {
                    IMetadataContent eapiMetadataContent = optionalEapiMetadata.get().getContent();
                    String eapiJson = eapiMetadataManager.serialize(eapiMetadataContent, PropertyNamingStrategy.UPPER_CAMEL_CASE);
                    eapis.append(eapiJson);

                    index += 1;
                }
            }
        }
        eapis.append("]");
        eapiMetadataManager.saveMetadataFile(targetStoragePath,
                formMetadataName.toLowerCase() + JITEngineConstants.EapiJsonFile, eapis.toString());
    }

    /**
     * 解析表单代码引用的eapi
     *
     * @param json              表单代码
     * @param targetStoragePath 解析后eapi存储路径
     */
    public void resolveEapi(FormDOM json, String formMetadataName, String targetStoragePath,
                            String nameSpace, String relativePath) {
        this.resolveEapi(json, formMetadataName, targetStoragePath, nameSpace, relativePath,
                (resolveEapiParameter) ->
                        resolveEapiParameter.getEapiMetadataManager().getEapiMetadata(resolveEapiParameter.getMetadataManagerParameter()));
    }

    @Getter
    public static class ResolveEapiParameter {
        private MetadataManagerParameter metadataManagerParameter;
        private EapiMetadataManager eapiMetadataManager;

        private ResolveEapiParameter() {
        }

        public static ResolveEapiParameter init(MetadataManagerParameter metadataManagerParameter, EapiMetadataManager eapiMetadataManager) {
            ResolveEapiParameter resolveEapiParameter = new ResolveEapiParameter();
            resolveEapiParameter.eapiMetadataManager = eapiMetadataManager;
            resolveEapiParameter.metadataManagerParameter = metadataManagerParameter;
            return resolveEapiParameter;
        }
    }
}
