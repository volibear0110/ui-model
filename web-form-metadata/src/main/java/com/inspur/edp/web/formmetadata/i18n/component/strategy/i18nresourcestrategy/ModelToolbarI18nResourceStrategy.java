/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceStrategyFactory;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * modal 工具栏标题提取 整个表单提取一次
 *
 * @author guozhiqi
 */
public class ModelToolbarI18nResourceStrategy {

    public I18nResourceItemCollection extractModalToolbar(FormDOM formDOM, String i18nResourceBaseId) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        if (formDOM.getModule().getToolbar() != null && !formDOM.getModule().getToolbar().isEmpty() && formDOM.getModule().getToolbar().containsKey("items")) {
            HashMap<String, List<HashMap<String, Object>>> toolbarItems = (HashMap<String, List<HashMap<String, Object>>>) formDOM.getModule().getToolbar().get("items");
            if (toolbarItems != null && !toolbarItems.isEmpty()) {
                for (Map.Entry<String, List<HashMap<String, Object>>> item : toolbarItems.entrySet()) {
                    List<HashMap<String, Object>> toolbarItemList = item.getValue();
                    if (toolbarItemList != null && !toolbarItemList.isEmpty()) {
                        toolbarItemList.forEach((toolbarItem) -> {
                            I18nResourceItemCollection toolbarItemResourceItemCollection = Objects.requireNonNull(I18nResourceStrategyFactory.getInstance().getI18nResourceStrategy(toolbarItem, i18nResourceBaseId)).extractI18nResource(i18nResourceBaseId, toolbarItem, formDOM);
                            if (toolbarItemResourceItemCollection != null) {
                                i18nResourceItemCollection.addRange(toolbarItemResourceItemCollection);
                            }
                        });

                    }
                }
            }
        }


        return i18nResourceItemCollection;
    }
}
