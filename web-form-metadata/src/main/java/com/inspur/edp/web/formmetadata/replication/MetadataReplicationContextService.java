/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;

import java.util.regex.Pattern;

/**
 * @author guozhiqi
 */
class MetadataReplicationContextService {

    /**
     * 创建元数据复制上下文
     *
     * @param sourceProjectName  源工程名称
     * @param sourceMetadata     待复制元数据
     * @param targetMetadataCode 复制后元数据编号
     * @param targetMetadataName 复制后元数据名称
     * @param targetProjectName  复制后元数据所在工程的工程名
     * @return
     */
    public static MetadataReplicationContext create(String sourceProjectName, GspMetadata sourceMetadata, String targetMetadataCode, String targetMetadataName, String targetProjectName) {
        MetadataReplicationContext metadataReplicationContext = new MetadataReplicationContext();
        metadataReplicationContext.setSourceMetadata(sourceMetadata);
        metadataReplicationContext.setSourceProjectName(sourceProjectName);

        // 目标工程名为空时，使用源工程名
        if (StringUtility.isNullOrEmpty(targetProjectName)) {
            targetProjectName = sourceProjectName;
        }
        MetadataDto metadataDto = new MetadataDto();
        metadataDto.setCode(targetMetadataCode);
        metadataDto.setName(targetMetadataName);
        metadataDto.setProjectName(targetProjectName);
        metadataReplicationContext.setTargetMetadataDescription(metadataDto);

        return metadataReplicationContext;
    }

    /**
     * 更新元数据复制上下文参数
     * 依据是否相同工程更新目标元数据相对路径参数
     *
     * @param metadataReplicationContext
     */
    public static void updateMetadataReplicationContext(MetadataReplicationContext metadataReplicationContext) {
        GspMetadata sourceMetadata = metadataReplicationContext.getSourceMetadata();
        String sourceProjectName = metadataReplicationContext.getSourceProjectName();
        String targetProjectName = metadataReplicationContext.getTargetMetadataDescription().getProjectName();

        if (sourceProjectName.equals(targetProjectName)) {
            // 新拷贝的表单与待拷贝表单在同一个工程
            metadataReplicationContext.getTargetMetadataDescription().setRelativePath(sourceMetadata.getRelativePath());
        } else {
            metadataReplicationContext.getTargetMetadataDescription().setRelativePath(sourceMetadata.getRelativePath().replace(sourceProjectName, targetProjectName));
            try {
                // 根据原工程信息更新目标工程信息，避免引用的元数据无法获取到的问题
                String[] sourceMetadataSegmentPathCollection = sourceMetadata.getRelativePath().replace("\\", "/").split(Pattern.quote("/"), -1);
                ProjectInformationManager.updateTargetProjectInformation(sourceMetadataSegmentPathCollection, sourceProjectName, targetProjectName);
            } catch (RuntimeException e) {
                throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0014, new String[]{sourceMetadata.getHeader().getId(), e.getMessage()}, e);
            }
        }
    }
}
