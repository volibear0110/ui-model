/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentContext;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

public class GridFieldI18nResourceStrategy extends AbstractI18nResourceStrategy {
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.CAPTION);
        if (componentNameStrategy == null) {
            return null;
        }

        String componentName = componentNameStrategy.getComponentName(component);

        return componentName;
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        I18nResourceItemCollection aggregateI18nResourceItemCollection = extractComponentAggregateI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (aggregateI18nResourceItemCollection != null && aggregateI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(aggregateI18nResourceItemCollection);
        }
        String strValueFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.ValueFieldKey);
        String strTextFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.TextFieldKey);

        // 从enumData中提取多语资源项
        ArrayList<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumData(currentComponent);
        String enumDataAttributeName = ComponentUtility.getInstance().GetEnumDataName(currentComponent);
        String enumDataI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent);
        I18nResourceItemCollection enumDataI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, enumDataI18nResourceItemBaseId, enumValueCollection, enumDataAttributeName, strValueFieldKey, strTextFieldKey);
        if (enumDataI18nResourceItemCollection != null && enumDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(enumDataI18nResourceItemCollection);
        }

        // 从editor中提取多语资源项
        HashMap<String, Object> editorAttributeObject = ComponentUtility.getInstance().GetEditor(currentComponent);
        // 排除grid中editor为枚举的值的提取
        if (editorAttributeObject != null &&
                !"EnumField".equals(ComponentUtility.getInstance().getType(editorAttributeObject))) {
            String editorI18nResourceItemBaseId = i18nResourceItemBaseId + ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().GetEditorName(editorAttributeObject) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
            ComponentContext componentI18nResourceContext = new ComponentContext(editorAttributeObject, editorI18nResourceItemBaseId, this.getFormDOM());
            I18nResourceItemCollection editorI18nResourceItemCollection = componentI18nResourceContext.extractI18nResource();
            if (editorI18nResourceItemCollection != null && editorI18nResourceItemCollection.size() > 0) {
                i18nResourceItemCollection.addRange(editorI18nResourceItemCollection);
            }
        }


        // 从formatter中提取多语资源项
        HashMap<String, Object> formatterAttributeObject = ComponentUtility.getInstance().GetFormatter(currentComponent);
        String formatterI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent);
        I18nResourceItemCollection formatterI18nResourceItemCollection = extractFormatterI18nResource(i18nResourceItemBaseId, formatterI18nResourceItemBaseId, formatterAttributeObject);
        if (formatterI18nResourceItemCollection != null && formatterI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(formatterI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractFormatterI18nResource(String i18nResourceItemBaseId, String fielI18nResourceItemBaseId, HashMap<String, Object> formatterObject) {
        I18nResourceItemCollection formatterI18nResourceItemCollection = null;
        if (StringUtility.isNullOrEmpty(fielI18nResourceItemBaseId) || formatterObject == null) {
            return formatterI18nResourceItemCollection;
        }

        String formatterI18nResourceItemBaseId = fielI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().GetFormatterName(formatterObject);

        // boolean目前仅用于formatter，后续提取成常量
        if ("boolean".equals(ComponentUtility.getInstance().getType(formatterObject))) {
            formatterI18nResourceItemCollection = new I18nResourceItemCollection();
            String trueTextValue = ComponentUtility.getInstance().GetTrueText(formatterObject);
            String trueTextName = ComponentUtility.getInstance().GetTrueTextName(formatterObject);
            I18nResourceItem trueTextI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, formatterI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + trueTextName, trueTextValue, trueTextValue);
            this.addInCollection(formatterI18nResourceItemCollection, trueTextI18nResourceItem);

            String falseTextValue = ComponentUtility.getInstance().GetFalseText(formatterObject);
            String falseTextName = ComponentUtility.getInstance().GetFalseTextName(formatterObject);
            I18nResourceItem falseTextI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, formatterI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + falseTextName, falseTextValue, falseTextValue);
            this.addInCollection(formatterI18nResourceItemCollection, falseTextI18nResourceItem);

        }

        return formatterI18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractComponentAggregateI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从aggregate属性中提取多语资源项
        HashMap<String, Object> aggregateObject = ComponentUtility.getInstance().GetAggregate(currentComponent);
        String aggregateAttributeName = ComponentUtility.getInstance().GetAggregateAttributeName(currentComponent);
        I18nResourceItemCollection aggregateObjectI18nResourceItemCollection = extractAggregateTemplateI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent, aggregateObject, aggregateAttributeName);
        if (aggregateObjectI18nResourceItemCollection != null && aggregateObjectI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(aggregateObjectI18nResourceItemCollection);
        }

        // 从groupAggregate属性中提取多语资源项
        HashMap<String, Object> groupAggregateObject = ComponentUtility.getInstance().GetGroupAggregate(currentComponent);
        String groupAggregateAttributeName = ComponentUtility.getInstance().GetGroupAggregateAttributeName(currentComponent);
        I18nResourceItemCollection groupAggregateObjectI18nResourceItemCollection = extractAggregateTemplateI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent, groupAggregateObject, groupAggregateAttributeName);
        if (groupAggregateObjectI18nResourceItemCollection != null && groupAggregateObjectI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(groupAggregateObjectI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractAggregateTemplateI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, HashMap<String, Object> aggregateObject, String aggregateAttributeName) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        // 属性为AggregateTemplate的值
        String aggregateTemplateAtributeValue = ComponentUtility.getInstance().GetAggregateTemplate(aggregateObject);

        if (!StringUtility.isNullOrEmpty(aggregateTemplateAtributeValue)) {
            // 使用合并属性的名称作为组件id
            String componentId = aggregateAttributeName;
            String generatedComponentId = currentComponentType + "/" + currentComponentId + "/" + componentId;

            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, aggregateTemplateAtributeValue, aggregateTemplateAtributeValue);
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }
}
