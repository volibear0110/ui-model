/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/02/26
 */
public class ViewChangeI18nResourceStrategy extends AbstractI18nResourceStrategy {
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        // 从toolbarData中提取多语资源项
        List<HashMap<String, Object>> navDataCollection = ComponentUtility.getInstance().getToolbarData(currentComponent);
        String toolbarDataAttributeName = ComponentUtility.getInstance().getToolbarDataName(currentComponent);
        String toolbarDataI18nResourceItemBaseId =
                ComponentUtility.getInstance().getType(currentComponent)
                        + I18nResourceConstant.SECOND_LEVEL_DELIMITER
                        + ComponentUtility.getInstance().getId(currentComponent);

        I18nResourceItemCollection navDataI18nResourceItemCollection =
                extractToolbarDataI18nResourceItemCollection(i18nResourceItemBaseId, toolbarDataI18nResourceItemBaseId, navDataCollection, toolbarDataAttributeName);
        if (navDataI18nResourceItemCollection != null && navDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(navDataI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    /// <summary>
    /// 提取枚举属性中多语资源项
    /// </summary>
    /// <returns></returns>
    private I18nResourceItemCollection extractToolbarDataI18nResourceItemCollection(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId,
                                                                                    List<HashMap<String, Object>> componentCollection, String enumAttributeName) {
        I18nResourceItemCollection enumValuesI18nResourceItemCollection = new I18nResourceItemCollection();

        enumI18nResourceItemBaseId += I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        String finalEnumI18nResourceItemBaseId = enumI18nResourceItemBaseId;
        componentCollection.forEach((component) -> {
            I18nResourceItem i18nResourceItem = getToolbarDataItemI18nResourceItem(i18nResourceItemBaseId, finalEnumI18nResourceItemBaseId + enumAttributeName, component);
            this.addInCollection(enumValuesI18nResourceItemCollection, i18nResourceItem);
        });

        return enumValuesI18nResourceItemCollection;
    }

    /// <summary>
    /// 获取枚举项中多语资源项
    /// </summary>
    /// <param name="enumI18nResourceItemBaseId"></param>
    /// <param name="enumItemObject"></param>
    /// <returns></returns>
    private I18nResourceItem getToolbarDataItemI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> enumItemObject) {
        String textAtributeValue = ComponentUtility.getInstance().getTitle(enumItemObject);
        String idAtributeValue = ComponentUtility.getInstance().getType(enumItemObject);

        String componentId = idAtributeValue;

        String generatedComponentId = enumI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, textAtributeValue, textAtributeValue);


        return i18nResourceItem;
    }
}
