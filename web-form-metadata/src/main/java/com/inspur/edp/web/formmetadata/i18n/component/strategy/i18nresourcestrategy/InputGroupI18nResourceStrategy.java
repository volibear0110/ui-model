/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.HashMap;
import java.util.List;

public class InputGroupI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 使用title作为文本
     */
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        String componentName = componentNameStrategy.getComponentName(component);

        return componentName;
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从placeHolder属性中提取多语字段
        I18nResourceItemCollection placeHolderI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractPlaceHolderI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (placeHolderI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(placeHolderI18nResourceItemCollection);
        }


        I18nResourceItemCollection groupTextI18nResourceItemCollection = this.extractGroupTextI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (groupTextI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(groupTextI18nResourceItemCollection);
        }

        // 填充inputgroup 弹窗
        I18nResourceItemCollection modalConfigResourceItemCollection = extractModalConfigI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (modalConfigResourceItemCollection != null && modalConfigResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(modalConfigResourceItemCollection);
        }
        return i18nResourceItemCollection;
    }

    /**
     * 提取groupText属性中多语资源项
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection extractGroupTextI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String groupTextValue = ComponentUtility.getInstance().getGroupText(currentComponent);
        String groupTextName = "groupText";

        String componentId = groupTextName;
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, groupTextValue, groupTextValue);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractModalConfigI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        HashMap<String, Object> modalConfigComponent = ComponentUtility.getInstance().GetInputGroupModalConfig(currentComponent);
        if (modalConfigComponent == null || modalConfigComponent.size() == 0) {
            return null;
        }

        String modalConfigTitle = ComponentUtility.getInstance().getTitle(modalConfigComponent);

        String generatedComponentId = currentComponentType + "/" + currentComponentId + "/modalconfig/";
        // inputgroup 弹窗标题
        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId + "title", modalConfigTitle, modalConfigTitle);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

        I18nResourceItemCollection footerButtonResourceCollection = extractModalConfigFooterButtonsI18nResourceCollection(i18nResourceItemBaseId, generatedComponentId, currentComponent, modalConfigComponent);
        if (footerButtonResourceCollection.size() > 0) {
            i18nResourceItemCollection.addRange(footerButtonResourceCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractModalConfigFooterButtonsI18nResourceCollection(String i18nResourceItemBaseId, String parentResourceId, HashMap<String, Object> currentComponent, HashMap<String, Object> modalConfigComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        List<HashMap<String, Object>> footerButtonsComponent = ComponentUtility.getInstance().GetFooterButtons(modalConfigComponent);
        if (footerButtonsComponent != null && footerButtonsComponent.size() > 0) {
            footerButtonsComponent.forEach((item) ->
            {
                String footerButtonType = ComponentUtility.getInstance().getType(item);
                String footerButtonId = ComponentUtility.getInstance().getId(item);
                String footerButtonText = ComponentUtility.getInstance().getText(item);
                String generatedComponentId = parentResourceId + "footerButtons/" + footerButtonType + "/" + footerButtonId;
                I18nResourceItem footerButtonI18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, footerButtonText, footerButtonText);

                this.addInCollection(i18nResourceItemCollection, footerButtonI18nResourceItem);
            });
        }
        return i18nResourceItemCollection;
    }

}
