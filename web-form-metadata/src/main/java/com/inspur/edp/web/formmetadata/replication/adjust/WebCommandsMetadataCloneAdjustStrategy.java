/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.command.component.metadata.WebCommandsMetadata;
import com.inspur.edp.web.formmetadata.replication.MetadataReplicationContext;

import java.util.regex.Pattern;

/**
 * web 命令元数据拷贝
 *
 * @author guozhiqi
 */
class WebCommandsMetadataCloneAdjustStrategy implements GspMetadataCloneAdjustStrategy {
    @Override
    public void adjust(GspMetadata replicateMetadata, IMetadataContent replicateMetadataContent, MetadataReplicationContext context) {
        if (!(replicateMetadataContent instanceof WebCommandsMetadata)) {
            return;
        }
        WebCommandsMetadata replicateWebCommandMetadataContent = (WebCommandsMetadata) replicateMetadataContent;
        replicateWebCommandMetadataContent.setId(replicateMetadata.getHeader().getId());
        replicateWebCommandMetadataContent.setCode(replicateMetadata.getHeader().getCode());
        replicateWebCommandMetadataContent.setName(replicateMetadata.getHeader().getName());

        replicateWebCommandMetadataContent.getExtendProperty().setFormCode(replicateMetadata.getHeader().getCode().split(Pattern.quote("_"), -1)[0]);


        com.inspur.edp.web.command.component.serializer.WebCmdMetadataSerializer webCmdMetadataSerializer = new com.inspur.edp.web.command.component.serializer.WebCmdMetadataSerializer();
        JsonNode replicateGSPViewModelStr = webCmdMetadataSerializer.Serialize(replicateWebCommandMetadataContent);

        replicateMetadata.setContent(webCmdMetadataSerializer.DeSerialize(replicateGSPViewModelStr));
    }
}
