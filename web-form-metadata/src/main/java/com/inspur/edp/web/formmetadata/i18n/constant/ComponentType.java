/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.constant;

/**
 * 组件类型
 *
 * @author noah
 */
public final class ComponentType {
    private ComponentType() {
    }
    ///#region 组件类型

    /**
     * 工具栏项
     */
    public static final String TOOL_BAR_ITEM = "ToolBarItem";

    /**
     * 按钮控件
     */
    public static final String BUTTON = "Button";

    /**
     * 列表控件字段
     */
    public static final String GRID_FIELD = "GridField";

    /**
     * 树列表控件字段
     */
    public static final String TREE_GRID_FIELD = "TreeGridField";

    /**
     * 日期控件
     */
    public static final String DATE_BOX = "DateBox";

    /**
     * 文本控件
     */
    public static final String TEXT_BOX = "TextBox";

    /**
     * 日历控件
     */
    public static final String CALENDAR = "Calendar";

    /**
     * Html模板控件
     */
    public static final String HTML_TEMPLATE = "HtmlTemplate";

    /**
     * Section控件
     */
    public static final String SECTION = "Section";

    /**
     * CheckGroup控件
     */
    public static final String CHECK_GROUP = "CheckGroup";

    /**
     * 显示控件
     */
    public static final String DisplayField = "DisplayField";

    /**
     * 多语控件
     */
    public static final String LanguageTextBox = "LanguageTextBox";

    /**
     * 查询方案控件
     */
    public static final String QUERY_SCHEME = "QueryScheme";

    /**
     * 数据列表
     */
    public static final String DATA_GRID = "DataGrid";

    /**
     * 标签
     */
    public static final String TAG = "Tag";

    /**
     * table oa 布局  单元格
     */
    public static  final String TableTd="TableTd";

    /**
     * 附件上传预览组件类型
     */
    public static final String FileUploadPreview = "FileUploadPreview";
    /**
     * 标签
     */
    public static final String TAGS = "Tags";

    /**
     * 过滤条
     */
    public static final String LIST_FILTER = "ListFilter";

    /**
     * 日历组件
     */
    public static final String AppointmentCalendar = "AppointmentCalendar";

    /**
     * 数字框
     */
    public static final String NUMERIC_BOX = "NumericBox";

    /**
     * 智能输入框类型
     */
    public static final String INPUT_GROUP = "InputGroup";

    /**
     * 富文本控件
     */
    public static final String MULTI_TEXT_BOX = "MultiTextBox";

    /**
     * 枚举字段
     */
    public static final String ENUM_FIELD = "EnumField";

    /**
     * 帮助控件
     * 从LookupEdit映射到HelpProvider
     */
    public static final String HELP_PROVIDER = "LookupEdit";

    /**
     * 下拉列表
     */
    public static final String COMBOLIST = "ComboList";

    /**
     * 下拉帮助
     */
    public static final String COMBOLOOKUP = "ComboLookup";

    /**
     * 帮助
     */
    public static final String LOOKUP = "Lookup";

    /**
     * 组织选择类型
     */
    public static final String OrganizationSelector = "OrganizationSelector";

    /**
     * 人员选择类型
     */
    public static final String PersonnelSelector = "PersonnelSelector";

    /**
     * ListNav
     */
    public static final String LIST_NAV = "ListNav";

    /**
     * Sidebar控件
     */
    public static final String SIDEBAR = "Sidebar";

    /**
     * Scrollspy
     */
    public static final String SCROLL_SPY = "Scrollspy";

    public static final String QUERY_FRAMEWORK = "QdpFramework";

    public static final String WIZARD = "Wizard";

    public static final String RADIO_GROUP = "RadioGroup";

    public static final String TimePicker = "TimePicker";

    public static final String TimeSpinner = "TimeSpinner";

    public static final String CheckBox = "CheckBox";

    public static final String TabToolbarItem = "TabToolbarItem";

    public static final String TabPage = "TabPage";

    public static final String Avatar = "Avatar";

    public static final String DatePicker = "DatePicker";

    public static final String RichTextBox = "RichTextBox";

    public static final String Image = "Image";

    public static final String InputGroup = "InputGroup";

    public static final String ListView = "ListView";

    public static final String MultiSelect = "MultiSelect";

    public static final String NumberRange = "NumberRange";

    public static final String NumberSpinner = "NumberSpinner";

    public static final String ExternalContainer = "ExternalContainer";

    public static final String SwitchField = "SwitchField";

    public static final String FieldSet = "FieldSet";

    public static final String Steps = "Steps";

    public static final String Footer = "Footer";

    public static final String Header = "Header";

    public static final String NavTab = "NavTab";

    public static final String ViewChange = "ViewChange";
    ///#endregion
}
