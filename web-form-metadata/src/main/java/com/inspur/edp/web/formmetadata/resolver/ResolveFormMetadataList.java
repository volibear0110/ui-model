/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.resolver;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.utility.ListUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * 待生成json的元数据列表信息
 */
public class ResolveFormMetadataList {

    private ResolveFormMetadataList() {
    }

    public static ResolveFormMetadataList getNewInstance() {
        return new ResolveFormMetadataList();
    }

    private List<ResolveFormMetadataItem> resolveFormMetadataItemList;

    /**
     * @return
     */
    public List<ResolveFormMetadataItem> getResolveFormMetadataItemList() {
        if (this.resolveFormMetadataItemList == null) {
            this.resolveFormMetadataItemList = new ArrayList<>();
        }
        return resolveFormMetadataItemList;
    }

    /**
     * 增加具体的元数据项
     * @param gspMetadata
     * @param isForceDynamicForm
     */
    public void add(GspMetadata gspMetadata, boolean isForceDynamicForm,boolean dynamicFormTag) {
        if (this.getResolveFormMetadataItemList().stream().anyMatch(t -> t.getGspMetadata().equals(gspMetadata))) {
            ResolveFormMetadataItem selectedFormMetadataItem = this.getResolveFormMetadataItemList().stream().filter(t -> t.getGspMetadata().equals(gspMetadata)).findFirst().get();
            selectedFormMetadataItem.setForceDynamicForm(isForceDynamicForm);
            selectedFormMetadataItem.setFormDynamicTag(dynamicFormTag);
        } else {
            ResolveFormMetadataItem resolveFormMetadataItem = new ResolveFormMetadataItem();
            resolveFormMetadataItem.setGspMetadata(gspMetadata);
            resolveFormMetadataItem.setForceDynamicForm(isForceDynamicForm);
            resolveFormMetadataItem.setFormDynamicTag(dynamicFormTag);
            this.getResolveFormMetadataItemList().add(resolveFormMetadataItem);
        }
    }

    /**
     * 是否为空列表
     *
     * @return
     */
    public boolean isEmpty() {
        return ListUtility.isEmpty(this.resolveFormMetadataItemList);
    }

    /**
     * 判断列表不为空
     *
     * @return
     */
    public boolean isNotEmpty() {
        return !this.isEmpty();
    }

}
