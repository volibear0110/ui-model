/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.II18nResource;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.HashMap;

/**
 * 抽象组件
 * 每个组件都需实现II18nResource接口，所以将其提取AbstractComponent，而不是分散到组件实现中
 *
 * @author noah
 */
public class BaseComponent implements IComponent, II18nResource {
    /**
     * 组件实体
     */
    private final HashMap<String, Object> ComponentEntity;

    @Override
    public final HashMap<String, Object> getComponentEntity() {
        return ComponentEntity;
    }

    /**
     * 国际化资源项 根id
     */
    private final String I18nResourceBaseId;

    @Override
    public final String getI18nResourceBaseId() {
        return I18nResourceBaseId;
    }


    private final FormDOM formDOM;

    public final FormDOM getFormDOM() {
        return this.formDOM;
    }


    public BaseComponent(HashMap<String, Object> currentComponent, String i18nResourceBaseId, FormDOM formDOM) {
        this.ComponentEntity = currentComponent;
        this.I18nResourceBaseId = i18nResourceBaseId;
        this.formDOM = formDOM;
    }

    /**
     * 从组件中提取多语资源
     */
    @Override
    public final I18nResourceItemCollection extractI18nResource() {
        // 获取组件对应多语策略，提取多语资源项
        I18nResourceStrategyContext i18nResourceStrategyContext = new I18nResourceStrategyContext(this.getComponentEntity(), this.getI18nResourceBaseId(),this.getFormDOM());
        return i18nResourceStrategyContext.ExtractI18nResource();
    }
}
