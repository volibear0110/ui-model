/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n;

import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.lcm.metadata.spi.MetadataI18nService;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy.ModelToolbarI18nResourceStrategy;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContentService;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentContext;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.ComponentType;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class FormMetadataI18nService implements MetadataI18nService {
    /**
     * 从基础元数据中抽取资源元数据内容
     *
     * @param metadata 待提取元数据
     * @return
     */
    @Override
    public final I18nResource getResourceItem(GspMetadata metadata) {
        if (metadata == null || metadata.getContent() == null) {
            return null;
        }
        return GetFormMetadataI18nResource(metadata);
    }

    /**
     * 合并（将资源对应的多语资源整合到当前元数据中）
     *
     * @param metadata
     * @param resources
     * @return
     */
    @Override
    public final GspMetadata merge(GspMetadata metadata, List<I18nResource> resources) {

        return metadata;
    }

    /**
     * 根据表单元数据获取对应的资源项列表
     *
     * @param formMetadata
     * @return
     */
    private I18nResource GetFormMetadataI18nResource(GspMetadata formMetadata) {
        FormMetadataContent formMetadataContent = (FormMetadataContent) ((formMetadata.getContent() instanceof FormMetadataContent) ? formMetadata.getContent() : null);
        if (formMetadataContent == null) {
            return null;
        }

        MetadataHeader formMetadataHeader = formMetadata.getHeader();

        String i18nResourceItemBaseId = formMetadataHeader.getNameSpace() + I18nResourceConstant.FIRST_LEVEL_DELIMITER +
                formMetadataHeader.getCode() + I18nResourceConstant.FIRST_LEVEL_DELIMITER +
                formMetadataHeader.getType() + I18nResourceConstant.FIRST_LEVEL_DELIMITER;

        I18nResource i18nResource = new I18nResource();
        i18nResource.setResourceType(ResourceType.Metadata);
        i18nResource.setResourceLocation(ResourceLocation.Frontend);
        i18nResource.setLanguage(formMetadata.getHeader().getLanguage());

        I18nResourceItemCollection i18nResourceItemCollection = this.GetI18nResourceItemCollection(formMetadataContent, i18nResourceItemBaseId, formMetadataHeader.getType());
        i18nResource.setStringResources(i18nResourceItemCollection);

        return i18nResource;
    }

    /**
     * 从表单元数据内容中提取多语资源
     *
     * @param formMetadataContent
     * @return
     */
    private I18nResourceItemCollection GetI18nResourceItemCollection(FormMetadataContent formMetadataContent, String i18nResourceItemBaseId, String formType) {
        // 从表单元数据中提取多语字段
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        // 表单元数据反序列化成FormDom
        FormDOM formContent = FormMetadataContentService.getInstance().getFormContent(formMetadataContent);
        if (formContent == null || formContent.getModule() == null || formContent.getModule().getComponents() == null) {
            return i18nResourceItemCollection;
        }

        // 设置表单类型（MobileForm=移动表单）
        formContent.setFormType(formType);

        // 提取modal  工具栏
        I18nResourceItemCollection modalToolbarResourceCollection = new ModelToolbarI18nResourceStrategy().extractModalToolbar(formContent, i18nResourceItemBaseId);
        if (modalToolbarResourceCollection != null && !modalToolbarResourceCollection.isEmpty()) {
            i18nResourceItemCollection.addRange(modalToolbarResourceCollection);
        }

        ArrayList<HashMap<String, Object>> components = formContent.getModule().getComponents();
        // 递归提取每个component中的组件及其对应的待国际化的值
        for (HashMap<String, Object> component : components) {
            // 每个component是一个树结构
            // 表单树表现出浅而宽的特性，所以优先使用深度优先算法
            if (component != null) {
                DepthFirstSearchTraverse(i18nResourceItemCollection, i18nResourceItemBaseId, component, formContent);
            }
        }

        // 移动：递归提取expressions中对应的待国际化的值
        if("MobileForm".equals(formType)){
            List<HashMap<String, Object>> expressions = formContent.getModule().getExpressions();
            if(expressions != null) {
                for (HashMap<String, Object> expression : expressions) {
                    if (expression != null) {
                        extractExpressionI18nResource(i18nResourceItemCollection,i18nResourceItemBaseId,expression);
                    }
                }
            }
        }


        return i18nResourceItemCollection;
    }

    private void extractExpressionI18nResource(I18nResourceItemCollection i18nResourceItemCollection,String i18nResourceItemBaseId,HashMap<String,Object> expression){
        if(expression.get("expression") == null || expression.get("fieldId") == null) {
            return;
        }

        List<HashMap<String, String>> expressionItems = (List<HashMap<String, String>>)expression.get("expression");

        for(HashMap<String, String> expressionItem : expressionItems) {
            if(expressionItem == null){
                continue;
            }

            String type = expressionItem.get("type");
            if(!"require".equals(type) && !"validate".equals(type)){
                continue;
            }

            String id = expressionItem.get("id");
            String message = expressionItem.get("message");
            String path = "Expression/"+type+"/"+id;

            I18nResourceItem expressionResourceItem =  I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, path, message, message);
            i18nResourceItemCollection.add(expressionResourceItem);
        }

    }


    /**
     * 深度优先搜索遍历(DFS)
     *
     * @param i18nResourceItemCollection 资源项列表
     * @param i18nResourceItemBaseId     资源项根id
     * @param component                  表单component
     * @return
     */
    public ArrayList<Object> DepthFirstSearchTraverse(I18nResourceItemCollection i18nResourceItemCollection, String i18nResourceItemBaseId, HashMap<String, Object> component, FormDOM formDOM) {
        ArrayList<Object> list = new ArrayList<>();

        Stack<HashMap<String, Object>> componentStack = new Stack<>();
        // 将当前节点入栈
        componentStack.push(component);
        while (componentStack.size() != 0) {
            // 节点出栈
            HashMap<String, Object> currentComponent = componentStack.pop();
            if (currentComponent == null) {
                break;
            }

            String componentType = ComponentUtility.getInstance().getType(currentComponent);
            // 处理当前出栈的节点  提取对应的资源项
            HandlePoppedNode(i18nResourceItemCollection, i18nResourceItemBaseId, currentComponent, formDOM);

            // 兄弟节点压栈
            AppendSiblingNode(componentStack, currentComponent);

            // 子节点压栈
            // 由于checkGroup中包含items属性且不包含具体的类型 所以针对checkGroup 不读取其items
            if (!StringUtility.isNullOrEmpty(componentType) &&
                    !"CheckGroup".equals(componentType)) {
                AppendChildrenNode(componentStack, currentComponent);
            }
        }

        return list;
    }

    /**
     * 子节点入栈
     *
     * @param componentStack
     * @param currentComponent
     */
    private void AppendChildrenNode(Stack<HashMap<String, Object>> componentStack, HashMap<String, Object> currentComponent) {
        if (currentComponent == null || currentComponent.isEmpty()) {
            return;
        }
        ArrayList<HashMap<String, Object>> childComponentCollection = GetChildComponents(currentComponent);
        for (int i = childComponentCollection.size() - 1; i >= 0; i--) {
            HashMap<String, Object> childComponent = childComponentCollection.get(i);
            if (childComponent != null) {
                componentStack.push(childComponent);
            }
        }
    }

    /**
     * 追加兄弟节点：特殊处理非子组件属性中。如tabPage中的toolbar
     * {"contents":{},"toolbar":{}, "editor":{}}
     *
     * @param componentStack
     * @param currentComponent
     */
    private static void AppendSiblingNode(Stack<HashMap<String, Object>> componentStack, HashMap<String, Object> currentComponent) {
        String componentType = ComponentUtility.getInstance().getType(currentComponent);
        if (!StringUtility.isNullOrEmpty(componentType) && componentType.equals(ComponentType.TabPage)) {
            HashMap<String, Object> toolbarObject = ComponentUtility.getInstance().GetToolbar(currentComponent);
            if (toolbarObject != null) {
                componentStack.push(toolbarObject);
            }
        }
    }


    /**
     * 提取当前节点的国际化翻译项
     *
     * @param i18nResourceItemCollection 国际化资源项
     * @param i18nResourceItemBaseId     baseId
     * @param currentComponent           当前待处理的节点
     */
    private void HandlePoppedNode(I18nResourceItemCollection i18nResourceItemCollection, String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, FormDOM formDOM) {
        I18nResourceItemCollection currentI18nResourceItemCollection = GetI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent, formDOM);
        if (currentI18nResourceItemCollection != null && currentI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(currentI18nResourceItemCollection);
        }
    }

    /**
     * 提取具体的资源项
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection GetI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, FormDOM formDOM) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 获取组件的多语资源项
        ComponentContext componentI18nResourceContext = new ComponentContext(currentComponent, i18nResourceItemBaseId, formDOM);
        I18nResourceItemCollection componentI18nResourceItemCollection = componentI18nResourceContext.extractI18nResource();
        if (componentI18nResourceItemCollection != null && componentI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(componentI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    /**
     * 获取子组件列表
     *
     * @param component
     * @return
     */
    private ArrayList<HashMap<String, Object>> GetChildComponents(HashMap<String, Object> component) {
        ArrayList<HashMap<String, Object>> childCompnentCollection = new ArrayList<>();

        // 获取组件属性中包含的子组件
        ArrayList<HashMap<String, Object>> childAttribtueCompnentCollection = GetAttribtueChildComponents(component);
        if (childAttribtueCompnentCollection != null && !childAttribtueCompnentCollection.isEmpty()) {
            childCompnentCollection.addAll(childAttribtueCompnentCollection);
        }

        Object contents = component.get("contents");
        if (contents == null) {
            // 尝试从items中获取子节点信息（兼容使用items属性存储子节点的场景，如ToolBar）
            contents = component.get("items");
            if (contents == null) {
                // 兼容datagrid
                contents = component.get("fields");
            }
            // 兼容OA 模板  table 结构布局
            if (contents == null && ComponentUtility.getInstance().getType(component).equals("Table")) {
                contents = component.get("rows");
            }
            // 兼容OA 模板 table 结构布局
            if (contents == null && ComponentUtility.getInstance().getType(component).equals("TableRow")) {
                contents = component.get("columns");
            }
            // 兼容OA 模板 table结构布局 提取ed中的editor
            if (contents == null && ComponentUtility.getInstance().getType(component).equals("TableTd") &&
                    ComponentUtility.getInstance().getValue(component, "tdType").equals("editor")) {
                List<Object> editorList = new ArrayList<>();
                editorList.add(component.get("editor"));
                contents = editorList;
            }
            if (contents == null) {
                return childCompnentCollection;
            }
        }

        // 如何从object转换成List<Dictionary<string, object>>
        // https://stackoverflow.com/questions/632570/cast-received-object-to-a-listobject-or-ienumerableobject
        ArrayList<Object> contentsCollection = new ArrayList<>((List<Object>) contents);
        for (Object localComponent : contentsCollection) {
            // 从object转换成Dictionary<string, object>
            HashMap<String, Object> convertObject = ConvertToDictionaryObject(localComponent);
            childCompnentCollection.add(convertObject);
        }

        return childCompnentCollection;
    }

    /**
     * 侧边栏或tabPage的工具栏
     *
     * @param component
     * @return
     */
    private ArrayList<HashMap<String, Object>> GetAttribtueChildComponents(HashMap<String, Object> component) {
        ArrayList<HashMap<String, Object>> childAttribtueCompnentCollection = new ArrayList<>();
        // 侧边栏 或tab的工具栏
        if (ComponentUtility.getInstance().getType(component).equals(ComponentType.SIDEBAR)) {
            HashMap<String, Object> toolbarAttributeObject = ComponentUtility.getInstance().GetToolbar(component);
            // 仅在存在toolbar时处理
            if (toolbarAttributeObject != null) {
                childAttribtueCompnentCollection.addAll(ComponentUtility.getInstance().GetItems(toolbarAttributeObject));
            }
            return childAttribtueCompnentCollection;
        }


        return null;
    }

    // 如何从object转换成Dictionary<string, object>
    // https://stackoverflow.com/questions/11576886/how-to-convert-object-to-dictionarytkey-tvalue-in-c
    private static HashMap<String, Object> ConvertToDictionaryObject(Object targetObject) {
        String json = SerializeUtility.getInstance().serialize(targetObject, false);
        return SerializeUtility.getInstance().deserialize(json, HashMap.class);
    }

}
