/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.formdom;

import com.inspur.edp.web.formmetadata.metadata.module.ModuleService;

/** 
 表单DOM服务
 @author  noah
*/
public class FormDomService
{
		///#region Singleton
	private FormDomService()
	{

	}

	private static final FormDomService formDomService = new FormDomService();
	public static FormDomService GetInstance()
	{
		return formDomService;
	}

	public final String GetLanguage(FormDOM formDOM)
	{
		if (formDOM == null)
		{
			return null;
		}

		return ModuleService.GetInstance().GetLanguage(formDOM.getModule());
	}



}
