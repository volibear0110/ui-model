/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 枚举字段国际化参数提取
 * @author  noah
 */
public class EnumFieldI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 使用title作为文本
     */
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        return componentNameStrategy.getComponentName(component);
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从placeHolder属性中提取多语字段
        I18nResourceItemCollection placeHolderI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractPlaceHolderI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (placeHolderI18nResourceItemCollection != null && placeHolderI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(placeHolderI18nResourceItemCollection);
        }

        String strValueFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.IdFieldKey, I18nResourceConstant.ValueFieldKey);
        String strTextFieldKey = ComponentUtility.getInstance().getValueWithKey(currentComponent, I18nResourceConstant.TextFieldKey);

        // 从enumData中提取多语资源项
        ArrayList<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumData(currentComponent);
        String enumDataAttributeName = ComponentUtility.getInstance().GetEnumDataName(currentComponent);
        String enumDataI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent);
        I18nResourceItemCollection enumDataI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, enumDataI18nResourceItemBaseId, enumValueCollection, enumDataAttributeName, strValueFieldKey, strTextFieldKey);
        if (enumDataI18nResourceItemCollection != null && enumDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(enumDataI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }
}
