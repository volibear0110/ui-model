/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.HashMap;
import java.util.List;

/**
 * 日历组件参数提取
 *
 * @author guozhiqi
 */
public class AppointmentCalendarResourceStrategy extends AbstractI18nResourceStrategy {

    /**
     * 参数提取
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }
        // 提取placeTitle参数
        String placeTitleValue = ComponentUtility.getInstance().getPlaceTitle(currentComponent);
        String placeTitleName = "placeTitle";
        String generatedPlaceTitleComponentId = currentComponentType + "/" + currentComponentId + "/" + placeTitleName;
        I18nResourceItem i18nPlaceholderResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedPlaceTitleComponentId, placeTitleValue, placeTitleValue);
        this.addInCollection(i18nResourceItemCollection, i18nPlaceholderResourceItem);

        // 提取placeTitle参数
        String reservationTitleValue = ComponentUtility.getInstance().getReservationTitle(currentComponent);
        String reservationTitleName = "reservationTitle";
        String generatedReservationTitleComponentId = currentComponentType + "/" + currentComponentId + "/" + reservationTitleName;
        I18nResourceItem i18nReservationTitleResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedReservationTitleComponentId, reservationTitleValue, reservationTitleValue);
        this.addInCollection(i18nResourceItemCollection, i18nReservationTitleResourceItem);

        // 提取ContentTemplate的国际化参数值
        HashMap<String, Object> contentTemplateComponent = ComponentUtility.getInstance().getContentTemplate(currentComponent);
        if (contentTemplateComponent != null && contentTemplateComponent.size() > 0) {
            List<HashMap<String, Object>> contentTemplateFields = ComponentUtility.getInstance().GetFieldsFromContentTemplate(contentTemplateComponent);
            String contentTemplateResourceId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                    currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "contentTemplate";
            extractTemplateFields(i18nResourceItemBaseId, i18nResourceItemCollection, contentTemplateComponent, contentTemplateFields, contentTemplateResourceId);
        }

        // 提取placeNameTemplate
        HashMap<String, Object> placeNameTemplateComponent = ComponentUtility.getInstance().getPlaceNameTemplate(currentComponent);
        if (placeNameTemplateComponent != null && placeNameTemplateComponent.size() > 0) {
            List<HashMap<String, Object>> placeNameTemplateFields = ComponentUtility.getInstance().GetFieldsFromContentTemplate(placeNameTemplateComponent);
            String placeNameTemplateResourceId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                    currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getPlaceNameTemplateAttributeName();
            extractTemplateFields(i18nResourceItemBaseId, i18nResourceItemCollection, placeNameTemplateComponent, placeNameTemplateFields, placeNameTemplateResourceId);
        }

        // 提取detailBadgeTemplate
        HashMap<String, Object> detailBadgeTemplateComponent = ComponentUtility.getInstance().getDetailBadgeTemplate(currentComponent);
        if (detailBadgeTemplateComponent != null && detailBadgeTemplateComponent.size() > 0) {
            List<HashMap<String, Object>> detailBadgeTemplateFields = ComponentUtility.getInstance().GetFieldsFromContentTemplate(detailBadgeTemplateComponent);
            String detailBadgeTemplateResourceId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                    currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getDetailBadgeTemplateAttributeName();
            extractTemplateFields(i18nResourceItemBaseId, i18nResourceItemCollection, detailBadgeTemplateComponent, detailBadgeTemplateFields, detailBadgeTemplateResourceId);
        }


        // 提取detailColumns的字段定义
        List<HashMap<String, Object>> detailColumnsComponent = ComponentUtility.getInstance().getDetailColumns(currentComponent);
        if (detailColumnsComponent != null && detailColumnsComponent.size() > 0) {
            // 提取对应的title标题
            detailColumnsComponent.forEach((item) -> {
                String detailColumnId = this.getComponentId(item);
                String detailBadgeTemplateResourceId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                        currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getDetailColumnsAttributeName() + I18nResourceConstant.SECOND_LEVEL_DELIMITER + detailColumnId;
                String detailColumnTitleValue = ComponentUtility.getInstance().getTitle(item);
                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, detailBadgeTemplateResourceId, detailColumnTitleValue, detailColumnTitleValue);
                this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
            });
        }

        return i18nResourceItemCollection;
    }

    /**
     * 提取Html 模板中的字段定义
     *
     * @param i18nResourceItemBaseId
     * @param i18nResourceItemCollection
     * @param templateComponent
     * @param templateFields
     * @param templateResourceId
     */
    private void extractTemplateFields(String i18nResourceItemBaseId, I18nResourceItemCollection i18nResourceItemCollection, HashMap<String, Object> templateComponent, List<HashMap<String, Object>> templateFields, String templateResourceId) {
        I18nResourceItemCollection resourceItemCollection = extractFieldsI18nResourceItemCollection(i18nResourceItemBaseId, templateResourceId, templateFields);
        if (resourceItemCollection != null) {
            i18nResourceItemCollection.addRange(resourceItemCollection);
        }
        HashMap<String, Object> contentTemplateToolbar = ComponentUtility.getInstance().GetAttributeObject(templateComponent, "toolbar");
        if (contentTemplateToolbar != null) {
            List<HashMap<String, Object>> contentTemplateToolbarContents = ComponentUtility.getInstance().GetToolbarContentsFromContentTemplate(contentTemplateToolbar);
            if (contentTemplateToolbarContents != null && contentTemplateToolbarContents.size() > 0) {
                I18nResourceItemCollection toolbarContentsResourceCollection = extractToolbarContentsI18nResourceItemCollection(i18nResourceItemBaseId, templateResourceId, contentTemplateToolbarContents);
                if (toolbarContentsResourceCollection != null) {
                    i18nResourceItemCollection.addRange(toolbarContentsResourceCollection);
                }
            }
        }
    }

    /// <summary>
    /// 提取枚举属性中多语资源项
    /// </summary>
    /// <returns></returns>
    private I18nResourceItemCollection extractToolbarContentsI18nResourceItemCollection(String i18nResourceItemBaseId,
                                                                                        String appointmentCalendarI18nResourceItemBaseId,
                                                                                        List<HashMap<String, Object>> componentCollection) {
        I18nResourceItemCollection templateI18nResourceItemCollection = new I18nResourceItemCollection();
        componentCollection.forEach((component) -> {
            String textAtributeValue = ComponentUtility.getInstance().getText(component);
            String idAtributeValue = ComponentUtility.getInstance().getId(component);
            String typeAttributeValue = ComponentUtility.getInstance().getType(component);

            String generatedComponentId =
                    appointmentCalendarI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "toobar" + I18nResourceConstant.SECOND_LEVEL_DELIMITER + typeAttributeValue + I18nResourceConstant.SECOND_LEVEL_DELIMITER + idAtributeValue;

            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, textAtributeValue, textAtributeValue);

            this.addInCollection(templateI18nResourceItemCollection, i18nResourceItem);
        });


        return templateI18nResourceItemCollection;
    }


    /// <summary>
    /// 提取枚举属性中多语资源项
    /// </summary>
    /// <returns></returns>
    private I18nResourceItemCollection extractFieldsI18nResourceItemCollection(String i18nResourceItemBaseId,
                                                                               String appointmentCalendarI18nResourceItemBaseId,
                                                                               List<HashMap<String, Object>> componentCollection) {
        I18nResourceItemCollection contentTemplateI18nResourceItemCollection = new I18nResourceItemCollection();

        componentCollection.forEach((component) -> {
            I18nResourceItemCollection i18nResourceItem = getContentTemplateFieldsI18nResourceItem(i18nResourceItemBaseId, appointmentCalendarI18nResourceItemBaseId, component);
            if (i18nResourceItem != null) {
                contentTemplateI18nResourceItemCollection.addRange(i18nResourceItem);
            }
        });


        return contentTemplateI18nResourceItemCollection;
    }

    /// <summary>
    /// 提取contentTemplate中fields中的具体项
    /// </summary>
    /// <param name="enumI18nResourceItemBaseId"></param>
    /// <param name="fieldItemObject"></param>
    /// <returns></returns>
    private I18nResourceItemCollection getContentTemplateFieldsI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> fieldItemObject) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String nameAtributeValue = ComponentUtility.getInstance().getName(fieldItemObject);
        String idAtributeValue = ComponentUtility.getInstance().getId(fieldItemObject);
        String componentId = idAtributeValue;

        String generatedComponentId = enumI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER +
                ComponentUtility.getInstance().getType(fieldItemObject) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
        this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

        I18nResourceItemCollection fieldEnumResource = getContentTemplateFieldsI18nResourceItemEnumData(i18nResourceItemBaseId, generatedComponentId, fieldItemObject);
        if (fieldEnumResource != null) {
            i18nResourceItemCollection.addRange(fieldEnumResource);
        }
        return i18nResourceItemCollection;
    }

    /// <summary>
    /// 提取contentTemplate中fields中的具体项
    /// </summary>
    /// <param name="enumI18nResourceItemBaseId"></param>
    /// <param name="fieldItemObject"></param>
    /// <returns></returns>
    private I18nResourceItemCollection getContentTemplateFieldsI18nResourceItemEnumData(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> fieldItemObject) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String strValueFieldKey = "value";
        String strTextFieldKey = "name";

        // 从enumData中提取多语资源项
        List<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumData(fieldItemObject);
        String enumDataAttributeName = ComponentUtility.getInstance().GetEnumDataName(fieldItemObject);

        I18nResourceItemCollection enumDataI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, enumI18nResourceItemBaseId, enumValueCollection, enumDataAttributeName, strValueFieldKey, strTextFieldKey);
        if (enumDataI18nResourceItemCollection != null && enumDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(enumDataI18nResourceItemCollection);
        }
        return i18nResourceItemCollection;
    }

}
