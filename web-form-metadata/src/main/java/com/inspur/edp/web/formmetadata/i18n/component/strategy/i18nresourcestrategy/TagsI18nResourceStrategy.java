/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

public class TagsI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 使用title作为文本
     */
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        String componentName = componentNameStrategy.getComponentName(component);

        return componentName;
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        // 从tagData中提取多语资源项
        ArrayList<HashMap<String, Object>> tagDataCollection = ComponentUtility.getInstance().GetTagData(currentComponent);
        String enumDataAttributeName = ComponentUtility.getInstance().GetTagDataName(currentComponent);
        String tagDataI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent);

        I18nResourceItemCollection tagDataI18nResourceItemCollection = extractTagDataI18nResourceItemCollection(i18nResourceItemBaseId, tagDataI18nResourceItemBaseId, tagDataCollection, enumDataAttributeName);
        if (tagDataI18nResourceItemCollection != null && tagDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(tagDataI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }


    /**
     * 提取枚举属性中多语资源项
     *
     * @return
     */
    private I18nResourceItemCollection extractTagDataI18nResourceItemCollection(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, ArrayList<HashMap<String, Object>> componentCollection, String enumAttributeName) {
        I18nResourceItemCollection enumValuesI18nResourceItemCollection = new I18nResourceItemCollection();

        enumI18nResourceItemBaseId += I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        for (HashMap<String, Object> component : componentCollection) {
            I18nResourceItem i18nResourceItem = getTagDataItemI18nResourceItem(i18nResourceItemBaseId, enumI18nResourceItemBaseId + enumAttributeName, component);
            this.addInCollection(enumValuesI18nResourceItemCollection, i18nResourceItem);
        }

        return enumValuesI18nResourceItemCollection;
    }

    /**
     * 获取枚举项中多语资源项
     *
     * @param enumI18nResourceItemBaseId
     * @param enumItemObject
     * @return
     */
    private I18nResourceItem getTagDataItemI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> enumItemObject) {
        I18nResourceItem i18nResourceItem = null;


        String nameAtributeValue = ComponentUtility.getInstance().getName(enumItemObject);
        String idAtributeValue = ComponentUtility.getInstance().getId(enumItemObject);
        if (!StringUtility.isNull(nameAtributeValue)) {
            String componentId = idAtributeValue;

            String generatedComponentId = enumI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

            i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
        }

        return i18nResourceItem;
    }
}
