package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import org.apache.commons.lang3.StringUtils;

/**
 * 输入控件策略提取策略
 */
public class MobileInputComponentI18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    public MobileInputComponentI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        this.collectTitleAttrResourceItem();
        this.collectLabelTipsAttrResourceItem();
        this.collectPlaceholderAttrResourceItem();

        if (this.componentType.equals(ComponentTypes.RadioGroup)
                || this.componentType.equals(ComponentTypes.CheckGroup)
                || this.componentType.equals(ComponentTypes.EnumField)
        ) {
            this.collectEnumDataAttrResourceItems();
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集Input标题相关资源项
     */
    private void collectTitleAttrResourceItem() {
        this.collectSimpleAttrResourceItem("title");
        if (this.componentType.equals(ComponentTypes.LookupEdit)
        ) {
            this.collectSimpleAttrResourceItem("navTitle");
            this.collectSimpleAttrResourceItem("groupTitle");

        }
    }

    /**
     * 搜集Input输入提示相关资源项
     */
    private void collectLabelTipsAttrResourceItem() {
        String[] noLabelTipsComponentTypes = {
                ComponentTypes.PersonnelSelector,
                ComponentTypes.OrganizationSelector,
                ComponentTypes.ScanTextBox,
        };
        boolean hasLabelTips = !Arrays.asList(noLabelTipsComponentTypes).contains(this.componentType);
        if (hasLabelTips) {
            this.collectSimpleAttrResourceItem("labelTips");
        }
    }

    /**
     * 搜集Input占位符相关资源项
     */
    private void collectPlaceholderAttrResourceItem() {
        if (this.componentType.equals(ComponentTypes.RadioGroup)
                || this.componentType.equals(ComponentTypes.CheckGroup)
                || this.componentType.equals(ComponentTypes.SwitchField)
        ) {
            return;
        }
        if (this.componentType.equals(ComponentTypes.NumericBox)) {
            this.collectSimpleAttrResourceItem("suffix");
        }
        this.collectSimpleAttrResourceItem("placeholder");
        this.collectValidationRulesAttrResourceItems();
    }

    /**
     * 搜集枚举数据项集合的资源项
     */
    private void collectEnumDataAttrResourceItems() {
        if (!this.componentType.equals(ComponentTypes.RadioGroup) &&
                !this.componentType.equals(ComponentTypes.EnumField) &&
                !this.componentType.equals(ComponentTypes.CheckGroup)
        ) {
            return;
        }

        ArrayList<HashMap<String, Object>> items = null;
        String itemsPath = null;

        if (this.componentType.equals(ComponentTypes.CheckGroup)) {
            items = this.componentUtility.GetItems(this.component);
            itemsPath = this.getComponentAttrPath("items");
        } else {
            items = this.componentUtility.GetEnumData(this.component);
            itemsPath = this.getComponentAttrPath("enumData");
        }


        String textField = this.getComponentAttr(this.component, "textField");
        String valueField = this.getComponentAttr(this.component, "valueField");
        if (StringUtils.isBlank(textField) || StringUtils.isBlank(valueField)) {
            return;
        }
        for (HashMap<String, Object> item : items) {
            String itemVal = this.getComponentAttr(item, valueField);
            String itemName = this.getComponentAttr(item, textField);
            String itemPath = itemsPath + "/" + itemVal;

            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemName, itemName);
            this.resourceItemCollection.add(resourceItem);
        }
    }

    /**
     * 搜集校验信息集合的资源项
     */
    private void collectValidationRulesAttrResourceItems() {
        ArrayList<HashMap<String, Object>> validationRules = this.componentUtility.GetValidationRulesData(this.component);
        if (validationRules == null || validationRules.size() == 0) {
            return;
        }

        String itemsPath = this.getComponentAttrPath("validationRules");

        for (HashMap<String, Object> validationRule : validationRules) {
            String itemVal = this.getComponentAttr(validationRule, "id");
            String itemName = this.getComponentAttr(validationRule, "message");
            if (StringUtils.isNotBlank(itemVal) && StringUtils.isNotBlank(itemName)) {
                String itemPath = itemsPath + "/" + itemVal;
                I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemName, itemName);
                this.resourceItemCollection.add(resourceItem);
            }
        }
    }

}
