/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;


import java.util.ArrayList;
import java.util.HashMap;

public class ScrollSpyI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String itemsI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        I18nResourceItemCollection itemsI18nResourceItemCollection = extractItemsI18nResourceItemCollection(i18nResourceItemBaseId, itemsI18nResourceItemBaseId, currentComponent);
        if (itemsI18nResourceItemCollection != null && itemsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(itemsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractItemsI18nResourceItemCollection(String i18nResourceItemBaseId, String itemsI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从items属性中提取多语资源
        itemsI18nResourceItemBaseId += ComponentUtility.getInstance().GetItemsName(currentComponent);
        itemsI18nResourceItemBaseId += I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        ArrayList<HashMap<String, Object>> childComponents = ComponentUtility.getInstance().GetItems(currentComponent);

        I18nResourceItemCollection childComponentsI18nResourceItemCollection = extractChildComponentI18nResourceItemCollection(i18nResourceItemBaseId, itemsI18nResourceItemBaseId, childComponents);
        if (childComponentsI18nResourceItemCollection != null && childComponentsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(childComponentsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractChildComponentI18nResourceItemCollection(String i18nResourceItemBaseId, String itemsI18nResourceItemBaseId, ArrayList<HashMap<String, Object>> childComponentList) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        for (HashMap<String, Object> childComponent : childComponentList) {
            String idAtributeValue = ComponentUtility.getInstance().getId(childComponent);
            String titleAtributeValue = ComponentUtility.getInstance().getTitle(childComponent);
            if (!StringUtility.isNull(idAtributeValue)) {
                String generatedComponentId = idAtributeValue;

                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, itemsI18nResourceItemBaseId + generatedComponentId, titleAtributeValue, titleAtributeValue);
                this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
            }
        }

        return i18nResourceItemCollection;
    }
}
