package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * 业务控件策略提取策略
 */
public class MobileBizComponentI18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    public MobileBizComponentI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        switch (this.componentType) {
            case ComponentTypes.QueryScheme: {
                this.collectQuerySchemeResourceItems();
                this.collectFieldConfigsAttrResourceItems();
                break;
            }
            case ComponentTypes.ApprovalInfo: {
                this.collectSimpleAttrResourceItem("code");
                this.collectSimpleAttrResourceItem("name");
                break;
            }
            case ComponentTypes.ApprovalComments:
            case ComponentTypes.ApprovalLogsPanel:
                this.collectSimpleAttrResourceItem("title");
                break;
            case ComponentTypes.PersonnelInfo:
                this.collectSimpleAttrResourceItem("titleText");
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集QueryScheme相关的资源项
     */
    private void collectQuerySchemeResourceItems() {
        this.collectSimpleAttrResourceItem("placeholder");
    }

    /**
     * 搜集校验信息集合的资源项
     */
    private void collectFieldConfigsAttrResourceItems() {
        ArrayList<HashMap<String, Object>> fieldConfigs = this.componentUtility.GetFieldConfigs(this.component);
        if (fieldConfigs == null || fieldConfigs.size() == 0) {
            return;
        }
        // placeholder
        List<String> attrs = Arrays.asList("placeholder", "enumValues");
        String itemsPath = this.getComponentAttrPath("fieldConfigs");

        for (HashMap<String, Object> fieldConfig : fieldConfigs) {
            String fieldConfigPath = itemsPath + "/" + fieldConfig.get("id");
            // 筛选方案标题
            if (fieldConfig.containsKey("name")) {
                String itemPath = fieldConfigPath + "/name";
                String itemName = this.getComponentAttr(fieldConfig, "name");
                I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemName, itemName);
                this.resourceItemCollection.add(resourceItem);
            }

            HashMap<String, Object> control = this.componentUtility.GetAttributeObject(fieldConfig, "control");
            if (control == null || !control.containsKey("id")) {
                continue;
            }
            String controlPath = fieldConfigPath + "/control";

            for (String attr : attrs) {
                if (control.containsKey(attr)) {
                    // 筛选方案枚举值
                    if (attr.equals("enumValues")) {
                        ArrayList<HashMap<String, Object>> enumValues = this.componentUtility.GetEnumValues(control);
                        if (enumValues == null) {
                            continue;
                        }
                        for (HashMap<String, Object> enumValue : enumValues) {
                            String itemName = this.getComponentAttr(enumValue, "name");
                            String itemVal = this.getComponentAttr(enumValue, "value");

                            String itemPath = controlPath + "/enumValues/" + itemVal;

                            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemName, itemName);
                            this.resourceItemCollection.add(resourceItem);
                        }
                        continue;
                    }
                    // 筛选方案输入控件占位符
                    String itemPath = controlPath + "/" + attr;
                    String itemName = this.getComponentAttr(control, attr);
                    I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemName, itemName);
                    this.resourceItemCollection.add(resourceItem);
                }
            }
        }
    }

}
