/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.module;

/**
 * 模块服务
 */
public class ModuleService {
    ///Singleton
    private ModuleService() {

    }

    private static final ModuleService moduleService = new ModuleService();

    public static ModuleService GetInstance() {
        return moduleService;
    }

    public final String GetLanguage(Module module) {
        if (module == null || module.getI18nSetting() == null) {
            return null;
        }

        return module.getI18nSetting().getlanguage();
    }

}
