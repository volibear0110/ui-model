package com.inspur.edp.web.formmetadata.i18n.component.mobile.constants;

/**
 * 控件分组常量
 */
public class ComponentGroups {
    public final static String BASIC = "basic";
    public final static String CONTAINER = "container";
    public final static String INPUT = "input";
    public final static String DISPLAY = "display";
    public final static String BIZ = "biz";
    public final static String COLLECTION = "collection";
    public final static String NAV = "nav";
    public final static String OTHER = "other";
}
