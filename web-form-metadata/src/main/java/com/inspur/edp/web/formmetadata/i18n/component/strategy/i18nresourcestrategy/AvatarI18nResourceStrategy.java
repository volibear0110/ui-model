/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;

import java.util.HashMap;

/**
 * 头像组件国际化参数提取
 * @author guozhiqi
 */
public class AvatarI18nResourceStrategy extends  AbstractI18nResourceStrategy{
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection=new I18nResourceItemCollection();

        String currentComponentType = this.getCurrentComponentType(currentComponent);
        String currentComponentId = this.getCurrentComponentId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        // 使用合并属性的名称作为组件id
        // 提取Image的alt属性参数值
        String imgTitleResourceKey = ComponentUtility.getInstance().getImgTitleAttributeName();
        String generatedImgTitleComponentId = currentComponentType + "/" + currentComponentId + "/" + imgTitleResourceKey;
        String imgTitleValue = ComponentUtility.getInstance().getImgTitle(currentComponent);
        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedImgTitleComponentId, imgTitleValue, imgTitleValue);
        this.addInCollection(i18nResourceItemCollection,i18nResourceItem);

        return i18nResourceItemCollection;

    }
}
