/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.entity.ResultCode;
import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.formmetadata.constant.I18nMsgConstant;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

/**
 * 元数据拷贝前参数及必要条件验证
 *
 * @author guozhiqi
 */
class MetaDataReplicateBeforeValidator {
    /**
     * 针对待复制目标元数据的校验
     *
     * @param targetMetaData 目标元数据
     * @return
     */
    public ResultMessage<String> validate(GspMetadata targetMetaData) {
        ResultMessage<String> resultMessage = null;
        // 判断目标元数据是否存在
        boolean isExists = MetadataUtility.getInstance().isMetaDataExistsWithMetadataPathAndFileNameWithDesign(targetMetaData.getRelativePath(), targetMetaData.getHeader().getFileName());
        if (isExists) {

            resultMessage = ResultCode.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0006,
                    targetMetaData.getHeader().getName(),
                    targetMetaData.getRelativePath()));
            return resultMessage;
        }

        // 表单元数据内容不能为空
        if (targetMetaData.getContent() == null) {
            resultMessage = ResultCode.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0007));
            return resultMessage;
        }
        // 待复制的必须为表单元数据
        if (!(targetMetaData.getContent() instanceof FormMetadataContent)) {
            resultMessage = ResultCode.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0008));
            return resultMessage;
        }

        return ResultCode.success();
    }
}
