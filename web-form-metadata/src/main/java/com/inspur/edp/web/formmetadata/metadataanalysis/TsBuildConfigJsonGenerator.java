/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadataanalysis;

public class TsBuildConfigJsonGenerator {
    /**
     * ts编译配置文件
     */
    private static final String tsConfigJsonContent = "{\n" +
            "  \"compileOnSave\": false,\n" +
            "  \"compilerOptions\": {\n" +
            "    \"baseUrl\": \"./\",\n" +
            "    \"outDir\": \"./dist-tsc\",\n" +
            "    \"rootDir\": \"./\",\n" +
            "    \"sourceMap\": false,\n" +
            "    \"declaration\": false,\n" +
            "    \"module\": \"es2015\",\n" +
            "    \"moduleResolution\": \"node\",\n" +
            "    \"emitDecoratorMetadata\": true,\n" +
            "    \"experimentalDecorators\": true,\n" +
            "    \"importHelpers\": true,\n" +
            "    \"noImplicitAny\": false,\n" +
            "    \"noStrictGenericChecks\": true,\n" +
            "    \"noUnusedLocals\": false,\n" +
            "    \"noEmitOnError\": true,\n" +
            "    \"suppressExcessPropertyErrors\": true,\n" +
            "    \"skipDefaultLibCheck\": true,\n" +
            "    \"skipLibCheck\": true,\n" +
            "    \"noEmitHelpers\": true,\n" +
            "    \"diagnostics\": false,\n" +
            "    \"traceResolution\": false,\n" +
            "    \"removeComments\": false,\n" +
            "    \"target\": \"es5\",\n" +
            "    \"typeRoots\": [\n" +
            "      \"node_modules/@types\"\n" +
            "    ],\n" +
            "    \"lib\": [\n" +
            "      \"es2018\",\n" +
            "      \"dom\"\n" +
            "    ],\n" +
            "    \"paths\": {}\n" +
            "  },\n" +
            "  \"angularCompilerOptions\": {\n" +
            "    \"strictInjectionParameters\": false,\n" +
            "    \"trace\": false,\n" +
            "    \"preserveWhitespaces\": false,\n" +
            "    \"fullTemplateTypeCheck\": false,\n" +
            "    \"disableTypeScriptVersionCheck\": true,\n" +
            "    \"skipMetadataEmit\": true,\n" +
            "    \"skipTemplateCodegen\": false,\n" +
            "    \"annotationsAs\": \"decorators\"\n" +
            "  }\n" +
            "}";

    public static String getTsConfigJsonContent() {
        return tsConfigJsonContent;
    }
}
