/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.formdom;

import com.inspur.edp.web.formmetadata.metadata.module.Module;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/12
 */
public class FormDOM {

    private String formType;

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    private Module module;

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    private FormOptions options;

    public FormOptions getOptions() {
        if (this.options == null) {
            this.options = new FormOptions();
        }
        return options;
    }

    public void setOptions(FormOptions options) {
        this.options = options;
    }
}
