/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.common.JSONSerializer;

/**
 * description:表单元数据内容序列化器
 *
 * @author Noah Guo
 * @date 2021/01/15
 */
public class FormMetadataContentSerializer implements MetadataContentSerializer {
    @Override
    public JsonNode Serialize(IMetadataContent metadataContent){
        if (metadataContent == null) {
            return null;
        }

        ObjectMapper mapper = JSONSerializer.getObjectMapper();
        return mapper.valueToTree(metadataContent);
    }

    @Override
    public IMetadataContent DeSerialize(JsonNode formMetadataContentNode){
        if (formMetadataContentNode == null) {
            return null;
        }

        SerializeUtility serializeHelper = SerializeUtility.getInstance();
        JsonNode contentNode = formMetadataContentNode.get("Contents");
        if(JsonNodeType.STRING == contentNode.getNodeType()){
            ((ObjectNode)formMetadataContentNode).set("Contents", serializeHelper.readTree(contentNode.textValue()));
        }

        ObjectMapper mapper = JSONSerializer.getObjectMapper();
        return mapper.convertValue(formMetadataContentNode, FormMetadataContent.class);
    }
}
