/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.dataextendinfo.VoDataExtendInfo;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataSerializerHelper;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;
import com.inspur.edp.web.formmetadata.constant.I18nMsgConstant;
import com.inspur.edp.web.formmetadata.replication.MetadataReplicationContext;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * GspViewModel 的元数据拷贝 内容调整
 * 保证VO元数据，元数据描述的id和code与VO内容中数据的一致性
 *
 * @author guozhiqi
 */
class GspViewModelCloneAdjustStrategy implements GspMetadataCloneAdjustStrategy {
    @Override
    public void adjust(GspMetadata replicateMetadata, IMetadataContent replicateMetadataContent, MetadataReplicationContext context) {
        if (!(replicateMetadataContent instanceof GspViewModel)) {
            return;
        }
        GspViewModel replicateGSPViewModel = (GspViewModel) replicateMetadataContent;

        String oldViewModelCode = replicateGSPViewModel.getCode();

        replicateGSPViewModel.setID(replicateMetadata.getHeader().getId());

        replicateGSPViewModel.getVariables().setName(replicateGSPViewModel.getVariables().getName().replace(oldViewModelCode, replicateMetadata.getHeader().getCode()));
        replicateGSPViewModel.setName(replicateMetadata.getHeader().getName());

        replicateGSPViewModel.getVariables().setCode(replicateGSPViewModel.getVariables().getCode().replace(oldViewModelCode, replicateMetadata.getHeader().getCode()));
        replicateGSPViewModel.setCode(replicateMetadata.getHeader().getCode());

        // bugfix: 542458 另存表单后没有把响应的构件文件复制出来，打开另存出的vo元数据，代码打开双击java文件时提示路径无效。
        // 清空对构件的引用，并提示出来。
        VMActionCollection actions = replicateGSPViewModel.getActions();
        for (int i = actions.size() - 1; i >= 0; i--) {
            ViewModelAction viewModelAction = actions.get(i);
            if (!(viewModelAction instanceof MappedCdpAction)) {
                continue;
            }
            MappedCdpAction action = (MappedCdpAction) viewModelAction;
            if (action != null && action.getIsGenerateComponent()) {
                actions.remove(i);
                String actionName = action.getName();
                String location = ((GspViewModel) replicateMetadataContent).getName() + "::" + "ActionList";
                context.getReplicationResult().add(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0011, actionName, location));
            }
        }
        VoDataExtendInfo dataExtendInfo = replicateGSPViewModel.getDataExtendInfo();
        String [] dataExtendTypes = {"DataMappingActions", "BeforeQueryActions", "QueryActions", "AfterQueryActions",
                "BeforeRetrieveActions", "RetrieveActions", "AfterRetrieveActions", "BeforeModifyActions",
                "ModifyActions", "AfterModifyActions", "ChangesetMappingActions", "BeforeCreateActions",
                "CreateActions", "AfterCreateActions", "BeforeDeleteActions", "DeleteActions", "AfterDeleteActions",
                "BeforeSaveActions", "DataReversalMappingActions", "AfterSaveActions", "ChangesetReversalMappingActions",
                "BeforeMultiDeleteActions", "MultiDeleteActions", "AfterMultiDeleteActions"};
        for (String type : dataExtendTypes) {
            VMActionCollection dataExtendActions = null;
            switch(type) {
                case "DataMappingActions":
                    dataExtendActions = dataExtendInfo.getDataMappingActions();
                    break;
                case "BeforeQueryActions":
                    dataExtendActions = dataExtendInfo.getBeforeQueryActions();
                    break;
                case "QueryActions":
                    dataExtendActions = dataExtendInfo.getQueryActions();
                    break;
                case "AfterQueryActions":
                    dataExtendActions = dataExtendInfo.getAfterQueryActions();
                    break;
                case "BeforeRetrieveActions":
                    dataExtendActions = dataExtendInfo.getBeforeRetrieveActions();
                    break;
                case "RetrieveActions":
                    dataExtendActions = dataExtendInfo.getRetrieveActions();
                    break;
                case "AfterRetrieveActions":
                    dataExtendActions = dataExtendInfo.getAfterRetrieveActions();
                    break;
                case "BeforeModifyActions":
                    dataExtendActions = dataExtendInfo.getBeforeModifyActions();
                    break;
                case "ModifyActions":
                    dataExtendActions = dataExtendInfo.getModifyActions();
                    break;
                case "AfterModifyActions":
                    dataExtendActions = dataExtendInfo.getAfterModifyActions();
                    break;
                case "ChangesetMappingActions":
                    dataExtendActions = dataExtendInfo.getChangesetMappingActions();
                    break;
                case "BeforeCreateActions":
                    dataExtendActions = dataExtendInfo.getBeforeCreateActions();
                    break;
                case "CreateActions":
                    dataExtendActions = dataExtendInfo.getCreateActions();
                    break;
                case "AfterCreateActions":
                    dataExtendActions = dataExtendInfo.getAfterCreateActions();
                    break;
                case "BeforeDeleteActions":
                    dataExtendActions = dataExtendInfo.getBeforeDeleteActions();
                    break;
                case "DeleteActions":
                    dataExtendActions = dataExtendInfo.getDeleteActions();
                    break;
                case "AfterDeleteActions":
                    dataExtendActions = dataExtendInfo.getAfterDeleteActions();
                    break;
                case "BeforeSaveActions":
                    dataExtendActions = dataExtendInfo.getBeforeSaveActions();
                    break;
                case "DataReversalMappingActions":
                    dataExtendActions = dataExtendInfo.getDataReversalMappingActions();
                    break;
                case "AfterSaveActions":
                    dataExtendActions = dataExtendInfo.getAfterSaveActions();
                    break;
                case "ChangesetReversalMappingActions":
                    dataExtendActions = dataExtendInfo.getChangesetReversalMappingActions();
                    break;
                case "BeforeMultiDeleteActions":
                    dataExtendActions = dataExtendInfo.getBeforeMultiDeleteActions();
                    break;
                case "MultiDeleteActions":
                    dataExtendActions = dataExtendInfo.getMultiDeleteActions();
                    break;
                case "AfterMultiDeleteActions":
                    dataExtendActions = dataExtendInfo.getAfterMultiDeleteActions();
                    break;
                default:
                    break;
            }
            if (dataExtendActions == null) {
                continue;
            }
            for (int i = dataExtendActions.size() - 1; i >= 0; i--) {
                ViewModelAction viewModelAction = dataExtendActions.get(i);
                if (!(viewModelAction instanceof MappedCdpAction)) {
                    continue;
                }
                MappedCdpAction action = (MappedCdpAction) viewModelAction;
                if (action != null && action.getIsGenerateComponent()) {
                    dataExtendActions.remove(i);
                    String actionName = action.getName();
                    String location = ((GspViewModel) replicateMetadataContent).getName() + "::" + type;
                    context.getReplicationResult().add(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0011, actionName, location));
                }
            }
        }


        MetadataContentSerializer transferSerializerManager = MetadataSerializerHelper.getInstance().getManager(replicateMetadata.getHeader().getType());
        if (transferSerializerManager == null) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0013, new String[]{replicateMetadata.getHeader().getType()}, ExceptionLevel.Error);
        }


        String replicateGSPViewModelStr = transferSerializerManager.Serialize(replicateGSPViewModel).toString();
        String i18nResourceInfoPrefix = replicateMetadata.getHeader().getNameSpace() + "." + "Vo" + ".";
        replicateGSPViewModelStr = replicateGSPViewModelStr.replace(i18nResourceInfoPrefix + oldViewModelCode, i18nResourceInfoPrefix + replicateGSPViewModel.getCode());

        replicateMetadata.setContent(transferSerializerManager.DeSerialize(SerializeUtility.getInstance().readTree(replicateGSPViewModelStr)));
    }
}
