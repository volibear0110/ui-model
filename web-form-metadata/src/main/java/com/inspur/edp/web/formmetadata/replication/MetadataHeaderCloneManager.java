/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.web.common.utility.RandomUtility;
import com.inspur.edp.web.common.utility.StringUtility;

/**
 * 元数据header deepClone
 *
 * @author guozhiqi
 */
class MetadataHeaderCloneManager {
    /**
     * 构造target  metadata header
     *
     * @param sourceMetadata
     * @param metadataDto
     * @return
     */
    public static MetadataHeader cloneMetadataHeader(GspMetadata sourceMetadata, MetadataDto metadataDto) {
        // 构造target  header
        MetadataHeader metadataHeader = createFromSourceMetadata(sourceMetadata);

        tryToUpdateMetadataHeader(metadataDto, metadataHeader);

        return metadataHeader;
    }

    private static MetadataHeader createFromSourceMetadata(GspMetadata sourceMetadata) {
        MetadataHeader targetMetadataHeader = (MetadataHeader) sourceMetadata.getHeader().clone();
        targetMetadataHeader.setId(RandomUtility.newGuid());
        targetMetadataHeader.setCode(sourceMetadata.getHeader().getCode() + "copy");
        targetMetadataHeader.setName(sourceMetadata.getHeader().getName() + "copy");
        targetMetadataHeader.setFileName(sourceMetadata.getHeader().getCode() + "copy" + sourceMetadata.getHeader().getFileName().substring(sourceMetadata.getHeader().getFileName().lastIndexOf(".")));
        return targetMetadataHeader;
    }

    private static void tryToUpdateMetadataHeader(MetadataDto metadataDto, MetadataHeader targetMetadataHeader) {
        if (metadataDto == null) {
            return;
        }

        StringUtility.executeIfPresent(metadataDto.getId(), targetMetadataHeader::setId);
        StringUtility.executeIfPresent(metadataDto.getNameSpace(), targetMetadataHeader::setNameSpace);
        StringUtility.executeIfPresent(metadataDto.getCode(), targetMetadataHeader::setCode);
        StringUtility.executeIfPresent(metadataDto.getName(), targetMetadataHeader::setName);
        StringUtility.executeIfPresent(metadataDto.getFileName(), targetMetadataHeader::setFileName);
        StringUtility.executeIfPresent(metadataDto.getType(), targetMetadataHeader::setType);
        StringUtility.executeIfPresent(metadataDto.getBizobjectID(), targetMetadataHeader::setBizobjectID);
        StringUtility.executeIfPresent(metadataDto.getLanguage(), targetMetadataHeader::setLanguage);
        if (metadataDto.isTranslating()) {
            targetMetadataHeader.setTranslating(metadataDto.isTranslating());
        }
    }
}
