package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;

import java.util.HashMap;

/**
 * table 布局中td单元格中国际化参数提取
 *
 * @author noah
 * 2023/8/22 14:24
 */
public class TableTdI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * td 类型 staticText
     */
    private static final String StaticTextTdType = "staticText";

    /**
     * td 类型 editor
     */
    private static final String EditorTdType = "editor";

    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        String tdType = ComponentUtility.getInstance().getValue(currentComponent, "tdType");
        if (StringUtility.isNullOrEmpty(tdType)) {
            tdType = StaticTextTdType;
        }

        // 仅提取staticText的文本内容
        if (StringUtility.equals(tdType, StaticTextTdType)) {
            HashMap<String, Object> staticTextMap = ComponentUtility.getInstance().GetAttributeObject(currentComponent, "staticText");
            if (staticTextMap != null) {
                String componentType = ComponentUtility.getInstance().getType(currentComponent);
                String componentId = ComponentUtility.getInstance().getId(currentComponent);

                String generatedComponentId = componentType + "/" + componentId + "/staticText/text";
                String staticTextValue = ComponentUtility.getInstance().getValue(staticTextMap, "text");

                this.addInCollection(i18nResourceItemCollection, I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, staticTextValue, staticTextValue));
            }
        }


        return i18nResourceItemCollection;
    }
}
