/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;

import java.util.ArrayList;
import java.util.List;


/**
 * 表单复制传递上下文参数
 *
 * @author guozhiqi
 */
public final class MetadataReplicationContext {
    /**
     * 待复制元数据
     */
    private GspMetadata sourceMetadata;

    public GspMetadata getSourceMetadata() {
        return this.sourceMetadata;
    }

    public void setSourceMetadata(GspMetadata value) {
        this.sourceMetadata = value;
    }

    /**
     * 待复制元数据所在工程的名称
     */
    private String sourceProjectName;

    public String getSourceProjectName() {
        return this.sourceProjectName;
    }

    public void setSourceProjectName(String value) {
        this.sourceProjectName = value;
    }

    /**
     * 目标元数据描述
     */
    private MetadataDto targetMetadataDescription;

    public MetadataDto getTargetMetadataDescription() {
        return this.targetMetadataDescription;
    }

    public void setTargetMetadataDescription(MetadataDto value) {
        this.targetMetadataDescription = value;
    }

    /**
     * 获取目标元数据文件的名称
     */
    private String targetFileName;

    public void setTargetFileName(String targetFileSuffix) {
        this.targetFileName = targetFileSuffix;
    }

    public String getTargetFileName() {
        return this.targetFileName;
    }

    public MetadataReplicationContext() {

    }

    /**
     * 复制结果
     * 存储复制过程中无法复制的内容，提示给用户。
     */
    private final List<String> replicationResult = new ArrayList<>();

    public List<String> getReplicationResult() {
        return this.replicationResult;
    }

}
