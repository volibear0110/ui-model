/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.utility.CommonUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContentService;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.replication.MetadataReplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * 表单元数据拷贝 数据调整
 */
class FormMetadataCloneAdjustStrategy implements GspMetadataCloneAdjustStrategy {
    @Override
    public void adjust(GspMetadata replicateMetadata, IMetadataContent targetMetadataContent, MetadataReplicationContext context) {
        if (!(targetMetadataContent instanceof FormMetadataContent)) {
            return;
        }
        FormMetadataContent replicateFormMetadataContent = (FormMetadataContent) targetMetadataContent;

        replicateFormMetadataContent.setId(replicateMetadata.getHeader().getId());

        FormDOM formDOM = FormMetadataContentService.getInstance().getFormContent(replicateFormMetadataContent);
        formDOM.getModule().setId(replicateMetadata.getHeader().getCode());
        formDOM.getModule().setCode(replicateMetadata.getHeader().getCode());
        formDOM.getModule().setName(replicateMetadata.getHeader().getName());
        formDOM.getModule().setCaption(replicateMetadata.getHeader().getName());
        formDOM.getModule().setCreationDate(CommonUtility.getCurrentDateString());
        // 如果存在筛选方案  那么需要将筛选方案的formId调整为当前的表单code
        for (HashMap<String, Object> item : formDOM.getModule().getComponents()) {
            this.rescure(item, formDOM);
        }


        //更新工程名
        FormMetadataContentService.getInstance().setFormContent(replicateFormMetadataContent, formDOM);
    }


    /**
     * 递归进行查找筛选方案 并进行参数调整
     *
     * @param currentComponent
     * @param formDOM
     */
    private void rescure(HashMap<String, Object> currentComponent, FormDOM formDOM) {
        if (currentComponent != null) {
            // 如果是筛选方案   当前仅处理筛选方案
            if (Objects.equals(currentComponent.get("type"), "QueryScheme")) {
                currentComponent.put("formId", formDOM.getModule().getCode());
            }
            // 如果包含子 那么需要递归进行查找
            if (currentComponent.get("contents") != null && (currentComponent.get("contents") instanceof List)) {
                List<HashMap<String, Object>> contentList = (List) currentComponent.get("contents");
                for (HashMap<String, Object> item : contentList) {
                    this.rescure(item, formDOM);
                }
            }
        }
    }


}
