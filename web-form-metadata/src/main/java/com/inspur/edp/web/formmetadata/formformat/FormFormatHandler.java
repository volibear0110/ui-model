package com.inspur.edp.web.formmetadata.formformat;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.formserver.viewmodel.formentity.ButtonGroup;
import com.inspur.edp.formserver.viewmodel.formentity.ObjectData;
import com.inspur.edp.formserver.viewmodel.formentity.VoFormModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.web.formmetadata.formformat.handler.WFFormFormatMobileHandler;
import com.inspur.edp.web.formmetadata.formformat.handler.WFFormFormatPcHandler;

import java.util.List;

/**
 * 表单格式化处理handler
 *
 */
public  abstract class FormFormatHandler {

    public abstract String getFormType();
//    public abstract List<UrlParameter> getDefaultUrlParams();
    public abstract List<ButtonGroup> getButtons(JsonNode formContent);
    public abstract ObjectData getObjectData(JsonNode formContent);

    public abstract VoFormModel getVoFormModel(GspMetadata formMetadata, String formatType, String basePath);

//    public abstract List<AttachmentGroup> getAttachmentGroups(JsonNode formContent);

    public abstract void pushFormFormat(GspMetadata formMetadata, String formatType, String formPath);

    public abstract void pushRtcFormFormat(GspMetadata formMetadata, String formatType, DimensionExtendEntity dimension);

    public static FormFormatHandler getFormFormatHandler(String formType) {
        if ("Form".equals(formType)) {
            return new WFFormFormatPcHandler();
        } else {
            return new WFFormFormatMobileHandler();
        }
    }
}
