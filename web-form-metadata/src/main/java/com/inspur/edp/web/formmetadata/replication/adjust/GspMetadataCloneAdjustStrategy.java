/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.formmetadata.replication.MetadataReplicationContext;

/**
 * 元数据复制进行的内部调整接口
 * 针对不同元数据需要进行调整的参数或数据值不同
 * 使之成为函数式接口 方便通过对应的lambda表达式进行处理
 * @author guozhiqi
 */
@FunctionalInterface
public interface GspMetadataCloneAdjustStrategy {
    /**
     * 参数调整
     * @param sourceMetadata
     * @param targetMetadataContent
     */
    void adjust(GspMetadata sourceMetadata, IMetadataContent targetMetadataContent, MetadataReplicationContext context);
}
