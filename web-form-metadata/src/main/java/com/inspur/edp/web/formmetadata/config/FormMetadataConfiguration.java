/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.config;

import com.inspur.edp.web.formmetadata.api.FormMetadataCommonService;
import com.inspur.edp.web.formmetadata.formresource.FormResourceManager;
import com.inspur.edp.web.formmetadata.lic.FormMetadataCreateLicControlListener;
import com.inspur.edp.web.formmetadata.service.FormMetadataCommonServiceImpl;
import com.inspur.edp.web.formmetadata.service.FormMetadataRTService;
import com.inspur.edp.web.formmetadata.service.FormMetadataService;
import com.inspur.edp.web.formmetadata.service.FormRelateMetadataService;
import com.inspur.edp.web.formmetadata.webservice.FormMetadataWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration("com.inspur.edp.web.formmetadata.config.FormMetadataConfiguration")
public class FormMetadataConfiguration {
    @Bean()
    public RESTEndpoint formMetadataWebapiEndPoint() {
        return new RESTEndpoint("/dev/main/v1.0/form-metadata", new FormMetadataWebServiceImpl());
    }

    @Bean
    public FormMetadataRTService formMetadataRTService() {
        return new FormMetadataRTService();
    }

    /**
     * 增加 ConditionOnClass 的目的是为了在补丁工具中，不存在这个 class 的时候，不需要该 bean
     * @return
     */
    @Bean
    @ConditionalOnClass(name = {"io.iec.edp.caf.businessobject.api.service.DevBasicInfoService"})
    public FormMetadataCommonService formMetadataCommonService() {
        return new FormMetadataCommonServiceImpl();
    }

    /**
     * 增加 ConditionOnClass 的目的是为了在补丁工具中，不存在这个 class 的时候，不需要该 bean
     * @return
     */
    @Bean
    @ConditionalOnClass(name = {"io.iec.edp.caf.licservice.api.manager.StandardControlFactory"})
    public FormMetadataCreateLicControlListener getFormMetadataCreateLicControlListener() {
        return new FormMetadataCreateLicControlListener();
    }

    @Bean
    public FormMetadataService getFormMetadataService() {
        return new FormMetadataService();
    }

    @Bean
    public FormRelateMetadataService getFormRelateMetadataService() {
        return new FormRelateMetadataService();
    }


    @Bean
    public FormResourceManager getFormResourceManager() {
        return new FormResourceManager();
    }


}
