/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadatamanager;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;

/**
 * 命令元数据manager
 *
 * @author noah
 */
public class CommandsMetadataManager extends BaseMetaDataManager {

    public CommandsMetadataManager(ExecuteEnvironment executeEnvironment, String relativePath, boolean isUpdradeTool) {
        super(executeEnvironment, relativePath, isUpdradeTool);
    }

    public GspMetadata getWebCommands(MetadataManagerParameter metadataManagerParameter) {
        GspMetadata webCommandMetadata = getMetadata(() -> {
            MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
            getterMetadataInfo.setId(metadataManagerParameter.getId());
            getterMetadataInfo.setMetadataType(MetadataTypeEnum.Command);
            getterMetadataInfo.setCode(metadataManagerParameter.getExtendParameter().getCode());
            getterMetadataInfo.setPath(this.getRelativePath());
            getterMetadataInfo.setNameSpace(metadataManagerParameter.getExtendParameter().getNameSpace());
            getterMetadataInfo.setName(metadataManagerParameter.getExtendParameter().getName());
            return getterMetadataInfo;
        }, null, this.getDefaultMetadataNotFoundFormatMessage(metadataManagerParameter.getId(), MetadataTypeEnum.Command));
        if (webCommandMetadata == null) {
            throw new WebCustomException("load webCommand metadata is null,the commandId is " + metadataManagerParameter.getId());
        }
        return webCommandMetadata;
    }
}
