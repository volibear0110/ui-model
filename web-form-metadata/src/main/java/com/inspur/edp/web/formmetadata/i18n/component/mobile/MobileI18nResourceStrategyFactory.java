/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.mobile;

import java.util.HashMap;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.II18nResourceStrategy;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentGroups;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies.*;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * 国际化资源提取策略工厂
 */
public class MobileI18nResourceStrategyFactory {

    /**
     * 组件分组集合
     */
    private static final HashMap<String, String> groups = new HashMap<String, String>();

    /**
     * 提取策略类集合
     */
    private static final HashMap<String, AbstractMobileI18nResourceStrategy> strategies = new HashMap<>();

    /**
     * 静态单例属性
     */
    private static volatile MobileI18nResourceStrategyFactory _instance = null;

    /**
     * _lock
     */
    private static final Object _lock = new Object();

    /**
     * 工厂方法
     */
    public static MobileI18nResourceStrategyFactory getInstance() {
        if (_instance == null) {
            synchronized (_lock) {
                if (_instance == null) {
                    _instance = new MobileI18nResourceStrategyFactory();
                }
            }
        }
        return _instance;
    }

    /**
     * 私有构造函数
     */
    private MobileI18nResourceStrategyFactory() {
        this.initGroups();
        this.initStrategies();
    }

    /**
     * 初始化控件类型和分组映射
     */
    private void initGroups() {

        // 基础控件
        groups.put(ComponentTypes.Button, ComponentGroups.BASIC);
        groups.put(ComponentTypes.ButtonGroup, ComponentGroups.BASIC);
        groups.put(ComponentTypes.ToolBarArea, ComponentGroups.BASIC);
        groups.put(ComponentTypes.NavigationBar, ComponentGroups.BASIC);
        groups.put(ComponentTypes.SearchBox, ComponentGroups.BASIC);
        groups.put(ComponentTypes.TabBar, ComponentGroups.BASIC);

        // 容器控件
        groups.put(ComponentTypes.Component, ComponentGroups.CONTAINER);
        groups.put(ComponentTypes.TabPage, ComponentGroups.CONTAINER);
        groups.put(ComponentTypes.Card, ComponentGroups.CONTAINER);
        groups.put(ComponentTypes.FieldSet, ComponentGroups.CONTAINER);
        groups.put(ComponentTypes.Section, ComponentGroups.CONTAINER);

        // 输入控件
        groups.put(ComponentTypes.TextBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.MultiTextBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.NumericBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.DateBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.CalendarInput, ComponentGroups.INPUT);
        groups.put(ComponentTypes.RadioGroup, ComponentGroups.INPUT);
        groups.put(ComponentTypes.CheckGroup, ComponentGroups.INPUT);
        groups.put(ComponentTypes.EnumField, ComponentGroups.INPUT);
        groups.put(ComponentTypes.LookupEdit, ComponentGroups.INPUT);
        groups.put(ComponentTypes.RichTextEditor, ComponentGroups.INPUT);
        groups.put(ComponentTypes.SwitchField, ComponentGroups.INPUT);
        groups.put(ComponentTypes.TelphoneTextBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.ScanTextBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.AreaPicker, ComponentGroups.INPUT);
        groups.put(ComponentTypes.CityPicker, ComponentGroups.INPUT);
        groups.put(ComponentTypes.CustomBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.PersonnelSelector, ComponentGroups.INPUT);
        groups.put(ComponentTypes.OrganizationSelector, ComponentGroups.INPUT);
        groups.put(ComponentTypes.ScanTextBox, ComponentGroups.INPUT);
        groups.put(ComponentTypes.ExternalDataInput, ComponentGroups.INPUT);

        // 展示控件
        groups.put(ComponentTypes.ListView, ComponentGroups.DISPLAY);
        groups.put(ComponentTypes.Schedule, ComponentGroups.DISPLAY);

        // 业务控件
        groups.put(ComponentTypes.QueryScheme, ComponentGroups.BIZ);
        groups.put(ComponentTypes.PersonnelInfo, ComponentGroups.BIZ);
        groups.put(ComponentTypes.ApprovalLogsPanel, ComponentGroups.BIZ);
        groups.put(ComponentTypes.ApprovalInfo, ComponentGroups.BIZ);
        groups.put(ComponentTypes.ApprovalComments, ComponentGroups.BIZ);

        // 集合控件
        groups.put(ComponentTypes.LightAttachment, ComponentGroups.COLLECTION);

        // 导航控件
        groups.put(ComponentTypes.ScrollNavbar, ComponentGroups.NAV);
    }

    private void initStrategies() {
       strategies.put(ComponentGroups.BASIC, new MobileBasicComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.CONTAINER, new MobileContainerComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.INPUT, new MobileInputComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.DISPLAY, new MobileDisplayComponent18nResourceStrategy());
       strategies.put(ComponentGroups.BIZ, new MobileBizComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.COLLECTION, new MobileCollectionComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.NAV, new MobileNavComponentI18nResourceStrategy());
       strategies.put(ComponentGroups.OTHER, new MobileDefaultI18nResourceStrategy());
    }

    /**
     * 根据控件类型获取分组
     */
    private String getGroupByType(String componentType) {
        String group = groups.get(componentType);
        return group;
    }

    /**
     * 根据组件类型创建对应的提取策略
     */
    public final II18nResourceStrategy getStrategy(HashMap<String, Object> currentComponent, String i18nResourceBaseId, FormDOM formDOM) {
        String componentType = ComponentUtility.getInstance().getType(currentComponent);
        if (StringUtility.isNullOrEmpty(componentType)) {
            return null;
        }

        String group = this.getGroupByType(componentType);
        if (group == null) {
            group = ComponentGroups.OTHER;
        }

        AbstractMobileI18nResourceStrategy strategy = strategies.get(group);
        return strategy;
    }
}
