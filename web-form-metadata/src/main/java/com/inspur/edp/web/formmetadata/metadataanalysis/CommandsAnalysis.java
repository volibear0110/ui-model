/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadataanalysis;

import com.inspur.edp.cdp.web.component.metadata.define.WebComponentMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.command.component.metadata.*;
import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.api.dto.FormRelateMetadataInDesignResultDto;
import com.inspur.edp.web.formmetadata.api.dto.RelateMetadataTypeEnum;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.formmetadata.metadatamanager.CommandsMetadataManager;
import com.inspur.edp.web.formmetadata.metadatamanager.ComponentMetadataManager;
import com.inspur.edp.web.formmetadata.metadatamanager.MetadataManagerParameter;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.core.session.CafSession;
import lombok.Getter;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 命令解析
 *
 * @author noah
 */

public class CommandsAnalysis extends AbstractMetadataAnalysis {

    // 是否生成command.json 文件
    private boolean createCommandJsonFile = true;

    /**
     * web 命令元数据列表
     */
    private Map<String, WebCommandsMetadataAndExtra> webCommandsMetadataHashMap;

    public Map<String, WebCommandsMetadataAndExtra> getWebCommandsMetadataHashMap() {
        if (this.webCommandsMetadataHashMap == null) {
            this.webCommandsMetadataHashMap = new HashMap<>();
        }
        return this.webCommandsMetadataHashMap;
    }

    public CommandsAnalysis(ExecuteEnvironment executeEnvironment, boolean isUpgradeTool) {
        this(executeEnvironment, isUpgradeTool, true);
    }

    public CommandsAnalysis(ExecuteEnvironment executeEnvironment, boolean isUpgradeTool, boolean createCommandJsonFile) {
        super(executeEnvironment, isUpgradeTool);
        this.createCommandJsonFile = createCommandJsonFile;
    }


    /**
     * resolveCommand 参数
     */
    @Getter
    public static class ResolveCommandParameter {
        private FormDOM json;

        /**
         * 关联的表单元数据信息
         */
        private GspMetadata formMetadata;

        private String basePath;

        /**
         * 获取表单元数据的相对路径
         * 如果对应表单元数据为空，那么对应的相对路径参数为空
         *
         * @return
         */
        public String getFormMetadataRelativePath() {
            if (StringUtils.isEmpty(this.basePath)) {
                return this.formMetadata == null ? null : this.formMetadata.getRelativePath();
            }
            return this.basePath;
        }

        public static ResolveCommandParameter init(GspMetadata formMetadata, FormDOM formDOM) {
            ResolveCommandParameter parameter = new ResolveCommandParameter();
            parameter.json = formDOM;
            parameter.formMetadata = formMetadata;
            return parameter;
        }

        public static ResolveCommandParameter init(GspMetadata formMetadata, FormDOM formDOM, String basePath) {
            ResolveCommandParameter parameter = new ResolveCommandParameter();
            parameter.json = formDOM;
            parameter.formMetadata = formMetadata;
            parameter.basePath = basePath;
            return parameter;
        }
    }

    public void resolveCommand(ResolveCommandParameter parameter, String formMetadataName, String targetStoragePath,
                               HashMap<String, WebComponentMetadataAndExtra> projectCmpList) {
        HashMap<String, WebComponentMetadataAndExtra> cmpList = new HashMap<>(8);
        String strFormRelativePath = parameter.getFormMetadataRelativePath();
        CommandsMetadataManager cmdManager = new CommandsMetadataManager(this.executeEnvironment, strFormRelativePath, this.isUpgradeTool);
        ComponentMetadataManager componentMetadataManager = new ComponentMetadataManager(this.executeEnvironment, this.isUpgradeTool, strFormRelativePath);
        StringBuilder sbCommands = new StringBuilder();
        sbCommands.append("{");
        if (parameter.getJson().getModule() != null &&
                ListUtility.isNotEmpty(parameter.getJson().getModule().getWebcmds())) {
            sbCommands.append("\"commands\":{");
            int index = 0;
            WebCommandsMetadata metadata;
            String cmdId;
            for (HashMap<String, Object> c : parameter.getJson().getModule().getWebcmds()) {
                if (index > 0) {
                    sbCommands.append(",");
                }
                cmdId = c.get("id").toString();

                MetadataManagerParameter metadataManagerParameter = MetadataManagerParameter.init(cmdId,
                        StringUtility.getOrDefaultEmpty(c.get("code")),
                        StringUtility.getOrDefaultEmpty(c.get("name")),
                        StringUtility.getOrDefaultEmpty(c.get("nameSpace")));
                GspMetadata commandMetadata = cmdManager.getWebCommands(metadataManagerParameter);


                metadata = (WebCommandsMetadata) commandMetadata.getContent();

                // 写入缓存 避免多次对同一个元数据的请求
                this.getWebCommandsMetadataHashMap().put(commandMetadata.getHeader().getId(),
                        WebCommandsMetadataAndExtra.init(metadata, commandMetadata.getHeader().getId(), commandMetadata.getHeader().getName(), commandMetadata.getHeader().getFileName(), commandMetadata.getRelativePath()));

                analysisComponentMetadata(metadata, cmpList, null, componentMetadataManager, projectCmpList);

                try {
                    analysisServiceRef(parameter.getJson(), cmpList);
                } catch (Exception e) {
                    WebLogger.Instance.error(e);
                    return;
                }

                sbCommands.append("\"").append(cmdId).append("\":");
                sbCommands.append(cmdManager.serialize(metadata));
                index += 1;
            }
            sbCommands.append("}");

            if (cmpList.size() > 0) {
                sbCommands.append(",\"serviceList\":[");
                AtomicInteger atomicInteger = new AtomicInteger(0);
                cmpList.forEach((k, v) -> {
                    if (atomicInteger.getAndIncrement() > 0) {
                        sbCommands.append(",");
                    }
                    sbCommands.append(SerializeUtility.getInstance().serialize(v.getWebComponentMetadata(), false));
                });

                sbCommands.append("]");
            }
        }
        sbCommands.append("}");


        if (this.createCommandJsonFile) {
            cmdManager.saveMetadataFile(targetStoragePath, formMetadataName.toLowerCase() + JITEngineConstants.CommandJsonFile, sbCommands.toString());
        }

    }

    private static class RevolveCommandFutureResult {
        Map<String, WebComponentMetadataAndExtra> cmpList;
        Map<String, WebCommandsMetadataAndExtra> webCommandsMetadataAndExtraHashMap;
    }

    @Getter
    public static class ResolveCommandAsyncParameter {
        private CafSession cafSession;
        private FormDOM json;
        private GspMetadata formMetadata;

        public static ResolveCommandAsyncParameter init(CafSession cafSession, FormDOM formDOM, GspMetadata formMetadata) {
            ResolveCommandAsyncParameter parameter = new ResolveCommandAsyncParameter();
            parameter.cafSession = cafSession;
            parameter.json = formDOM;
            parameter.formMetadata = formMetadata;
            return parameter;
        }
    }

    public void resolveCommandAsync(ResolveCommandAsyncParameter parameter,
                                    Map<String, WebComponentMetadataAndExtra> projectCmpList,
                                    List<CompletableFuture<?>> completableFutureList,
                                    Map<String, FormRelateMetadataInDesignResultDto> formRelateMetadataInDesignResultDtoConcurrentHashMap
    ) {
        CommandsMetadataManager cmdManager = new CommandsMetadataManager(this.executeEnvironment, parameter.getFormMetadata().getRelativePath(), this.isUpgradeTool);

        ComponentMetadataManager componentMetadataManager = new ComponentMetadataManager(this.executeEnvironment, this.isUpgradeTool, parameter.getFormMetadata().getRelativePath());

        if (parameter.getJson().getModule() != null && parameter.getJson().getModule().getWebcmds() != null && parameter.getJson().getModule().getWebcmds().size() > 0) {
            // 使用并行流
            parameter.getJson().getModule().getWebcmds().forEach(c -> {
                CompletableFuture<RevolveCommandFutureResult> commandCompletableFuture = CompletableFuture.supplyAsync(() -> {
                    CAFContext.current.getService().setCurrentThreadPoolSession(parameter.getCafSession());
                    Map<String, WebComponentMetadataAndExtra> cmpList = new ConcurrentHashMap<>(8);
                    Map<String, WebCommandsMetadataAndExtra> webCommandsMetadataAndExtraMap = new ConcurrentHashMap<>();

                    String cmdId = c.get("id").toString();
                    MetadataManagerParameter metadataManagerParameter = MetadataManagerParameter.init(cmdId,
                            StringUtility.getOrDefaultEmpty(c.get("code")),
                            StringUtility.getOrDefaultEmpty(c.get("name")),
                            StringUtility.getOrDefaultEmpty(c.get("nameSpace")));
                    GspMetadata commandMetadata = cmdManager.getWebCommands(metadataManagerParameter);
                    WebCommandsMetadata metadata = (WebCommandsMetadata) commandMetadata.getContent();
                    // 写入web 命令元数据
                    webCommandsMetadataAndExtraMap.put(commandMetadata.getHeader().getId(), WebCommandsMetadataAndExtra.init(metadata, commandMetadata.getHeader().getId(), commandMetadata.getHeader().getName(), commandMetadata.getHeader().getFileName(), commandMetadata.getRelativePath()));
                    analysisComponentMetadata(metadata, cmpList, null, componentMetadataManager, projectCmpList);
                    try {
                        analysisServiceRef(parameter.getJson(), cmpList);
                    } catch (Exception e) {
                        WebLogger.Instance.error(e);
                    }
                    RevolveCommandFutureResult revolveCommandFutureResult = new RevolveCommandFutureResult();
                    revolveCommandFutureResult.cmpList = cmpList;
                    revolveCommandFutureResult.webCommandsMetadataAndExtraHashMap = webCommandsMetadataAndExtraMap;
                    return revolveCommandFutureResult;
                }).thenApply((RevolveCommandFutureResult revolveCommandFutureResult) -> {
                    Map<String, CommandsAnalysis.WebCommandsMetadataAndExtra> webCommandsMetadataHashMap = revolveCommandFutureResult.webCommandsMetadataAndExtraHashMap;
                    if (webCommandsMetadataHashMap != null && !webCommandsMetadataHashMap.isEmpty()) {
                        webCommandsMetadataHashMap.forEach((k, v) -> {
                            FormRelateMetadataInDesignResultDto commandMetadataResultDto = new FormRelateMetadataInDesignResultDto(RelateMetadataTypeEnum.Command, v.getMetadataName(), v.getMetadataFileName(),
                                    v.getMetadataId(), v.getWebCommandsMetadata(), v.getMetadataRelativePath());
                            formRelateMetadataInDesignResultDtoConcurrentHashMap.put(v.getMetadataId(), commandMetadataResultDto);
                        });
                    }

                    if (revolveCommandFutureResult.cmpList != null && !revolveCommandFutureResult.cmpList.isEmpty()) {
                        revolveCommandFutureResult.cmpList.forEach((k, v) -> {
                            FormRelateMetadataInDesignResultDto componentMetadataResultDto = new FormRelateMetadataInDesignResultDto(RelateMetadataTypeEnum.Component, v.getMetadataName(), v.getMetadataFileName(),
                                    v.getMetadataId(), v.getWebComponentMetadata(), v.getMetadataRelativePath());
                            formRelateMetadataInDesignResultDtoConcurrentHashMap.put(v.getMetadataId(), componentMetadataResultDto);
                        });
                    }
                    return null;
                });

                completableFutureList.add(commandCompletableFuture);
            });
        }
    }

    private void analysisComponentMetadata(WebCommandsMetadata metadata, Map<String, WebComponentMetadataAndExtra> cmpList,
                                           List<BranchCommandItem> items, ComponentMetadataManager cmpManager,
                                           Map<String, WebComponentMetadataAndExtra> projectCmpList) {
        if (ListUtility.isEmpty(items)) {
            if (ListUtility.isNotEmpty(metadata.getCommands())) {
                for (WebCommand webCommandItem : metadata.getCommands()) {
                    try {
                        analysisComponentCommandItemList(webCommandItem.getItems(), cmpList, cmpManager, projectCmpList);
                    } catch (Exception e) {
                        WebLogger.Instance.error(e);
                    }
                }
            }
        } else {
            for (BranchCommandItem branchCommandItem : items) {
                try {
                    analysisComponentCommandItemList(branchCommandItem.getItems(), cmpList, cmpManager, projectCmpList);
                } catch (Exception e) {
                    WebLogger.Instance.error(e);
                }
            }
        }
    }

    /// <summary>
    ///  解析commandItem
    /// </summary>
    /// <param name="commandItemList"></param>
    /// <param name="cmpList"></param>
    /// <param name="cmpManager"></param>
    private void analysisComponentCommandItemList(List<CommandItem> commandItemList, Map<String, WebComponentMetadataAndExtra> cmpList,
                                                  ComponentMetadataManager cmpManager, Map<String, WebComponentMetadataAndExtra> projectCmpList) throws Exception {
        if (commandItemList != null && commandItemList.size() > 0) {
            if (cmpManager == null) {
                cmpManager = new ComponentMetadataManager(this.executeEnvironment, this.isUpgradeTool, null);
            }
            WebComponentMetadata cmpMetadata = null;
            for (CommandItem item : commandItemList) {
                switch (item.getItemType()) {
                    case MethodRefer:
                        if (!cmpList.containsKey(((CmpMethodRefering) item).getComponentId())) {
                            if (!projectCmpList.containsKey(((CmpMethodRefering) item).getComponentId())) {
                                try {
                                    CmpMethodRefering cmpMethodReferingItem = (CmpMethodRefering) item;
                                    MetadataManagerParameter metadataManagerParameter = MetadataManagerParameter.init(cmpMethodReferingItem.getComponentId(),
                                            cmpMethodReferingItem.getCode(),
                                            cmpMethodReferingItem.getName(),
                                            cmpMethodReferingItem.getComponentPath());
                                    cmpMetadata = cmpManager.getComponentMetadata(metadataManagerParameter);
                                    if (cmpMetadata == null) {
                                        throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0005, new String[]{((CmpMethodRefering) item).getComponentId()});
                                    }
                                    WebLogger.Instance.info("Get WebCommands By Id:" + cmpMetadata.getId(), this.getClass().getName());
                                    WebComponentMetadataAndExtra webComponentMetadataAndExtra = WebComponentMetadataAndExtra.init(cmpMetadata, cmpMethodReferingItem.getComponentId(), cmpMethodReferingItem.getName(), cmpMethodReferingItem.getName(), cmpMethodReferingItem.getComponentPath());
                                    cmpList.put(cmpMetadata.getId(), webComponentMetadataAndExtra);
                                    projectCmpList.put(cmpMetadata.getId(), webComponentMetadataAndExtra);
                                } catch (Exception ex) {
                                    throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0001, new String[]{((CmpMethodRefering) item).getComponentId()}, ex);
                                }
                            } else {
                                WebComponentMetadataAndExtra webComponentMetadataAndExtra = projectCmpList.get(((CmpMethodRefering) item).getComponentId());
                                cmpMetadata = webComponentMetadataAndExtra.getWebComponentMetadata();
                                cmpList.put(cmpMetadata.getId(), webComponentMetadataAndExtra);
                            }
                        }
                        break;
                    case BranchCollection:
                        analysisComponentMetadata(null, cmpList, ((BranchCollectionCommandItem) item).getItems(), cmpManager, projectCmpList);
                        break;
                    default:
                        break;

                }
            }
        }
    }

    private void analysisServiceRef(FormDOM json, Map<String, WebComponentMetadataAndExtra> cmpList) throws Exception {
        if (cmpList != null && !cmpList.isEmpty()) {

            cmpList.forEach((k, v) -> {
                HashMap<String, Object> serviceRef = new HashMap<>();
                WebComponentMetadataAndExtra component = v;
                if (component == null) {
                    throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0006, new String[]{k});
                }

                WebComponentMetadata webComponentMetadata = component.getWebComponentMetadata();
                if (!hasServiceReference(json.getModule().getServiceRefs(), webComponentMetadata)) {
                    serviceRef.put("cmpId", webComponentMetadata.getId());
                    serviceRef.put("name", webComponentMetadata.getClassName());
                    String path = webComponentMetadata.getSource();
                    if (path == null || path.isEmpty()) {
                        path = getTSFileName(webComponentMetadata.getId());
                        webComponentMetadata.setSource(path);
                    }
                    serviceRef.put("path", path);

                    serviceRef.put("isCommon", webComponentMetadata.isCommon() ? "1" : "0");
                    serviceRef.put("alias", webComponentMetadata.getClassName() + "1");

                    json.getModule().getServiceRefs().add(serviceRef);
                }
            });

            for (Map.Entry<String, WebComponentMetadataAndExtra> stringWebComponentMetadataAndExtraEntry : cmpList.entrySet()) {
                HashMap.Entry entry = stringWebComponentMetadataAndExtraEntry;

                HashMap<String, Object> serviceRef = new HashMap<>();
                WebComponentMetadataAndExtra component = (WebComponentMetadataAndExtra) entry.getValue();
                if (component == null) {
                    throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0006, new String[]{String.valueOf(entry.getKey())});
                }

                WebComponentMetadata webComponentMetadata = component.getWebComponentMetadata();
                if (!hasServiceReference(json.getModule().getServiceRefs(), webComponentMetadata)) {
                    serviceRef.put("cmpId", webComponentMetadata.getId());
                    serviceRef.put("name", webComponentMetadata.getClassName());
                    String path = webComponentMetadata.getSource();
                    if (path == null || path.isEmpty()) {
                        path = getTSFileName(webComponentMetadata.getId());
                        webComponentMetadata.setSource(path);
                    }
                    serviceRef.put("path", path);

                    serviceRef.put("isCommon", webComponentMetadata.isCommon() ? "1" : "0");
                    serviceRef.put("alias", webComponentMetadata.getClassName() + "1");

                    json.getModule().getServiceRefs().add(serviceRef);
                }
            }
        }
    }

    private boolean hasServiceReference(List<HashMap<String, Object>> serviceReferenceList, WebComponentMetadata component) {
        boolean flag = false;
        for (HashMap<String, Object> serviceRef : serviceReferenceList) {
            if (serviceRef.containsKey("name") && serviceRef.containsKey("path")) {
                if (serviceRef.get("name") == null) {
                    throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0007, new String[]{String.valueOf(serviceRef.get("cmpId"))});
                }

                if (serviceRef.get("path") == null) {
                    throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0008, new String[]{String.valueOf(serviceRef.get("cmpId"))});
                }

                if (serviceRef.get("name").toString().equals(component.getClassName()) &&
                        serviceRef.get("path") == component.getSource()) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 获取ts文件路径和名称
     */
    private String getTSFileName(String cmpId) {
        // 最理想的方式：路径来自webcmp构件路径，名称来自path（即元数据的Source属性）
        // 先用webcmp的文件名构造，后续可以考虑调整
        if (cmpId == null || cmpId.isEmpty()) {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0002);
        }

        CommandsMetadataManager manager = new CommandsMetadataManager(this.executeEnvironment, null, this.isUpgradeTool);
        GspMetadata metadata = manager.getMetadata(() -> {
            MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
            getterMetadataInfo.setId(cmpId);
            getterMetadataInfo.setMetadataType(MetadataTypeEnum.TS);
            return getterMetadataInfo;
        }, null, null);

        if (metadata == null) {
            throw new WebCustomException("load command metatdata is null,commandId is " + cmpId);
        }

        String path = metadata.getRelativePath();
        String cmpFileName = metadata.getHeader().getFileName();
        // 后缀为.webcmp
        int suffixIndex = cmpFileName.lastIndexOf(".webcmp");
        String fileName;
        if (suffixIndex > 0 && suffixIndex + 7 == cmpFileName.length()) {
            fileName = cmpFileName.substring(0, suffixIndex);
        } else if (cmpFileName.contains(metadata.getHeader().getCode())) {
            fileName = cmpFileName;
        } else {
            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0003);
        }

        return path + "/" + fileName + ".ts";
    }

    /**
     * web 命令元数据及其额外参数
     */
    public static class WebCommandsMetadataAndExtra {
        private final WebCommandsMetadata webCommandsMetadata;
        private String metadataId;
        private String metadataFileName;
        private String metadataName;
        private String metadataRelativePath;

        public WebCommandsMetadataAndExtra(WebCommandsMetadata webCommandsMetadata) {
            this.webCommandsMetadata = webCommandsMetadata;
        }

        public WebCommandsMetadataAndExtra(WebCommandsMetadata webCommandsMetadata, String metadataId, String metadataName, String metadataFileName, String metadataRelativePath) {
            this.webCommandsMetadata = webCommandsMetadata;
            this.metadataId = metadataId;
            this.metadataName = metadataName;
            this.metadataRelativePath = metadataRelativePath;
            this.metadataFileName = metadataFileName;
        }

        public static WebCommandsMetadataAndExtra init(WebCommandsMetadata webCommandsMetadata, String metadataId, String metadataName, String metadataFileName, String metadataRelativePath) {
            return new WebCommandsMetadataAndExtra(webCommandsMetadata, metadataId, metadataName, metadataFileName, metadataRelativePath);
        }

        public WebCommandsMetadata getWebCommandsMetadata() {
            return webCommandsMetadata;
        }

        public String getMetadataId() {
            return metadataId;
        }

        public String getMetadataName() {
            return metadataName;
        }

        public String getMetadataRelativePath() {
            return metadataRelativePath;
        }

        public String getMetadataFileName() {
            return metadataFileName;
        }
    }

    /**
     * webcomponent及其额外参数
     */
    public static class WebComponentMetadataAndExtra {
        private final WebComponentMetadata webComponentMetadata;
        private String metadataId;
        private String metadataName;
        private String metadataFileName;
        private String metadataRelativePath;

        public WebComponentMetadataAndExtra(WebComponentMetadata webComponentMetadata) {
            this.webComponentMetadata = webComponentMetadata;
        }

        public WebComponentMetadataAndExtra(WebComponentMetadata webComponentMetadata, String metadataId, String metadataName, String metadataFileName, String metadataRelativePath) {
            this.webComponentMetadata = webComponentMetadata;
            this.metadataId = metadataId;
            this.metadataName = metadataName;
            this.metadataRelativePath = metadataRelativePath;
            this.metadataFileName = metadataFileName;
        }

        public static WebComponentMetadataAndExtra init(WebComponentMetadata webComponentMetadata, String metadataId, String metadataName, String metadataFileName, String metadataRelativePath) {
            return new WebComponentMetadataAndExtra(webComponentMetadata, metadataId, metadataName, metadataFileName, metadataRelativePath);
        }

        public WebComponentMetadata getWebComponentMetadata() {
            return webComponentMetadata;
        }

        public String getMetadataId() {
            return metadataId;
        }

        public String getMetadataName() {
            return metadataName;
        }

        public String getMetadataRelativePath() {
            return metadataRelativePath;
        }

        public String getMetadataFileName() {
            return metadataFileName;
        }
    }
}
