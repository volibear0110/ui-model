/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.cdp.web.component.metadata.define.WebComponentMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.tsfile.api.service.TsFileService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.io.File;

/**
 * 表单构件关联的ts 文件拷贝
 *
 * @author guozhiqi
 */
public class FormMetadataCmpSourceTsFileCloneManager {
    public static void copy(WebComponentMetadata webComponentMetadata, String sourceProjectName, String targetProjectName, GspMetadata sourceComponentMetadata, GspMetadata replicatedWebComponentMetadata) {
        String path = webComponentMetadata.getSource();
        String absolutePath = MetadataUtility.getInstance().getAbsolutePath(path);

        // 如果文件不存在
        if (!FileUtility.exists(absolutePath)) {
            return;
        }

        String tsSourceFileContent = FileUtility.readAsString(absolutePath);

        // 更新服务构件
        String targetPath = path;
        if (!sourceProjectName.equals(targetProjectName)) {
            // 调整工程路径参数
            targetPath = targetPath.replace(sourceProjectName, targetProjectName);
        }

        String updateAbsolutPath = MetadataUtility.getInstance().getAbsolutePath(targetPath);
        updateAbsolutPath = updateAbsolutPath.replace(sourceComponentMetadata.getHeader().getCode(), replicatedWebComponentMetadata.getHeader().getCode());

        File file = new File(updateAbsolutPath);
        String filePath = file.getParent();
        String fileName = file.getName();

        TsFileService tsFileService = SpringBeanUtils.getBean(TsFileService.class);
        String oldTsClassName = tsFileService.getTsClassNameByPath(absolutePath);
        String newTsClassName = tsFileService.getTsClassNameByPath(fileName);
        tsSourceFileContent = tsSourceFileContent.replace(oldTsClassName, newTsClassName);

        FileUtility.writeFile(filePath, fileName, tsSourceFileContent);

        // 更新服务构件
        WebComponentMetadata replicateComponentMetadataContent = (WebComponentMetadata) replicatedWebComponentMetadata.getContent();
        replicateComponentMetadataContent.setSource(FileUtility.getPlatformIndependentPath(FileUtility.combine(new File(targetPath).getParent(), fileName)));
        // 调整 webComponent中的扩展 ts中的class名称
        replicatedWebComponentMetadata.setExtendProperty(replicatedWebComponentMetadata.getExtendProperty().replace(oldTsClassName, newTsClassName));
        replicateComponentMetadataContent.setClassName(newTsClassName);
    }
}
