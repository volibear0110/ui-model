/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * 表单元数据服务
 */
public class FormMetadataContentService {
    ///Singleton
    private FormMetadataContentService() {

    }

    private static final FormMetadataContentService formMetadataService = new FormMetadataContentService();

    public static FormMetadataContentService getInstance() {
        return formMetadataService;
    }

    /**
     * 依据表单元数据反序列化其为FormDom
     * @param formMetadataContent
     * @return
     */
    public final FormDOM getFormContent(FormMetadataContent formMetadataContent) {
        String sourceContent = SerializeUtility.getInstance().serialize(formMetadataContent.getContents(), false);
        return SerializeUtility.getInstance().deserialize(sourceContent, FormDOM.class);
    }

    /**
     * 第一个参数formMetadataId
     * 第二个参数formCode
     * 第三个参数formName
     * 第四个参数voId
     *
     * @param formMetadata
     * @return
     */
    public final ParameterFromMetadataResult getParameterFromFormMetadata(GspMetadata formMetadata) {
        if (formMetadata == null || formMetadata.getContent() == null) {
            return null;
        }
        FormMetadataContent content = (FormMetadataContent) ((formMetadata.getContent() instanceof FormMetadataContent) ? formMetadata.getContent() : null);
        if (content == null) {
            return null;
        }

        FormDOM formDOM = getFormContent(content);
        if (formDOM != null && formDOM.getModule() != null && formDOM.getModule().getSchemas().size() > 0) {
            String voId = formDOM.getModule().getSchemas().stream().findFirst().get().get("id").toString();
            String formCode = formDOM.getModule().getCode();
            String formName = formDOM.getModule().getName();
            String formMetadataId = formMetadata.getHeader().getId();
            return new ParameterFromMetadataResult(formMetadataId, formCode, formName, voId);
        }
        return null;
    }

    /**
     * 使用静态内部类进行封装特殊类型的返回值
     * 使用内部类进行隐藏
     */
    public static class ParameterFromMetadataResult {
        private final String metadataId;
        private final String formCode;
        private final String formName;
        private final String voId;

        public ParameterFromMetadataResult(String metadataId, String formCode, String formName, String voId) {
            this.metadataId = metadataId;
            this.formCode = formCode;
            this.formName = formName;
            this.voId = voId;
        }

        public String getMetadataId() {
            return this.metadataId;
        }

        public String getFormCode() {
            return this.formCode;
        }

        public String getFormName() {
            return this.formName;
        }

        public String getVoId() {
            return this.voId;
        }
    }

    public final void setFormContent(FormMetadataContent formMetadataContent, FormDOM formDOM) {
        String formDomStr = SerializeUtility.getInstance().serialize(formDOM, false);
        JsonNode contentNode = SerializeUtility.getInstance().toJsonNode(formDomStr);
        formMetadataContent.setContents(contentNode);
    }

}
