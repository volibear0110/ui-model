/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadatamanager;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

/**
 * 表单元数据管理
 *
 * @author guozhiqi
 */
public class FormMetadataManager extends BaseMetaDataManager {
    public FormMetadataManager(ExecuteEnvironment executeEnvironment, String relativePath, boolean isUpdradeTool) {
        super(executeEnvironment, relativePath, isUpdradeTool);
    }

    /**
     * 获取可视化的DOM
     */
    public String getVisualDom(String metaDataFileName, String metaDataFilePath) {
        GspMetadata metadata = MetadataUtility.getInstance().getMetadataWithDesign(metaDataFileName, metaDataFilePath);
        this.setRelativePath(metadata.getRelativePath());
        return ((FormMetadataContent) metadata.getContent()).getContents().toString();
    }


    /**
     * 根据元数据id获取表单元数据
     *
     * @param metadataManagerParameter
     * @return
     */
    private GspMetadata getFormMetaData(MetadataManagerParameter metadataManagerParameter) {
        GspMetadata metadata = getMetadata(() -> {
            MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
            getterMetadataInfo.setId(metadataManagerParameter.getId());
            getterMetadataInfo.setCode(metadataManagerParameter.getExtendParameter().getCode());
            getterMetadataInfo.setName(metadataManagerParameter.getExtendParameter().getName());
            getterMetadataInfo.setNameSpace(metadataManagerParameter.getExtendParameter().getNameSpace());
            getterMetadataInfo.setMetadataType(MetadataTypeEnum.Frm);
            return getterMetadataInfo;
        }, null, this.getDefaultMetadataNotFoundFormatMessage(metadataManagerParameter.getId(), MetadataTypeEnum.Frm));
        if (metadata == null) {
            throw new WebCustomException("load form metadata is null,the metaDataId is " + metadataManagerParameter.getId());
        }
        return metadata;
    }

    /**
     * 根据元数据ID获取元数据信息
     */
    public String getVisualDom(String metadataID, String code, String name, String nameSpace) {
        MetadataManagerParameter metadataManagerParameter = MetadataManagerParameter.init(metadataID, code, name, nameSpace);
        GspMetadata metadata = this.getFormMetaData(metadataManagerParameter);
        this.setRelativePath(metadata.getRelativePath());
        return ((FormMetadataContent) metadata.getContent()).getContents().toString();
    }
}
