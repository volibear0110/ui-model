package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.HashMap;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;


/**
 * 展示控件策略提取策略
 */
public class MobileDisplayComponent18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    public MobileDisplayComponent18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        switch (this.componentType) {
            case ComponentTypes.ListView:
                this.collectListviewResourceItems();
                break;
            case ComponentTypes.Schedule:
                this.collectScheduleResourceItems();
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集Listview相关的资源项
     */
    private void collectListviewResourceItems() {
        this.collectToolbarResourceItems("toolbar");
        this.collectToolbarResourceItems("swipeToolbar");
    }

    /**
     * 搜集Schedule相关的资源项
     */
    private void collectScheduleResourceItems() {
        this.collecScheduleEventsResourceItems();
    }

}
