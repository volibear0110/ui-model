/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.lic;

import com.inspur.edp.lcm.metadata.spi.lic.LicControlExtendForIde;
import com.inspur.edp.lcm.metadata.spi.lic.MetadataCreateLicControlContext;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.utility.StringUtility;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.licservice.api.manager.StandardControlFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 表单元数据创建授权控制
 * @author noah
 */
public class FormMetadataCreateLicControlListener implements LicControlExtendForIde {
    private StandardControlFactory standardControlFactory = null;
    private List<ModuleWithMetadataType> MODULE_WITH_METADATA_TYPE_LIST = null;

    public FormMetadataCreateLicControlListener() {
        this.initModuleWithMetadateType();
        this.standardControlFactory = SpringBeanUtils.getBean(StandardControlFactory.class);
    }

    @Override
    public void metadataCreateControl(MetadataCreateLicControlContext context) {
        // 如果入参为空 那么不进行任何修饰
        if (context == null || context.getMetadataDto() == null || StringUtility.isNullOrEmpty(context.getMetadataDto().getType())) {
            return;
        }
        // 仅控制PC表单和移动表单
        Optional<ModuleWithMetadataType> optionalModuleWithMetadataType = this.needControl(context.getMetadataDto().getType());
        optionalModuleWithMetadataType.ifPresent(moduleWithMetadataType -> this.standardControlFactory.licStandardControl(moduleWithMetadataType.getModuleType()));
    }

    /**
     * 是否需要进行权限控制的元数据类型
     *
     * @param metaType
     * @return
     */
    private Optional<ModuleWithMetadataType> needControl(String metaType) {
        return this.MODULE_WITH_METADATA_TYPE_LIST.stream().filter(t -> t.getMetadataTypeList().contains(metaType)).findFirst();
    }

    /**
     * 初始化模块类型
     */
    private void initModuleWithMetadateType() {
        this.MODULE_WITH_METADATA_TYPE_LIST = new ArrayList<>(2);
        ModuleWithMetadataType formModuleType = new ModuleWithMetadataType();
        formModuleType.setModuleType("WDP");
        formModuleType.getMetadataTypeList().add(TerminalType.PC.getMetadataType());
        this.MODULE_WITH_METADATA_TYPE_LIST.add(formModuleType);

        ModuleWithMetadataType mobileFormModuleType = new ModuleWithMetadataType();
        mobileFormModuleType.setModuleType("MDP");
        mobileFormModuleType.getMetadataTypeList().add(TerminalType.MOBILE.getMetadataType());
        this.MODULE_WITH_METADATA_TYPE_LIST.add(mobileFormModuleType);

    }
}
