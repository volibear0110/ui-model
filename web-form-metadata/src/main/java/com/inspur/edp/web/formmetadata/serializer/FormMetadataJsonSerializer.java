/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

public class FormMetadataJsonSerializer implements MetadataContentSerializer
{
	private static final FormMetadataJsonSerializer jsonSerializer = new FormMetadataJsonSerializer();

	public static FormMetadataJsonSerializer getInstance()
	{
		return jsonSerializer;
	}

	/**
	 Json反序列化接口

	 @param contentString
	 @return
	*/
	@Override
	public final IMetadataContent DeSerialize(JsonNode contentString)
	{
		if (contentString == null)
		{
			return null;
		}

		return SerializeUtility.getInstance().<FormMetadataContent>toObject(contentString, FormMetadataContent.class);
	}

	/**
	 Json序列化接口

	 @param metadataContent
	 @return
	*/
	@Override
	public final JsonNode Serialize(IMetadataContent metadataContent)
	{
		if (metadataContent == null)
		{
			return null;
		}
		else
		{
			FormMetadataContent metadata = (FormMetadataContent)((metadataContent instanceof FormMetadataContent) ? metadataContent : null);
			return SerializeUtility.getInstance().valueToJson(metadata, PropertyNamingStrategy.UPPER_CAMEL_CASE);
		}
	}
}
