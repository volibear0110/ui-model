/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadatamanager;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;
import com.inspur.edp.web.formmetadata.constant.I18nMsgConstant;

import java.util.function.Supplier;

/**
 * 基础元数据manager
 *
 * @author noah
 */
public abstract class BaseMetaDataManager {

    private boolean isUpgradeTool = false;

    private String relativePath = "";

    /**
     * 默认的元数据找不到异常信息提示格式化参数
     */

    /**
     * 设定必须获取当前的运行环境
     *
     * @param executeEnvironment
     */
    public BaseMetaDataManager(ExecuteEnvironment executeEnvironment, String relativePath, boolean isUpgradeTool) {
        this.setExecuteEnvironment(executeEnvironment);
        this.isUpgradeTool = isUpgradeTool;
        this.relativePath = relativePath;
    }

    /**
     * 获取默认的元数据找不到异常信息
     *
     * @param metadataId
     * @param metadataType
     * @return
     */
    protected String getDefaultMetadataNotFoundFormatMessage(String metadataId, MetadataTypeEnum metadataType) {
        return ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_FORM_METADATA_MSG_0004, metadataId, metadataType.getName());
    }


    public String getRelativePath() {
        return this.relativePath;
    }

    protected void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    // 运行环境  运行时或设计时
    private ExecuteEnvironment executeEnvironment = ExecuteEnvironment.Design;

    public ExecuteEnvironment getExecuteEnvironment() {
        return this.executeEnvironment;
    }

    public void setExecuteEnvironment(ExecuteEnvironment value) {
        this.executeEnvironment = value;
    }

    /**
     * 依据元数据id获取目标元数据信息
     * 目标元数据构造不能为空，源  元数据构造允许为空，增加源 元数据构造的目的是为了异常提示友好
     *
     * @param targetMetadataSupplierAction 目标元数据参数构造supplierAction
     * @param sourceMetadataSupplierAction 源 元数据参数构造 supplierAction
     * @return
     */
    public GspMetadata getMetadata(Supplier<MetadataGetterParameter.GetterMetadataInfo> targetMetadataSupplierAction,
                                   Supplier<MetadataGetterParameter.GetterMetadataInfo> sourceMetadataSupplierAction, String customTargetMetadataNotFoundMessage) {
        if (targetMetadataSupplierAction == null) {

            throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0010);
        }

        return MetadataUtility.getInstance().getMetadataWithEnvironment(targetMetadataSupplierAction, sourceMetadataSupplierAction,
                customTargetMetadataNotFoundMessage, this.executeEnvironment, isUpgradeTool);
    }

    public String serialize(IMetadataContent obj) {
        return SerializeUtility.getInstance().serialize(obj, false);
    }

    public String serialize(IMetadataContent obj, PropertyNamingStrategy namingStrategy) {
        return SerializeUtility.getInstance().serialize(obj, namingStrategy, false);
    }

    public void saveMetadataFile(String path, String fileName, String contents) {
        FileUtility.writeFile(path, fileName, contents);
    }

}
