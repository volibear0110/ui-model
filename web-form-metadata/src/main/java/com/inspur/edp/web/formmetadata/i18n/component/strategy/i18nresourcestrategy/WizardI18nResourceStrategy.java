/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Wizard组件策略
 *
 * @author noah
 */
public class WizardI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String itemsI18nResourceItemBaseId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + ComponentUtility.getInstance().getId(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        I18nResourceItemCollection progressDataI18nResourceItemCollection = extractProgressDataI18nResourceItemCollection(i18nResourceItemBaseId, itemsI18nResourceItemBaseId, currentComponent);
        if (progressDataI18nResourceItemCollection != null && progressDataI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(progressDataI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractProgressDataI18nResourceItemCollection(String i18nResourceItemBaseId, String itemsI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        itemsI18nResourceItemBaseId = itemsI18nResourceItemBaseId + ComponentUtility.getInstance().getProgressDataName(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;

        HashMap<String, Object> progressDataObject = ComponentUtility.getInstance().getProgressData(currentComponent);

        I18nResourceItemCollection stepMessagesI18nResourceItemCollection = extractStepMessagesI18nResourceItemCollection(i18nResourceItemBaseId, itemsI18nResourceItemBaseId, progressDataObject);
        if (stepMessagesI18nResourceItemCollection != null && stepMessagesI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(stepMessagesI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }


    private I18nResourceItemCollection extractStepMessagesI18nResourceItemCollection(String i18nResourceItemBaseId, String itemsI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        if (currentComponent == null || currentComponent.size() == 0) {
            return i18nResourceItemCollection;
        }
        itemsI18nResourceItemBaseId = itemsI18nResourceItemBaseId + ComponentUtility.getInstance().getStepMessagesName(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER;

        ArrayList<HashMap<String, Object>> stepMessageCollection = ComponentUtility.getInstance().getStepMessages(currentComponent);


        for (HashMap<String, Object> stepMessageObject : stepMessageCollection) {
            String componentId = ComponentUtility.getInstance().getId(stepMessageObject);
            String componentName = ComponentUtility.getInstance().getTitle(stepMessageObject);

            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, itemsI18nResourceItemBaseId + componentId, componentName, componentName);
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }
}
