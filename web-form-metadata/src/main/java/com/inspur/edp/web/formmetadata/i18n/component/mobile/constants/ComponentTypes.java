package com.inspur.edp.web.formmetadata.i18n.component.mobile.constants;

/**
 * 控件类型常量
 */
public class ComponentTypes {

    // 基础控件
    public final static String Button = "Button";
    public final static String ButtonGroup = "ButtonGroup";
    public final static String ToolBarArea = "ToolBarArea";
    public final static String SearchBox = "SearchBox";
    public final static String TabBar = "TabBar";
    public final static String NavigationBar = "NavigationBar";

    // 容器控件
    public final static String Component = "Component";
    public final static String TabPage = "TabPage";
    public final static String FieldSet = "FieldSet";
    public final static String Card = "Card";
    public final static String Section = "Section";

    // 输入控件
    public final static String TextBox = "TextBox";
    public final static String MultiTextBox = "MultiTextBox";
    public final static String NumericBox = "NumericBox";
    public final static String DateBox = "DateBox";
    public final static String CalendarInput = "CalendarInput";
    public final static String RadioGroup = "RadioGroup";
    public final static String CheckGroup = "CheckGroup";
    public final static String EnumField = "EnumField";
    public final static String LookupEdit = "LookupEdit";
    public final static String RichTextEditor = "RichTextEditor";
    public final static String SwitchField = "SwitchField";
    public final static String TelphoneTextBox = "TelphoneTextBox";
    public final static String ScanTextBox = "ScanTextBox";
    public final static String AreaPicker = "AreaPicker";
    public final static String CityPicker = "CityPicker";
    public final static String CustomBox = "CustomBox";
    public final static String PersonnelSelector = "PersonnelSelector";
    public final static String OrganizationSelector = "OrganizationSelector";
    public final static String ExternalDataInput = "ExternalDataInput";

    // 展示类控件
    public final static String ListView = "ListView";
    public final static String Schedule = "Schedule";
    public final static String StaticText = "StaticText";

    // 业务控件
    public final static String QueryScheme = "QueryScheme";
    public final static String ApprovalLogsPanel = "ApprovalLogsPanel";
    public final static String ApprovalInfo = "ApprovalInfo";
    public final static String PersonnelInfo = "PersonnelInfo";
    public final static String ApprovalComments = "ApprovalComments";

    // 集合控件
    public final static String LightAttachment = "LightAttachment";

    // 导航控件
    public final static String ScrollNavbar = "ScrollNavbar";
}
