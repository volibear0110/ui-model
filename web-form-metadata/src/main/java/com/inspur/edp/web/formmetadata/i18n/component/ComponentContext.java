/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.HashMap;

/**
 * 组件上下文
 *
 * @author noah
 */
public class ComponentContext {
    /**
     * 组件
     */
    private final BaseComponent component;

    public ComponentContext(HashMap<String, Object> currentComponent, String curretnI18nResourceItemBaseId, FormDOM formDOM) {
        component = new BaseComponent(currentComponent, curretnI18nResourceItemBaseId, formDOM);
    }

    /**
     * 从组件中提取多语资源
     */
    public final I18nResourceItemCollection extractI18nResource() {
        if (component == null) {
            return null;
        }

        return component.extractI18nResource();
    }
}
