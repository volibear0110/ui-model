/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.web.common.utility.StringUtility;
import org.apache.commons.lang3.StringUtils;

/**
 * 资源项构造
 */
public class I18nResourceItemManager {
    /**
     * 创建对应的资源项
     *
     * @return
     */
    private static I18nResourceItem createI18nResourceItem(String key, String value, String comment) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        if (StringUtils.isEmpty(value)) {
            value = "";
        }
        if (StringUtils.isEmpty(comment)) {
            comment = "";
        }
        I18nResourceItem i18nResourceItem = new I18nResourceItem();
        i18nResourceItem.setKey(key);
        i18nResourceItem.setValue(value);
        i18nResourceItem.setComment(comment);
        return i18nResourceItem;
    }

    /**
     * 创建对应的资源项
     * 在值为html格式时，不进行国际化提取 返回null
     *
     * @param strBaseId 基础id
     * @param key
     * @param value
     * @param comment
     * @return
     */
    public static I18nResourceItem createI18nResourceItem(String strBaseId, String key,
                                                          String value, String comment) {
        if (StringUtils.isEmpty(comment)) {
            comment = value;
        }

        // html 格式的字符串不进行国际化提取
        if (!StringUtility.isNullOrEmpty(value) && StringUtility.isHtml(value)) {
            return null;
        }

        String strKey = MergeI18nResourceId(strBaseId, key);
        return createI18nResourceItem(strKey, value, comment);
    }

    /**
     * 资源项id合并
     *
     * @param strBaseId
     * @param resourceId
     * @return
     */
    public static String MergeI18nResourceId(String strBaseId, String resourceId) {
        if (StringUtils.isEmpty(strBaseId)) {
            strBaseId = "";
        }

        // 针对资源项key中的点号 替换成下划线
        resourceId = resourceId.replace(".", "_");
        String strKey = strBaseId + resourceId;
        return strKey;
    }
}
