package com.inspur.edp.web.formmetadata.formformat.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.customize.form.api.RuntimeCheckVoFormVariableService;
import com.inspur.edp.customize.service.VoRuntimeBuildFormFormatService;
import com.inspur.edp.formserver.viewmodel.formentity.*;
import com.inspur.edp.formserver.vmapi.formconfig.VoBuildFormFormatService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.web.formmetadata.formformat.FormFormatHandler;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
import io.iec.edp.caf.businessobject.api.service.DevBasicInfoService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import com.inspur.edp.formserver.viewmodel.formentity.ButtonGroup;
import com.inspur.edp.formserver.viewmodel.formentity.ObjectData;
import com.inspur.edp.formserver.viewmodel.formentity.VoFormModel;

import java.util.List;


/**
 * 向vo推送按钮、字段、命令，用于权限控制
 */
public class WFFormFormatMobileHandler  extends FormFormatHandler {

    private final  List<String> controlsWithButton = Arrays.asList("ListView","Button","ButtonGroup","Card","Section");

    /**
     * 表单类型
     * @return
     */
    @Override
    public String getFormType() {
        return "Mobile";
    }

    @Override
    public void pushFormFormat(GspMetadata formMetadata, String formatType, String formPath) {
        MetadataProjectService projectService = SpringBeanUtils.getBean(MetadataProjectService.class);
        Path path = Paths.get(formPath);
        Path parentPath = path.getParent();
        String formPathExcludeFileName = parentPath != null ? parentPath.toString() : "";
        String projectPath = projectService.getProjPath(formPathExcludeFileName);
        VoBuildFormFormatService formFormatService = SpringBeanUtils.getBean(VoBuildFormFormatService.class);
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();
        JsonNode content = formContents.getContents();
        String voId = content.at("/module/schemas/0/id").textValue();
        formFormatService.buildFormFormat(getVoFormModel(formMetadata, formatType, projectPath), voId, formMetadata.getHeader().getBizobjectID());
    }

    @Override
    public void pushRtcFormFormat(GspMetadata formMetadata, String formatType, DimensionExtendEntity dimension) {
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();
        JsonNode content = formContents.getContents();
        String voId = content.at("/module/schemas/0/id").textValue();
        RuntimeCheckVoFormVariableService runtimeCheckVoFormVariableService = SpringBeanUtils.getBean(RuntimeCheckVoFormVariableService.class);
        boolean isPushRtcForm = runtimeCheckVoFormVariableService.checkVoFormVariable(voId);
        if (!isPushRtcForm) {
            return;
        }
        VoRuntimeBuildFormFormatService voRuntimeBuildFormFormatService = SpringBeanUtils.getBean(VoRuntimeBuildFormFormatService.class);

        VoFormModel voFormModel = getVoFormModel(formMetadata, formatType, null);
        String dim1 = StringUtils.isNotBlank(dimension.getFirstDimension())?dimension.getFirstDimension():"public";
        String dim2 = StringUtils.isNotBlank(dimension.getSecondDimension())?dimension.getSecondDimension():"public";

        generateParameters(voFormModel.getUrlParameters(), "dim1", "维度一", dim1);
        generateParameters(voFormModel.getUrlParameters(), "dim2", "维度二", dim2);
        generateParameters(voFormModel.getUrlParameters(), "metadataId", "元数据id", dimension.getBasicMetadataId());

        voRuntimeBuildFormFormatService.buildFormFormat(voFormModel, dimension, voId, formMetadata.getHeader().getBizobjectID());
    }

    /**
     * 获取表单模型
     * @param formMetadata
     * @param formatType
     * @return
     */
    @Override
    public VoFormModel getVoFormModel(GspMetadata formMetadata, String formatType, String basePath) {
        VoFormModel voFormModel = new VoFormModel();
        MetadataHeader header = formMetadata.getHeader();
        FormMetadataContent formContents = (FormMetadataContent) formMetadata.getContent();

        JsonNode content = formContents.getContents();
        String voId = content.at("/module/schemas/0/id").textValue();
        voFormModel.setId(header.getId());
        voFormModel.setCode(header.getCode());
        voFormModel.setName(header.getName());
        voFormModel.setTerminal(getFormType());
        voFormModel.setFormatType(formatType);
        voFormModel.setObjectAuthInfo(this.getObjectData(content));
        voFormModel.setMethods(this.getMethods(content));
        voFormModel.setButtonGroup(this.getButtons(content));
        //voFormModel.setAttachmentGroup(getAttachmentGroups(content));
//        voFormModel.setBizcategory(header.getBizobjectID());
//        voFormModel.setRefId(voId);
        voFormModel.setProjectPath(basePath);
        generateFormFormat(voFormModel, formMetadata, basePath);


        return voFormModel;
    }

//    @Override
//    public List<AttachmentGroup> getAttachmentGroups(JsonNode formContent) {
//        // 获取component中字段信息
//        JsonNode components = formContent.at("/module/components");
//        List<AttachmentGroup> attachmentGroups = new ArrayList<>();
//        extractAttachment(null, components, attachmentGroups);
//        return attachmentGroups;
//    }

    /**
     * 获取实体信息
     * @param formContent
     * @return
     */
    @Override
    public ObjectData getObjectData(JsonNode formContent) {
        // 1.提取viewmodel中的字段
        JsonNode viewModels = formContent.at("/module/viewmodels");
        List<String> fieldIds = new ArrayList<>();
        viewModels.forEach(viewModel -> {
            JsonNode fields = viewModel.get("fields");
            if (fields != null && !fields.isEmpty()) {
                fieldIds.addAll(fields.findValuesAsText("id"));
            }
        });
        JsonNode schema = formContent.at("/module/schemas/0");
        if (schema == null || schema.isEmpty()) {
            return null;
        }

        // 2.获取component中字段信息
        JsonNode components = formContent.at("/module/components");
        Map<String, ChildData> fieldMap = new HashMap<>();
        extractComponents(components, fieldMap, fieldIds);

        // 3.构件实体树
        JsonNode mainEntity = schema.at("/entities/0");
        ObjectData root = new ObjectData();
        extractEntities(mainEntity, root, fieldMap);
        return root;
    }




    /**
     * 提取按钮
     * @param formContent 表单内容
     * @return 按钮信息
     */
    @Override
    public List<ButtonGroup> getButtons(JsonNode formContent){
        // 递归找Button
        JsonNode componentArr = formContent.at("/module/components");
        JsonNode viewmodelArr = formContent.at("/module/viewmodels");

        List<ButtonGroup> buttonGroupList  = new ArrayList<>();
        List<String> cardPageviewModelIdList = new ArrayList<>();

        componentArr.forEach(component->{
            String componentType = getValue(component,"componentType",null);
            String pageType = getValue(component,"pageType",null);
            String viewModelId = getValue(component,"viewModel",null);

            //找到卡片页面的视图模型
            if( "Page".equals(componentType) && "Card".equals(pageType) && StringUtils.isNotBlank(viewModelId)){
                cardPageviewModelIdList.add(viewModelId);
            }
        });

        for(JsonNode component :componentArr){
            String viewModelId = getValue(component,"viewModel",null);
            JsonNode parentViewModel = findParentViewModelId( viewmodelArr, viewModelId);

            //只提取卡片页面及其子组件
            if( cardPageviewModelIdList.contains(getValue(parentViewModel,"id",null))){
                searchComponentsForButton(component, buttonGroupList);
            }
        }

        return buttonGroupList;
    }

    private JsonNode findParentViewModelId(JsonNode viewmodelArr,String curViewmodelId){
        JsonNode result = null;
        if(StringUtils.isNotBlank(curViewmodelId) ){
            for(JsonNode viewmodel :viewmodelArr){
                if(curViewmodelId.equals(getValue(viewmodel,"id",null))){
                    result = viewmodel;
                    String parentViewModelId =  getValue(viewmodel,"parent",null);
                    if(StringUtils.isNotBlank(parentViewModelId)){
                        JsonNode parentViewModel = findParentViewModelId( viewmodelArr, parentViewModelId);
                        if(parentViewModel != null ){
                            result = parentViewModel;

                        }
                    }
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 提取方法
     * @param formContent
     * @return
     */
    public List<MethodGroup> getMethods(JsonNode formContent) {
        // 1.遍历页面组件
        JsonNode componentArr = formContent.at("/module/components");
        String cardViewModelId = null;
        for(JsonNode component:componentArr){
            String componentType = getValue(component,"componentType",null);
            String pageType = getValue(component,"pageType",null);

            //找到卡片页面
            if( "Page".equals(componentType) && "Card".equals(pageType)){
                cardViewModelId =  getValue(component,"viewModel","");
                break;
            }

        }

        List<MethodGroup> methodGroupList = new ArrayList<>();
        if(StringUtils.isNotBlank(cardViewModelId)){
            JsonNode viewModelArr = formContent.at("/module/viewmodels");
            //2.遍历视图模型
            for(JsonNode viewModel:viewModelArr){
                String viewModelId = getValue(viewModel,"id",null);
                if(StringUtils.isNotBlank(viewModelId) && cardViewModelId.equals(viewModelId)){
                    JsonNode commands = viewModel.get("commands");
                    //3.遍历命令
                    if(commands != null && commands.size() > 0){
                        commands.forEach(command->{
                            MethodGroup methodGroup = new MethodGroup();
                            methodGroup.setId(getValue(command,"id",null));
                            methodGroup.setMethodCode(getValue(command,"code",null));
                            methodGroup.setMethodName(getValue(command,"name",null));
                            //4.遍历命令参数
                            JsonNode params = command.get("params");
                            if(params != null && params.size() > 0){
                                List<MethodParam> methodParamList = new ArrayList<>();
                                params.forEach(param->{
                                    MethodParam methodParam = new MethodParam();
                                    methodParam.setCode(getValue(param,"name",null));
                                    methodParam.setName(getValue(param,"shownName",null));
                                    methodParam.setValue(null);
                                    methodParamList.add(methodParam);

                                });
                                methodGroup.setMethodParam(methodParamList);
                            }
                            methodGroupList.add(methodGroup);

                        });

                    }
                }
            }
        }
        return methodGroupList;
    }


    /**
     * 查找带按钮的组件
     * @param root 页面组件
     * @param buttonGroupList 按钮组列表
     */
    private void searchComponentsForButton(JsonNode root, List<ButtonGroup> buttonGroupList) {
        if(root.isArray()) {
            for (JsonNode jsonNode : root) {
                searchComponentsForButton(jsonNode, buttonGroupList);
            }
        }
        if(root.isObject()){
            JsonNode type = root.get("type");
            if(type != null && controlsWithButton.contains(type.asText())){
                this.findButtons(root, buttonGroupList);
            }else {
                Iterator<String> fieldNames = root.fieldNames();
                while(fieldNames.hasNext()) {
                    String fieldName = fieldNames.next();
                    JsonNode fieldValue = root.get(fieldName);
                    if(fieldValue.isArray() || fieldValue.isObject()){
                        searchComponentsForButton(fieldValue, buttonGroupList);
                    }
                }
            }
        }
    }

    /**
     * 查找控件中按钮
     * @param node 组件节点
     * @param buttonGroupList 按钮组列表
     */
    private void findButtons(JsonNode node,List<ButtonGroup> buttonGroupList){
        String type = node.get("type").asText();
        switch (type){
            case "NavigationBar":{
                JsonNode toolbar =  node.get("toolbar");
                if(null == toolbar){
                    break;
                }
                ArrayNode items = (ArrayNode) toolbar.get("items");
                if(items !=null && items.size() > 0 ){
                    this.findComplexButtons( items, node , buttonGroupList);
                }
                break;
            }
            case "ListView":{
                //删除
                JsonNode swipeToolbar =  node.get("swipeToolbar");
                if(swipeToolbar != null){
                    ArrayNode items = (ArrayNode) swipeToolbar.get("items");
                    if(items != null && items.size() > 0 ){
                        this.findComplexButtons( items, node , buttonGroupList);
                    }
                }

                //多选
                JsonNode toolbar =  node.get("toolbar");
                if(toolbar != null){
                    ArrayNode items = (ArrayNode) toolbar.get("items");
                    if(items != null && items.size() > 0 ){
                        this.findComplexButtons( items, node , buttonGroupList);
                    }
                }
                break;
            }
            case "Button":{
                findSingleButton(node, buttonGroupList);
                break;
            }
            case "ButtonGroup":{
                ArrayNode items = (ArrayNode) node.get("items");
                if(items != null && items.size() > 0 ){
                    this.findComplexButtons( items, node , buttonGroupList);
                }
                break;
            }
            case "Card":{
                ArrayNode items = (ArrayNode) node.get("actions");
                if(items != null && items.size() > 0 ){
                    this.findComplexButtons( items, node , buttonGroupList);
                }
                break;
            }
            case "Section":{
                JsonNode footerToolbar =  node.get("footerToolbar");
                if(null == footerToolbar){
                    break;
                }
                ArrayNode items = (ArrayNode) footerToolbar.get("items");
                if(items !=null && items.size() > 0 ){
                    this.findComplexButtons( items, node , buttonGroupList);
                }
                break;
            }
        }
    }

    /**
     * 查找简单按钮
     * @param node 组件节点
     * @param buttonGroupList 按钮组列表
     */
    private void findSingleButton(JsonNode node, List<ButtonGroup> buttonGroupList) {
        // 1.创建按钮组。button 当前没有分组信息，用方法名暂时做分组名
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.setId(UUID.randomUUID().toString());
        buttonGroup.setCode(buttonGroup.getId());
        String name = "按钮";
        String id = "";
        if (node.get("id") != null) {
            id = node.get("id").asText();
        }
        name += "[" + id + "]";
        buttonGroup.setName(name);

        //3.创建按钮
        List<String> configAbleAttrs = getConfigAbleAttrs(node);
        if (!configAbleAttrs.isEmpty()) {
            Button button  = new Button();
            button.setId(node.get("id").asText());
            button.setCode(node.get("id").asText());
            button.setType(node.get("type").asText());
            button.setName(getName(node,"按钮"));

            button.setConfigurableAttrs(configAbleAttrs);
            buttonGroup.setButtons(Collections.singletonList(button));
        }

        buttonGroupList.add(buttonGroup);
    }

    /**
     * 查找复杂按钮
     * @param childNodes 按钮项
     * @param parentNode 按钮父级信息
     * @param buttonGroupList 按钮组列表
     */
    private void findComplexButtons(ArrayNode childNodes, JsonNode parentNode , List<ButtonGroup> buttonGroupList){
        // 1.创建按钮组。button 当前没有分组信息，用方法名暂时做分组名
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.setId(parentNode.get("id").asText());
        buttonGroup.setCode(parentNode.get("id").asText());
        String name= getName(parentNode,"按钮");
        String id = "";
        if (parentNode.get("id") != null) {
            id = parentNode.get("id").asText();
        }
        name += "[" + id + "]";
        buttonGroup.setName(name);

        List<Button> buttons  = new ArrayList<>();
        childNodes.forEach(item -> {
            List<String> configAbleAttrs = getConfigAbleAttrs(item);
            if (!configAbleAttrs.isEmpty()) {
                Button button  = new Button();
                button.setId(item.get("id").asText());
                button.setCode(item.get("id").asText());
                button.setName(getName(item,"按钮"));

                button.setType(getValue(item,"type",null));
                button.setConfigurableAttrs(configAbleAttrs);
                buttons.add(button);
            }
        });

        buttonGroup.setButtons(buttons);
        buttonGroupList.add(buttonGroup);
    }

    /**
     * 获取对象的值
     * @param node 节点
     * @param key 属性名
     * @param defaultValue 默认值
     * @return 属性值
     */
    private String getValue(JsonNode node,String key,String defaultValue){
        JsonNode result = node.get(key);
        if(result != null){
            return result.asText();
        }
        return  defaultValue;
    }

    /**
     * 获取component中字段信息
     * @param components
     * @param fieldMap
     * @param fieldIds
     */
    private void extractComponents(JsonNode components, Map<String, ChildData> fieldMap, List<String> fieldIds) {
        for (JsonNode component : components) {
            //1.跳过列表页

            String componentType = getValue(component,"componentType",null);
            String pageType = getValue(component,"pageType",null);
            if( "Page".equals(componentType) && !"Card".equals(pageType)){
                continue;
            }
            JsonNode binding = component.get("binding");
            if (binding != null) {
                JsonNode fieldId = binding.get("field");
                if (fieldId != null) {
                    String fieldIdText = fieldId.asText();
                    if (fieldIds.contains(fieldIdText)) {
                        ChildData fieldData = new ChildData();
                        List<String> configAbleAttrs = getConfigAbleAttrs(component);
                        fieldData.setConfigurableAttrs(configAbleAttrs);
                        if (!configAbleAttrs.isEmpty()) {
                            // 获取字段名称
                            fieldData.setName(getName(component,"字段"));
                            fieldMap.put(fieldIdText, fieldData);
                        }
                        continue;
                    }
                }
            }
            if (component.get("contents") != null && !component.get("contents").isEmpty()) {
                extractComponents(component.get("contents"), fieldMap, fieldIds);
            }
            if (component.get("fields") != null && !component.get("fields").isEmpty()) {
                extractComponents(component.get("fields"), fieldMap, fieldIds);
            }
        }
    }

    /**
     * 获取组件名称
     * @param component
     * @return
     */
    private String getName(JsonNode component,String defaultName) {
        JsonNode text = component.get("text");
        if (text != null) {
            return text.asText();
        }

        JsonNode title = component.get("title");
        if (title != null) {
            return title.asText();
        }
        return defaultName;

    }

    /**
     * 构件实体树
     * @param entityNode
     * @param entity
     * @param fieldMap
     */
    private void extractEntities(JsonNode entityNode, ObjectData entity, Map<String, ChildData> fieldMap) {
        entity.setId(entityNode.get("id").asText());
        entity.setCode(entityNode.get("label").asText());
        entity.setName(entityNode.get("name").asText());

        List<ChildData> fields = new ArrayList<>();
        JsonNode fieldNodes = entityNode.at("/type/fields");
        extractFields(fieldNodes, fields, fieldMap);
        entity.setElements(fields);

        JsonNode childEntityNodes =  entityNode.at("/type/entities");
        if (childEntityNodes != null && !childEntityNodes.isEmpty()) {
            List<ObjectData> childEntities = new ArrayList<>();
            entity.setChildObjects(childEntities);
            childEntityNodes.forEach(childEntityNode -> {
                ObjectData childEntity = new ObjectData();
                extractEntities(childEntityNode, childEntity, fieldMap);
                childEntities.add(childEntity);
            });
        }
    }

    /**
     * 构件字段
     * @param fieldNodes
     * @param fields
     * @param fieldMap
     */
    public void extractFields(JsonNode fieldNodes, List<ChildData> fields, Map<String, ChildData> fieldMap) {
        fieldNodes.forEach(fieldNode -> {
            ChildData field = new ChildData();
            String id = fieldNode.get("id").asText();
            field.setId(fieldNode.get("id").asText());
            field.setName(fieldNode.get("name").asText());
            field.setCode(fieldNode.get("bindingPath").asText());
            if (fieldMap.containsKey(id)) {
                // 如果当前字段需要抽取，则赋值component中名称
                field.setName(fieldMap.get(id).getName());
                field.setConfigurableAttrs(fieldMap.get(id).getConfigurableAttrs());
                fields.add(field);
            }
            JsonNode childFieldNodes = fieldNode.at("/type/fields");
            if (childFieldNodes != null && !childFieldNodes.isEmpty()) {
                List<ChildData> childFields = new ArrayList<>();
                extractFields(childFieldNodes, childFields, fieldMap);
                if (!childFields.isEmpty()) {
                    field.setChildFields(childFields);
                    fields.add(field);
                }

            }
        });
    }

    /**
     * 获取移动主表卡片页面
     * @param md
     * @return
     */
    private JsonNode getMobilePages(GspMetadata md){
        JsonNode result = null;

        FormMetadataContent content = (FormMetadataContent) md.getContent();
        JsonNode formContent = content.getContents();

        JsonNode components = formContent.get("module").get("components");
        JsonNode viewmodels = formContent.get("module").get("viewmodels");

        if(components != null && components.size() > 0 && viewmodels != null && viewmodels.size() > 0 ){
            for(JsonNode component:components){
                //页面
                if(component.get("componentType") == null || !component.get("componentType").asText().equals("Page") ){
                    continue;
                }
                //卡片
                if(component.get("pageType") == null ||  !component.get("pageType").asText().equals("Card")){
                    continue;
                }
                //有路由地址
                if(component.get("route") == null || component.get("route").get("uri")  == null){
                    continue;
                }
                //主表
                for(JsonNode viewmodel:viewmodels){
                    if(viewmodel.get("id") == null || !viewmodel.get("id").equals(component.get("viewModel"))){
                        continue;
                    }
                    if(viewmodel.get("bindTo") == null || viewmodel.get("bindTo").asText().equals("") || viewmodel.get("bindTo").asText().equals("/") ){
                        result = component;

                    }
                }
                if(result != null){
                    break;
                }
            }
        }
        return result;
    }

    private void generateFormFormat(VoFormModel voFormModel, GspMetadata md, String formPath){
        FormMetadataContent formMetadataContent = (FormMetadataContent) md.getContent();
        JsonNode content = formMetadataContent.getContents();

        JsonNode codeNode = content.at("/module/code");
        String code = "";
        if (codeNode != null && !StringUtils.isEmpty(codeNode.asText())) {
            code = codeNode.asText();
        }
        JsonNode page = this.getMobilePages(md);
        String uri = page.get("route").get("uri").asText();
        String url = "";
        if (StringUtils.isEmpty(formPath)) {
            DevBasicInfoService devBasicInfoService = SpringBeanUtils.getBean(DevBasicInfoService.class);
            DevBasicBoInfo devBasicBoInfo = devBasicInfoService.getDevBasicBoInfo(md.getHeader().getBizobjectID());
            String serviceUnitPath = devBasicBoInfo.getAppCode().toLowerCase() + "/" + devBasicBoInfo.getSuCode().toLowerCase();
            String projectName = content.at("/module/projectName").asText();
            url = "/apps/" + serviceUnitPath + "/mob/" + projectName + "/index.html#/" + code;
        } else {
            MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
            GspProject project = metadataService.getGspProjectInfo(formPath);
            String deploymentPath = project.getSuDeploymentPath();
            String projectName = project.getMetadataProjectName().toLowerCase();
            url = "/" + deploymentPath + "/mob/" + projectName + "/index.html#/" + code;
        }
        url += "/" + uri;

        url += "?formRulePushedFrom=LCDP";
        JsonNode formRulePushMode = content.at("/options/formRulePushMode");
        if (formRulePushMode != null) {
            url += "&formRulePushMode=" + formRulePushMode.textValue();
        }
        voFormModel.setUrlType("url");
        voFormModel.setUrl(url);

        List<Parameters> parameters = new ArrayList<>();
        generateParameters(parameters, "action", "动作", "LoadAndViewForCard");
        generateParameters(parameters, "id", "内码", "{\"expr\":\"DefaultFunction.GetContextParameter(\\\"dataId\\\")\",\"sexpr\":\"\"}\n");

        voFormModel.setUrlParameters(parameters);
    }


    private static List<String> getConfigAbleAttrs(JsonNode button) {
        List<String> configAbleAttrs = new ArrayList<>();
        setConfigAbleAttrs(button.get("visibleControlledByRules"), "Visible", configAbleAttrs);
        setConfigAbleAttrs(button.get("disableControlledByRules"), "Disable", configAbleAttrs);
        setConfigAbleAttrs(button.get("readonlyControlledByRules"), "Readonly", configAbleAttrs);
        setConfigAbleAttrs(button.get("requireControlledByRules"), "Required", configAbleAttrs);
        return configAbleAttrs;
    }

    /**
     * 获取字段配置信息
     *
     * @param attr
     * @param code
     * @param configAbleAttrs
     */
    private static void setConfigAbleAttrs(JsonNode attr, String code, List<String> configAbleAttrs) {
        if (attr != null) {
            boolean visibleControl = attr.asBoolean(false);
            if (visibleControl) {
                configAbleAttrs.add(code);
            }
        }
    }

    private void addConfigAbleAttrs(String code, String name, List<ConfigableAttrs> configAbleAttrs) {
        ConfigableAttrs param = new ConfigableAttrs();
        param.setName(name);
        param.setCode(code);
        configAbleAttrs.add(param);
    }

    private void generateParameters(List<Parameters> parameters, String code, String name, String value) {
        Parameters actionParam = new Parameters();
        actionParam.setCode(code);
        actionParam.setName(name);
        actionParam.setValue(value);
        parameters.add(actionParam);
    }
}
