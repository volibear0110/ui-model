/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * 帮助控件
 */
public class HelpProviderI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 使用title作为文本
     */
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        String componentName = componentNameStrategy.getComponentName(component);

        return componentName;
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从placeHolder属性中提取多语字段
        I18nResourceItemCollection placeHolderI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractPlaceHolderI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (placeHolderI18nResourceItemCollection != null && placeHolderI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(placeHolderI18nResourceItemCollection);
        }

        I18nResourceItemCollection dialogTitleI18nResourceItemCollection = ExtractDialogTitleI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (dialogTitleI18nResourceItemCollection != null && dialogTitleI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(dialogTitleI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    /**
     * 获取dialogTitle属性中多语资源项集合
     */
    private I18nResourceItemCollection ExtractDialogTitleI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从dialogTitle属性中提取多语字段
        String dialogTitleValue = ComponentUtility.getInstance().GetDiaglogTitle(currentComponent);
        String dialogTitleName = ComponentUtility.getInstance().GetDiaglogTitleName(currentComponent);
        if (!StringUtils.isEmpty(dialogTitleName)) {
            String componentId = ComponentUtility.getInstance().getId(currentComponent);
            String generatedComponentId = ComponentUtility.getInstance().getType(currentComponent) + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + dialogTitleName;
            I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, dialogTitleValue, dialogTitleValue);
            this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }
}
