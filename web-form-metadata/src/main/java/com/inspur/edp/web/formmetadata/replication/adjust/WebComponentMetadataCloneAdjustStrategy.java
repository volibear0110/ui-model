/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.inspur.edp.cdp.web.component.metadata.define.WebComponentMetadata;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.formmetadata.replication.MetadataReplicationContext;

import java.util.regex.Pattern;

/**
 * web 组件元数据 参数调整
 *
 * @author guozhiqi
 */
class WebComponentMetadataCloneAdjustStrategy implements GspMetadataCloneAdjustStrategy {
    @Override
    public void adjust(GspMetadata replicateMetadata, IMetadataContent replicateMetadataContent, MetadataReplicationContext context) {
        if (!(replicateMetadataContent instanceof WebComponentMetadata)) {
            return;
        }
        WebComponentMetadata replicateWebComponentMetadataContent = (WebComponentMetadata) replicateMetadataContent;
        replicateWebComponentMetadataContent.setId(replicateMetadata.getHeader().getId());
        replicateWebComponentMetadataContent.setCode(replicateMetadata.getHeader().getCode());

        // TODO：优化实现方式
        replicateWebComponentMetadataContent.setFormCode(replicateMetadata.getHeader().getCode().split(Pattern.quote("_"), -1)[0]);

        // 使用构件的序列化器
        com.inspur.edp.cdp.web.component.metadata.serializer.JsonSerializer jsonSerializer = new com.inspur.edp.cdp.web.component.metadata.serializer.JsonSerializer();
        replicateMetadata.setContent(jsonSerializer.DeSerialize(jsonSerializer.Serialize(replicateWebComponentMetadataContent)));
    }
}
