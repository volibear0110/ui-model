/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.HashMap;

/**
 * header 组件的国际化提取
 */
public class HeaderI18nResourceStrategy extends AbstractI18nResourceStrategy {
    @Override
    protected String getComponentName(HashMap<String, Object> component) {
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.TITLE);
        if (componentNameStrategy == null) {
            return null;
        }

        return componentNameStrategy.getComponentName(component);
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // mainTitle 提取
        I18nResourceItem mainTitleI18nResourceItem = extractMainTitleResourceItem(i18nResourceItemBaseId, currentComponent);
        this.addInCollection(i18nResourceItemCollection, mainTitleI18nResourceItem);

        // subTitle提取
        I18nResourceItem subTitleI18nResourceItem = extractSubTitleResourceItem(i18nResourceItemBaseId, currentComponent);
        this.addInCollection(i18nResourceItemCollection, subTitleI18nResourceItem);


        return i18nResourceItemCollection;
    }


    private I18nResourceItem extractMainTitleResourceItem(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        String mainTitleValue = ComponentUtility.getInstance().getMainTitle(currentComponent);
        String mainTitleName = ComponentUtility.getInstance().getMainTitleName();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);

        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + mainTitleName;

        return I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, mainTitleValue, mainTitleValue);
    }

    private I18nResourceItem extractSubTitleResourceItem(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        String subTitleValue = ComponentUtility.getInstance().getSubTitle(currentComponent);
        String subTitleName = ComponentUtility.getInstance().getSubTitleName();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);

        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + subTitleName;

        return I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, subTitleValue, subTitleValue);
    }
}
