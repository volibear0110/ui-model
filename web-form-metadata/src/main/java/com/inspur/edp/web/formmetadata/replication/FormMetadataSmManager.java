/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/02/26
 */
class FormMetadataSmManager {
    public static void copyFormMetadataStateMachine(MetadataReplicationContext metadataReplicationContext, GspMetadata sourceFormMetadata,
                                                    MetadataDto targetMetadataDescription, FormDOM replicateFormDOM, boolean IsDebug) {
        ArrayList<HashMap<String, Object>> stateMachineCollection = replicateFormDOM.getModule().getStateMachines();
        if (stateMachineCollection != null && !stateMachineCollection.isEmpty()) {
            for (HashMap<String, Object> stateMachine : stateMachineCollection) {
                String stateMachineId = stateMachine.containsKey("uri") ? stateMachine.get("uri").toString() : "";
                if (StringUtility.isNullOrEmpty(stateMachineId)) {
                    continue;
                }
                MetadataGetterParameter metadataGetterParameter = MetadataGetterParameter.getNewInstance(stateMachineId, sourceFormMetadata.getRelativePath(), MetadataTypeEnum.StateMachine);
                metadataGetterParameter.setSourceMetadata(sourceFormMetadata, MetadataTypeEnum.Frm);
                GspMetadata stateMachineMetadata = MetadataUtility.getInstance().getMetadataWithDesign(metadataGetterParameter);
                MetadataDto stateMachineMetadataDto = new MetadataDto();
                stateMachineMetadataDto.setCode(stateMachineMetadata.getHeader().getCode().replace(sourceFormMetadata.getHeader().getCode(), targetMetadataDescription.getCode()));
                stateMachineMetadataDto.setName(stateMachineMetadata.getHeader().getName().replace(sourceFormMetadata.getHeader().getName(), targetMetadataDescription.getName()));
                stateMachineMetadataDto.setProjectName(targetMetadataDescription.getProjectName());
                stateMachineMetadataDto.setRelativePath(targetMetadataDescription.getRelativePath());


                MetadataReplicationContext stateMachineMetadataReplicationContext = new MetadataReplicationContext();
                stateMachineMetadataReplicationContext.setSourceProjectName(metadataReplicationContext.getSourceProjectName());
                stateMachineMetadataReplicationContext.setSourceMetadata(stateMachineMetadata);
                stateMachineMetadataReplicationContext.setTargetMetadataDescription(stateMachineMetadataDto);

                GspMetadata replicateStateMachineMetadata = MetadataCloneManager.cloneMetadata(stateMachineMetadataReplicationContext);
                MetadataUtility.getInstance().createMetadataIfNotExistsWithDesign(replicateStateMachineMetadata);

                MetadataUtility.getInstance().saveMetadataWithDesign(replicateStateMachineMetadata);
                //原stateMachineId
                stateMachineId = replicateStateMachineMetadata.getHeader().getId();
                // 更新表单内容statemachine
                stateMachine.put("uri", stateMachineId);

                // 更新表单关联statemachine
                // 暂不更新ID：如果更新ID，还需更新表单中使用ID的地方
                stateMachine.put("name", stateMachine.get("name").toString().replace(sourceFormMetadata.getHeader().getName(), targetMetadataDescription.getName()));
            }
        }
    }

}
