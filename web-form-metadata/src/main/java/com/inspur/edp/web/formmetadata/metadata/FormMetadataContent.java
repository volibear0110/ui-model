/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TextNode;
import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.constant.I18nExceptionConstant;

import java.util.Date;


public class FormMetadataContent extends AbstractMetadataContent {

    private String Id;

    @JsonProperty("Id")
    public final String getId() {
        return Id;
    }

    public final void setId(String value) {
        Id = value;
    }

    private JsonNode Contents;

    @JsonProperty("Contents")
    public final JsonNode getContents() {
        if (this.Contents != null && this.Contents instanceof TextNode) {
            try {
                this.Contents = SerializeUtility.getInstance().getDefaultObjectMapper().readTree(this.Contents.asText());
            } catch (Exception ex) {
                throw new WebCustomException(I18nExceptionConstant.WEB_FORM_METADATA_ERROR_0004, new String[]{this.Contents.asText()}, ex);
            }
        }
        return Contents;
    }

    public final void setContents(JsonNode value) {
        Contents = value;
    }


    private Date CreationDate;

    @JsonProperty("CreationDate")
    public final Date getCreationDate() {
        return CreationDate;
    }

    public final void setCreationDate(Date value) {
        CreationDate = value;
    }


    @Override
    public FormMetadataContent clone() {
        FormMetadataContent formMetaDataContent = new FormMetadataContent();
        formMetaDataContent.setId(this.getId());

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode newContent = mapper.treeToValue(mapper.valueToTree(this.getContents()), JsonNode.class);
            formMetaDataContent.setContents(newContent);
        } catch (JsonProcessingException e) {
            WebLogger.Instance.error(e);
            throw new WebCustomException("Form Content克隆失败, id=" + this.getId() + ", code=" + this.getCode() + ", name=" + this.getName(), e);
        }
        formMetaDataContent.setName(this.getName());
        formMetaDataContent.setCode(this.getCode());
        return formMetaDataContent;
    }

    @JsonIgnore
    public boolean isDynamic() {
        if (Contents == null || Contents.isMissingNode() || Contents.isNull()) {
            return false;
        }
        JsonNode renderMode = Contents.at("/options/renderMode");
        if (renderMode == null || renderMode.isMissingNode() || renderMode.isNull()) {
            return false;
        }
        return "dynamic".equals(renderMode.asText());
    }

    @JsonIgnore
    public boolean isCompile() {
        if (Contents == null || Contents.isMissingNode() || Contents.isNull()) {
            return true;
        }
        JsonNode renderMode = Contents.at("/options/renderMode");
        if (renderMode == null || renderMode.isMissingNode() || renderMode.isNull()) {
            return true;
        }
        return "compile".equals(renderMode.asText());
    }
}
