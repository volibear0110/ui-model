/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.serializer;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataTransferSerializer;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

public class FormMetadataTransferSerializer implements MetadataTransferSerializer {
    private static final FormMetadataTransferSerializer transferJsonSerializer = new FormMetadataTransferSerializer();

    public static FormMetadataTransferSerializer getTransferJsonSerializer() {
        return transferJsonSerializer;
    }

    /**
     * Json反序列化接口
     *
     * @param contentString
     * @return
     */
    @Override
    public final IMetadataContent deserialize(String contentString) {
        if (StringUtility.isNullOrEmpty(contentString)) {
            return null;
        }
        return SerializeUtility.getInstance().<FormMetadataContent>deserialize(contentString, FormMetadataContent.class);
    }

    /**
     * Json序列化接口
     *
     * @param metadataContent
     * @return
     */
    @Override
    public final String serialize(IMetadataContent metadataContent) {
        if (metadataContent == null) {
            return null;
        } else {
            FormMetadataContent metadata = (FormMetadataContent) ((metadataContent instanceof FormMetadataContent) ? metadataContent : null);
            return SerializeUtility.getInstance().serialize(metadata, PropertyNamingStrategy.UPPER_CAMEL_CASE,false);
        }
    }
}
