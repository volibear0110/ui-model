/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadataanalysis.form;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/14
 */
public class AnalysisExternalComponentResult {
    private FormDOM json;
    private String externalComponentPath;
    private String relativePath;
    private String externalComponentUri;
    private boolean useIsolateJs = false;

    public FormDOM getJson() {
        return json;
    }

    public void setJson(FormDOM json) {
        this.json = json;
    }

    public String getExternalComponentPath() {
        // 如果扩展表单路径以数字开头 那么增加默认的f参数值
        if (StringUtility.isStartWithNumber(this.externalComponentPath)) {
            return "f" + this.externalComponentPath;
        }
        return externalComponentPath;
    }

    public void setExternalComponentPath(String externalComponentPath) {
        this.externalComponentPath = externalComponentPath;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public String getExternalComponentUri() {
        return externalComponentUri;
    }

    public void setExternalComponentUri(String externalComponentUri) {
        this.externalComponentUri = externalComponentUri;
    }

    public boolean isUseIsolateJs() {
        return useIsolateJs;
    }

    public void setUseIsolateJs(boolean useIsolateJs) {
        this.useIsolateJs = useIsolateJs;
    }
}
