/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.HashMap;

/**
 * 组件名称生成factory
 *
 * @author noah
 */
public class ComponentNameFactory {
    private ComponentNameFactory() {
    }

    /**
     * 静态单例属性
     */
    private static volatile ComponentNameFactory _instance = null;

    private static final Object _lock = new Object();

    public static ComponentNameFactory getInstance() {
        if (_instance == null) {
            synchronized (_lock) {
                if (_instance == null) {
                    _instance = new ComponentNameFactory();
                }
            }
        }
        return _instance;
    }

    /**
     * 缓存字典 避免重复创建
     */
    private final HashMap<String, IComponentNameStrategy> componentNameStrategyDictionary = new HashMap<>();

    /**
     * 根据组件名称类型创建组件名称策略
     *
     * @param componentNameType
     * @return
     */
    public final IComponentNameStrategy creatComponentNameStrategy(String componentNameType) {
        if (StringUtility.isNullOrEmpty(componentNameType)) {
            return null;
        }

        IComponentNameStrategy componentNameStrategy = null;

        // 如果缓存字典中已包含 那么从缓存中读取该实例
        if (componentNameStrategyDictionary.containsKey(componentNameType)) {
            componentNameStrategy = componentNameStrategyDictionary.get(componentNameType);
            return componentNameStrategy;
        }

        switch (componentNameType) {
            case ComponentNameType.TEXT:
                componentNameStrategy = new TextStrategy();
                break;
            case ComponentNameType.TITLE:
                componentNameStrategy = new TitleStrategy();
                break;
            case ComponentNameType.CAPTION:
                componentNameStrategy = new CaptionStrategy();
                break;
            case ComponentNameType.PRESET_QUERY_SOLUTION_NAME:
                componentNameStrategy = new PresetQuerySolutionNameStrategy();
                break;
            case ComponentNameType.QUERY_NAME:
                componentNameStrategy = new QueryNameStrategy();
                break;
            case ComponentNameType.LABEL:
                componentNameStrategy = new LabelStrategy();
                break;
            case ComponentNameType.NAME:
                // 默认从Name中获取组件名
            default:
                componentNameStrategy = new DefaultNameStrategy();
                break;
        }

        // 默认的策略不缓存
        if (componentNameStrategy != null) {
            componentNameStrategyDictionary.put(componentNameType, componentNameStrategy);
        }

        return componentNameStrategy;
    }
}
