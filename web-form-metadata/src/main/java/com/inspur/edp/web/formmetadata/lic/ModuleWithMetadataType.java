/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.lic;

import java.util.ArrayList;
import java.util.List;

/**
 * 模块类型与元数据类型的映射关联
 */
public class ModuleWithMetadataType {
    /**
     * 模块类型
     */
    private String moduleType;
    /**
     * 关联的元数据类型列表
     */
    private List<String> metadataTypeList;

    public ModuleWithMetadataType() {
        this.metadataTypeList = new ArrayList<>(2);
    }

    public String getModuleType() {
        return moduleType;
    }

    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    public List<String> getMetadataTypeList() {
        return metadataTypeList;
    }

    public void setMetadataTypeList(List<String> metadataTypeList) {
        this.metadataTypeList = metadataTypeList;
    }
}
