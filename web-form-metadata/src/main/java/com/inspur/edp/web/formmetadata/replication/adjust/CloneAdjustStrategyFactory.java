/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.replication.adjust;

import com.inspur.edp.cdp.web.component.metadata.define.WebComponentMetadata;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.web.command.component.metadata.WebCommandsMetadata;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;

/**
 * 拷贝策略调整 factory
 *
 * @author guozhiqi
 */
public class CloneAdjustStrategyFactory {
    /**
     * 根据元数据内容获取对应的处理策略
     *
     * @param metadataContent 元数据内容content
     * @return
     */
    public static GspMetadataCloneAdjustStrategy build(IMetadataContent metadataContent) {
        if (metadataContent instanceof GspViewModel) {
            return new GspViewModelCloneAdjustStrategy();
        }
        if (metadataContent instanceof FormMetadataContent) {
            return new FormMetadataCloneAdjustStrategy();
        }

        if (metadataContent instanceof WebCommandsMetadata) {
            return new WebCommandsMetadataCloneAdjustStrategy();
        }
        if (metadataContent instanceof WebComponentMetadata) {
            return new WebComponentMetadataCloneAdjustStrategy();
        }

        return new DefaultCloneAdjustStrategy();
    }
}
