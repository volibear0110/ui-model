package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.ArrayList;
import java.util.HashMap;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;


/**
 * 基础控件策略提取策略
 */
public class MobileBasicComponentI18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    /**
     * 构造函数
     */
    public MobileBasicComponentI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        switch (this.componentType) {
            case ComponentTypes.Button:
                this.collectionButtonResourceItems();
                break;
            case ComponentTypes.ButtonGroup:
            case ComponentTypes.ToolBarArea:
                this.collectionButtonGroupResourceItems();
                break;
            case ComponentTypes.NavigationBar:
                this.collectionNavigationBarResourceItems();
                break;
            case ComponentTypes.SearchBox:
                this.collectionSearchBoxResourceItems();
                break;
            case ComponentTypes.TabBar:
                this.collectionTabbarResourceItems();
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集Button相关资源项
     */
    private void collectionButtonResourceItems() {
        this.collectSimpleAttrResourceItem("text");
    }

    /**
     * 搜集ButtonGroup相关资源项
     * 对于`type=ToolBarArea`的控件，也视为`ButtonGroup`按钮组
     */
    private void collectionButtonGroupResourceItems() {
        ArrayList<HashMap<String, Object>> items = this.componentUtility.GetItems(this.component);
        String baseItemPath = this.getComponentAttrPath("items");

        for (HashMap<String, Object> item : items) {
            String itemId = this.getComponentAttr(item, "id");
            String itemText = this.getComponentAttr(item, "text");
            String itemPath = baseItemPath + "/" + itemId + "/text";

            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemText, itemText);
            this.resourceItemCollection.add(resourceItem);
        }
    }

    /**
     * 搜集NavigationBar资源项
     */
    private void collectionNavigationBarResourceItems() {

        // 标题
        this.collectSimpleAttrResourceItem("title");

        // 工具栏
        this.collectToolbarResourceItems("toolbar");
    }

    /**
     * 搜集SearchBox相关资源项
     */
    private void collectionSearchBoxResourceItems() {
        this.collectSimpleAttrResourceItem("placeholder");
    }

    /**
     * 搜集Tabbar相关资源项
     */
    private void collectionTabbarResourceItems() {
        ArrayList<HashMap<String, Object>> items = this.componentUtility.GetItems(this.component);
        String baseItemPath = this.getComponentAttrPath("items");

        for (HashMap<String, Object> item : items) {
            String itemName = this.getComponentAttr(item, "name");
            String itemTitle = this.getComponentAttr(item, "title");
            String itemPath = baseItemPath + "/" + itemName + "/title";

            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemTitle, itemTitle);
            this.resourceItemCollection.add(resourceItem);
        }
    }

}
