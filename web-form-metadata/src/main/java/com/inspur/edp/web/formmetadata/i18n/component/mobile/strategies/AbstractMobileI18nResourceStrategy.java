package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.II18nResourceStrategy;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class AbstractMobileI18nResourceStrategy implements II18nResourceStrategy {

    /**
     * 路径前缀
     */
    protected String baseId;

    /**
     * 组件DOM
     */
    protected HashMap<String, Object> component;

    /**
     * 完整表单DOM
     */
    protected FormDOM formDOM;

    /**
     * 国际化资源集合
     */
    I18nResourceItemCollection resourceItemCollection;

    /**
     * 组件类型
     */
    protected String componentType;

    /**
     * 组件ID
     */
    protected String componentId;

    /**
     * 组件工具类
     */
    protected ComponentUtility componentUtility = ComponentUtility.getInstance();

    public AbstractMobileI18nResourceStrategy() {
    }

    /**
     * 初始化
     */
    public void init(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.baseId = baseId;
        this.component = component;
        this.formDOM = formDOM;
        this.resourceItemCollection = new I18nResourceItemCollection();
        this.componentType = this.getComponentType(component);
        this.componentId = this.getComponentId(component);
    }

    /**
     * 抽象提取方法
     */
    public abstract I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> currentComponent, FormDOM formDOM);


    /**
     * 获取组件属性值
     */
    protected String getComponentAttr(HashMap<String, Object> component, String attrName) {
        String attrVal = this.componentUtility.getValueWithKey(component, attrName);
        return attrVal;
    }

    /**
     * 获取组件ID
     */
    protected String getComponentId(HashMap<String, Object> component) {
        String id = this.componentUtility.getId(component);
        return id;
    }

    /**
     * 获取组件类型
     */
    protected String getComponentType(HashMap<String, Object> component) {
        String type = this.componentUtility.getType(component);
        return type;
    }

    /**
     * 获取组件标题
     */
    protected String getComponentTitle(HashMap<String, Object> component) {
        String title = this.componentUtility.getTitle(component);
        return title;
    }

    /**
     * 获取组件属性路径
     */
    protected String getComponentAttrPath(String attrName) {
        String path = this.componentType + "/" + this.componentId + "/" + attrName;
        return path;
    }

    /**
     * 搜集普通文本属性的资源项
     */
    protected void collectSimpleAttrResourceItem(String attrName) {
        String attrVal = this.componentUtility.getValueWithKey(this.component, attrName);
        String attrPath = this.getComponentAttrPath(attrName);

        I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, attrPath, attrVal, attrVal);
        this.resourceItemCollection.add(resourceItem);
    }

    /**
     * 搜集Toolbar相关资源项
     */
    protected void collectToolbarResourceItems(String toolbarAttrName) {
        HashMap<String, Object> toolbarComponent = this.componentUtility.GetAttributeObject(component, toolbarAttrName);
        if (toolbarComponent == null) {
            return;
        }

        ArrayList<HashMap<String, Object>> toolbarItems = this.componentUtility.GetItems(toolbarComponent);
        String toolItemBasePath = this.getComponentAttrPath(toolbarAttrName + "/items");

        for (HashMap<String, Object> toolbarItem : toolbarItems) {
            String itemId = this.getComponentAttr(toolbarItem, "id");
            String itemText = this.getComponentAttr(toolbarItem, "text");
            String itemPath = toolItemBasePath + "/" + itemId + "/text";

            I18nResourceItem resourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, itemPath, itemText, itemText);
            if (resourceItem != null) {
                this.resourceItemCollection.add(resourceItem);
            }
        }
    }

    /**
     * 搜集日程表Schedule相关资源项
     */
    protected void collecScheduleEventsResourceItems() {
        ArrayList<HashMap<String, Object>> eventsComponent = this.componentUtility.GetEventsCollection(component);
        if (eventsComponent == null) {
            return;
        }
        for (HashMap<String, Object> eventItem : eventsComponent) {
            if (!eventItem.containsKey("id") || eventItem.get("id") == null) {
                continue;
            }
            String itemTitle = this.getComponentAttr(eventItem, "title");
            String titlePath = this.getComponentAttrPath("events/" + eventItem.get("id") + "/title");
            I18nResourceItem titleResourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, titlePath, itemTitle, itemTitle);
            this.resourceItemCollection.add(titleResourceItem);

            String itemContext = this.getComponentAttr(eventItem, "content");
            String contextPath = this.getComponentAttrPath("events/" + eventItem.get("id") + "/content");
            I18nResourceItem contextResourceItem = I18nResourceItemManager.createI18nResourceItem(this.baseId, contextPath, itemContext, itemContext);
            this.resourceItemCollection.add(contextResourceItem);
        }
    }
}
