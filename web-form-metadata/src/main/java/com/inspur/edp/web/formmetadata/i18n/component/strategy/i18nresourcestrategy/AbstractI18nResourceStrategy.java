/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.II18nResourceStrategy;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.idstrategy.ComponentIdFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.idstrategy.ComponentIdType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.idstrategy.IComponentIdStrategy;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * 组件多语策略抽象类
 *
 * @author guozhiqi
 */
public abstract class AbstractI18nResourceStrategy implements II18nResourceStrategy {

    private FormDOM formDOM;

    protected FormDOM getFormDOM() {
        return formDOM;
    }

    /**
     * 执行资源项增加
     * @param resourceItemCollection 资源项列表
     * @param resourceItem 待添加的资源项
     */
    protected void addInCollection(I18nResourceItemCollection resourceItemCollection, I18nResourceItem resourceItem) {
        if (resourceItemCollection == null || resourceItem == null) {
            return;
        }
        resourceItemCollection.add(resourceItem);
    }


    /**
     * 从组件中提取多语资源
     * 不要再次继承该方法
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    public final I18nResourceItemCollection extractI18nResource(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, FormDOM formDOM) {

        this.formDOM = formDOM;

        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        I18nResourceItemCollection componentI18nResourceItemCollection = extractComponentI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (componentI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(componentI18nResourceItemCollection);
        }

        // 提取属性中多语资源
        I18nResourceItemCollection attributeI18nResourceItemCollection = extractAttributeI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (attributeI18nResourceItemCollection != null && attributeI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(attributeI18nResourceItemCollection);
        }


        return i18nResourceItemCollection;
    }


    /**
     * 获取当前参数中的type属性值
     *
     * @param currentComponent
     * @return
     */
    protected String getCurrentComponentType(HashMap<String, Object> currentComponent) {
        return ComponentUtility.getInstance().getType(currentComponent);
    }

    /**
     * 获取当前参数中的id参数值
     *
     * @param currentComponent 当前组件实例
     * @return
     */
    protected String getCurrentComponentId(HashMap<String, Object> currentComponent) {
        return ComponentUtility.getInstance().getId(currentComponent);
    }


    /**
     * 提取组件项
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItemCollection extractComponentI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        // 提取当前组件的Id和Name
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        I18nResourceItem i18nResourceItem = getComponentNameI18nResourceItem(i18nResourceItemBaseId, currentComponent);
        if (i18nResourceItem != null) {
            i18nResourceItemCollection.add(i18nResourceItem);
        }

        // Extend: 组件中存在多个字段需要提取

        return i18nResourceItemCollection;
    }

    /**
     * 提取组件name属性
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    private I18nResourceItem getComponentNameI18nResourceItem(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        // 读取组件名称
        String componentName = getComponentName(currentComponent);
        if (StringUtils.isEmpty(componentName)) {
            componentName = "";
        }

        // 读取组件ID
        String generatedComponentId = getComponentId(currentComponent);

        return I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, componentName, componentName);
    }

    /**
     * 获取当前组件的名称 组件常见的属性设置
     *
     * @param currentComponent
     * @return
     */
    protected String getComponentName(HashMap<String, Object> currentComponent) {
        // 读取组件名称
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.NAME);
        if (componentNameStrategy == null) {
            return null;
        }

        return componentNameStrategy.getComponentName(currentComponent);
    }

    protected String getComponentId(HashMap<String, Object> currentComponent) {
        IComponentIdStrategy componentIdStrategy = ComponentIdFactory.getInstance().creatComponentIdStrategy(ComponentIdType.ID);
        if (componentIdStrategy == null) {
            return null;
        }

        return componentIdStrategy.getComponentId(currentComponent);
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    protected abstract I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent);
}
