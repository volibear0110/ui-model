package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.ArrayList;
import java.util.HashMap;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.constants.ComponentTypes;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

/**
 * 容器控件策略提取策略
 */
public class MobileContainerComponentI18nResourceStrategy extends AbstractMobileI18nResourceStrategy {

    public MobileContainerComponentI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String baseId, HashMap<String, Object> component, FormDOM formDOM) {
        this.init(baseId, component, formDOM);

        String componentType = this.getComponentType(this.component);

        switch(this.componentType) {
            case ComponentTypes.Component:
                this.collectionComponentResourceItems();
                break;
            case ComponentTypes.TabPage:
                this.collectTabPageResourceItems();
                break;
            case ComponentTypes.Card:
                this.collectCardResourceItems();
                break;
            case ComponentTypes.Section:
                this.collectSectionResourceItems();
                break;
            case ComponentTypes.FieldSet:
                this.collectFieldSetResourceItems();
                break;
            default:
                return null;
        }

        return this.resourceItemCollection;
    }

    /**
     * 搜集Component相关资源项
     */
    private void collectionComponentResourceItems() {
        String componentType = this.componentUtility.getValueWithKey(this.component, "componentType");
        if (componentType.equals("Page") == false) {
            return;
        }
        this.collectSimpleAttrResourceItem("title");
    }

    /**
     * 搜集TabPage相关的资源项
     */
    private void collectTabPageResourceItems() {
        this.collectSimpleAttrResourceItem("title");
    }

    /**
     * 搜集Card相关的资源项
     */
    private void collectCardResourceItems() {

        // 标题
        this.collectSimpleAttrResourceItem("title");

        // 工具栏
        ArrayList<HashMap<String, Object>> actions =  this.componentUtility.GetActions(this.component);
        for(HashMap<String, Object> action : actions) {
            String actionId = this.getComponentAttr(action, "id");
            String actionText = this.getComponentAttr(action, "text");
            String actionPath = this.getComponentAttrPath("actions");
            actionPath = actionPath + "/" + actionId + "/text";

            I18nResourceItem resourceItem =  I18nResourceItemManager.createI18nResourceItem(this.baseId, actionPath, actionText, actionText);
            this.resourceItemCollection.add(resourceItem);
        }
    }

    /**
     * 搜集Section相关的资源项
     */
    private void collectSectionResourceItems() {
        this.collectSimpleAttrResourceItem("mainTitle");
    }

    /**
     * 搜集FieldSet相关的资源项
     */
    private void collectFieldSetResourceItems() {
        this.collectSimpleAttrResourceItem("title");
    }

}
