/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.i18nresourcestrategy;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;
import com.inspur.edp.web.formmetadata.i18n.component.I18nResourceUtility;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameFactory;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.ComponentNameType;
import com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy.IComponentNameStrategy;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;

import java.util.ArrayList;
import java.util.HashMap;

public class QuerySchemeI18nResourceStrategy extends AbstractI18nResourceStrategy {
    /**
     * 获取当前组件的名称
     *
     * @param currentComponent
     * @return
     */
    @Override
    protected String getComponentName(HashMap<String, Object> currentComponent) {
        // 读取组件名称
        IComponentNameStrategy componentNameStrategy = ComponentNameFactory.getInstance().creatComponentNameStrategy(ComponentNameType.PRESET_QUERY_SOLUTION_NAME);
        if (componentNameStrategy == null) {
            return null;
        }

        String componentName = componentNameStrategy.getComponentName(currentComponent);

        return componentName;
    }

    /**
     * 获取组件属性中多语资源项集合
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    @Override
    protected I18nResourceItemCollection extractAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        // 提取 filterText 的参数值
        String filterTextKey = "filterText";
        if (currentComponent.containsKey(filterTextKey)) {
            // 表示筛选按钮文本为常量显示
            if (currentComponent.get(filterTextKey) instanceof String) {
                String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
                String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
                String filterTextValue = ComponentUtility.getInstance().getValue(currentComponent, filterTextKey);
                String generatedComponentId = currentComponentType + "/" + currentComponentId + "/filterText";
                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, filterTextValue, "筛选按钮文本");
                this.addInCollection(i18nResourceItemCollection, i18nResourceItem);
            }
        }
        // 从fieldConfigs属性中提取多语字段

        I18nResourceItemCollection fieldConfigsI18nResourceItemCollection = extractFieldConfigsI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent);
        if (fieldConfigsI18nResourceItemCollection != null && fieldConfigsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(fieldConfigsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractFieldConfigsI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        // 从fieldConfigs属性中提取多语资源
        ArrayList<HashMap<String, Object>> childComponents = ComponentUtility.getInstance().GetFieldConfigs(currentComponent);
        I18nResourceItemCollection childComponentsI18nResourceItemCollection = extractChildComponentsI18nResourceItemCollection(i18nResourceItemBaseId, currentComponent, childComponents);
        if (childComponentsI18nResourceItemCollection != null && childComponentsI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(childComponentsI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractChildComponentsI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, ArrayList<HashMap<String, Object>> childComponentList) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        if (StringUtility.isNullOrEmpty(currentComponentType) || StringUtility.isNullOrEmpty(currentComponentId)) {
            return null;
        }

        for (HashMap<String, Object> childComponent : childComponentList) {
            String idAtributeValue = ComponentUtility.getInstance().getId(childComponent);
            String nameAtributeValue = ComponentUtility.getInstance().getName(childComponent);
            if (!StringUtility.isNullOrEmpty(idAtributeValue)) {
                String componentId = idAtributeValue;
                String generatedComponentId = currentComponentType + "/" + currentComponentId + "/" + componentId;

                I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
                this.addInCollection(i18nResourceItemCollection, i18nResourceItem);

                //提取placeholder属性参数
                String placeHolderName = ComponentUtility.getInstance().GetPlaceHolderName(childComponent);
                String placeHolderValue = ComponentUtility.getInstance().getPlaceHolder(childComponent);
                String generatedPlaceHolderComponentId = currentComponentType + "/" + currentComponentId + "/" + componentId + "/" + placeHolderName;
                I18nResourceItem i18nPlaceholderResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedPlaceHolderComponentId, placeHolderValue, placeHolderValue);
                this.addInCollection(i18nResourceItemCollection, i18nPlaceholderResourceItem);

                // 提取开始 placeHolder
                String beginPlaceHolderName = ComponentUtility.getInstance().getBeginPlaceHolderName(childComponent);
                String beginplaceHolderValue = ComponentUtility.getInstance().getBeginPlaceHolder(childComponent);
                if (!StringUtility.isNullOrEmpty(beginplaceHolderValue)) {
                    String generatedBeginPlaceHolderComponentId = currentComponentType + "/" + currentComponentId + "/" + componentId + "/" + beginPlaceHolderName;
                    I18nResourceItem i18nBeginPlaceholderResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedBeginPlaceHolderComponentId, beginplaceHolderValue, beginplaceHolderValue);
                    this.addInCollection(i18nResourceItemCollection, i18nBeginPlaceholderResourceItem);
                }

                // 提取结束 endPlaceHolder
                String endPlaceHolderName = ComponentUtility.getInstance().getEndPlaceHolderName(childComponent);
                String endPlaceHolderValue = ComponentUtility.getInstance().getEndPlaceHolder(childComponent);
                if (!StringUtility.isNullOrEmpty(endPlaceHolderValue)) {
                    String generatedEndPlaceHolderComponentId = currentComponentType + "/" + currentComponentId + "/" + componentId + "/" + endPlaceHolderName;
                    I18nResourceItem i18nEndPlaceholderResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedEndPlaceHolderComponentId, endPlaceHolderValue, endPlaceHolderValue);
                    this.addInCollection(i18nResourceItemCollection, i18nEndPlaceholderResourceItem);
                }


                // 子组件的属性中包含多语资源项
                I18nResourceItemCollection childComponentAttributeI18nResourceItemCollection = extractChildComponentAttributeI18nResourceItemCollection(i18nResourceItemBaseId, generatedComponentId, childComponent);
                if (childComponentAttributeI18nResourceItemCollection != null && childComponentAttributeI18nResourceItemCollection.size() > 0) {
                    i18nResourceItemCollection.addRange(childComponentAttributeI18nResourceItemCollection);
                }
            }
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractChildComponentAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, String componentI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        //从control属性获取对象，然后在该对象的enumValues中获取资源项
        I18nResourceItemCollection controlAttributeI18nResourceItemCollection = extractControlAttributeI18nResourceItemCollection(i18nResourceItemBaseId, componentI18nResourceItemBaseId, currentComponent);
        if (controlAttributeI18nResourceItemCollection != null && controlAttributeI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(controlAttributeI18nResourceItemCollection);
        }

        return i18nResourceItemCollection;
    }

    private I18nResourceItemCollection extractControlAttributeI18nResourceItemCollection(String i18nResourceItemBaseId, String componentI18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();
        HashMap<String, Object> controlAttributeObject = ComponentUtility.getInstance().GetControlAttributeObject(currentComponent);

        String controlAttributeName = ComponentUtility.getInstance().GetControlAttributeName(currentComponent);
        componentI18nResourceItemBaseId += I18nResourceConstant.SECOND_LEVEL_DELIMITER + controlAttributeName;

        String strValueFieldKey = ComponentUtility.getInstance().getValueWithKey(controlAttributeObject, I18nResourceConstant.ValueFieldKey);
        String strTextFieldKey = ComponentUtility.getInstance().getValueWithKey(controlAttributeObject, I18nResourceConstant.TextFieldKey);

        // 从enumValues中提取多语资源项
        ArrayList<HashMap<String, Object>> enumValueCollection = ComponentUtility.getInstance().GetEnumValues(controlAttributeObject);
        String enumValuesAttributeName = ComponentUtility.getInstance().GetEnumValuesName(controlAttributeObject);

        I18nResourceItemCollection enumValuesI18nResourceItemCollection = I18nResourceUtility.GetInstance().ExtractEnumDataI18nResourceItemCollection(i18nResourceItemBaseId, componentI18nResourceItemBaseId, enumValueCollection, enumValuesAttributeName, strValueFieldKey, strTextFieldKey);
        if (enumValuesI18nResourceItemCollection != null && enumValuesI18nResourceItemCollection.size() > 0) {
            i18nResourceItemCollection.addRange(enumValuesI18nResourceItemCollection);
        }

        // 如果是帮助 提取对应的帮助标题
        String controlType = ComponentUtility.getInstance().getValueWithKey(controlAttributeObject, "controltype");
        if (!StringUtility.isNullOrEmpty(controlType) && controlType.equals("help")) {
            String dialogTitleValue = ComponentUtility.getInstance().getValueWithKey(controlAttributeObject, "dialogTitle");
            String generateDialogTitleComponentId = componentI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + controlType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "dialogTitle";
            I18nResourceItem i18nDialogTitleResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generateDialogTitleComponentId, dialogTitleValue, dialogTitleValue);
            this.addInCollection(i18nResourceItemCollection, i18nDialogTitleResourceItem);
        }

        // 提取input-group的  groupText属性值
        if (!StringUtility.isNullOrEmpty(controlType) && controlType.equals("input-group")) {
            String groupTextValue = ComponentUtility.getInstance().getValueWithKey(controlAttributeObject, "groupText");
            String generateGroupTextComponentId = componentI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + controlType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + "groupText";
            I18nResourceItem i18nGroupTextResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generateGroupTextComponentId, groupTextValue, groupTextValue);
            this.addInCollection(i18nResourceItemCollection, i18nGroupTextResourceItem);
        }

        return i18nResourceItemCollection;
    }
}
