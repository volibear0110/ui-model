/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.i18n.component.mobile.MobileI18nResourceStrategyFactory;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import java.util.HashMap;

/**
 * 组件多语策略上下文
 */
public class I18nResourceStrategyContext {
    /**
     * 组件策略
     */
    private final II18nResourceStrategy i18nResourceStrategy;

    /**
     * 组件
     */
    private final HashMap<String, Object> component;

    /**
     * 多语资源项基础ID
     */
    private final String i18nResourceItemBaseId;

    private final FormDOM formDOM;

    public I18nResourceStrategyContext(HashMap<String, Object> currentComponent, String currentI18nResourceItemBaseId, FormDOM formDOM) {
        this.component = currentComponent;
        this.i18nResourceItemBaseId = currentI18nResourceItemBaseId;
        this.formDOM = formDOM;

        // 根据节点的类型，选用策略
        if(formDOM.getFormType().equals("MobileForm")) {
            i18nResourceStrategy = MobileI18nResourceStrategyFactory.getInstance().getStrategy(currentComponent, currentI18nResourceItemBaseId, this.formDOM);
        } else {
            i18nResourceStrategy = I18nResourceStrategyFactory.getInstance().getI18nResourceStrategy(currentComponent, currentI18nResourceItemBaseId);
        }
    }

    /**
     * 获取多语资源
     */
    public final I18nResourceItemCollection ExtractI18nResource() {
        if (i18nResourceStrategy == null) {
            return null;
        }

        return i18nResourceStrategy.extractI18nResource(i18nResourceItemBaseId, component, this.formDOM);
    }
}
