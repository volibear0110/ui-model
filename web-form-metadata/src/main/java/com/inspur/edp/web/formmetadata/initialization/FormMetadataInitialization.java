/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.initialization;

import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/12
 */
public class FormMetadataInitialization implements MetadataContentManager {

    @Override
    public void build(GspMetadata metadata) {
        FormMetadataContent webDevMetadata = new FormMetadataContent();
        if (metadata != null && metadata.getHeader() != null && !StringUtility.isNullOrEmpty(metadata.getHeader().getId())) {
            webDevMetadata.setId(metadata.getHeader().getId());
            metadata.setContent(webDevMetadata);
        }
    }
}
