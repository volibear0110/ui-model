/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.formdom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/03/18
 */

@Data
public class FormOptions {
    private boolean enableTextArea;
    private boolean enableDragAndDropToModifyLayout;
    /**
     * 渲染方式
     * compile  dynamic
     */
    private String renderMode = "compile";

    /**
     * 变更集策略
     * entire
     * valid
     */
    private String changeSetPolicy;

    /**
     * 启用服务器端变更检测：菜单或应用关闭前调用后端接口确认后端缓存中的数据是否已经保存并提示用户
     */
    private boolean enableServerSideChangeDetection;

    /**
     * 生成表单代码时将之前的源码都删除
     */
    private boolean enableDeleteSourceCode;

    /**
     * 表单是否启用业务流
     */
    private boolean enableAif;

    /**
     * 数据转换类型
     */
    private boolean paramTypeTransform;

    /**
     * 表单规则推送模式，低代码新建的表单默认为pushToVO
     */
    private String formRulePushMode;

    /**
     * 表单组合模式
     */
    private String combineFormMode = "custom";

    /**
     * 标识是否解析表单  根据渲染模式来进行确定
     *
     * @return
     */
    @JsonIgnore
    public boolean isEnableFormJieXi() {
        return "dynamic".equals(this.renderMode);
    }

}
