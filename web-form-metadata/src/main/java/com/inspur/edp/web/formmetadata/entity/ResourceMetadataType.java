/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.entity;

/**
 * 资源元数据类型
 *
 * @author guozhiqi
 */
public enum ResourceMetadataType {
    /**
     * 简体中文
     */
    ZH_CH {
        @Override
        public String getSuffix() {
            return ".res";
        }

        @Override
        public String getSuffixWithoutResExtension() {
            return "";
        }
    },
    /**
     * 英文
     */
    EN {
        @Override
        public String getSuffix() {
            return ".en.lres";
        }

        @Override
        public String getSuffixWithoutResExtension() {
            return ".en";
        }
    },
    /**
     * 繁体中文
     */
    ZH_CHT {
        @Override
        public String getSuffix() {
            return ".zh-CHT.lres";
        }

        @Override
        public String getSuffixWithoutResExtension() {
            return ".zh-CHT";
        }
    };

    /**
     * 获取对应的元数据文件名称后缀
     *
     * @return
     */
    public abstract String getSuffix();

    /**
     * 获取不包含res后缀的名称
     *
     * @return
     */
    public abstract String getSuffixWithoutResExtension();

    /**
     * 构造对应的资源元数据的实际文件名称
     *
     * @param fileNameWithoutExtension
     * @return
     */
    public String getFileName(String fileNameWithoutExtension) {
        return fileNameWithoutExtension + this.getSuffix();
    }


}
