/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.entity;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

/**
 * 表单关联viewModel
 * @author guozhiqi
 */
public class FormRelateViewModelEntity {
    /**
     * 关联的viewModel元数据
     */
    private GspMetadata viewModelMetadata;

    /**
     * 关联表单元数据
     */
    private  GspMetadata formMetadata;

    /**
     * viewModel元数据id
     */
    private String viewModelMetadataId;

    /**
     * 表单元数据id
     */
    private  String formMetadataId;

    /**
     * 表单code
     */
    private  String formCode;

    /**
     * 表单name
     */
    private  String formName;

    public GspMetadata getViewModelMetadata() {
        return viewModelMetadata;
    }

    public void setViewModelMetadata(GspMetadata viewModelMetadata) {
        this.viewModelMetadata = viewModelMetadata;
    }

    public GspMetadata getFormMetadata() {
        return formMetadata;
    }

    public void setFormMetadata(GspMetadata formMetadata) {
        this.formMetadata = formMetadata;
    }

    public String getViewModelMetadataId() {
        return viewModelMetadataId;
    }

    public void setViewModelMetadataId(String viewModelMetadataId) {
        this.viewModelMetadataId = viewModelMetadataId;
    }

    public String getFormMetadataId() {
        return formMetadataId;
    }

    public void setFormMetadataId(String formMetadataId) {
        this.formMetadataId = formMetadataId;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }
}
