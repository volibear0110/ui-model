/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component.strategy.namestrategy;

import com.inspur.edp.web.formmetadata.i18n.component.ComponentUtility;

import java.util.HashMap;

/**
 * 预置筛选方案组件Name策略
 */
public class PresetQuerySolutionNameStrategy extends AbstractComponentNameStrategy {
    @Override
    public String getComponentName(HashMap<String, Object> component) {
        return ComponentUtility.getInstance().getPresetQuerySolutionName(component);
    }
}
