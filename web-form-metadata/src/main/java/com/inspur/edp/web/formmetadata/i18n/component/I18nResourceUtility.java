/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.i18n.component;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.i18n.I18nResourceItemManager;
import com.inspur.edp.web.formmetadata.i18n.constant.I18nResourceConstant;


import java.util.HashMap;
import java.util.List;

public class I18nResourceUtility {

    private I18nResourceUtility() {

    }

    private static final I18nResourceUtility i18nResourceUtilityInstance = new I18nResourceUtility();

    public static I18nResourceUtility GetInstance() {
        return i18nResourceUtilityInstance;
    }


    /**
     * 提取枚举属性中多语资源项
     *
     * @return
     */
    public final I18nResourceItemCollection ExtractEnumDataI18nResourceItemCollection(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, List<HashMap<String, Object>> componentCollection, String enumAttributeName, String valueField, String textField) {
        I18nResourceItemCollection enumValuesI18nResourceItemCollection = new I18nResourceItemCollection();

        enumI18nResourceItemBaseId += I18nResourceConstant.SECOND_LEVEL_DELIMITER;
        for (HashMap<String, Object> component : componentCollection) {
            I18nResourceItem i18nResourceItem = GetEnumItemI18nResourceItem(i18nResourceItemBaseId, enumI18nResourceItemBaseId + enumAttributeName, component, valueField, textField);
            if (i18nResourceItem != null) {
                enumValuesI18nResourceItemCollection.add(i18nResourceItem);
            }
        }

        return enumValuesI18nResourceItemCollection;
    }

    /**
     * 获取枚举项中多语资源项
     *
     * @param enumI18nResourceItemBaseId
     * @param enumItemObject
     * @return
     */

    private I18nResourceItem GetEnumItemI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, java.util.HashMap<String, Object> enumItemObject, String valueField) {
        return GetEnumItemI18nResourceItem(i18nResourceItemBaseId, enumI18nResourceItemBaseId, enumItemObject, valueField, "textField");
    }

    private I18nResourceItem GetEnumItemI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, java.util.HashMap<String, Object> enumItemObject) {
        return GetEnumItemI18nResourceItem(i18nResourceItemBaseId, enumI18nResourceItemBaseId, enumItemObject, "valueField", "textField");
    }

    //C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
//ORIGINAL LINE: private I18nResourceItem GetEnumItemI18nResourceItem(string i18nResourceItemBaseId, string enumI18nResourceItemBaseId, Dictionary<string, object> enumItemObject, string valueField = "valueField", string textField = "textField")
    private I18nResourceItem GetEnumItemI18nResourceItem(String i18nResourceItemBaseId, String enumI18nResourceItemBaseId, HashMap<String, Object> enumItemObject, String valueField, String textField) {
        I18nResourceItem i18nResourceItem = null;

        // 属性为value的值
        String valueAtributeValue = ComponentUtility.getInstance().getValueWithKey(enumItemObject, valueField, "value");
        String nameAtributeValue = ComponentUtility.getInstance().getValueWithKey(enumItemObject, textField, "name");
        if (!StringUtility.isNullOrEmpty(valueAtributeValue)) {
            String componentId = valueAtributeValue;

            String generatedComponentId = enumI18nResourceItemBaseId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

            i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, nameAtributeValue, nameAtributeValue);
        }

        return i18nResourceItem;
    }


    /**
     * 提取placeHolder属性中多语资源项
     *
     * @param i18nResourceItemBaseId
     * @param currentComponent
     * @return
     */
    public final I18nResourceItemCollection ExtractPlaceHolderI18nResourceItemCollection(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent) {
        I18nResourceItemCollection i18nResourceItemCollection = new I18nResourceItemCollection();

        String placeHolderValue = ComponentUtility.getInstance().getPlaceHolder(currentComponent);
        String placeHolderName = ComponentUtility.getInstance().GetPlaceHolderName(currentComponent);

        String componentId = placeHolderName;
        String currentComponentType = ComponentUtility.getInstance().getType(currentComponent);
        // 数字类型的控件
        if (!StringUtility.isNullOrEmpty(currentComponentType) &&
                "NumericBox".equals(currentComponentType)) {
            String strControlSource = ComponentUtility.getInstance().GetControlSource(currentComponent);
            if (!StringUtility.isNullOrEmpty(strControlSource) &&
                    "Farris".equals(strControlSource)) {
                currentComponentType = "NumberSpinner";
            }
        }


        String currentComponentId = ComponentUtility.getInstance().getId(currentComponent);
        String generatedComponentId = currentComponentType + I18nResourceConstant.SECOND_LEVEL_DELIMITER + currentComponentId + I18nResourceConstant.SECOND_LEVEL_DELIMITER + componentId;

        I18nResourceItem i18nResourceItem = I18nResourceItemManager.createI18nResourceItem(i18nResourceItemBaseId, generatedComponentId, placeHolderValue, placeHolderValue);
        if (i18nResourceItem != null) {
            i18nResourceItemCollection.add(i18nResourceItem);
        }

        return i18nResourceItemCollection;
    }

}
