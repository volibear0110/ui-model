package com.inspur.edp.web.formmetadata.i18n.component.mobile.strategies;

import java.util.HashMap;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;


/**
 * 容器控件策略提取策略
 */
public class MobileDefaultI18nResourceStrategy  extends AbstractMobileI18nResourceStrategy {

    /**
     * 构造函数
     */
    public MobileDefaultI18nResourceStrategy() {
        super();
    }

    /**
     * 提取方法
     */
    @Override
    public I18nResourceItemCollection extractI18nResource(String i18nResourceItemBaseId, HashMap<String, Object> currentComponent, FormDOM formDOM) {
        return null;
    }
}
