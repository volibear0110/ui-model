/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.metadata.i18nsetting;

/**
 * 表单DOM（表单代码）
 */
public class I18nSetting {
    /**
     * 是否启用国际化
     */
    private boolean enableI18n;

    public final boolean getenableI18n() {
        return enableI18n;
    }

    public final void setenableI18n(boolean value) {
        enableI18n = value;
    }

    /**
     * 国家或地区
     */
    private String region;

    public final String getregion() {
        return region;
    }

    public final void setregion(String value) {
        region = value;
    }

    /**
     * 语言
     */
    private String language;

    public final String getlanguage() {
        return language;
    }

    public final void setlanguage(String value) {
        language = value;
    }
}
