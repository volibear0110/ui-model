/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.components;

import java.util.HashMap;

/**
 * card  模板 编辑器构造
 */
public class CardTemplateEditorGenerator {

    public static HashMap<String, Object> generateWithEditorType(String editorType) {
        HashMap<String, Object> editorHashMap = new HashMap<>();
        switch (editorType) {
            case "ContentContainer":
                editorHashMap.put("id", "container");
                editorHashMap.put("type", "ContentContainer");
                break;
            case "ButtonGroup":
                editorHashMap.put("id", "ButtonGroup");
                editorHashMap.put("type", "ButtonGroup");
                editorHashMap.put("appearance", "{class:\"btn-toolbar my-1 mx-2\"}");
                break;
            case "Button":
                editorHashMap.put("id", "button");
                editorHashMap.put("type", "Button");
                editorHashMap.put("text", "");
                editorHashMap.put("seperate", true);
                editorHashMap.put("textAligment", "center");
                editorHashMap.put("visible", true);
                editorHashMap.put("dataSourceType", "static");
                break;
            case "TextBox":
                editorHashMap.put("id", "textbox1");
                editorHashMap.put("type", "TextBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "文本");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("isPassword", false);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("enableTips", true);
                break;
            case "MultiTextBox":
                editorHashMap.put("id", "multiTextBox1");
                editorHashMap.put("type", "MultiTextBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "多行文本框");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("enableTips", true);
                editorHashMap.put("autoHeight", false);
                break;
            case "NumericBox":
                editorHashMap.put("id", "numericBox1");
                editorHashMap.put("type", "NumericBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "数值框");
                editorHashMap.put("controlSource", "Farris");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("textAlign", "left");
                editorHashMap.put("precisionSourceType", "static");
                editorHashMap.put("step", 1);
                editorHashMap.put("useThousands", true);
                editorHashMap.put("bigNumber", false);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", false);
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("showZero", true);
                editorHashMap.put("showButton", true);

                break;
            case "DateBox":
                editorHashMap.put("id", "date");
                editorHashMap.put("type", "DateBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "日期选择");
                editorHashMap.put("controlSource", "Farris");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("editable", true);
                editorHashMap.put("dateRange", false);
                editorHashMap.put("showTime", false);
                editorHashMap.put("showType", 1);
                editorHashMap.put("dateFormat", "yyyy-MM-dd");
                editorHashMap.put("returnFormat", "yyyy-MM-dd");
                editorHashMap.put("showWeekNumbers", false);
                editorHashMap.put("dateRangeDatesDelimiter", "~");
                editorHashMap.put("useDefault", false);
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("textAlign", "left");
                editorHashMap.put("precisionSourceType", "static");
                editorHashMap.put("step", 1);
                editorHashMap.put("useThousands", true);
                editorHashMap.put("bigNumber", false);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", false);
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("localization", false);
                break;
            case "CheckBox":
                editorHashMap.put("id", "checkBox1");
                editorHashMap.put("type", "CheckBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "复选框");
                editorHashMap.put("checked", false);
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("hasDefaultFocus", false);
                break;
            case "EnumField":
                editorHashMap.put("id", "");
                editorHashMap.put("type", "EnumField");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "下拉框");
                editorHashMap.put("controlSource", "Farris");
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("idField", "");
                editorHashMap.put("textField", "");
                editorHashMap.put("multiSelect", false);
                editorHashMap.put("uri", "");
                editorHashMap.put("autoWidth", true);
                editorHashMap.put("enableClear", true);
                editorHashMap.put("enableCancelSelected", false);
                editorHashMap.put("dataSourceType", "static");
                break;
            case "RadioGroup":
                editorHashMap.put("id", "radioGroup1");
                editorHashMap.put("type", "RadioGroup");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "单选组");
                editorHashMap.put("isHorizontal", true);
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("dataSourceType", "static");
                editorHashMap.put("textField", "name");
                editorHashMap.put("valueField", "value");

                break;
            case "Tab":
                editorHashMap.put("id", "tab1");
                editorHashMap.put("type", "Tab");
                editorHashMap.put("controlSource", "Farris");
                editorHashMap.put("position", "top");
                editorHashMap.put("contentFill", false);
                editorHashMap.put("autoTitleWidth", false);
                editorHashMap.put("titleWidth", 0);
                break;
            case "TabPage":
                editorHashMap.put("id", "tabPage1");
                editorHashMap.put("type", "TabPage");
                editorHashMap.put("controlSource", "Farris");
                editorHashMap.put("title", "标签页");
                editorHashMap.put("removeable", false);
                editorHashMap.put("toolbar", "");
                editorHashMap.put("visible", true);
                break;
            case "TabToolbarItem":
                editorHashMap.put("id", "tabToolbarItem");
                editorHashMap.put("type", "TabToolbarItem");
                editorHashMap.put("title", "工具栏按钮");
                editorHashMap.put("disable", false);
                editorHashMap.put("visible", true);
                break;
            case "Section":
                editorHashMap.put("id", "section1");
                editorHashMap.put("type", "Section");
                editorHashMap.put("visible", true);
                editorHashMap.put("mainTitle", "");
                editorHashMap.put("subTitle", "");
                editorHashMap.put("headerClass", "");
                editorHashMap.put("titleClass", "");
                editorHashMap.put("extendedHeaderAreaClass", "");
                editorHashMap.put("toolbarClass", "");
                editorHashMap.put("extendedAreaClass", "");
                editorHashMap.put("contentTemplateClass", "");
                editorHashMap.put("fill", false);
                editorHashMap.put("expanded", true);
                editorHashMap.put("accordionMode", "default");
                editorHashMap.put("showHeader", false);
                editorHashMap.put("headerTemplate", "");
                editorHashMap.put("titleTemplate", "");
                editorHashMap.put("extendedHeaderAreaTemplate", "");
                editorHashMap.put("toolbarTemplate", "");
                editorHashMap.put("extendedAreaTemplate", "");
                editorHashMap.put("isScrollSpyItem", false);

                break;
            case "ToolBarItem":
                editorHashMap.put("id", "");
                editorHashMap.put("type", "ToolBarItem");
                editorHashMap.put("disable", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("text", "");
                break;
            case "LanguageTextBox":
                editorHashMap.put("id", "languageTextBox1");
                editorHashMap.put("type", "LanguageTextBox");
                editorHashMap.put("titleSourceType", "static");
                editorHashMap.put("title", "多语控件");
                editorHashMap.put("readonly", false);
                editorHashMap.put("require", false);
                editorHashMap.put("disable", false);
                editorHashMap.put("enableClear", true);
                editorHashMap.put("placeHolder", "");
                editorHashMap.put("holdPlace", false);
                editorHashMap.put("linkedLabelEnabled", false);
                editorHashMap.put("visible", true);
                editorHashMap.put("path", "");
                editorHashMap.put("isTextArea", true);
                editorHashMap.put("hasDefaultFocus", false);
                editorHashMap.put("useFrameworkLanguage", true);
                editorHashMap.put("editable", false);
                editorHashMap.put("enableAppend", false);
                editorHashMap.put("inputAppendType", "button");
                editorHashMap.put("inputAppendDisabled", false);

                break;
            default:
                throw new RuntimeException("未知的控件类型");
        }
        return editorHashMap;
    }
}
