/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.components;

import com.inspur.edp.web.common.serialize.SerializeUtility;

import java.util.HashMap;

public class GridFieldComponentMetadata {
    public static HashMap<String, Object> getDefaultGridFieldMetadata() {
        return SerializeUtility.getInstance().deserialize("{\n" +
                "                    \"id\": \"col\",\n" +
                "                    \"type\": \"GridField\",\n" +
                "                    \"controlSource\": \"Farris\",\n" +
                "                    \"caption\": \"\",\n" +
                "                    \"captionTemplate\": null,\n" +
                "                    \"dataField\": \"\",\n" +
                "                    \"dataType\": \"\",\n" +
                "                    \"binding\": null,\n" +
                "                    \"enumData\": null,\n" +
                "                    \"appearance\": null,\n" +
                "                    \"size\": {\n" +
                "                        \"width\": 120\n" +
                "                    },\n" +
                "                    \"displayTemplate\": null,\n" +
                "                    \"editor\": null,\n" +
                "                    \"draggable\": false,\n" +
                "                    \"frozen\": \"none\",\n" +
                "                    \"sortable\": true,\n" +
                "                    \"sortOrder\": null,\n" +
                "                    \"resizeable\": true,\n" +
                "                    \"aggregate\": { \"type\": \"none\", \"formatter\": { \"type\": \"none\" } },\n" +
                "                    \"groupAggregate\": { \"type\": \"none\", \"formatter\": { \"type\": \"none\" } },\n" +
                "                    \"styler\": \"\",\n" +
                "                    \"colTemplate\": \"\",\n" +
                "                    \"linkedLabelEnabled\": false,\n" +
                "                    \"linkedLabelClick\": null,\n" +
                "                    \"textAlign\": \"left\",\n" +
                "                    \"hAlign\": \"left\",\n" +
                "                    \"vAlign\": \"middle\",\n" +
                "                    \"formatter\": { \"type\": \"none\" },\n" +
                "                    \"showTips\": false,\n" +
                "                    \"tipContent\": null,\n" +
                "                    \"multiLanguage\": false,\n" +
                "                    \"enableFilter\": false,\n" +
                "                    \"headerStyler\": \"\",\n" +
                "                    \"localization\": false,\n" +
                "                    \"idField\": \"value\",\n" +
                "                    \"textField\": \"name\"\n" +
                "                }", HashMap.class);
    }
}
