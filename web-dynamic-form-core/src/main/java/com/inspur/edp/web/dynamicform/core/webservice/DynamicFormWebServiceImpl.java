/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.webservice;

import com.inspur.edp.web.common.entity.ResultCode;
import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.dynamicform.core.api.entity.DynamicCreateFormMetadataParameter;
import com.inspur.edp.web.dynamicform.core.api.entity.DynamicFormCreateEntity;
import com.inspur.edp.web.dynamicform.core.api.entity.FormMetadataInfo;
import com.inspur.edp.web.dynamicform.core.api.entity.FormMetadataTemplateInfo;
import com.inspur.edp.web.dynamicform.core.api.service.DynamicCreateFormMetadataService;
import com.inspur.edp.web.dynamicform.core.api.webservice.DynamicFormWebService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 动态创建表单
 *
 * @author guozhiqi
 */
public class DynamicFormWebServiceImpl implements DynamicFormWebService {
    @Override
    public ResultMessage<String> create(DynamicFormCreateEntity createEntity) {
        ResultMessage resultMessage = ResultCode.success();


        DynamicCreateFormMetadataService dynamicCreateFormMetadataService = SpringBeanUtils.getBean(DynamicCreateFormMetadataService.class);
        DynamicCreateFormMetadataParameter parameter = new DynamicCreateFormMetadataParameter();

        parameter.setBizobjectID(createEntity.getBizObjectId());

        parameter.setProjectCode("testaddproj");
        parameter.setProjectPath("testadd/testaddmodule/testaddproj/bo-testaddprojfront/metadata");
        parameter.setProjectNameSpace("Inspur.GS.testadd.testaddmodule.testaddproj");

        FormMetadataInfo formMetadataInfo = new FormMetadataInfo();
        formMetadataInfo.setCode("testaddcreate9");
        formMetadataInfo.setName("testaddcreate9");

        formMetadataInfo.setEapiMetadataId(createEntity.getEapiMetadataId());
        // 设置eapi暴露的uri地址
        formMetadataInfo.setEapiSourceUri(createEntity.getEapiSourceUri());
        formMetadataInfo.setVoMetadataId(createEntity.getVoMetadataId());

        FormMetadataTemplateInfo formMetadataTemplateInfo = new FormMetadataTemplateInfo();
        // 列表使用的模板 new-list-template
        // 卡片使用的模板 new-card-template
        formMetadataTemplateInfo.setTemplateCode("list-template");
        formMetadataTemplateInfo.setTemplateName("list-template");
        formMetadataTemplateInfo.setTemplateId("list-template");
        formMetadataInfo.setTemplateInfo(formMetadataTemplateInfo);
        parameter.getFormMetadataInfoList().add(formMetadataInfo);

        FormMetadataInfo formMetadataInfoCard = new FormMetadataInfo();
        formMetadataInfoCard.setCode("testaddcreate9_card");
        formMetadataInfoCard.setName("testaddcreate9_card");

        formMetadataInfoCard.setEapiMetadataId(createEntity.getEapiMetadataId());
        // 设置eapi暴露的uri地址
        formMetadataInfoCard.setEapiSourceUri(createEntity.getEapiSourceUri());
        formMetadataInfoCard.setVoMetadataId(createEntity.getVoMetadataId());

        FormMetadataTemplateInfo formMetadataTemplateInfoCard = new FormMetadataTemplateInfo();
        // 列表使用的模板 new-list-template
        // 卡片使用的模板 new-card-template
        formMetadataTemplateInfoCard.setTemplateCode("card-template");
        formMetadataTemplateInfoCard.setTemplateName("card-template");
        formMetadataTemplateInfoCard.setTemplateId("card-template");
        formMetadataInfoCard.setTemplateInfo(formMetadataTemplateInfoCard);
        parameter.getFormMetadataInfoList().add(formMetadataInfoCard);


        dynamicCreateFormMetadataService.create(parameter);
        return resultMessage;
    }
}
