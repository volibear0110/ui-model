/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.statemachine;

/**
 * 获取列表模板对应的状态机定义
 * @author guozhiqi
 */
public class ListStateMachine {
    public static  String getContent(){
        return "{\n" +
                "  \"renderState\": {\n" +
                "    \"canAdd\": {\n" +
                "      \"condition\": [\n" +
                "        {\n" +
                "          \"lBracket\": \"\",\n" +
                "          \"source\": \"state\",\n" +
                "          \"compare\": \"===\",\n" +
                "          \"target\": \"'init'\",\n" +
                "          \"relation\": \"\",\n" +
                "          \"rBracket\": \"\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"name\": \"新增\",\n" +
                "      \"description\": \"\"\n" +
                "    },\n" +
                "    \"canEdit\": {\n" +
                "      \"condition\": [\n" +
                "        {\n" +
                "          \"lBracket\": \"\",\n" +
                "          \"source\": \"state\",\n" +
                "          \"compare\": \"===\",\n" +
                "          \"target\": \"'init'\",\n" +
                "          \"relation\": \"\",\n" +
                "          \"rBracket\": \"\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"name\": \"编辑\",\n" +
                "      \"description\": \"\"\n" +
                "    },\n" +
                "    \"canRemove\": {\n" +
                "      \"condition\": [\n" +
                "        {\n" +
                "          \"lBracket\": \"\",\n" +
                "          \"source\": \"state\",\n" +
                "          \"compare\": \"===\",\n" +
                "          \"target\": \"'init'\",\n" +
                "          \"relation\": \"\",\n" +
                "          \"rBracket\": \"\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"name\": \"删除\",\n" +
                "      \"description\": \"\"\n" +
                "    },\n" +
                "    \"canView\": {\n" +
                "      \"condition\": [\n" +
                "        {\n" +
                "          \"lBracket\": \"\",\n" +
                "          \"source\": \"state\",\n" +
                "          \"compare\": \"===\",\n" +
                "          \"target\": \"'init'\",\n" +
                "          \"relation\": \"\",\n" +
                "          \"rBracket\": \"\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"name\": \"查看\",\n" +
                "      \"description\": \"是否允许查看\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"state\": [{\n" +
                "    \"state\": \"init\",\n" +
                "    \"name\": \"初始\",\n" +
                "    \"description\": \"处于初始状态\"\n" +
                "  }\n" +
                "  ],\n" +
                "  \"initialState\": \"init\",\n" +
                "  \"action\": {\n" +
                "  }\n" +
                "}";
    }
}
