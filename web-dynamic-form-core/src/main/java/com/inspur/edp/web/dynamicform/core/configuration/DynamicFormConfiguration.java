/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.configuration;

import com.inspur.edp.web.dynamicform.core.api.service.DynamicCreateFormMetadataService;
import com.inspur.edp.web.dynamicform.core.service.DynamicCreateFormMetadataServiceImpl;
import com.inspur.edp.web.dynamicform.core.webservice.DynamicFormWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 动态创建表单web 配置
 *
 * @author guozhiqi
 */
@Configuration
public class DynamicFormConfiguration {
    @Bean()
    public RESTEndpoint dynamicFormWebServiceEndpoint() {
        return new RESTEndpoint("/dev/main/v1.0/dynamicform", new DynamicFormWebServiceImpl());
    }

    @Bean
    public DynamicCreateFormMetadataService getDynamicCreateFormMetadataService() {
        return new DynamicCreateFormMetadataServiceImpl();
    }
}
