/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.statemachine;

/**
 * 卡片对应状态机
 * @author guozhiqi
 */
public class CardStateMachine {
    public  static  String getContent(){
        return "{\n" +
                "    \"renderState\": {\n" +
                "        \"canAdd\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'init'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"新增\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canEdit\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'init'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"编辑\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canCancel\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'add'\",\n" +
                "                \"relation\": \"or\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }, {\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'edit'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"取消\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canSave\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'add'\",\n" +
                "                \"relation\": \"or\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }, {\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'edit'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"保存\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canRemove\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'init'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"删除\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canAddDetail\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'add'\",\n" +
                "                \"relation\": \"or\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }, {\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'edit'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"新增明细\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"canRemoveDetail\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'add'\",\n" +
                "                \"relation\": \"or\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }, {\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'edit'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"删除明细\",\n" +
                "            \"description\": \"\"\n" +
                "        },\n" +
                "        \"editable\": {\n" +
                "            \"condition\": [{\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'add'\",\n" +
                "                \"relation\": \"or\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }, {\n" +
                "                \"lBracket\": \"\",\n" +
                "                \"source\": \"state\",\n" +
                "                \"compare\": \"===\",\n" +
                "                \"target\": \"'edit'\",\n" +
                "                \"relation\": \"\",\n" +
                "                \"rBracket\": \"\"\n" +
                "            }],\n" +
                "            \"name\": \"可编辑\",\n" +
                "            \"description\": \"\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"state\": [{\n" +
                "        \"state\": \"add\",\n" +
                "        \"name\": \"新增\",\n" +
                "        \"description\": \"处于新增状态\"\n" +
                "    }, {\n" +
                "        \"state\": \"init\",\n" +
                "        \"name\": \"初始\",\n" +
                "        \"description\": \"处于初始状态\"\n" +
                "    }, {\n" +
                "        \"state\": \"edit\",\n" +
                "        \"name\": \"编辑\",\n" +
                "        \"description\": \"处于编辑状态\"\n" +
                "    }],\n" +
                "    \"initialState\": \"init\",\n" +
                "    \"action\": {\n" +
                "        \"Create\": {\n" +
                "            \"name\": \"创建\",\n" +
                "            \"transitTo\": \"add\",\n" +
                "            \"description\": \"新建并切换至新增状态\"\n" +
                "        },\n" +
                "        \"Edit\": {\n" +
                "            \"name\": \"编辑\",\n" +
                "            \"transitTo\": \"edit\",\n" +
                "            \"description\": \"编辑并切换至编辑状态\"\n" +
                "        },\n" +
                "        \"Cancel\": {\n" +
                "            \"name\": \"取消\",\n" +
                "            \"transitTo\": \"init\",\n" +
                "            \"description\": \"取消并切换至初始状态\"\n" +
                "        },\n" +
                "        \"Save\": {\n" +
                "            \"name\": \"保存\",\n" +
                "            \"transitTo\": \"init\",\n" +
                "            \"description\": \"保存并切换至初始状态\"\n" +
                "        }\n" +
                "    }\n" +
                "}";
    }
}
