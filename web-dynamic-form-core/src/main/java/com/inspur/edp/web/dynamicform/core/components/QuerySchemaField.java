/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.dynamicform.core.components;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

/**
 * 筛选方案字段
 *
 * @author guozhiqi
 */
@Data
public class QuerySchemaField {
    private String id;
    private String labelCode;
    private String code;

    private String name;

    private QuerySchemaFieldControl control;

    /**
     * 从dom结构 构造对应的筛选方案字段
     *
     * @param componentItem
     * @return
     */
    public static QuerySchemaField fromComponentItem(HashMap<String, Object> componentItem) {
        QuerySchemaField querySchemaField = new QuerySchemaField();

        return querySchemaField;
    }


    /**
     * 筛选方案字段 control属性参数
     */
    @Data
    public static class QuerySchemaFieldControl {
        private String id;
        private String controltype;
        private boolean require;
        private String className;

    }

    /**
     * text field control
     */
    public static class QuerySchemaTextFieldControl extends QuerySchemaFieldControl {
        {
            this.setControltype("text");
            this.setRequire(false);
            this.setClassName("");
        }
    }

    /**
     * single-number control
     */
    @Getter
    @Setter
    public static class QuerySchemaSingleNumberFieldControl extends QuerySchemaFieldControl {
        {
            this.setControltype("single-number");
            this.setRequire(false);
            this.setClassName("");
            this.setTextAlign("left");
        }

        private String textAlign;
        private double precision;

        private boolean isBigNumber;
    }

    /**
     * number field control
     */
    @Getter
    @Setter
    public static class QuerySchemaNumberFieldControl extends QuerySchemaSingleNumberFieldControl {
        {
            this.setControltype("number");
            this.setRequire(false);
            this.setClassName("");
            this.setTextAlign("left");
        }
    }

    @Getter
    @Setter
    public static class QuerySchemaSingleDateFieldControl extends QuerySchemaFieldControl {
        private String format;

        {
            this.setControltype("single-date");
            this.setRequire(false);
            this.setFormat("yyyy-MM-dd");
        }
    }

    @Getter
    @Setter
    public static class QuerySchemaDateFieldControl extends QuerySchemaFieldControl {
        private boolean weekSelect;
        private String format;

        {
            this.setControltype("date");
            this.setRequire(false);
            this.setFormat("yyyy-MM-dd");
            this.setWeekSelect(false);
        }
    }

    @Getter
    @Setter
    public static class QuerySchemaDropdownFieldControl extends QuerySchemaFieldControl {
        private String valueType;
        private Object enumValues;
        private boolean multiSelect;
        private double panelHeight;

        {
            this.setControltype("dropdown");
            this.setRequire(false);
            this.setValueType("1");
            this.setClassName("");
            this.setMultiSelect(false);
        }

    }
}
