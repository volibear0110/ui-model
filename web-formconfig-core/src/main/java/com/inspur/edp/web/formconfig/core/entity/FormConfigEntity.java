/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.core.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.formconfig.api.dto.FormConfig;
import com.inspur.edp.web.formconfig.api.dto.FormConfigItem;
import io.iec.edp.caf.commons.utils.StringUtils;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Data
@Entity
@Table(name = "gspformconfig")
public class FormConfigEntity {

    @Id
    private String id;

    /**
     * 表单控件状态配置项集合
     */
    private String items;

    /**
     * 修改人
     */
    private String lastChangedBy;

    /**
     * 修改时间
     */
    private Date lastChangedOn;

    /**
     * 表单元数据ID
     */
    private String formId;

    /**
     * 表单控件ID
     */
    private String type;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createOn;

    /**
     * 关联表单设置ID
     */
    private String refConfigId;

    public FormConfigEntity() {

    }

    /**
     * 通过dto构造entity(无自动生成属性)
     * @param formConfig
     */
    public FormConfigEntity(FormConfig formConfig) {
        try {
            if (formConfig.getItems() != null && formConfig.getItems().size() > 0) {
                String contentStr = new ObjectMapper().writeValueAsString(formConfig.getItems());
                this.setItems(contentStr);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        this.setId(formConfig.getId());
        this.setFormId(formConfig.getFormId());
        this.setRefConfigId(formConfig.getRefConfigId());
        this.setType(formConfig.getType());

    }

    /**
     * 通过dto构造entity
     * @param formConfig
     */
    public void assignFormConfigEntity(FormConfig formConfig) {
        try {
            if (formConfig.getItems() != null && formConfig.getItems().size() > 0) {
                String contentStr = new ObjectMapper().writeValueAsString(formConfig.getItems());
                this.setItems(contentStr);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        this.setId(StringUtils.isEmpty(formConfig.getId()) ? UUID.randomUUID().toString() : formConfig.getId());
        this.setFormId(formConfig.getFormId());
        this.setRefConfigId(formConfig.getRefConfigId());
        this.setType(formConfig.getType());
    }


    /**
     * 通过entity构造dto
     * @return
     */
    public FormConfig toModel() {
        FormConfig formConfig = new FormConfig();
        ObjectMapper mapper = new ObjectMapper();
        List<FormConfigItem> items = new ArrayList<>();
        try {
            if (!StringUtils.isEmpty(this.items)) {
                items = mapper.readValue(this.getItems(), new TypeReference<List<FormConfigItem>>() {});
            }
            formConfig.setItems(items);
        } catch (JsonProcessingException e) {
            throw new WebCustomException("FormConfig toModel失败",e);
        }
        formConfig.setId(this.getId());
        formConfig.setFormId(this.getFormId());
        formConfig.setRefConfigId(this.getRefConfigId());
        formConfig.setType(this.getType());
        return formConfig;
    }
}
