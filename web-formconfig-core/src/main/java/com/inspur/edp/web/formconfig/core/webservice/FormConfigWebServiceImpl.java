/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.core.webservice;

import com.inspur.edp.web.formconfig.api.dto.FormConfig;
import com.inspur.edp.web.formconfig.api.service.FormConfigService;
import com.inspur.edp.web.formconfig.api.webservice.FormConfigWebService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;

public class FormConfigWebServiceImpl implements FormConfigWebService {

    FormConfigService formConfigService = SpringBeanUtils.getBean(FormConfigService.class);

    @Override
    public void addFormConfig(FormConfig formConfig) {
        formConfigService.addFormConfig(formConfig);
    }

    @Override
    public void updateFormConfig(FormConfig formConfig) {
        formConfigService.updateFormConfig(formConfig);

    }

    @Override
    public FormConfig getFomConfigById(String configId) {
        return formConfigService.getFormConfigById(configId);
    }

    @Override
    public List<FormConfig> getFomConfig(FormConfig formConfig) {
        return formConfigService.getFormConfig(formConfig);
    }
}
