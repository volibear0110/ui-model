/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.core.config;

import com.inspur.edp.web.formconfig.api.service.FormConfigService;
import com.inspur.edp.web.formconfig.core.service.FormConfigServiceImpl;
import com.inspur.edp.web.formconfig.core.webservice.FormConfigWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.inspur.edp.web.formconfig.core.repository")
@EntityScan("com.inspur.edp.web.formconfig.core.entity")
public class FormConfigConfiguration {
    @Bean
    public RESTEndpoint formConfigEndpoint() {
        return new RESTEndpoint("/dev/main/v1.0/formconfig", new FormConfigWebServiceImpl());
    }

    @Bean
    public FormConfigService formConfigService() {
        return new FormConfigServiceImpl();
    }
}
