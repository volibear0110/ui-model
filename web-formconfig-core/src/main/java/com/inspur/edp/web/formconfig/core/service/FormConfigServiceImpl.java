/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.core.service;

import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formconfig.api.dto.FormConfig;
import com.inspur.edp.web.formconfig.api.service.FormConfigService;
import com.inspur.edp.web.formconfig.core.entity.FormConfigEntity;
import com.inspur.edp.web.formconfig.core.repository.FormConfigRepository;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class FormConfigServiceImpl implements FormConfigService {

    FormConfigRepository repository = SpringBeanUtils.getBean(FormConfigRepository.class);

    @Override
    public void addFormConfig(FormConfig config) {
        FormConfigEntity entity = new FormConfigEntity();
        entity.assignFormConfigEntity(config);
        entity.setCreateBy(CAFContext.current.getUserId());
        entity.setCreateOn(new Date());
        entity.setLastChangedBy(CAFContext.current.getUserId());
        entity.setLastChangedOn(new Date());
        repository.save(entity);
    }

    @Override
    public void updateFormConfig(FormConfig config) {
        Optional<FormConfigEntity> optionalEntity = repository.findById(config.getId());
        optionalEntity.ifPresent(t -> {
            t.assignFormConfigEntity(config);
            t.setLastChangedOn(new Date());
            t.setLastChangedBy(CAFContext.current.getUserId());
            repository.save(t);
        });
    }

    @Override
    public FormConfig getFormConfigById(String id) {
        if (StringUtility.isNullOrEmpty(id)) {
            throw new WebCustomException("根据格式定义id获取格式定义信息，格式定义id参数不能为空");
        }
        return repository.findById(id).map(FormConfigEntity::toModel).orElse(null);
    }

    @Override
    public List<FormConfig> getFormConfig(FormConfig formConfig) {
        FormConfigEntity entity = new FormConfigEntity(formConfig);
        Example<FormConfigEntity> example = Example.of(entity);
        List<FormConfigEntity> formConfigEntities = repository.findAll(example);
        List<FormConfig> result = new LinkedList<>();
        if (formConfigEntities.size() > 0) {
            formConfigEntities.forEach(item -> result.add(item.toModel()));
        }
        return result;
    }
}
