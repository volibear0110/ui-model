package com.inspur.edp.web.form.process.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/")
@Consumes({"application/json"})
@Produces({"application/json"})
public class FormProcessWebService {
    @Path("/form-format")
    @PUT
    public void publishFormFormat(@QueryParam("id") String formId, @QueryParam("path") String path) {
        FormProcessManager.getInstance().publishFormFormat(formId, path);
    }
}
