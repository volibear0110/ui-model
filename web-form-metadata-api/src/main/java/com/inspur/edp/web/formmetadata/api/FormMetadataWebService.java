/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.api;

import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.formmetadata.api.dto.FormRelateMetadataInDesignParameterDto;
import com.inspur.edp.web.formmetadata.api.dto.FormRelateMetadataInDesignResultDto;
import com.inspur.edp.web.formmetadata.api.entity.FormSuInfoEntity;
import com.inspur.edp.web.formmetadata.api.entity.ReplicateFormRequestBody;
import com.inspur.edp.web.formmetadata.api.entity.SuInfoWithBizobjIdEntity;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author guozhiqi
 */
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface FormMetadataWebService {
    /**
     * 同步语言包到表单元数据
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Path("sync-language-package")
    @POST
    void synchronizeLanuagePackage(@QueryParam("fileName") String fileName, @QueryParam("path") String path);

    /**
     * 导出表单元数据中语言包
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Path("export-language-package")
    @POST
    void exportLanguagePackage(@QueryParam("fileName") String fileName, @QueryParam("path") String path);

    /**
     * 同步本地语言包到表单元数据
     *
     * @param fileName 待同步表单元数据文件名
     * @param path     待同步表单元数据路径
     */
    @Path("sync-local-language-package")
    @POST
    void synchronizeLocalLanuagePackage(@QueryParam("fileName") String fileName, @QueryParam("path") String path);

    /**
     * 获取启用的语种
     */
    @Path("language-enabled-list")
    @GET
    List<EcpLanguage> getEnabledLanguageList();

    /**
     * 获取系统内置的语种
     */
    @Path("language-built-in-list")
    @GET
    List<EcpLanguage> getBuiltinLanguageList();

    /**
     * 获取所有语种的列表
     */
    @Path("all-language-list")
    @GET
    List<EcpLanguage> getAllLanguageList();

    /**
     * 复制表单及关联元数据
     */
    @Path("replicate")
    @POST
    ResultMessage<String> replicateForm(@RequestBody ReplicateFormRequestBody replicateFormRequestBody);


    @Path("get-suinfo-with-bizobjid")
    @POST
    FormSuInfoEntity getSuInfoWithBizobjId(@RequestBody SuInfoWithBizobjIdEntity bizobjIdEntity);


    /**
     * 获取表单关联的元数据参数信息系
     *
     * @param parameter
     * @return
     */
    @Path("get-form-relate-metadata")
    @POST
    List<FormRelateMetadataInDesignResultDto> getFormRelateMetadata(@RequestBody  FormRelateMetadataInDesignParameterDto parameter);
}
