/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.api.dto;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.web.formmetadata.api.entity.FormSuInfoEntity;

/**
 * 表单关联元数据设计时获取返回结果
 *
 * @author guozhiqi
 */
public class FormRelateMetadataInDesignResultDto {
    public FormRelateMetadataInDesignResultDto(RelateMetadataTypeEnum relateMetadataType,
                                               String metadataName, String metadataFileName, String metadataId,
                                               IMetadataContent metadataContent, String relativePath) {
        this.metadataType = relateMetadataType;
        this.metadataContent = metadataContent;
        this.metadataName = metadataName;
        this.metadataId = metadataId;
        this.relativePath = relativePath;
        this.metadataFileName = metadataFileName;
    }

    public FormRelateMetadataInDesignResultDto(RelateMetadataTypeEnum relateMetadataType,
                                               String metadataName, String metadataFileName, String metadataId,
                                               IMetadataContent metadataContent, String relativePath, FormSuInfoEntity suInfo) {
        this.metadataType = relateMetadataType;
        this.metadataContent = metadataContent;
        this.metadataName = metadataName;
        this.metadataId = metadataId;
        this.suInfo = suInfo;
        this.relativePath = relativePath;
        this.metadataFileName = metadataFileName;
    }

    private final String metadataName;

    private final String metadataFileName;
    private final String metadataId;
    // 元数据类型
    private final RelateMetadataTypeEnum metadataType;
    // 元数据内容
    private final IMetadataContent metadataContent;

    /**
     * 元数据的相对路径参数
     */
    private final String relativePath;

    /**
     * 关联的表单元数据id  仅限于和表单有关联的元数据
     */
    private  String relateFormMetadataId;

    /**
     * 对应的su信息
     * 当前仅设置表单元数据所属的SU Info信息
     */
    private FormSuInfoEntity suInfo;

    public String getMetadataName() {
        return metadataName;
    }

    public String getMetadataId() {
        return metadataId;
    }

    public RelateMetadataTypeEnum getMetadataType() {
        return metadataType;
    }

    public IMetadataContent getMetadataContent() {
        return metadataContent;
    }

    public FormSuInfoEntity getSuInfo() {
        return suInfo;
    }

    public void setSuInfo(FormSuInfoEntity suInfo) {
        this.suInfo = suInfo;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public String getMetadataFileName() {
        return metadataFileName;
    }

    public String getRelateFormMetadataId() {
        return relateFormMetadataId;
    }

    public void setRelateFormMetadataId(String relateFormMetadataId) {
        this.relateFormMetadataId = relateFormMetadataId;
    }
}