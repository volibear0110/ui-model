/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.api.dto;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取表单及其关联元数据请求参数
 * 该接口适用于设计时使用元数据文件名称及路径获取
 *
 * @author guozhiqi
 */
public class FormRelateMetadataInDesignParameterDto {
    /**
     * 表单元数据文件名称
     */
    private String formMetadataFileName;
    /**
     * 表单元数据所属工程路径
     */
    private String formMetadataPath;

    /**
     * 运行时传递元数据ID
     * 设计时传递元数据文件路径
     */
    private  String formMetadataId;

    /**
     * 执行环境
     * 获取设计时元数据还是运行时元数据
     */
    private ExecuteEnvironment environment = ExecuteEnvironment.Design;

    /**
     * 待获取的关联元数据类型
     * 此参数为空 那么获取表单、状态机、命令元数据三类
     * 当前不使用该参数，因为解析表单当前需要获取这三类元数据
     */
    private List<RelateMetadataTypeEnum> relateMetadataTypeList;

    public String getFormMetadataFileName() {
        return formMetadataFileName;
    }

    public void setFormMetadataFileName(String formMetadataFileName) {
        this.formMetadataFileName = formMetadataFileName;
    }

    public String getFormMetadataPath() {
        return formMetadataPath;
    }

    public void setFormMetadataPath(String formMetadataPath) {
        this.formMetadataPath = formMetadataPath;
    }

    public List<RelateMetadataTypeEnum> getRelateMetadataTypeList() {
        if (this.relateMetadataTypeList == null) {
            this.relateMetadataTypeList = new ArrayList<>();
        }
        return relateMetadataTypeList;
    }

    public void setRelateMetadataTypeList(List<RelateMetadataTypeEnum> relateMetadataTypeList) {
        this.relateMetadataTypeList = relateMetadataTypeList;
    }

    public String getFormMetadataId() {
        return formMetadataId;
    }

    public void setFormMetadataId(String formMetadataId) {
        this.formMetadataId = formMetadataId;
    }

    public ExecuteEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(ExecuteEnvironment environment) {
        this.environment = environment;
    }
}