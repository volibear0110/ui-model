/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.api.dto;

/**
 * 关联元数据类型
 * @author guozhiqi
 */
public enum RelateMetadataTypeEnum {
    Form {
        @Override
        public String getName() {
            return "Form";
        }
    },
    StateMachine {
        @Override
        public String getName() {
            return "StateMachine";
        }
    },
    Command {
        @Override
        public String getName() {
            return "Command";
        }
    },
    Component{
        @Override
        public String getName() {
            return "Component";
        }
    };

    /**
     * 获取关联类型对应的固定名称定义
     *
     * @return
     */
    public abstract String getName();
}