/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formmetadata.api.entity;

import java.io.Serializable;

/**
 * 表单复制参数
 *
 * @author guozhiqi
 */
public final class ReplicateFormRequestBody implements Serializable {
    private static final long serialVersionUID = 9090L;

    /**
     * 元数据id
     */
    private String sourceMetadataId;

    public String getSourceMetadataId() {
        return this.sourceMetadataId;
    }

    public void setSourceMetadataId(String value) {
        this.sourceMetadataId = value;
    }

    /**
     * 元数据文件相对路径
     */
    private String sourceMetadataRelativePath;

    public String getSourceMetadataRelativePath() {
        return this.sourceMetadataRelativePath;
    }

    public void setSourceMetadataRelativePath(String value) {
        this.sourceMetadataRelativePath = value;
    }

    /**
     * 目标元数据编号
     */
    private String targetMetadataCode;

    public String getTargetMetadataCode() {
        return this.targetMetadataCode;
    }

    public void setTargetMetadataCode(String value) {
        this.targetMetadataCode = value;
    }

    /**
     * 目标元数据名称
     */
    private String targetMetadataName;

    public String getTargetMetadataName() {
        return this.targetMetadataName;
    }

    public void setTargetMetadataName(String value) {
        this.targetMetadataName = value;
    }

    /**
     * 目标工程名
     */
    private String targetProjectName;

    public String getTargetProjectName() {
        return this.targetProjectName;
    }

    public void setTargetProjectName(String value) {
        this.targetProjectName = value;
    }
}
