/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.api.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FormConfig {

    private String id;
    /**
     * 控件修改属性集合
     */
    private List<FormConfigItem> items;

    /**
     * 表单元数据ID
     */
    private String formId;

    /**
     * 表单控件类型
     */
    private String type;

    /**
     * 关联表单格式设置Id
     */
    private String refConfigId;

    public List<FormConfigItem> getItems() {
        if (items == null) {
            items = new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<FormConfigItem> items) {
        this.items = items;
    }
}
