/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.formconfig.api.webservice;

import com.inspur.edp.web.formconfig.api.dto.FormConfig;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface FormConfigWebService {

    @POST
    @Path("/addformconfig")
    void addFormConfig(FormConfig formConfig);

    @POST
    @Path("/updateformconfig")
    void updateFormConfig(FormConfig formConfig);

    @GET
    @Path("/getfomconfigbyid/{configid}")
    FormConfig getFomConfigById(@PathParam("configid") String configId);


    @POST
    @Path("/getfomconfig")
    List<FormConfig> getFomConfig(FormConfig formConfig);


}
