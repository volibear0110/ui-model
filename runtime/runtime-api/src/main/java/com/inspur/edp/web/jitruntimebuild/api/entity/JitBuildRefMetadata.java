/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.api.entity;

import com.inspur.edp.web.common.metadata.MetadataTypeEnum;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class JitBuildRefMetadata {
    /**
     * 元数据内容
     */
    private   String content;

    /**
     * 元数据类型
     */
    private MetadataTypeEnum metadataType;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MetadataTypeEnum getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(MetadataTypeEnum metadataType) {
        this.metadataType = metadataType;
    }

    /**
     * 元数据名称
     */
    private  String metaDataName;

    public String getMetaDataName() {
        return metaDataName;
    }

    public void setMetaDataName(String metaDataName) {
        this.metaDataName = metaDataName;
    }

    /**
     * 元数据名称
     */
    private  String formName;

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     * 元数据名称
     */
    private  String formCode;

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }
}
