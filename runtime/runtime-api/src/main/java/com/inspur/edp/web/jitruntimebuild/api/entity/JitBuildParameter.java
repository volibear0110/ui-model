/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.api.entity;

import com.inspur.edp.web.common.JITEngineConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description:运行时定制编译参数
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class JitBuildParameter {
    /**
     * 编译的工程目录 绝对路径 运行时定制根目录
     */
    private String buildProjectPath;

    /**
     * 编译依赖的node_modules路径  绝对路径
     */
    private String buildNodeModulePath;

    /**
     * 编译的表单路径。 依据表单所在的维度信息构造唯一的路径信息
     */
    private String buildFormPath;

    public String getExtraFormPathStr() {
        return extraFormPathStr;
    }

    public void setExtraFormPathStr(String extraFormPathStr) {
        this.extraFormPathStr = extraFormPathStr;
    }

    /**
     * 表单路径额外参数 只有在buildFormPath为空时读取该参数
     */
    private String[] extraFormPath;


    /**
     * 维度信息的字符串表述形式
     */
    private String extraFormPathStr;

    /**
     * 表单JSON文件存放路径
     */
    private String buildWebDevPath;

    /**
     * webdev 文件目录名称 默认为webdev
     * 该参数默认不需要任何更改，仅当需要调整存放目录时进行调整
     */
    private String webDevPathName = JITEngineConstants.WebDevPathName;

    /**
     * node_modules 路径名称
     * 该参数默认不需要任何更改，仅当node_modules文件夹名称发生变化时进行调整。
     */
    private String node_ModulesName = JITEngineConstants.Node_ModulesPathName;

    /**
     * 生成表单ts源代码文件目录
     */
    private String buildAppPath;

    /**
     * app 路径
     */
    private String appPathName = "app";

    /**
     * 表单文件保存后缀
     */
    private String formFileSuffix = ".json";

    private String formId;

    /**
     * 是否移动app
     */
    private boolean isMobileApp = false;

    public boolean isMobileApp() {
        return isMobileApp;
    }

    public void setMobileApp(boolean mobileApp) {
        isMobileApp = mobileApp;
    }

    private boolean isBabelCompile = false;

    public boolean isBabelCompile() {
        return isBabelCompile;
    }

    public void setBabelCompile(boolean babelCompile) {
        isBabelCompile = babelCompile;
    }

    // 是否移动审批
    private boolean isMobileApprove = false;


    // 是否在tool中运行
    private boolean isInUpgradeTool = false;

    public boolean isInUpgradeTool() {
        return isInUpgradeTool;
    }

    public void setInUpgradeTool(boolean inUpgradeTool) {
        isInUpgradeTool = inUpgradeTool;
    }

    /**
     * 表单code
     */
    private String formCode;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * su路径。apps/scm/sd
     */
    private String serviceUnitPath;

    private String boCode;

    private String boName;

    /**
     * 部署目标选项枚举   默认为部署单个表单
     */
    private DeployTargetEnum deployTargetEnum = DeployTargetEnum.Form;

    /**
     * 部署目标选项
     *
     * @return
     */
    public DeployTargetEnum getDeployTargetEnum() {
        return deployTargetEnum;
    }

    /**
     * 设置部署目标选项
     *
     * @param deployTargetEnum
     */
    public void setDeployTargetEnum(DeployTargetEnum deployTargetEnum) {
        this.deployTargetEnum = deployTargetEnum;
    }


    public String getBoCode() {
        return boCode;
    }

    public void setBoCode(String boCode) {
        this.boCode = boCode;
    }

    public String getBoName() {
        return boName;
    }

    public void setBoName(String boName) {
        this.boName = boName;
    }

    public boolean isMobileApprove() {
        return isMobileApprove;
    }

    public void setMobileApprove(boolean mobileApprove) {
        isMobileApprove = mobileApprove;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * 运行时表单编译依赖元数据
     */
    private List<JitBuildRefMetadata> buildRefMetadataList;

    public String getBuildProjectPath() {
        return buildProjectPath;
    }

    public void setBuildProjectPath(String buildProjectPath) {
        this.buildProjectPath = buildProjectPath;
    }

    public String getBuildNodeModulePath() {
        return buildNodeModulePath;
    }

    public void setBuildNodeModulePath(String buildNodeModulePath) {
        this.buildNodeModulePath = buildNodeModulePath;
    }

    public String getBuildFormPath() {
        return buildFormPath;
    }

    public void setBuildFormPath(String buildFormPath) {
        this.buildFormPath = buildFormPath;
    }

    public String[] getExtraFormPath() {
        return extraFormPath;
    }

    public void setExtraFormPath(String[] extraFormPath) {
        this.extraFormPath = extraFormPath;
    }

    public String getBuildWebDevPath() {
        return buildWebDevPath;
    }

    public void setBuildWebDevPath(String buildWebDevPath) {
        this.buildWebDevPath = buildWebDevPath;
    }

    public String getWebDevPathName() {
        return webDevPathName;
    }

    public void setWebDevPathName(String webDevPathName) {
        this.webDevPathName = webDevPathName;
    }

    public String getNode_ModulesName() {
        return node_ModulesName;
    }

    public void setNode_ModulesName(String node_ModulesName) {
        this.node_ModulesName = node_ModulesName;
    }

    public String getBuildAppPath() {
        return buildAppPath;
    }

    public void setBuildAppPath(String buildAppPath) {
        this.buildAppPath = buildAppPath;
    }

    public String getAppPathName() {
        return appPathName;
    }

    public String getBabelRunTimeAppPathName() {
        return "appForBabel";
    }

    public void setAppPathName(String appPathName) {
        this.appPathName = appPathName;
    }

    public String getFormFileSuffix() {
        return formFileSuffix;
    }

    public void setFormFileSuffix(String formFileSuffix) {
        this.formFileSuffix = formFileSuffix;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getServiceUnitPath() {
        return serviceUnitPath;
    }

    public void setServiceUnitPath(String serviceUnitPath) {
        this.serviceUnitPath = serviceUnitPath;
    }

    public List<JitBuildRefMetadata> getBuildRefMetadataList() {
        return buildRefMetadataList;
    }

    public void setBuildRefMetadataList(List<JitBuildRefMetadata> buildRefMetadataList) {
        this.buildRefMetadataList = buildRefMetadataList;
    }

    /**
     * 额外的参数选项 为了避免每次参数的增加都需要进行多方修正
     * 因此增加该参数
     */
    private Map<String, Object> extraOptions;

    public Map<String, Object> getExtraOptions() {
        if (extraOptions == null) {
            extraOptions = new HashMap<>();
        }
        return extraOptions;
    }

    public void setExtraOptions(Map<String, Object> extraOptions) {
        this.extraOptions = extraOptions;
    }
}
