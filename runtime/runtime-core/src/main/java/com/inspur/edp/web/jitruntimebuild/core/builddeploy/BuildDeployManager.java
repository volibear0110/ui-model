/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.builddeploy;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.jitruntimebuild.api.entity.DeployTargetEnum;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.core.utility.JitBuildUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.service.ScriptCacheService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerPathGenerator;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;

/**
 * @author noah
 */
public class BuildDeployManager {
    private BuildDeployManager() {
    }

    private static final String CurrentClassName = BuildDeployManager.class.getName();

    /**
     * 表单参数部署
     *
     * @param buildParameter
     */
    public static void deployForm(JitBuildParameter buildParameter) {
        WebLogger.Instance.info("begin deploy form, the formCode is " + buildParameter.getFormCode(), CurrentClassName);
        String strDeployPath = getDeployPath(buildParameter);


        String strBaseSourcePath = buildParameter.getBuildAppPath() + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                JITEngineConstants.DistRollupPathName + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                buildParameter.getBoCode()).toLowerCase();

        if (buildParameter.getDeployTargetEnum() == DeployTargetEnum.Form) {
            String sourcePath = strBaseSourcePath + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getFormName();
            // 如果是移动或babel编译 那么拷贝整个工程到模板目录
            if (buildParameter.isMobileApp() || buildParameter.isBabelCompile()) {
                sourcePath = strBaseSourcePath;
            }
            FileUtility.copyFolder(sourcePath, strDeployPath, true);

            WebLogger.Instance.info("deploy form,the source path is " + sourcePath + " , target path is " + strDeployPath, CurrentClassName);

            // 部署version.json 文件
            String strSourceVersionPath = FileUtility.combine(strBaseSourcePath, "version.json");
            String strTargetVersionPath = FileUtility.combine(strDeployPath, "version.json");
            if (FileUtility.exists(strSourceVersionPath)) {
                FileUtility.copyFile(strSourceVersionPath, strTargetVersionPath, true);
                WebLogger.Instance.info("deploy version file success,the source file path is " + strSourceVersionPath + ",the target file path is " + strTargetVersionPath, CurrentClassName);
            }

            WebLogger.Instance.info("deploy form success, the formCode is " + buildParameter.getFormCode(), CurrentClassName);


            // 郭志奇 获取对应的元数据id
            String currentServiceUnit = null;
            // 如果是在tool中运行
            if (buildParameter.isInUpgradeTool()) {
                currentServiceUnit = MultiTenantContextHolder.get().getServiceUnit();
                MultiTenantContextHolder.get().setServiceUnit(null);
            }

            publishScript(buildParameter, strDeployPath);

            // 如果是在tool中运行
            if (buildParameter.isInUpgradeTool()) {
                MultiTenantContextHolder.get().setServiceUnit(currentServiceUnit);
            }
        } else if (buildParameter.getDeployTargetEnum() == DeployTargetEnum.Project) {
            WebLogger.Instance.info("deploy project,the source project path is " + strBaseSourcePath, CurrentClassName);

            FileUtility.copyFolder(strBaseSourcePath, strDeployPath, true);

            publishScript(buildParameter, strDeployPath);

            WebLogger.Instance.info("deploy project success,the deploy path is " + strDeployPath, CurrentClassName);
        }
    }

    /**
     * 获取部署路径
     *
     * @param buildParameter
     * @return
     */
    private static String getDeployPath(JitBuildParameter buildParameter) {
        String strDeployPath = FileUtility.getCurrentWorkPath(buildParameter.isInUpgradeTool()) + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                "web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                buildParameter.getServiceUnitPath() + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                JITEngineConstants.DeployWebPathName + FileUtility.DIRECTORY_SEPARATOR_CHAR
                + buildParameter.getBoCode() + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                buildParameter.getFormName() + JitBuildUtility.getExtraPath(buildParameter.getExtraFormPath())).toLowerCase();
        if (buildParameter.isMobileApp()) {
            strDeployPath = FileUtility.getCurrentWorkPath(buildParameter.isInUpgradeTool()) + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    "web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getServiceUnitPath() + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    "mob" + FileUtility.DIRECTORY_SEPARATOR_CHAR
                    + buildParameter.getBoCode() + JitBuildUtility.getExtraPath(buildParameter.getExtraFormPath())).toLowerCase();
        }

        if (buildParameter.isBabelCompile()) {

            strDeployPath = FileUtility.getCurrentWorkPath(buildParameter.isInUpgradeTool()) + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    "web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getServiceUnitPath() + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    "web" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getBoCode() + "forbabelruntime" + FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getFormName() +
                    JitBuildUtility.getExtraPath(buildParameter.getExtraFormPath())).toLowerCase();
        }
        return strDeployPath;
    }

    /**
     * 执行脚本发布
     *
     * @param buildParameter
     * @param strDeployPath
     */
    private static void publishScript(JitBuildParameter buildParameter, String strDeployPath) {
        WebLogger.Instance.info("execute form script cache ", CurrentClassName);
        try {
            PublishScriptRequest request = getPublishScriptRequest(buildParameter, strDeployPath);

            ScriptCacheService scriptCacheService = SpringBeanUtils.getBean(ScriptCacheService.class);
            scriptCacheService.publishScriptWithDirectory(request);
            WebLogger.Instance.info("execute form script cache completely!!!", CurrentClassName);
        } catch (Exception e) {
            WebLogger.Instance.error(e);
        }
    }

    /**
     * 构造对应的请求入参
     *
     * @param buildParameter
     * @param strDeployPath
     * @return
     */
    private static PublishScriptRequest getPublishScriptRequest(JitBuildParameter buildParameter, String strDeployPath) {
        PublishScriptRequest request = new PublishScriptRequest();
        request.setAbsoluteBaseDirectory(strDeployPath);
        request.setMetaDataId(buildParameter.getFormId());
        request.setProjectName(buildParameter.getBoCode().toLowerCase());
        request.setScriptName(buildParameter.getFormCode());
        // 设置当前表单code
        request.setFormCode(buildParameter.getFormCode());

        // 设置脚本文件相对于运行环境得相对路径
        String strLocalServerPath = LocalServerPathGenerator.getNewInstance(buildParameter.isInUpgradeTool()).getLocalServerWebPath();
        String strRelativePath = FileUtility.getRelativePath(strLocalServerPath, strDeployPath, true);
        request.setProjectRelativePath(strRelativePath);

        if(buildParameter.isMobileApp()){
            request.setRuntimeMobileForm(true);
        }
        return request;
    }


}
