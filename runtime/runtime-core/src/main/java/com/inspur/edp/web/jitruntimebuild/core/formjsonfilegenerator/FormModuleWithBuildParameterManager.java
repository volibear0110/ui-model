/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;

import java.util.concurrent.atomic.AtomicReference;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/07/29
 */
public class FormModuleWithBuildParameterManager {

    /**
     * 根据编译参数获取对应的表单 moduleCode
     *
     * @param buildParameter
     * @return
     */
    public static AtomicReference<String> getFormModuleCodeAtomicReference(JitBuildParameter buildParameter,String formCode) {
        AtomicReference<String> formModuleCode = new AtomicReference<>();
        buildParameter.getBuildRefMetadataList().forEach((item) -> {
            if (item.getMetadataType() == MetadataTypeEnum.Frm && (item.getFormCode() == null || formCode.equals(item.getFormCode()))) {
                FormRuntimeMetadataEntity formRuntimeMetadataEntity = SerializeUtility.getInstance().deserialize(item.getContent(), FormRuntimeMetadataEntity.class);
                if (formRuntimeMetadataEntity != null) {
                    FormDOM visualDom = SerializeUtility.getInstance().deserialize(formRuntimeMetadataEntity.getContents().toString(), FormDOM.class);
                    if (visualDom != null) {
                        formModuleCode.set(visualDom.getModule().getCode());
                    }
                }
            }
        });
        return formModuleCode;
    }
}
