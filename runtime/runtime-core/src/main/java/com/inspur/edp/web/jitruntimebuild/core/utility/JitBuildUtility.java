/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.utility;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.core.constant.FormJsonFileSuffixConstant;

public class JitBuildUtility {
    /**
     * 根据维度获取对应路径
     * @param extraData
     * @return
     */
    public static String getExtraPath(String[] extraData) {
        StringBuilder strWeiDuInfo = new StringBuilder();
        for (String extraDataItem : extraData) {
            if (StringUtility.isNullOrEmpty(extraDataItem)) {
                strWeiDuInfo.append(FileUtility.DIRECTORY_SEPARATOR_CHAR).append(FormJsonFileSuffixConstant.DefaultWeiDuName);
            } else {
                strWeiDuInfo.append(FileUtility.DIRECTORY_SEPARATOR_CHAR).append(extraDataItem);
            }
        }
        return strWeiDuInfo.toString();

    }
}
