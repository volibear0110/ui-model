/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator.formroute;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FormRoutePageEntity {
    @JsonProperty("id")
    private  String id;

    @JsonProperty("code")
    private  String code;

    @JsonProperty("name")
    private  String name;

    @JsonProperty("fileName")
    private  String fileName;

    @JsonProperty("relativePath")
    private  String relativePath;

    @JsonProperty("formUri")
    private  String formUri;

    @JsonProperty("routeUri")
    private  String routeUri;

    @JsonProperty("routeParams")
    private  String routeParams;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public String getFormUri() {
        return formUri;
    }

    public void setFormUri(String formUri) {
        this.formUri = formUri;
    }

    public String getRouteUri() {
        return routeUri;
    }

    public void setRouteUri(String routeUri) {
        this.routeUri = routeUri;
    }

    public String getRouteParams() {
        return routeParams;
    }

    public void setRouteParams(String routeParams) {
        this.routeParams = routeParams;
    }

}
