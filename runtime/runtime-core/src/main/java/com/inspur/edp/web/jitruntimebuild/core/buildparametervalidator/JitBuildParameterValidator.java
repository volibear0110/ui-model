/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.buildparametervalidator;

import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildResponse;

/**
 * jit 编译参数校验
 */
public class JitBuildParameterValidator {
    /**
     * 编译参数验证
     *
     * @param buildParameter 编译参数
     */
    public static JitBuildResponse validate(JitBuildParameter buildParameter) {
        JitBuildResponse buildResponse = new JitBuildResponse();
        // 编译参数不允许为空
        if (buildParameter == null) {
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage("build parameter can not be null");
            return buildResponse;
        }
        boolean isMobileApprove = buildParameter.isMobileApprove();

        // 针对具体类型进行不同参数的验证
        if (!isMobileApprove) {
            buildResponse = specificValidate(buildParameter);
        } else {
            buildResponse = mobileApprovalValidate(buildParameter);
        }
        return buildResponse;

    }

    /**
     * 运行时定制传递参数验证
     *
     * @param buildParameter
     * @return
     */
    private static JitBuildResponse specificValidate(JitBuildParameter buildParameter) {
        JitBuildResponse buildResponse = new JitBuildResponse();
        if (StringUtility.isNullOrEmpty(buildParameter.getFormCode())) {
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage("jit build,formCode can not be null");
            return buildResponse;
        }

        if ((buildParameter.getBuildRefMetadataList() == null || buildParameter.getBuildRefMetadataList().size() == 0)) {
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage("jit build,form metadata can not be null");
            return buildResponse;
        }
        return buildResponse;
    }

    /**
     * 移动审批参数校验
     *
     * @param buildParameter 构建参数验证
     * @return
     */
    private static JitBuildResponse mobileApprovalValidate(JitBuildParameter buildParameter) {
        JitBuildResponse buildResponse = new JitBuildResponse();
        // 移动审批中webdev path 是必须存在
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildWebDevPath())) {
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage("webdev path is necessary");
            return buildResponse;
        }
        return buildResponse;
    }
}
