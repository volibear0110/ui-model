/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
abstract class AbstractFormJsonFileGenerator implements JsonFileGeneratorInterface {

    private final ExecuteEnvironment executeEnvironment;
    private boolean isUpgradeTool = false;

    protected AbstractFormJsonFileGenerator(ExecuteEnvironment executeEnvironment, boolean isUpgradeTool) {
        this.executeEnvironment = executeEnvironment;
        this.isUpgradeTool = isUpgradeTool;
    }

    protected boolean isUpgradeTool() {
        return isUpgradeTool;
    }


    protected ExecuteEnvironment getExecuteEnvironment() {
        return this.executeEnvironment;
    }

    /**
     * 生成文件全称
     *
     * @param fileNameWithoutExtension
     * @param fileSuffix
     * @return
     */
    protected String generateFileName(String fileNameWithoutExtension, String fileSuffix) {
        return fileNameWithoutExtension + fileSuffix;
    }

    /**
     * 文件写入
     *
     * @param basePath
     * @param fileName
     * @param content
     */
    protected void writeFile(String basePath, String fileName, String content) {
        FileUtility.writeFile(basePath, fileName, content);
    }

}
