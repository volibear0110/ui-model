/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
class ResourceJsonFileGenerator extends AbstractFormJsonFileGenerator implements JsonFileGeneratorInterface {

    ResourceJsonFileGenerator(ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        super(executeEnvironment, isUpdradeTool);
    }

    @Override
    public void generate(String basePath, String formName, String content) {
        String resourceFileName = this.generateFileName(formName, JITEngineConstants.FrmSuffix + JITEngineConstants.ResourceJsonFile);
        this.writeFile(basePath, resourceFileName, content);
    }
}
