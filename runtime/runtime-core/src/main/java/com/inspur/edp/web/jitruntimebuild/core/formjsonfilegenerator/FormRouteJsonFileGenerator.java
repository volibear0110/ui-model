/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator.formroute.FormRouteEntity;
import com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator.formroute.FormRoutePageEntity;
import com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator.formroute.FormRouteProjectEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
class FormRouteJsonFileGenerator extends AbstractFormJsonFileGenerator implements JsonFileGeneratorInterface {

    FormRouteJsonFileGenerator(ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        super(executeEnvironment, isUpdradeTool);
    }

    @Override
    public void generate(String basePath, String formName, String content) {

    }

    public void generate(String basePath, String projectName, Map<String,String> formCodeNameMap, String content, JitBuildParameter buildParameter) {
        String routeJsonFileName = this.generateFileName(projectName, JITEngineConstants.ProjectRouteFileExtension);
        FormRouteEntity formRouteEntity = this.generateRouteContent(formCodeNameMap, projectName, buildParameter);
        this.writeFile(basePath, routeJsonFileName, SerializeUtility.getInstance().serialize(formRouteEntity));
    }

    /**
     * 根据表单code构造页面流
     *
     * @param formCodeNameMap
     * @return
     */
    private FormRouteEntity generateRouteContent(Map<String,String> formCodeNameMap, String projectName, JitBuildParameter buildParameter) {
        FormRouteEntity formRouteEntity = new FormRouteEntity();
        formRouteEntity.setId(UUID.randomUUID().toString());

        // 设置路由entry
        formRouteEntity.setEntry(projectName);
        // 设置页面流 project
        FormRouteProjectEntity formRouteProjectEntity = new FormRouteProjectEntity();
        formRouteProjectEntity.setName(projectName);
        formRouteEntity.setProject(formRouteProjectEntity);

        List<FormRoutePageEntity> pages = new ArrayList<>();

        for(final Map.Entry<String,String> formCodeName : formCodeNameMap.entrySet()) {
            String formCode = formCodeName.getKey();
            String formName = formCodeName.getValue();
            AtomicReference<String> formModuleCode = FormModuleWithBuildParameterManager.getFormModuleCodeAtomicReference(buildParameter,formCode);

            FormRoutePageEntity formRoutePageEntity = new FormRoutePageEntity();
            formRoutePageEntity.setId(UUID.randomUUID().toString());
            if (formModuleCode != null) {
                formRoutePageEntity.setCode(formModuleCode.get());
            } else {
                formRoutePageEntity.setCode(formCode);
            }
            formRoutePageEntity.setName(formCode);
            formRoutePageEntity.setFileName(formCode + ".frm");
            formRoutePageEntity.setRouteUri(formName);
            pages.add(formRoutePageEntity);
        }

        formRouteEntity.setPages(pages.toArray(new FormRoutePageEntity[0]));
        return formRouteEntity;
    }
}
