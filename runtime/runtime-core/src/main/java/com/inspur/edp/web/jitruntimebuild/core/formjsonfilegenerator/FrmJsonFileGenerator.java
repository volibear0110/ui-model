/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.i18n.resource.api.metadata.ResourceMetadata;
import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;

import com.inspur.edp.web.formmetadata.metadataanalysis.*;
import com.inspur.edp.web.formmetadata.metadataanalysis.form.AnalysisExternalComponentResult;
import com.inspur.edp.web.jitengine.expressions.*;
import com.inspur.edp.web.jitengine.expressions.utility.ExpressionUtility;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class FrmJsonFileGenerator extends AbstractFormJsonFileGenerator implements JsonFileGeneratorInterface {

    public FrmJsonFileGenerator(ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        super(executeEnvironment, isUpdradeTool);
    }

    @Override
    public void generate(String basePath, String formName, String content) {
        String frmJsonFileName = this.generateFileName(formName, JITEngineConstants.FrmSuffix + JITEngineConstants.FrmJsonFile);
        FormRuntimeMetadataEntity formRuntimeMetadataEntity = SerializeUtility.getInstance().deserialize(content, FormRuntimeMetadataEntity.class);
        if (formRuntimeMetadataEntity != null) {
            this.writeFile(basePath, frmJsonFileName, formRuntimeMetadataEntity.getContents().toString());
        } else {
            WebLogger.Instance.warn("build parameter has none form metadata content, the formCode is " + formName, FrmJsonFileGenerator.class.getName());
        }

        // 生成command json文件
        this.generateCommandJson(formRuntimeMetadataEntity.getContents().toString(), basePath, formName);

        // 生成对应的资源文件
        // this.generateResourceJson(formRuntimeMetadataEntity.getContents(), basePath, formName, "");


        FormDOM visualDom = SerializeUtility.getInstance().deserialize(formRuntimeMetadataEntity.getContents().toString(), FormDOM.class);

        ExpressionManifest expressionManifest = new ExpressionManifest();
        // 设置表达式 manifest.json 文件的code及其对应的文件名
        expressionManifest.setFormModuleCode(visualDom.getModule().getCode());
        expressionManifest.setManifestJsonPath(ExpressionUtility.getExpressionManifestJsonPath(formRuntimeMetadataEntity.getCode()));

        ///获取表单对应的表达式
        ModuleFormExpressions moduleFormExpressions = ExpressionFormGenerator.generate(visualDom, "", null);
        // 仅仅在包含表达式时才进行添加
        if (moduleFormExpressions != null && moduleFormExpressions.getExpressions() != null && moduleFormExpressions.getExpressions().size() > 0) {
            expressionManifest.getExpressions().add(moduleFormExpressions);
        }

        HashMap<String, CommandsAnalysis.WebComponentMetadataAndExtra> projectWebCmd = new HashMap<>();
        this.executeExternalComponent(visualDom, basePath, basePath + "/services", basePath, basePath, null, projectWebCmd, expressionManifest);

        // 写入表单表达式json文件
        ExpressionManifestManager.writeExpressionJson(expressionManifest, basePath, false);
    }


    /**
     * 生成command json文件
     *
     * @param content
     * @param basePath
     */
    private void generateCommandJson(String content, String basePath, String fileName) {
        if (StringUtility.isNullOrEmpty(content)) {
            return;
        }
        WebLogger.Instance.info("generate command json file", FrmJsonFileGenerator.class.getName());
        // 反序列化表单元数据
        FormDOM json = SerializeUtility.getInstance().deserialize(content, FormDOM.class);
        CommandsAnalysis commandsAnalysis = new CommandsAnalysis(this.getExecuteEnvironment(), this.isUpgradeTool());
        HashMap<String, CommandsAnalysis.WebComponentMetadataAndExtra> projectCmpList = new HashMap<>();
        try {
            fileName = fileName + ".frm";
            commandsAnalysis.resolveCommand(CommandsAnalysis.ResolveCommandParameter.init(null, json), fileName, basePath, projectCmpList);
        } catch (Exception e) {
            WebLogger.Instance.error(e);
        }
    }

    /**
     * 生成对应的资源文件
     *
     * @param content
     * @param basePath
     * @param fileName
     */
    private void generateResourceJson(String content, String basePath, String fileName, String keyPrefix) {
        GspMetadata gspMetadata = SerializeUtility.getInstance().deserialize(content, FormDOM.class);
        List<MetadataReference> refList = gspMetadata.getRefs();
        if (refList != null && refList.size() > 0) {
            List<I18nResource> resourceList = new ArrayList<>();
            refList.forEach(MetadataReference -> {
                if (MetadataReference.getDependentMetadata().getType().equals("ResourceMetadata")) {
                    GspMetadata refMetadata = MetadataUtility.getInstance().getMetadataWithEnvironment(() -> {
                        MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
                        getterMetadataInfo.setId(MetadataReference.getDependentMetadata().getId());
                        getterMetadataInfo.setMetadataType(MetadataTypeEnum.Resource);
                        return getterMetadataInfo;
                    }, null, this.getExecuteEnvironment(), this.isUpgradeTool());
                    if (refMetadata != null && refMetadata.getContent() != null) {
                        ResourceMetadata resourceMetadata = (ResourceMetadata) refMetadata.getContent();
                        if (resourceMetadata != null) {
                            I18nResource item = ConvertToI18nResource(resourceMetadata, resourceMetadata.getOriginalLanguage());
                            resourceList.add(item);
                        }
                    }
                }
            });

            if (resourceList.size() > 0) {
                resourceList.forEach(item -> {
                    I18nResourceItemCollection resourceItemCollection = item.getStringResources();
                    if (resourceItemCollection == null || resourceItemCollection.size() == 0) {
                        return;
                    }

                    // 根据资源项构建生成结构
                    StringBuilder resouceItemsStringBuilder = new StringBuilder();
                    resouceItemsStringBuilder.append("{");
                    for (int i = 0; i < resourceItemCollection.size(); i++) {
                        I18nResourceItem resourceItem = resourceItemCollection.get(i);
                        StringBuilder resouceItemStringBuilder = new StringBuilder();
                        String id = resourceItem.getKey();

                        String updatedId = id.substring(id.lastIndexOf(".") + 1);// 解析ID
                        String value = resourceItem.getValue();
                        String comment = resourceItem.getComment();
                        resouceItemStringBuilder.append("\"").append(updatedId).append("\": {");
                        resouceItemStringBuilder.append("    \"name\": " + "\"").append(value).append("\",");
                        resouceItemStringBuilder.append("    \"comment\": " + "\"").append(comment).append("\"");
                        resouceItemStringBuilder.append("}");


                        if (i != resourceItemCollection.size() - 1) {
                            resouceItemStringBuilder.append(",");
                        }

                        resouceItemsStringBuilder.append(resouceItemStringBuilder);
                    }
                    resouceItemsStringBuilder.append("}");

                    // 将生成的结构持久化到磁盘
                    FileUtility.writeFile(basePath, gspMetadata.getHeader().getFileName().toLowerCase() + JITEngineConstants.ResourceJsonFile, resouceItemsStringBuilder.toString());

                });
            }


        }
    }


    private I18nResource ConvertToI18nResource(ResourceMetadata metadata, String language) {
        if (metadata == null) {
            return null;
        }
        I18nResource i18nResource = new I18nResource();
        i18nResource.setLanguage((language == null) ? "en" : language);
        i18nResource.setStringResources(new I18nResourceItemCollection());

        if (metadata.getStringResources() != null && metadata.getStringResources().size() != 0) {
            metadata.getStringResources().forEach(stringResource -> {
                I18nResourceItem item = new I18nResourceItem();
                item.setKey(stringResource.getId());
                item.setValue(stringResource.getValue());
                item.setValue(stringResource.getComment());

                i18nResource.getStringResources().add(item);
            });

        }

        return i18nResource;
    }

    /**
     * 处理扩展组件
     */
    private void executeExternalComponent(FormDOM json, String targetStorageBasePath, String formServicePath, String projectPath,
                                          String webdevpath, String relativePath, HashMap<String, CommandsAnalysis.WebComponentMetadataAndExtra> projectCmpList, ExpressionManifest expressionManifest) {

        if (json != null && json.getModule() != null && json.getModule().getExternalComponents() != null && json.getModule().getExternalComponents().size() > 0) {
            for (HashMap<String, Object> item : json.getModule().getExternalComponents()) {

                FormAnalysis formAnalysis = new FormAnalysis(this.getExecuteEnvironment(), this.isUpgradeTool());

                AnalysisExternalComponentResult externalVisualDom;
                try {
                    externalVisualDom = formAnalysis.analysisExternalComponent(targetStorageBasePath, json.getModule().getCode().toLowerCase(), relativePath, item, webdevpath);
                } catch (Exception e) {
                    WebLogger.Instance.error(e);
                    return;
                }

                // 如果获取不到对应的表单元数据。针对的是弹窗Url  情况
                if (externalVisualDom != null && externalVisualDom.getJson() != null) {

                    String strContainerId = getExternalContainerId(item);
                    if (externalVisualDom.isUseIsolateJs()) {
                        return;
                    }

                    ///获取表单对应的表达式
                    ModuleFormExpressions moduleFormExpressions = ExpressionFormGenerator.generate(externalVisualDom.getJson(), strContainerId, item);
                    // 仅仅在包含表达式时才进行添加
                    if (moduleFormExpressions != null && moduleFormExpressions.getExpressions() != null && moduleFormExpressions.getExpressions().size() > 0) {
                        expressionManifest.getExpressions().add(moduleFormExpressions);
                    }

                    String externalFormBasePath = FileUtility.combine(targetStorageBasePath, json.getModule().getCode().toLowerCase(), externalVisualDom.getExternalComponentPath());
                    /// 表单service文件路径
                    String externalFormServicePath = FileUtility.combine(formServicePath, json.getModule().getCode().toLowerCase(), "externalcomponents", externalVisualDom.getExternalComponentPath());
                    String metaFileName = externalVisualDom.getJson().getModule().getCode() + ".frm";
                    GspMetadata formMetadata = MetadataUtility.getInstance().getMetadataWithEnvironment(() -> {
                        MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
                        getterMetadataInfo.setId(externalVisualDom.getExternalComponentUri());
                        getterMetadataInfo.setMetadataType(MetadataTypeEnum.Frm);
                        return getterMetadataInfo;
                    }, () -> {
                        MetadataGetterParameter.GetterMetadataInfo getterMetadataInfo = new MetadataGetterParameter.GetterMetadataInfo();
                        getterMetadataInfo.setMetadataType(MetadataTypeEnum.Frm);
                        getterMetadataInfo.setCode(json.getModule().getCode());
                        getterMetadataInfo.setName(json.getModule().getName());
                        return getterMetadataInfo;
                    }, this.getExecuteEnvironment(), this.isUpgradeTool());


                    try {
                        resolveFormMetadataWithVisualDom(formMetadata, externalVisualDom.getJson(),
                                externalFormBasePath, externalVisualDom.getRelativePath(), externalFormServicePath, projectPath,
                                projectCmpList, webdevpath, expressionManifest);
                    } catch (Exception e) {
                        WebLogger.Instance.error(e);
                    }
                }
            }
        }
    }

    private String getExternalContainerId(HashMap<String, Object> item) {
        String externalComponentId = (String) item.get("id");
        String externalComponentType = (String) item.get("type");
        String containerId = (String) item.get("containerId");
        String strContainerId;
        if (externalComponentType.equals("ModalContainer")) {
            strContainerId = !StringUtility.isNullOrEmpty(containerId) ? containerId : externalComponentId;
        } else {
            strContainerId = externalComponentId;
        }

        return strContainerId;
    }


    private void resolveFormMetadataWithVisualDom(GspMetadata formMetadata, FormDOM formDom,
                                                  String targetStorageBasePath, String relativePath, String formServiceBasePath,
                                                  String projectPath, HashMap<String, CommandsAnalysis.WebComponentMetadataAndExtra> projectCmpList,
                                                  String webDevPath, ExpressionManifest expressionManifest) throws Exception {
        String formMetadataName = formMetadata.getHeader().getFileName();
        if ("RTC".equals(formMetadata.getExtendProperty())) {
            formMetadataName = formMetadata.getHeader().getCode() + ".frm";

        }
//        临时注释，方便调试
        // Step 2: Resolve StateMachine, and Save StateMachine
        StateMachineAnalysis stateMachineAnalysis = new StateMachineAnalysis(this.getExecuteEnvironment(), this.isUpgradeTool());
        stateMachineAnalysis.resolveStateMachine(formDom, formMetadataName, targetStorageBasePath, relativePath);
//
//        // Step 3: Resolve Command
        CommandsAnalysis commandsAnalysis = new CommandsAnalysis(this.getExecuteEnvironment(), this.isUpgradeTool());
        commandsAnalysis.resolveCommand(CommandsAnalysis.ResolveCommandParameter.init(formMetadata, formDom),
                formMetadataName, targetStorageBasePath, projectCmpList);
//
//        // Step 4: Resolve eapi
        EapiAnalysis eapiAnalysis = new EapiAnalysis(this.getExecuteEnvironment(), this.isUpgradeTool());
        eapiAnalysis.resolveEapi(formDom, formMetadataName, targetStorageBasePath, formMetadata.getHeader().getNameSpace(), relativePath);
//
//        // Step 5: Resolve command service
        CommandServiceAnalysis.resolveCommandService(formDom, relativePath, formServiceBasePath, webDevPath, this.getExecuteEnvironment(), this.isUpgradeTool());

        // 构造对应的国际化资源项
        //GenerateResourceManager.generateI18nResource(formMetadata, i18nResourceList, i18nResourceKeyPrefix,"en");

        // 递归解析   如果存在子组件 那么需要递归执行子组件
        executeExternalComponent(formDom,
                targetStorageBasePath, formServiceBasePath, projectPath,
                webDevPath, relativePath, projectCmpList, expressionManifest);
    }


}
