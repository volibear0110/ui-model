/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.service;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitengine.JITEngineManager;
import com.inspur.edp.web.jitengine.ProjectCompileContext;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildResponse;
import com.inspur.edp.web.jitruntimebuild.api.service.JitBuildService;
import com.inspur.edp.web.jitruntimebuild.core.builddeploy.BuildDeployManager;
import com.inspur.edp.web.jitruntimebuild.core.buildparametergenerator.JitBuildParameterGenerator;
import com.inspur.edp.web.jitruntimebuild.core.buildparametervalidator.JitBuildParameterValidator;
import com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator.FormJsonFileManager;
import com.inspur.edp.web.jitruntimebuild.core.sourcecodemetadata.SourceCodeMetadataManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * description:运行时表单执行入口
 * 该方法被其他引用 方法名不能更改
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class JitBuildServiceImp implements JitBuildService {

    private static final String CurrentClassName = JitBuildServiceImp.class.getName();

    // 设定执行环境为运行时
    private static final ExecuteEnvironment executeEnvironment = ExecuteEnvironment.Runtime;

    /**
     * jit 运行时编译
     *
     * @param buildParameter
     * @return
     */
    @Override
    public JitBuildResponse jitBuild(JitBuildParameter buildParameter) {

        WebLogger.Instance.info("begin execute runtime build", CurrentClassName);
        // 执行入参验证
        JitBuildResponse buildResponse = JitBuildParameterValidator.validate(buildParameter);
        if (!buildResponse.isSuccess()) {
            WebLogger.Instance.error(buildResponse.getErrorMessage(), CurrentClassName);
            return buildResponse;
        }

        // 对编译参数进行调整  设置默认值
        if (!buildParameter.isMobileApprove()) {
            JitBuildParameterGenerator.generate(buildParameter);
            WebLogger.Instance.info("build parameter executed completely", CurrentClassName);
        }

        // 首先删除service文件夹 避免service文件变更导致的编译失败
        String sourceServicePath = buildParameter.getBuildWebDevPath() + "/services/";
        try {
            FileUtility.deleteFolder(sourceServicePath);
            WebLogger.Instance.info("delete service path ,the service path is " + sourceServicePath, CurrentClassName);
        } catch (Exception ignored) {
            WebLogger.Instance.info("delete service path failed ,the service path is " + sourceServicePath + " " + Arrays.toString(ignored.getStackTrace()), CurrentClassName);
        }

        // 生成具体的json文件
        WebLogger.Instance.info("begin generate form json file, the formCode is " + buildParameter.getFormCode(), CurrentClassName);
        FormJsonFileManager.generateSpecificJsonFile(buildParameter, executeEnvironment);
        WebLogger.Instance.info("generate form json file completely, the formCode is " + buildParameter.getFormCode(), CurrentClassName);

        // 执行 脚本文件生成
        WebLogger.Instance.info("begine generate source code", CurrentClassName);
        ProjectCompileContext projectCompileContext = generateProjectCompileContext(buildParameter);
        try {
            JITEngineManager.compileProject(projectCompileContext);
        } catch (RuntimeException e) {
            WebLogger.Instance.error(e.getMessage(), CurrentClassName);
            buildResponse = new JitBuildResponse();
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage(e.getMessage());
            return buildResponse;
        }


        WebLogger.Instance.info("begin compile project, the formCode is " + buildParameter.getFormCode(), CurrentClassName);


        // 拷贝service文件到具体的文件夹路径
        String targetServiceProductPath = buildParameter.getBuildAppPath() + "/projects" + "/" + buildParameter.getBoCode().toLowerCase() + "/src/app";
        if (buildParameter.isMobileApp()) {
            targetServiceProductPath = buildParameter.getBuildAppPath() + "/src/apps";
        }
        FileUtility.copyFolder(sourceServicePath, targetServiceProductPath);


        ///解析自定义web构件
        SourceCodeMetadataManager.analysisSourceCodeMetadata(buildParameter);

        // 脚本编译
        // 临时注释编译
        String buildErrorMessage = null;

        try {
            buildErrorMessage = JITEngineManager.buildFrontendProjectWithRuntime(buildParameter.getBuildAppPath(), buildParameter.isInUpgradeTool(), buildParameter);
        } catch (RuntimeException ex) {
            // 捕获编译异常   应该指定特定的异常 方便进行异常信息提示
            buildErrorMessage = ex.getMessage();
            WebLogger.Instance.error(ex.getMessage() + Arrays.toString(ex.getStackTrace()));
        }


        // 执行工程部署 且执行文件脚本发布
        if (StringUtility.isNullOrEmpty(buildErrorMessage) || StringUtility.isBlank(buildErrorMessage.trim())) {
            BuildDeployManager.deployForm(buildParameter);
        } else {
            WebLogger.Instance.error("build failed,can not execute script deploy", CurrentClassName);
            buildResponse = new JitBuildResponse();
            buildResponse.setSuccess(false);
            buildResponse.setErrorMessage(buildErrorMessage);
            return buildResponse;
        }

        if (buildParameter.isBabelCompile()) {
            // 如果是babel编译 那么返回对应的url参数
            String babelUrl = FileUtility.getPlatformIndependentPath("/" + buildParameter.getServiceUnitPath() + "/" + "web" + "/" + buildParameter.getBoCode() + "forbabelruntime" + "/" +
                    buildParameter.getFormName() +
                    com.inspur.edp.web.jitruntimebuild.core.utility.JitBuildUtility.getExtraPath(buildParameter.getExtraFormPath()) + "/index.html#/" + buildParameter.getFormName());
            Map<String, String> mapResult = new HashMap<>();
            mapResult.put("babelUrl", babelUrl);
        }

        // 构造成功地返回值
        buildResponse = new JitBuildResponse();

        return buildResponse;
    }

    /**
     * 构造对应的编译上下文参数
     *
     * @param buildParameter
     * @return
     */
    private static ProjectCompileContext generateProjectCompileContext(JitBuildParameter buildParameter) {
        ProjectCompileContext projectCompileContext = new ProjectCompileContext(
                buildParameter.getBoCode(),
                buildParameter.getBuildNodeModulePath(),
                "pc", TerminalType.PC.getFrameworkType(),
                buildParameter.getBuildWebDevPath(),
                buildParameter.getBuildAppPath(), null,
                buildParameter.getServiceUnitPath(), executeEnvironment);
        if (buildParameter.isMobileApp()) {
            projectCompileContext.setFormType("mobile");
            projectCompileContext.setFrameworkType(TerminalType.MOBILE.getFrameworkType());
        }
        // 设置运行时定制维度信息
        projectCompileContext.extraFormPath = buildParameter.getExtraFormPathStr();
        projectCompileContext.setGenerateViewModel(!buildParameter.isBabelCompile());
        // 运行时定制设置生成前删除代码目录
        projectCompileContext.setDeleteSourceCodeBeforeGenerate(true);
        return projectCompileContext;
    }

}
