/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.buildparametergenerator;

import com.inspur.edp.web.common.JITEngineConstants;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.core.constant.FormJsonFileSuffixConstant;
import com.inspur.edp.web.jitruntimebuild.core.utility.JitBuildUtility;

import java.util.ArrayList;

/**
 * jit 编译参数构造
 */
public class JitBuildParameterGenerator {

    /**
     * 构造jit编译参数。 根据其中的参数值，来进行补充
     *
     * @param buildParameter 编译参数
     * @return
     */
    public static void generate(JitBuildParameter buildParameter) {

        String currentWorkPath = FileUtility.getCurrentWorkPath(buildParameter.isInUpgradeTool());

        // 如果未设定编译路径 那么设定当前程序运行环境所在根目录
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildProjectPath())) {
            // 工当前运行环境目录下增加runtimeprojects 文件目录
            String strCurrentWorkPath = currentWorkPath + (FileUtility.DIRECTORY_SEPARATOR_CHAR + FormJsonFileSuffixConstant.defaultProjectPathName).toLowerCase();
            buildParameter.setBuildProjectPath(strCurrentWorkPath);
        }

        // 如果node_modules 文件路径为空 移动到projects文件目录下 不再放置于rtc文件目录下
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildNodeModulePath())) {

            // 工当前运行环境目录下增加runtime/projects 文件目录  不再放置于rtc目录下
            String strCurrentWorkPath = currentWorkPath + (FileUtility.DIRECTORY_SEPARATOR_CHAR + FormJsonFileSuffixConstant.defaultProjectPathNameWithoutRtc).toLowerCase();
            String node_modulesPath = strCurrentWorkPath + FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getNode_ModulesName();
            buildParameter.setBuildNodeModulePath(node_modulesPath);
        }

        // 如果boCode为空 那么采用formCode
        if (StringUtility.isNullOrEmpty(buildParameter.getBoCode())) {
            buildParameter.setBoCode(buildParameter.getFormCode());
        }
        String extraPath = JitBuildUtility.getExtraPath(buildParameter.getExtraFormPath());
        // 设置维度路径信息
        buildParameter.setExtraFormPathStr(extraPath);
        // 如果构造表单路径为空 那么从参数中读取
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildFormPath())) {

            String defaultBuildFormPath = buildParameter.getBuildProjectPath() + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                    buildParameter.getBoCode() + FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getFormCode()).toLowerCase();
            if (buildParameter.isMobileApp()) {
                defaultBuildFormPath = buildParameter.getBuildProjectPath() + FileUtility.DIRECTORY_SEPARATOR_CHAR + "mobile" + (FileUtility.DIRECTORY_SEPARATOR_CHAR +
                        buildParameter.getBoCode() + FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getFormCode()).toLowerCase();
            }

            String buildFormPath = defaultBuildFormPath + extraPath.toLowerCase();
            buildParameter.setBuildFormPath(buildFormPath);
        }

        // 如果未设定webdev目录 那么构造对应的目录
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildWebDevPath())) {
            String buildWebDevPath = buildParameter.getBuildFormPath() + (FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getWebDevPathName()).toLowerCase();
            buildParameter.setBuildWebDevPath(buildWebDevPath);
        }

        // 如果未设置formName，那么以formCode为准
        if (StringUtility.isNullOrEmpty(buildParameter.getFormName())) {
            buildParameter.setFormName(buildParameter.getFormCode().toLowerCase());
        }

        // 如果编译app路径为空 那么构造
        if (StringUtility.isNullOrEmpty(buildParameter.getBuildAppPath())) {
            String buildAppPath = buildParameter.getBuildFormPath() + (FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getAppPathName()).toLowerCase();
            // 如果采用babel 编译
            if (buildParameter.isBabelCompile()) {
                buildAppPath = buildParameter.getBuildFormPath() + (FileUtility.DIRECTORY_SEPARATOR_CHAR + buildParameter.getBabelRunTimeAppPathName()).toLowerCase();
            }
            buildParameter.setBuildAppPath(buildAppPath);
        }

        if (buildParameter.getBuildRefMetadataList() == null) {
            buildParameter.setBuildRefMetadataList(new ArrayList<>());
        }

        // 如果su路径不为空
        if (!StringUtility.isNullOrEmpty(buildParameter.getServiceUnitPath())) {
            String serviceUnitPath = buildParameter.getServiceUnitPath();
            if (!serviceUnitPath.startsWith(JITEngineConstants.DefaultSUBasePath)) {
                // 在serviceUnit 路径前面追加apps
                serviceUnitPath = JITEngineConstants.DefaultSUBasePath + FileUtility.DIRECTORY_SEPARATOR_CHAR + serviceUnitPath;
                buildParameter.setServiceUnitPath(serviceUnitPath.toLowerCase());
            }
        }

        // 转换成为小写形式
        buildParameter.setFormName(buildParameter.getFormName().toLowerCase());
        buildParameter.setFormCode(buildParameter.getFormCode().toLowerCase());
        buildParameter.setBoCode(buildParameter.getBoCode().toLowerCase());

    }
}
