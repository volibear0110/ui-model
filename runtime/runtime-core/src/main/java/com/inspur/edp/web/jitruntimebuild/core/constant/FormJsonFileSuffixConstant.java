/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.constant;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class FormJsonFileSuffixConstant {

    /**
     * 默认的运行时工程路径名称 转换成小写形式
     */
    public static final String defaultProjectPathName = "web/runtime/projects/rtc";

    /**
     * 默认的运行时工程路径名称 转换成小写形式
     */
    public static final String defaultProjectPathNameWithoutRtc = "web/runtime/projects";

    /**
     * 维度信息为空时，设置的默认维度信息
     */
    public static final String DefaultWeiDuName = "public";
}
