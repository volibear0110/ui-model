/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;


import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class JsonFileGeneratorFactory {
    public static JsonFileGeneratorInterface create(MetadataTypeEnum metadataType, ExecuteEnvironment executeEnvironment, boolean isUpdradeTool) {
        JsonFileGeneratorInterface jsonFileGenerator = null;
        switch (metadataType) {
            case Frm:
                jsonFileGenerator = new FrmJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            case Eapi:
                jsonFileGenerator = new EapiJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            case Command:
                jsonFileGenerator = new CommandJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            case Resource:
                jsonFileGenerator = new ResourceJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            case StateMachine:
                jsonFileGenerator = new StateMachineJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            case Route:
                jsonFileGenerator = new FormRouteJsonFileGenerator(executeEnvironment, isUpdradeTool);
                break;
            default:
                WebLogger.Instance.error("unknown meta data type " + metadataType);
                break;
        }
        return jsonFileGenerator;
    }
}
