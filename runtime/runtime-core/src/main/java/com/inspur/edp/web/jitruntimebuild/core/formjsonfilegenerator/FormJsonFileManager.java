/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * description: 表单json文件构造
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
public class FormJsonFileManager {

    /**
     * 根据参数构造具体的表单json文件
     *
     * @param buildParameter
     */
    public static void generateSpecificJsonFile(JitBuildParameter buildParameter, ExecuteEnvironment executeEnvironment) {
        String webDevPath = buildParameter.getBuildWebDevPath();
        Map<String,String> formCodeNameMap = new HashMap<>();
        formCodeNameMap.put(buildParameter.getFormCode(),buildParameter.getFormName());

        if (ListUtility.isNotEmpty(buildParameter.getBuildRefMetadataList())) {
            buildParameter.getBuildRefMetadataList().forEach(jitBuildRefMetadata -> {
                if (!StringUtility.isNullOrEmpty(jitBuildRefMetadata.getContent())) {
                    MetadataTypeEnum metadataTypeEnum = jitBuildRefMetadata.getMetadataType();
                    String strBasePath = webDevPath;
                    // 针对移动状态机的定义，如果传递的元数据包含的自定义的名称 那么使用自定义的名称  否则使用表单code
                    String formCode = buildParameter.getFormCode();
                    if(StringUtils.isNotBlank(jitBuildRefMetadata.getFormCode())){
                        formCode = jitBuildRefMetadata.getFormCode();
                        String formName = jitBuildRefMetadata.getFormName();
                        if(!formCodeNameMap.containsKey(formCode)){
                            formCodeNameMap.put(formCode,formName);
                        }
                    }

                    // 针对命令构件  主表单需要放置于对应的moduleCode目录下
                    if (metadataTypeEnum == MetadataTypeEnum.Command) {
                        AtomicReference<String> formModuleCode = FormModuleWithBuildParameterManager.getFormModuleCodeAtomicReference(buildParameter,formCode);
                        if (formModuleCode != null && formModuleCode.get() != null) {
                            strBasePath = webDevPath + "/services/" + formModuleCode.get().toLowerCase() + "/services/";
                        } else {
                            WebLogger.Instance.error("can not get form module content,please check!", FormJsonFileManager.class.getName());
                            return;
                        }
                    }
                    JsonFileGeneratorInterface jsonFileGenerator = JsonFileGeneratorFactory.create(jitBuildRefMetadata.getMetadataType(), executeEnvironment, buildParameter.isInUpgradeTool());

                    jsonFileGenerator.generate(strBasePath, formCode.toLowerCase(), jitBuildRefMetadata.getContent());

                }
            });
        }

        // 生成页面流文件
        FormRouteJsonFileGenerator routeJsonFileGenerator = new FormRouteJsonFileGenerator(executeEnvironment, buildParameter.isInUpgradeTool());
        routeJsonFileGenerator.generate(webDevPath, buildParameter.getBoCode().toLowerCase(), formCodeNameMap, null, buildParameter);
    }


}
