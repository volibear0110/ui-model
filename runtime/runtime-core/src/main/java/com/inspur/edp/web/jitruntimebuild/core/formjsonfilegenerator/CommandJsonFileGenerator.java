/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.formjsonfilegenerator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.web.common.environment.ExecuteEnvironment;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.StringUtility;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/04/29
 */
class CommandJsonFileGenerator extends AbstractFormJsonFileGenerator implements JsonFileGeneratorInterface {

      CommandJsonFileGenerator(ExecuteEnvironment executeEnvironment, boolean isUpgradeTool) {
        super(executeEnvironment, isUpgradeTool);
    }


    /**
     * 命令文件生成
     *
     * @param basePath
     * @param formName
     * @param content
     */
    @Override
    public void generate(String basePath, String formName, String content) {
        if (StringUtility.isNullOrEmpty(content)) {
            return;
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(content).get("serviceList");
            if (jsonNode != null) {
                jsonNode.forEach(node -> {
                    try {
                        String tsFileCode = node.at("/Content/Code").textValue() + ".ts";

                        JsonNode sourceCodeNode = mapper.readTree(node.get("ExtendProperty").textValue()).get("sourceCode");
                        if (sourceCodeNode != null) {
                            String tsFileContent = sourceCodeNode.textValue();
                            this.writeFile(basePath, tsFileCode.toLowerCase(), tsFileContent);
                        }
                    } catch (JsonProcessingException e) {
                        WebLogger.Instance.error(e);
                    }

                });
            }
        } catch (JsonProcessingException e) {
            WebLogger.Instance.error(e);
        }
    }
}
