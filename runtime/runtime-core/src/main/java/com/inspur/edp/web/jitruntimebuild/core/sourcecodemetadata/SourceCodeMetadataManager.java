/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.core.sourcecodemetadata;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.utility.Base64Utility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitengine.sourcecode.SourceCodeInFormManager;
import com.inspur.edp.web.jitengine.sourcecode.SourceCodeInFormRef;
import com.inspur.edp.web.jitruntimebuild.api.constant.I18nMsgConstant;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildParameter;
import com.inspur.edp.web.jitruntimebuild.api.entity.JitBuildRefMetadata;
import com.inspur.edp.web.sourcecode.metadata.entity.SourceCodeItemEntity;
import com.inspur.edp.web.sourcecode.metadata.entity.SourceCodeMetadataEntity;

import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * description: 运行时定制自定义web构件
 *
 * @author Noah Guo
 * @date 2020/06/16
 */
public class SourceCodeMetadataManager {


    /**
     * 解析自定义web构件元数据
     *
     * @param jitBuildParameter
     */
    public static void analysisSourceCodeMetadata(JitBuildParameter jitBuildParameter) {

        List<SourceCodeInFormRef> sourceCodeInFormRefList = SourceCodeMetadataManager.getSourceCodeRefInForm(jitBuildParameter);

        List<SourceCodeMetadataEntity> sourceCodeMetadataEntityList = SourceCodeInFormManager.getSourceCodeMetadataEntities(sourceCodeInFormRefList, jitBuildParameter.isInUpgradeTool());

        // 解压缩内容到指定的文件路径
        unZipSourceMetadataEntity(sourceCodeMetadataEntityList, jitBuildParameter);
    }

    /**
     * 解析自定义web构件元数据信息
     *
     * @param sourceCodeMetadataEntityList
     */
    private static void unZipSourceMetadataEntity(List<SourceCodeMetadataEntity> sourceCodeMetadataEntityList, JitBuildParameter buildParameter) {
        if (sourceCodeMetadataEntityList != null && sourceCodeMetadataEntityList.size() > 0) {

            sourceCodeMetadataEntityList.forEach((sourceCodeMetadataEntityItem) -> {
                List<SourceCodeItemEntity> items = sourceCodeMetadataEntityItem.getItems();
                if (items != null && items.size() > 0) {

                    items.forEach(item -> {

                        try {
                            String strFileContent = Base64Utility.decode(item.getSourceFileContent());
                            // 如果文件内容为空 也进行文件内容的生成操作
                            String strWriteFilePath = getWriteFilePath(item, buildParameter);
                            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_RUNTIME_BUILD_MSG_0001, strWriteFilePath), SourceCodeMetadataManager.class.getName());
                            FileUtility.writeFile(strWriteFilePath, strFileContent);
                        } catch (UnsupportedEncodingException e) {
                            WebLogger.Instance.error(e);
                        }

                    });
                }
            });
        }
    }

    private static String getWriteFilePath(SourceCodeItemEntity sourceCodeItemEntity, JitBuildParameter buildParameter) {

        Path writeFilePath = Paths.get(buildParameter.getBuildAppPath(), generateSourcePath(sourceCodeItemEntity, buildParameter));
        return writeFilePath.toString();
    }

    private static String generateSourcePath(SourceCodeItemEntity sourceCodeItemEntity, JitBuildParameter buildParameter) {
        String strSourcePath = StringUtility.isNullOrEmpty(sourceCodeItemEntity.getTargetPath()) ? sourceCodeItemEntity.getSourcePath() : sourceCodeItemEntity.getTargetPath();
        strSourcePath = strSourcePath.replace("{{projectname}}", buildParameter.getBoCode());
        return strSourcePath.toLowerCase();
    }


    /**
     * 从表单元数据中读取自定义web构件引用
     *
     * @param buildParameter
     * @return
     */
    public static List<SourceCodeInFormRef> getSourceCodeRefInForm(JitBuildParameter buildParameter) {

        List<SourceCodeInFormRef> formRefList = new ArrayList<>();

        List<JitBuildRefMetadata> refMetadataList = buildParameter.getBuildRefMetadataList();
        if (refMetadataList != null && refMetadataList.size() > 0) {
            refMetadataList.forEach((metadata) -> {
                MetadataTypeEnum metadataTypeEnum = metadata.getMetadataType();
                if (metadataTypeEnum == MetadataTypeEnum.Frm) {
                    String strFormMetadataContent = metadata.getContent();
                    SourceCodeInFormManager.getSourceCodeWithFormMetadataContent(formRefList, strFormMetadataContent);
                }
            });
        }
        return formRefList;
    }


}
