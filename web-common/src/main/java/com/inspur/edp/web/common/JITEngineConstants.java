/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common;

/**
 * JITEngine执行常量参数
 *
 * @author guozhiqi
 */
public class JITEngineConstants {
    private JITEngineConstants() {
    }

    /**
     * 工程路由文件后缀格式
     */
    public static final String ProjectRouteFileExtension = ".route.json";

    /**
     * 当前执行编译的NG版本  当前为Ng7 不可更改
     */
    public static final String NgVersion = "ng7";

    /**
     * 当前执行UI类库Kendo  不可更改
     */
    public static final String UiLibrary = "kendo";

    /**
     * Node_Modules文件目录
     */
    public static final String Node_ModulesPathName = "node_modules";


    /**
     * jit 执行命令js文件相对于server nodejs路径的相对路径
     */
    public static final String JitCommandJsRelativePath = "node_modules/@farris/jit-engine/bin/index.js";

    /**
     * ng 执行命令相对于server的nodejs路径的相对路径
     */
    public static final String NgCommandRelativePath = "node_modules/@angular/cli/bin/ng";


    public static final String ServerScriptsPath = "serverscripts";

    public static final String TscCommandRelativePath = "typescript/bin/tsc";

    public static final String RollupCommandRelativePath = "rollup/bin/rollup";

    /**
     * 表单文件后缀
     */
    public static final String FrmJsonFile = ".json";

    /**
     * 命令json文件后缀  .command.json
     */
    public static final String CommandJsonFile = ".command.json";

    /**
     * eapi json文件后缀.eapi.json
     */
    public static final String EapiJsonFile = ".eapi.json";

    /**
     * 资源文件后缀 .resource.json
     */
    public static final String ResourceJsonFile = ".resource.json";

    /**
     * 状态机文件后缀  .sm.json
     */
    public static final String StateMachineJsonFile = ".sm.json";

    /**
     * 表单文件后缀 .frm
     */
    public static final String FrmSuffix = ".frm";

    /**
     * su运行的根目录 apps
     */
    public static final String DefaultSUBasePath = "apps";

    /**
     * 生成脚本文件目录名称 dist-rollup
     */
    public static final String DistRollupPathName = "dist-rollup";

    /**
     * 部署目录中web
     */
    public static final String DeployWebPathName = "web";

    /**
     * webdev 路径
     */
    public static final String WebDevPathName = "webdev";
}
