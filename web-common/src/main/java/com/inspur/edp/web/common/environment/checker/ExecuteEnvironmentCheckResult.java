/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.environment.checker;

/**
 * @Title: ExecuteEnvironmentCheckResult 运行环境检测结果值
 * @Description: com.inspur.edp.web.common.environment.checker
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/29 15:23
 */
public class ExecuteEnvironmentCheckResult {
    /**
     * 是否检测成功标识
     */
    private boolean success = true;

    /**
     * 如果检测失败 对应的提示信息
     */
    private String errorMessage = "";

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static ExecuteEnvironmentCheckResult getInstance() {
        return new ExecuteEnvironmentCheckResult();
    }
}
