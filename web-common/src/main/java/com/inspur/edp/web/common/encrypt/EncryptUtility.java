/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.encrypt;

import com.inspur.edp.web.common.constant.WebCommonExceptionConstant;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.logger.WebLogger;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * description:des 加解密
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public class EncryptUtility {
    public static final String MD5 = "MD5";
    public static final String SHA1 = "SHA1";
    public static final String HmacMD5 = "HmacMD5";
    public static final String HmacSHA1 = "HmacSHA1";
    public static final String DES = "DES";
    public static final String AES = "AES";

    /**
     * 编码格式；默认使用uft-8
     */
    public String charset = "utf-8";

    private EncryptUtility() {
        //单例
    }

    private volatile static EncryptUtility _instance = null;
    private static final Object _lock = new Object();

    /**
     * 不使用内部静态类形式创建单例  避免启动过程中就创建实例，影响启动速度
     *
     * @return
     */
    public static EncryptUtility getInstance() {
        if (_instance == null) {
            synchronized (_lock) {
                if (_instance == null) {
                    _instance = new EncryptUtility();
                }
            }
        }
        return _instance;
    }

    /**
     * 使用Base64进行加密
     *
     * @param res 密文
     * @return
     */
    public String Base64Encode(String res) {
        if (res == null || res.trim().isEmpty()) {
            return "";
        }
        String result = "";
        try {
            result = Base64.encode(res.getBytes(charset));
        } catch (Exception e) {
            throw new WebCustomException(WebCommonExceptionConstant.WEB_COMMON_ERROR_0001, new String[]{res}, e);
        }
        return result;
    }

    /**
     * 使用Base64进行解密
     *
     * @param res
     * @return
     */
    public String Base64Decode(String res) {
        if (res == null || res.trim().isEmpty()) {
            return "";
        }
        try {
            return new String(Base64.decode(res), charset);
        } catch (Exception e) {
            throw new WebCustomException(WebCommonExceptionConstant.WEB_COMMON_ERROR_0002, e);
        }
    }

    /**
     * 判断字符串是否经过base64编码
     *
     * @param source
     * @return
     */
    public boolean isBase64(String source) {
        if (source == null || source.trim().length() == 0) {
            return false;
        } else {
            if (source.length() % 4 != 0) {
                return false;
            }

            char[] strChars = source.toCharArray();
            for (char c : strChars) {
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')
                        || c == '+' || c == '/' || c == '=') {
                } else {
                    return false;
                }
            }
            return true;
        }
    }
}
