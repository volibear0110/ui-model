/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.constant;

/**
 * 前端工程常量定义
 *
 * @author guozhiqi
 */
public final class FrontendProjectConstant {
    private FrontendProjectConstant() {
    }
    ///工程常量

    /**
     * 前端工程类型
     */
    public static final String FRONTEND_PROJECT_TYPE = "frontend";

    /**
     * 非前端工程类型
     */
    public static final String NON_FRONTEND_PROJECT_TYPE = "non-frontend";

    /**
     * 前端工程PC表单编译目录
     */
    public static final String FRONTEND_PROJECT_COMPILE_PATH = "src";

    /**
     * 前端工程PC表单生成目录（Babel）
     */
    public static final String PROJECT_GENERATE_PATH_FOR_BABEL = "appforbabel";

    /**
     * appforjiexi 常量
     */
    public static final String PROJECT_GENERATE_PATH_FOR_Dynamic = "appfordynamic";

    public static final String PROJECT_GENDER_PATH_FOR_DYNAMIC_FORM = "dynamicform";

    /**
     * 命令服务生成目录
     */
    public static final String COMMAND_SERVICES_PRODUCT_PATH = "services";

    /**
     * htmltemplate生成目录
     */
    public static final String HtmlTemplate_PRODUCT_PATH = "htmltemplate";

    /**
     * 多语资源存放目录
     */
    public static final String I18N_SRESOURCE_PATH = "i18n";

    /**
     * 工程发布目录
     */
    public static final String PROJECT_PUBLISH_PATH = "publish";

    public static final String LINUX_ENVIRONMENT_BUILD = "https://open.inspures.com/openplat/#/doc/md/iGIX%2FiGIX_2312%2Fdevelop%2Flowcode-dev%2Fquickstart%2Fbuilding-development-environment%2FLinuxbuild.md";

    public static final String WIN_ENVIRONMENT_BUILD = "https://open.inspures.com/openplat/#/doc/md/iGIX%2FiGIX_2312%2Fdevelop%2Flowcode-dev%2Fquickstart%2Fbuilding-development-environment%2FWindowsbuild.md";

    public static final String NODE_MODULES_UPDATE_URL = "https://open.inspures.com/openplat/#/doc/md/iGIX%2FiGIX_2312%2Fdevelop%2Flowcode-dev%2Fdev-sub%2Fform-dev%2Fnpm-install%2Fnpm%E5%8C%85%E5%9C%A8%E7%BA%BF%E5%AE%89%E8%A3%85.md";
}
