/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import lombok.Data;

/**
 * 继承 MetadataUri  主要目的是重写其中的输出方法
 *
 * @author guozhiqi
 */
@Data
public class WebMetadataUri extends MetadataURI {
    /**
     * 元数据找不到自定义异常 使用此 覆盖父类
     */
    private String customMetadataNotFoundMessage;

    /**
     * 是否使用默认的自定义提示信息
     */
    private boolean useCustomMetadataNotFoundMessage = false;

    /**
     * 元数据关联名称
     */
    private String name;

    public WebMetadataUri(String id, String code, String name, String type, String nameSpace, String customMetadataNotFoundMessage) {
        super(id, code, type, nameSpace);
        this.customMetadataNotFoundMessage = customMetadataNotFoundMessage;
    }

    @Override
    public String getURIDesc() {
        // 如果强制使用自定义的异常提示 或者 code参数为空  且自定义异常提示信息不为空
        // 不使用自定义异常提示
//        if ((this.isUseCustomMetadataNotFoundMessage() || StringUtility.isNullOrEmpty(this.getCode())) && StringUtility.isNotNullOrEmpty(this.getCustomMetadataNotFoundMessage())) {
//            return this.getCustomMetadataNotFoundMessage();
//        }

        return super.getURIDesc();
    }
}
