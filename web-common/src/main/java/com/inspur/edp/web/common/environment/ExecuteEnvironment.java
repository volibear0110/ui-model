/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.environment;

/**
 * 运行环境 位置 分为设计时及运行时、元数据部署工具
 *
 * @author guozhiqi
 */
public enum ExecuteEnvironment {
    /**
     * 枚举名称Design
     */
    Design {
        @Override
        public String getName() {
            return "Design";
        }
    },

    /**
     * 枚举名称Runtime
     */
    Runtime {
        @Override
        public String getName() {
            return "Runtime";
        }
    },

    /**
     * 枚举名称
     */
    UpgradeTool {
        @Override
        public String getName() {
            return "UpgradeTool";
        }
    },
    None {
        /**
         * 未指定具体运行位置
         * @return
         */
        @Override
        public String getName() {
            return "None";
        }
    };

    /**
     * 获取对应的名称描述
     *
     * @return
     */
    public abstract String getName();

}
