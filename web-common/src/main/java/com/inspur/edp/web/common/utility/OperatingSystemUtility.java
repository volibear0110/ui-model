/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

import org.apache.commons.lang3.SystemUtils;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2021/01/12
 */
public class OperatingSystemUtility {
    private OperatingSystemUtility() {
    }

    /**
     * 判断当前运行操作系统 是否是Linux
     *
     * @return
     */
    public static boolean isLinux() {
        return SystemUtils.IS_OS_LINUX;
    }

    /**
     * 是否Mac OS
     * @return
     */
    public static boolean isMac() {
        return SystemUtils.IS_OS_MAC;
    }

    /**
     * 判断当前运行操作系统 是否是windows
     *
     * @return
     */
    public static boolean isWindows() {
        return SystemUtils.IS_OS_WINDOWS;
    }
}
