/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

/**
 * 命令执行结果
 *
 * @author noah
 */
public class CommandExecuteResult {

    /**
     * 是否重新执行命令
     */
    private boolean reExecute = false;

    /**
     * 重新执行命令最大次数
     * 默认为3 次
     */
    private int reExecuteMaxTime = 3;

    /**
     * 重新执行命令 当前执行次数
     * 默认为0
     */
    private int reExecuteCurrentTime = 0;

    public boolean isReExecute() {
        return reExecute;
    }

    public void setReExecute(boolean reExecute) {
        this.reExecute = reExecute;
    }

    public int getReExecuteMaxTime() {
        return reExecuteMaxTime;
    }

    public void setReExecuteMaxTime(int reExecuteMaxTime) {
        this.reExecuteMaxTime = reExecuteMaxTime;
    }

    public int getReExecuteCurrentTime() {
        return reExecuteCurrentTime;
    }

    public void setReExecuteCurrentTime(int reExecuteCurrentTime) {
        this.reExecuteCurrentTime = reExecuteCurrentTime;
    }
}
