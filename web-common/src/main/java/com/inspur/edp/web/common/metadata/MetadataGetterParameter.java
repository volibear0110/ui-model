/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.constant.WebCommonExceptionConstant;
import com.inspur.edp.web.common.customexception.WebCustomException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 元数据获取请求参数
 *
 * @author noah
 */
public class MetadataGetterParameter implements Serializable {

    private static final long serialVersionUID = 523654455L;

    /**
     * 请求参数  源  元数据信息
     */
    @Getter
    @Setter
    private GetterMetadataInfo sourceMetadataInfo;

    /**
     * 请求参数 目标  元数据信息
     */
    @Getter
    @Setter
    private GetterMetadataInfo targetMetadataInfo;


    /**
     * 元数据找不到自定义异常提示   应对的是sourceMetadataCode
     */
    @Getter
    @Setter
    private String targetMetadataNotFoundMessage;

    private MetadataGetterParameter(String targetMetadataId, String spacePath, MetadataTypeEnum metadataType) {
        this();

        this.targetMetadataInfo.setId(targetMetadataId);
        this.targetMetadataInfo.setPath(spacePath);
        this.targetMetadataInfo.setMetadataType(metadataType);
    }

    private MetadataGetterParameter() {
        this.sourceMetadataInfo = new GetterMetadataInfo();
        this.targetMetadataInfo = new GetterMetadataInfo();
    }


    /**
     * 构造对应的参数实例
     *
     * @param targetMetadataId 目标元数据id
     * @param spacePath        目标元数据spacePath
     * @param metadataType     目标元数据的元数据类型
     * @return
     */
    public static MetadataGetterParameter getNewInstance(String targetMetadataId, String spacePath, MetadataTypeEnum metadataType) {
        return new MetadataGetterParameter(targetMetadataId, spacePath, metadataType);
    }

    /**
     * 增加此方法的目的是为了可以直接对请求参数直接赋值
     *
     * @return
     */
    public static MetadataGetterParameter getNewInstance() {
        return new MetadataGetterParameter();
    }

    /**
     * 仅包含目标元数据的id
     *
     * @return
     */
    public WebMetadataUri getTargetMetadataUri() {
        if (this.getTargetMetadataInfo() == null) {
            throw new WebCustomException(WebCommonExceptionConstant.WEB_COMMON_ERROR_0005);
        }
        return new WebMetadataUri(this.getTargetMetadataInfo().getId(),
                this.getTargetMetadataInfo().getCode(),
                this.getTargetMetadataInfo().getName(),
                this.getTargetMetadataInfo().getMetadataType().getCode(),
                this.getTargetMetadataInfo().getNameSpace(),  this.getTargetMetadataNotFoundMessage());
    }

    /**
     * 构造source MetadataUri参数
     *
     * @return
     */
    public WebMetadataUri getSourceMetadataUri() {
        if (this.getSourceMetadataInfo() == null) {
            return null;
        }
        return new WebMetadataUri(this.getSourceMetadataInfo().getId(),
                this.getSourceMetadataInfo().getCode(),
                this.getSourceMetadataInfo().getName(),
                this.getSourceMetadataInfo().getMetadataType().getCode(),
                this.getSourceMetadataInfo().getPath(), null);
    }

    /**
     * 通过设置sourceMetadata 进行参数整体赋值
     *
     * @param gspMetadata
     * @param sourceMetadataType
     */
    public void setSourceMetadata(GspMetadata gspMetadata, MetadataTypeEnum sourceMetadataType) {
        this.setSourceMetadataId(gspMetadata.getHeader().getId());
        this.setSourceMetadataCode(gspMetadata.getHeader().getCode());
        this.setSourceMetadataName(gspMetadata.getHeader().getName());
        this.setSourceMetadataNamespace(gspMetadata.getHeader().getNameSpace());
        this.setSourceMetadataPath(gspMetadata.getRelativePath());
        this.setSourceMetadataType(sourceMetadataType);
    }

    public void setSourceMetadataId(String metadataId) {
        this.sourceMetadataInfo.setId(metadataId);
    }

    public void setSourceMetadataCode(String metadataCode) {
        this.sourceMetadataInfo.setCode(metadataCode);
    }

    public void setSourceMetadataName(String metadataName) {
        this.sourceMetadataInfo.setName(metadataName);
    }

    public void setSourceMetadataType(MetadataTypeEnum metadataType) {
        this.sourceMetadataInfo.setMetadataType(metadataType);
    }

    public void setSourceMetadataPath(String metadataPath) {
        this.sourceMetadataInfo.setPath(metadataPath);
    }

    public void setSourceMetadataNamespace(String metadataNamespace) {
        this.sourceMetadataInfo.setNameSpace(metadataNamespace);
    }


    public void setTargetMetadataCode(String metadataCode) {
        this.targetMetadataInfo.setCode(metadataCode);
    }

    public void setTargetMetadataName(String metadataName) {
        this.targetMetadataInfo.setName(metadataName);
    }


    public void setTargetMetadataNamespace(String metadataNamespace) {
        this.targetMetadataInfo.setNameSpace(metadataNamespace);
    }

    /**
     * 参数中元数据信息
     */
    @Data
    public static class GetterMetadataInfo {
        /**
         * 请求参数中元数据id
         */
        private String id;
        /**
         * 请求参数中元数据code
         */
        private String code;
        /**
         * 请求参数中元数据name
         */
        private String name;
        /**
         * 请求参数元数据类型
         */
        private MetadataTypeEnum metadataType = MetadataTypeEnum.Frm;
        /**
         * 请求参数中元数据路径
         */
        private String path;
        /**
         * 请求参数中元数据namespace
         */
        private String nameSpace;
    }

}
