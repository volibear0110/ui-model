/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * rest  api 返回值格式
 *
 * @author noah
 */
@Data
public class ResultMessage<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 返回代码code
     */
    @JsonProperty("code")
    private Integer code;

    /**
     * 是否执行成功标识
     */
    @JsonProperty("success")
    private boolean success = true;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private T data;

    ResultMessage(Integer code, String message) {
        this(code, message, null);
    }

    ResultMessage(boolean success, String message) {
        this(success, message, null);
    }

    ResultMessage(boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.code = success ? 1 : 0;
        this.data = data;
    }

    ResultMessage(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.success = this.code == 1;
    }
}
