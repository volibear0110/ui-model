/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.web.common.utility.StringUtility;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.Optional;

/**
 * 元数据工程信息utility
 *
 * @author guozhiqi
 */
public class MetadataProjectUtility {
    /**
     * 根据元数据相对路径获取工程信息
     *
     * @param metadataRelativePath
     * @return
     */
    public static Optional<MetadataProject> getMetadataProject(String metadataRelativePath) {
        if (StringUtility.isNullOrEmpty(metadataRelativePath)) {
            return Optional.empty();
        }

        return Optional.ofNullable(MetadataProjectServiceInstance.get().getMetadataProjInfo(metadataRelativePath));
    }

    private static class MetadataProjectServiceInstance {
        public static MetadataProjectService get() {
            return SpringBeanUtils.getBean(MetadataProjectService.class);
        }
    }

}
