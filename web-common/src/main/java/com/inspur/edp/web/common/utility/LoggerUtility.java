/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

import com.inspur.edp.web.common.logger.WebLogger;

/**
 * 日志输出service
 */
public class LoggerUtility {

    private LoggerUtility() {
    }

    /**
     * 日志输出
     *
     * @param message
     * @param level
     */
    public static void log(String message, LoggerLevelEnum level) {
        log(message, level, true);
    }

    /**
     * 日志输出
     *
     * @param message
     * @param level
     */
    public static void log(String message, LoggerLevelEnum level, boolean includeMessageHeader) {
        if (!StringUtility.isNullOrEmpty(message)) {
            switch (level) {
                case Info:
                    logInfo(message, includeMessageHeader);
                    break;
                case Error:
                    logError(message, includeMessageHeader);
                    break;
                default:
                    logDefault(message, includeMessageHeader);
                    break;
            }
        }
    }

    public static void log(String message, LoggerLevelEnum level, boolean printToBrowser, boolean includeMessageHeader) {
        log(message, level, includeMessageHeader);
    }

    public static void logToBrowser(String message, LoggerLevelEnum level) {
        logToBrowser(message, level, true);
    }

    public static void logToBrowser(String message, LoggerLevelEnum level, boolean includeMessageHeader) {
        log(message, level, true, includeMessageHeader);
    }

    /**
     * 默认的数据输出
     *
     * @param message
     */
    private static void logDefault(String message) {
        logDefault(message, true);
    }

    /**
     * 默认的数据输出
     *
     * @param message
     */
    private static void logDefault(String message, boolean includeMessageHeader) {
        WebLogger.Instance.info(message, LoggerUtility.class.getName());
    }


    /**
     * Error 级别的输出输出
     *
     * @param message
     */
    private static void logError(String message) {
        logError(message, true);
    }

    /**
     * Error 级别的输出输出
     *
     * @param message
     */
    private static void logError(String message, boolean includeMessageHeader) {
        WebLogger.Instance.error(message, LoggerUtility.class.getName());
    }

    /**
     * Info 级别的消息输出
     *
     * @param message
     */
    private static void logInfo(String message, boolean includeMessageHeader) {
        if (includeMessageHeader) {
            WebLogger.Instance.info("Info:" + message, LoggerUtility.class.getName());
        } else {
            WebLogger.Instance.info(message, LoggerUtility.class.getName());
        }
    }

}
