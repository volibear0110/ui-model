/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.entity;

/**
 * nodejs常用命令
 *
 * @author guozhiqi
 */
public enum NodeJsCommandEnum {
    /**
     * 执行命令的node命令
     */
    Node {
        /**
         * 获取对应的命令参数值 node
         * @return
         */
        @Override
        public String commandName() {
            return "node";
        }
    },
    /**
     * 执行npm包的npm命令
     */
    Npm {
        /**
         * 获取对应的命令参数值 npm
         * @return
         */
        @Override
        public String commandName() {
            return "npm";
        }
    },
    /**
     * 可以执行npm command的npx命令
     */
    Npx {
        /**
         * 获取对应的命令参数值  npx
         * @return
         */
        @Override
        public String commandName() {
            return "npx";
        }
    };

    /**
     * 获取对应的命令参数值
     * @return
     */
    public abstract String commandName();


}
