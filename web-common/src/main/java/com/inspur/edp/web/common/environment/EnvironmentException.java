/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.environment;

import com.inspur.edp.web.common.constant.WebCommonI18nMsgConstant;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 环境检查自定义异常
 * 用以进行自定义异常捕获
 *
 * @author guozhiqi
 */
public class EnvironmentException extends RuntimeException {



    /**
     * 级别  主要用于在客户端进行展现
     */
    private ExceptionLevel level = ExceptionLevel.Error;

    public String getLevel() {
        return this.level.name().toLowerCase();
    }

    public void setLevel(ExceptionLevel exceptionLevel) {
        this.level = exceptionLevel;
    }

    /**
     * 用于界面展现的title标题展示
     */
    private String title;

    public EnvironmentException() {
        this.title = ResourceLocalizeUtil.getString(WebCommonI18nMsgConstant.WEB_COMMON_MSG_0001);
    }

    public String getTitle() {
        ResourceLocalizeUtil.getString(this.title);
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public EnvironmentException(String exceptionMessage, String title, ExceptionLevel exceptionLevel) {
        super(exceptionMessage);
        this.title = title;
        this.level = exceptionLevel;
    }

}
