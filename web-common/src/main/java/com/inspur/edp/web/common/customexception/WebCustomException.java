/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.customexception;

import com.inspur.edp.web.common.constant.WebCommonExceptionConstant;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * web 自定义异常 对界面的提示异常，无需捕获其堆栈
 *
 * @author guozhiqi
 */
public class WebCustomException extends CAFRuntimeException {

    /**
     * 自定义异常定义 设置默认的日志级别为：Error
     *
     * @param message
     */
    public WebCustomException(String message) {
        this(message, new String[]{});
    }

    public WebCustomException(Exception innerException) {
        super(WebCommonExceptionConstant.SU, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, null, null, innerException, ExceptionLevel.Error);
    }

    /**
     * 自定义异常定义 包含message和内部异常
     */
    public WebCustomException(String exceptionCode, Exception innerException) {
        super(WebCommonExceptionConstant.SU, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, exceptionCode, null, innerException, ExceptionLevel.Error);
    }

    public WebCustomException(String message, Exception innerException, ExceptionLevel exceptionLevel) {
        this("", "", message, innerException, exceptionLevel, true);
    }

    /**
     * 自定义异常类   构造的异常为业务异常
     *
     * @param message 异常信息
     * @param level   日志级别
     */
    public WebCustomException(String message, ExceptionLevel level) {
        this(message, level, true);
    }

    public WebCustomException(String message, ExceptionLevel level, boolean bizException) {
        super("", "", message, null, level, bizException);
    }


    public WebCustomException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, exceptionCode, message, innerException, level, bizException);
    }

    public WebCustomException(String exceptionCode, String[] messageParams) {
        super(WebCommonExceptionConstant.SU, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, exceptionCode, messageParams, null, ExceptionLevel.Error);
    }

    public WebCustomException(String exceptionCode, String[] messageParams, ExceptionLevel level) {
        super(WebCommonExceptionConstant.SU, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, exceptionCode, messageParams, null, level);
    }

    public WebCustomException(String exceptionCode, String[] messageParams, Exception e) {
        super(WebCommonExceptionConstant.SU, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, exceptionCode, messageParams, e, ExceptionLevel.Error);
    }



    public WebCustomException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level);
    }

    public WebCustomException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level, bizException);
    }


}
