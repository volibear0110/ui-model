/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.GspProjectService;
import com.inspur.edp.web.common.constant.WebCommonExceptionConstant;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.logger.WebLogger;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * GspProject 工程信息Utility
 *
 * @author guozhiqi
 */
public class GspProjectUtility {
    /**
     * 获取工程信息
     *
     * @param projectPath
     * @return
     */
    public static GspProject getProjectInformation(String projectPath) {
        try {
            return GspProjectServiceInstance.get().getGspProjectInfo(projectPath);
        } catch (Exception ex) {
            WebLogger.Instance.error(ex, GspProjectUtility.class.getName());
            throw new WebCustomException(WebCommonExceptionConstant.WEB_COMMON_ERROR_0004, ExceptionLevel.Error);
        }
    }


    /**
     * 根据工程路径获取工程名称 获取的是其对应的小写形式
     *
     * @param projectPath 工程路径
     * @return 工程名称
     */
    public static String getProjectName(String projectPath) {
        GspProject currentProject = getProjectInformation(projectPath);
        return getProjectName(currentProject);
    }

    /**
     * 获取工程名称的小写形式
     *
     * @param gspProject
     * @return
     */
    public static String getProjectName(GspProject gspProject) {
        return gspProject.getMetadataProjectName().toLowerCase();
    }

    /**
     * 从工程路径中获取su路径，统一使用小写
     *
     * @param projectPath
     * @return
     */
    public String getServiceUnitPath(String projectPath) {
        GspProject currentProject = getProjectInformation(projectPath);
        return currentProject.getSuDeploymentPath().toLowerCase();
    }

    /**
     * GspProjectService
     */
    private static class GspProjectServiceInstance {
        public static GspProjectService get() {
            return SpringBeanUtils.getBean(GspProjectService.class);
        }
    }


}
