/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guozhiqi
 */
public class ListUtility {
    private ListUtility() {
    }

    /**
     * 判断集合是否为空
     *
     * @param sourceList 源集合列表
     * @return
     */
    public static boolean isEmpty(List<?> sourceList) {
        return sourceList == null || sourceList.size() == 0;
    }

    /**
     * 判断集合非空
     *
     * @param sourceList
     * @return
     */
    public static boolean isNotEmpty(List<?> sourceList) {
        return !isEmpty(sourceList);
    }

    /**
     * 获取列表的长度
     *
     * @param sourceList
     * @return
     */
    public static int getLength(List<?> sourceList) {
        if (isEmpty(sourceList)) {
            return 0;
        }
        return sourceList.size();
    }

    /**
     * 将目标集合添加至源集合中
     *
     * @param sourceList 源集合
     * @param targetList 目标集合
     * @param <T>
     */
    public static <T> List<T> add(List<T> sourceList, List<T> targetList) {
        // 如果目标集合为空 那么不进行任何操作
        if (isEmpty(targetList)) {
            return sourceList;
        }
        if (sourceList == null) {
            sourceList = new ArrayList<>();
        }
        sourceList.addAll(targetList);
        return sourceList;
    }

    /**
     * 添加具体项至集合列表中
     *
     * @param sourceList 源集合
     * @param element
     * @param <T>
     */
    public static <T> void add(List<T> sourceList, T element) {
        if (element == null) {
            return;
        }
        if (sourceList == null) {
            sourceList = new ArrayList<>();
        }
        sourceList.add(element);
    }

    /**
     * 判断一个element是否在集合中存在
     *
     * @param sourceList
     * @param element
     * @param <T>
     * @return
     */
    public static <T> boolean contains(List<T> sourceList, T element) {
        if (sourceList == null || sourceList.isEmpty()) {
            return false;
        }
        if (element == null) {
            return false;
        }
        return sourceList.contains(element);
    }
}
