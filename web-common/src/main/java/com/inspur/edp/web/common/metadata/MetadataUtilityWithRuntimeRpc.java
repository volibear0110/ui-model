/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationServerService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 主要应用于升级工具中执行元数据的读取
 * 此种情况下，可能存在对应的服务不存在的情况，因此需要针对性的进行处理
 */
class MetadataUtilityWithRuntimeRpc {
    /**
     * 判断是否存在对应的服务实例
     *
     * @return
     */
    public static boolean hasCustomizationServerServiceInstance() {
        return CustomizationServerServiceInstance.get() != null;
    }

    /**
     * 通过rpc调用获取对应的元数据服务
     * 主要应用于分su场景下
     *
     * @param medaDataId
     * @return
     */
    public static GspMetadata getMetadata(String medaDataId) {
        return CustomizationServerServiceInstance.get().getMetadata(medaDataId);
    }

    /**
     * 运行时元数据服务的rpc
     */
    private static class CustomizationServerServiceInstance {
        public static CustomizationServerService get() {
            return SpringBeanUtils.getBean(CustomizationServerService.class);
        }
    }
}
