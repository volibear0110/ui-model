package com.inspur.edp.web.common.entity;

import lombok.Data;

@Data
public class CommandExecutedResult {
    private String command;

    private String outputInfo;

    private String errorInfo;

    private int exitCode;

}
