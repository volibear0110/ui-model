/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.web.common.utility.StringUtility;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 设计时元数据操作
 *
 * @author guozhiqi
 */
class MetadataUtilityWithDesign {
    /**
     * 获取指定工程路径下的元数据列表
     * 如果传递的过滤元数据类型为空 那么不进行过滤
     *
     * @param spacePath
     * @param metadataTypes
     * @return
     */
    public static List<GspMetadata> getMetadataList(String spacePath, List<String> metadataTypes) {
        if (metadataTypes == null || metadataTypes.isEmpty()) {
            return MetadataServiceInstance.get().getMetadataList(spacePath);
        }
        return MetadataServiceInstance.get().getMetadataList(spacePath, metadataTypes);
    }

    /**
     * 获取指定类型的元数据
     *
     * @param projectPath
     * @param metadataType
     * @return
     */
    public static List<GspMetadata> getMetadataListInProject(String projectPath, String metadataType) {
        ArrayList<String> searchedMetadataTypes = new ArrayList<>();
        searchedMetadataTypes.add(metadataType);
        return MetadataServiceInstance.get().getMetadataList(projectPath, searchedMetadataTypes);
    }

    /**
     * 根据元数据名称及元数据所在路径获取元数据信息。该方法不支持跨工程引用
     *
     * @param metadataFileName
     * @param metadataPath
     * @return
     */
    public static GspMetadata getMetadata(String metadataFileName, String metadataPath) {
        if (StringUtility.isNullOrEmpty(metadataFileName) || StringUtility.isNullOrEmpty(metadataPath)) {
            return null;
        }

        return MetadataServiceInstance.get().loadMetadata(metadataFileName, metadataPath);
    }

    /**
     * 元数据保存  如果元数据已经存在，则先进行删除旧的元数据文件
     *
     * @param metadata
     */
    public static void saveMetadata(GspMetadata metadata) {
        MetadataServiceInstance.get().saveMetadata(metadata, Paths.get(metadata.getRelativePath()).resolve(metadata.getHeader().getFileName()).toString());
    }

    /**
     * 删除对应的元数据物理文件
     *
     * @param metadataPath     元数据所属路径
     * @param metadataFileName 元数据对应的文件名称
     */
    public static void deleteMetadata(String metadataPath, String metadataFileName) {
        if (StringUtility.isNullOrEmpty(metadataFileName)) {
            return;
        }

        MetadataServiceInstance.get().deleteMetadata(metadataPath, metadataFileName);
    }

    /**
     * 创建表单元数据  如果已经存在 不进行创建，否则进行元数据的创建
     *
     * @param metadata 待保存元数据
     */
    public static void createMetadataIfNotExists(GspMetadata metadata) {
        MetadataService metadataService = MetadataServiceInstance.get();
        // 元数据文件不存在时进行创建
        if (!metadataService.isMetadataExist(metadata.getRelativePath(), metadata.getHeader().getFileName())) {
            metadataService.createMetadata(metadata.getRelativePath(), metadata);
        }
    }

    /**
     * 判断指定路径 指定文件名称元数据是否存在
     *
     * @param spacePath
     * @param metadataId
     * @return
     */
    public static boolean isMetaDataExistsWithMetadataIDAndPath(String spacePath, String metadataId) {
        return MetadataServiceInstance.get().isMetadataExistInProject(spacePath, metadataId);
    }

    /**
     * 判断指定工程路径下指定的元数据文件是否存在
     *
     * @param metaDataPath 对应的元数据所在工程路径
     * @param fileName     对应的元数据文件名称
     * @return
     */
    public static boolean isMetaDataExistsWithMetadataPathAndFileName(String metaDataPath, String fileName) {
        return MetadataServiceInstance.get().isMetadataExist(metaDataPath, fileName);
    }

    /**
     * @param metadataGetterParameter
     * @return
     */
    public static GspMetadata getMetadata(MetadataGetterParameter metadataGetterParameter) {
        return getMetadata(metadataGetterParameter.getTargetMetadataUri(), metadataGetterParameter.getSourceMetadataUri(), metadataGetterParameter.getTargetMetadataInfo().getPath());
    }

    public static GspMetadata getMetadata(MetadataURI targetMetadataUri, MetadataURI sourceMetadataUri, String projectPath) {
        return RefCommonServiceInstance.get().getRefMetadata(targetMetadataUri, sourceMetadataUri, projectPath);
    }

    /**
     * 设计时元数据操作
     * 设计时根据元数据路径和名称使用该服务
     */
    private static class MetadataServiceInstance {
        public static MetadataService get() {
            return SpringBeanUtils.getBean(MetadataService.class);
        }
    }

    /**
     * 设计时获取元数据
     * 设计时根据元数据id获取使用该服务
     */
    private static class RefCommonServiceInstance {
        public static RefCommonService get() {
            return SpringBeanUtils.getBean(RefCommonService.class);
        }
    }
}
