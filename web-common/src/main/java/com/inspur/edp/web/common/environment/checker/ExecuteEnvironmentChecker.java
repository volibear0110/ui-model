/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.environment.checker;

import com.inspur.edp.web.common.constant.FrontendProjectConstant;
import com.inspur.edp.web.common.constant.WebCommonI18nMsgConstant;
import com.inspur.edp.web.common.entity.CommandExecutedResult;
import com.inspur.edp.web.common.environment.EnvironmentException;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.CommandLineUtility;
import com.inspur.edp.web.common.utility.OperatingSystemUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @Title: EnvironmentChecker  运行环境检测
 * @Description: com.inspur.edp.web.common.environment
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/29 15:20
 */
public class ExecuteEnvironmentChecker {

    public static String getEndMessage() {
        return ResourceLocalizeUtil.getString(WebCommonI18nMsgConstant.WEB_COMMON_MSG_0002);
    }


    public static String getPrefixMessage() {
        return ResourceLocalizeUtil.getString(WebCommonI18nMsgConstant.WEB_COMMON_MSG_0003);
    }
    /**
     * 默认的弹窗标题
     */
    public static String getDefaultModalTitle() {
        return ResourceLocalizeUtil.getString(WebCommonI18nMsgConstant.WEB_COMMON_MSG_0004);
    }
    /**
     * 默认的弹窗级别
     */
    private static final ExceptionLevel defaultModalLevel = ExceptionLevel.Warning;

    /**
     * 根据不同系统版本返回对应的提示url地址
     *
     * @return
     */
    private static String getKnowledgeUrl() {
        if (OperatingSystemUtility.isLinux()) {
            return FrontendProjectConstant.LINUX_ENVIRONMENT_BUILD;
        }
        return FrontendProjectConstant.WIN_ENVIRONMENT_BUILD;
    }

    /**
     * 检测是否部署node命令
     *
     * @return
     */
    public static ExecuteEnvironmentCheckResult checkGlobalNodeInstalled() {
        return getExecuteEnvironmentCheckResult("node -v", WebCommonI18nMsgConstant.WEB_COMMON_MSG_0005);
    }

    /**
     * 检测是否部署npm命令
     *
     * @return
     */
    public static ExecuteEnvironmentCheckResult checkGlobalNpmInstalled() {
        return getExecuteEnvironmentCheckResult("npm -v", WebCommonI18nMsgConstant.WEB_COMMON_MSG_0005);
    }



    /**
     * 检测是否部署jit命令
     *
     * @return
     */
    public static ExecuteEnvironmentCheckResult checkGlobalJitEngineInstalled() {
        return getExecuteEnvironmentCheckResult("jit --version", WebCommonI18nMsgConstant.WEB_COMMON_MSG_0006);
    }

    /**
     * 检测是否不是ng命令
     *
     * @return
     */
    public static ExecuteEnvironmentCheckResult checkGlobalNgInstalled() {
        return getExecuteEnvironmentCheckResult("ng --version", WebCommonI18nMsgConstant.WEB_COMMON_MSG_0007);
    }

    private static ExecuteEnvironmentCheckResult getExecuteEnvironmentCheckResult(String command, String errorMsgI18nCode) {
        ExecuteEnvironmentCheckResult checkResult = ExecuteEnvironmentCheckResult.getInstance();
        CommandExecutedResult result = new CommandExecutedResult();
        try {
            result = CommandLineUtility.runCommandWithoutThrows(command);
        } catch (RuntimeException ex) {
            result.setExitCode(-1);
            result.setErrorInfo(ex.getMessage());
        }

        WebLogger.Instance.info(command + " result:\r\n " + result.getOutputInfo());

        if (result.getExitCode() == 0) {
            return checkResult;
        }
        checkResult.setSuccess(false);
        checkResult.setErrorMessage(ResourceLocalizeUtil.getString(errorMsgI18nCode,
                getPrefixMessage(),
                getKnowledgeUrl(),
                getEndMessage()));
        return checkResult;
    }

    /**
     * 编译前环境检查
     */
    public static void beforeCompile() {
        /// 不考虑npm使用安装盘的情况
        ExecuteEnvironmentCheckResult nodeCheckResult = ExecuteEnvironmentChecker.checkGlobalNodeInstalled();
        if (!nodeCheckResult.isSuccess()) {
            throw new EnvironmentException(nodeCheckResult.getErrorMessage(), getDefaultModalTitle(), defaultModalLevel);
        }
        ExecuteEnvironmentCheckResult npmCheckResult = ExecuteEnvironmentChecker.checkGlobalNpmInstalled();
        if (!npmCheckResult.isSuccess()) {
            throw new EnvironmentException(npmCheckResult.getErrorMessage(), getDefaultModalTitle(), defaultModalLevel);
        }

        ExecuteEnvironmentCheckResult ngCheckResult = ExecuteEnvironmentChecker.checkGlobalNgInstalled();
        if (!ngCheckResult.isSuccess()) {
            throw new EnvironmentException(ngCheckResult.getErrorMessage(), getDefaultModalTitle(), defaultModalLevel);
        }
    }

    /**
     * 生成前环境检查
     */
    public static void beforeGenerate() {
        /// 不考虑npm使用安装盘的情况
        ExecuteEnvironmentCheckResult nodeCheckResult = ExecuteEnvironmentChecker.checkGlobalNodeInstalled();
        if (!nodeCheckResult.isSuccess()) {
            throw new EnvironmentException(nodeCheckResult.getErrorMessage(), getDefaultModalTitle(), defaultModalLevel);
        }

        ExecuteEnvironmentCheckResult jitCheckResult = ExecuteEnvironmentChecker.checkGlobalJitEngineInstalled();
        if (!jitCheckResult.isSuccess()) {
            throw new EnvironmentException(jitCheckResult.getErrorMessage(), getDefaultModalTitle(), defaultModalLevel);
        }
    }
}
