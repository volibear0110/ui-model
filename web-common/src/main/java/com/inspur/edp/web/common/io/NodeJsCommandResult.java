/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.io;

import lombok.Data;

/**
 * @Title: NodeJsCommandResult
 * @Description: com.inspur.edp.web.common.io nodejs命令结果
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/6 16:19
 */
@Data
public class NodeJsCommandResult {
    /**
     * Nodejs 命令或对应路径
     */
    private String nodeJsCommand;
    /**
     * 是否使用全局命令   如果当前路径下不存在对应命令执行文件，那么使用其环境变量配置命令
     */
    private boolean isUseGlobal;

}
