/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.metadata;

import com.inspur.edp.web.common.logger.WebLogger;

import java.util.Arrays;
import java.util.Optional;

/**
 * 定义元数据类型枚举参数
 *
 * @author guozhiqi
 */
public enum MetadataTypeEnum {
    /**
     * 表单元数据
     */
    Frm {
        @Override
        public String getCode() {
            return "Form";
        }

        @Override
        public String getName() {
            return "表单元数据";
        }

    },
    /**
     * eapi 元数据
     */
    Eapi {
        @Override
        public String getCode() {
            return "ExternalApi";
        }

        @Override
        public String getName() {
            return "Eapi元数据";
        }
    },
    /**
     * 资源元数据
     */
    Resource {
        @Override
        public String getCode() {
            return "ResourceMetadata";
        }

        @Override
        public String getName() {
            return "资源元数据";
        }
    },
    /**
     * 状态机元数据
     */
    StateMachine {
        @Override
        public String getCode() {
            return "StateMachine";
        }

        @Override
        public String getName() {
            return "状态机元数据";
        }
    },
    /**
     * 命令元数据
     */
    Command {
        @Override
        public String getCode() {
            return "Command";
        }

        @Override
        public String getName() {
            return "命令元数据";
        }
    },
    /**
     * 页面流
     */
    Route {
        @Override
        public String getCode() {
            return "PageFlowMetadata";
        }

        @Override
        public String getName() {
            return "页面流元数据";
        }
    },
    /**
     * 源代码文件定义
     */
    SourceCode {
        @Override
        public String getCode() {
            return "SourceCode";
        }

        @Override
        public String getName() {
            return "SourceCode元数据";
        }
    },
    Component {
        @Override
        public String getCode() {
            return "Component";
        }

        @Override
        public String getName() {
            return "构件元数据";
        }
    },
    ViewModel {
        @Override
        public String getCode() {
            return "GSPViewModel";
        }

        @Override
        public String getName() {
            return "视图模型";
        }
    },
    TS {
        @Override
        public String getCode() {
            return "Ts";
        }

        @Override
        public String getName() {
            return "自定义构件元数据";
        }
    };

    /**
     * 获取对应元数据的code
     *
     * @return
     */
    public abstract String getCode();

    public abstract String getName();

    /**
     * 判断传递的元数据类型是否是对应的枚举类型
     *
     * @param metadataType
     * @return
     */
    public boolean isCurrentMetadataType(String metadataType) {
        return this.getCode().equals(metadataType);
    }

    /**
     * 执行类型转换
     *
     * @param metadataType
     * @return
     */
    public static MetadataTypeEnum from(String metadataType) {
        try {
            Optional<MetadataTypeEnum> findMetadataType = Arrays.stream(MetadataTypeEnum.values()).filter(t -> t.getCode().equals(metadataType)).findFirst();
            return findMetadataType.orElse(MetadataTypeEnum.Frm);
        } catch (Exception ex) {
            WebLogger.Instance.error("元数据类型转换失败，待转换类型为：" + metadataType);

        }
        return MetadataTypeEnum.Frm;
    }

    @Override
    public String toString() {
        return this.getCode();
    }
}
