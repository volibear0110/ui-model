/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

import com.inspur.edp.web.common.logger.WebLogger;

import java.util.Arrays;

/**
 * 配置web自定义线程异常捕获 避免出现未知未捕获异常导致线程终止
 * 子线程捕获到异常，写入到日志文件，不再对外抛出异常
 *
 * @author guozhiqi
 */
public class WebThreadUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        if (thread != null && throwable != null) {
            WebLogger.Instance.error("Web命令执行出现异常，对应线程名称" + thread.getName() +"异常信息"+ throwable.getMessage() + Arrays.toString(throwable.getStackTrace()), this.getClass().getName());
        }
    }
}
