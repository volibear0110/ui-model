package com.inspur.edp.web.common.constant;

public class WebCommonExceptionConstant {

    public final static String WEB_ERROR_FILE_NAME = "WebErrorCode.properties";

    public final static String SU = "pfcommon";

    public final static String WEB_COMMON_ERROR_0001 = "WEB_COMMON_ERROR_0001";
    public final static String WEB_COMMON_ERROR_0002 = "WEB_COMMON_ERROR_0002";
    public final static String WEB_COMMON_ERROR_0003 = "WEB_COMMON_ERROR_0003";
    public final static String WEB_COMMON_ERROR_0004 = "WEB_COMMON_ERROR_0004";
    public final static String WEB_COMMON_ERROR_0005 = "WEB_COMMON_ERROR_0005";

    public final static String WEB_COMMON_ERROR_0006 = "WEB_COMMON_ERROR_0006";
    public final static String WEB_COMMON_ERROR_0007 = "WEB_COMMON_ERROR_0007";
    public final static String WEB_COMMON_ERROR_0008 = "WEB_COMMON_ERROR_0008";
    public final static String WEB_COMMON_ERROR_0009 = "WEB_COMMON_ERROR_0009";
    public final static String WEB_COMMON_ERROR_0010 = "WEB_COMMON_ERROR_0010";

    public final static String WEB_COMMON_ERROR_0011 = "WEB_COMMON_ERROR_0011";

    public final static String WEB_APPROVAL_FORMAT_EXCEPTION_0001 = "WEB_APPROVAL_FORMAT_EXCEPTION_0001";

    public final static String WEB_APPROVAL_FORMAT_EXCEPTION_0002 = "WEB_APPROVAL_FORMAT_EXCEPTION_0002";

    public final static String WEB_APPROVAL_FORMAT_EXCEPTION_0003 = "WEB_APPROVAL_FORMAT_EXCEPTION_0003";
}
