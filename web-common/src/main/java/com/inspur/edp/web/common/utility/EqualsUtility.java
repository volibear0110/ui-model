/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;


import java.util.Objects;

/**
 * description: 字符串比较
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
public class EqualsUtility {
    private boolean isLinux = false;

    private static final Object _lock = new Object();

    private EqualsUtility() {
        // 当前运行环境是否是linux环境
        this.isLinux = OperatingSystemUtility.isLinux();
    }

    /**
     * 增加volatile  避免由于指令重排序导致获取到的实例不完整
     */
    private static volatile EqualsUtility _instance;

    public static EqualsUtility getInstance() {
        if (_instance == null) {
            synchronized (_lock) {
                if (_instance == null) {
                    _instance = new EqualsUtility();
                }
            }
        }
        return _instance;
    }

    /**
     * 依据操作系统进行的字符串比较
     * 如果是windows  那么字符串不区分大小写
     * 如果是linux  那么字符串区分大小写
     *
     * @param value1
     * @param value2
     * @return
     */
    public boolean equalsWithOS(String value1, String value2) {
        // 如果是linux 那么比较区分大小写
        if (isLinux) {
            return value1.equals(value2);
        }

        // 如果是windows  那么转换成小写形式进行比较
        return value1.equalsIgnoreCase(value2);
    }


    /**
     * 依据操作系统进行的字符串比较*
     *
     * @param value1
     * @param value2
     * @return
     */
    public boolean equalsWithoutOS(String value1, String value2) {
        return Objects.equals(value1, value2);
    }

    /**
     * 字符串比较
     *
     * @param value1
     * @param value2
     * @param useCaseSensitive 是否区分大小写
     * @return
     */
    public boolean equalsWithCaseSensitive(String value1, String value2, boolean useCaseSensitive) {
        if (!useCaseSensitive) {
            return equalsWithoutOS(value1.toLowerCase(), value2.toLowerCase());
        }
        return equalsWithoutOS(value1, value2);
    }

}
