package com.inspur.edp.web.common.utility;

import com.inspur.edp.web.common.constant.WebCommonExceptionConstant;
import com.inspur.edp.web.common.logger.WebLogger;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.api.I18nContextService;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

public class ResourceLocalizeUtil {
    public ResourceLocalizeUtil() {

    }

    public static String getString(String i18nCode) {
        ResourceLocalizer resourceLocalizer = SpringBeanUtils.getBean(ResourceLocalizer.class);
        I18nContextService i18nContextService = SpringBeanUtils.getBean(I18nContextService.class);
        String language = i18nContextService.getLanguage();
        return resourceLocalizer.getString(i18nCode, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, WebCommonExceptionConstant.SU, language);
    }

    public static String getZhChsString(String i18nCode) {
        ResourceLocalizer resourceLocalizer = SpringBeanUtils.getBean(ResourceLocalizer.class);
        return resourceLocalizer.getString(i18nCode, WebCommonExceptionConstant.WEB_ERROR_FILE_NAME, WebCommonExceptionConstant.SU, "zh-CHS");
    }

    public static String getString(String i18nCode, Object ...params) {
        String result;
        try{
            result = String.format(getString(i18nCode), params);
        } catch (RuntimeException e) {
            result = getString(i18nCode);
            WebLogger.Instance.error(e.getMessage());
        }
        return result;
    }
}
