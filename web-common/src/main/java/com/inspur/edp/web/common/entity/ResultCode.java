/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.entity;

/**
 * 返回结果定义
 *
 * @author noah
 */
public class ResultCode<T> {
    private ResultCode() {
    }

    private static final int SuccessCode = 1;
    private static final int FailureCode = 0;

    private static final int InfoCode = 2;

    /**
     * 获取成功编码  1
     * 暴露此方法的目的是为了外部可以获取到对应的成功编码
     *
     * @return
     */
    public static int getSuccessCode() {
        return SuccessCode;
    }

    /**
     * 获取失败编码  0
     * 暴露此方法的目的是为了外部可以获取到对应的失败编码
     *
     * @return
     */
    public static int getFailureCode() {
        return FailureCode;
    }

    /**
     * 不包含data参数 只有一个成功标识
     *
     * @param <T>
     * @return
     */
    public static <T> ResultMessage<T> success() {
        return new ResultMessage<>(SuccessCode, "");
    }

    /**
     * 不包含data参数 传递对应的info提示信息
     *
     * @param message
     * @param <T>
     * @return
     */
    public static <T> ResultMessage<T> info(String message) {
        return new ResultMessage<>(InfoCode, message);
    }

    public static <T> ResultMessage<T> success(T data) {
        return new ResultMessage<>(SuccessCode, "", data);
    }

    public static <T> ResultMessage<T> success(String message) {
        return new ResultMessage<>(SuccessCode, message);
    }

    public static <T> ResultMessage<T> failure(String errorMessage) {
        return new ResultMessage<>(FailureCode, errorMessage);
    }

    public static <T> ResultMessage<T> failure(String errorMessage, T data) {
        return new ResultMessage<>(FailureCode, errorMessage, data);
    }

    public static <T> ResultMessage<T> custom(Integer code, String errorMessage) {
        return new ResultMessage<>(code, errorMessage);
    }

    public static <T> ResultMessage<T> custom(Integer code, String errorMessage, T data) {
        return new ResultMessage<>(code, errorMessage, data);
    }
}
