package com.inspur.edp.web.common.utility;

import com.inspur.edp.lcm.metadata.inner.api.IdeLogService;
import com.inspur.edp.web.common.logger.WebLogger;

public class IdeLogUtils {
    private IdeLogService ideLogService;

    public IdeLogUtils() {
        try {
            Class.forName("com.inspur.edp.lcm.metadata.inner.api.utils.IdeLogUtils");
            ideLogService = com.inspur.edp.lcm.metadata.inner.api.utils.IdeLogUtils.getIdeLogService();
        } catch (ClassNotFoundException e) {
            ideLogService = null;
            WebLogger.Instance.warn("com.inspur.edp.lcm.metadata.inner.api.utils.IdeLogUtils not found");
        }
    }

    public void pushLog(String var1, String var2) {
        if (ideLogService != null) {
            try {
                ideLogService.pushLog(var1, var2);
            } catch (Exception e) {
                WebLogger.Instance.warn("Failed to push log: " + e.getMessage());
            }
        }
    }
}


