/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.logger;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.Random;

/**
 * @Title: WebLoggerTest
 * @Description: com.inspur.edp.web.common.logger
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/11 11:47
 */
class WebLoggerTest {


    @Before
    void init() {

    }

    @Test
    void debug() {
        WebLogger.Instance.debug("test debug message");
    }

    @Test
    void info() {
        WebLogger.Instance.info("test info message");
    }

    @Test
    void error() {
        WebLogger.Instance.error("test error message");
    }

    @Test
    void warn() {
        WebLogger.Instance.warn("test warn message");
    }

    @Test
    public void performanceTest() {
        final long beginTime = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            WebLogger.Instance.info(String.format("使用自定义的默认logger%s", i), String.valueOf(new Random().nextInt()));
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("总耗时 " + (endTime - beginTime));
    }
}
