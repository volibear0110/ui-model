package com.inspur.edp.web.common.loadclass;

/**
 * @author noah
 * 2023/7/20 14:16
 */
public class CustomLoadClass {
    static {
        System.out.println("static method ");
    }

    {
        System.out.println("method  specific ");
    }

    public CustomLoadClass() {
        System.out.println("custom load class constructor");
    }

}
