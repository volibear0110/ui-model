/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.utility;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class StringUtilityTest {

    @Test
    public void testReplaceDotToXiaHuaxian() {
        String source = ".gg.ggg.ggg";
        String result = StringUtility.replaceDotToUnderLine(source);
        assertEquals("_gg_ggg_ggg", result);
    }

    @Test
    public void replaceLineToUnderLine() {
        String source = "-noah-guo";
        String target = StringUtility.replaceLineToUnderLine(source);
        assertEquals(target, "_noah_guo");
    }

    @Test
    public void executeIfPresent() {
        StringUtility.executeIfPresent(null, (item) -> {
            System.out.println("这一行不会输出");
        });

        StringUtility.executeIfPresent("", (item) -> {
            System.out.println("这一行不会输出");
        });

        StringUtility.executeIfPresent("1", (item) -> {
            System.out.println("这一行会进行输出");
        });

        StringUtility.executeIfPresent(null, (item) -> {
            return StringUtility.isNotNullOrEmpty(item);
        }, (item) -> {
            System.out.println("通过自定义计算进行输出，但此行不会输出");
        });


        StringUtility.executeIfPresent("", (item) -> {
            return StringUtility.isNotNullOrEmpty(item);
        }, (item) -> {
            System.out.println("通过自定义计算进行输出，但此行不会输出");
        });

        StringUtility.executeIfPresent("1", (item) -> {
            return StringUtility.isNotNullOrEmpty(item);
        }, (item) -> {
            System.out.println("通过自定义计算进行输出，但此行会输出");
        });

    }

    @Test
    public void testTrim() {
//        String source = "'\"fdfdfdfdf'fdfdfdf\"'";
//        String result = StringUtility.trimStringWith(source, Collections.singletonList('\''));
//        Assert.assertEquals(result, "fdfdfdfdf'fdfdfdf");
    }


}