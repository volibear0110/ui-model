package com.inspur.edp.web.common.stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author noah
 * 2023/8/18 15:17
 */
public class StreamTest {
    private List<String> stringList;

    @BeforeEach
    public void init() {
        stringList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            stringList.add(String.valueOf(i));
        }
    }

    @Test
    public void test() {
        System.out.println(stringList.size());
        this.stringList.stream().peek(t -> {
            System.out.println(t);
        });

    }


}
