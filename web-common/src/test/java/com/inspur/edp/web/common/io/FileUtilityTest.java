/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.io;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FileUtilityTest {


    public void testFileCopy() {
        String sourceFile = "D:\\InspurCode\\N转J之后代码\\npm安装\\web\\web-common\\src\\test\\java\\com\\inspur\\edp\\web\\common\\io\\sourcefile\\package.json";
        String targetFile = "D:\\InspurCode\\N转J之后代码\\npm安装\\web\\web-common\\src\\test\\java\\com\\inspur\\edp\\web\\common\\io\\targetfile\\package.json";
        FileUtility.copyFile(sourceFile, targetFile, true);

        Assert.assertTrue(FileUtility.isSameFile(sourceFile, targetFile));
    }

    /**
     * 判断文件权限
     */
    @Test
    public void testPermission() {
        String sourceFile = "D:\\InspurCode\\N转J之后代码\\npm安装\\web\\web-common\\src\\test\\java\\com\\inspur\\edp\\web\\common\\io\\sourcefile\\package.json";
        File file = new File(sourceFile);
        System.out.println(file.canRead());
        System.out.println(file.canWrite());
    }

    @Test
    public void testGetCurrentWorkPath() {
        String path = FileUtility.getCurrentWorkPath(true);
        System.out.println(path);
    }

    @Test
    public void combine() {
//        System.out.println(FileUtility.combine("noah", "c:/noah"));
//        System.out.println(Paths.get("noah", "c:/noah"));
//        Path path = Paths.get("noah", "noah");
//        System.out.println(FileUtility.combine("noah", "noah", "noah"));
//        System.out.println(Paths.get("noah", "noah", "noah"));
//        System.out.println(Paths.get("noah", "c:/noah"));


    }


    @Test
    public void testExists() {
//        File file = new File("c:/bucunzai");
//        Assert.assertTrue(file.isDirectory());
    }


    @Test
    public void testSeparator() {
        System.out.println(FileUtility.DIRECTORY_SEPARATOR_CHAR);
    }

    @Test
    public void testFor() {

        List<String> sourceList = IntStream.range(1, 31).mapToObj(t -> String.valueOf(t)).collect(Collectors.toList());
        for (String item : sourceList) {
            if (item.equals("5")) {
                System.out.println("当前的循环值为5");
                return;
            }
            System.out.println("当前的循环值为" + item);
        }
    }

    @Test
    public void testCombine() {
    }
}
