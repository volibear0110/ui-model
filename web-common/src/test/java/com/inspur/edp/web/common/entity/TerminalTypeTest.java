/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.entity;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TerminalTypeTest {

    @Test
    public void getAllTypeSuffixList() {
        List<String> terminalTypeSuffixList = TerminalType.getAllTypeSuffixList();
        assertEquals(terminalTypeSuffixList.size(), 2);
        assertTrue(terminalTypeSuffixList.contains(TerminalType.PC.getFormMetadataSuffix()));
        assertTrue(terminalTypeSuffixList.contains(TerminalType.MOBILE.getFormMetadataSuffix()));
    }

    @Test
    public void testEquals() {
        System.out.println(TerminalType.PC == TerminalType.PC);
        System.out.println(TerminalType.PC.equals(TerminalType.PC));
    }
}