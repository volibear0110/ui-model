package com.inspur.edp.web.common.loadclass;

import org.junit.jupiter.api.Test;

/**
 * @author noah
 * 2023/7/20 14:16
 */
public class LoadClassTest {

    @Test
    public void load() throws ClassNotFoundException {
//        Class classInfo = Class.forName("com.inspur.edp.web.common.loadclass.CustomLoadClass");
//        System.out.println(classInfo.getName());

//        Class classInfo1 = Class.forName("com.inspur.edp.web.common.loadclass.CustomLoadClass", false, this.getClass().getClassLoader());
//        System.out.println(classInfo1.getName());

        Class classInfo2 = ClassLoader.getSystemClassLoader().loadClass("com.inspur.edp.web.common.loadclass.CustomLoadClass");
        System.out.println(classInfo2.getName());
    }
}
