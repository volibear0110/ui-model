/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.common.environment.checker;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Title: ExecuteEnvironmentCheckerTest
 * @Description: com.inspur.edp.web.common.environment.checker
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/7/29 15:35
 */
public class ExecuteEnvironmentCheckerTest {

    @Test
    public void checkGlobalNodeInstalled() {
        ExecuteEnvironmentCheckResult result = ExecuteEnvironmentChecker.checkGlobalNodeInstalled();
        System.out.println(result.getErrorMessage());
        assertTrue(result.isSuccess());
    }

    @Test
    public void checkGlobalNpmInstalled() {
        ExecuteEnvironmentCheckResult result = ExecuteEnvironmentChecker.checkGlobalNpmInstalled();
        System.out.println(result.getErrorMessage());
        assertTrue(result.isSuccess());
    }

    @Test
    public void checkGlobalJitEngineInstalled() {
        ExecuteEnvironmentCheckResult result = ExecuteEnvironmentChecker.checkGlobalJitEngineInstalled();
        System.out.println(result.getErrorMessage());
        assertTrue(result.isSuccess());
    }

    @Test
    public void checkGlobalNgInstalled() {
        ExecuteEnvironmentCheckResult result = ExecuteEnvironmentChecker.checkGlobalNgInstalled();
        System.out.println(result.getErrorMessage());
        assertTrue(result.isSuccess());
    }
}
