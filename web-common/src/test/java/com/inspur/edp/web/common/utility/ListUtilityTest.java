package com.inspur.edp.web.common.utility;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class ListUtilityTest {

    @Test
    public void testIsEmpty_withEmptyList_shouldReturnTrue() {
        List<Integer> list = new ArrayList<>();
        boolean result = ListUtility.isEmpty(list);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsEmpty_withNonEmptyList_shouldReturnFalse() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        boolean result = ListUtility.isEmpty(list);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsNotEmpty_withEmptyList_shouldReturnFalse() {
        List<Integer> list = new ArrayList<>();
        boolean result = ListUtility.isNotEmpty(list);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsNotEmpty_withNonEmptyList_shouldReturnTrue() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        boolean result = ListUtility.isNotEmpty(list);
        Assert.assertTrue(result);
    }

    @Test
    public void testGetLength_withEmptyList_shouldReturnZero() {
        List<Integer> list = new ArrayList<>();
        int result = ListUtility.getLength(list);
        Assert.assertEquals(0, result);
    }

    @Test
    public void testGetLength_withNonEmptyList_shouldReturnSize() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        int result = ListUtility.getLength(list);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testAdd_withNullSourceList_shouldAddTargetListToEmptyList() {
        List<Integer> sourceList = null;
        List<Integer> targetList = new ArrayList<>();
        targetList.add(1);
        targetList.add(2);
        sourceList = ListUtility.add(sourceList, targetList);
        Assert.assertEquals(targetList, sourceList);
    }

    @Test
    public void testAdd_withNonNullSourceList_shouldAddTargetListToSourceList() {
        List<Integer> sourceList = new ArrayList<>();
        sourceList.add(1);
        List<Integer> targetList = new ArrayList<>();
        targetList.add(2);
        targetList.add(3);
        ListUtility.add(sourceList, targetList);
        List<Integer> expectedList = new ArrayList<>();
        expectedList.add(1);
        expectedList.add(2);
        expectedList.add(3);
        Assert.assertEquals(expectedList, sourceList);
    }

    @Test
    public void testAdd_withNullElement_shouldNotAddToSourceList() {
        List<Integer> sourceList = new ArrayList<>();
        sourceList.add(1);
        ListUtility.add(sourceList, new ArrayList<>());
        Assert.assertEquals(1, sourceList.size());
    }

    @Test
    public void testAdd_withNonNullElement_shouldAddToSourceList() {
        List<Integer> sourceList = new ArrayList<>();
        sourceList.add(1);
        ListUtility.add(sourceList, 2);
        List<Integer> expectedList = new ArrayList<>();
        expectedList.add(1);
        expectedList.add(2);
        Assert.assertEquals(expectedList, sourceList);
    }

    @Test
    public void testContains_withEmptyList_shouldReturnFalse() {
        List<Integer> list = new ArrayList<>();
        boolean result = ListUtility.contains(list, 1);
        Assert.assertFalse(result);
    }

    @Test
    public void testContains_withNonEmptyListAndElementNotPresent_shouldReturnFalse() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        boolean result = ListUtility.contains(list, 3);
        Assert.assertFalse(result);
    }

    @Test
    public void testContains_withNonEmptyListAndElementPresent_shouldReturnTrue() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        boolean result = ListUtility.contains(list, 2);
        Assert.assertTrue(result);
    }
}