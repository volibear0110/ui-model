/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.service;

import org.junit.Assert;

import java.nio.file.Paths;

class ScriptCacheServiceImpTest {


   // @org.junit.jupiter.api.Test
    void publishScriptWithDirectory() {

        String strScriptBaseDir = "C:\\env\\igix_2011_x86_64_build20200918_rc2\\web\\apps\\scm\\sd\\web\\bo-salesorderfrontguojihua";
        String strRelativeScriptBaseDir = "apps\\scm\\sd\\web\\bo-salesorderfrontguojihua";
    }


   // @org.junit.jupiter.api.Test
    void testRelativePath() {
        String strLocalServerPath = "C:\\env\\igix_2011_x86_64_build20200918_rc2\\web";
        String strDeployPath = "C:\\env\\igix_2011_x86_64_build20200918_rc2\\web\\apps\\scm\\sd\\web\\bo-salesorderfrontcombolist";
        String strRelativePath = Paths.get(strLocalServerPath).relativize(Paths.get(strDeployPath)).toString();


        String strDeplayFilePath = "C:\\env\\igix_2011_x86_64_build20200918_rc2\\web\\apps\\scm\\sd\\web\\bo-salesorderfronttestruntime\\version.json";
        String strParentDirectory = Paths.get(strDeplayFilePath).getParent().toString();


        Assert.assertNotNull(strRelativePath);
    }

}
