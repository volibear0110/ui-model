/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository;

import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheContentEntity;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.Optional;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/10/01
 */
public interface FormScriptCacheContentRepository extends DataRepository<FormScriptCacheContentEntity, String> {

    @Query(value = "SELECT a FROM FormScriptCacheContentEntity a WHERE id= :id")
    Optional<FormScriptCacheContentEntity> findFormScriptCacheContentById(@Param("id") String id);


    /**
     * 保存脚本内容缓存
     *
     * @param entity
     * @return
     */
    @Override
    FormScriptCacheContentEntity save(FormScriptCacheContentEntity entity);


    /**
     * 根据id删除对应的数据
     *
     * @param id
     */
    @Modifying
    @Query(value = "DELETE FROM FormScriptCacheContentEntity WHERE id = :id")
    void deleteFormScriptCacheContentById(@Param("id") String id);

    /**
     * 更新工程版本缓存
     *
     * @param id
     * @param lastModifier
     * @param lastModifyTime
     */
    @Modifying
    @Query(value = "UPDATE FormScriptCacheContentEntity SET content=:content, lastmodifier= :lastModifier, lastmodifytime=:lastModifyTime where id= :id")
    void updateFormScriptCacheContentVersion(@Param("id") String id, @Param("content") String content,@Param("lastModifier") String lastModifier, @Param("lastModifyTime") Date lastModifyTime);

}
