/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.rpc;

import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.util.List;

@GspServiceBundle(applicationName = "runtime",serviceUnitName = "bcc",serviceName = "local_server_version_rpc_service")
public interface LocalServerVersionRpcService {

    /**
     * 获取文件列表
     *
     * @param formMetadataId rpc参数  表单元数据id
     * @return
     */
    List<FormScriptCache> getFormScriptCacheByMetadataId(@RpcParam(paramName = "formMetadataId") String formMetadataId);

    /**
     * 根据缓存工程id获取对应的工程版本信息
     * @param projectVersionId
     * @return
     */
    FormProjectCache getFormProjectCacheById(@RpcParam(paramName = "projectVersionId") String projectVersionId);

    /**
     * 根据脚本缓存id获取对应的脚本缓存内容
     * @param contentId
     * @return
     */
    FormScriptCacheContent getFormScriptCacheContentById(@RpcParam(paramName = "contentId") String contentId);

    /**
     * 根据工程名以及对应的工程路径获取工程版本缓存信息
     * @param projectName
     * @param strProjectRelativePath
     * @return
     */
    FormProjectCache getByProjectNameAndProjectRelativePath(@RpcParam(paramName = "projectName") String projectName, @RpcParam(paramName = "strProjectRelativePath") String strProjectRelativePath);

    /**
     * 根据工程版本id信息获取对应的脚本缓存列表
     * @param formProjectVersionId
     * @return
     */
    List<FormScriptCache> getFormScriptCacheByProjectVersionId(@RpcParam(paramName = "formProjectVersionId") String formProjectVersionId);
}
