/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.EqualsUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * description: server缓存脚本文件操作
 * 全局实例 需保证整个应用中存在一份
 * 因为会在初始化时进行脚本缓存版本的初始
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
class LocalServerVersionFileOperation {

    /**
     * 定义私有构造函数
     */
    private LocalServerVersionFileOperation() {
        // 执行本地文件缓存初始化
        this.initLocalServerVersionList();
    }

    private final LocalServerPathGenerator localServerPathGeneratorInstance = LocalServerPathGenerator.getNewInstance(false);

    /**
     * 定义本地脚本缓存
     */
    private static LocalServerVersionFileContent localServerVersionContent = new LocalServerVersionFileContent();

    private static final Object _lock = new Object();

    /**
     * 增加volatile  避免由于指令重排而导致的获取实例不完整
     * 主要针对的是多线程高并发特殊场景下
     */
    private static volatile LocalServerVersionFileOperation instance;

    /**
     * 获取文件操作实例  保持该实例唯一。避免由于多个实例，对同一个文件进行不同操作，导致文件被锁的问题
     * double-check
     * * @return
     */
    public static LocalServerVersionFileOperation getSingleInstance() {
        if (instance == null) {
            synchronized (_lock) {
                if (instance == null) {
                    instance = new LocalServerVersionFileOperation();
                }
            }
        }
        return instance;
    }

    /**
     * 初始化本地脚本缓存文件列表
     */
    private void initLocalServerVersionList() {

        String strLocalServerPath = localServerPathGeneratorInstance.getLocalServerPath();
        String localServerVersionFilePathAndName = localServerPathGeneratorInstance.getLocalServerFilePathAndName();

        // 如果文件不存在 那么创建一个默认的空文件
        if (!FileUtility.exists(localServerVersionFilePathAndName)) {
            this.reInitServerVersionContent(strLocalServerPath);
            return;
        }
        // 如果文件存在
        String versionFileContent = FileUtility.readAsString(localServerVersionFilePathAndName);
        localServerVersionContent = SerializeUtility.getInstance().deserialize(versionFileContent, LocalServerVersionFileContent.class);
        // 如果不是当前的服务器 那么删除该文件  重新创建文件
        if (!EqualsUtility.getInstance().equalsWithOS(localServerVersionContent.getServerPath(), strLocalServerPath)) {
            this.reInitServerVersionContent(strLocalServerPath);
        }
    }

    /**
     * 重新初始化服务器脚本版本
     *
     * @param strLocalServerPath
     */
    private void reInitServerVersionContent(String strLocalServerPath) {
        localServerVersionContent.setVersionList(new ArrayList<>());
        // 设置当前的服务器路径
        localServerVersionContent.setServerPath(strLocalServerPath);
        // 保存当前缓存文件
        saveFile();
    }


    /**
     * 保存缓存工程脚本至本地文件
     *
     * @param version
     */
    public void saveProjectVersion(LocalServerProjectVersion version) {
        if (version == null) {
            return;
        }

        LocalServerProjectVersion currentLocalProjectVersion = findProjectVersionByProjectNameAndProjectRelativePath(version.getProjectName(), version.getProjectRelativePath());
        if (currentLocalProjectVersion == null) {
            localServerVersionContent.getVersionList().add(version);
        } else {
            currentLocalProjectVersion.setVersion(version.getVersion());
        }
        saveFile();
    }


    /**
     * 根据工程名称及工程相对路径获取本地文件缓存信息
     *
     * @param projectName
     * @param projectRelativePath
     * @return
     */
    public LocalServerProjectVersion findProjectVersionByProjectNameAndProjectRelativePath(String projectName, String projectRelativePath) {
        List<LocalServerProjectVersion> versionList = localServerVersionContent.getVersionList().stream().filter((item) -> EqualsUtility.getInstance().equalsWithOS(item.getProjectName(), projectName)
                && EqualsUtility.getInstance().equalsWithOS(item.getProjectRelativePath(), projectRelativePath)).collect(Collectors.toList());
        if (versionList != null && versionList.size() > 0) {
            return versionList.get(0);
        }
        return null;
    }

    /**
     * 根据工程名称和工程部署相对路径获取对应版本
     *
     * @param projectName            工程名称
     * @param strProjectRelativePath 工程相对路径
     * @return
     */
    public String getVersion(String projectName, String strProjectRelativePath) {
        LocalServerProjectVersion projectVersion = getProjectVersionInfo(projectName, strProjectRelativePath);
        if (projectVersion != null) {
            return projectVersion.getVersion();
        }

        return null;
    }

    /**
     * 根据工程名称及工程相对路径获取工程缓存信息
     *
     * @param projectName
     * @param strProjectPath
     * @return
     */
    public LocalServerProjectVersion getProjectVersionInfo(String projectName, String strProjectPath) {
        Optional<LocalServerProjectVersion> localServerProjectVersionOptional = localServerVersionContent.getVersionList().stream()
                .filter((serverVersion) -> EqualsUtility.getInstance().equalsWithOS(serverVersion.getProjectName(), projectName) &&
                        EqualsUtility.getInstance().equalsWithOS(serverVersion.getProjectRelativePath(), strProjectPath)).findFirst();
        return localServerProjectVersionOptional.orElse(null);
    }

    /**
     * 保存本地服务器缓存文件
     */
    public void saveFile() {
        String localServerVersionFilePath = localServerPathGeneratorInstance.getLocalServerFilePath();
        String localServerVersionFileName = localServerPathGeneratorInstance.getLocalServerFileName();
        String serializedVersionList = SerializeUtility.getInstance().serialize(localServerVersionContent);
        FileUtility.writeFile(localServerVersionFilePath, localServerVersionFileName, serializedVersionList);
    }
}
