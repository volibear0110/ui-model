/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager;

import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormProjectCacheEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormProjectCacheRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public class FormProjectCacheManager {
    private final FormProjectCacheRepository repository;

    public FormProjectCacheManager(FormProjectCacheRepository repository) {

        this.repository = repository;
    }

    /**
     * 保存工程交换缓存
     *
     * @param formProjectCacheEntity
     */
    public void save(FormProjectCacheEntity formProjectCacheEntity) {

        this.repository.save(formProjectCacheEntity);
    }

    /**
     * 更新工程交换缓存
     *
     * @param formProjectCacheEntity
     */
    public void update(FormProjectCacheEntity formProjectCacheEntity) {

        this.repository.updateFormProjectCacheVersion(formProjectCacheEntity.getId(), formProjectCacheEntity.getVersion(), formProjectCacheEntity.getLastModifier(), formProjectCacheEntity.getLastModifyTime());
    }

    /**
     * 根据工程名称及工程部署相对路径获取对应得工程缓存信息
     *
     * @param projectName
     * @param strProjectRelativePath
     * @return
     */
    public FormProjectCache getByProjectNameAndProjectRelativePath(String projectName, String strProjectRelativePath) {
        List<FormProjectCacheEntity> formProjectCacheEntities = this.repository.getByProjectNameAndProjectRelativePath(projectName);
        if (formProjectCacheEntities == null || formProjectCacheEntities.size() == 0) {
            return null;
        }

        AtomicReference<FormProjectCacheEntity> findProjectCacheEntity = new AtomicReference<>();
        formProjectCacheEntities.forEach((item) -> {
            if (item.getProjectRelativePath().equals(strProjectRelativePath)) {
                findProjectCacheEntity.set(item);
            }
        });
        if (findProjectCacheEntity.get() != null) {
            return findProjectCacheEntity.get().convertTo();
        }
        return null;
    }

    /**
     * 根据id获取projectVersion
     *
     * @param formProjectId
     * @return
     */
    public FormProjectCache getById(String formProjectId) {
        Optional<FormProjectCacheEntity> formProjectCacheEntity = this.repository.findFormProjectCacheById(formProjectId);
        return formProjectCacheEntity.map(FormProjectCacheEntity::convertTo).orElse(null);
    }
}
