/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;

import com.inspur.edp.web.common.io.FileUtility;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/10/03
 */
class LocalServerFileOperation {

    /**
     * 保存文件内容至指定的文件位置
     *
     * @param scriptFileName
     * @param strRelativePath
     * @param fileContent
     */
    static void saveFile(String scriptFileName, String strRelativePath, String fileContent) {
        String localServerPath = LocalServerPathGenerator.getNewInstance(false).getLocalServerWebPath();
        String scriptFilePath = FileUtility.combine(localServerPath, strRelativePath);
        FileUtility.writeFile(scriptFilePath, scriptFileName, fileContent);
    }
}
