/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository;

import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormProjectCacheEntity;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public interface FormProjectCacheRepository extends DataRepository<FormProjectCacheEntity, String> {

    @Query(value = "SELECT a FROM FormProjectCacheEntity a WHERE id= :id")
    Optional<FormProjectCacheEntity> findFormProjectCacheById(@Param("id") String id);


    /**
     * 保存缓存实体
     *
     * @param entity
     * @return
     */
    @Override
    FormProjectCacheEntity save(FormProjectCacheEntity entity);


    /**
     * 更新工程版本缓存
     *
     * @param id
     * @param version
     * @param lastModifier
     * @param lastModifyTime
     */
    @Modifying
    @Query(value = "UPDATE FormProjectCacheEntity SET version = :version , lastmodifier= :lastModifier, lastmodifytime=:lastModifyTime WHERE id=:id")
    void updateFormProjectCacheVersion(@Param("id") String id,@Param("version") String version,@Param("lastModifier") String lastModifier,@Param("lastModifyTime") Date lastModifyTime);


    /**
     * 根据id删除对应的数据
     *
     * @param id
     */
    @Modifying
    @Query(value = "DELETE FROM FormProjectCacheEntity WHERE id = :id")
    void deleteFormProjectCacheById(@Param("id") String id);

    /**
     * 根据工程名称及工程相对路径获取对应得工程缓存信息
     * @param projectName
     * @return
     */
    @Query(value = "SELECT a FROM FormProjectCacheEntity a WHERE projectname= :projectName ")
    List<FormProjectCacheEntity> getByProjectNameAndProjectRelativePath(@Param("projectName") String projectName);

}
