/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.webservice;

import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.constant.I18nMsgConstant;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.ScriptCacheCheckVersionRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.ScriptCacheResponse;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.webservice.ScriptCacheWebService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerVersionManager;

import javax.annotation.Resource;

/**
 * description: 脚本缓存web调用入口
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public class ScriptCacheWebServiceImpl implements ScriptCacheWebService {
    @Resource
    private LocalServerVersionManager localServerVersionManager;

    /**
     * 脚本缓存  版本检测
     *
     * @param request 请求参数
     * @return
     */
    @Override
    public ScriptCacheResponse checkVersion(ScriptCacheCheckVersionRequest request) {
        ScriptCacheResponse scriptCacheResponse = ScriptCacheResponse.getInstance();

        // 进行入参检测
        if (request == null) {
            scriptCacheResponse.setSuccess(false);
            scriptCacheResponse.setErrorMessage("script version check,the parameter request is null");
            return scriptCacheResponse;
        }

        // 如果参数中表单元数据id不为空 那么使用元数据id进行版本检测
        if (!StringUtility.isNullOrEmpty(request.getFormMetadataId())) {
            // 进行本地版本与数据库版本比较
            scriptCacheResponse = this.localServerVersionManager.checkVersionWithFormMetadataId(request.getFormMetadataId());
        } else {
            scriptCacheResponse = this.checkVersionWithProjectInfo(request);
        }

        //获取当前工程对应的版本
        //ScriptCacheVersionManager.checkMetadataVersion()
        //进行元数据的版本检测
        // 检查ts文件 不需要进行版本检测
        // 如果需要更新  获取工程下哪些脚本需要更新

        //
        return scriptCacheResponse;
    }


    /**
     * 依据工程信息进行脚本文件版本检测
     *
     * @param request
     * @return
     */
    private ScriptCacheResponse checkVersionWithProjectInfo(ScriptCacheCheckVersionRequest request) {
        ScriptCacheResponse scriptCacheResponse = ScriptCacheResponse.getInstance();

        if (StringUtility.isNullOrEmpty(request.getProjectName())) {
            scriptCacheResponse.setSuccess(false);
            scriptCacheResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0018));
            return scriptCacheResponse;
        }
        if (StringUtility.isNullOrEmpty(request.getProjectRelativePath())) {
            scriptCacheResponse.setSuccess(false);
            scriptCacheResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0019));
            return scriptCacheResponse;
        }
        scriptCacheResponse = this.localServerVersionManager.checkVersionWithProjectNameAndRelativePath(request.getProjectName(), request.getProjectRelativePath());
        return scriptCacheResponse;
    }

}
