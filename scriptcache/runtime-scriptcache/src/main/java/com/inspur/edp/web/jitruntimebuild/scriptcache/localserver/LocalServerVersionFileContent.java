/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
class LocalServerVersionFileContent {
    /**
     * 工程版本内容列表
     */
    private List<LocalServerProjectVersion> versionList;

    /**
     * 服务器server 路径
     */
    private String serverPath;

    public List<LocalServerProjectVersion> getVersionList() {
        if (this.versionList == null) {
            this.versionList = new ArrayList<>();
        }
        return versionList;
    }

    public void setVersionList(List<LocalServerProjectVersion> versionList) {
        this.versionList = versionList;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

}
