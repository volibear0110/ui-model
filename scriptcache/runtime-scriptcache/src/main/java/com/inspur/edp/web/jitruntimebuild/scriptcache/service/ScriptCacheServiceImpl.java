/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.service;

import com.inspur.edp.lcm.rtcustomization.cache.api.entity.CustomizationGlobalVersion;
import com.inspur.edp.lcm.rtcustomization.cache.api.entity.CustomizationType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.utility.*;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.constant.I18nMsgConstant;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.PublishScriptRequest;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.service.ScriptCacheService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.CustomizationCacheServiceInstanceManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.ScriptCacheVersionManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * description: 脚本缓存service
 *
 * @author Noah Guo
 * @date 2020/09/28
 */
public class ScriptCacheServiceImpl implements ScriptCacheService {

    @Resource
    private ScriptCacheVersionManager scriptCacheVersionManager;

    private static final String CurrentClassName = ScriptCacheServiceImpl.class.getName();


    /**
     * 发布文件目录下的脚本文件  如果发生变化 则更新对应的版本
     *
     * @param request 脚本缓存发布入参
     */
    @Override
    public void publishScriptWithSingleFileName(PublishScriptRequest request, String relativeFolder, String fileName) {
        String strProjectName = request.getProjectName();
        String strProjectCode = request.getProjectName();
        String strProjectRelativePath = request.getProjectRelativePath();
        // 初始化工程脚本缓存信息
        FormProjectCache formProjectCache = this.scriptCacheVersionManager.getByProjectNameAndProjectRelativePath(strProjectName, strProjectRelativePath);
        if (formProjectCache == null) {
            // 如果获取工程缓存信息为空
            formProjectCache = this.createFormProjectCache(strProjectName, strProjectCode, strProjectRelativePath);

            // 将当前工程缓存信息保存至数据库表
            this.scriptCacheVersionManager.saveFormProjectCache(formProjectCache);
        }
        if (StringUtility.isNullOrEmpty(relativeFolder)) {
            relativeFolder = "";
        }

        // 获取文件目录下的所有文件列表
        String strFolder = request.getAbsoluteBaseDirectory();

        if (!StringUtility.isNullOrEmpty(relativeFolder)) {
            strFolder = FileUtility.combine(request.getAbsoluteBaseDirectory(), relativeFolder);
        }

        ArrayList<File> fileList = new ArrayList<>();
        if (StringUtility.isNullOrEmpty(fileName)) {
            fileList = FileUtility.getFiles(strFolder, true);
        } else {
            String filePath = FileUtility.combine(strFolder, fileName);
            fileList.add(new File(filePath));
        }

        WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0014,
                request.getAbsoluteBaseDirectory(),
                ListUtility.getLength(fileList)));
        // 如果存在文件列表
        if (fileList != null && fileList.size() > 0) {
            FormProjectCache finalFormProjectCache = formProjectCache;
            fileList.forEach(t -> {
                // 脚本发布
                if (t != null) {
                    pubishScript(t, request.getMetaDataId(), finalFormProjectCache, request);
                }
            });
        }

        // 保存元数据版本
        saveMetadataVersion(request, formProjectCache);
    }

    /**
     * 发布文件目录下的所有脚本文件 并更新对应的版本
     *
     * @param request 请求参数
     */
    @Override
    public void publishScriptWithDirectory(PublishScriptRequest request) {
        publishScriptWithSingleFileName(request, null, null);
    }

    /**
     * 保存元数据版本
     *
     * @param request
     * @param formProjectCache
     */
    private void saveMetadataVersion(PublishScriptRequest request, FormProjectCache formProjectCache) {
        if (!request.isUpdateMetadataVersion()) {
            return;
        }
        WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0015), CurrentClassName);
        try {
            CustomizationGlobalVersion globalVersionInstance = new CustomizationGlobalVersion();
            globalVersionInstance.setCustomizationType(CustomizationType.Metadata);
            globalVersionInstance.setId(request.getMetaDataId());
            globalVersionInstance.setObjectId(request.getMetaDataId());
            globalVersionInstance.setVersion(formProjectCache.getVersion());
            CustomizationCacheServiceInstanceManager.getCustomizationCacheInstance().saveGlobalVersion(globalVersionInstance);

            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0016,
                    request.getMetaDataId(),
                    formProjectCache.getVersion()), CurrentClassName);
        } catch (Exception e) {
            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0017,
                    request.getMetaDataId(),
                    formProjectCache.getVersion()), CurrentClassName);
            throw e;
        }


    }


    /**
     * 发布指定路径下的脚本文件
     *
     * @param scriptFilePath
     * @param metaDataId
     * @param formProjectCache
     */
    public void publishScriptWithFilePath(String scriptFilePath, String metaDataId, FormProjectCache formProjectCache, PublishScriptRequest request) {
        File file = new File(scriptFilePath);
        pubishScript(file, metaDataId, formProjectCache, request);
    }

    /**
     * 发布脚本  依据
     *
     * @param file
     * @param metaDataId
     * @param formProjectCache
     */
    public void pubishScript(File file, String metaDataId, FormProjectCache formProjectCache, PublishScriptRequest request) {
        if (!file.exists()) {
            WebLogger.Instance.warn("the file is not exists, file path is:" + file.getPath(), this.getClass().getName());
            return;
        }

        // 获取当前脚本的相对路径
        // 此相对路径是相对于工程路径而言
        String strDeployFileDirectory = file.getParent();
        String strDeployBaseDirectory = request.getAbsoluteBaseDirectory();
        String relativePath = FileUtility.getRelativePath(strDeployBaseDirectory, strDeployFileDirectory, true);

        if (StringUtility.isNullOrEmpty(relativePath)) {
            relativePath = "";
        }

        // 获取当前文件内容
        String fileContent = FileUtility.readAsString(file.getPath());

        // 定义脚本文件内容id
        String formScriptContentId = null;
        String formScriptVersion = null;

        FormScriptCache formScriptCache;
        // 移动零代码特殊处理
        if (!request.isZeroCodeMobileForm() && !request.isRuntimeMobileForm()) {
            formScriptCache = this.scriptCacheVersionManager.getFormScriptCacheByMulCondition(formProjectCache.getId(), file.getName(), relativePath);
        } else {
            formScriptCache = this.scriptCacheVersionManager.getFormScriptCacheByMulCondition(formProjectCache.getId(), request.getFormCode(), file.getName(), relativePath);
        }

        // 是否新建脚本缓存
        boolean isNewScriptCache = formScriptCache == null;


        // 构造对应的表单缓存脚本文件内容
        FormScriptCacheContent scriptCacheContent = this.createFormScriptCacheContent(fileContent, formScriptVersion, formScriptContentId);
        if (isNewScriptCache) {
            formScriptContentId = RandomUtility.newGuid();
            formScriptVersion = this.scriptCacheVersionManager.generateRandomVersion();
            formScriptCache = this.createFormScriptCache(file.getName(), request.getFormCode(), relativePath, metaDataId, formProjectCache, formScriptContentId, formScriptVersion);

            scriptCacheContent.setId(formScriptContentId);

            // 移动运行时定制，更新脚本文件内容
            if(request.isRuntimeMobileForm()){
                this.updateMobileRtcFormCache(formProjectCache,file ,relativePath,formScriptCache,scriptCacheContent );
            }
            // 保存表单脚本缓存
            this.scriptCacheVersionManager.saveFormScriptCache(formScriptCache);
            this.scriptCacheVersionManager.saveFormScriptCacheContent(scriptCacheContent);

            formProjectCache.setVersion(this.scriptCacheVersionManager.generateRandomVersion());
            formProjectCache.setLastModifyTime(CommonUtility.getCurrentDate());
            this.scriptCacheVersionManager.updateFormProjectCache(formProjectCache);
        } else {
            formScriptContentId = formScriptCache.getScriptContentId();
            formScriptVersion = formScriptCache.getVersion();

            // 从数据库表读取脚本文件内容项
            FormScriptCacheContent formScriptCacheContent = this.scriptCacheVersionManager.getFormScriptCacheContentById(formScriptContentId);
            if (formScriptCacheContent == null) {
                formScriptVersion = this.scriptCacheVersionManager.generateRandomVersion();
                scriptCacheContent.setId(formScriptContentId);

                formScriptCache.setVersion(formScriptVersion);
                formScriptCache.setLastModifyTime(CommonUtility.getCurrentDate());
                formScriptCache.setFormMetadataId(metaDataId);
                formScriptCache.setScriptCode(request.getFormCode());
                // 移动运行时定制，更新脚本文件内容
                if(request.isRuntimeMobileForm()){
                    this.updateMobileRtcFormCache(formProjectCache,file ,relativePath,formScriptCache,scriptCacheContent );
                }
                this.scriptCacheVersionManager.updateFormScriptCache(formScriptCache);
                this.scriptCacheVersionManager.saveFormScriptCacheContent(scriptCacheContent);

                formProjectCache.setVersion(this.scriptCacheVersionManager.generateRandomVersion());
                formProjectCache.setLastModifyTime(CommonUtility.getCurrentDate());
                this.scriptCacheVersionManager.updateFormProjectCache(formProjectCache);
            } else {
                boolean hasSameContent = EqualsUtility.getInstance().equalsWithoutOS(fileContent, formScriptCacheContent.getContent());
                if (!hasSameContent) {
                    formScriptVersion = this.scriptCacheVersionManager.generateRandomVersion();
                    scriptCacheContent.setId(formScriptCacheContent.getId());
                    scriptCacheContent.setLastModifyTime(CommonUtility.getCurrentDate());
                    scriptCacheContent.setContent(fileContent);

                    formScriptCache.setLastModifyTime(CommonUtility.getCurrentDate());
                    formScriptCache.setFormMetadataId(metaDataId);
                    formScriptCache.setVersion(formScriptVersion);
                    formScriptCache.setScriptCode(request.getFormCode());

                    // 移动运行时定制，更新脚本文件内容
                    if(request.isRuntimeMobileForm()){
                        this.updateMobileRtcFormCache(formProjectCache,file ,relativePath,formScriptCache,scriptCacheContent );
                    }
                    this.scriptCacheVersionManager.updateFormScriptCache(formScriptCache);
                    // 保存表单脚本缓存内容
                    this.scriptCacheVersionManager.updateFormScriptCacheContent(scriptCacheContent);

                    formProjectCache.setVersion(this.scriptCacheVersionManager.generateRandomVersion());
                    formProjectCache.setLastModifyTime(CommonUtility.getCurrentDate());
                    this.scriptCacheVersionManager.updateFormProjectCache(formProjectCache);
                }
            }
        }


    }

    /**
     * 更新脚本缓存信息。移动运行时定制场景下使用
     * @param formProjectCache
     * @param file
     * @param relativePath
     */
    private void updateMobileRtcFormCache(FormProjectCache formProjectCache,File file,String relativePath,FormScriptCache currentFormScriptCache,FormScriptCacheContent currentFormScriptCacheContent){
        if(formProjectCache == null || file == null || currentFormScriptCache == null || currentFormScriptCacheContent == null){
            return;
        }

        //更新脚本缓存版本号
        List<FormScriptCache> formScriptCacheList =  this.scriptCacheVersionManager.getFormScriptCachesByMulCondition(formProjectCache.getId(), file.getName(), relativePath);
        if(formScriptCacheList == null || formScriptCacheList.isEmpty()){
            return;
        }
        for(FormScriptCache formScriptCacheItem : formScriptCacheList){
            formScriptCacheItem.setVersion(currentFormScriptCache.getVersion());
            formScriptCacheItem.setLastModifyTime(CommonUtility.getCurrentDate());
            this.scriptCacheVersionManager.updateFormScriptCache(formScriptCacheItem);

            //更新脚本缓存内容
            FormScriptCacheContent formScriptCacheContent = this.scriptCacheVersionManager.getFormScriptCacheContentById(formScriptCacheItem.getScriptContentId());
            if(formScriptCacheContent != null){
                formScriptCacheContent.setContent(currentFormScriptCacheContent.getContent());
                formScriptCacheContent.setLastModifyTime(CommonUtility.getCurrentDate());
                this.scriptCacheVersionManager.updateFormScriptCacheContent(formScriptCacheContent);

            }
        }
    }


    @Override
    public void checkScriptVersion() {

    }

    /**
     * 初始化一个表单工程版本信息
     *
     * @param projectName         工程名称
     * @param projectCode         工程code
     * @param projectRelativePath 工程相对路径
     * @return
     */
    private FormProjectCache createFormProjectCache(String projectName, String projectCode, String projectRelativePath) {
        FormProjectCache formProjectCache = new FormProjectCache();
        formProjectCache.setId(RandomUtility.newGuid());
        formProjectCache.setProjectName(projectName);
        formProjectCache.setProjectCode(projectCode);
        formProjectCache.setProjectRelativePath(projectRelativePath);
        formProjectCache.setVersion(this.scriptCacheVersionManager.generateRandomVersion());
        formProjectCache.setCreateDate(CommonUtility.getCurrentDate());
        formProjectCache.setLastModifyTime(CommonUtility.getCurrentDate());
        return formProjectCache;
    }

    /**
     * 创建对应的表单脚本文件缓存内容
     *
     * @param fileContent
     * @return
     */
    private FormScriptCacheContent createFormScriptCacheContent(String fileContent, String formScriptVersion, String formScriptContentId) {
        FormScriptCacheContent formScriptCacheContent = new FormScriptCacheContent();
        formScriptCacheContent.setId(formScriptContentId);
        formScriptCacheContent.setContent(fileContent);
        formScriptCacheContent.setLastModifyTime(CommonUtility.getCurrentDate());
        formScriptCacheContent.setCreateDate(CommonUtility.getCurrentDate());
        return formScriptCacheContent;
    }


    /**
     * 构造对应的表单脚本文件缓存
     *
     * @param scriptFileName
     * @param scriptCode
     * @param scriptRelativePath
     * @param metaDataId
     * @param formProjectCache*
     * @return
     */
    private FormScriptCache createFormScriptCache(String scriptFileName, String scriptCode, String scriptRelativePath,
                                                  String metaDataId, FormProjectCache formProjectCache, String formScriptCacheContentId, String formScriptVersion) {
        FormScriptCache formScriptCache = new FormScriptCache();
        formScriptCache.setId(RandomUtility.newGuid());
        formScriptCache.setFormMetadataId(metaDataId);

        formScriptCache.setProjectVersionId(formProjectCache.getId());

        formScriptCache.setVersion(formScriptVersion);
        // 设置脚本文件名称
        formScriptCache.setScriptName(scriptFileName);
        // 可以为不包含文件后缀的文件名称
        formScriptCache.setScriptCode(scriptCode);
        // 设置脚本的相对路径 相对于apps而言
        formScriptCache.setScriptRelativePath(scriptRelativePath);

        // 设置对应的脚本文件内容
        formScriptCache.setScriptContentId(formScriptCacheContentId);
        formScriptCache.setLastModifyTime(CommonUtility.getCurrentDate());
        formScriptCache.setCreateDate(CommonUtility.getCurrentDate());

        return formScriptCache;
    }
}
