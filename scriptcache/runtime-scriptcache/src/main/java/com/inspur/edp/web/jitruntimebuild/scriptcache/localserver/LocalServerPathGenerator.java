/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;

/**
 * description:缓存同步 本地文件路径构造
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
public class LocalServerPathGenerator {

    private String _localServerPath;

    /**
     * 是否运行在元数据部署工具
     */
    private boolean isUpgradeTool = false;


    private LocalServerPathGenerator() {
    }

    /**
     * 通过具体实例方式进行方法读取
     *
     * @param isUpgradeTool
     * @return
     */
    public static LocalServerPathGenerator getNewInstance(boolean isUpgradeTool) {
        LocalServerPathGenerator _instance = new LocalServerPathGenerator();
        _instance.isUpgradeTool = isUpgradeTool;
        return _instance;
    }

    /**
     * 获取当前服务器路径
     *
     * @return
     */
    public String getLocalServerPath() {
        // 重复调用得参数  增加缓存定义 避免每次都需要重新获取
        // 获取该参数值得成本相对较高
        if (StringUtility.isNullOrEmpty(_localServerPath)) {
            _localServerPath = FileUtility.getPlatformIndependentPath(FileUtility.getCurrentWorkPath(isUpgradeTool));
        }

        return _localServerPath;
    }

    /**
     * 当前服务器缓存脚本文件路径  不包含文件名称
     *
     * @return 缓存文件路径
     */

    public String getLocalServerFilePath() {
        String currentServerPath = getLocalServerPath();
        String localServerFilePath = FileUtility.combineOptional(currentServerPath, "web", "runtime", "web");
        localServerFilePath = FileUtility.getPlatformIndependentPath(localServerFilePath);
        return localServerFilePath;
    }


    /**
     * 当前服务器缓存脚本文件路径  不包含文件名称
     *
     * @return 缓存文件路径
     */

    public String getLocalServerWebPath() {
        String currentServerPath = getLocalServerPath();
        String localServerFilePath = FileUtility.combine(currentServerPath, "web");
        localServerFilePath = FileUtility.getPlatformIndependentPath(localServerFilePath);
        return localServerFilePath;
    }

    /**
     * 当前服务器缓存脚本文件路径  包含文件名称
     *
     * @return 缓存文件路径
     */

    public String getLocalServerFilePathAndName() {
        String currentServerPath = getLocalServerPath();
        String versionFileName = getLocalServerFileName();
        String localServerFilePath = FileUtility.combineOptional(currentServerPath, "web/runtime/web", versionFileName);
        localServerFilePath = FileUtility.getPlatformIndependentPath(localServerFilePath);
        return localServerFilePath;
    }


    /**
     * 获取放置于服务器环境目录下得脚本缓存文件名称
     *
     * @return
     */
    public String getLocalServerFileName() {
        return "scriptcache.json";
    }
}
