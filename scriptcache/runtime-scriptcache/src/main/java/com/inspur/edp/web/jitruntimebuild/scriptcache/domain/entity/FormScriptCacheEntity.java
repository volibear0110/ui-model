/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity;

import com.inspur.edp.web.common.converter.WebEntityConverter;
import com.inspur.edp.web.common.utility.CommonUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "formscriptcache")
public class FormScriptCacheEntity {

    @Id
    private String id;

    /**
     * 表单元数据id
     */
    private String formMetadataId;

    /**
     * 对应工程缓存id
     */
    private String projectVersionId;

    /**
     * 脚本文件版本
     */
    private String version;

    /**
     * 脚本文件名称
     */
    private String scriptName;

    /**
     * 脚本文件编码
     */
    private String scriptCode;

    /**
     * 脚本文件相对路径
     */
    private String scriptRelativePath;

    /**
     * 脚本文件内容关联id
     */
    private String scriptContentId;

    private Date createDate;

    private String creator;

    private Date lastModifyTime;

    private String lastModifier;

    /**
     * 将FormScriptCacheEntity 转换成为 FormScriptCache
     * @return
     */
    public FormScriptCache convertTo() {
        return new ScriptCacheConverter().convertTo(this);
    }

    /**
     * 将FormScriptCache 转换成为 FormScriptCacheEntity 实体
     * static
     * @param scriptCache
     * @return
     */
    public static FormScriptCacheEntity convertFrom(FormScriptCache scriptCache) {
        return new ScriptCacheConverter().convertFrom(scriptCache);
    }

    private static class ScriptCacheConverter implements WebEntityConverter<FormScriptCacheEntity, FormScriptCache> {

        @Override
        public FormScriptCache convertTo(FormScriptCacheEntity scriptCacheEntity) {
            FormScriptCache scriptCache = new FormScriptCache();
            if (scriptCacheEntity == null) {
                return scriptCache;
            }
            scriptCache.setScriptContent(scriptCacheEntity.getScriptContentId());
            scriptCache.setScriptContentId(scriptCacheEntity.getScriptContentId());
            scriptCache.setScriptRelativePath(scriptCacheEntity.getScriptRelativePath());
            scriptCache.setScriptCode(scriptCacheEntity.getScriptCode());
            scriptCache.setScriptName(scriptCacheEntity.getScriptName());
            scriptCache.setVersion(scriptCacheEntity.getVersion());
            scriptCache.setProjectVersionId(scriptCacheEntity.getProjectVersionId());
            scriptCache.setId(scriptCacheEntity.getId());
            scriptCache.setFormMetadataId(scriptCacheEntity.getFormMetadataId());
            scriptCache.setCreateDate(scriptCacheEntity.getCreateDate());
            scriptCache.setCreator(scriptCacheEntity.getCreator());
            scriptCache.setLastModifier(scriptCacheEntity.getLastModifier());
            scriptCache.setLastModifyTime(scriptCacheEntity.getLastModifyTime());
            return scriptCache;
        }

        @Override
        public FormScriptCacheEntity convertFrom(FormScriptCache scriptCache) {
            FormScriptCacheEntity cacheEntity = new FormScriptCacheEntity();
            if (scriptCache == null) {
                return cacheEntity;
            }
            cacheEntity.setCreateDate(scriptCache.getCreateDate());
            cacheEntity.setCreator(scriptCache.getCreator());
            cacheEntity.setFormMetadataId(scriptCache.getFormMetadataId());
            cacheEntity.setId(scriptCache.getId());
            cacheEntity.setLastModifier(scriptCache.getLastModifier());
            if (scriptCache.getLastModifyTime() == null) {
                cacheEntity.setLastModifyTime(CommonUtility.getCurrentDate());
            } else {
                cacheEntity.setLastModifyTime(scriptCache.getLastModifyTime());
            }

            cacheEntity.setProjectVersionId(scriptCache.getProjectVersionId());
            cacheEntity.setScriptCode(scriptCache.getScriptCode());
            cacheEntity.setScriptContentId(scriptCache.getScriptContentId());
            cacheEntity.setScriptName(scriptCache.getScriptName());
            cacheEntity.setScriptRelativePath(scriptCache.getScriptRelativePath());
            cacheEntity.setVersion(scriptCache.getVersion());
            return cacheEntity;
        }
    }
}
