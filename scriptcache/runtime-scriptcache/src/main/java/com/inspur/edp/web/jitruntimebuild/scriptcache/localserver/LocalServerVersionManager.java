/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;


import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.engine.core.BefEngineInfoCache;
import com.inspur.edp.bff.engine.core.cache.BffEngineCacheService;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.EqualsUtility;
import com.inspur.edp.web.common.utility.ListUtility;
import com.inspur.edp.web.common.utility.ResourceLocalizeUtil;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.formmetadata.metadata.FormMetadataContent;
import com.inspur.edp.web.formmetadata.metadata.formdom.FormDOM;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.constant.I18nMsgConstant;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.ScriptCacheResponse;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.rpc.LocalServerVersionRpcService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.CustomizationCacheServiceInstanceManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.ScriptCacheVersionManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.rpc.client.RpcClassHolder;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * description:服务器脚本版本manager
 * 注册为Bean
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
public class LocalServerVersionManager {

    private static final String CurrentClassName = LocalServerVersionManager.class.getName();

    private static final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);


    private LocalServerVersionRpcService localServerVersionRpcService;

    @Resource
    private ScriptCacheVersionManager scriptCacheVersionManagerInstance;

    @Resource
    private RpcClassHolder rpcClassHolder;

    @PostConstruct
    private  void init(){
        // 此参数不能通过构造函数获取
        this.localServerVersionRpcService = rpcClassHolder.getRpcClass("bcc", LocalServerVersionRpcService.class);
    }

    public LocalServerVersionManager() {

    }


    /**
     * 本地脚本文件与数据库脚本文件进行比较
     *
     * @param formMetadataId
     */
    public ScriptCacheResponse checkVersionWithFormMetadataId(String formMetadataId) {
        GspMetadata gspMetadata = customizationService.getMetadata(formMetadataId);
        List<String> extFormIds = new ArrayList<>();
        // 递归获取所有的组合表单元数据id
        getExtFormIdList(gspMetadata, extFormIds);
        if (!extFormIds.isEmpty()) {
            // 拉取组合表单脚本文件信息
            extFormIds.forEach(this::getScriptCacheResponse);
        }
        return getScriptCacheResponse(formMetadataId);
    }

    private ScriptCacheResponse getScriptCacheResponse(String formMetadataId) {
        ScriptCacheResponse cacheResponse = ScriptCacheResponse.getInstance();

        if (StringUtility.isNullOrEmpty(formMetadataId)) {
            cacheResponse.setSuccess(false);
            cacheResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0001));
            return cacheResponse;
        }
        // 获取数据库中存储的脚本文件信息
        List<FormScriptCache> formScriptCacheList = this.localServerVersionRpcService.getFormScriptCacheByMetadataId(formMetadataId);
        if (ListUtility.isEmpty(formScriptCacheList)) {
            cacheResponse.setSuccess(false);
            String errorMessage = ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0002, formMetadataId);
            WebLogger.Instance.error(errorMessage);
            errorMessage = ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0003, formMetadataId);
            cacheResponse.setErrorMessage(errorMessage);
            return cacheResponse;
        }

        FormProjectCache formProjectCache = this.localServerVersionRpcService.getFormProjectCacheById(formScriptCacheList.get(0).getProjectVersionId());
        if (formProjectCache == null) {
            cacheResponse.setSuccess(false);
            cacheResponse.setErrorMessage(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0004, formMetadataId));
            return cacheResponse;
        }

        cacheResponse = checkProjectVersion(formScriptCacheList, formProjectCache);

        return cacheResponse;
    }

    private static void getExtFormIdList(GspMetadata gspMetadata, List<String> extForm) {
        if (gspMetadata != null) {
            FormMetadataContent content = (FormMetadataContent) gspMetadata.getContent();
            FormDOM dom = SerializeUtility.getInstance().deserialize(content.getContents().toString(), FormDOM.class);
            if (dom != null) {
                List<HashMap<String, Object>> externalComponents = dom.getModule().getExternalComponents();
                if (ListUtility.isNotEmpty(externalComponents)) {
                    for (HashMap<String, Object> externalComponent : externalComponents) {
                        Object rtcFileRelativePath = externalComponent.get("rtcFileRelativePath");
                        if (rtcFileRelativePath != null) {
                            String rtcFileRelativePathStr = (String) rtcFileRelativePath;
                            if (!rtcFileRelativePathStr.isEmpty()) {
                                Object uri = externalComponent.get("uri");
                                if (uri != null) {
                                    String uriStr = (String) uri;
                                    if (!uriStr.isEmpty() && !extForm.contains(uriStr)) {
                                        extForm.add(uriStr);
                                        GspMetadata extFormMd = customizationService.getMetadata(uriStr);
                                        getExtFormIdList(extFormMd, extForm);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private ScriptCacheResponse checkProjectVersion(List<FormScriptCache> formScriptCacheList, FormProjectCache formProjectCache) {
        ScriptCacheResponse cacheResponse = ScriptCacheResponse.getInstance();

        String formProjectVersion = formProjectCache.getVersion();

        //获取本地的工程缓存信息
        //根据工程名和工程路径获取工程的版本信息
        LocalServerProjectVersion localServerProjectVersion = LocalServerVersionFileOperation.getSingleInstance().getProjectVersionInfo(formProjectCache.getProjectName(), formProjectCache.getProjectRelativePath());

        boolean needReSaveFile = false;
        // 注意 本地工程版本可能会为空 因为在初始化时 该参数即为空
        if (localServerProjectVersion == null) {
            localServerProjectVersion = LocalServerProjectVersion.getInstance();
            localServerProjectVersion.setProjectName(formProjectCache.getProjectName());
            localServerProjectVersion.setProjectRelativePath(formProjectCache.getProjectRelativePath());
            // 设定版本为新的随机数值
            localServerProjectVersion.setVersion(this.scriptCacheVersionManagerInstance.generateRandomVersion());
            needReSaveFile = true;
        }
        // 获取当前工程本地存储的工程版本
        String currentLocalServerProjectVersion = localServerProjectVersion.getVersion();
        boolean needUpdateMetadataVersion = true;
        // 比较工程版本
        if (!EqualsUtility.getInstance().equalsWithCaseSensitive(formProjectVersion, currentLocalServerProjectVersion, false)) {
            // 如果工程版本发生变化 那么更新工程版本
            localServerProjectVersion.setVersion(formProjectCache.getVersion());
            needReSaveFile = true;
            // 如果工程版本不同 那么进行脚本文件的检测
            // 否则认为没有发生任何变化
            AtomicBoolean existsInLocal = new AtomicBoolean(false);

            // 已执行更新的元数据id列表
            List<String> updatedMetadataList = new ArrayList<>();
            // 针对从数据库表获取到的表单脚本文件
            for (FormScriptCache t : formScriptCacheList) {
                existsInLocal.set(false);

                // 针对本地存储的脚本版本列表
                for (LocalServerScriptVersion m : localServerProjectVersion.getScriptVersionList()) {
                    // 如果存在对应的项  那么比较版本
                    if (EqualsUtility.getInstance().equalsWithoutOS(t.getScriptName(), m.getScriptName()) &&
                            EqualsUtility.getInstance().equalsWithoutOS(t.getScriptRelativePath(), m.getScriptPath())) {
                        existsInLocal.set(true);
                        // 表示版本发生了变化 那么需要重新获取最新脚本文件
                        if (!EqualsUtility.getInstance().equalsWithoutOS(t.getVersion(), m.getVersion())) {
                            needReSaveFile = true;
                            m.setVersion(t.getVersion());
                            // 获取最新的脚本文件内容 并保存至本地server
                            // 获取数据库存储的问价内容脚本
                            FormScriptCacheContent formScriptCacheContent = this.localServerVersionRpcService.getFormScriptCacheContentById(t.getScriptContentId());
                            if (formScriptCacheContent != null) {
                                // 构造脚本文件路径
                                saveScriptFileWithContent(formProjectCache, t, formScriptCacheContent, currentLocalServerProjectVersion, needUpdateMetadataVersion, updatedMetadataList);
                                if (needUpdateMetadataVersion) {
                                    needUpdateMetadataVersion = false;
                                }

                            } else {
                                WebLogger.Instance.warn(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0005,
                                        t.getScriptContentId()), CurrentClassName);
                            }
                        }
                        break;
                    }
                }


                // 如果是新增的脚本文件
                if (!existsInLocal.get()) {
                    LocalServerScriptVersion localServerScriptVersion = new LocalServerScriptVersion();
                    localServerScriptVersion.setVersion(t.getVersion());
                    localServerScriptVersion.setFormMetadataId(t.getFormMetadataId());
                    localServerScriptVersion.setScriptName(t.getScriptName());
                    localServerScriptVersion.setScriptPath(t.getScriptRelativePath());
                    localServerProjectVersion.getScriptVersionList().add(localServerScriptVersion);
                    needReSaveFile = true;

                    // 如果为新增的脚本文件配置 那么保存至本地脚本文件中
                    FormScriptCacheContent formScriptCacheContent = this.localServerVersionRpcService.getFormScriptCacheContentById(t.getScriptContentId());
                    if (formScriptCacheContent != null) {
                        // 构造脚本文件路径
                        saveScriptFileWithContent(formProjectCache, t, formScriptCacheContent, currentLocalServerProjectVersion, needUpdateMetadataVersion, updatedMetadataList);
                        if (needUpdateMetadataVersion) {
                            needUpdateMetadataVersion = false;
                        }
                    }

                }
            }
        } else {
            WebLogger.Instance.info("checkProjectVersion  equalsWithCaseSensitive:false  formProjectVersion:" + formProjectVersion + "  currentLocalServerProjectVersion:" + currentLocalServerProjectVersion, CurrentClassName);
        }
        // 同步更新本地文件版本  如果需要同步回写脚本文件数据 那么重新保存至本地文件
        if (needReSaveFile) {
            LocalServerVersionFileOperation.getSingleInstance().saveProjectVersion(localServerProjectVersion);
        }

        return cacheResponse;
    }

    /**
     * 更新元数据版本
     *
     * @param metadataId
     * @param version
     */
    private void updateMetadataVersion(String metadataId, String version) {
        WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0006), CurrentClassName);
        try {
            CustomizationCacheServiceInstanceManager.getCustomizationCacheInstance().checkGlobalVersion(metadataId, version);

            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0007,
                    metadataId, version), CurrentClassName);

            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0008, metadataId), CurrentClassName);
            clearDBOAndBECache(metadataId);
        } catch (Exception e) {
            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0009,
                    metadataId, version), CurrentClassName);
            throw e;
        }
    }

    private void clearDBOAndBECache(String metaDataId) {
        GspMetadata formMetadata = MetadataUtility.getInstance().getMetadataWithRuntime(metaDataId);
        if (formMetadata != null) {
            FormMetadataContent formMetadataContent = (FormMetadataContent) formMetadata.getContent();
            JsonNode content = formMetadataContent.getContents();
            String voId = content.at("/module/schemas/0/id").textValue();
            if (!StringUtility.isNullOrEmpty(voId)) {
                GspMetadata vo = MetadataUtility.getInstance().getMetadataWithRuntime(voId);
                //vo缓存清理
                BffEngineCacheService.remove(voId);
                WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0010, voId), CurrentClassName);
                if (vo != null) {
                    String beId = ((GspViewModel) vo.getContent()).getMapping().getTargetMetadataId();
                    if (!StringUtility.isNullOrEmpty(beId)) {
                        GspMetadata beMetadata = MetadataUtility.getInstance().getMetadataWithRuntime(beId);
                        // 如果获取到BE元数据
                        if (beMetadata != null) {
                            IDatabaseObjectRtService databaseObjectRtService = SpringBeanUtils.getBean(IDatabaseObjectRtService.class);
                            List<IGspCommonObject> allObjectList = ((GspBusinessEntity) beMetadata.getContent()).getAllObjectList();
                            allObjectList.forEach(obj -> {
                                WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0011,
                                        obj.getRefObjectName()), CurrentClassName);
                                databaseObjectRtService.clearDatabaseObjectContentById(obj.getRefObjectName());
                            });
                        }
                        //清理be元数据
                        WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0012,
                                beId), CurrentClassName);
                        BefEngineInfoCache.removeBefEngineInfo(beId);
                    }
                }
            }
        }

        //databaseObjectRtService.clearDatabaseObjectContentById(dboid);
    }


    /**
     * 保存脚本文件至本地文件路径
     *
     * @param formProjectCache
     * @param t
     * @param formScriptCacheContent
     */
    private void saveScriptFileWithContent(FormProjectCache formProjectCache, FormScriptCache t, FormScriptCacheContent formScriptCacheContent, String currentLocalServerProjectVersion, boolean needUpdateMedataVersion, List<String> updatedMetadataList) {

        //if (needUpdateMedataVersion) {
        // 如果该元数据未进行更新 那么执行更新 否则不进行更新，主要针对一个元数据id对应多个文件的情况
        // 避免元数据id的重复更新产生的性能问题
        if (!updatedMetadataList.contains(t.getFormMetadataId())) {
            // 执行元数据id更新
            updateMetadataVersion(t.getFormMetadataId(), currentLocalServerProjectVersion);
            // 将更新的元数据id保存至更新列表中
            updatedMetadataList.add(t.getFormMetadataId());
        }
        //}

        String strProjectRelativePath = formProjectCache.getProjectRelativePath();
        String strScriptRelativePath = t.getScriptRelativePath();
        String strRelativePath = FileUtility.combine(strProjectRelativePath, strScriptRelativePath);
        LocalServerFileOperation.saveFile(t.getScriptName(), strRelativePath, formScriptCacheContent.getContent());
    }


    /**
     * 检测工程下所有表单的依赖
     *
     * @param projectName
     * @param strProjectRelativePath
     * @return
     */
    public ScriptCacheResponse checkVersionWithProjectNameAndRelativePath(String projectName, String strProjectRelativePath) {

        ScriptCacheResponse scriptCacheResponse = ScriptCacheResponse.getInstance();

        strProjectRelativePath = FileUtility.getPlatformIndependentPath(strProjectRelativePath);
        FormProjectCache formProjectCache = this.localServerVersionRpcService.getByProjectNameAndProjectRelativePath(projectName, strProjectRelativePath);
        if (formProjectCache == null) {
            WebLogger.Instance.info(ResourceLocalizeUtil.getString(I18nMsgConstant.WEB_SCRIPT_CACHE_MSG_0013,
                    projectName),
                    CurrentClassName);
            return scriptCacheResponse;
        }
        List<FormScriptCache> formScriptCacheList = this.localServerVersionRpcService.getFormScriptCacheByProjectVersionId(formProjectCache.getId());
        scriptCacheResponse = checkProjectVersion(formScriptCacheList, formProjectCache);
        return scriptCacheResponse;
    }
}
