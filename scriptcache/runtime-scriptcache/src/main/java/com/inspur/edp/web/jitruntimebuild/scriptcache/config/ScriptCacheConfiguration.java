/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.config;

import com.inspur.edp.web.jitruntimebuild.scriptcache.api.service.ScriptCacheService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormProjectCacheManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormScriptCacheContentManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormScriptCacheManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormProjectCacheRepository;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormScriptCacheContentRepository;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormScriptCacheRepository;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.LocalServerVersionManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.rpc.LocalServerVersionRpcService;
import com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.rpc.LocalServerVersionRpcServiceImpl;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.ScriptCacheVersionManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.service.ScriptCacheServiceImpl;
import com.inspur.edp.web.jitruntimebuild.scriptcache.webservice.ScriptCacheWebServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration(proxyBeanMethods = false)
@EnableJpaRepositories("com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository")
@EntityScan({"com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity"})
public class ScriptCacheConfiguration {
    @Bean
    public FormProjectCacheManager formProjectCacheManager(FormProjectCacheRepository repository) {
        return new FormProjectCacheManager(repository);
    }

    @Bean
    public FormScriptCacheContentManager formScriptCacheContentManager(FormScriptCacheContentRepository repository) {
        return new FormScriptCacheContentManager(repository);
    }

    @Bean
    public FormScriptCacheManager formScriptCacheManager(FormScriptCacheRepository repository) {
        return new FormScriptCacheManager(repository);
    }

    @Bean
    public ScriptCacheVersionManager scriptCacheVersionManager() {
        return new ScriptCacheVersionManager();
    }

    @Bean
    public LocalServerVersionManager localServerVersionManager() {
        return new LocalServerVersionManager();
    }

    @Bean
    public ScriptCacheService scriptCacheServiceImp() {
        return new ScriptCacheServiceImpl();
    }

    @Bean
    public ScriptCacheWebServiceImpl scriptCacheWebServiceImp() {
        return new ScriptCacheWebServiceImpl();
    }


    @Bean
    public RESTEndpoint scriptCacheWebServiceEndpoint(ScriptCacheWebServiceImpl webservice) {
        return new RESTEndpoint("/runtime/jit/v1.0/scriptcache", webservice);
    }

    @Bean()
    public LocalServerVersionRpcService localServerVersionRpcService() {
        return new LocalServerVersionRpcServiceImpl();
    }

}
