/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository;

import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheEntity;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/10/01
 */
public interface FormScriptCacheRepository extends DataRepository<FormScriptCacheEntity, String> {

    /**
     * 保存脚本内容缓存
     *
     * @param entity
     * @return
     */
    @Override
    FormScriptCacheEntity save(FormScriptCacheEntity entity);


    /**
     * 根据id删除对应的数据
     *
     * @param id
     */
    @Modifying
    @Query(value = "DELETE FROM FormScriptCacheEntity WHERE id = :id")
    void deleteFormScriptCacheById(@Param("id") String id);

    /**
     * 更新工程版本缓存
     *
     * @param id
     * @param version
     * @param lastModifier
     * @param lastModifyTime
     */
    @Modifying
    @Query(value = "UPDATE FormScriptCacheEntity SET version = :version , lastModifier= :lastModifier, lastModifyTime=:lastModifyTime,formMetadataId=:formMetadataId,scriptCode=:scriptCode WHERE id=:id")
    void updateFormScriptCacheVersion(@Param("id") String id, @Param("version") String version, @Param("lastModifier") String lastModifier, @Param("formMetadataId") String formMetadataId, @Param("scriptCode") String scriptCode, @Param("lastModifyTime") Date lastModifyTime);

    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE id= :id")
    Optional<FormScriptCacheEntity> findFormScriptCacheById(@Param("id") String id);

    /**
     * 根据元数据id、工程versionid、脚本名称、脚本相对路径
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName ")
    Optional<FormScriptCacheEntity> getFormScriptCacheByMulCondition(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName);

    /**
     * 根据元数据id、工程versionid、脚本名称、脚本相对路径
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName  and scriptcode= :scriptcode")
    Optional<FormScriptCacheEntity> getFormScriptCacheByMulConditionAndScriptCode(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName,@Param("scriptcode")String scriptCode);

    /**
     * 根据元数据id、工程versionid、脚本名称、脚本相对路径
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName and scriptrelativepath= :scriptrelativepath")
    Optional<FormScriptCacheEntity> getFormScriptCacheByMulCondition(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName,@Param("scriptrelativepath") String scriptRelativePath);

    /**
     * 根据元数据id、工程versionid、脚本名称
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName")
    List<FormScriptCacheEntity>  getFormScriptCachesByMulCondition(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName);


    /**
     * 根据元数据id、工程versionid、脚本名称、脚本相对路径
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName and scriptrelativepath= :scriptrelativepath")
    List<FormScriptCacheEntity>  getFormScriptCachesByMulCondition(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName,@Param("scriptrelativepath") String scriptRelativePath);

    /**
     * 根据元数据id、工程versionid、脚本名称、脚本相对路径
     *
     * @param projectVersionId
     * @param scriptName
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE  projectversionid= :projectVersionId and scriptname= :scriptName and scriptrelativepath= :scriptrelativepath and scriptcode= :scriptcode")
    Optional<FormScriptCacheEntity> getFormScriptCacheByMulConditionAndScriptCode(@Param("projectVersionId") String projectVersionId, @Param("scriptName") String scriptName,@Param("scriptrelativepath") String scriptRelativePath,@Param("scriptcode") String scriptCode);

    /**
     * 根据表单元数据id获取对应的脚本缓存列表
     *
     * @param formMetadataId
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE formmetadataid= :formmetadataid ")
    List<FormScriptCacheEntity> getFormScriptCacheByMetadataId(@Param("formmetadataid") String formMetadataId);

    /**
     * 根据工程脚本缓存d获取对应的脚本缓存列表
     *
     * @param projectVersionId
     * @return
     */
    @Query(value = "SELECT a FROM FormScriptCacheEntity a WHERE projectversionid= :projectVersionId ")
    List<FormScriptCacheEntity> getFormScriptCacheByProjectVersionId(@Param("projectVersionId") String projectVersionId);
}
