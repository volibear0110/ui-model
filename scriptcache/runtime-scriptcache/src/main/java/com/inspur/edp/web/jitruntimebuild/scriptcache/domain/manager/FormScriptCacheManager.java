/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormScriptCacheRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public class FormScriptCacheManager {
    private final FormScriptCacheRepository repository;

    public FormScriptCacheManager(FormScriptCacheRepository repository) {
        this.repository = repository;
    }

    /**
     * 保存表单脚本缓存
     *
     * @param formScriptCacheEntity
     */
    public void save(FormScriptCacheEntity formScriptCacheEntity) {
        this.repository.save(formScriptCacheEntity);
    }

    /**
     * 保存表单脚本缓存
     *
     * @param formScriptCacheEntity
     */
    @Transactional
    public void update(FormScriptCacheEntity formScriptCacheEntity) {
        this.repository.updateFormScriptCacheVersion(formScriptCacheEntity.getId(), formScriptCacheEntity.getVersion(), formScriptCacheEntity.getLastModifier(), formScriptCacheEntity.getFormMetadataId(), formScriptCacheEntity.getScriptCode(), formScriptCacheEntity.getLastModifyTime());
    }


    /**
     * 根据多个条件参数获取对应的脚本文件缓存信息
     *
     * @param projectVersionId
     * @param scriptName
     * @param scriptRelativePath
     * @return
     */
    public FormScriptCache getFormScriptCacheByMulCondition(String projectVersionId, String scriptCode, String scriptName, String scriptRelativePath) {

        scriptRelativePath = FileUtility.getPlatformIndependentPath(scriptRelativePath);

        Optional<FormScriptCacheEntity> formScriptCacheEntity;
        if (StringUtility.isNullOrEmpty(scriptRelativePath)) {
            if (StringUtility.isNullOrEmpty(scriptCode)) {
                formScriptCacheEntity = this.repository.getFormScriptCacheByMulCondition(projectVersionId, scriptName);
            } else {
                formScriptCacheEntity = this.repository.getFormScriptCacheByMulConditionAndScriptCode(projectVersionId, scriptName, scriptCode);
            }

        } else {
            if (StringUtility.isNullOrEmpty(scriptCode)) {
                formScriptCacheEntity = this.repository.getFormScriptCacheByMulCondition(projectVersionId, scriptName, scriptRelativePath);
            } else {
                formScriptCacheEntity = this.repository.getFormScriptCacheByMulConditionAndScriptCode(projectVersionId, scriptName, scriptRelativePath, scriptCode);
            }
        }

        if (formScriptCacheEntity == null || !formScriptCacheEntity.isPresent()) {
            return null;
        }

        return formScriptCacheEntity.get().convertTo();
    }

    /**
     * 根据多个条件参数获取对应的脚本文件缓存信息
     *
     * @param projectVersionId
     * @param scriptName
     * @param scriptRelativePath
     * @return
     */
    public List<FormScriptCache> getFormScriptCachesByMulCondition(String projectVersionId, String scriptName, String scriptRelativePath) {
        scriptRelativePath = FileUtility.getPlatformIndependentPath(scriptRelativePath);

        List<FormScriptCacheEntity> formScriptCacheEntityList = null;

        if (StringUtility.isNullOrEmpty(scriptRelativePath)) {
            formScriptCacheEntityList = this.repository.getFormScriptCachesByMulCondition(projectVersionId, scriptName);
        } else {
            formScriptCacheEntityList = this.repository.getFormScriptCachesByMulCondition(projectVersionId, scriptName, scriptRelativePath);
        }

        if (formScriptCacheEntityList == null || formScriptCacheEntityList.isEmpty()) {
            return null;
        }

        List<FormScriptCache> formScriptCacheList = new ArrayList<>();
        // 创建并行流 进行数据转换
        formScriptCacheEntityList.stream().filter(Objects::nonNull).forEach((cacheEntityItem) -> formScriptCacheList.add(cacheEntityItem.convertTo()));
        return formScriptCacheList;
    }

    /**
     * 根据表单元数据id获取对应的脚本缓存列表
     *
     * @param formMetadataId
     * @return
     */
    public List<FormScriptCache> getFormScriptCacheByMetadataId(String formMetadataId) {
        List<FormScriptCacheEntity> formScriptCacheEntityList = this.repository.getFormScriptCacheByMetadataId(formMetadataId);
        if (formScriptCacheEntityList == null) {
            return null;
        }
        List<FormScriptCache> formScriptCacheList = new ArrayList<>();
        // 创建并行流 进行数据转换
        formScriptCacheEntityList.stream().filter(Objects::nonNull).forEach((cacheEntityItem) -> formScriptCacheList.add(cacheEntityItem.convertTo()));
        return formScriptCacheList;
    }

    /**
     * 根据工程脚本缓存获取对应的脚本缓存列表
     *
     * @param projectVersionId
     * @return
     */
    public List<FormScriptCache> getFormScriptCacheByProjectVersionId(String projectVersionId) {
        List<FormScriptCacheEntity> formScriptCacheEntityList = this.repository.getFormScriptCacheByProjectVersionId(projectVersionId);
        if (formScriptCacheEntityList == null) {
            return null;
        }
        List<FormScriptCache> formScriptCacheList = new ArrayList<>();
        formScriptCacheEntityList.stream().filter(Objects::nonNull).forEach((cacheEntityItem) -> formScriptCacheList.add(cacheEntityItem.convertTo()));
        return formScriptCacheList;
    }
}
