/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity;

import com.inspur.edp.web.common.converter.WebEntityConverter;
import com.inspur.edp.web.common.utility.CommonUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "formprojectcache")
public class FormProjectCacheEntity {
    /**
     * 主键字段
     */
    @Id
    private String id;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 工程编码
     */
    private String projectCode;

    /**
     * 工程相对路径
     */
    private String projectRelativePath;

    /**
     * 工程对应版本
     */
    private String version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectRelativePath() {
        return projectRelativePath;
    }

    public void setProjectRelativePath(String projectRelativePath) {
        this.projectRelativePath = projectRelativePath;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 最后修改人
     */
    private String lastModifier;

    /**
     * 最后修改时间
     */
    private Date lastModifyTime;

    /**
     * 将当前实体转换成为ProjectCache实例
     * @return
     */
    public FormProjectCache convertTo() {
        return new ProjectCacheConverter().convertTo(this);
    }

    /**
     * 将ProjectCache实例转换为当前实例
     * 完成实体的转换操作
     * static 方法
     * @param projectCache
     * @return
     */
    public static FormProjectCacheEntity convertFrom(FormProjectCache projectCache) {
        return new ProjectCacheConverter().convertFrom(projectCache);
    }

    /**
     * 使用内部类 封装对应的转换操作
     */
    private static class ProjectCacheConverter implements WebEntityConverter<FormProjectCacheEntity, FormProjectCache> {

        @Override
        public FormProjectCache convertTo(FormProjectCacheEntity source) {
            FormProjectCache projectCache = new FormProjectCache();
            if (source == null) {
                return projectCache;
            }
            projectCache.setId(source.getId());
            projectCache.setCreateDate(source.getCreateDate());
            projectCache.setCreator(source.getCreator());
            projectCache.setLastModifier(source.getLastModifier());
            projectCache.setLastModifyTime(CommonUtility.getCurrentDate());
            projectCache.setProjectCode(source.getProjectCode());
            projectCache.setProjectName(source.getProjectName());
            projectCache.setProjectRelativePath(source.getProjectRelativePath());
            projectCache.setVersion(source.getVersion());
            return projectCache;
        }

        @Override
        public FormProjectCacheEntity convertFrom(FormProjectCache projectCache) {
            FormProjectCacheEntity projectCacheEntity = new FormProjectCacheEntity();
            if (projectCache == null) {
                return projectCacheEntity;
            }
            projectCacheEntity.setId(projectCache.getId());
            projectCacheEntity.setCreateDate(projectCache.getCreateDate());
            projectCacheEntity.setCreator(projectCache.getCreator());
            projectCacheEntity.setLastModifier(projectCache.getLastModifier());
            if (projectCache.getLastModifyTime() == null) {
                projectCacheEntity.setLastModifyTime(CommonUtility.getCurrentDate());
            } else {
                projectCacheEntity.setLastModifyTime(projectCache.getLastModifyTime());
            }

            projectCacheEntity.setProjectCode(projectCache.getProjectCode());
            projectCacheEntity.setProjectName(projectCache.getProjectName());
            projectCacheEntity.setProjectRelativePath(projectCache.getProjectRelativePath());
            projectCacheEntity.setVersion(projectCache.getVersion());
            return projectCacheEntity;
        }
    }

}

