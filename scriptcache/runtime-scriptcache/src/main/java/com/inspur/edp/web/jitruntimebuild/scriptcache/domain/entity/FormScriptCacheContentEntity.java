/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity;

import com.inspur.edp.web.common.converter.WebEntityConverter;
import com.inspur.edp.web.common.encrypt.EncryptUtility;
import com.inspur.edp.web.common.utility.CommonUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "formscriptcachecontent")
public class FormScriptCacheContentEntity {
    /**
     * 主键id
     */
    @Id
    private String id;

    /**
     * 文件内容
     */
    private String content;

    /**
     * 创建时间
     */
    private Date createDate;

    private String creator;

    private String lastModifier;

    private Date lastModifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    /**
     * 将FormScriptCacheContentEntity 转换成为FormScriptCacheContent
     * @return
     */
    public FormScriptCacheContent convertTo() {
        return new ScriptCacheContentConverter().convertTo(this);
    }

    public static FormScriptCacheContentEntity convertFrom(FormScriptCacheContent cacheContent) {
        return new ScriptCacheContentConverter().convertFrom(cacheContent);
    }

    /**
     * 使用内部类  封装转换操作
     * 不对外提供
     */
    private static class ScriptCacheContentConverter implements WebEntityConverter<FormScriptCacheContentEntity, FormScriptCacheContent> {

        @Override
        public FormScriptCacheContent convertTo(FormScriptCacheContentEntity contentEntity) {
            FormScriptCacheContent cacheContent = new FormScriptCacheContent();
            if (contentEntity == null) {
                return cacheContent;
            }

            String decodeContent = contentEntity.getContent();
            // 获取到的脚本永远进行解码操作
            if (EncryptUtility.getInstance().isBase64(decodeContent)) {
                decodeContent = EncryptUtility.getInstance().Base64Decode(decodeContent);
            }
            cacheContent.setContent(decodeContent);
            cacheContent.setId(contentEntity.getId());
            cacheContent.setCreateDate(contentEntity.getCreateDate());
            cacheContent.setCreator(contentEntity.getCreator());
            cacheContent.setLastModifier(contentEntity.getLastModifier());
            cacheContent.setLastModifyTime(contentEntity.getLastModifyTime());
            return cacheContent;
        }

        @Override
        public FormScriptCacheContentEntity convertFrom(FormScriptCacheContent scriptCacheContent) {
            FormScriptCacheContentEntity cacheContentEntity = new FormScriptCacheContentEntity();
            if (scriptCacheContent == null) {
                return cacheContentEntity;
            }
            String encodeContent = scriptCacheContent.getContent();
            // 如果内容进行了base64编码 那么进行base64解码之后返回
            // 保存的脚本内容永远进行base64编码
            encodeContent = EncryptUtility.getInstance().Base64Encode(encodeContent);

            cacheContentEntity.setContent(encodeContent);
            cacheContentEntity.setCreateDate(scriptCacheContent.getCreateDate());
            cacheContentEntity.setCreator(scriptCacheContent.getCreator());
            cacheContentEntity.setId(scriptCacheContent.getId());
            cacheContentEntity.setLastModifier(scriptCacheContent.getLastModifier());
            if (scriptCacheContent.getLastModifyTime() == null) {
                cacheContentEntity.setLastModifyTime(CommonUtility.getCurrentDate());
            } else {
                cacheContentEntity.setLastModifyTime(scriptCacheContent.getLastModifyTime());
            }

            return cacheContentEntity;
        }
    }
}
