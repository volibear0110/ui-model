/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver;

import java.util.ArrayList;
import java.util.List;

/**
 * description: 当前环境下 工程对应的版本信息
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
class LocalServerProjectVersion {
    private LocalServerProjectVersion() {
        this.scriptVersionList = new ArrayList<>();
    }

    public static LocalServerProjectVersion getInstance() {
        return new LocalServerProjectVersion();
    }


    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 工程部署相对路径
     */
    private String projectRelativePath;

    /**
     * 当前server 当前工程对应版本
     */
    private String version;

    /**
     * 工程关联的脚本文件缓存信息列表
     */
    private List<LocalServerScriptVersion> scriptVersionList;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectRelativePath() {
        return projectRelativePath;
    }

    public void setProjectRelativePath(String projectRelativePath) {
        this.projectRelativePath = projectRelativePath;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<LocalServerScriptVersion> getScriptVersionList() {
        return scriptVersionList;
    }

    public void setScriptVersionList(List<LocalServerScriptVersion> scriptVersionList) {
        this.scriptVersionList = scriptVersionList;
    }
}
