/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager;

import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheContentEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.repository.FormScriptCacheContentRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/24
 */
public class FormScriptCacheContentManager {
    private final FormScriptCacheContentRepository repository;

    public FormScriptCacheContentManager(FormScriptCacheContentRepository repository) {
        this.repository = repository;
    }

    /**
     * 保存表单脚本文件缓存内容
     *
     * @param contentEntity
     */
    public void save(FormScriptCacheContentEntity contentEntity) {
        this.repository.save(contentEntity);
    }

    /**
     * 保存表单脚本文件缓存内容
     *
     * @param contentEntity
     */
    @Transactional
    public void update(FormScriptCacheContentEntity contentEntity) {
        this.repository.updateFormScriptCacheContentVersion(contentEntity.getId(), contentEntity.getContent(), contentEntity.getLastModifier(), contentEntity.getLastModifyTime());
    }

    /**
     * 根据contentId获取对应的内容
     *
     * @param scriptContentId
     * @return
     */
    public FormScriptCacheContent findById(String scriptContentId) {
        Optional<FormScriptCacheContentEntity> scriptCacheContentEntity = this.repository.findById(scriptContentId);
        return scriptCacheContentEntity.map(FormScriptCacheContentEntity::convertTo).orElse(null);

    }

}
