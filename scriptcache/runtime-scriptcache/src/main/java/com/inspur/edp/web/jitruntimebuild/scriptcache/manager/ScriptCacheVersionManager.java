/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.manager;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.RandomUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormProjectCacheEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheContentEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.entity.FormScriptCacheEntity;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormProjectCacheManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormScriptCacheContentManager;
import com.inspur.edp.web.jitruntimebuild.scriptcache.domain.manager.FormScriptCacheManager;

import javax.annotation.Resource;
import java.util.List;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/25
 */
public class ScriptCacheVersionManager {

    @Resource
    private FormProjectCacheManager formProjectCacheManager;

    @Resource
    private FormScriptCacheManager formScriptCacheManager;

    @Resource
    private FormScriptCacheContentManager formScriptCacheContentManager;


    public ScriptCacheVersionManager() {
    }

    /**
     * 生成随机数版本号
     *
     * @return
     */
    public String generateRandomVersion() {
        return RandomUtility.newGuid();
    }


    /**
     * 保存工程脚本缓存
     *
     * @param formProjectCache 工程脚本缓存
     */
    public void saveFormProjectCache(FormProjectCache formProjectCache) {

        if (StringUtility.isNullOrEmpty(formProjectCache.getId())) {
            formProjectCache.setId(RandomUtility.newGuid());
        }
        FormProjectCacheEntity projectCacheEntity = FormProjectCacheEntity.convertFrom(formProjectCache);
        this.formProjectCacheManager.save(projectCacheEntity);
    }

    /**
     * 更新工程脚本缓存
     *
     * @param formProjectCache 工程脚本缓存
     */
    public void updateFormProjectCache(FormProjectCache formProjectCache) {
        StringUtility.executeIfPresent(formProjectCache.getId(), StringUtility::isNullOrEmpty, (param) -> formProjectCache.setId(RandomUtility.newGuid()));
        FormProjectCacheEntity projectCacheEntity = FormProjectCacheEntity.convertFrom(formProjectCache);
        this.formProjectCacheManager.save(projectCacheEntity);
    }

    /**
     * 保存表单脚本缓存
     *
     * @param formScriptCache 表单脚本缓存
     */
    public void saveFormScriptCache(FormScriptCache formScriptCache) {
        StringUtility.executeIfPresent(formScriptCache.getId(), StringUtility::isNullOrEmpty, (item) -> {
            formScriptCache.setId(RandomUtility.newGuid());
        });

        FormScriptCacheEntity scriptCacheEntity = FormScriptCacheEntity.convertFrom(formScriptCache);
        this.formScriptCacheManager.save(scriptCacheEntity);
    }

    /**
     * 保存表单脚本缓存
     *
     * @param formScriptCache 表单脚本缓存
     */
    public void updateFormScriptCache(FormScriptCache formScriptCache) {
        StringUtility.executeIfPresent(formScriptCache.getId(), StringUtility::isNullOrEmpty, (item) -> {
            formScriptCache.setId(RandomUtility.newGuid());
        });
        FormScriptCacheEntity scriptCacheEntity = FormScriptCacheEntity.convertFrom(formScriptCache);
        this.formScriptCacheManager.update(scriptCacheEntity);
    }


    /**
     * 保存表单脚本缓存内容
     *
     * @param formScriptCacheContent 表单脚本缓存内容
     */
    public void saveFormScriptCacheContent(FormScriptCacheContent formScriptCacheContent) {
        StringUtility.executeIfPresent(formScriptCacheContent.getId(), StringUtility::isNullOrEmpty, (item) -> {
            formScriptCacheContent.setId(RandomUtility.newGuid());
        });
        FormScriptCacheContentEntity contentEntity = FormScriptCacheContentEntity.convertFrom(formScriptCacheContent);
        this.formScriptCacheContentManager.save(contentEntity);

    }

    /**
     * 保存表单脚本缓存内容
     *
     * @param formScriptCacheContent 表单脚本缓存内容
     */
    public void updateFormScriptCacheContent(FormScriptCacheContent formScriptCacheContent) {
        StringUtility.executeIfPresent(formScriptCacheContent.getId(), StringUtility::isNullOrEmpty, (item) -> {
            formScriptCacheContent.setId(RandomUtility.newGuid());
        });
        FormScriptCacheContentEntity contentEntity = FormScriptCacheContentEntity.convertFrom(formScriptCacheContent);
        this.formScriptCacheContentManager.update(contentEntity);

    }

    /**
     * 根据工程名称及对应得部署相对路径获取对应得工程缓存信息
     *
     * @param projectName
     * @param strProjectRelativePath
     * @return
     */
    public FormProjectCache getByProjectNameAndProjectRelativePath(String projectName, String strProjectRelativePath) {
        // 调整路径分隔符为一致形式
        strProjectRelativePath = FileUtility.getPlatformIndependentPath(strProjectRelativePath);
        return this.formProjectCacheManager.getByProjectNameAndProjectRelativePath(projectName, strProjectRelativePath);
    }

    /**
     * 根据元数据id、工程版本id、脚本文件名称、脚本相对路径获取对应的脚本缓存信息
     *
     * @param projectVersionId
     * @param scriptName
     * @param strScriptRelativePath
     * @return
     */
    public FormScriptCache getFormScriptCacheByMulCondition(String projectVersionId, String scriptName, String strScriptRelativePath) {
        // 脚本相对文件路径
        strScriptRelativePath = FileUtility.getPlatformIndependentPath(strScriptRelativePath);
        return this.formScriptCacheManager.getFormScriptCacheByMulCondition(projectVersionId, null, scriptName, strScriptRelativePath);
    }

    /**
     * 根据元数据id、工程版本id、脚本文件名称、脚本相对路径获取对应的脚本缓存信息
     *
     * @param projectVersionId
     * @param scriptName
     * @param strScriptRelativePath
     * @return
     */
    public FormScriptCache getFormScriptCacheByMulCondition(String projectVersionId, String scriptCode, String scriptName, String strScriptRelativePath) {
        // 脚本相对文件路径
        strScriptRelativePath = FileUtility.getPlatformIndependentPath(strScriptRelativePath);
        return this.formScriptCacheManager.getFormScriptCacheByMulCondition(projectVersionId, scriptCode, scriptName, strScriptRelativePath);
    }

    /**
     * 根据元数据id、工程版本id、脚本文件名称、脚本相对路径获取对应的脚本缓存信息
     *
     * @param projectVersionId
     * @param scriptName
     * @param strScriptRelativePath
     * @return
     */
    public List<FormScriptCache> getFormScriptCachesByMulCondition(String projectVersionId,  String scriptName, String strScriptRelativePath) {
        // 脚本相对文件路径
        strScriptRelativePath = FileUtility.getPlatformIndependentPath(strScriptRelativePath);
        return this.formScriptCacheManager.getFormScriptCachesByMulCondition(projectVersionId, scriptName, strScriptRelativePath);
    }

    /**
     * 根据内容项id获取对应的内容
     *
     * @param contentId
     * @return
     */
    public FormScriptCacheContent getFormScriptCacheContentById(String contentId) {
        return this.formScriptCacheContentManager.findById(contentId);
    }

    /**
     * 根据元数据id获取对应的脚本缓存列表
     *
     * @param formMetadataId
     * @return
     */
    public List<FormScriptCache> getFormScriptCacheByMetadataId(String formMetadataId) {
        return this.formScriptCacheManager.getFormScriptCacheByMetadataId(formMetadataId);
    }

    /**
     * 根据工程缓存id获取对应的工程缓存信息
     *
     * @param formProjectCacheId
     * @return
     */
    public FormProjectCache getFormProjectCacheById(String formProjectCacheId) {
        return this.formProjectCacheManager.getById(formProjectCacheId);
    }

    /**
     * 根据工程脚本缓存id获取对应的脚本缓存信息
     *
     * @param formProjecVersionId
     * @return
     */
    public List<FormScriptCache> getFormScriptCacheByProjectVersionId(String formProjecVersionId) {
        return this.formScriptCacheManager.getFormScriptCacheByProjectVersionId(formProjecVersionId);
    }
}
