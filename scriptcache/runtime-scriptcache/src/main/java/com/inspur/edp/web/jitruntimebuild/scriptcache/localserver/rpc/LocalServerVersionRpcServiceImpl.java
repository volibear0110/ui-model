/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.localserver.rpc;

import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormProjectCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCache;
import com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity.FormScriptCacheContent;
import com.inspur.edp.web.jitruntimebuild.scriptcache.manager.ScriptCacheVersionManager;

import javax.annotation.Resource;
import java.util.List;

public class LocalServerVersionRpcServiceImpl implements LocalServerVersionRpcService {

    @Resource
    private ScriptCacheVersionManager scriptCacheVersionManager;

    public LocalServerVersionRpcServiceImpl() {

    }


    /**
     * 获取文件列表
     *
     * @param formMetadataId rpc参数  表单元数据id
     * @return
     */
    @Override
    public List<FormScriptCache> getFormScriptCacheByMetadataId(String formMetadataId) {
        return this.scriptCacheVersionManager.getFormScriptCacheByMetadataId(formMetadataId);
    }

    /**
     * 根据缓存工程id获取对应的工程版本信息
     *
     * @param projectVersionId
     * @return
     */
    @Override
    public FormProjectCache getFormProjectCacheById(String projectVersionId) {
        return this.scriptCacheVersionManager.getFormProjectCacheById(projectVersionId);
    }

    /**
     * 根据脚本缓存id获取对应的脚本缓存内容
     *
     * @param contentId
     * @return
     */
    @Override
    public FormScriptCacheContent getFormScriptCacheContentById(String contentId) {
        return this.scriptCacheVersionManager.getFormScriptCacheContentById(contentId);
    }

    /**
     * 根据工程名以及对应的工程路径获取工程版本缓存信息
     *
     * @param projectName
     * @param strProjectRelativePath
     * @return
     */
    @Override
    public FormProjectCache getByProjectNameAndProjectRelativePath(String projectName, String strProjectRelativePath) {
        return this.scriptCacheVersionManager.getByProjectNameAndProjectRelativePath(projectName, strProjectRelativePath);
    }

    /**
     * 根据工程版本id信息获取对应的脚本缓存列表
     *
     * @param formProjectVersionId@return
     */
    @Override
    public List<FormScriptCache> getFormScriptCacheByProjectVersionId(String formProjectVersionId) {
        return this.scriptCacheVersionManager.getFormScriptCacheByProjectVersionId(formProjectVersionId);
    }


}
