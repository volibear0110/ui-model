package com.inspur.edp.web.jitruntimebuild.scriptcache.api.constant;

public class I18nExceptionConstant {
    public final static String WEB_SCRIPT_CACHE_ERROR_0001 = "WEB_SCRIPT_CACHE_ERROR_0001";
    public final static String WEB_SCRIPT_CACHE_ERROR_0002 = "WEB_SCRIPT_CACHE_ERROR_0002";
    public final static String WEB_SCRIPT_CACHE_ERROR_0003 = "WEB_SCRIPT_CACHE_ERROR_0003";
    public final static String WEB_SCRIPT_CACHE_ERROR_0004 = "WEB_SCRIPT_CACHE_ERROR_0004";
    public final static String WEB_SCRIPT_CACHE_ERROR_0005 = "WEB_SCRIPT_CACHE_ERROR_0005";
    public final static String WEB_SCRIPT_CACHE_ERROR_0006 = "WEB_SCRIPT_CACHE_ERROR_0006";
    public final static String WEB_SCRIPT_CACHE_ERROR_0007 = "WEB_SCRIPT_CACHE_ERROR_0007";
    public final static String WEB_SCRIPT_CACHE_ERROR_0008 = "WEB_SCRIPT_CACHE_ERROR_0008";
}
