/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity;


import com.inspur.edp.web.common.utility.StringUtility;

import java.util.Date;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/23
 */
public class FormScriptCache {
    /**
     * 主键
     */
    private String id;

    /**
     * 表单元数据id
     */
    private String formMetadataId;

    /**
     * 对应工程缓存id
     */
    private String projectVersionId;

    /**
     * 脚本文件版本
     */
    private String version;

    /**
     * 脚本文件名称
     */
    private String scriptName;

    /**
     * 脚本文件编码
     */
    private String scriptCode;

    /**
     * 脚本文件相对路径
     */
    private String scriptRelativePath;

    /**
     * 脚本文件内容关联id
     */
    private String scriptContentId;

    /**
     * 脚本文件内容
     */
    private String scriptContent;

    private Date createDate;

    private String creator;

    private Date lastModifyTime;

    private String lastModifier;

    public String getId() {
        if (StringUtility.isNullOrEmpty(id)) {
            id = StringUtility.convertNullToEmptyString(id);
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormMetadataId() {
        if (StringUtility.isNullOrEmpty(formMetadataId)) {
            formMetadataId = StringUtility.convertNullToEmptyString(formMetadataId);
        }
        return formMetadataId;
    }

    public void setFormMetadataId(String formMetadataId) {
        this.formMetadataId = formMetadataId;
    }

    public String getProjectVersionId() {
        if (StringUtility.isNullOrEmpty(projectVersionId)) {
            projectVersionId = StringUtility.convertNullToEmptyString(projectVersionId);
        }
        return projectVersionId;
    }

    public void setProjectVersionId(String projectVersionId) {
        this.projectVersionId = projectVersionId;
    }

    public String getVersion() {
        if (StringUtility.isNullOrEmpty(version)) {
            version = StringUtility.convertNullToEmptyString(version);
        }
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getScriptName() {
        if (StringUtility.isNullOrEmpty(scriptName)) {
            scriptName = StringUtility.convertNullToEmptyString(scriptName);
        }
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getScriptCode() {
        if (StringUtility.isNullOrEmpty(scriptCode)) {
            scriptCode = StringUtility.convertNullToEmptyString(scriptCode);
        }
        return scriptCode;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public String getScriptRelativePath() {
        if (StringUtility.isNullOrEmpty(scriptRelativePath)) {
            scriptRelativePath = StringUtility.convertNullToEmptyString(scriptRelativePath);
        }
        return scriptRelativePath;
    }

    public void setScriptRelativePath(String scriptRelativePath) {
        this.scriptRelativePath = scriptRelativePath;
    }

    public String getScriptContentId() {
        if (StringUtility.isNullOrEmpty(scriptContentId)) {
            scriptContentId = StringUtility.convertNullToEmptyString(scriptContentId);
        }
        return scriptContentId;
    }

    public void setScriptContentId(String scriptContentId) {
        this.scriptContentId = scriptContentId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    // 如果未设置最后修改时间 那么设置该时间为当前时间
    public Date getLastModifyTime() {
        if (lastModifyTime == null) {
            lastModifyTime = new Date();
        }
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public String getScriptContent() {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
    }
}
