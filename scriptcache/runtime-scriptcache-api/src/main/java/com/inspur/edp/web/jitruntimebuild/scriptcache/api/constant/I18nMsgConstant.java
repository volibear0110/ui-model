package com.inspur.edp.web.jitruntimebuild.scriptcache.api.constant;

public class I18nMsgConstant {
    public final static String WEB_SCRIPT_CACHE_MSG_0001 = "WEB_SCRIPT_CACHE_MSG_0001";
    public final static String WEB_SCRIPT_CACHE_MSG_0002 = "WEB_SCRIPT_CACHE_MSG_0002";
    public final static String WEB_SCRIPT_CACHE_MSG_0003 = "WEB_SCRIPT_CACHE_MSG_0003";
    public final static String WEB_SCRIPT_CACHE_MSG_0004 = "WEB_SCRIPT_CACHE_MSG_0004";
    public final static String WEB_SCRIPT_CACHE_MSG_0005 = "WEB_SCRIPT_CACHE_MSG_0005";
    public final static String WEB_SCRIPT_CACHE_MSG_0006 = "WEB_SCRIPT_CACHE_MSG_0006";
    public final static String WEB_SCRIPT_CACHE_MSG_0007 = "WEB_SCRIPT_CACHE_MSG_0007";
    public final static String WEB_SCRIPT_CACHE_MSG_0008 = "WEB_SCRIPT_CACHE_MSG_0008";
    public final static String WEB_SCRIPT_CACHE_MSG_0009 = "WEB_SCRIPT_CACHE_MSG_0009";
    public final static String WEB_SCRIPT_CACHE_MSG_0010 = "WEB_SCRIPT_CACHE_MSG_0010";
    public final static String WEB_SCRIPT_CACHE_MSG_0011 = "WEB_SCRIPT_CACHE_MSG_0011";
    public final static String WEB_SCRIPT_CACHE_MSG_0012 = "WEB_SCRIPT_CACHE_MSG_0012";
    public final static String WEB_SCRIPT_CACHE_MSG_0013 = "WEB_SCRIPT_CACHE_MSG_0013";
    public final static String WEB_SCRIPT_CACHE_MSG_0014 = "WEB_SCRIPT_CACHE_MSG_0014";
    public final static String WEB_SCRIPT_CACHE_MSG_0015 = "WEB_SCRIPT_CACHE_MSG_0015";
    public final static String WEB_SCRIPT_CACHE_MSG_0016 = "WEB_SCRIPT_CACHE_MSG_0016";
    public final static String WEB_SCRIPT_CACHE_MSG_0017 = "WEB_SCRIPT_CACHE_MSG_0017";
    public final static String WEB_SCRIPT_CACHE_MSG_0018 = "WEB_SCRIPT_CACHE_MSG_0018";
    public final static String WEB_SCRIPT_CACHE_MSG_0019 = "WEB_SCRIPT_CACHE_MSG_0019";

}
