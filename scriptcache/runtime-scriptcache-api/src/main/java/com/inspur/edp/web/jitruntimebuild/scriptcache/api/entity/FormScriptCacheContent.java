/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.Date;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/23
 */
public class FormScriptCacheContent {
    /**
     * 主键id
     */
    private  String id;

    /**
     * 文件内容
     */
    private  String content;

    /**
     * 创建时间
     */
    private Date createDate;

    private  String creator;

    private  String lastModifier;

    private  Date lastModifyTime;

    public String getId() {
        if (StringUtility.isNullOrEmpty(id)) {
            id = StringUtility.convertNullToEmptyString(id);
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        if (StringUtility.isNullOrEmpty(content)) {
            content = StringUtility.convertNullToEmptyString(content);
        }
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

}
