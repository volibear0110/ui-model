/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity;

/**
 * description:
 *
 * @author Noah Guo
 * @date 2020/09/29
 */
public class PublishScriptRequest {
    // 表单元数据id
    private String metaDataId;

    // 所属工程名称
    private String projectName;

    // 部署脚本相对路径
    private String projectRelativePath;

    // 脚本文件名称
    private String scriptName;

    //关联表单code
    private String formCode;

    /**
     * 是否更新元数据版本
     */
    private boolean updateMetadataVersion = true;

    private String absoluteBaseDirectory;

    /**
     * 是否零代码表单 默认为false
     */
    private boolean isZeroCodeMobileForm = false;

    /**
     * 是否移动运行时定制表单 默认为false
     */
    private boolean isRuntimeMobileForm = false;


    public String getMetaDataId() {
        return metaDataId;
    }

    public void setMetaDataId(String metaDataId) {
        this.metaDataId = metaDataId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectRelativePath() {
        return projectRelativePath;
    }

    public void setProjectRelativePath(String projectRelativePath) {
        this.projectRelativePath = projectRelativePath;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getAbsoluteBaseDirectory() {
        return absoluteBaseDirectory;
    }

    public void setAbsoluteBaseDirectory(String absoluteBaseDirectory) {
        this.absoluteBaseDirectory = absoluteBaseDirectory;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public boolean isUpdateMetadataVersion() {
        return updateMetadataVersion;
    }

    public void setUpdateMetadataVersion(boolean updateMetadataVersion) {
        this.updateMetadataVersion = updateMetadataVersion;
    }

    public boolean isZeroCodeMobileForm() {
        return isZeroCodeMobileForm;
    }

    public void setZeroCodeMobileForm(boolean zeroCodeMobileForm) {
        isZeroCodeMobileForm = zeroCodeMobileForm;
    }

    public boolean isRuntimeMobileForm() {
        return isRuntimeMobileForm;
    }

    public void setRuntimeMobileForm(boolean runtimeMobileForm) {
        isRuntimeMobileForm = runtimeMobileForm;
    }
}
