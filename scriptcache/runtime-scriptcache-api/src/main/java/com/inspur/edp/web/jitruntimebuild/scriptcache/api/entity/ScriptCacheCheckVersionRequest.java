/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity;

/**
 * description:脚本缓存
 *
 * @author Noah Guo
 * @date 2020/09/23
 */
public class ScriptCacheCheckVersionRequest {
    /**
     * 关联表单元数据id
     */
    private  String formMetadataId;
    /**
     * 工程名称
     */
    private String projectName;
    /**
     * 工程相对路径
     */
    private String projectRelativePath;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectRelativePath() {
        return projectRelativePath;
    }

    public void setProjectRelativePath(String projectRelativePath) {
        this.projectRelativePath = projectRelativePath;
    }

    public String getFormMetadataId() {
        return formMetadataId;
    }

    public void setFormMetadataId(String formMetadataId) {
        this.formMetadataId = formMetadataId;
    }
}
