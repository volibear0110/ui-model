/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.jitruntimebuild.scriptcache.api.entity;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.Date;

/**
 * description: 表单工程缓存
 *
 * @author Noah Guo
 * @date 2020/09/23
 */
public class FormProjectCache {

    /**
     * 主键字段
     */
    private String id;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 工程编码
     */
    private String projectCode;

    /**
     * 工程相对路径
     */
    private String projectRelativePath;

    /**
     * 工程对应版本
     */
    private String version;

    public String getId() {
        if (StringUtility.isNullOrEmpty(id)) {
            id = StringUtility.convertNullToEmptyString(id);
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        if (StringUtility.isNullOrEmpty(projectName)) {
            projectName = StringUtility.convertNullToEmptyString(projectName);
        }
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        if (StringUtility.isNullOrEmpty(projectCode)) {
            projectCode = StringUtility.convertNullToEmptyString(projectCode);
        }
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectRelativePath() {
        if (StringUtility.isNullOrEmpty(projectRelativePath)) {
            projectRelativePath = StringUtility.convertNullToEmptyString(projectRelativePath);
        }
        return projectRelativePath;
    }

    public void setProjectRelativePath(String projectRelativePath) {
        this.projectRelativePath = projectRelativePath;
    }

    public String getVersion() {
        if (StringUtility.isNullOrEmpty(version)) {
            version = StringUtility.convertNullToEmptyString(version);
        }
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 最后修改人
     */
    private String lastModifier;

    /**
     * 最后修改时间
     */
    private Date lastModifyTime;


}
