/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;

/**
 * 零代码表单关联元数据
 */
public class ZeroCodeFormRefMetadataParameter {
    /**
     * 元数据内容
     */
    private GspMetadata metadata;
    /**
     * 元数据类型
     */
    private MetadataTypeEnum metadataType;

    public MetadataTypeEnum getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(MetadataTypeEnum metadataType) {
        this.metadataType = metadataType;
    }

    public GspMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(GspMetadata metadata) {
        this.metadata = metadata;
    }
}
