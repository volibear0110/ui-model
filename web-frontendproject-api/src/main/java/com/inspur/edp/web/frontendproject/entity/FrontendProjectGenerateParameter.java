/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.entity;

/**
 * 前端工程生成参数
 */
public class FrontendProjectGenerateParameter {

    /**
     * 是否强制启用解析
     */
    private boolean forceUseJieXi = false;

    /**
     * 工程路径
     */
    private String projectPath;

    /**
     * 参与生成的表单列表
     */
    private ChosenFormList buildFormList;


    public boolean isForceUseJieXi() {
        return forceUseJieXi;
    }

    public void setForceUseJieXi(boolean forceUseJieXi) {
        this.forceUseJieXi = forceUseJieXi;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public ChosenFormList getBuildFormList() {
        if (buildFormList == null) {
            buildFormList = ChosenFormList.getNewInstance();
        }
        return buildFormList;
    }

    public void setBuildFormList(ChosenFormList buildFormList) {
        this.buildFormList = buildFormList;
    }
}
