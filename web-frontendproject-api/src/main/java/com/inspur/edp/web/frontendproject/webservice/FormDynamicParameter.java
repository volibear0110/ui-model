/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.webservice;

/**
 * 表单解析参数
 *
 * @author guozhiqi
 */
public class FormDynamicParameter {

    /**
     * 工程路径
     */
    private String projectPath;
    /**
     * 表单code 如果增加该参数 那么使用单独的表单解析
     * 如果不包含该参数 那么使用工程路径解析
     */
    private String formCode;

    /**
     * 是否仅使用单个表单进行解析
     * 如果设置单个表单解析，那么formCode参数必须有值
     * 如果不是单个表单解析，那么formCode参数必须为空
     */
    private boolean useSingleForm = false;

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public boolean isUseSingleForm() {
        return useSingleForm;
    }

    public void setUseSingleForm(boolean useSingleForm) {
        this.useSingleForm = useSingleForm;
    }
}
