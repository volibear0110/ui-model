/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.entity;

import lombok.Data;

/**
 * @Title: ChosenFormItem
 * @Description: com.inspur.edp.web.frontendproject.entity 选择的表单选项
 * @Author: Noah
 * @Version: V1.0
 * @Create: 2022/5/14 10:55
 */
@Data
public class ChosenFormItem {
    /**
     * 表单code
     */
    private String formCode;

    /**
     * 是否强制使用解析
     */
    private boolean isForceDynamicForm;

    @Override
    public boolean equals(Object o) {
        if (o instanceof ChosenFormItem) {
            ChosenFormItem formItem = (ChosenFormItem) o;
            return this.formCode.equals(formItem.getFormCode());
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "ChosenFormItem [formCode=" + formCode +", isForceDynamicForm="
                + isForceDynamicForm + "]";
    }
}
