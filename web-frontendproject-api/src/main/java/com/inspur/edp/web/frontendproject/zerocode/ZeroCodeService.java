/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

/**
 * 零代码service  主要提供代码生成、编译、部署
 * @author  noah
 */
public interface ZeroCodeService {

    /**
     * 解析表单元数据  生成对应的webdev  json文件   生成ts代码依赖
     * @param zeroCodeParameter
     */
    @Deprecated()
    void  resolveMetadataAndGenerateSource(ZeroCodeParameter zeroCodeParameter);

    /**
     * 编译生成后的代码
     * @param zeroCodeParameter
     */
    @Deprecated()
    void build(ZeroCodeParameter zeroCodeParameter);

    /**
     * 部署编译后的交付物
     * @param zeroCodeParameter
     */
    @Deprecated()
    void  deploy(ZeroCodeParameter zeroCodeParameter);


    /**
     * 解析、编译、部署
     * @param zeroCodeParameter 零代码入参
     */
    void resolveBuildAndDeploy(ZeroCodeParameter zeroCodeParameter);

}
