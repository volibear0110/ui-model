/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.entity;

public class IdeConfigEntity {
    private boolean useNewIde = true;

    public boolean isUseNewIde() {
        return useNewIde;
    }

    public void setUseNewIde(boolean useNewIde) {
        this.useNewIde = useNewIde;
    }

    /**
     * 默认为旧版IDE
     * @return
     */
    public static IdeConfigEntity init() {
        IdeConfigEntity ideConfigEntity = new IdeConfigEntity();
        ideConfigEntity.setUseNewIde(false);
        return ideConfigEntity;
    }
}
