/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.webservice;

import com.inspur.edp.web.common.entity.ResultMessage;
import com.inspur.edp.web.frontendproject.entity.IdeConfigEntity;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")
public interface FrontendProjectWebService {

    /**
     * 判断当前工程是否是前端工程
     * @param projectPath 工程路径
     * @return
     */
    @GET
    @Path("/is-frontend-project")
    boolean isFrontendProject(@QueryParam("projectPath") String projectPath);


    @POST
    @Path("/resolve")
    void resolveFrontendProject(@QueryParam("projectPath") String projectPath);

    @POST
    @Path("/generate")
    void generateFrontendProject(@QueryParam("projectPath") String projectPath);

    @POST
    @Path("/build")
    ResultMessage buildFrontendProject(@QueryParam("projectPath") String projectPath);

    @POST
    @Path("/resolve-generate")
    void resolveAndGenerateFrontendProject(@QueryParam("projectPath") String projectPath);

    @POST
    @Path("/resolve-generate-build")
    ResultMessage resolveAndGenerateAndBuildFrontendProject(@QueryParam("projectPath") String projectPath);


    @POST
    @Path("/deploy")
    void deployFrontendProject(@QueryParam("projectPath") String projectPath);

    @POST
    @Path("/one-key-deploy")
    void oneKeyDeployFrontendProject(@QueryParam("projectPath") String projectPath);

    /**
     * 使用babel编译前端工程并部署。
     */
    @POST
    @Path("/babel-build-deploy")
    void babelBuildAndDeploy(@QueryParam("projectPath") String projectPath, @QueryParam("formCode") String formCode);

    /**
     * 解析表单生成 解析表单预览
     * 仅生成该表单相关的依赖脚本
     * @param dynamicParameter  解析参数
     */
    @POST
    @Path("/generate-for-interpretation")
    ResultMessage generateFrontendProjectWithDynamic(@RequestBody FormDynamicParameter dynamicParameter);

    @POST
    @Path("/project-config")
    ResultMessage<IdeConfigEntity>getProjectConfig();
}
