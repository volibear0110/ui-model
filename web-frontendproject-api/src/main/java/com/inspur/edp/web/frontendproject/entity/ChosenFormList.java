/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.entity;

import com.inspur.edp.web.common.utility.StringUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 选中的表单元数据列表
 *
 * @author guozhiqi
 */
public class ChosenFormList {
    /**
     * 私有构造方法
     */
    private ChosenFormList() {
    }

    /**
     * 获取新实例  每次调用都会创建新的实例
     *
     * @return
     */
    public static ChosenFormList getNewInstance() {
        return new ChosenFormList();
    }

    private List<ChosenFormItem> itemList;

    /**
     * 获取对应的选中表单code列表
     * 入股code列表实例为空，那么进行实例化
     *
     * @return
     */
    public List<ChosenFormItem> getItemList() {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
        return itemList;
    }

    /**
     * 依据formCode获取对应的选中项
     *
     * @param formCode
     * @return
     */
    public Optional<ChosenFormItem> getByFormCode(String formCode) {
        if (!this.contains(formCode)) {
            return Optional.ofNullable(null);
        }
        return this.getItemList().stream().filter(t -> t.getFormCode().equals(formCode)).findFirst();
    }

    /**
     * 获取是否强制解析模式
     *
     * @param formCode
     * @return
     */
    public boolean getIsDynamicFormByFormCode(String formCode) {
        Optional<ChosenFormItem> selectChosenItem = this.getByFormCode(formCode);
        return selectChosenItem.map(ChosenFormItem::isForceDynamicForm).orElse(false);
    }

    /**
     * 包含的列表项是否为空
     *
     * @return
     */
    public boolean isEmpty() {
        return this.getItemList().isEmpty();
    }

    /**
     * 包含的列表项不为空
     *
     * @return
     */
    public boolean isNotEmpty() {
        return !this.isEmpty();
    }

    /**
     * 是否包含指定的code
     *
     * @param formCode
     * @return
     */
    public boolean contains(String formCode) {
        if (StringUtility.isNullOrEmpty(formCode)) {
            return false;
        }
        return this.getItemList().stream().anyMatch(t -> t.getFormCode().equals(formCode));
    }

    /**
     * 添加待生成、编译的表单code
     *
     * @param formCode
     */
    public ChosenFormList add(String formCode) {
        this.addOrUpdate(formCode, false);
        return this;
    }

    /**
     * 添加待生成、编译的表单code
     *
     * @param formCode
     */
    public ChosenFormList add(String formCode, boolean isForceDynamicForm) {
        this.addOrUpdate(formCode, isForceDynamicForm);
        return this;
    }

    /**
     * 新增或更新表单选项
     *
     * @param formCode
     * @param isForceDynamicForm
     */
    private void addOrUpdate(String formCode, boolean isForceDynamicForm) {
        if (StringUtility.isNullOrEmpty(formCode)) {
            return;
        }

        if (this.contains(formCode)) {
            ChosenFormItem selectFormItem = this.getItemList().stream().filter(t -> t.getFormCode().equals(formCode)).findFirst().get();
            selectFormItem.setForceDynamicForm(isForceDynamicForm);
        } else {
            ChosenFormItem formItem = new ChosenFormItem();
            formItem.setFormCode(formCode);
            formItem.setForceDynamicForm(isForceDynamicForm);
            this.getItemList().add(formItem);
        }
    }
}
