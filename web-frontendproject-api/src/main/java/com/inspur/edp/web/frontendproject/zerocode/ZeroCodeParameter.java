/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.utility.StringUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * 零代码参数
 *
 * @author noah
 */
public class ZeroCodeParameter {
    /**
     * 生成的源代码的根路径
     */
    private String absoluteBasePath;

    /**
     * 工程名称
     */
    private String projectName;


    /**
     * 依赖node_modules 路径
     */
    private String relyNodeModulesPath;

    /**
     * serviceunit路径   scm/sd
     */
    private String serviceUnitPath;

    private String originalServiceUnitPath;

    /**
     * 服务环境目录
     */
    private String serverPath;
    /**
     * 一个表单--->包含多个元数据
     */
    private List<ZeroCodeFormParameter> formParameters;

    /**
     * 是否使用解析模式
     */
    private boolean useJieXiMode = false;

    public boolean isUseJieXiMode() {
        return useJieXiMode;
    }

    public void setUseJieXiMode(boolean useJieXiMode) {
        this.useJieXiMode = useJieXiMode;
    }

    public String getAbsoluteBasePath() {
        return absoluteBasePath;
    }

    public void setAbsoluteBasePath(String absoluteBasePath) {
        if (!StringUtility.isNullOrEmpty(absoluteBasePath)) {
            absoluteBasePath = FileUtility.getPlatformIndependentPath(absoluteBasePath);
        }
        this.absoluteBasePath = absoluteBasePath;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getRelyNodeModulesPath() {
        return relyNodeModulesPath;
    }

    public void setRelyNodeModulesPath(String relyNodeModulesPath) {
        this.relyNodeModulesPath = relyNodeModulesPath;
    }

    public String getServiceUnitPath() {
        return serviceUnitPath;
    }

    public void setServiceUnitPath(String serviceUnitPath) {
        this.serviceUnitPath = serviceUnitPath;
    }

    public List<ZeroCodeFormParameter> getFormParameters() {
        if (formParameters == null) {
            formParameters = new ArrayList<>();
        }
        return formParameters;
    }

    public void setFormParameters(List<ZeroCodeFormParameter> formParameters) {
        this.formParameters = formParameters;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    /**
     * 判定是否包含指定类型的表单数据
     *
     * @param terminalType
     * @return
     */
    public boolean hasFormParameter(TerminalType terminalType) {
        boolean isMobile = terminalType == TerminalType.MOBILE;
        return this.getFormParameters().stream().anyMatch(t -> t.isMobile() == isMobile);
    }

    public String getOriginalServiceUnitPath() {
        return originalServiceUnitPath;
    }

    public void setOriginalServiceUnitPath(String originalServiceUnitPath) {
        this.originalServiceUnitPath = originalServiceUnitPath;
    }
}
