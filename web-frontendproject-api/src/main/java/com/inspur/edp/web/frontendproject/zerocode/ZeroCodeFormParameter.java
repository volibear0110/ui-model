/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.zerocode;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 零代码表单关联参数
 */
public class ZeroCodeFormParameter {

    private GspMetadata metadata;
    /**
     * 表单元数据code
     */
    private String code;
    /**
     * 表单源苏剧name
     */
    private String name;

    /**
     * 表单关联的元数据列表，例如 状态机
     */
    private List<ZeroCodeFormRefMetadataParameter> refMetadataParameters;

    /**
     * 是否移动表单
     */
    private boolean isMobile = false;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ZeroCodeFormRefMetadataParameter> getRefMetadataParameters() {
        if (this.refMetadataParameters == null) {
            this.refMetadataParameters = new ArrayList<>();
        }
        return refMetadataParameters;
    }

    public void setRefMetadataParameters(List<ZeroCodeFormRefMetadataParameter> refMetadataParameters) {
        this.refMetadataParameters = refMetadataParameters;
    }

    public GspMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(GspMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * 获取指定类型的元数据
     *
     * @param metadataTypeEnum
     * @return
     */
    public List<ZeroCodeFormRefMetadataParameter> getSpecialFormRefMetaData(MetadataTypeEnum metadataTypeEnum) {
        List<ZeroCodeFormRefMetadataParameter> filterFormRefMetadataParameters = new ArrayList<>();
        this.getRefMetadataParameters().forEach(t -> {
            if (t.getMetadataType() == metadataTypeEnum) {
                filterFormRefMetadataParameters.add(t);
            }
        });
        return filterFormRefMetadataParameters;
    }


    /**
     * 获取指定类型的元数据
     *
     * @param metadataId       指定元数据id
     * @param metadataTypeEnum
     * @return
     */
    public ZeroCodeFormRefMetadataParameter getSpecialFormRefMetaData(String metadataId, MetadataTypeEnum metadataTypeEnum) {
        AtomicReference<ZeroCodeFormRefMetadataParameter> formRefMetadataParameter = new AtomicReference<>();
        this.getRefMetadataParameters().forEach(t -> {
            if (t.getMetadataType() == metadataTypeEnum && t.getMetadata().getHeader().getId().equals(metadataId)) {
                formRefMetadataParameter.set(t);
            }
        });
        return formRefMetadataParameter.get();
    }

    public boolean isMobile() {
        return isMobile;
    }

    public void setMobile(boolean mobile) {
        isMobile = mobile;
    }

    // region 零代码中由表单格式合并得到的表单

    private final List<ZeroCodeFormFormatParameter> formFormatList = new ArrayList<>();

    public List<ZeroCodeFormFormatParameter> getFormFormatList() {
        return this.formFormatList;
    }

    public void addFormFormat(String configId, IMetadataContent metadataContent) {
        this.formFormatList.add(new ZeroCodeFormFormatParameter(configId, metadataContent));
    }

    // endregion
}
