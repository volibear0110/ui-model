/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.webservice;

import com.inspur.edp.web.common.entity.ResultMessage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")
public interface ZeroCodeWebService {

    /**
     * 请求跳转前加载对应文件内容
     *
     * @param indexHtmlUrl 默认首页url地址   /web/apps/.../index.html
     * @param options      可选参数
     * @return
     */
    @GET()
    @Path("/beforenavigate")
    ResultMessage beforeNavigateLoadFile(@QueryParam("url") String indexHtmlUrl, @QueryParam("routeUri") String routeUri, @QueryParam("options") String options);
}
