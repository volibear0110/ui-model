/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.frontendproject.entity;

import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import org.junit.Test;

import static org.junit.Assert.*;

public class IdeConfigEntityTest {

    @Test
    public void testSerialize() {
        IdeConfigEntity ideConfigEntity = IdeConfigEntity.init();

        String content = SerializeUtility.getInstance().serialize(ideConfigEntity);

        IdeConfigEntity deserializeIdeConfig = SerializeUtility.getInstance().deserialize(content, IdeConfigEntity.class);
        assertFalse(deserializeIdeConfig.isUseNewIde());


    }

    @Test
    public void testDeserialize() {
        IdeConfigEntity ideConfigEntity = IdeConfigEntity.init();
        ideConfigEntity.setUseNewIde(false);

        String content = SerializeUtility.getInstance().serialize(ideConfigEntity);
        FileUtility.writeFile( "./ide_config.json", content);

        boolean exists = FileUtility.exists( "./ide_config.json");
        assertTrue(exists);

        String readContent = FileUtility.readAsString( "./ide_config.json");
        IdeConfigEntity deserializedIdeConfigEntity = SerializeUtility.getInstance().deserialize(readContent, IdeConfigEntity.class);
        assertEquals(deserializedIdeConfigEntity.isUseNewIde(),ideConfigEntity.isUseNewIde());

    }

}