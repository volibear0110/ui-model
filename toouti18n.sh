version=$(sed -n 's/.*<custom.version>\([^<]*\)<\/custom.version>.*/\1/p' pom.xml)
echo $version
rm -rf  out

mkdir -p out/server/platform/dev/main/libs
mkdir -p out/server/platform/common/libs
mkdir -p out/server/platform/runtime/bcc/libs
cp web-designschema/target/web-designschema-$version.jar ./out/server/platform/common/libs/web-designschema.jar
cp form-process/web-form-process/target/web-form-process-$version.jar ./out/server/platform/dev/main/libs/web-form-process.jar
cp web-form-jitengine/target/web-jitengine-$version.jar ./out/server/platform/common/libs/web-jitengine.jar
cp web-common/target/web-jitengine-common-$version.jar ./out/server/platform/common/libs/web-jitengine-common.jar
cp web-form-metadata/target/web-jitengine-formmetadata-$version.jar ./out/server/platform/common/libs/web-jitengine-formmetadata.jar
cp web-frontendproject/target/web-jitengine-frontendproject-$version.jar ./out/server/platform/common/libs/web-jitengine-frontendproject.jar
cp runtime/runtime-api/target/web-jitengine-runtimebuild-api-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-api.jar
cp runtime/runtime-core/target/web-jitengine-runtimebuild-core-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-core.jar
cp scriptcache/runtime-scriptcache/target/web-jitengine-runtimebuild-scriptcache-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-scriptcache.jar
cp scriptcache/runtime-scriptcache-api/target/web-jitengine-runtimebuild-scriptcache-api-$version.jar ./out/server/platform/common/libs/web-jitengine-runtimebuild-scriptcache-api.jar
cp jitengine-web-api/target/web-jitengine-web-api-$version.jar ./out/server/platform/common/libs/web-jitengine-web-api.jar
cp jitengine-web-core/target/web-jitengine-web-core-$version.jar ./out/server/platform/common/libs/web-jitengine-web-core.jar
cp npmpackage/web-npmpackage-api/target/web-npmpackage-api-$version.jar ./out/server/platform/common/libs/web-npmpackage-api.jar
cp npmpackage/web-npmpackage-core/target/web-npmpackage-core-$version.jar ./out/server/platform/common/libs/web-npmpackage-core.jar
cp tsfile/web-tsfile-api/target/web-tsfile-api-$version.jar ./out/server/platform/common/libs/web-tsfile-api.jar
cp tsfile/web-tsfile-core/target/web-tsfile-core-$version.jar ./out/server/platform/common/libs/web-tsfile-core.jar
