/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;

/**
 * 页面流元数据
 */
public class PageFlowMetadataEntityManager {
    /**
     * 页面流元数据实体序列化
     *
     * @param pageFlowMetadataEntity
     * @return
     */
    public static String serialize(PageFlowMetadataEntity pageFlowMetadataEntity) {
        String pageFlowMetadataEntityStr = "";
        if (pageFlowMetadataEntity == null) {
            return pageFlowMetadataEntityStr;
        }

        pageFlowMetadataEntityStr = SerializeUtility.getInstance().serialize(pageFlowMetadataEntity);

        return pageFlowMetadataEntityStr;
    }

    /**
     * 页面流元数据反序列化
     *
     * @param pageFlowMetadataEntityStr
     * @return
     */
    public static PageFlowMetadataEntity deserialize(String pageFlowMetadataEntityStr) {
        if (StringUtility.isNullOrEmpty(pageFlowMetadataEntityStr)) {
            return null;
        }

        return SerializeUtility.getInstance().deserialize(pageFlowMetadataEntityStr, PageFlowMetadataEntity.class);
    }

    /**
     * 基于pageId，获取对应的route uri
     *
     * @param pageFlowMetadataEntity
     * @param pageId
     * @return
     */
    public static String getPageRouteUri(PageFlowMetadataEntity pageFlowMetadataEntity, String pageId) {
        String routeUri = "";

        if (pageFlowMetadataEntity == null) {
            return routeUri;
        }

        com.inspur.edp.web.pageflow.metadata.entity.Page[] pages = pageFlowMetadataEntity.getPages();

        for (Page page : pages) {
            if (page.getId().equals(pageId)) {
                routeUri = page.getRouteUri();
                break;
            }
        }

        return routeUri;
    }
}
