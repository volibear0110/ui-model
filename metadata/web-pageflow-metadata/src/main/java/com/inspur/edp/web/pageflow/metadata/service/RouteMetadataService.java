/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.web.common.customexception.WebCustomException;
import com.inspur.edp.web.common.io.FileUtility;
import com.inspur.edp.web.common.logger.WebLogger;
import com.inspur.edp.web.common.metadata.MetadataGetterParameter;
import com.inspur.edp.web.common.metadata.MetadataTypeEnum;
import com.inspur.edp.web.common.metadata.MetadataUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntityManager;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * 页面流元数据service
 *
 * @author noah
 */
public class RouteMetadataService {

    /**
     * 获取路由元数据内容
     *
     * @param routeMetadataId 页面流元数据id
     * @param projectPath     基于开发根目录的路径
     * @return
     */
    public static PageFlowMetadataEntity getRouteMetadataContentWithMetadataIdAndProjectPath(String routeMetadataId, String routeMetadataFileName, String projectPath) {

        if (StringUtility.isNullOrEmpty(routeMetadataId)) {
            return null;
        }

        // 如果对应元数据不存在
        if (!StringUtility.isNullOrEmpty(routeMetadataFileName) && !MetadataUtility.getInstance().isMetaDataExistsWithMetadataIDAndPathWithDesign(projectPath, routeMetadataId)) {
            return null;
        }

        MetadataGetterParameter metadataGetterParameter = MetadataGetterParameter.getNewInstance(routeMetadataId, projectPath, MetadataTypeEnum.Route);
        metadataGetterParameter.setTargetMetadataNotFoundMessage("获取页面流元数据为空，请检查页面流元数据是否被删除或app.config.json中页面流定义是否正确");
        GspMetadata routeMetadata = MetadataUtility.getInstance().getMetadataWithDesign(metadataGetterParameter);
        if (routeMetadata == null) {
            return null;
        }

        return (PageFlowMetadataEntity) routeMetadata.getContent();
    }

    /**
     * 保存路由元数据内容
     *
     * @param pageFlowMetadataEntity 待保存元数据内容
     * @param routeMetadataId        页面流元数据id
     * @param projectPath            基于开发根目录的路径
     */
    public static void saveRouteMetadataContent(PageFlowMetadataEntity pageFlowMetadataEntity, String routeMetadataId, String projectPath) {
        if (StringUtility.isNullOrEmpty(routeMetadataId) || StringUtility.isNullOrEmpty(projectPath)) {
            return;
        }
        MetadataGetterParameter metadataGetterParameter = MetadataGetterParameter.getNewInstance(routeMetadataId, projectPath, MetadataTypeEnum.Route);
        metadataGetterParameter.setTargetMetadataNotFoundMessage("获取页面流元数据为空，请检查页面流元数据是否存在或app.config.json中页面流定义是否正确");

        GspMetadata routeMetadata = MetadataUtility.getInstance().getMetadataWithDesign(metadataGetterParameter);
        if (routeMetadata == null) {
            WebLogger.Instance.error("Save Route Metadata Content Operation is Stopped Because Target Route Metadata is NULL.");
            return;
        }

        routeMetadata.setContent(pageFlowMetadataEntity);

        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
        metadataService.saveMetadata(routeMetadata, routeMetadata.getRelativePath() + FileUtility.DIRECTORY_SEPARATOR_CHAR + routeMetadata.getHeader().getFileName());
    }

    /**
     * 通过表单ID，查找对应的RouteUri
     *
     * @param pageFlowMetadataEntity
     * @return
     */
    public static String getCurrentPageRouteUri(PageFlowMetadataEntity pageFlowMetadataEntity, String pageId) {
        if (pageFlowMetadataEntity == null || StringUtility.isNullOrEmpty(pageId)) {
            throw new WebCustomException("Current Route Metadata Content is Null or the Page id is Nulll or Empty.", ExceptionLevel.Warning);
        }

        return PageFlowMetadataEntityManager.getPageRouteUri(pageFlowMetadataEntity, pageId);
    }
}
