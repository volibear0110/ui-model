/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.listener;

import com.inspur.edp.lcm.metadata.spi.event.MetadataCreateEventListener;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import com.inspur.edp.web.appconfig.core.service.GspAppConfigService;
import com.inspur.edp.web.common.entity.TerminalType;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;
import com.inspur.edp.web.pageflow.metadata.service.PageFlowMetadataUpdateService;

public class PageFlowMetadataEventListener implements MetadataEventListener, MetadataCreateEventListener {

    @Override
    public void fireMetadataSavingEvent(MetadataEventArgs metadataEventArgs) {

    }


    /**
     * 业务实体元数据保存后事件
     */
    @Override
    public void fireMetadataSavedEvent(MetadataEventArgs metadataEventArgs) {
        if (metadataEventArgs == null || metadataEventArgs.getMetadata() == null || metadataEventArgs.getMetadata().getHeader() == null) {
            return;
        }

        // 类型判断（避免对其他元数据产生影响）
        if (metadataEventArgs.getMetadata().getContent() instanceof PageFlowMetadataEntity) {
            pageFlowMetadataSaved(metadataEventArgs);
        }
    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {
        if (metadataEventArgs == null || metadataEventArgs.getMetadata() == null || metadataEventArgs.getMetadata().getHeader() == null) {
            return;
        }
        PageFlowMetadataUpdateService.getInstance().syncPageFlowMetadataAfterFormDeleted(metadataEventArgs);
    }

    @Override
    public void fireMetadataCreatingEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataCreatedEvent(MetadataEventArgs metadataEventArgs) {
        if (metadataEventArgs == null || metadataEventArgs.getMetadata() == null || metadataEventArgs.getMetadata().getHeader() == null) {
            return;
        }

        if (TerminalType.isPCOrMobile(metadataEventArgs.getMetadata().getHeader().getType())) {
            PageFlowMetadataUpdateService.getInstance().updatePageFlowMetadata(metadataEventArgs);
        }
    }

    private void pageFlowMetadataSaved(MetadataEventArgs metadataEventArgs) {
        // 获取页面流元数据ID
        String pageFlowMetaDataID = metadataEventArgs.getMetadata().getHeader().getId();
        if (StringUtility.isNullOrEmpty(pageFlowMetaDataID)) {
            return;
        }

        // 获取页面流元数据文件名称
        String pageFlowMetadataFileName = metadataEventArgs.getMetadata().getHeader().getFileName();
        if (StringUtility.isNullOrEmpty(pageFlowMetadataFileName)) {
            return;
        }

        // 获取页面流元数据路径
        String pageFlowMetadataFullPath = metadataEventArgs.getPath();

        // TODO：迁移到AppConfig中处理
        GspAppConfigService.getCurrent().updateAppConfigFileWhenPageFlowFileCreated(pageFlowMetaDataID, pageFlowMetadataFileName, pageFlowMetadataFullPath);
    }
}
