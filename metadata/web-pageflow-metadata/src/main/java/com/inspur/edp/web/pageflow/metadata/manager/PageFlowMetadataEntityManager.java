/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.manager;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;
import com.inspur.edp.web.pageflow.metadata.service.RouteMetadataService;

public class PageFlowMetadataEntityManager implements MetadataContentManager {
    @Override
    public void build(GspMetadata metadata) {
        if (metadata == null) {
            return;
        }

        PageFlowMetadataEntity pageFlowMetadataEntity = new PageFlowMetadataEntity();

        if (metadata.getHeader() != null && !StringUtility.isNullOrEmpty(metadata.getHeader().getId())) {
            pageFlowMetadataEntity.setId(metadata.getHeader().getId());
            metadata.setContent(pageFlowMetadataEntity);
        }
    }

    /**
     * @deprecated 没有找到使用者
     * @param pageFlowMetadataEntity
     * @param pageId
     * @return
     */
    public static String getCurrentPageRouteUri(PageFlowMetadataEntity pageFlowMetadataEntity, String pageId) {
        return RouteMetadataService.getCurrentPageRouteUri(pageFlowMetadataEntity, pageId);
    }

}
