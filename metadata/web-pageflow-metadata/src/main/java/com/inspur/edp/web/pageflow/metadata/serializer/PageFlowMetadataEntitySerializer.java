/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;

public class PageFlowMetadataEntitySerializer implements MetadataContentSerializer {
    /**
     * JSON 字符串反序列化
     *
     * @param matadataContent 元数据内容
     * @return
     */
    @Override
    public IMetadataContent DeSerialize(JsonNode matadataContent) {
        if (matadataContent == null) {
            return null;
        }

        return SerializeUtility.getInstance().<PageFlowMetadataEntity>toObject(matadataContent, PageFlowMetadataEntity.class);
    }

    /**
     * JSON 字符串序列化
     *
     * @param metadataContent 元数据内容
     * @return
     */
    @Override
    public JsonNode Serialize(IMetadataContent metadataContent) {
        if (metadataContent == null) {
            return null;
        }

        PageFlowMetadataEntity metadata = (PageFlowMetadataEntity) ((metadataContent instanceof PageFlowMetadataEntity) ? metadataContent : null);

        String metadataString = SerializeUtility.getInstance().serialize(metadata);
        return SerializeUtility.getInstance().toJsonNode(metadataString);
    }

}
