/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.serializer;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataTransferSerializer;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.pageflow.metadata.entity.PageFlowMetadataEntity;
import io.iec.edp.caf.common.JSONSerializer;

/**
 * @deprecated 元数据扩展里面不再需要TransferSerializer
 */
@Deprecated()
public class PageFlowMetadataTransferSerializer implements MetadataTransferSerializer {
    /**
     * Json反序列化接口
     *
     * @param contentString
     * @return
     */
    @Override
    public PageFlowMetadataEntity deserialize(String contentString) {
        if (StringUtility.isNullOrEmpty(contentString)) {
            return null;
        } else {
            return JSONSerializer.deserialize(contentString, PageFlowMetadataEntity.class);
        }
    }

    /**
     * Json序列化接口
     *
     * @param metadataContent
     * @return
     */
    @Override
    public String serialize(IMetadataContent metadataContent) {
        if (metadataContent == null) {
            return null;
        } else {
            PageFlowMetadataEntity metadata = (PageFlowMetadataEntity) ((metadataContent instanceof PageFlowMetadataEntity) ? metadataContent : null);
            return JSONSerializer.serialize(metadata);
        }
    }

}
