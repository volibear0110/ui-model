/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.pageflow.metadata.serializer.PageFlowMetadataTransferSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 页面流定义实体
 * @author guozhiqi
 */
@Getter
@Setter
public class PageFlowMetadataEntity extends AbstractMetadataContent implements Serializable {
    private static final long serialVersionUID = 2062374129918L;

    /**
     * ID
     */
    private String id;

    /**
     * 工程配置信息
     */
    private Project project;

    /**
     * 页面集合
     */
    private Page[] pages;


    /**
     * 工程入口
     */
    private String entry;
    private List<Route> routes;

    /**
     * 关联应用code
     */
    private String appCode;
    /**
     * 关联应用name
     */
    private String appName;

    private List<PagePublish> publishes;

    @Override
    public PageFlowMetadataEntity clone() {
        PageFlowMetadataTransferSerializer serializer = new PageFlowMetadataTransferSerializer();
        String json = serializer.serialize(this);
        return serializer.deserialize(json);
    }

    public static String Serialize(PageFlowMetadataEntity pageFlowMetadataEntity) {
        String pageFlowMetadataEntityStr = "";
        if (pageFlowMetadataEntity == null) {
            return pageFlowMetadataEntityStr;
        }

        pageFlowMetadataEntityStr = SerializeUtility.getInstance().serialize(pageFlowMetadataEntity, false);

        return pageFlowMetadataEntityStr;
    }

    public static PageFlowMetadataEntity Deserialize(String pageFlowMetadataEntityStr) {
        PageFlowMetadataEntity pageFlowMetadataEntity = null;
        if (StringUtility.isNullOrEmpty(pageFlowMetadataEntityStr)) {
            return pageFlowMetadataEntity;
        }

        pageFlowMetadataEntity = SerializeUtility.getInstance().deserialize(pageFlowMetadataEntityStr, PageFlowMetadataEntity.class);

        return pageFlowMetadataEntity;
    }

    /**
     * 基于pageId，获取对应的route uri
     *
     * @param pageFlowMetadataEntity
     * @param pageId
     * @return
     */
    public static String GetPageRouteUri(PageFlowMetadataEntity pageFlowMetadataEntity, String pageId) {
        String routeUri = "";

        if (pageFlowMetadataEntity == null) {
            return routeUri;
        }

        Page[] pages = pageFlowMetadataEntity.getPages();
        for (Page page : pages) {
            if (page.getId().equals(pageId)) {
                routeUri = page.getRouteUri();
                break;
            }
        }

        return routeUri;
    }

    /**
     * 页面流中未配置任何页面
     *
     * @return
     */
    public boolean hasEmptyPage() {
        return this.getPages() == null || this.getPages().length == 0;
    }
}
