/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.common.utility.StringUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * 页面流实体转换service
 * @author  noah
 */
public class AdaptedPageFlowMetadataEntityService {
    /**
     * 基于 PageFlowMetadataEntity 实例生成 AdaptedPageFlowMetadataEntity 实例
     */
    public static AdaptedPageFlowMetadataEntity generateAdaptedPageFlowMetadataEntity(PageFlowMetadataEntity pageFlowMetadataEntity) {
        if (pageFlowMetadataEntity == null) {
            return null;
        }
        AdaptedPageFlowMetadataEntity adaptedPageFlowMetadataEntity = new AdaptedPageFlowMetadataEntity();
        adaptedPageFlowMetadataEntity.setId(pageFlowMetadataEntity.getId());
        adaptedPageFlowMetadataEntity.setProject(pageFlowMetadataEntity.getProject());
        adaptedPageFlowMetadataEntity.setPages(generateAdaptedPage(pageFlowMetadataEntity));
        adaptedPageFlowMetadataEntity.setEntry(pageFlowMetadataEntity.getEntry());
        adaptedPageFlowMetadataEntity.setAppCode(pageFlowMetadataEntity.getAppCode());
        adaptedPageFlowMetadataEntity.setAppName(pageFlowMetadataEntity.getAppName());
        adaptedPageFlowMetadataEntity.setPublishes(pageFlowMetadataEntity.getPublishes());

        return adaptedPageFlowMetadataEntity;
    }

    private static ArrayList<AdaptedPage> generateAdaptedPage(PageFlowMetadataEntity pageFlowMetadataEntity) {
        if (pageFlowMetadataEntity == null || pageFlowMetadataEntity.getPages() == null || pageFlowMetadataEntity.getPages().length == 0) {
            return null;
        }

        ArrayList<AdaptedPage> adaptedPageCollection = new ArrayList<>();
        for (Page page : pageFlowMetadataEntity.getPages()) {
            AdaptedPage adaptedPage = new AdaptedPage();
            adaptedPage.setId(page.getId());
            adaptedPage.setCode(page.getCode());
            adaptedPage.setName(page.getName());
            adaptedPage.setFileName(page.getFileName());
            adaptedPage.setRelativePath(page.getRelativePath());
            adaptedPage.setFormUri(page.getFormUri());
            adaptedPage.setRouteUri(page.getRouteUri());
            adaptedPage.setRouteParams(cloneRouteParams(page.getRouteParams()));
            adaptedPage.setChildren(generateRouteChildren(page.getId(), pageFlowMetadataEntity));

            adaptedPageCollection.add(adaptedPage);
        }
        return adaptedPageCollection;
    }

    private static List<String> cloneRouteParams(List<String> curretnRouteParams) {
        if (curretnRouteParams == null || curretnRouteParams.size() == 0) {
            return null;
        }

        return new ArrayList<>(curretnRouteParams);
    }

    private static List<AdaptedRoute> generateRouteChildren(String targetPageId, PageFlowMetadataEntity pageFlowMetadataEntity) {

        if (pageFlowMetadataEntity == null || StringUtility.isNullOrEmpty(targetPageId)) {
            return null;
        }

        List<Route> routeCollection = pageFlowMetadataEntity.getRoutes();
        Page[] pageCollection = pageFlowMetadataEntity.getPages();
        if (routeCollection == null || routeCollection.size() == 0) {
            return null;
        }

        ArrayList<AdaptedRoute> adaptedRouteList = new ArrayList<>();

        for (Route route : routeCollection) {
            if (route.getFrom().equals(targetPageId)) {
                AdaptedRoute adaptedRoute = new AdaptedRoute();

                adaptedRoute.setTo(route.getTo());
                Page childPage = getPage(route.getTo(), pageCollection);
                if (childPage != null) {
                    adaptedRoute.setCode(childPage.getCode());
                }
                adaptedRoute.setRouteUri(route.getRouteUri());
                adaptedRoute.setRouteParams(cloneRouteParams(route.getRouteParams()));
                adaptedRoute.setImportRouteModule(route.isImportRouteModule());
                adaptedRouteList.add(adaptedRoute);
            }
        }

        return adaptedRouteList;
    }

    private static Page getPage(String pageId, Page[] pageCollection) {
        Page page = null;

        for (Page value : pageCollection) {
            if (value.getId().equals(pageId)) {
                page = value;
                break;
            }
        }

        return page;
    }

    public static String serialize(AdaptedPageFlowMetadataEntity pageFlowMetadataEntity) {
        String pageFlowMetadataEntityStr = "";
        if (pageFlowMetadataEntity == null) {
            return pageFlowMetadataEntityStr;
        }

        pageFlowMetadataEntityStr = SerializeUtility.getInstance().serialize(pageFlowMetadataEntity);

        return pageFlowMetadataEntityStr;
    }

    public static AdaptedPageFlowMetadataEntity deserialize(String pageFlowMetadataEntityStr) {
        AdaptedPageFlowMetadataEntity pageFlowMetadataEntity = null;
        if (StringUtility.isNullOrEmpty(pageFlowMetadataEntityStr)) {
            return null;
        }

        pageFlowMetadataEntity = SerializeUtility.getInstance().deserialize(pageFlowMetadataEntityStr, AdaptedPageFlowMetadataEntity.class);

        return pageFlowMetadataEntity;
    }
}
