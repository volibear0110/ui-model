/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 路由
 */
@Getter
@Setter
public class Route {
    private String from;
    private String to;
    private String routeUri;
    private List<String> routeParams;

    private boolean importRouteModule;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getRouteUri() {
        return routeUri;
    }

    public void setRouteUri(String routeUri) {
        this.routeUri = routeUri;
    }

    public List<String> getRouteParams() {
        return routeParams;
    }

    public void setRouteParams(List<String> routeParams) {
        this.routeParams = routeParams;
    }

    public boolean isImportRouteModule() {
        return importRouteModule;
    }

    public void setImportRouteModule(boolean importRouteModule) {
        this.importRouteModule = importRouteModule;
    }
}
