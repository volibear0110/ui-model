/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 页面--面向生成器的实体结构
 * @author  noah
 */
@Getter
@Setter
public class AdaptedPage {
    private String id;
    private String code;
    private String name;
    private String fileName;
    private String relativePath;
    /**
     * 表单Uri，替换对relativePath的依赖
     */
    private String formUri;
    private String routeUri;
    private List<String> routeParams;
    private List<AdaptedRoute> children;
}