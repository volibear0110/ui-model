/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.pageflow.metadata.entity;

import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author  noah
 */
@Getter
@Setter
public class AdaptedPageFlowMetadataEntity extends AbstractMetadataContent {
    private String id;
    private Project project;
    private List<AdaptedPage> pages;
    private String entry;
    /**
     * 关联应用code
     */
    private  String appCode;
    /**
     * 关联应用name
     */
    private  String appName;

    private  List<PagePublish>publishes;

    @Override
    public AdaptedPageFlowMetadataEntity clone() {
        String json = AdaptedPageFlowMetadataEntityService.serialize(this);
        return AdaptedPageFlowMetadataEntityService.deserialize(json);
    }
}
