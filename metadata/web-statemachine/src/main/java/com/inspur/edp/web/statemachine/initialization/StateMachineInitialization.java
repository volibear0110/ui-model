package com.inspur.edp.web.statemachine.initialization;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.statemachine.metadata.StateMachineMetadataContent;

public class StateMachineInitialization implements MetadataContentManager {
    @Override
    public void build(GspMetadata metadata) {
        StateMachineMetadataContent stateMachineContent = new StateMachineMetadataContent();
        if (metadata != null && metadata.getHeader() != null && !StringUtility.isNullOrEmpty(metadata.getHeader().getId())) {
            metadata.setContent(stateMachineContent);
        }
    }
}
