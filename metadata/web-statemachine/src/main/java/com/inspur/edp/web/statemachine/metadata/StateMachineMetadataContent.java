/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.statemachine.metadata;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.web.statemachine.serializer.StateMachineTransferSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = true)
public class StateMachineMetadataContent extends AbstractMetadataContent {

    @JsonProperty("renderState")
    private HashMap<String, Object> renderState;

    @JsonProperty("state")
    private LinkedList<Object> state;

    @JsonProperty("initialState")
    private String initialState;

    @JsonProperty("action")
    private HashMap<String, Object> action;

    public final HashMap<String, Object> getRenderState() {
        return renderState;
    }

    public final void setRenderState(HashMap<String, Object> value) {
        renderState = value;
    }


    public final LinkedList<Object> getState() {
        return state;
    }

    public final void setState(LinkedList<Object> value) {
        state = value;
    }

    public final String getInitialState() {
        return initialState;
    }

    public final void setInitialState(String value) {
        initialState = value;
    }

    public final HashMap<String, Object> getAction() {
        return action;
    }

    public final void setAction(HashMap<String, Object> value) {
        action = value;
    }

    @Override
    public Object clone() {
        StateMachineTransferSerializer serializer = new StateMachineTransferSerializer();
        String json = serializer.serialize(this);
        IMetadataContent obj = serializer.deserialize(json);
        return obj;
    }

    public final String toJson() {
        StateMachineTransferSerializer serializer = new StateMachineTransferSerializer();
        return serializer.serialize(this);
    }
}
