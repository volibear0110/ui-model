/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.statemachine.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.web.common.serialize.SerializeUtility;
import com.inspur.edp.web.statemachine.metadata.StateMachineMetadataContent;

public class StateMachineSerializer implements MetadataContentSerializer {

    @Override
    public final IMetadataContent DeSerialize(JsonNode contentJObject) {
        if (contentJObject != null) {
            StateMachineMetadataContent stateMachineMetadataContent = SerializeUtility.getInstance().deserialize(contentJObject.toString(), StateMachineMetadataContent.class);
            return stateMachineMetadataContent;
        } else {
            return null;
        }
    }

    @Override
    public final JsonNode Serialize(IMetadataContent metadataContent) {
        if (metadataContent == null) {
            return null;
        }

        StateMachineMetadataContent metadata = (StateMachineMetadataContent) ((metadataContent instanceof StateMachineMetadataContent) ? metadataContent : null);
        JsonNode jsonNode = SerializeUtility.getInstance().toJsonNode(SerializeUtility.getInstance().serialize(metadata));
        return jsonNode;
    }

}
