/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.entity;

/**
 * 自定义web构件具体项
 *
 */
public class SourceCodeItemEntity {
    /**
     * 具体源文件项id
     */
    private String id;

    /**
     * 源文件路径 仅支持相对路径
     */
    private String sourcePath;

    /**
     * 目标文件路径 仅支持相对路径 默认和源文件路径相同
     */
    private String targetPath;

    /**
     * 相对路径枚举
     */
    private String relativePathEnum;

    /**
     * 源文件内容
     */
    private String sourceFileContent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public String getRelativePathEnum() {
        return relativePathEnum;
    }

    public void setRelativePathEnum(String relativePathEnum) {
        this.relativePathEnum = relativePathEnum;
    }

    public String getSourceFileContent() {
        return sourceFileContent;
    }

    public void setSourceFileContent(String sourceFileContent) {
        this.sourceFileContent = sourceFileContent;
    }
}
