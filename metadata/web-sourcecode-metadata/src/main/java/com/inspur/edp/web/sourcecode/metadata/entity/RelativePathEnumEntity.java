/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 相对路径枚举值
 *
 */
public enum RelativePathEnumEntity {
    /**
     * 工作区
     */
    @JsonProperty("workspace")
    WorkSpace,
    /**
     * 工程
     */
    @JsonProperty("project")
    Project

}
