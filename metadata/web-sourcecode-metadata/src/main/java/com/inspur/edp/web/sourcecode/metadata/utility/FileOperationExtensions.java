/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.utility;

import java.util.*;

/**
 * 文件操作扩展名
 *
 * @author noah
 */
public class FileOperationExtensions {
    // 注：实例必须有 volatile 关键字修饰，其保证初始化完全。
    private volatile static FileOperationExtensions instance = null;

    public static FileOperationExtensions getInstance() {
        if (instance == null) {
            synchronized (FileOperationExtensions.class) {
                if (instance == null) {
                    instance = new FileOperationExtensions();
                }
            }
        }
        return instance;
    }

    private FileOperationExtensions() {
        this.extensions = new ArrayList<>();
        this.getExtensions().add("ts");
        this.getExtensions().add("html");
        this.getExtensions().add("css");
        this.getExtensions().add("scss");
        this.getExtensions().add("sass");
    }


    /**
     * 可以进行操作的文件扩展名
     */
    private final ArrayList<String> extensions;

    public final ArrayList<String> getExtensions() {
        return extensions;
    }
}