/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.entity;

import com.inspur.edp.lcm.metadata.api.AbstractMetadataContent;
import com.inspur.edp.web.sourcecode.metadata.serializer.SourceCodeMetadataTransferSerializer;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义web代码
 */
@Getter
@Setter
public class SourceCodeMetadataEntity extends AbstractMetadataContent {
    public SourceCodeMetadataEntity() {
        if (this.getItems() == null) {
            this.items = new ArrayList<>();
        }
    }

    /**
     * 自定义web代码对应id
     */
    private String id;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 元数据所在路径
     */
    private String path;

    /**
     * 工程名称
     */
    private String project;

    /**
     * 自定义web代码包含的具体文件项
     */
    private List<SourceCodeItemEntity> items;

    /**
     * 描述信息
     */
    private String description;

    @Override
    public Object clone() {
        SourceCodeMetadataTransferSerializer serializer = new SourceCodeMetadataTransferSerializer();
        String json = serializer.serialize(this);
        return serializer.deserialize(json);
    }
}
