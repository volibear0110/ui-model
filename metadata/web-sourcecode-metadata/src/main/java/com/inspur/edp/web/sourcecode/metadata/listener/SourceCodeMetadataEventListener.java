/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.listener;

import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import com.inspur.edp.web.common.metadata.GspProjectUtility;
import com.inspur.edp.web.sourcecode.metadata.entity.SourceCodeMetadataEntity;
import com.inspur.edp.web.sourcecode.metadata.utility.SourceCodeMetadataUtility;

/**
 * 自定义web构建事件监听
 */
public class SourceCodeMetadataEventListener implements MetadataEventListener {


    @Override
    public void fireMetadataSavingEvent(MetadataEventArgs e) {
        if (e.getMetadata() != null && e.getMetadata().getContent() != null && e.getMetadata().getContent() instanceof SourceCodeMetadataEntity) {
            String path = e.getMetadata().getRelativePath();
            GspProject project = GspProjectUtility.getProjectInformation(path);

            String projectName = project.getMetadataProjectName();

            SourceCodeMetadataEntity sourceCodeMetadataEntity = (SourceCodeMetadataEntity) e.getMetadata().getContent();
            // 根据源文件路径获取对应文件的内容
            SourceCodeMetadataUtility.sourceCodeItemsAddFileContent(sourceCodeMetadataEntity, projectName);
            // 重新填充元数据内容
            e.getMetadata().setContent(sourceCodeMetadataEntity);
        }
    }

    @Override
    public void fireMetadataSavedEvent(MetadataEventArgs metadataEventArgs) {
        //N版方法为空
    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {
        //N版方法为空
    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {
        //N版方法为空
    }
}
