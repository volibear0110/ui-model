/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.serializer;

import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataTransferSerializer;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.sourcecode.metadata.entity.SourceCodeMetadataEntity;
import io.iec.edp.caf.common.JSONSerializer;

public class SourceCodeMetadataTransferSerializer implements MetadataTransferSerializer {

    @Override
    public String serialize(IMetadataContent metadataContent) {
        if (metadataContent == null) {
            return null;
        } else {
            SourceCodeMetadataEntity metadata = (SourceCodeMetadataEntity) ((metadataContent instanceof SourceCodeMetadataEntity) ? metadataContent : null);
            return JSONSerializer.serialize(metadata);
        }
    }

    @Override
    public IMetadataContent deserialize(String contentString) {
        if (StringUtility.isNullOrEmpty(contentString)) {
            return null;
        } else {
            return JSONSerializer.deserialize(contentString, SourceCodeMetadataEntity.class);
        }
    }
}
