/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.web.sourcecode.metadata.manager;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.web.common.utility.StringUtility;
import com.inspur.edp.web.sourcecode.metadata.entity.SourceCodeMetadataEntity;

/**
 * 自定义web构建 manager
 */
public class SourceCodeMetadataManager implements MetadataContentManager {
    /**
     * 创建默认的自定义web构件内容
     *
     * @param metadata
     */
    @Override
    public void build(GspMetadata metadata) {
        if (metadata == null) {
            return;
        }

        SourceCodeMetadataEntity sourceCodeMetadataEntity = new SourceCodeMetadataEntity();

        if (metadata.getHeader() != null && !StringUtility.isNullOrEmpty(metadata.getHeader().getId())) {
            sourceCodeMetadataEntity.setId(metadata.getHeader().getId());
            sourceCodeMetadataEntity.setCode(metadata.getHeader().getCode());
            sourceCodeMetadataEntity.setName(metadata.getHeader().getName());
            sourceCodeMetadataEntity.setPath(metadata.getRelativePath());
            metadata.setContent(sourceCodeMetadataEntity);
        }
    }

}
